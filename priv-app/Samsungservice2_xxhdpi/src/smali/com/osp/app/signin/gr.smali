.class final Lcom/osp/app/signin/gr;
.super Lcom/msc/c/b;
.source "PasswordChangeView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/PasswordChangeView;

.field private d:J

.field private e:J

.field private f:J

.field private g:J

.field private h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private k:Z

.field private l:Z

.field private m:Z


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/PasswordChangeView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1952
    iput-object p1, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    .line 1953
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 1938
    iput-boolean v0, p0, Lcom/osp/app/signin/gr;->k:Z

    .line 1943
    iput-boolean v0, p0, Lcom/osp/app/signin/gr;->l:Z

    .line 1954
    iput-object p2, p0, Lcom/osp/app/signin/gr;->i:Ljava/lang/String;

    .line 1955
    iput-object p3, p0, Lcom/osp/app/signin/gr;->j:Ljava/lang/String;

    .line 1956
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2200
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 2201
    iget-object v0, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    iget-object v1, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-static {v1}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, p1, v1, v2, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/gr;->f:J

    .line 2203
    iget-wide v0, p0, Lcom/osp/app/signin/gr;->f:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/gr;->a(J)V

    .line 2204
    iget-wide v0, p0, Lcom/osp/app/signin/gr;->f:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/gr;->a(JLjava/lang/String;)V

    .line 2206
    iget-wide v0, p0, Lcom/osp/app/signin/gr;->f:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 2207
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 2232
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 2234
    iget-object v0, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    const v1, 0x7f0c0108

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/widget/EditText;

    .line 2235
    iget-object v0, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    const v2, 0x7f0c0104

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/EditText;

    .line 2236
    if-eqz v1, :cond_0

    if-nez v4, :cond_1

    .line 2247
    :cond_0
    :goto_0
    return-void

    .line 2240
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    iget-object v2, p0, Lcom/osp/app/signin/gr;->i:Ljava/lang/String;

    iget-object v3, p0, Lcom/osp/app/signin/gr;->j:Ljava/lang/String;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    move-object v6, p0

    invoke-static/range {v0 .. v6}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/gr;->d:J

    .line 2243
    iget-wide v0, p0, Lcom/osp/app/signin/gr;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/gr;->a(J)V

    .line 2244
    iget-wide v0, p0, Lcom/osp/app/signin/gr;->d:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/gr;->a(JLjava/lang/String;)V

    .line 2246
    iget-wide v0, p0, Lcom/osp/app/signin/gr;->d:J

    sget-object v2, Lcom/msc/b/g;->d:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto :goto_0
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 1968
    iget-object v0, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-static {v0}, Lcom/msc/c/e;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1969
    if-eqz v0, :cond_0

    .line 1971
    invoke-direct {p0, v0}, Lcom/osp/app/signin/gr;->c(Ljava/lang/String;)V

    .line 1976
    :goto_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 1974
    :cond_0
    const-string v0, "j5p7ll8g33"

    invoke-direct {p0, v0}, Lcom/osp/app/signin/gr;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 2031
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2033
    if-nez p1, :cond_1

    .line 2108
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2038
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 2039
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 2041
    iget-wide v4, p0, Lcom/osp/app/signin/gr;->e:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_3

    .line 2045
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2046
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 2048
    iget-object v1, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-static {v1, v0}, Lcom/msc/c/e;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 2049
    invoke-virtual {p0}, Lcom/osp/app/signin/gr;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2053
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/gr;->l:Z

    .line 2054
    const-string v0, "j5p7ll8g33"

    invoke-direct {p0, v0}, Lcom/osp/app/signin/gr;->b(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2059
    :catch_0
    move-exception v0

    .line 2061
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2062
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/gr;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 2031
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2057
    :cond_2
    :try_start_4
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/gr;->b:Lcom/msc/c/f;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2064
    :cond_3
    :try_start_5
    iget-wide v4, p0, Lcom/osp/app/signin/gr;->f:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    .line 2066
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onSuccess() : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2069
    :try_start_6
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2070
    invoke-virtual {p0}, Lcom/osp/app/signin/gr;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2074
    const-string v3, "j5p7ll8g33"

    const-string v4, "5763D0052DC1462E13751F753384E9A9"

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    const-string v1, "authorization_code"

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/gr;->g:J

    iget-wide v0, p0, Lcom/osp/app/signin/gr;->g:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/gr;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/gr;->g:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/gr;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/gr;->g:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 2075
    :catch_1
    move-exception v0

    .line 2077
    :try_start_7
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2078
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/gr;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 2080
    :cond_4
    iget-wide v4, p0, Lcom/osp/app/signin/gr;->g:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_5

    .line 2082
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onSuccess() : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 2085
    :try_start_8
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-static {v0, v2}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2086
    iget-object v1, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-static {v1, v0}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 2087
    invoke-virtual {p0}, Lcom/osp/app/signin/gr;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2091
    invoke-direct {p0, v0}, Lcom/osp/app/signin/gr;->c(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 2092
    :catch_2
    move-exception v0

    .line 2094
    :try_start_9
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2095
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/gr;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 2097
    :cond_5
    iget-wide v4, p0, Lcom/osp/app/signin/gr;->d:J
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 2101
    :try_start_a
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-static {v2}, Lcom/msc/c/h;->u(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/gr;->h:Ljava/lang/String;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    .line 2102
    :catch_3
    move-exception v0

    .line 2104
    :try_start_b
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2105
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/gr;->b:Lcom/msc/c/f;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1981
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 1982
    iget-object v0, p0, Lcom/osp/app/signin/gr;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_2

    .line 1984
    iget-boolean v0, p0, Lcom/osp/app/signin/gr;->m:Z

    if-eqz v0, :cond_0

    .line 1986
    iget-object v0, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-static {v0}, Lcom/osp/app/signin/PasswordChangeView;->d(Lcom/osp/app/signin/PasswordChangeView;)V

    .line 1987
    iget-object v0, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/PasswordChangeView;->setResult(I)V

    .line 1988
    iget-object v0, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-virtual {v0}, Lcom/osp/app/signin/PasswordChangeView;->finish()V

    .line 2027
    :goto_0
    return-void

    .line 1990
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/gr;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1992
    iget-object v0, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/PasswordChangeView;->b(I)V

    .line 1994
    :cond_1
    invoke-virtual {p0, v3}, Lcom/osp/app/signin/gr;->a(Z)V

    goto :goto_0

    .line 1997
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/gr;->h:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1999
    const-string v0, "true"

    iget-object v1, p0, Lcom/osp/app/signin/gr;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2005
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    iget-object v1, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    const v2, 0x7f090047

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/PasswordChangeView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2006
    iget-object v0, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/PasswordChangeView;->b(I)V

    .line 2008
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SAMSUNGACCOUNT_CHANGED_PASSWORD_COMPLETED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2010
    iget-object v1, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/PasswordChangeView;->sendBroadcast(Landroid/content/Intent;)V

    .line 2016
    iget-object v0, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-virtual {v0}, Lcom/osp/app/signin/PasswordChangeView;->finish()V

    goto :goto_0

    .line 2019
    :cond_3
    invoke-virtual {p0, v3}, Lcom/osp/app/signin/gr;->a(Z)V

    .line 2020
    iget-object v0, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/PasswordChangeView;->b(I)V

    goto :goto_0

    .line 2024
    :cond_4
    invoke-virtual {p0, v3}, Lcom/osp/app/signin/gr;->a(Z)V

    .line 2025
    iget-object v0, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/PasswordChangeView;->b(I)V

    goto :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2112
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 2114
    if-nez p1, :cond_1

    .line 2170
    :cond_0
    :goto_0
    return-void

    .line 2119
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 2120
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 2121
    invoke-virtual {p1}, Lcom/msc/b/c;->f()Ljava/lang/Exception;

    move-result-object v3

    .line 2125
    iget-wide v4, p0, Lcom/osp/app/signin/gr;->f:J

    cmp-long v4, v0, v4

    if-nez v4, :cond_4

    .line 2127
    if-eqz v2, :cond_0

    .line 2129
    iget-boolean v0, p0, Lcom/osp/app/signin/gr;->l:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/gr;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_3

    const-string v0, "AUT_1302"

    iget-object v1, p0, Lcom/osp/app/signin/gr;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "AUT_1830"

    iget-object v1, p0, Lcom/osp/app/signin/gr;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2133
    :cond_2
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/osp/app/signin/gr;->b:Lcom/msc/c/f;

    .line 2134
    invoke-virtual {p0}, Lcom/osp/app/signin/gr;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2138
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    const v1, 0x7f0c0104

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    iget-object v2, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-static {v2}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x0

    invoke-static {v1, v2, v0, v4, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/osp/app/signin/cn;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/gr;->e:J

    iget-wide v0, p0, Lcom/osp/app/signin/gr;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/gr;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/gr;->e:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/gr;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/gr;->e:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2139
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2142
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0, v3}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v0, p0, Lcom/osp/app/signin/gr;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 2144
    :cond_3
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/osp/app/signin/gr;->l:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/gr;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_0

    const-string v0, "AUT_1094"

    iget-object v1, p0, Lcom/osp/app/signin/gr;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2147
    iput-boolean v6, p0, Lcom/osp/app/signin/gr;->m:Z

    goto/16 :goto_0

    .line 2153
    :cond_4
    iget-wide v4, p0, Lcom/osp/app/signin/gr;->d:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 2155
    if-eqz v2, :cond_0

    .line 2157
    iget-boolean v0, p0, Lcom/osp/app/signin/gr;->k:Z

    if-nez v0, :cond_5

    const-string v0, "ACF_0403"

    iget-object v1, p0, Lcom/osp/app/signin/gr;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2159
    iget-object v0, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 2160
    iput-object v7, p0, Lcom/osp/app/signin/gr;->b:Lcom/msc/c/f;

    .line 2161
    iput-boolean v6, p0, Lcom/osp/app/signin/gr;->k:Z

    .line 2162
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/gr;->l:Z

    .line 2163
    const-string v0, "j5p7ll8g33"

    invoke-direct {p0, v0}, Lcom/osp/app/signin/gr;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2166
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-virtual {v0, v6}, Lcom/osp/app/signin/PasswordChangeView;->b(I)V

    goto/16 :goto_0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 1960
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 1961
    iget-object v0, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/PasswordChangeView;->setResult(I)V

    .line 1962
    iget-object v0, p0, Lcom/osp/app/signin/gr;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-virtual {v0}, Lcom/osp/app/signin/PasswordChangeView;->finish()V

    .line 1963
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1900
    invoke-virtual {p0}, Lcom/osp/app/signin/gr;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1900
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/gr;->a(Ljava/lang/Boolean;)V

    return-void
.end method
