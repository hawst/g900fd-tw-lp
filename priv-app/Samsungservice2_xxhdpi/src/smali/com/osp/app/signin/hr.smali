.class final Lcom/osp/app/signin/hr;
.super Lcom/msc/c/b;
.source "ShowDisclaimerView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/ShowDisclaimerView;

.field private d:Ljava/lang/String;

.field private e:J


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/ShowDisclaimerView;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/osp/app/signin/hr;->c:Lcom/osp/app/signin/ShowDisclaimerView;

    .line 111
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 113
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 6

    .prologue
    .line 125
    iget-object v0, p0, Lcom/osp/app/signin/hr;->c:Lcom/osp/app/signin/ShowDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/ShowDisclaimerView;->a(Lcom/osp/app/signin/ShowDisclaimerView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/hr;->c:Lcom/osp/app/signin/ShowDisclaimerView;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v2, " prepareGetShowDisclaimer go "

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/hr;->e:J

    iget-wide v0, p0, Lcom/osp/app/signin/hr;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/hr;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/hr;->e:J

    const-string v2, "none"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/hr;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/hr;->e:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 127
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 153
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    if-nez p1, :cond_1

    .line 167
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 160
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 161
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 163
    iget-wide v4, p0, Lcom/osp/app/signin/hr;->e:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 165
    iput-object v2, p0, Lcom/osp/app/signin/hr;->d:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 153
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 132
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 133
    iget-object v0, p0, Lcom/osp/app/signin/hr;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_0

    .line 135
    invoke-virtual {p0, v1}, Lcom/osp/app/signin/hr;->a(Z)V

    .line 136
    iget-object v0, p0, Lcom/osp/app/signin/hr;->c:Lcom/osp/app/signin/ShowDisclaimerView;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/ShowDisclaimerView;->b(I)V

    .line 137
    iget-object v0, p0, Lcom/osp/app/signin/hr;->c:Lcom/osp/app/signin/ShowDisclaimerView;

    invoke-virtual {v0}, Lcom/osp/app/signin/ShowDisclaimerView;->finish()V

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/hr;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/hr;->c:Lcom/osp/app/signin/ShowDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/ShowDisclaimerView;->b(Lcom/osp/app/signin/ShowDisclaimerView;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/hr;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    :goto_0
    return-void

    .line 145
    :cond_1
    invoke-virtual {p0, v1}, Lcom/osp/app/signin/hr;->a(Z)V

    .line 146
    iget-object v0, p0, Lcom/osp/app/signin/hr;->c:Lcom/osp/app/signin/ShowDisclaimerView;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/ShowDisclaimerView;->b(I)V

    .line 147
    iget-object v0, p0, Lcom/osp/app/signin/hr;->c:Lcom/osp/app/signin/ShowDisclaimerView;

    invoke-virtual {v0}, Lcom/osp/app/signin/ShowDisclaimerView;->finish()V

    goto :goto_0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 117
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 118
    iget-object v0, p0, Lcom/osp/app/signin/hr;->c:Lcom/osp/app/signin/ShowDisclaimerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/ShowDisclaimerView;->b(I)V

    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/hr;->c:Lcom/osp/app/signin/ShowDisclaimerView;

    invoke-virtual {v0}, Lcom/osp/app/signin/ShowDisclaimerView;->finish()V

    .line 120
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/osp/app/signin/hr;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 95
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/hr;->a(Ljava/lang/Boolean;)V

    return-void
.end method
