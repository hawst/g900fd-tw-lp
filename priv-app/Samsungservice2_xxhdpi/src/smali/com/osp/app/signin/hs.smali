.class final Lcom/osp/app/signin/hs;
.super Lcom/msc/c/b;
.source "ShowTncInfoView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/ShowTncInfoView;

.field private d:Ljava/lang/String;

.field private e:J


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/ShowTncInfoView;)V
    .locals 0

    .prologue
    .line 289
    iput-object p1, p0, Lcom/osp/app/signin/hs;->c:Lcom/osp/app/signin/ShowTncInfoView;

    .line 291
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 295
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 6

    .prologue
    .line 299
    const-string v0, "svctnc"

    iget-object v1, p0, Lcom/osp/app/signin/hs;->c:Lcom/osp/app/signin/ShowTncInfoView;

    invoke-static {v1}, Lcom/osp/app/signin/ShowTncInfoView;->a(Lcom/osp/app/signin/ShowTncInfoView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "tnc"

    iget-object v1, p0, Lcom/osp/app/signin/hs;->c:Lcom/osp/app/signin/ShowTncInfoView;

    invoke-static {v1}, Lcom/osp/app/signin/ShowTncInfoView;->a(Lcom/osp/app/signin/ShowTncInfoView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 301
    :cond_0
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/hs;->c:Lcom/osp/app/signin/ShowTncInfoView;

    iget-object v0, p0, Lcom/osp/app/signin/hs;->c:Lcom/osp/app/signin/ShowTncInfoView;

    invoke-static {v0}, Lcom/osp/app/signin/ShowTncInfoView;->c(Lcom/osp/app/signin/ShowTncInfoView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SRS"

    const-string v2, " prepareGetServiceTNC go "

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/msc/c/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/hs;->e:J

    iget-wide v0, p0, Lcom/osp/app/signin/hs;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/hs;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/hs;->e:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 303
    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 353
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 355
    if-nez p1, :cond_1

    .line 371
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 360
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 361
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 363
    iget-wide v4, p0, Lcom/osp/app/signin/hs;->e:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 365
    if-eqz v2, :cond_0

    .line 367
    iput-object v2, p0, Lcom/osp/app/signin/hs;->d:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 353
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 308
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 309
    iget-object v0, p0, Lcom/osp/app/signin/hs;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_1

    .line 311
    invoke-virtual {p0, v3}, Lcom/osp/app/signin/hs;->a(Z)V

    .line 342
    :cond_0
    :goto_0
    return-void

    .line 314
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/hs;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    const-string v0, ""

    iget-object v1, p0, Lcom/osp/app/signin/hs;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 316
    :cond_2
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/hs;->c:Lcom/osp/app/signin/ShowTncInfoView;

    iget-object v1, p0, Lcom/osp/app/signin/hs;->c:Lcom/osp/app/signin/ShowTncInfoView;

    const v2, 0x7f09003e

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/ShowTncInfoView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 319
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/hs;->c:Lcom/osp/app/signin/ShowTncInfoView;

    invoke-virtual {v0}, Lcom/osp/app/signin/ShowTncInfoView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/osp/app/signin/hs;->c:Lcom/osp/app/signin/ShowTncInfoView;

    invoke-static {v0}, Lcom/osp/app/signin/ShowTncInfoView;->b(Lcom/osp/app/signin/ShowTncInfoView;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/hs;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 375
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 377
    if-nez p1, :cond_1

    .line 401
    :cond_0
    :goto_0
    return-void

    .line 382
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 383
    invoke-virtual {p1}, Lcom/msc/b/c;->f()Ljava/lang/Exception;

    move-result-object v2

    .line 385
    iget-wide v4, p0, Lcom/osp/app/signin/hs;->e:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 390
    const-string v0, " "

    iput-object v0, p0, Lcom/osp/app/signin/hs;->d:Ljava/lang/String;

    .line 392
    if-eqz v2, :cond_2

    .line 394
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v0, p0, Lcom/osp/app/signin/hs;->b:Lcom/msc/c/f;

    goto :goto_0

    .line 397
    :cond_2
    new-instance v0, Lcom/msc/c/f;

    iget-object v1, p0, Lcom/osp/app/signin/hs;->c:Lcom/osp/app/signin/ShowTncInfoView;

    const v2, 0x7f09004a

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/ShowTncInfoView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/msc/c/f;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/hs;->b:Lcom/msc/c/f;

    goto :goto_0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/osp/app/signin/hs;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final onCancelled()V
    .locals 2

    .prologue
    .line 346
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ShowTncInfoView"

    const-string v1, "GetServiceTermsAndConditionsTask is canceled"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    invoke-super {p0}, Lcom/msc/c/b;->onCancelled()V

    .line 348
    iget-object v0, p0, Lcom/osp/app/signin/hs;->c:Lcom/osp/app/signin/ShowTncInfoView;

    invoke-virtual {v0}, Lcom/osp/app/signin/ShowTncInfoView;->finish()V

    .line 349
    return-void
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 274
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/hs;->a(Ljava/lang/Boolean;)V

    return-void
.end method
