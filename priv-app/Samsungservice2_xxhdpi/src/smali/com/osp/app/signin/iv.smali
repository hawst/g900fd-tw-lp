.class final Lcom/osp/app/signin/iv;
.super Ljava/lang/Object;
.source "SignInView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/SignInView;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/SignInView;)V
    .locals 0

    .prologue
    .line 7859
    iput-object p1, p0, Lcom/osp/app/signin/iv;->a:Lcom/osp/app/signin/SignInView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 7865
    new-instance v0, Lcom/osp/app/bigdatalog/c;

    iget-object v1, p0, Lcom/osp/app/signin/iv;->a:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x4

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/osp/app/signin/iv;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v4}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 7867
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/iv;->a:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 7868
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/iv;->a:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;Lcom/osp/app/bigdatalog/c;)V

    .line 7871
    iget-object v0, p0, Lcom/osp/app/signin/iv;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7873
    iget-object v0, p0, Lcom/osp/app/signin/iv;->a:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/osp/app/signin/iv;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/Intent;)V

    .line 7887
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/iv;->a:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignInView;->finish()V

    .line 7889
    return-void

    .line 7877
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/iv;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 7879
    iget-object v0, p0, Lcom/osp/app/signin/iv;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "is_cancelable_just_one_activity"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 7881
    iget-object v0, p0, Lcom/osp/app/signin/iv;->a:Lcom/osp/app/signin/SignInView;

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/osp/app/signin/iv;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/Intent;)V

    goto :goto_0

    .line 7884
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/iv;->a:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/iv;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/Intent;)V

    goto :goto_0
.end method
