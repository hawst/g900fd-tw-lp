.class final Lcom/osp/app/signin/l;
.super Ljava/lang/Object;
.source "AccountView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/AccountView;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/AccountView;)V
    .locals 0

    .prologue
    .line 645
    iput-object p1, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const v9, 0x7f090058

    const v8, 0x10100f

    const/16 v7, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 650
    .line 651
    iget-object v0, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->a(Lcom/osp/app/signin/AccountView;)Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 653
    iget-object v0, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0, v9}, Lcom/osp/app/signin/AccountView;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v3}, Lcom/osp/app/signin/AccountView;->a(Lcom/osp/app/signin/AccountView;)Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    .line 659
    :goto_0
    iget-object v3, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {}, Lcom/osp/app/util/r;->s()Z

    move-result v3

    if-eqz v3, :cond_5

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/osp/app/util/r;->h()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {}, Lcom/osp/app/util/r;->i()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->c()Ljava/lang/String;

    move-result-object v0

    const-string v3, "DCM"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "isCheckDCM :true"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    move v0, v1

    :goto_1
    if-nez v0, :cond_5

    .line 662
    iget-object v0, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->b(Lcom/osp/app/signin/AccountView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 664
    iget-object v0, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountView;->a(Lcom/osp/app/signin/AccountView;Z)Z

    .line 665
    iget-object v0, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030033

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 667
    const v0, 0x7f0c0157

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 668
    iget-object v3, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v3}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f020087

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 670
    const v0, 0x7f0c0156

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 671
    iget-object v3, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v3}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v4}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v4, 0x7f070020

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 673
    iget-object v3, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f090171

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f09006a

    new-instance v6, Lcom/osp/app/signin/n;

    invoke-direct {v6, p0}, Lcom/osp/app/signin/n;-><init>(Lcom/osp/app/signin/l;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    new-instance v5, Lcom/osp/app/signin/m;

    invoke-direct {v5, p0}, Lcom/osp/app/signin/m;-><init>(Lcom/osp/app/signin/l;)V

    invoke-virtual {v4, v9, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/osp/app/signin/AccountView;->a(Lcom/osp/app/signin/AccountView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 703
    iget-object v3, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v3}, Lcom/osp/app/util/r;->w(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v3}, Lcom/osp/app/signin/AccountView;->d(Lcom/osp/app/signin/AccountView;)Lcom/osp/app/signin/h;

    move-result-object v3

    invoke-virtual {v3, v8}, Lcom/osp/app/signin/h;->a(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 705
    iget-object v1, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->d(Lcom/osp/app/signin/AccountView;)Lcom/osp/app/signin/h;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/osp/app/signin/h;->b(I)Lcom/osp/app/signin/j;

    move-result-object v1

    .line 707
    new-instance v3, Landroid/widget/ListView;

    iget-object v4, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-direct {v3, v4}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 708
    invoke-virtual {v3, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 709
    invoke-virtual {v3, v2}, Landroid/widget/ListView;->setClickable(Z)V

    .line 710
    const/4 v1, 0x0

    invoke-virtual {v3, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 711
    const/4 v1, 0x2

    invoke-virtual {v3, v1}, Landroid/widget/ListView;->setOverScrollMode(I)V

    .line 712
    const/high16 v1, 0x2000000

    invoke-virtual {v3, v1}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    .line 715
    iget-object v1, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v1}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f080045

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 716
    invoke-static {v3, v1, v2, v1, v2}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    .line 718
    iget-object v1, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->e(Lcom/osp/app/signin/AccountView;)Landroid/app/AlertDialog;

    move-result-object v1

    const v4, 0x7f09004f

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 719
    iget-object v1, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->e(Lcom/osp/app/signin/AccountView;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    .line 726
    :goto_2
    iget-object v1, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->e(Lcom/osp/app/signin/AccountView;)Landroid/app/AlertDialog;

    move-result-object v1

    new-instance v3, Lcom/osp/app/signin/o;

    invoke-direct {v3, p0}, Lcom/osp/app/signin/o;-><init>(Lcom/osp/app/signin/l;)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 734
    iget-object v1, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->e(Lcom/osp/app/signin/AccountView;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 735
    iget-object v1, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->e(Lcom/osp/app/signin/AccountView;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 737
    iget-object v1, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->e(Lcom/osp/app/signin/AccountView;)Landroid/app/AlertDialog;

    move-result-object v1

    const v3, 0x102000b

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 738
    if-eqz v1, :cond_0

    .line 740
    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 741
    invoke-virtual {v1}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 742
    invoke-virtual {v1}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 744
    invoke-virtual {v1}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 745
    if-eqz v0, :cond_4

    .line 747
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 749
    if-eqz v1, :cond_3

    .line 751
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 772
    :cond_0
    :goto_3
    return-void

    .line 659
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "isCheckDCM :false"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    move v0, v2

    goto/16 :goto_1

    .line 722
    :cond_2
    iget-object v3, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v3}, Lcom/osp/app/signin/AccountView;->e(Lcom/osp/app/signin/AccountView;)Landroid/app/AlertDialog;

    move-result-object v3

    const v4, 0x7f090116

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 723
    iget-object v3, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v3}, Lcom/osp/app/signin/AccountView;->e(Lcom/osp/app/signin/AccountView;)Landroid/app/AlertDialog;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    goto :goto_2

    .line 754
    :cond_3
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 758
    :cond_4
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 764
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 766
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "Logging Service initiated by just clicking on skip button"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 767
    iget-object v0, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountView;->f()V

    .line 769
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/AccountView;->b(I)V

    .line 770
    iget-object v0, p0, Lcom/osp/app/signin/l;->a:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountView;->finish()V

    goto :goto_3

    :cond_7
    move v0, v2

    goto/16 :goto_0
.end method
