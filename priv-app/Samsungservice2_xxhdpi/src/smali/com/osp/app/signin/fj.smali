.class public final Lcom/osp/app/signin/fj;
.super Lcom/msc/c/b;
.source "EmailValidationView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/EmailValidationView;

.field private d:Landroid/app/ProgressDialog;

.field private e:Z

.field private f:J

.field private g:Lcom/msc/a/g;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/EmailValidationView;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    .line 71
    invoke-direct {p0, p2}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 73
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 77
    new-instance v0, Lcom/msc/a/f;

    invoke-direct {v0}, Lcom/msc/a/f;-><init>()V

    .line 78
    iget-object v1, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v1}, Lcom/osp/app/signin/EmailValidationView;->a(Lcom/osp/app/signin/EmailValidationView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->d(Ljava/lang/String;)V

    .line 80
    iget-object v1, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v1}, Lcom/osp/app/signin/EmailValidationView;->b(Lcom/osp/app/signin/EmailValidationView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->f(Ljava/lang/String;)V

    .line 81
    iget-object v1, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v1}, Lcom/osp/app/signin/EmailValidationView;->c(Lcom/osp/app/signin/EmailValidationView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->g(Ljava/lang/String;)V

    .line 82
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v1}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 86
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 88
    const-string v2, "Y"

    invoke-virtual {v0, v2}, Lcom/msc/a/f;->b(Ljava/lang/String;)V

    .line 89
    invoke-virtual {v0, v1}, Lcom/msc/a/f;->c(Ljava/lang/String;)V

    .line 93
    :cond_0
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 94
    iget-object v1, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v1, v0, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/a/f;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/fj;->f:J

    .line 96
    iget-wide v0, p0, Lcom/osp/app/signin/fj;->f:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/fj;->a(J)V

    .line 97
    iget-wide v0, p0, Lcom/osp/app/signin/fj;->f:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/fj;->a(JLjava/lang/String;)V

    .line 98
    iget-wide v0, p0, Lcom/osp/app/signin/fj;->f:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 99
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 180
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182
    if-nez p1, :cond_1

    .line 206
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 187
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 188
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 190
    iget-wide v4, p0, Lcom/osp/app/signin/fj;->f:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 194
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v0, v2}, Lcom/msc/c/h;->b(Landroid/content/Context;Ljava/lang/String;)Lcom/msc/a/g;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/fj;->g:Lcom/msc/a/g;

    .line 196
    iget-object v0, p0, Lcom/osp/app/signin/fj;->g:Lcom/msc/a/g;

    invoke-virtual {v0}, Lcom/msc/a/g;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 198
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/fj;->e:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 200
    :catch_0
    move-exception v0

    .line 202
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 203
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/fj;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 180
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 104
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 107
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-virtual {v0}, Lcom/osp/app/signin/EmailValidationView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/fj;->d:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/osp/app/signin/fj;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/fj;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_2

    .line 113
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "USR_3174"

    iget-object v1, p0, Lcom/osp/app/signin/fj;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 115
    iget-object v0, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    iget-object v1, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/EmailValidationView;->a(Lcom/osp/app/signin/EmailValidationView;Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-virtual {v0}, Lcom/osp/app/signin/EmailValidationView;->finish()V

    .line 158
    :cond_1
    :goto_0
    return-void

    .line 120
    :cond_2
    iget-boolean v0, p0, Lcom/osp/app/signin/fj;->e:Z

    if-eqz v0, :cond_4

    .line 123
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.osp.app.signin.action.EMAIL_VALIDATION_COMPLETED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 124
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->l()I

    move-result v1

    const/16 v2, 0xb

    if-le v1, v2, :cond_3

    .line 126
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 128
    :cond_3
    iget-object v1, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/EmailValidationView;->sendBroadcast(Landroid/content/Intent;)V

    .line 129
    iget-object v0, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    const/16 v1, 0x64

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/EmailValidationView;->a(ILandroid/content/DialogInterface$OnDismissListener;)V

    .line 130
    iget-object v0, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/osp/app/signin/EmailValidationView;->a(Lcom/osp/app/signin/EmailValidationView;Z)Z

    .line 131
    iget-object v0, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    iget-object v1, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v1}, Lcom/osp/app/signin/EmailValidationView;->d(Lcom/osp/app/signin/EmailValidationView;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/EmailValidationView;->b(Lcom/osp/app/signin/EmailValidationView;Z)V

    .line 133
    iget-object v0, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/EmailValidationView;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 134
    const v1, 0x132df82

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 136
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 138
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 139
    const-string v1, "com.msc.action.MARKETING_POPUP_NOTIFICATION"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 140
    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.BackgroundModeService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    iget-object v1, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/EmailValidationView;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 142
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "EVV"

    const-string v1, "startService MarketingPopupNotiIntent"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 154
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 147
    :cond_4
    :try_start_1
    iget-object v0, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v0}, Lcom/osp/app/signin/EmailValidationView;->e(Lcom/osp/app/signin/EmailValidationView;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 149
    iget-object v0, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v0}, Lcom/osp/app/signin/EmailValidationView;->f(Lcom/osp/app/signin/EmailValidationView;)V

    .line 151
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/EmailValidationView;->a(Lcom/osp/app/signin/EmailValidationView;Z)Z

    .line 152
    iget-object v0, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    iget-object v1, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v1}, Lcom/osp/app/signin/EmailValidationView;->d(Lcom/osp/app/signin/EmailValidationView;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/EmailValidationView;->b(Lcom/osp/app/signin/EmailValidationView;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/osp/app/signin/fj;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 63
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/fj;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected final onPreExecute()V
    .locals 3

    .prologue
    .line 162
    invoke-super {p0}, Lcom/msc/c/b;->onPreExecute()V

    .line 163
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/app/signin/fj;->d:Landroid/app/ProgressDialog;

    .line 164
    iget-object v0, p0, Lcom/osp/app/signin/fj;->d:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    const v2, 0x7f090036

    invoke-static {v1, v2}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 165
    iget-object v0, p0, Lcom/osp/app/signin/fj;->d:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 168
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/fj;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-virtual {v0}, Lcom/osp/app/signin/EmailValidationView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/osp/app/signin/fj;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 172
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
