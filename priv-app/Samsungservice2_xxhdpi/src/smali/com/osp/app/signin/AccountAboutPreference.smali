.class public Lcom/osp/app/signin/AccountAboutPreference;
.super Lcom/osp/app/util/BasePreferenceActivity;
.source "AccountAboutPreference.java"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/osp/app/util/BasePreferenceActivity;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountAboutPreference;->a:Z

    return-void
.end method

.method private a(I)V
    .locals 4

    .prologue
    const v3, 0x7f08011e

    .line 179
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AccountView::initLayoutParams orientation : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 181
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 183
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountAboutPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08011f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 184
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountAboutPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 185
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 187
    const v0, 0x7f0c015e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountAboutPreference;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 188
    invoke-virtual {p0, v0, p1}, Lcom/osp/app/signin/AccountAboutPreference;->a(Landroid/view/ViewGroup;I)V

    .line 190
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountAboutPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 192
    :cond_0
    const v2, 0x7f0c0161

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/AccountAboutPreference;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 193
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-static {v2, v0, v1, p1}, Lcom/osp/app/util/q;->b(Landroid/view/View;III)V

    .line 195
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountAboutPreference;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 32
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.web_with_close_button"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "ServerUrl"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountAboutPreference;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountAboutPreference;)Z
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountAboutPreference;->a()Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 51
    invoke-super {p0, p1, p2, p3}, Lcom/osp/app/util/BasePreferenceActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountAboutPreference;->a:Z

    .line 53
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountAboutPreference;->setResult(I)V

    .line 46
    invoke-super {p0}, Lcom/osp/app/util/BasePreferenceActivity;->onBackPressed()V

    .line 47
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 165
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AAP"

    const-string v1, "onConfigurationChanged()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountAboutPreference;->a(I)V

    .line 169
    invoke-super {p0, p1}, Lcom/osp/app/util/BasePreferenceActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 170
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const v3, 0x7f0800e7

    .line 59
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/osp/app/signin/SamsungService;->a(Z)V

    .line 61
    invoke-super {p0, p1}, Lcom/osp/app/util/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 62
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Samsung account does not exist"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 65
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountAboutPreference;->finish()V

    .line 149
    :goto_0
    return-void

    .line 77
    :cond_0
    const v0, 0x7f030005

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountAboutPreference;->setContentView(I)V

    .line 78
    const v0, 0x7f040001

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountAboutPreference;->addPreferencesFromResource(I)V

    .line 80
    const v0, 0x7f0c0037

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountAboutPreference;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 81
    const v1, 0x7f0c0036

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountAboutPreference;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 82
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 83
    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 84
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountAboutPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 85
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountAboutPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 86
    invoke-static {v0, v1}, Lcom/msc/sa/c/d;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 88
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountAboutPreference;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 90
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountAboutPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 95
    :goto_1
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 97
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountAboutPreference;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 99
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountAboutPreference;->b()V

    .line 106
    :cond_1
    :goto_2
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountAboutPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountAboutPreference;->a(I)V

    .line 108
    const-string v0, "Samsung_services"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountAboutPreference;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 109
    new-instance v1, Lcom/osp/app/signin/a;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/a;-><init>(Lcom/osp/app/signin/AccountAboutPreference;)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 147
    invoke-static {p0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;)V

    goto/16 :goto_0

    .line 93
    :cond_2
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountAboutPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070013

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 102
    :cond_3
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountAboutPreference;->c()V

    goto :goto_2
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 221
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 223
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 227
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/osp/app/util/BasePreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 226
    :pswitch_0
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountAboutPreference;->onBackPressed()V

    goto :goto_0

    .line 223
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 199
    invoke-super {p0}, Lcom/osp/app/util/BasePreferenceActivity;->onResume()V

    .line 201
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountAboutPreference;->a:Z

    if-eqz v0, :cond_0

    .line 203
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountAboutPreference;->finish()V

    .line 205
    :cond_0
    return-void
.end method
