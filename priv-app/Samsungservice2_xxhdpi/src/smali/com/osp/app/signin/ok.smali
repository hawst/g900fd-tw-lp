.class final Lcom/osp/app/signin/ok;
.super Lcom/msc/c/b;
.source "TnCView.java"


# instance fields
.field public c:J

.field public d:J

.field public e:J

.field public f:J

.field public g:J

.field public h:J

.field public i:J

.field public j:J

.field final synthetic k:Lcom/osp/app/signin/TnCView;

.field private final l:Landroid/os/Handler;

.field private m:Ljava/lang/String;

.field private n:Lcom/msc/a/b;

.field private o:Lcom/osp/app/signin/om;

.field private p:Lcom/osp/app/signin/on;

.field private q:Lcom/osp/app/signin/oo;

.field private r:J

.field private s:J


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/TnCView;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 5766
    iput-object p1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    .line 5767
    const v0, 0x7f090056

    invoke-direct {p0, p2, v0}, Lcom/msc/c/b;-><init>(Landroid/content/Context;I)V

    .line 5746
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/ok;->l:Landroid/os/Handler;

    .line 5747
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/ok;->m:Ljava/lang/String;

    .line 5771
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/ok;Lcom/msc/c/f;)Lcom/msc/c/f;
    .locals 0

    .prologue
    .line 5744
    iput-object p1, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/ok;)Lcom/osp/app/signin/om;
    .locals 1

    .prologue
    .line 5744
    iget-object v0, p0, Lcom/osp/app/signin/ok;->o:Lcom/osp/app/signin/om;

    return-object v0
.end method

.method static synthetic a(Lcom/osp/app/signin/ok;J)V
    .locals 1

    .prologue
    .line 5744
    invoke-virtual {p0, p1, p2}, Lcom/osp/app/signin/ok;->b(J)V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/ok;JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 5744
    invoke-virtual {p0, p1, p2, p3}, Lcom/osp/app/signin/ok;->a(JLjava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/ok;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 5744
    invoke-virtual {p0, p1}, Lcom/osp/app/signin/ok;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/osp/app/signin/ok;Lcom/msc/c/f;)Lcom/msc/c/f;
    .locals 0

    .prologue
    .line 5744
    iput-object p1, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    return-object p1
.end method

.method static synthetic b(Lcom/osp/app/signin/ok;)Lcom/osp/app/signin/on;
    .locals 1

    .prologue
    .line 5744
    iget-object v0, p0, Lcom/osp/app/signin/ok;->p:Lcom/osp/app/signin/on;

    return-object v0
.end method

.method static synthetic b(Lcom/osp/app/signin/ok;J)V
    .locals 1

    .prologue
    .line 5744
    invoke-virtual {p0, p1, p2}, Lcom/osp/app/signin/ok;->b(J)V

    return-void
.end method

.method static synthetic b(Lcom/osp/app/signin/ok;JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 5744
    invoke-virtual {p0, p1, p2, p3}, Lcom/osp/app/signin/ok;->a(JLjava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 6527
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SignUpView::SignUpDual_End parsedText : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6529
    const-string v1, "Success"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 6532
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "=========================================== SignInDual_End PROCESSING_SUCCESS==========================================="

    invoke-static {v1}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 6563
    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 6565
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->S(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->d()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 6576
    :cond_0
    if-nez v0, :cond_2

    .line 6578
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "TncView SignUpDualEnd"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6579
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/TnCView;->b(I)V

    .line 6580
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "ERR_MDM_SECURITY"

    const-string v2, "fail_MDM_Security"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    .line 6581
    const-string v0, "Fail"

    invoke-direct {p0, v0}, Lcom/osp/app/signin/ok;->b(Ljava/lang/String;)V

    .line 6582
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->finish()V

    .line 6684
    :cond_1
    :goto_0
    return-void

    .line 6589
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    const-string v1, "14eev3f64b"

    invoke-static {v0, v1}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 6595
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->W(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->S(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/SignUpinfo;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v3}, Lcom/msc/c/c;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 6596
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->ad(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/c/e;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 6597
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/msc/c/c;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->S(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/SignUpinfo;->d()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v3}, Lcom/msc/c/c;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 6602
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->af(Lcom/osp/app/signin/TnCView;)V

    .line 6604
    invoke-static {}, Lcom/osp/app/pushmarketing/k;->a()Lcom/osp/app/pushmarketing/k;

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/k;->c(Landroid/content/Context;)V

    .line 6606
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->S(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/TnCView;->d(Lcom/osp/app/signin/TnCView;Ljava/lang/String;)V

    goto :goto_0

    .line 6608
    :cond_3
    const-string v1, "SignInFail"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 6611
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->c()V

    goto :goto_0

    .line 6615
    :cond_4
    iget-object v1, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    if-eqz v1, :cond_1

    .line 6617
    iget-object v1, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    iget-object v1, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_b

    .line 6619
    const-string v1, "USR_3117"

    iget-object v2, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    invoke-virtual {v2}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "PRT_3011"

    iget-object v2, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    invoke-virtual {v2}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 6621
    :cond_5
    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->ag(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/util/n;

    iget-object v1, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 6622
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 6623
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v2, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v4}, Lcom/osp/app/signin/TnCView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090060

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v0, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    invoke-virtual {v3}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 6662
    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "signinstae = error getCode()  : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    invoke-virtual {v2}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6637
    :cond_6
    const-string v0, "MCC_NULL"

    iget-object v1, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 6639
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "mcc null error during sign up"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6640
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/msc/c/e;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 6641
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 6643
    :cond_7
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "mcc from db null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6644
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 6646
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mcc save to db "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6647
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v2}, Lcom/osp/app/signin/TnCView;->getApplicationContext()Landroid/content/Context;

    iget-object v2, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 6654
    :cond_8
    :goto_2
    invoke-virtual {p0, v6}, Lcom/osp/app/signin/ok;->a(Z)V

    goto/16 :goto_1

    .line 6650
    :cond_9
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "finish sign up flow cause by mcc null error"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6651
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->finish()V

    goto :goto_2

    .line 6660
    :cond_a
    invoke-virtual {p0, v6}, Lcom/osp/app/signin/ok;->a(Z)V

    goto/16 :goto_1

    .line 6666
    :cond_b
    iget-object v1, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_c

    .line 6674
    invoke-virtual {p0, v6}, Lcom/osp/app/signin/ok;->a(Z)V

    .line 6676
    :cond_c
    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->ac(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/jz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/jz;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 6678
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    iget-object v2, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->ac(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/jz;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/jz;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0
.end method

.method static synthetic c(Lcom/osp/app/signin/ok;)Lcom/msc/a/b;
    .locals 1

    .prologue
    .line 5744
    iget-object v0, p0, Lcom/osp/app/signin/ok;->n:Lcom/msc/a/b;

    return-object v0
.end method

.method static synthetic c(Lcom/osp/app/signin/ok;Lcom/msc/c/f;)Lcom/msc/c/f;
    .locals 0

    .prologue
    .line 5744
    iput-object p1, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    return-object p1
.end method

.method static synthetic c(Lcom/osp/app/signin/ok;J)V
    .locals 1

    .prologue
    .line 5744
    invoke-virtual {p0, p1, p2}, Lcom/osp/app/signin/ok;->b(J)V

    return-void
.end method

.method static synthetic c(Lcom/osp/app/signin/ok;JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 5744
    invoke-virtual {p0, p1, p2, p3}, Lcom/osp/app/signin/ok;->a(JLjava/lang/String;)V

    return-void
.end method

.method static synthetic d(Lcom/osp/app/signin/ok;Lcom/msc/c/f;)Lcom/msc/c/f;
    .locals 0

    .prologue
    .line 5744
    iput-object p1, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    return-object p1
.end method

.method static synthetic d(Lcom/osp/app/signin/ok;J)V
    .locals 1

    .prologue
    .line 5744
    invoke-virtual {p0, p1, p2}, Lcom/osp/app/signin/ok;->b(J)V

    return-void
.end method

.method static synthetic d(Lcom/osp/app/signin/ok;JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 5744
    invoke-virtual {p0, p1, p2, p3}, Lcom/osp/app/signin/ok;->a(JLjava/lang/String;)V

    return-void
.end method

.method static synthetic e(Lcom/osp/app/signin/ok;Lcom/msc/c/f;)Lcom/msc/c/f;
    .locals 0

    .prologue
    .line 5744
    iput-object p1, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    return-object p1
.end method

.method static synthetic e(Lcom/osp/app/signin/ok;J)V
    .locals 1

    .prologue
    .line 5744
    invoke-virtual {p0, p1, p2}, Lcom/osp/app/signin/ok;->b(J)V

    return-void
.end method

.method static synthetic e(Lcom/osp/app/signin/ok;JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 5744
    invoke-virtual {p0, p1, p2, p3}, Lcom/osp/app/signin/ok;->a(JLjava/lang/String;)V

    return-void
.end method

.method static synthetic f(Lcom/osp/app/signin/ok;Lcom/msc/c/f;)Lcom/msc/c/f;
    .locals 0

    .prologue
    .line 5744
    iput-object p1, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    return-object p1
.end method

.method static synthetic f(Lcom/osp/app/signin/ok;J)V
    .locals 1

    .prologue
    .line 5744
    invoke-virtual {p0, p1, p2}, Lcom/osp/app/signin/ok;->b(J)V

    return-void
.end method

.method static synthetic f(Lcom/osp/app/signin/ok;JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 5744
    invoke-virtual {p0, p1, p2, p3}, Lcom/osp/app/signin/ok;->a(JLjava/lang/String;)V

    return-void
.end method

.method static synthetic g(Lcom/osp/app/signin/ok;J)V
    .locals 1

    .prologue
    .line 5744
    invoke-virtual {p0, p1, p2}, Lcom/osp/app/signin/ok;->a(J)V

    return-void
.end method

.method static synthetic g(Lcom/osp/app/signin/ok;JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 5744
    invoke-virtual {p0, p1, p2, p3}, Lcom/osp/app/signin/ok;->a(JLjava/lang/String;)V

    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 6216
    new-instance v0, Lcom/osp/app/signin/om;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/om;-><init>(Lcom/osp/app/signin/ok;)V

    iput-object v0, p0, Lcom/osp/app/signin/ok;->o:Lcom/osp/app/signin/om;

    .line 6217
    new-instance v0, Lcom/osp/app/signin/on;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/on;-><init>(Lcom/osp/app/signin/ok;)V

    iput-object v0, p0, Lcom/osp/app/signin/ok;->p:Lcom/osp/app/signin/on;

    .line 6219
    iget-object v0, p0, Lcom/osp/app/signin/ok;->l:Landroid/os/Handler;

    new-instance v1, Lcom/osp/app/signin/ol;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/ol;-><init>(Lcom/osp/app/signin/ok;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 6252
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/ok;->o:Lcom/osp/app/signin/om;

    invoke-virtual {v0}, Lcom/osp/app/signin/om;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/ok;->p:Lcom/osp/app/signin/on;

    invoke-virtual {v0}, Lcom/osp/app/signin/on;->d()Z

    move-result v0

    if-nez v0, :cond_2

    .line 6254
    :cond_1
    invoke-virtual {p0}, Lcom/osp/app/signin/ok;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6256
    :cond_2
    const-string v0, "Success"

    iget-object v1, p0, Lcom/osp/app/signin/ok;->o:Lcom/osp/app/signin/om;

    invoke-virtual {v1}, Lcom/osp/app/signin/om;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "Success"

    iget-object v1, p0, Lcom/osp/app/signin/ok;->p:Lcom/osp/app/signin/on;

    invoke-virtual {v1}, Lcom/osp/app/signin/on;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 6274
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/ok;->m:Ljava/lang/String;

    .line 6283
    :goto_0
    return-void

    .line 6278
    :cond_3
    const-string v0, "SignInFail"

    iput-object v0, p0, Lcom/osp/app/signin/ok;->m:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 11

    .prologue
    .line 5779
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5781
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/msc/openprovider/b;->e(Landroid/content/Context;)V

    .line 5783
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->T(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SignUpView::SignUpDualSignUp OspVersion : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->S(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->k(Lcom/osp/app/signin/TnCView;)Z

    move-result v2

    iget-object v3, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v3}, Lcom/osp/app/signin/TnCView;->l(Lcom/osp/app/signin/TnCView;)Z

    move-result v3

    iget-object v4, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v4}, Lcom/osp/app/signin/TnCView;->m(Lcom/osp/app/signin/TnCView;)Z

    move-result v4

    iget-object v5, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v5}, Lcom/osp/app/signin/TnCView;->n(Lcom/osp/app/signin/TnCView;)Z

    move-result v5

    iget-object v6, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v6}, Lcom/osp/app/signin/TnCView;->Y(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v7}, Lcom/osp/app/signin/TnCView;->Z(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v8}, Lcom/osp/app/signin/TnCView;->aa(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v9}, Lcom/osp/app/signin/TnCView;->y(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    iget-object v9, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v9}, Lcom/osp/app/signin/TnCView;->I(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    iget-object v9, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v9}, Lcom/osp/app/signin/TnCView;->ab(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v9

    move-object v10, p0

    invoke-static/range {v0 .. v10}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/osp/app/signin/SignUpinfo;ZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/ok;->r:J

    iget-wide v0, p0, Lcom/osp/app/signin/ok;->r:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/ok;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/ok;->r:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/ok;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/ok;->r:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/osp/app/signin/ok;->h()V

    .line 5785
    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 7

    .prologue
    .line 5870
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5872
    if-nez p1, :cond_1

    .line 5940
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 5877
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 5878
    invoke-virtual {p1}, Lcom/msc/b/c;->b()Ljava/lang/String;

    .line 5879
    invoke-virtual {p1}, Lcom/msc/b/c;->c()Ljava/util/HashMap;

    .line 5880
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 5882
    iget-wide v4, p0, Lcom/osp/app/signin/ok;->r:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    .line 5884
    :try_start_2
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/TnCView;->c(Lcom/osp/app/signin/TnCView;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    :try_start_3
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->W(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->W(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    iget-object v0, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    const-string v1, "ERR_0000"

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    const-string v1, "fail"

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/TnCView;->b(I)V

    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/ok;->m:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 5870
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 5884
    :catch_0
    move-exception v0

    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->W(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->S(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v3}, Lcom/osp/app/signin/TnCView;->S(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/signin/SignUpinfo;->i()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->X(Lcom/osp/app/signin/TnCView;)Lcom/osp/security/identity/f;

    move-result-object v0

    sget-object v1, Lcom/msc/c/d;->a:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->W(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/security/identity/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->X(Lcom/osp/app/signin/TnCView;)Lcom/osp/security/identity/f;

    move-result-object v0

    sget-object v1, Lcom/msc/c/d;->b:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->S(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/security/identity/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->X(Lcom/osp/app/signin/TnCView;)Lcom/osp/security/identity/f;

    move-result-object v0

    sget-object v1, Lcom/msc/c/d;->h:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/security/identity/f;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    :catch_1
    move-exception v0

    :try_start_6
    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    const-string v1, "No peer certificate"

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    const v2, 0x7f090027

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    const-string v1, "NPC"

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->a(Ljava/lang/String;)V

    :cond_3
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/ok;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 5885
    :cond_4
    iget-wide v4, p0, Lcom/osp/app/signin/ok;->c:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_5

    .line 5887
    iget-object v0, p0, Lcom/osp/app/signin/ok;->o:Lcom/osp/app/signin/om;

    if-eqz v0, :cond_0

    .line 5889
    iget-object v0, p0, Lcom/osp/app/signin/ok;->o:Lcom/osp/app/signin/om;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    iget-object v0, v0, Lcom/osp/app/signin/om;->a:Lcom/osp/app/signin/ok;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->r(Ljava/lang/String;)Lcom/msc/a/b;

    move-result-object v1

    iput-object v1, v0, Lcom/osp/app/signin/ok;->n:Lcom/msc/a/b;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v2}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    :catch_2
    move-exception v0

    :try_start_8
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 5896
    :cond_5
    iget-wide v4, p0, Lcom/osp/app/signin/ok;->d:J

    cmp-long v3, v0, v4

    if-eqz v3, :cond_6

    iget-wide v4, p0, Lcom/osp/app/signin/ok;->f:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_7

    .line 5898
    :cond_6
    iget-object v3, p0, Lcom/osp/app/signin/ok;->p:Lcom/osp/app/signin/on;

    if-eqz v3, :cond_0

    .line 5900
    iget-object v3, p0, Lcom/osp/app/signin/ok;->p:Lcom/osp/app/signin/on;

    invoke-static {v3, v0, v1, v2}, Lcom/osp/app/signin/on;->a(Lcom/osp/app/signin/on;JLjava/lang/String;)V

    goto/16 :goto_0

    .line 5906
    :cond_7
    iget-wide v4, p0, Lcom/osp/app/signin/ok;->e:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_8

    .line 5908
    iget-object v0, p0, Lcom/osp/app/signin/ok;->p:Lcom/osp/app/signin/on;

    if-eqz v0, :cond_0

    .line 5910
    iget-object v0, p0, Lcom/osp/app/signin/ok;->p:Lcom/osp/app/signin/on;

    invoke-static {v0, v2}, Lcom/osp/app/signin/on;->a(Lcom/osp/app/signin/on;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5912
    :cond_8
    iget-wide v4, p0, Lcom/osp/app/signin/ok;->g:J

    cmp-long v3, v0, v4

    if-eqz v3, :cond_9

    iget-wide v4, p0, Lcom/osp/app/signin/ok;->f:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_a

    .line 5914
    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/ok;->q:Lcom/osp/app/signin/oo;

    if-eqz v0, :cond_0

    .line 5916
    iget-object v0, p0, Lcom/osp/app/signin/ok;->q:Lcom/osp/app/signin/oo;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/oo;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5918
    :cond_a
    iget-wide v4, p0, Lcom/osp/app/signin/ok;->h:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_b

    .line 5920
    iget-object v0, p0, Lcom/osp/app/signin/ok;->q:Lcom/osp/app/signin/oo;

    if-eqz v0, :cond_0

    .line 5922
    iget-object v0, p0, Lcom/osp/app/signin/ok;->q:Lcom/osp/app/signin/oo;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/oo;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5924
    :cond_b
    iget-wide v4, p0, Lcom/osp/app/signin/ok;->i:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_c

    .line 5926
    iget-object v0, p0, Lcom/osp/app/signin/ok;->q:Lcom/osp/app/signin/oo;

    if-eqz v0, :cond_0

    .line 5928
    iget-object v1, p0, Lcom/osp/app/signin/ok;->q:Lcom/osp/app/signin/oo;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :try_start_9
    iget-object v0, v1, Lcom/osp/app/signin/oo;->a:Lcom/osp/app/signin/ok;

    iget-object v0, v0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/signin/TnCView;->f(Lcom/osp/app/signin/TnCView;Ljava/lang/String;)Ljava/lang/String;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :goto_2
    :try_start_a
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v2, "signinstae = auth"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, v1, Lcom/osp/app/signin/oo;->a:Lcom/osp/app/signin/ok;

    iget-object v2, v1, Lcom/osp/app/signin/oo;->a:Lcom/osp/app/signin/ok;

    iget-object v2, v2, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    iget-object v3, v1, Lcom/osp/app/signin/oo;->a:Lcom/osp/app/signin/ok;

    iget-object v3, v3, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v3}, Lcom/osp/app/signin/TnCView;->y(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v1, Lcom/osp/app/signin/oo;->a:Lcom/osp/app/signin/ok;

    iget-object v4, v4, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v4}, Lcom/osp/app/signin/TnCView;->ad(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v1, Lcom/osp/app/signin/oo;->a:Lcom/osp/app/signin/ok;

    iget-object v5, v5, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v5}, Lcom/osp/app/signin/TnCView;->ah(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v1, Lcom/osp/app/signin/oo;->a:Lcom/osp/app/signin/ok;

    invoke-static {v2, v3, v4, v5, v6}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/osp/app/signin/ok;->j:J

    iget-object v0, v1, Lcom/osp/app/signin/oo;->a:Lcom/osp/app/signin/ok;

    iget-object v2, v1, Lcom/osp/app/signin/oo;->a:Lcom/osp/app/signin/ok;

    iget-wide v2, v2, Lcom/osp/app/signin/ok;->j:J

    invoke-virtual {v0, v2, v3}, Lcom/osp/app/signin/ok;->b(J)V

    iget-object v0, v1, Lcom/osp/app/signin/oo;->a:Lcom/osp/app/signin/ok;

    iget-object v2, v1, Lcom/osp/app/signin/oo;->a:Lcom/osp/app/signin/ok;

    iget-wide v2, v2, Lcom/osp/app/signin/ok;->j:J

    const-string v4, "from_json"

    invoke-virtual {v0, v2, v3, v4}, Lcom/osp/app/signin/ok;->a(JLjava/lang/String;)V

    iget-object v0, v1, Lcom/osp/app/signin/oo;->a:Lcom/osp/app/signin/ok;

    iget-wide v0, v0, Lcom/osp/app/signin/ok;->j:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto/16 :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 5930
    :cond_c
    iget-wide v4, p0, Lcom/osp/app/signin/ok;->j:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_d

    .line 5932
    iget-object v0, p0, Lcom/osp/app/signin/ok;->q:Lcom/osp/app/signin/oo;

    if-eqz v0, :cond_0

    .line 5934
    iget-object v0, p0, Lcom/osp/app/signin/ok;->q:Lcom/osp/app/signin/oo;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/oo;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5936
    :cond_d
    iget-wide v2, p0, Lcom/osp/app/signin/ok;->s:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5938
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "SignUpView - Name Check Success."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 5804
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 5806
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SignUpAsyncTASK.onPostExecute() resultString is %s"

    new-array v1, v8, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/osp/app/signin/ok;->m:Ljava/lang/String;

    aput-object v2, v1, v9

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5810
    iget-object v0, p0, Lcom/osp/app/signin/ok;->m:Ljava/lang/String;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SignUpView::SignUpDual_End parsedText : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    const-string v1, "Success"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "=========================================== SignInDual_End PROCESSING_SUCCESS==========================================="

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_12

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->S(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->d()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    :goto_0
    if-nez v0, :cond_3

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "TncView SignUpNewDualEnd"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/TnCView;->b(I)V

    new-instance v0, Lcom/msc/c/f;

    const-string v1, "ERR_MDM_SECURITY"

    const-string v2, "fail_MDM_Security"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    const-string v0, "Fail"

    invoke-direct {p0, v0}, Lcom/osp/app/signin/ok;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->finish()V

    .line 5823
    :cond_0
    :goto_1
    const-string v0, "Success"

    iget-object v1, p0, Lcom/osp/app/signin/ok;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5825
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5829
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5831
    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    const-string v0, "450"

    iget-object v2, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->d(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-nez v0, :cond_11

    move v0, v8

    :goto_2
    iget-object v2, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->S(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/SignUpinfo;->m()Z

    move-result v2

    invoke-static {v1, v9, v0, v2}, Lcom/msc/openprovider/b;->a(Landroid/content/Context;ZZZ)V

    .line 5832
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->y(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v9}, Lcom/msc/openprovider/b;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 5837
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->U(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->V(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/util/r;->t(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5839
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 5841
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "Show MybenefitNoti"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5845
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.mybenefitwebview_internal"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5858
    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    const/high16 v2, 0x8000000

    invoke-static {v1, v9, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 5860
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    const v4, 0x7f09004f

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    const v5, 0x7f090152

    invoke-virtual {v4, v5}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    .line 5866
    :cond_2
    return-void

    .line 5810
    :cond_3
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    sget-object v1, Lcom/msc/c/d;->c:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/ok;->n:Lcom/msc/a/b;

    invoke-virtual {v2}, Lcom/msc/a/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    sget-object v1, Lcom/msc/c/d;->d:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/ok;->n:Lcom/msc/a/b;

    invoke-virtual {v2}, Lcom/msc/a/b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    sget-object v1, Lcom/msc/c/d;->a:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/ok;->n:Lcom/msc/a/b;

    invoke-virtual {v2}, Lcom/msc/a/b;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    sget-object v1, Lcom/msc/c/d;->f:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/ok;->n:Lcom/msc/a/b;

    invoke-virtual {v2}, Lcom/msc/a/b;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    sget-object v1, Lcom/msc/c/d;->b:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->S(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/SignUpinfo;->d()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    sget-object v1, Lcom/msc/c/d;->h:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/common/util/i;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    sget-object v1, Lcom/msc/c/d;->i:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/osp/common/util/h;->a()Lcom/osp/common/util/h;

    iget-object v2, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/common/util/h;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    sget-object v1, Lcom/msc/c/d;->g:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/ok;->n:Lcom/msc/a/b;

    invoke-virtual {v2}, Lcom/msc/a/b;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    const-string v1, "j5p7ll8g33"

    invoke-static {v0, v1}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    const-string v1, "j5p7ll8g33"

    const-string v2, "5763D0052DC1462E13751F753384E9A9"

    iget-object v3, p0, Lcom/osp/app/signin/ok;->n:Lcom/msc/a/b;

    invoke-virtual {v3}, Lcom/msc/a/b;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/ok;->n:Lcom/msc/a/b;

    invoke-virtual {v4}, Lcom/msc/a/b;->f()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->ad(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/c/e;->e(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->W(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->S(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/SignUpinfo;->d()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/ok;->n:Lcom/msc/a/b;

    invoke-virtual {v3}, Lcom/msc/a/b;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->y(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v0, "j5p7ll8g33"

    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->y(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->ae(Lcom/osp/app/signin/TnCView;)Lcom/msc/a/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/d;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    :cond_5
    :goto_4
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->af(Lcom/osp/app/signin/TnCView;)V

    invoke-static {}, Lcom/osp/app/pushmarketing/k;->a()Lcom/osp/app/pushmarketing/k;

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/k;->c(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->S(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/TnCView;->d(Lcom/osp/app/signin/TnCView;Ljava/lang/String;)V

    goto/16 :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    :cond_6
    const-string v0, "tj9u972o46"

    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->y(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->y(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->ae(Lcom/osp/app/signin/TnCView;)Lcom/msc/a/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/msc/a/d;->g()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v3}, Lcom/osp/app/signin/TnCView;->ae(Lcom/osp/app/signin/TnCView;)Lcom/msc/a/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/msc/a/d;->j()J

    move-result-wide v3

    iget-object v5, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v5}, Lcom/osp/app/signin/TnCView;->ae(Lcom/osp/app/signin/TnCView;)Lcom/msc/a/d;

    move-result-object v5

    invoke-virtual {v5}, Lcom/msc/a/d;->h()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v6}, Lcom/osp/app/signin/TnCView;->ae(Lcom/osp/app/signin/TnCView;)Lcom/msc/a/d;

    move-result-object v6

    invoke-virtual {v6}, Lcom/msc/a/d;->k()J

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;J)V

    goto :goto_4

    :cond_7
    const-string v1, "SignInFail"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->c()V

    goto/16 :goto_1

    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    iget-object v0, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_f

    const-string v0, "USR_3117"

    iget-object v1, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    const-string v0, "PRT_3011"

    iget-object v1, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->ag(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/util/n;

    iget-object v0, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v3}, Lcom/osp/app/signin/TnCView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090060

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v9

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    invoke-virtual {v2}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v8}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "signinstae = error getCode()  : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    invoke-virtual {v2}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_a
    const-string v0, "MCC_NULL"

    iget-object v1, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "mcc null error during sign up"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/msc/c/e;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_b
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "mcc from db null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mcc save to db "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v2}, Lcom/osp/app/signin/TnCView;->getApplicationContext()Landroid/content/Context;

    iget-object v2, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    :goto_6
    invoke-virtual {p0, v9}, Lcom/osp/app/signin/ok;->a(Z)V

    goto/16 :goto_5

    :cond_d
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "finish sign up flow cause by mcc null error"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->finish()V

    goto :goto_6

    :cond_e
    invoke-virtual {p0, v9}, Lcom/osp/app/signin/ok;->a(Z)V

    goto/16 :goto_5

    :cond_f
    iget-object v0, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_10

    invoke-virtual {p0, v9}, Lcom/osp/app/signin/ok;->a(Z)V

    :cond_10
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->ac(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/jz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/jz;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->ac(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/jz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/jz;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v8}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    :cond_11
    move v0, v9

    .line 5831
    goto/16 :goto_2

    :cond_12
    move v0, v8

    goto/16 :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 7

    .prologue
    .line 6000
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 6002
    if-nez p1, :cond_1

    .line 6082
    :cond_0
    :goto_0
    return-void

    .line 6007
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 6008
    invoke-virtual {p1}, Lcom/msc/b/c;->b()Ljava/lang/String;

    .line 6009
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 6010
    invoke-virtual {p1}, Lcom/msc/b/c;->f()Ljava/lang/Exception;

    move-result-object v3

    .line 6012
    iget-wide v4, p0, Lcom/osp/app/signin/ok;->r:J

    cmp-long v4, v0, v4

    if-nez v4, :cond_2

    .line 6014
    if-eqz v3, :cond_0

    .line 6017
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/TnCView;->b(I)V

    goto :goto_0

    .line 6025
    :cond_2
    iget-wide v4, p0, Lcom/osp/app/signin/ok;->d:J

    cmp-long v4, v0, v4

    if-eqz v4, :cond_3

    iget-wide v4, p0, Lcom/osp/app/signin/ok;->f:J

    cmp-long v4, v0, v4

    if-nez v4, :cond_4

    .line 6028
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/ok;->p:Lcom/osp/app/signin/on;

    if-eqz v0, :cond_0

    .line 6030
    iget-object v0, p0, Lcom/osp/app/signin/ok;->p:Lcom/osp/app/signin/on;

    invoke-virtual {v0}, Lcom/osp/app/signin/on;->b()V

    goto :goto_0

    .line 6032
    :cond_4
    iget-wide v4, p0, Lcom/osp/app/signin/ok;->g:J

    cmp-long v4, v0, v4

    if-eqz v4, :cond_5

    iget-wide v4, p0, Lcom/osp/app/signin/ok;->f:J

    cmp-long v4, v0, v4

    if-nez v4, :cond_6

    .line 6035
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/ok;->q:Lcom/osp/app/signin/oo;

    if-eqz v0, :cond_0

    .line 6037
    iget-object v0, p0, Lcom/osp/app/signin/ok;->q:Lcom/osp/app/signin/oo;

    invoke-virtual {v0}, Lcom/osp/app/signin/oo;->e()V

    goto :goto_0

    .line 6039
    :cond_6
    iget-wide v4, p0, Lcom/osp/app/signin/ok;->h:J

    cmp-long v4, v0, v4

    if-nez v4, :cond_8

    .line 6041
    if-eqz v2, :cond_0

    .line 6043
    iget-object v0, p0, Lcom/osp/app/signin/ok;->q:Lcom/osp/app/signin/oo;

    if-eqz v0, :cond_0

    .line 6045
    iget-object v0, p0, Lcom/osp/app/signin/ok;->q:Lcom/osp/app/signin/oo;

    iget-object v1, v0, Lcom/osp/app/signin/oo;->a:Lcom/osp/app/signin/ok;

    iget-object v1, v1, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AUT_1302"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "AUT_1830"

    iget-object v2, v0, Lcom/osp/app/signin/oo;->a:Lcom/osp/app/signin/ok;

    iget-object v2, v2, Lcom/osp/app/signin/ok;->b:Lcom/msc/c/f;

    invoke-virtual {v2}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_7
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "TNC"

    const-string v2, "signinstae = athen"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v1, v0, Lcom/osp/app/signin/oo;->a:Lcom/osp/app/signin/ok;

    iget-object v2, v0, Lcom/osp/app/signin/oo;->a:Lcom/osp/app/signin/ok;

    iget-object v2, v2, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    iget-object v3, v0, Lcom/osp/app/signin/oo;->a:Lcom/osp/app/signin/ok;

    iget-object v3, v3, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v3}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/osp/app/signin/oo;->a:Lcom/osp/app/signin/ok;

    iget-object v4, v4, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-static {v4}, Lcom/osp/app/signin/TnCView;->S(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/osp/app/signin/SignUpinfo;->h()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    iget-object v6, v0, Lcom/osp/app/signin/oo;->a:Lcom/osp/app/signin/ok;

    invoke-static {v2, v3, v4, v5, v6}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/osp/app/signin/cn;Lcom/msc/b/h;)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/osp/app/signin/ok;->i:J

    iget-object v1, v0, Lcom/osp/app/signin/oo;->a:Lcom/osp/app/signin/ok;

    iget-object v2, v0, Lcom/osp/app/signin/oo;->a:Lcom/osp/app/signin/ok;

    iget-wide v2, v2, Lcom/osp/app/signin/ok;->i:J

    invoke-virtual {v1, v2, v3}, Lcom/osp/app/signin/ok;->b(J)V

    iget-object v1, v0, Lcom/osp/app/signin/oo;->a:Lcom/osp/app/signin/ok;

    iget-object v2, v0, Lcom/osp/app/signin/oo;->a:Lcom/osp/app/signin/ok;

    iget-wide v2, v2, Lcom/osp/app/signin/ok;->i:J

    const-string v4, "from_json"

    invoke-virtual {v1, v2, v3, v4}, Lcom/osp/app/signin/ok;->a(JLjava/lang/String;)V

    iget-object v0, v0, Lcom/osp/app/signin/oo;->a:Lcom/osp/app/signin/ok;

    iget-wide v0, v0, Lcom/osp/app/signin/ok;->i:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto/16 :goto_0

    .line 6048
    :cond_8
    iget-wide v4, p0, Lcom/osp/app/signin/ok;->i:J

    cmp-long v4, v0, v4

    if-nez v4, :cond_a

    .line 6050
    if-eqz v3, :cond_9

    .line 6052
    iget-object v0, p0, Lcom/osp/app/signin/ok;->q:Lcom/osp/app/signin/oo;

    if-eqz v0, :cond_0

    .line 6054
    iget-object v0, p0, Lcom/osp/app/signin/ok;->q:Lcom/osp/app/signin/oo;

    invoke-virtual {v0}, Lcom/osp/app/signin/oo;->d()V

    goto/16 :goto_0

    .line 6056
    :cond_9
    if-eqz v2, :cond_0

    .line 6058
    iget-object v0, p0, Lcom/osp/app/signin/ok;->q:Lcom/osp/app/signin/oo;

    if-eqz v0, :cond_0

    .line 6060
    iget-object v0, p0, Lcom/osp/app/signin/ok;->q:Lcom/osp/app/signin/oo;

    invoke-virtual {v0}, Lcom/osp/app/signin/oo;->c()V

    goto/16 :goto_0

    .line 6063
    :cond_a
    iget-wide v4, p0, Lcom/osp/app/signin/ok;->j:J

    cmp-long v4, v0, v4

    if-nez v4, :cond_c

    .line 6065
    if-eqz v3, :cond_b

    .line 6067
    iget-object v0, p0, Lcom/osp/app/signin/ok;->q:Lcom/osp/app/signin/oo;

    if-eqz v0, :cond_0

    .line 6069
    iget-object v0, p0, Lcom/osp/app/signin/ok;->q:Lcom/osp/app/signin/oo;

    invoke-virtual {v0}, Lcom/osp/app/signin/oo;->b()V

    goto/16 :goto_0

    .line 6071
    :cond_b
    if-eqz v2, :cond_0

    .line 6073
    iget-object v0, p0, Lcom/osp/app/signin/ok;->q:Lcom/osp/app/signin/oo;

    if-eqz v0, :cond_0

    .line 6075
    iget-object v0, p0, Lcom/osp/app/signin/ok;->q:Lcom/osp/app/signin/oo;

    invoke-virtual {v0}, Lcom/osp/app/signin/oo;->a()V

    goto/16 :goto_0

    .line 6078
    :cond_c
    iget-wide v2, p0, Lcom/osp/app/signin/ok;->s:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 6080
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "SignUpView - Name Check Fail."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected final d()V
    .locals 1

    .prologue
    .line 5794
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 5795
    iget-object v0, p0, Lcom/osp/app/signin/ok;->k:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->finish()V

    .line 5796
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 5744
    invoke-virtual {p0}, Lcom/osp/app/signin/ok;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 5744
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/ok;->a(Ljava/lang/Boolean;)V

    return-void
.end method
