.class final Lcom/osp/app/signin/bm;
.super Ljava/lang/Object;
.source "AccountinfoView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/AccountinfoView;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/AccountinfoView;)V
    .locals 0

    .prologue
    .line 2491
    iput-object p1, p0, Lcom/osp/app/signin/bm;->a:Lcom/osp/app/signin/AccountinfoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 2495
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AccountinfoView::initializeComponent mBtnChPassword.setOnClickListener onClick"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2496
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2497
    const-string v1, "OSP_01"

    iget-object v2, p0, Lcom/osp/app/signin/bm;->a:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountinfoView;->k(Lcom/osp/app/signin/AccountinfoView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2499
    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.PasswordChangeView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2505
    :goto_0
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/bm;->a:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountinfoView;->j(Lcom/osp/app/signin/AccountinfoView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/bm;->a:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountinfoView;->j(Lcom/osp/app/signin/AccountinfoView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2507
    const-string v1, "key_login_id"

    iget-object v2, p0, Lcom/osp/app/signin/bm;->a:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountinfoView;->j(Lcom/osp/app/signin/AccountinfoView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2514
    :cond_0
    new-instance v1, Lcom/osp/app/bigdatalog/c;

    iget-object v2, p0, Lcom/osp/app/signin/bm;->a:Lcom/osp/app/signin/AccountinfoView;

    invoke-virtual {v2}, Lcom/osp/app/signin/AccountinfoView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x5

    const/4 v4, 0x4

    iget-object v5, p0, Lcom/osp/app/signin/bm;->a:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v5}, Lcom/osp/app/signin/AccountinfoView;->l(Lcom/osp/app/signin/AccountinfoView;)Landroid/content/Intent;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 2516
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/bm;->a:Lcom/osp/app/signin/AccountinfoView;

    invoke-virtual {v3}, Lcom/osp/app/signin/AccountinfoView;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 2517
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/bm;->a:Lcom/osp/app/signin/AccountinfoView;

    invoke-virtual {v3}, Lcom/osp/app/signin/AccountinfoView;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;Lcom/osp/app/bigdatalog/c;)V

    .line 2519
    iget-object v1, p0, Lcom/osp/app/signin/bm;->a:Lcom/osp/app/signin/AccountinfoView;

    const/16 v2, 0xef

    invoke-virtual {v1, v0, v2}, Lcom/osp/app/signin/AccountinfoView;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2520
    return-void

    .line 2502
    :cond_1
    const-string v1, "com.msc.action.samsungaccount.changepassword"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method
