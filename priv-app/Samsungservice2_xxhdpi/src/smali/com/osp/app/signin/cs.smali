.class final Lcom/osp/app/signin/cs;
.super Lcom/msc/c/b;
.source "BackgroundModeService.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/BackgroundModeService;

.field private final d:Lcom/osp/app/util/d;

.field private e:Ljava/lang/String;

.field private f:J

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/BackgroundModeService;Lcom/osp/app/util/d;)V
    .locals 0

    .prologue
    .line 2413
    iput-object p1, p0, Lcom/osp/app/signin/cs;->c:Lcom/osp/app/signin/BackgroundModeService;

    .line 2414
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 2415
    invoke-virtual {p0}, Lcom/osp/app/signin/cs;->c()V

    .line 2416
    iput-object p2, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    .line 2419
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 2423
    iget-object v0, p0, Lcom/osp/app/signin/cs;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/cs;->g:Ljava/lang/String;

    .line 2427
    new-instance v0, Lcom/msc/a/f;

    invoke-direct {v0}, Lcom/msc/a/f;-><init>()V

    .line 2428
    iget-object v1, p0, Lcom/osp/app/signin/cs;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->d(Ljava/lang/String;)V

    .line 2429
    invoke-virtual {v0}, Lcom/msc/a/f;->e()V

    .line 2430
    iget-object v1, p0, Lcom/osp/app/signin/cs;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->h(Ljava/lang/String;)V

    .line 2433
    iget-object v1, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->f(Ljava/lang/String;)V

    .line 2434
    iget-object v1, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->g(Ljava/lang/String;)V

    .line 2435
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/cs;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2438
    iget-object v1, p0, Lcom/osp/app/signin/cs;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 2439
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2441
    const-string v2, "Y"

    invoke-virtual {v0, v2}, Lcom/msc/a/f;->b(Ljava/lang/String;)V

    .line 2442
    invoke-virtual {v0, v1}, Lcom/msc/a/f;->c(Ljava/lang/String;)V

    .line 2445
    :cond_0
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v1, p0, Lcom/osp/app/signin/cs;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1, v0, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/a/f;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/cs;->f:J

    iget-wide v0, p0, Lcom/osp/app/signin/cs;->f:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/cs;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/cs;->f:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/cs;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/cs;->f:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 2446
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 2633
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2635
    if-nez p1, :cond_1

    .line 2657
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2640
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 2641
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 2643
    iget-wide v4, p0, Lcom/osp/app/signin/cs;->f:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 2647
    :try_start_2
    iget-object v0, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v1, p0, Lcom/osp/app/signin/cs;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1, v2}, Lcom/msc/c/h;->b(Landroid/content/Context;Ljava/lang/String;)Lcom/msc/a/g;

    move-result-object v1

    iput-object v1, v0, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2652
    :goto_1
    :try_start_3
    invoke-virtual {p0}, Lcom/osp/app/signin/cs;->isCancelled()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 2633
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2648
    :catch_0
    move-exception v0

    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v8, -0x1

    const/4 v2, 0x0

    .line 2451
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 2458
    const-string v0, "Success"

    iget-object v1, p0, Lcom/osp/app/signin/cs;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2460
    iget-object v0, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    invoke-virtual {v0}, Lcom/msc/a/g;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2462
    iget-object v0, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-boolean v0, v0, Lcom/osp/app/util/d;->k:Z

    if-eqz v0, :cond_1

    .line 2464
    iget-object v0, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2466
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2467
    const-string v1, "bg_result"

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2470
    const-string v1, "request_code"

    iget-object v2, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget v2, v2, Lcom/osp/app/util/d;->q:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2472
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2473
    iget-object v1, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2476
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2477
    iget-object v1, p0, Lcom/osp/app/signin/cs;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v2, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-wide v2, v2, Lcom/osp/app/util/d;->r:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/osp/app/signin/BackgroundModeService;->a(Landroid/content/Intent;J)V

    .line 2478
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sendBroadcast to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2488
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/cs;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget v1, v1, Lcom/osp/app/util/d;->s:I

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BackgroundModeService;->stopSelf(I)V

    .line 2629
    :goto_1
    return-void

    .line 2481
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "cancel broadcast - no package name"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2486
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/cs;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cs;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    invoke-static {v0, v1, v2}, Lcom/osp/app/signin/BackgroundModeService;->a(Lcom/osp/app/signin/BackgroundModeService;Ljava/lang/String;Lcom/osp/app/util/d;)V

    goto :goto_0

    .line 2492
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-boolean v0, v0, Lcom/osp/app/util/d;->a:Z

    if-eqz v0, :cond_6

    .line 2494
    iget-object v0, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    invoke-virtual {v0}, Lcom/msc/a/g;->d()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    invoke-virtual {v0}, Lcom/msc/a/g;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2496
    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.Update_NewTerms"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2497
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2498
    const-string v1, "tncAccepted"

    iget-object v3, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v3, v3, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    invoke-virtual {v3}, Lcom/msc/a/g;->d()Z

    move-result v3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2499
    const-string v1, "privacyAccepted"

    iget-object v3, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v3, v3, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    invoke-virtual {v3}, Lcom/msc/a/g;->e()Z

    move-result v3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2500
    const-string v1, "key_tnc_update_mode"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2501
    const-string v1, "country_code_mcc"

    iget-object v3, p0, Lcom/osp/app/signin/cs;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2502
    iget-object v1, p0, Lcom/osp/app/signin/cs;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/BackgroundModeService;->startActivity(Landroid/content/Intent;)V

    .line 2503
    iget-object v0, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iput-object v2, v0, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    .line 2506
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/cs;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 2508
    iget-object v1, p0, Lcom/osp/app/signin/cs;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1}, Lcom/msc/c/e;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 2509
    iget-object v1, p0, Lcom/osp/app/signin/cs;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1}, Lcom/msc/c/c;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 2516
    :try_start_0
    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v1

    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/msc/c/a;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4, v0}, Lcom/msc/c/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2517
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v6

    const-string v7, "EmailID"

    invoke-virtual {v6, v7}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " encrypted."

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5

    move-object v4, v1

    .line 2535
    :goto_2
    :try_start_2
    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v0

    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/c/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/msc/c/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v1

    .line 2536
    :try_start_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v3, "external_b encrypted."

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    move-object v3, v1

    .line 2544
    :goto_3
    :try_start_4
    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v0

    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/c/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Lcom/msc/c/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    move-result-object v0

    .line 2545
    :try_start_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "BMS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v5

    const-string v6, "key_user_id"

    invoke-virtual {v5, v6}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " encrypted."

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 2552
    :goto_4
    iget-object v1, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 2554
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2555
    const-string v2, "bg_result"

    invoke-virtual {v1, v2, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2556
    const-string v2, "apps_id"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2562
    const-string v2, "apps_birthdate"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2563
    const-string v2, "apps_userid"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2564
    const-string v0, "signUpInfo"

    const-string v2, ""

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2567
    const-string v0, "request_code"

    iget-object v2, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget v2, v2, Lcom/osp/app/util/d;->q:I

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2569
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2570
    iget-object v0, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2572
    iget-object v0, p0, Lcom/osp/app/signin/cs;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v2, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-wide v2, v2, Lcom/osp/app/util/d;->r:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/osp/app/signin/BackgroundModeService;->a(Landroid/content/Intent;J)V

    .line 2573
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sendBroadcast to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2628
    :goto_5
    iget-object v0, p0, Lcom/osp/app/signin/cs;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget v1, v1, Lcom/osp/app/util/d;->s:I

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BackgroundModeService;->stopSelf(I)V

    goto/16 :goto_1

    .line 2518
    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_6
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2521
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v6

    const-string v7, "EmailID"

    invoke-virtual {v6, v7}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " encrypt failed."

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v1

    goto/16 :goto_2

    .line 2537
    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_7
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2540
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v3, "external_b encrypt failed."

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v1

    goto/16 :goto_3

    .line 2546
    :catch_2
    move-exception v0

    move-object v1, v0

    move-object v0, v2

    :goto_8
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 2549
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "BMS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v5

    const-string v6, "key_user_id"

    invoke-virtual {v5, v6}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " encrypt failed."

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 2576
    :cond_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "cancel broadcast - no package name"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 2580
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 2582
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2583
    const-string v1, "bg_result"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2586
    const-string v1, "request_code"

    iget-object v2, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget v2, v2, Lcom/osp/app/util/d;->q:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2588
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2589
    iget-object v1, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2591
    iget-object v1, p0, Lcom/osp/app/signin/cs;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v2, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-wide v2, v2, Lcom/osp/app/util/d;->r:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/osp/app/signin/BackgroundModeService;->a(Landroid/content/Intent;J)V

    .line 2592
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sendBroadcast to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 2595
    :cond_7
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "cancel broadcast - no package name"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 2600
    :cond_8
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "USR_3174"

    iget-object v1, p0, Lcom/osp/app/signin/cs;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2602
    iget-object v0, p0, Lcom/osp/app/signin/cs;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    invoke-static {v0, v1}, Lcom/osp/app/signin/BackgroundModeService;->a(Lcom/osp/app/signin/BackgroundModeService;Lcom/osp/app/util/d;)V

    goto/16 :goto_5

    .line 2603
    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-boolean v0, v0, Lcom/osp/app/util/d;->b:Z

    if-eqz v0, :cond_a

    .line 2605
    iget-object v0, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iput-boolean v3, v0, Lcom/osp/app/util/d;->b:Z

    .line 2606
    iget-object v0, p0, Lcom/osp/app/signin/cs;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    invoke-static {v0, v3, v1}, Lcom/osp/app/signin/BackgroundModeService;->a(Lcom/osp/app/signin/BackgroundModeService;ZLcom/osp/app/util/d;)V

    goto/16 :goto_5

    .line 2609
    :cond_a
    iget-object v0, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 2611
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2612
    const-string v1, "bg_result"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2615
    const-string v1, "request_code"

    iget-object v2, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget v2, v2, Lcom/osp/app/util/d;->q:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2617
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2618
    iget-object v1, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2620
    iget-object v1, p0, Lcom/osp/app/signin/cs;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v2, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-wide v2, v2, Lcom/osp/app/util/d;->r:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/osp/app/signin/BackgroundModeService;->a(Landroid/content/Intent;J)V

    .line 2621
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sendBroadcast to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/cs;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 2624
    :cond_b
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "cancel broadcast - no package name"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 2546
    :catch_3
    move-exception v1

    goto/16 :goto_8

    .line 2537
    :catch_4
    move-exception v0

    goto/16 :goto_7

    .line 2518
    :catch_5
    move-exception v0

    goto/16 :goto_6
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 2661
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 2663
    if-nez p1, :cond_1

    .line 2677
    :cond_0
    :goto_0
    return-void

    .line 2668
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 2670
    iget-wide v2, p0, Lcom/osp/app/signin/cs;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2672
    invoke-virtual {p0}, Lcom/osp/app/signin/cs;->isCancelled()Z

    goto :goto_0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2403
    invoke-virtual {p0}, Lcom/osp/app/signin/cs;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2403
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/cs;->a(Ljava/lang/Boolean;)V

    return-void
.end method
