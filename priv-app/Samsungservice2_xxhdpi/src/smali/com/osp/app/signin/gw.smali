.class final Lcom/osp/app/signin/gw;
.super Lcom/msc/c/b;
.source "ReactivationSendEmailCheckManager.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/gs;

.field private d:Ljava/lang/String;

.field private e:J

.field private f:J

.field private final g:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/gs;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 107
    iput-object p1, p0, Lcom/osp/app/signin/gw;->c:Lcom/osp/app/signin/gs;

    .line 108
    invoke-direct {p0, p2}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 110
    iput-object p2, p0, Lcom/osp/app/signin/gw;->g:Landroid/content/Context;

    .line 112
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RSECM"

    const-string v1, "GetMyCountryZoneTask"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 117
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/gw;->g:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/msc/c/g;->b(Landroid/content/Context;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/gw;->e:J

    iget-wide v0, p0, Lcom/osp/app/signin/gw;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/gw;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/gw;->e:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 118
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 149
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    if-nez p1, :cond_1

    .line 199
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 156
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 157
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 159
    iget-wide v4, p0, Lcom/osp/app/signin/gw;->e:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_5

    .line 163
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->w(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 165
    if-eqz v0, :cond_2

    .line 167
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v1, p0, Lcom/osp/app/signin/gw;->g:Landroid/content/Context;

    invoke-static {v1}, Lcom/osp/common/util/i;->o(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 168
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/app/signin/gw;->g:Landroid/content/Context;

    invoke-static {v1}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 169
    if-eqz v2, :cond_3

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 171
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "SendReactivationEmailTask countryCodeFromNetworkMcc = ["

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 172
    iput-object v1, p0, Lcom/osp/app/signin/gw;->d:Ljava/lang/String;

    .line 180
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/osp/app/signin/gw;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/gw;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 182
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/gw;->g:Landroid/content/Context;

    iget-object v1, p0, Lcom/osp/app/signin/gw;->d:Ljava/lang/String;

    invoke-static {v0, v1, p0}, Lcom/msc/c/g;->h(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/gw;->f:J

    iget-wide v0, p0, Lcom/osp/app/signin/gw;->f:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/gw;->b(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/gw;->f:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 188
    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 149
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 175
    :cond_3
    :try_start_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SendReactivationEmailTask countryCode = ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 176
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v1, p0, Lcom/osp/app/signin/gw;->g:Landroid/content/Context;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/common/util/i;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/gw;->d:Ljava/lang/String;

    goto :goto_1

    .line 185
    :cond_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RSECM"

    const-string v1, "GeoIP is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 192
    :cond_5
    :try_start_5
    iget-wide v4, p0, Lcom/osp/app/signin/gw;->f:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 194
    const-string v0, "EML"

    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 196
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/gw;->b:Lcom/msc/c/f;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 128
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 129
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RSECM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SendReactivationEmailTask GeoIPMcc = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/gw;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/osp/app/signin/gw;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_1

    .line 133
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RSECM"

    const-string v1, "ReactivationEmailTask Error!"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-virtual {p0, v5}, Lcom/osp/app/signin/gw;->a(Z)V

    .line 137
    iget-object v0, p0, Lcom/osp/app/signin/gw;->c:Lcom/osp/app/signin/gs;

    invoke-static {v0}, Lcom/osp/app/signin/gs;->a(Lcom/osp/app/signin/gs;)Lcom/osp/app/signin/gu;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/osp/app/signin/gw;->c:Lcom/osp/app/signin/gs;

    invoke-static {v0}, Lcom/osp/app/signin/gs;->a(Lcom/osp/app/signin/gs;)Lcom/osp/app/signin/gu;

    move-result-object v0

    invoke-interface {v0}, Lcom/osp/app/signin/gu;->a()V

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 143
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/gw;->c:Lcom/osp/app/signin/gs;

    iget-object v1, p0, Lcom/osp/app/signin/gw;->g:Landroid/content/Context;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "RSECM"

    const-string v3, "showReactivationLockSigninFailPopUp"

    const-string v4, "START"

    invoke-static {v2, v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f090144

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f09013b

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f090064

    new-instance v3, Lcom/osp/app/signin/gt;

    invoke-direct {v3, v0}, Lcom/osp/app/signin/gt;-><init>(Lcom/osp/app/signin/gs;)V

    invoke-virtual {v2, v1, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 203
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 205
    if-nez p1, :cond_1

    .line 219
    :cond_0
    :goto_0
    return-void

    .line 210
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 212
    iget-wide v2, p0, Lcom/osp/app/signin/gw;->e:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 215
    iget-wide v0, p0, Lcom/osp/app/signin/gw;->f:J

    goto :goto_0
.end method

.method protected final d()V
    .locals 0

    .prologue
    .line 123
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 124
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/osp/app/signin/gw;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 82
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/gw;->a(Ljava/lang/Boolean;)V

    return-void
.end method
