.class public Lcom/osp/app/signin/ej;
.super Landroid/widget/BaseAdapter;
.source "CountryListAdapter.java"

# interfaces
.implements Landroid/widget/SectionIndexer;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/HashMap;

.field private final c:Landroid/app/Activity;

.field private d:[Ljava/lang/String;

.field private final e:[Ljava/lang/String;

.field private final f:Z

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Lcom/osp/app/signin/ek;

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/osp/app/signin/ej;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/osp/app/signin/ej;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;[Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 84
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 40
    iput-boolean v4, p0, Lcom/osp/app/signin/ej;->g:Z

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/ej;->j:Lcom/osp/app/signin/ek;

    .line 48
    iput-boolean v5, p0, Lcom/osp/app/signin/ej;->k:Z

    .line 85
    sget-object v0, Lcom/osp/app/signin/ej;->a:Ljava/lang/String;

    sget-object v1, Lcom/osp/app/signin/ej;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    iput-object p1, p0, Lcom/osp/app/signin/ej;->c:Landroid/app/Activity;

    .line 87
    iput-object p2, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    .line 88
    iput-boolean p4, p0, Lcom/osp/app/signin/ej;->f:Z

    .line 89
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/ej;->b:Ljava/util/HashMap;

    .line 91
    invoke-direct {p0}, Lcom/osp/app/signin/ej;->b()V

    .line 93
    iget-object v0, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    array-length v0, v0

    .line 94
    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 96
    iget-object v1, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 97
    iget-object v2, p0, Lcom/osp/app/signin/ej;->b:Ljava/util/HashMap;

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/ej;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 101
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 102
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 104
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 106
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 107
    if-eqz v0, :cond_1

    .line 109
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 112
    :cond_2
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 113
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/osp/app/signin/ej;->e:[Ljava/lang/String;

    .line 114
    iget-object v0, p0, Lcom/osp/app/signin/ej;->e:[Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 116
    iput-object p3, p0, Lcom/osp/app/signin/ej;->h:Ljava/lang/String;

    .line 118
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;[Ljava/lang/String;Ljava/lang/String;ZB)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 120
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 40
    iput-boolean v5, p0, Lcom/osp/app/signin/ej;->g:Z

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/ej;->j:Lcom/osp/app/signin/ek;

    .line 48
    iput-boolean v4, p0, Lcom/osp/app/signin/ej;->k:Z

    .line 121
    sget-object v0, Lcom/osp/app/signin/ej;->a:Ljava/lang/String;

    sget-object v1, Lcom/osp/app/signin/ej;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    iput-object p1, p0, Lcom/osp/app/signin/ej;->c:Landroid/app/Activity;

    .line 123
    iput-object p2, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    .line 124
    iput-boolean p4, p0, Lcom/osp/app/signin/ej;->f:Z

    .line 125
    iput-boolean v4, p0, Lcom/osp/app/signin/ej;->g:Z

    .line 127
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/ej;->b:Ljava/util/HashMap;

    .line 129
    invoke-direct {p0}, Lcom/osp/app/signin/ej;->b()V

    .line 131
    iget-object v0, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    array-length v0, v0

    .line 132
    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 134
    iget-object v1, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 135
    iget-object v2, p0, Lcom/osp/app/signin/ej;->b:Ljava/util/HashMap;

    invoke-virtual {v1, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/ej;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 139
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 140
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 142
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 144
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 145
    if-eqz v0, :cond_1

    .line 147
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 150
    :cond_2
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 151
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/osp/app/signin/ej;->e:[Ljava/lang/String;

    .line 152
    iget-object v0, p0, Lcom/osp/app/signin/ej;->e:[Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 154
    iput-object p3, p0, Lcom/osp/app/signin/ej;->i:Ljava/lang/String;

    .line 156
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;[Ljava/lang/String;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 50
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 40
    iput-boolean v4, p0, Lcom/osp/app/signin/ej;->g:Z

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/ej;->j:Lcom/osp/app/signin/ek;

    .line 48
    iput-boolean v5, p0, Lcom/osp/app/signin/ej;->k:Z

    .line 51
    sget-object v0, Lcom/osp/app/signin/ej;->a:Ljava/lang/String;

    sget-object v1, Lcom/osp/app/signin/ej;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    iput-object p1, p0, Lcom/osp/app/signin/ej;->c:Landroid/app/Activity;

    .line 53
    iput-object p2, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    .line 54
    iput-boolean p3, p0, Lcom/osp/app/signin/ej;->f:Z

    .line 55
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/ej;->b:Ljava/util/HashMap;

    .line 57
    invoke-direct {p0}, Lcom/osp/app/signin/ej;->b()V

    .line 59
    iget-object v0, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    array-length v0, v0

    .line 60
    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 62
    iget-object v1, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 63
    iget-object v2, p0, Lcom/osp/app/signin/ej;->b:Ljava/util/HashMap;

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/ej;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 67
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 68
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 70
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 72
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 73
    if-eqz v0, :cond_1

    .line 75
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 78
    :cond_2
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 79
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/osp/app/signin/ej;->e:[Ljava/lang/String;

    .line 80
    iget-object v0, p0, Lcom/osp/app/signin/ej;->e:[Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 82
    return-void
.end method

.method private static a(Lcom/osp/app/signin/ek;Landroid/content/res/Resources;)V
    .locals 2

    .prologue
    .line 355
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 357
    iget-object v0, p0, Lcom/osp/app/signin/ek;->c:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 359
    :cond_0
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 162
    iget-object v0, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_0

    .line 164
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    .line 165
    iget-object v0, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    iget-object v1, p0, Lcom/osp/app/signin/ej;->c:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09016c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    .line 166
    iput-boolean v3, p0, Lcom/osp/app/signin/ej;->k:Z

    .line 172
    :goto_0
    return-void

    .line 170
    :cond_0
    iput-boolean v1, p0, Lcom/osp/app/signin/ej;->k:Z

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 450
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/ej;->g:Z

    .line 451
    iget-object v0, p0, Lcom/osp/app/signin/ej;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 453
    iget-object v1, p0, Lcom/osp/app/signin/ej;->j:Lcom/osp/app/signin/ek;

    invoke-static {v1, v0}, Lcom/osp/app/signin/ej;->a(Lcom/osp/app/signin/ek;Landroid/content/res/Resources;)V

    .line 455
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 380
    int-to-long v0, p1

    return-wide v0
.end method

.method public getPositionForSection(I)I
    .locals 2

    .prologue
    .line 385
    iget-object v0, p0, Lcom/osp/app/signin/ej;->e:[Ljava/lang/String;

    array-length v0, v0

    if-gt v0, p1, :cond_0

    .line 387
    iget-object v0, p0, Lcom/osp/app/signin/ej;->e:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    .line 391
    :goto_0
    return v0

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/ej;->e:[Ljava/lang/String;

    aget-object v0, v0, p1

    .line 391
    iget-object v1, p0, Lcom/osp/app/signin/ej;->b:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public getSectionForPosition(I)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 397
    .line 398
    iget-object v0, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    array-length v0, v0

    if-le v0, p1, :cond_2

    .line 400
    iget-object v0, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 402
    iget-object v0, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    aget-object v0, v0, p1

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :goto_0
    move v1, v2

    .line 408
    :goto_1
    iget-object v3, p0, Lcom/osp/app/signin/ej;->e:[Ljava/lang/String;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 411
    iget-object v3, p0, Lcom/osp/app/signin/ej;->e:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 418
    :goto_2
    return v1

    .line 405
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    aget-object v0, v0, p1

    goto :goto_0

    .line 408
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v1, v2

    .line 418
    goto :goto_2
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/osp/app/signin/ej;->e:[Ljava/lang/String;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/16 v7, 0x21

    const/4 v6, 0x1

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 188
    if-nez p2, :cond_5

    .line 191
    iget-object v0, p0, Lcom/osp/app/signin/ej;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 192
    const v1, 0x7f030020

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 193
    new-instance v1, Lcom/osp/app/signin/ek;

    invoke-direct {v1, v4}, Lcom/osp/app/signin/ek;-><init>(B)V

    .line 195
    const v0, 0x7f0c00e7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/osp/app/signin/ek;->a:Landroid/widget/TextView;

    .line 196
    const v0, 0x7f0c00ea

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v1, Lcom/osp/app/signin/ek;->d:Landroid/widget/LinearLayout;

    .line 197
    const v0, 0x7f0c00eb

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/osp/app/signin/ek;->b:Landroid/widget/TextView;

    .line 198
    const v0, 0x7f0c00e6

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v1, Lcom/osp/app/signin/ek;->c:Landroid/widget/LinearLayout;

    .line 199
    const v0, 0x7f0c00e9

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/osp/app/signin/ek;->e:Landroid/view/View;

    .line 200
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    .line 206
    :goto_0
    iget-object v1, p0, Lcom/osp/app/signin/ej;->c:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 208
    invoke-static {v0, v1}, Lcom/osp/app/signin/ej;->a(Lcom/osp/app/signin/ek;Landroid/content/res/Resources;)V

    .line 228
    iget-object v2, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    array-length v2, v2

    if-ge p1, v2, :cond_0

    .line 231
    iget-object v2, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    aget-object v2, v2, p1

    .line 232
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v3, v6, :cond_6

    .line 234
    iget-object v3, v0, Lcom/osp/app/signin/ek;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 235
    iget-object v3, v0, Lcom/osp/app/signin/ek;->e:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 236
    iget-object v3, v0, Lcom/osp/app/signin/ek;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 237
    iget-object v3, v0, Lcom/osp/app/signin/ek;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 238
    iget-object v2, v0, Lcom/osp/app/signin/ek;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 239
    iget-object v2, v0, Lcom/osp/app/signin/ek;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 240
    iget-object v2, v0, Lcom/osp/app/signin/ek;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 251
    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/osp/app/signin/ej;->h:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 253
    iget-object v2, p0, Lcom/osp/app/signin/ej;->h:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/osp/app/signin/ej;->g:Z

    if-nez v2, :cond_3

    .line 256
    iget-object v2, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    aget-object v2, v2, p1

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 257
    iget-object v3, p0, Lcom/osp/app/signin/ej;->h:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    .line 259
    add-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v3

    .line 260
    iget-object v4, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    aget-object v4, v4, p1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-le v2, v4, :cond_1

    .line 262
    iget-object v2, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 265
    :cond_1
    new-instance v2, Landroid/text/SpannableStringBuilder;

    iget-object v4, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    aget-object v4, v4, p1

    invoke-direct {v2, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 267
    iget-object v4, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    aget-object v4, v4, p1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v4, v3, :cond_2

    .line 278
    iget-object v3, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    aget-object v3, v3, p1

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/ej;->h:Ljava/lang/String;

    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 280
    iget-object v3, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    aget-object v3, v3, p1

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/ej;->h:Ljava/lang/String;

    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 281
    iget-boolean v4, p0, Lcom/osp/app/signin/ej;->f:Z

    if-eqz v4, :cond_7

    .line 283
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    const v5, 0x7f070016

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v4, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iget-object v1, p0, Lcom/osp/app/signin/ej;->h:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v3

    invoke-virtual {v2, v4, v3, v1, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 308
    :cond_2
    :goto_2
    iget-object v1, v0, Lcom/osp/app/signin/ek;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 314
    :cond_3
    iget-boolean v1, p0, Lcom/osp/app/signin/ej;->g:Z

    if-ne v1, v6, :cond_4

    .line 316
    iget-object v1, p0, Lcom/osp/app/signin/ej;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 321
    iput-object v0, p0, Lcom/osp/app/signin/ej;->j:Lcom/osp/app/signin/ek;

    .line 323
    iget-boolean v1, p0, Lcom/osp/app/signin/ej;->f:Z

    if-eqz v1, :cond_8

    .line 325
    iget-object v0, v0, Lcom/osp/app/signin/ek;->c:Landroid/widget/LinearLayout;

    const v1, 0x7f020138

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 340
    :cond_4
    :goto_3
    return-object p2

    .line 204
    :cond_5
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/ek;

    goto/16 :goto_0

    .line 244
    :cond_6
    iget-object v3, v0, Lcom/osp/app/signin/ek;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 245
    iget-object v3, v0, Lcom/osp/app/signin/ek;->e:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 246
    iget-object v3, v0, Lcom/osp/app/signin/ek;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 247
    iget-object v3, v0, Lcom/osp/app/signin/ek;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 287
    :cond_7
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    const v5, 0x7f070017

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v4, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iget-object v1, p0, Lcom/osp/app/signin/ej;->h:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v3

    invoke-virtual {v2, v4, v3, v1, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_2

    .line 328
    :cond_8
    iget-object v0, v0, Lcom/osp/app/signin/ek;->c:Landroid/widget/LinearLayout;

    const v1, 0x7f020139

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_3
.end method

.method public isEnabled(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 429
    iget-boolean v1, p0, Lcom/osp/app/signin/ej;->k:Z

    if-nez v1, :cond_1

    .line 442
    :cond_0
    :goto_0
    return v0

    .line 434
    :cond_1
    iget-object v1, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    array-length v1, v1

    if-ge p1, v1, :cond_2

    .line 436
    iget-object v1, p0, Lcom/osp/app/signin/ej;->d:[Ljava/lang/String;

    aget-object v1, v1, p1

    .line 437
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 442
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/BaseAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method
