.class public Lcom/osp/app/signin/UserValidateCheck;
.super Lcom/osp/app/util/BaseActivity;
.source "UserValidateCheck.java"


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:Landroid/widget/TextView;

.field private E:Landroid/widget/EditText;

.field private F:Landroid/widget/CheckBox;

.field private G:Landroid/widget/TextView;

.field private H:Landroid/content/Context;

.field private I:Lcom/osp/app/signin/pn;

.field private J:Lcom/osp/app/signin/po;

.field private K:Landroid/content/SharedPreferences;

.field private L:Lcom/osp/app/signin/kd;

.field private M:Ljava/lang/String;

.field private N:Ljava/lang/String;

.field private O:Ljava/lang/String;

.field private P:Z

.field private Q:Ljava/lang/String;

.field private R:Ljava/lang/String;

.field private S:Z

.field private T:Ljava/util/HashMap;

.field private U:Ljava/lang/String;

.field private V:Ljava/lang/String;

.field private W:J

.field private X:Z

.field private Y:I

.field private Z:Z

.field final a:Landroid/view/View$OnClickListener;

.field private aa:Z

.field private ab:Lcom/samsung/android/sdk/pass/Spass;

.field private ac:Lcom/samsung/android/sdk/pass/SpassFingerprint;

.field private ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

.field private ae:Landroid/app/AlertDialog;

.field private af:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

.field final b:Landroid/view/View$OnClickListener;

.field private c:I

.field private d:Lcom/osp/app/signin/pm;

.field private e:Lcom/msc/a/g;

.field private f:I

.field private y:Landroid/content/Intent;

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 90
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 118
    iput v2, p0, Lcom/osp/app/signin/UserValidateCheck;->c:I

    .line 124
    iput v2, p0, Lcom/osp/app/signin/UserValidateCheck;->f:I

    .line 129
    iput-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    .line 134
    iput-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->z:Ljava/lang/String;

    .line 139
    iput-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->A:Ljava/lang/String;

    .line 144
    iput-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->B:Ljava/lang/String;

    .line 149
    iput-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->C:Ljava/lang/String;

    .line 209
    iput-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->L:Lcom/osp/app/signin/kd;

    .line 214
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->M:Ljava/lang/String;

    .line 219
    iput-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->N:Ljava/lang/String;

    .line 224
    iput-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->O:Ljava/lang/String;

    .line 229
    iput-boolean v2, p0, Lcom/osp/app/signin/UserValidateCheck;->P:Z

    .line 234
    iput-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->Q:Ljava/lang/String;

    .line 239
    iput-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->R:Ljava/lang/String;

    .line 249
    iput-boolean v2, p0, Lcom/osp/app/signin/UserValidateCheck;->S:Z

    .line 277
    iput-boolean v2, p0, Lcom/osp/app/signin/UserValidateCheck;->X:Z

    .line 297
    iput-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->ab:Lcom/samsung/android/sdk/pass/Spass;

    .line 298
    iput-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->ac:Lcom/samsung/android/sdk/pass/SpassFingerprint;

    .line 303
    new-instance v0, Lcom/osp/app/signin/ow;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ow;-><init>(Lcom/osp/app/signin/UserValidateCheck;)V

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->a:Landroid/view/View$OnClickListener;

    .line 319
    new-instance v0, Lcom/osp/app/signin/pe;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/pe;-><init>(Lcom/osp/app/signin/UserValidateCheck;)V

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->b:Landroid/view/View$OnClickListener;

    .line 3395
    new-instance v0, Lcom/osp/app/signin/UserValidateCheck$13;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/UserValidateCheck$13;-><init>(Lcom/osp/app/signin/UserValidateCheck;)V

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->af:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

    .line 3514
    return-void
.end method

.method static synthetic A(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->R:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->K:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic C(Lcom/osp/app/signin/UserValidateCheck;)Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/osp/app/signin/UserValidateCheck;->Z:Z

    return v0
.end method

.method static synthetic D(Lcom/osp/app/signin/UserValidateCheck;)Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->af:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

    return-object v0
.end method

.method static synthetic E(Lcom/osp/app/signin/UserValidateCheck;)Lcom/samsung/android/sdk/pass/SpassFingerprint;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ac:Lcom/samsung/android/sdk/pass/SpassFingerprint;

    return-object v0
.end method

.method static synthetic F(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->O:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic G(Lcom/osp/app/signin/UserValidateCheck;)J
    .locals 2

    .prologue
    .line 90
    iget-wide v0, p0, Lcom/osp/app/signin/UserValidateCheck;->W:J

    return-wide v0
.end method

.method static synthetic H(Lcom/osp/app/signin/UserValidateCheck;)Lcom/msc/a/g;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->e:Lcom/msc/a/g;

    return-object v0
.end method

.method static synthetic a(Lcom/osp/app/signin/UserValidateCheck;Landroid/content/SharedPreferences;)Landroid/content/SharedPreferences;
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/osp/app/signin/UserValidateCheck;->K:Landroid/content/SharedPreferences;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/UserValidateCheck;Lcom/msc/a/g;)Lcom/msc/a/g;
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/osp/app/signin/UserValidateCheck;->e:Lcom/msc/a/g;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/UserValidateCheck;Lcom/osp/app/signin/kd;)Lcom/osp/app/signin/kd;
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/osp/app/signin/UserValidateCheck;->L:Lcom/osp/app/signin/kd;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/UserValidateCheck;Lcom/osp/app/signin/pn;)Lcom/osp/app/signin/pn;
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/osp/app/signin/UserValidateCheck;->I:Lcom/osp/app/signin/pn;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/UserValidateCheck;Lcom/osp/app/signin/po;)Lcom/osp/app/signin/po;
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/osp/app/signin/UserValidateCheck;->J:Lcom/osp/app/signin/po;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/UserValidateCheck;Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/osp/app/signin/UserValidateCheck;->T:Ljava/util/HashMap;

    return-object p1
.end method

.method private a(I)V
    .locals 4

    .prologue
    const v3, 0x7f08011e

    .line 673
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 675
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08011f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 676
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 677
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 679
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 682
    :cond_0
    const v2, 0x7f0c016e

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/UserValidateCheck;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 683
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-static {v2, v0, v1, p1}, Lcom/osp/app/util/q;->b(Landroid/view/View;III)V

    .line 685
    invoke-static {}, Lcom/osp/app/util/r;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 687
    const v0, 0x7f0c011f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 690
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/UserValidateCheck;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/osp/app/signin/UserValidateCheck;->c()V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/UserValidateCheck;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 90
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ae:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ae:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ae:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_0
    invoke-static {p1}, Lcom/osp/app/util/ad;->i(Landroid/content/Context;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f09004f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0900c0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090042

    new-instance v2, Lcom/osp/app/signin/pb;

    invoke-direct {v2, p0, p1}, Lcom/osp/app/signin/pb;-><init>(Lcom/osp/app/signin/UserValidateCheck;Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090161

    new-instance v2, Lcom/osp/app/signin/pa;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/pa;-><init>(Lcom/osp/app/signin/UserValidateCheck;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/UserValidateCheck;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 90
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.web_with_close_button"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "ServerUrl"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/UserValidateCheck;Z)V
    .locals 9

    .prologue
    const v8, 0x7f090024

    const/16 v4, 0xda

    const/4 v7, 0x1

    const/4 v6, -0x1

    .line 90
    const-string v0, "CHANGE_SIM_ALERT"

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->M:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Inputpassword Certification"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-virtual {p0, v6}, Lcom/osp/app/signin/UserValidateCheck;->b(I)V

    :cond_0
    :goto_0
    iget v0, p0, Lcom/osp/app/signin/UserValidateCheck;->c:I

    if-ne v0, v7, :cond_6

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "start account info"

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/r;->u()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.msc.action.samsungaccount.myinfowebview_internal"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->startActivity(Landroid/content/Intent;)V

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    :cond_2
    :goto_2
    return-void

    :cond_3
    const-string v0, "REMOTE_CONTROLS"

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->M:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Inputpassword Certification"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-virtual {p0, v6}, Lcom/osp/app/signin/UserValidateCheck;->b(I)V

    goto :goto_0

    :cond_4
    const-string v0, "ALERT_MESSAGE_RECIPIENTS"

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->M:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Inputpassword Certification"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-virtual {p0, v6}, Lcom/osp/app/signin/UserValidateCheck;->b(I)V

    goto :goto_0

    :cond_5
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountinfoView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->z:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->A:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account_mode"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->B:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "service_name"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->O:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->N:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/UserValidateCheck;->W:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "is_authorized_internal_action_for_accountinfo"

    const-string v2, "TIME_PATTERN"

    invoke-static {v2}, Lcom/osp/app/util/ad;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->B:Ljava/lang/String;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->B:Ljava/lang/String;

    const-string v1, "ACCOUNTINFO_MODIFY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {}, Lcom/osp/app/util/r;->u()Z

    move-result v0

    if-eqz v0, :cond_7

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.msc.action.samsungaccount.myinfowebview_internal"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0, v4}, Lcom/osp/app/signin/UserValidateCheck;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_2

    :cond_7
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountinfoView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->z:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->A:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account_mode"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->B:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "service_name"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->O:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->N:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/UserValidateCheck;->W:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "is_authorized_internal_action_for_accountinfo"

    const-string v2, "TIME_PATTERN"

    invoke-static {v2}, Lcom/osp/app/util/ad;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0, v4}, Lcom/osp/app/signin/UserValidateCheck;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_2

    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->B:Ljava/lang/String;

    const-string v1, "CREDITCARD_MODIFY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.SignUpView_Payment"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->B:Ljava/lang/String;

    const-string v1, "ACCOUNT_VERIFY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "netflix"

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->U:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->T:Ljava/util/HashMap;

    if-eqz v0, :cond_f

    const-string v2, "issuetime"

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->T:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->T:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    :cond_a
    const-string v2, "confirmed"

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->T:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->T:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_b
    const-string v2, "expiretime"

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->T:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->T:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    :cond_c
    const-string v2, "userid"

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->T:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->T:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_d
    const-string v2, "signerid"

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->T:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->T:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_e
    const-string v2, "signature"

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->T:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->T:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_f
    invoke-virtual {p0, v6, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(ILandroid/content/Intent;)V

    :goto_3
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->L:Lcom/osp/app/signin/kd;

    if-eqz v0, :cond_10

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "m_FieldInfoGetFieldCount is "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->L:Lcom/osp/app/signin/kd;

    invoke-virtual {v1}, Lcom/osp/app/signin/kd;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->L:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->b()I

    move-result v0

    if-lez v0, :cond_10

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountinfoView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->z:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->A:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account_mode"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->B:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "service_name"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->O:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->N:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "direct_modify"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/UserValidateCheck;->W:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "is_authorized_internal_action_for_accountinfo"

    const-string v2, "TIME_PATTERN"

    invoke-static {v2}, Lcom/osp/app/util/ad;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->startActivity(Landroid/content/Intent;)V

    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->H:Landroid/content/Context;

    invoke-virtual {p0, v8}, Lcom/osp/app/signin/UserValidateCheck;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_10
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto/16 :goto_2

    :cond_11
    invoke-static {}, Lcom/osp/app/util/p;->a()Lcom/osp/app/util/p;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->H:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/osp/app/util/p;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_15

    const-string v0, "setting_finger"

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->U:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    if-eqz p1, :cond_12

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_15

    :cond_12
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "UVC"

    const-string v1, "success setting finger confirmed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/p;->a()Lcom/osp/app/util/p;

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->V:Ljava/lang/String;

    invoke-static {v0}, Lcom/osp/app/util/p;->a(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v6, :cond_13

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "UVC"

    const-string v2, "Wrong finger setting value"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    invoke-virtual {p0, v7, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    :cond_13
    invoke-static {}, Lcom/osp/app/util/p;->a()Lcom/osp/app/util/p;

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->H:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/osp/app/util/p;->a(Landroid/content/Context;I)Z

    move-result v0

    invoke-static {}, Lcom/osp/app/util/p;->a()Lcom/osp/app/util/p;

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->H:Landroid/content/Context;

    invoke-static {v1, v7}, Lcom/osp/app/util/p;->b(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v0, :cond_14

    if-nez v1, :cond_15

    :cond_14
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    invoke-virtual {p0, v7, v0}, Lcom/osp/app/signin/UserValidateCheck;->a(ILandroid/content/Intent;)V

    goto/16 :goto_3

    :cond_15
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    invoke-virtual {p0, v6, v0}, Lcom/osp/app/signin/UserValidateCheck;->a(ILandroid/content/Intent;)V

    goto/16 :goto_3

    :cond_16
    iget-boolean v0, p0, Lcom/osp/app/signin/UserValidateCheck;->P:Z

    if-eqz v0, :cond_17

    const-string v0, "BG_mode"

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->M:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "bg_result"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "authcode"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->Q:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "ServerUrl"

    invoke-static {p0}, Lcom/msc/c/c;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->C:Ljava/lang/String;

    if-eqz v1, :cond_17

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->C:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->z:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-wide v2, p0, Lcom/osp/app/signin/UserValidateCheck;->W:J

    invoke-virtual {p0, v0, v2, v3}, Lcom/osp/app/signin/UserValidateCheck;->a(Landroid/content/Intent;J)V

    :cond_17
    :goto_4
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    invoke-virtual {p0, v6, v0}, Lcom/osp/app/signin/UserValidateCheck;->a(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->L:Lcom/osp/app/signin/kd;

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "m_FieldInfoGetFieldCount is "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->L:Lcom/osp/app/signin/kd;

    invoke-virtual {v1}, Lcom/osp/app/signin/kd;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->L:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->b()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountinfoView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->z:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->A:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account_mode"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->B:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "service_name"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->O:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->N:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "direct_modify"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/UserValidateCheck;->W:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "is_authorized_internal_action_for_accountinfo"

    const-string v2, "TIME_PATTERN"

    invoke-static {v2}, Lcom/osp/app/util/ad;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->startActivity(Landroid/content/Intent;)V

    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->H:Landroid/content/Context;

    invoke-virtual {p0, v8}, Lcom/osp/app/signin/UserValidateCheck;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    :cond_18
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    const-string v1, "authcode"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->Q:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    const-string v1, "ServerUrl"

    invoke-static {p0}, Lcom/msc/c/c;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_4
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1231
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.resignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1232
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->z:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1233
    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->A:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1234
    const-string v1, "account_mode"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->B:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1235
    const-string v1, "OSP_VER"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->N:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1236
    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/UserValidateCheck;->W:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1238
    if-eqz p1, :cond_0

    .line 1240
    const-string v1, "email_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1249
    :cond_0
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->startActivity(Landroid/content/Intent;)V

    .line 1251
    return-void
.end method

.method static synthetic b(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->N:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/osp/app/signin/UserValidateCheck;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 90
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ae:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ae:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ae:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_0
    invoke-static {p1}, Lcom/osp/app/util/ad;->i(Landroid/content/Context;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f09004f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090196

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090042

    new-instance v2, Lcom/osp/app/signin/pd;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/pd;-><init>(Lcom/osp/app/signin/UserValidateCheck;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090161

    new-instance v2, Lcom/osp/app/signin/pc;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/pc;-><init>(Lcom/osp/app/signin/UserValidateCheck;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/osp/app/signin/UserValidateCheck;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/osp/app/signin/UserValidateCheck;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic c(Lcom/osp/app/signin/UserValidateCheck;)Lcom/osp/app/signin/pn;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->I:Lcom/osp/app/signin/pn;

    return-object v0
.end method

.method static synthetic c(Lcom/osp/app/signin/UserValidateCheck;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/osp/app/signin/UserValidateCheck;->R:Ljava/lang/String;

    return-object p1
.end method

.method private c()V
    .locals 3

    .prologue
    .line 1780
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1781
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->E:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    .line 1784
    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->E:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1786
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/osp/app/signin/UserValidateCheck;)Lcom/osp/app/signin/po;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->J:Lcom/osp/app/signin/po;

    return-object v0
.end method

.method static synthetic d(Lcom/osp/app/signin/UserValidateCheck;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/osp/app/signin/UserValidateCheck;->Q:Ljava/lang/String;

    return-object p1
.end method

.method private d()Z
    .locals 3

    .prologue
    .line 3344
    const/4 v0, 0x0

    .line 3345
    new-instance v1, Lcom/samsung/android/sdk/pass/Spass;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pass/Spass;-><init>()V

    iput-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->ab:Lcom/samsung/android/sdk/pass/Spass;

    .line 3346
    new-instance v1, Lcom/samsung/android/sdk/pass/SpassFingerprint;

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->H:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pass/SpassFingerprint;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->ac:Lcom/samsung/android/sdk/pass/SpassFingerprint;

    .line 3350
    :try_start_0
    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->ab:Lcom/samsung/android/sdk/pass/Spass;

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->H:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pass/Spass;->initialize(Landroid/content/Context;)V

    .line 3351
    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->ac:Lcom/samsung/android/sdk/pass/SpassFingerprint;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->hasRegisteredFinger()Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/SsdkUnsupportedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result v0

    .line 3365
    :goto_0
    return v0

    .line 3353
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 3357
    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/SsdkUnsupportedException;->printStackTrace()V

    goto :goto_0

    .line 3361
    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic e(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->E:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic e(Lcom/osp/app/signin/UserValidateCheck;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 90
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/osp/app/signin/EmailValidationView;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "email_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "is_resend"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "is_bgmode"

    const/16 v2, 0xca

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic f(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic g(Lcom/osp/app/signin/UserValidateCheck;)Z
    .locals 1

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->h()Z

    move-result v0

    return v0
.end method

.method static synthetic h(Lcom/osp/app/signin/UserValidateCheck;)Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/osp/app/signin/UserValidateCheck;->X:Z

    return v0
.end method

.method static synthetic i(Lcom/osp/app/signin/UserValidateCheck;)Z
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/UserValidateCheck;->X:Z

    return v0
.end method

.method static synthetic j(Lcom/osp/app/signin/UserValidateCheck;)Z
    .locals 1

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->h()Z

    move-result v0

    return v0
.end method

.method static synthetic k(Lcom/osp/app/signin/UserValidateCheck;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ae:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic l(Lcom/osp/app/signin/UserValidateCheck;)Lcom/osp/app/signin/BottomSoftkeyLinearLayout;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    return-object v0
.end method

.method static synthetic m(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->F:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic n(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->B:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic o(Lcom/osp/app/signin/UserValidateCheck;)I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/osp/app/signin/UserValidateCheck;->c:I

    return v0
.end method

.method static synthetic p(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->z:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic q(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->C:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic r(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->H:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic s(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->D:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic t(Lcom/osp/app/signin/UserValidateCheck;)V
    .locals 3

    .prologue
    .line 90
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->E:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->E:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    :cond_0
    return-void
.end method

.method static synthetic u(Lcom/osp/app/signin/UserValidateCheck;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v8, -0x1

    .line 90
    const-string v0, "CHANGE_SIM_ALERT"

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->M:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Inputpassword Certification"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-virtual {p0, v8}, Lcom/osp/app/signin/UserValidateCheck;->b(I)V

    :cond_0
    :goto_0
    const-string v0, "BG_mode"

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->M:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/osp/app/signin/UserValidateCheck;->S:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->D:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/msc/c/e;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Lcom/msc/c/c;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    :try_start_0
    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v1

    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/msc/c/a;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4, v0}, Lcom/msc/c/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "UVC"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v6

    const-string v7, "EmailID"

    invoke-virtual {v6, v7}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " encrypted."

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5

    move-object v4, v1

    :goto_1
    :try_start_2
    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v0

    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/c/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/msc/c/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v1

    :try_start_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "UVC"

    const-string v3, "external_b encrypted."

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    move-object v3, v1

    :goto_2
    :try_start_4
    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v0

    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/c/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Lcom/msc/c/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    move-result-object v0

    :try_start_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "UVC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v5

    const-string v6, "key_user_id"

    invoke-virtual {v5, v6}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " encrypted."

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    :goto_3
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "bg_result"

    invoke-virtual {v1, v2, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "apps_id"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "apps_birthdate"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "apps_userid"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "signUpInfo"

    const-string v2, ""

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->C:Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->C:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->C:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-wide v2, p0, Lcom/osp/app/signin/UserValidateCheck;->W:J

    invoke-virtual {p0, v1, v2, v3}, Lcom/osp/app/signin/UserValidateCheck;->a(Landroid/content/Intent;J)V

    :cond_1
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    :cond_2
    :goto_4
    return-void

    :cond_3
    const-string v0, "REMOTE_CONTROLS"

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->M:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Inputpassword Certification"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-virtual {p0, v8}, Lcom/osp/app/signin/UserValidateCheck;->b(I)V

    goto/16 :goto_0

    :cond_4
    const-string v0, "ALERT_MESSAGE_RECIPIENTS"

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->M:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Inputpassword Certification"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-virtual {p0, v8}, Lcom/osp/app/signin/UserValidateCheck;->b(I)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_5
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "UVC"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v6

    const-string v7, "EmailID"

    invoke-virtual {v6, v7}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " encrypt failed."

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v1

    goto/16 :goto_1

    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_6
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "UVC"

    const-string v3, "external_b encrypt failed."

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v1

    goto/16 :goto_2

    :catch_2
    move-exception v0

    move-object v1, v0

    move-object v0, v2

    :goto_7
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "UVC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v5

    const-string v6, "key_user_id"

    invoke-virtual {v5, v6}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " encrypt failed."

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_5
    iget v0, p0, Lcom/osp/app/signin/UserValidateCheck;->c:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_6

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountinfoView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->z:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->A:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account_mode"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->B:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "service_name"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->O:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->N:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/UserValidateCheck;->W:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "is_authorized_internal_action_for_accountinfo"

    const-string v2, "TIME_PATTERN"

    invoke-static {v2}, Lcom/osp/app/util/ad;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto/16 :goto_4

    :cond_6
    iget v0, p0, Lcom/osp/app/signin/UserValidateCheck;->c:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.SignUpView_Payment"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto/16 :goto_4

    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->B:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->B:Ljava/lang/String;

    const-string v1, "ACCOUNTINFO_MODIFY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountinfoView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->z:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->A:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account_mode"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->B:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "service_name"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->O:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->N:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/UserValidateCheck;->W:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "is_authorized_internal_action_for_accountinfo"

    const-string v2, "TIME_PATTERN"

    invoke-static {v2}, Lcom/osp/app/util/ad;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto/16 :goto_4

    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->B:Ljava/lang/String;

    const-string v1, "CREDITCARD_MODIFY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.SignUpView_Payment"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto/16 :goto_4

    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->B:Ljava/lang/String;

    const-string v1, "ACCOUNT_VERIFY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_a
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    invoke-virtual {p0, v8, v0}, Lcom/osp/app/signin/UserValidateCheck;->a(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto/16 :goto_4

    :catch_3
    move-exception v1

    goto/16 :goto_7

    :catch_4
    move-exception v0

    goto/16 :goto_6

    :catch_5
    move-exception v0

    goto/16 :goto_5
.end method

.method static synthetic v(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->A:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic w(Lcom/osp/app/signin/UserValidateCheck;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->T:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic x(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->U:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic y(Lcom/osp/app/signin/UserValidateCheck;)Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/osp/app/signin/UserValidateCheck;->P:Z

    return v0
.end method

.method static synthetic z(Lcom/osp/app/signin/UserValidateCheck;)Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/osp/app/signin/UserValidateCheck;->aa:Z

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const v4, 0x7f0c0197

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1276
    iget-boolean v0, p0, Lcom/osp/app/signin/UserValidateCheck;->Z:Z

    if-eqz v0, :cond_0

    .line 1278
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ae:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 1280
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ae:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 1281
    if-eqz v0, :cond_0

    .line 1283
    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->E:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-nez v1, :cond_3

    .line 1285
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1295
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->E:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_5

    .line 1297
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_1

    .line 1299
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1301
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/Boolean;)V

    .line 1308
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->x:Landroid/view/Menu;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->x:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->size()I

    move-result v0

    if-le v0, v2, :cond_2

    .line 1310
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->x:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1329
    :cond_2
    :goto_2
    return-void

    .line 1288
    :cond_3
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 1304
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/Boolean;)V

    goto :goto_1

    .line 1314
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_6

    .line 1316
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1318
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/Boolean;)V

    .line 1324
    :cond_6
    :goto_3
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->x:Landroid/view/Menu;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->x:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->size()I

    move-result v0

    if-le v0, v2, :cond_2

    .line 1326
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->x:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_2

    .line 1321
    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/Boolean;)V

    goto :goto_3
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 3111
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 876
    invoke-super {p0, p1, p2, p3}, Lcom/osp/app/util/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 877
    const/16 v0, 0x64

    if-ne p1, v0, :cond_3

    .line 879
    const/16 v0, 0xc

    if-ne p2, v0, :cond_1

    .line 881
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ae:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ae:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 883
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ae:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 940
    :cond_0
    :goto_0
    return-void

    .line 886
    :cond_1
    if-nez p2, :cond_2

    .line 888
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->b(I)V

    .line 889
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto :goto_0

    .line 893
    :cond_2
    invoke-virtual {p0, v3}, Lcom/osp/app/signin/UserValidateCheck;->b(I)V

    .line 894
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto :goto_0

    .line 899
    :cond_3
    const/16 v0, 0xd2

    if-eq p1, v0, :cond_0

    .line 912
    const/16 v0, 0xd6

    if-ne p1, v0, :cond_6

    .line 914
    iget-boolean v0, p0, Lcom/osp/app/signin/UserValidateCheck;->Z:Z

    if-eqz v0, :cond_5

    .line 917
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 918
    const-string v1, "error_code"

    const-string v2, "SAC_0201"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 919
    const-string v1, "error_message"

    const-string v2, "Self upgrade canceled"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 921
    invoke-virtual {p0, v3, v0}, Lcom/osp/app/signin/UserValidateCheck;->a(ILandroid/content/Intent;)V

    .line 939
    :cond_4
    :goto_1
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto :goto_0

    .line 924
    :cond_5
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->b(I)V

    goto :goto_1

    .line 927
    :cond_6
    const/16 v0, 0xda

    if-ne p1, v0, :cond_4

    .line 929
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    invoke-virtual {p0, p2, v0}, Lcom/osp/app/signin/UserValidateCheck;->a(ILandroid/content/Intent;)V

    .line 931
    invoke-static {}, Lcom/osp/app/util/r;->u()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 933
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SAMSUNGACCOUNT_SIGNUPINFO_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 934
    const-string v1, "signUpInfo"

    const-string v2, "SA_EMAIL_;SA_COUNTRY_;SA_BIRTHDATE_;SA_EMAILRECEIVE_;"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 936
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 760
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->I:Lcom/osp/app/signin/pn;

    if-eqz v0, :cond_0

    .line 762
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->I:Lcom/osp/app/signin/pn;

    invoke-virtual {v0}, Lcom/osp/app/signin/pn;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_0

    .line 764
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->I:Lcom/osp/app/signin/pn;

    invoke-virtual {v0}, Lcom/osp/app/signin/pn;->d()V

    .line 767
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->J:Lcom/osp/app/signin/po;

    if-eqz v0, :cond_1

    .line 769
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->J:Lcom/osp/app/signin/po;

    invoke-virtual {v0}, Lcom/osp/app/signin/po;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_1

    .line 771
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->J:Lcom/osp/app/signin/po;

    invoke-virtual {v0}, Lcom/osp/app/signin/po;->d()V

    .line 775
    :cond_1
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onBackPressed()V

    .line 776
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 660
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 662
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->a(I)V

    .line 663
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/16 v10, 0x8

    const v9, 0x7f090064

    const v8, 0x7f090019

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 398
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 400
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 401
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 402
    const-string v2, "theme"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 404
    const-string v2, "com.msc.action.samsungaccount.PROFILE_FOR_SETTING"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 406
    invoke-static {v7}, Lcom/osp/app/signin/SamsungService;->a(Z)V

    .line 409
    :cond_0
    const-string v2, "com.msc.action.samsungaccount.CONFIRM_PASSWORD_POPUP"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 411
    invoke-virtual {p0, v6}, Lcom/osp/app/signin/UserValidateCheck;->d(I)V

    .line 413
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->h(Ljava/lang/String;)V

    .line 415
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->k()V

    .line 416
    invoke-static {p0}, Lcom/osp/app/signin/UserValidateCheck;->a(Landroid/app/Activity;)V

    .line 417
    const v0, 0x7f030016

    iput v0, p0, Lcom/osp/app/signin/UserValidateCheck;->Y:I

    .line 419
    iput-boolean v6, p0, Lcom/osp/app/signin/UserValidateCheck;->Z:Z

    .line 421
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "client_id"

    aput-object v1, v0, v7

    const-string v1, "client_secret"

    aput-object v1, v0, v6

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->a([Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 423
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 656
    :cond_1
    :goto_0
    return-void

    .line 427
    :cond_2
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->l()I

    move-result v0

    .line 428
    const/16 v1, 0xe

    if-ge v0, v1, :cond_3

    .line 431
    const-string v1, "Not support device [%s]"

    new-array v2, v6, [Ljava/lang/Object;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ApiLevel"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 432
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 433
    const-string v2, "error_code"

    const-string v3, "SAC_0105"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 434
    const-string v2, "error_message"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 435
    invoke-virtual {p0, v6, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(ILandroid/content/Intent;)V

    .line 436
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    .line 438
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    goto :goto_0

    .line 440
    :cond_3
    invoke-static {p0}, Lcom/osp/app/util/x;->o(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 443
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->k(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x78

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->h(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->e(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 445
    const-string v1, "Not support device [%s]"

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 446
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 447
    const-string v2, "error_code"

    const-string v3, "SAC_0105"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 448
    const-string v2, "error_message"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 449
    invoke-virtual {p0, v6, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(ILandroid/content/Intent;)V

    .line 450
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    .line 452
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 457
    :cond_5
    const v0, 0x7f03001f

    iput v0, p0, Lcom/osp/app/signin/UserValidateCheck;->Y:I

    .line 459
    iput-boolean v7, p0, Lcom/osp/app/signin/UserValidateCheck;->Z:Z

    .line 462
    :cond_6
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 466
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0, p0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/app/Activity;)V

    .line 467
    iput-object p0, p0, Lcom/osp/app/signin/UserValidateCheck;->H:Landroid/content/Context;

    .line 481
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    const-string v1, "MODE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->M:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    const-string v1, "client_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->z:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    const-string v1, "client_secret"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->A:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    const-string v1, "account_mode"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->B:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    const-string v1, "OSP_VER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->N:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    const-string v1, "service_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->O:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    const-string v1, "more_info"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->U:Ljava/lang/String;

    invoke-static {}, Lcom/osp/app/util/p;->a()Lcom/osp/app/util/p;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->H:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/osp/app/util/p;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "setting_finger"

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->U:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    const-string v1, "set_value"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->V:Ljava/lang/String;

    :cond_7
    const-string v0, "SamsungApps"

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iput-boolean v6, p0, Lcom/osp/app/signin/UserValidateCheck;->S:Z

    :cond_8
    const-string v0, "BG_mode"

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->M:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    const-string v1, "mypackage"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->C:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    const-string v1, "key_request_id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/UserValidateCheck;->W:J

    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    const-string v1, "NEED_AUTHCODE"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/UserValidateCheck;->P:Z

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    const-string v1, "key_signout_request_from_settings"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/UserValidateCheck;->aa:Z

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.msc.action.samsungaccount.accountinfo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.msc.action.samsungaccount.PROFILE_FOR_SETTING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    :cond_a
    iput v6, p0, Lcom/osp/app/signin/UserValidateCheck;->c:I

    :cond_b
    :goto_1
    const-string v0, "OSP_02"

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->N:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->N:Ljava/lang/String;

    invoke-static {v0}, Lcom/osp/app/util/ad;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->N:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/osp/app/signin/UserValidateCheck;->Z:Z

    if-eqz v0, :cond_c

    const-string v0, "ACCOUNT_VERIFY"

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->B:Ljava/lang/String;

    .line 482
    :cond_c
    iget v0, p0, Lcom/osp/app/signin/UserValidateCheck;->Y:I

    invoke-static {p0}, Lcom/osp/app/util/r;->q(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(IZ)V

    .line 484
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-boolean v0, p0, Lcom/osp/app/signin/UserValidateCheck;->Z:Z

    if-eqz v0, :cond_12

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030017

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    const v0, 0x7f0c00b4

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->D:Landroid/widget/TextView;

    const v0, 0x7f0c00b6

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->E:Landroid/widget/EditText;

    const v0, 0x7f0c00b3

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0c00b5

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0c00b7

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->F:Landroid/widget/CheckBox;

    const v2, 0x7f0c00b8

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->G:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->F:Landroid/widget/CheckBox;

    if-eqz v2, :cond_d

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-le v2, v4, :cond_d

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0800e2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iget-object v4, p0, Lcom/osp/app/signin/UserValidateCheck;->F:Landroid/widget/CheckBox;

    invoke-static {v4, v2, v7, v7, v7}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    :cond_d
    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->D:Landroid/widget/TextView;

    if-eqz v2, :cond_e

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_e

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->D:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v5, 0x7f070006

    invoke-static {v5}, Lcom/osp/app/util/an;->a(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_e
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v4, 0x7f070020

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070020

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->F:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070020

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setTextColor(I)V

    const v0, 0x106000d

    invoke-virtual {v3, v0}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->G:Landroid/widget/TextView;

    if-eqz v0, :cond_f

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0800e6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->G:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f020089

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->G:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v4, 0x7f070014

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->G:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v4, 0x7f070014

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLinkTextColor(I)V

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->G:Landroid/widget/TextView;

    invoke-static {v1, v0, v0, v0, v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    :cond_f
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f09004f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/osp/app/signin/pl;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/pl;-><init>(Lcom/osp/app/signin/UserValidateCheck;)V

    invoke-virtual {v0, v8, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/osp/app/signin/ox;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/ox;-><init>(Lcom/osp/app/signin/UserValidateCheck;)V

    invoke-virtual {v0, v9, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/osp/app/signin/oy;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/oy;-><init>(Lcom/osp/app/signin/UserValidateCheck;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ae:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ae:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080101

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->ae:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 486
    :cond_10
    :goto_2
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1f

    .line 488
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/osp/app/signin/AccountView;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 489
    const-string v1, "MODE"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck;->M:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 490
    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/UserValidateCheck;->W:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 491
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->startActivity(Landroid/content/Intent;)V

    .line 492
    invoke-virtual {p0, v6}, Lcom/osp/app/signin/UserValidateCheck;->b(I)V

    .line 493
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto/16 :goto_0

    .line 481
    :cond_11
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.msc.action.samsungaccount.changecreditcard"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v0, 0x2

    iput v0, p0, Lcom/osp/app/signin/UserValidateCheck;->c:I

    goto/16 :goto_1

    .line 484
    :cond_12
    const v0, 0x7f0c00b5

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0c00b4

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/UserValidateCheck;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->D:Landroid/widget/TextView;

    const v1, 0x7f0c00b6

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/UserValidateCheck;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->E:Landroid/widget/EditText;

    const v1, 0x7f0c00b7

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/UserValidateCheck;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->F:Landroid/widget/CheckBox;

    invoke-static {}, Lcom/osp/app/util/r;->v()Z

    move-result v1

    if-eqz v1, :cond_16

    const v1, 0x7f0c00e3

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/UserValidateCheck;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_13

    invoke-virtual {v1, v10}, Landroid/view/View;->setVisibility(I)V

    :cond_13
    const v1, 0x7f0c016f

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/UserValidateCheck;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_14

    invoke-virtual {v1, v10}, Landroid/view/View;->setVisibility(I)V

    :cond_14
    const v1, 0x7f0c00e4

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/UserValidateCheck;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_15

    invoke-virtual {v1, v10}, Landroid/view/View;->setVisibility(I)V

    :cond_15
    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->F:Landroid/widget/CheckBox;

    if-eqz v1, :cond_16

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_16

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->F:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08007c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setMinimumHeight(I)V

    :cond_16
    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->F:Landroid/widget/CheckBox;

    if-eqz v1, :cond_17

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-le v1, v3, :cond_17

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0800e2

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iget-object v3, p0, Lcom/osp/app/signin/UserValidateCheck;->F:Landroid/widget/CheckBox;

    invoke-static {v3, v1, v7, v7, v7}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    :cond_17
    const v1, 0x7f0c00c8

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/UserValidateCheck;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    if-eqz v1, :cond_18

    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1d

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    :cond_18
    :goto_3
    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->D:Landroid/widget/TextView;

    if-eqz v1, :cond_19

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_19

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->D:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f070006

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_19

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->h()Z

    move-result v1

    if-nez v1, :cond_19

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->D:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070006

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    :cond_19
    if-eqz v0, :cond_1a

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f070006

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_1a
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->F:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->F:Landroid/widget/CheckBox;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f070004

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setTextColor(I)V

    :cond_1b
    const v0, 0x7f0c00b8

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->G:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->G:Landroid/widget/TextView;

    if-eqz v0, :cond_1c

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0800e6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->G:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f020089

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->G:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f070014

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->G:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f070014

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLinkTextColor(I)V

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->G:Landroid/widget/TextView;

    invoke-static {v1, v0, v0, v0, v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    :cond_1c
    const v0, 0x7f0c0065

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->h()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/content/Context;Ljava/lang/Boolean;)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v9}, Lcom/osp/app/signin/UserValidateCheck;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v9}, Lcom/osp/app/signin/UserValidateCheck;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v8}, Lcom/osp/app/signin/UserValidateCheck;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v8}, Lcom/osp/app/signin/UserValidateCheck;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_1d
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_18

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f020142

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_3

    :cond_1e
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v8}, Lcom/osp/app/signin/UserValidateCheck;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v8}, Lcom/osp/app/signin/UserValidateCheck;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v9}, Lcom/osp/app/signin/UserValidateCheck;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v9}, Lcom/osp/app/signin/UserValidateCheck;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 497
    :cond_1f
    invoke-static {p0, v6}, Lcom/msc/c/c;->a(Landroid/content/Context;Z)Z

    move-result v0

    if-nez v0, :cond_20

    .line 499
    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->a(Ljava/lang/String;)V

    .line 500
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto/16 :goto_0

    .line 505
    :cond_20
    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->D:Landroid/widget/TextView;

    if-eqz v1, :cond_21

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->D:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_21
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->E:Landroid/widget/EditText;

    if-eqz v0, :cond_23

    iget-boolean v0, p0, Lcom/osp/app/signin/UserValidateCheck;->Z:Z

    if-eqz v0, :cond_22

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_22

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->E:Landroid/widget/EditText;

    invoke-virtual {v0, v7}, Landroid/widget/EditText;->setImeOptions(I)V

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->E:Landroid/widget/EditText;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setImeOptions(I)V

    :cond_22
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->E:Landroid/widget/EditText;

    new-instance v1, Lcom/osp/app/signin/ph;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/ph;-><init>(Lcom/osp/app/signin/UserValidateCheck;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->E:Landroid/widget/EditText;

    new-instance v1, Lcom/osp/app/signin/pi;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/pi;-><init>(Lcom/osp/app/signin/UserValidateCheck;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->E:Landroid/widget/EditText;

    new-instance v1, Lcom/osp/app/signin/pj;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/pj;-><init>(Lcom/osp/app/signin/UserValidateCheck;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_23
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->F:Landroid/widget/CheckBox;

    if-eqz v0, :cond_24

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->F:Landroid/widget/CheckBox;

    new-instance v1, Lcom/osp/app/signin/pk;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/pk;-><init>(Lcom/osp/app/signin/UserValidateCheck;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_24
    iget-boolean v0, p0, Lcom/osp/app/signin/UserValidateCheck;->Z:Z

    if-nez v0, :cond_25

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_31

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    :cond_25
    :goto_4
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v0, v10}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->setVisibility(I)V

    :cond_26
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<a href=\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->h()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/msc/c/g;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->G:Landroid/widget/TextView;

    if-eqz v1, :cond_27

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->G:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x7f0900f1

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/UserValidateCheck;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "</a>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->G:Landroid/widget/TextView;

    new-instance v1, Lcom/osp/app/signin/pf;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/pf;-><init>(Lcom/osp/app/signin/UserValidateCheck;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->G:Landroid/widget/TextView;

    new-instance v1, Lcom/osp/app/signin/pg;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/pg;-><init>(Lcom/osp/app/signin/UserValidateCheck;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 506
    :cond_27
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->a()V

    .line 525
    invoke-static {p0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;)V

    .line 528
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->a(I)V

    .line 532
    invoke-static {p0}, Lcom/osp/app/util/ad;->k(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_34

    .line 534
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_32

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.msc.action.samsungaccount.PROFILE_FOR_SETTING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_28

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.msc.action.samsungaccount.CONFIRM_PASSWORD_POPUP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 537
    :cond_28
    new-instance v0, Lcom/osp/app/signin/pm;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/pm;-><init>(Lcom/osp/app/signin/UserValidateCheck;)V

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->d:Lcom/osp/app/signin/pm;

    .line 538
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->d:Lcom/osp/app/signin/pm;

    invoke-virtual {v0}, Lcom/osp/app/signin/pm;->b()V

    .line 555
    :cond_29
    :goto_5
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 557
    const-string v0, "done_cancel"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->e(Ljava/lang/String;)V

    .line 560
    :cond_2a
    iget-boolean v0, p0, Lcom/osp/app/signin/UserValidateCheck;->Z:Z

    if-eqz v0, :cond_2e

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ae:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2e

    .line 564
    invoke-static {}, Lcom/osp/app/util/p;->a()Lcom/osp/app/util/p;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/app/util/p;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 566
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->E:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 569
    :cond_2b
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->E:Landroid/widget/EditText;

    if-eqz v0, :cond_2c

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->E:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 571
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ae:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 572
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->E:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 574
    :cond_2c
    invoke-static {p0}, Lcom/osp/app/util/ad;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 576
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ae:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 578
    :cond_2d
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->a()V

    .line 583
    :cond_2e
    invoke-static {}, Lcom/osp/app/util/p;->a()Lcom/osp/app/util/p;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->H:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/osp/app/util/p;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_35

    const-string v0, "setting_finger"

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->U:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 585
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "UVC"

    const-string v1, "Wrong calling settings FP interface, not support FP"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    invoke-virtual {p0, v6, v0}, Lcom/osp/app/signin/UserValidateCheck;->a(ILandroid/content/Intent;)V

    .line 587
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    .line 639
    :cond_2f
    :goto_6
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v6, :cond_30

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_30

    .line 641
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Self Update check"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 643
    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getApplication()Landroid/app/Application;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v6, v7, v2}, Lcom/msc/sa/selfupdate/c;->a(Landroid/app/Application;ZZLjava/lang/String;)V

    .line 648
    :cond_30
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 650
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/bigdatalog/e;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 652
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/bigdatalog/e;->b(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 505
    :cond_31
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ad:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_4

    .line 542
    :cond_32
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->B:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_33

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->B:Ljava/lang/String;

    const-string v1, "ACCOUNT_VERIFY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 544
    :cond_33
    new-instance v0, Lcom/osp/app/signin/pm;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/pm;-><init>(Lcom/osp/app/signin/UserValidateCheck;)V

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->d:Lcom/osp/app/signin/pm;

    .line 545
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->d:Lcom/osp/app/signin/pm;

    invoke-virtual {v0}, Lcom/osp/app/signin/pm;->b()V

    goto/16 :goto_5

    .line 550
    :cond_34
    invoke-static {}, Lcom/osp/app/util/p;->a()Lcom/osp/app/util/p;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/app/util/p;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_29

    .line 552
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->E:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_5

    .line 589
    :cond_35
    invoke-static {}, Lcom/osp/app/util/p;->a()Lcom/osp/app/util/p;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->H:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/osp/app/util/p;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_38

    const-string v0, "setting_finger"

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->U:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_38

    invoke-static {}, Lcom/osp/app/util/p;->a()Lcom/osp/app/util/p;

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->H:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/p;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 593
    invoke-static {}, Lcom/osp/app/util/p;->a()Lcom/osp/app/util/p;

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->V:Ljava/lang/String;

    invoke-static {v0}, Lcom/osp/app/util/p;->a(Ljava/lang/String;)I

    move-result v0

    .line 594
    const/4 v1, -0x1

    if-ne v0, v1, :cond_36

    .line 596
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "UVC"

    const-string v2, "Wrong finger setting value"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    invoke-virtual {p0, v6, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(ILandroid/content/Intent;)V

    .line 598
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    .line 600
    :cond_36
    invoke-static {}, Lcom/osp/app/util/p;->a()Lcom/osp/app/util/p;

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->H:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/osp/app/util/p;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 602
    const/4 v0, -0x1

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(ILandroid/content/Intent;)V

    .line 607
    :goto_7
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto/16 :goto_6

    .line 605
    :cond_37
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    invoke-virtual {p0, v6, v0}, Lcom/osp/app/signin/UserValidateCheck;->a(ILandroid/content/Intent;)V

    goto :goto_7

    .line 609
    :cond_38
    invoke-static {}, Lcom/osp/app/util/p;->a()Lcom/osp/app/util/p;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->H:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/osp/app/util/p;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3a

    invoke-static {}, Lcom/osp/app/util/p;->a()Lcom/osp/app/util/p;

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->H:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/p;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 614
    invoke-direct {p0}, Lcom/osp/app/signin/UserValidateCheck;->d()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-static {p0}, Lcom/osp/app/util/ad;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 616
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ae:Landroid/app/AlertDialog;

    if-eqz v0, :cond_39

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ae:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_39

    .line 618
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ae:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 620
    :cond_39
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "UVC"

    const-string v1, "hasRegisteredFinger"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/osp/app/signin/oz;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/oz;-><init>(Lcom/osp/app/signin/UserValidateCheck;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v6, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_6

    .line 635
    :cond_3a
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 636
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->E:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_6
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 694
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onDestroy()V

    .line 695
    iput v0, p0, Lcom/osp/app/signin/UserValidateCheck;->c:I

    .line 696
    iput-boolean v0, p0, Lcom/osp/app/signin/UserValidateCheck;->P:Z

    .line 712
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->I:Lcom/osp/app/signin/pn;

    if-eqz v0, :cond_0

    .line 714
    iput-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->I:Lcom/osp/app/signin/pn;

    .line 716
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->J:Lcom/osp/app/signin/po;

    if-eqz v0, :cond_1

    .line 718
    iput-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->J:Lcom/osp/app/signin/po;

    .line 733
    :cond_1
    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/c;->d()V

    .line 737
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ab:Lcom/samsung/android/sdk/pass/Spass;

    if-eqz v0, :cond_2

    .line 739
    iput-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->ab:Lcom/samsung/android/sdk/pass/Spass;

    .line 742
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->ac:Lcom/samsung/android/sdk/pass/SpassFingerprint;

    if-eqz v0, :cond_3

    .line 744
    iput-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->ac:Lcom/samsung/android/sdk/pass/SpassFingerprint;

    .line 746
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->af:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

    if-eqz v0, :cond_4

    .line 748
    iput-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->af:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

    .line 751
    :cond_4
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 944
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 946
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 975
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 950
    :sswitch_0
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->onBackPressed()V

    goto :goto_0

    .line 959
    :sswitch_1
    invoke-direct {p0}, Lcom/osp/app/signin/UserValidateCheck;->c()V

    .line 960
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->N:Ljava/lang/String;

    const-string v1, "OSP_01"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 962
    new-instance v0, Lcom/osp/app/signin/pn;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/pn;-><init>(Lcom/osp/app/signin/UserValidateCheck;)V

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->I:Lcom/osp/app/signin/pn;

    .line 963
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->I:Lcom/osp/app/signin/pn;

    invoke-virtual {v0}, Lcom/osp/app/signin/pn;->b()V

    goto :goto_0

    .line 966
    :cond_1
    new-instance v0, Lcom/osp/app/signin/po;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/po;-><init>(Lcom/osp/app/signin/UserValidateCheck;)V

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->J:Lcom/osp/app/signin/po;

    .line 967
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->J:Lcom/osp/app/signin/po;

    invoke-virtual {v0}, Lcom/osp/app/signin/po;->b()V

    goto :goto_0

    .line 973
    :sswitch_2
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->y:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(ILandroid/content/Intent;)V

    .line 974
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto :goto_0

    .line 946
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0c018e -> :sswitch_2
        0x7f0c0197 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 851
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 855
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->F:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 857
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->F:Landroid/widget/CheckBox;

    const v1, 0x7f09012a

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    .line 860
    :cond_0
    if-eqz p1, :cond_2

    .line 862
    const-string v0, "FIELD_INFO"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 863
    if-eqz v0, :cond_1

    .line 865
    invoke-static {v0}, Lcom/osp/app/signin/kd;->a(Landroid/os/Bundle;)Lcom/osp/app/signin/kd;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->L:Lcom/osp/app/signin/kd;

    .line 867
    :cond_1
    const-string v0, "AUTH_CODE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->Q:Ljava/lang/String;

    .line 868
    const-string v0, "USER_AUTH_TOKEN"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->R:Ljava/lang/String;

    .line 872
    :cond_2
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 780
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onResume()V

    .line 786
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 788
    iget-boolean v0, p0, Lcom/osp/app/signin/UserValidateCheck;->Z:Z

    if-eqz v0, :cond_4

    .line 790
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 791
    const-string v1, "error_code"

    const-string v2, "SAC_0102"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 792
    const-string v1, "error_message"

    const-string v2, "Samsung account does not exist"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 793
    invoke-virtual {p0, v4, v0}, Lcom/osp/app/signin/UserValidateCheck;->a(ILandroid/content/Intent;)V

    .line 798
    :goto_0
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    .line 801
    :cond_0
    iget-boolean v0, p0, Lcom/osp/app/signin/UserValidateCheck;->Z:Z

    if-nez v0, :cond_1

    .line 803
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->D:Landroid/widget/TextView;

    if-nez v0, :cond_1

    .line 805
    const v0, 0x7f0c00b4

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->D:Landroid/widget/TextView;

    .line 808
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->D:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->D:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 810
    iget-boolean v0, p0, Lcom/osp/app/signin/UserValidateCheck;->Z:Z

    if-eqz v0, :cond_5

    .line 812
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 813
    const-string v1, "error_code"

    const-string v2, "SAC_0104"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 814
    const-string v1, "error_message"

    const-string v2, "Sign in ID is changed"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 815
    invoke-virtual {p0, v4, v0}, Lcom/osp/app/signin/UserValidateCheck;->a(ILandroid/content/Intent;)V

    .line 820
    :goto_1
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    .line 823
    :cond_2
    iget-boolean v0, p0, Lcom/osp/app/signin/UserValidateCheck;->Z:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->E:Landroid/widget/EditText;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->E:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 825
    invoke-virtual {p0}, Lcom/osp/app/signin/UserValidateCheck;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 828
    :cond_3
    iput-boolean v3, p0, Lcom/osp/app/signin/UserValidateCheck;->X:Z

    .line 829
    return-void

    .line 796
    :cond_4
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/UserValidateCheck;->b(I)V

    goto :goto_0

    .line 818
    :cond_5
    invoke-virtual {p0, v3}, Lcom/osp/app/signin/UserValidateCheck;->b(I)V

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 834
    if-eqz p1, :cond_1

    .line 837
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck;->L:Lcom/osp/app/signin/kd;

    if-eqz v0, :cond_0

    .line 839
    const-string v0, "FIELD_INFO"

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->L:Lcom/osp/app/signin/kd;

    invoke-virtual {v1}, Lcom/osp/app/signin/kd;->X()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 842
    :cond_0
    const-string v0, "AUTH_CODE"

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->Q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 843
    const-string v0, "USER_AUTH_TOKEN"

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck;->R:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 845
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 847
    :cond_1
    return-void
.end method
