.class final Lcom/osp/app/signin/oq;
.super Ljava/lang/Object;
.source "TnCView.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/op;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/op;)V
    .locals 0

    .prologue
    .line 3339
    iput-object p1, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3343
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onCheckedChanged() - AgreeAll ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3346
    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0, p2}, Lcom/osp/app/signin/TnCView;->b(Lcom/osp/app/signin/TnCView;Z)Z

    .line 3347
    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0, p2}, Lcom/osp/app/signin/TnCView;->c(Lcom/osp/app/signin/TnCView;Z)Z

    .line 3348
    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0, p2}, Lcom/osp/app/signin/TnCView;->d(Lcom/osp/app/signin/TnCView;Z)Z

    .line 3349
    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/util/r;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3351
    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0, p2}, Lcom/osp/app/signin/TnCView;->e(Lcom/osp/app/signin/TnCView;Z)Z

    .line 3353
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->i(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3355
    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0, p2}, Lcom/osp/app/signin/TnCView;->f(Lcom/osp/app/signin/TnCView;Z)Z

    .line 3359
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->r(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 3361
    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->r(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setSoundEffectsEnabled(Z)V

    .line 3362
    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->r(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 3366
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->s(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 3368
    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->s(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setSoundEffectsEnabled(Z)V

    .line 3369
    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->s(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 3373
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->t(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 3375
    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->t(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setSoundEffectsEnabled(Z)V

    .line 3376
    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->t(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 3380
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/util/r;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->u(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 3382
    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->u(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setSoundEffectsEnabled(Z)V

    .line 3383
    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->u(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 3387
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->i(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->v(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 3389
    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->v(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setSoundEffectsEnabled(Z)V

    .line 3390
    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->v(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 3394
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->w(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/op;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/op;->notifyDataSetChanged()V

    .line 3395
    iget-object v0, p0, Lcom/osp/app/signin/oq;->a:Lcom/osp/app/signin/op;

    iget-object v0, v0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->a()V

    .line 3396
    invoke-virtual {p1, v2}, Landroid/widget/CompoundButton;->playSoundEffect(I)V

    .line 3397
    return-void
.end method
