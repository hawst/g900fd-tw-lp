.class final Lcom/osp/app/signin/cr;
.super Lcom/msc/c/b;
.source "BackgroundModeService.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/BackgroundModeService;

.field private final d:Lcom/osp/app/util/d;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/BackgroundModeService;Lcom/osp/app/util/d;)V
    .locals 0

    .prologue
    .line 2204
    iput-object p1, p0, Lcom/osp/app/signin/cr;->c:Lcom/osp/app/signin/BackgroundModeService;

    .line 2205
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 2206
    invoke-virtual {p0}, Lcom/osp/app/signin/cr;->c()V

    .line 2207
    iput-object p2, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    .line 2210
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 2216
    :try_start_0
    new-instance v0, Lcom/osp/app/signin/jz;

    iget-object v1, p0, Lcom/osp/app/signin/cr;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-direct {v0, v1}, Lcom/osp/app/signin/jz;-><init>(Landroid/content/Context;)V

    .line 2219
    iget-object v1, p0, Lcom/osp/app/signin/cr;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/jz;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/osp/app/signin/kb;

    move-result-object v0

    .line 2220
    sget-object v1, Lcom/osp/app/signin/kb;->j:Lcom/osp/app/signin/kb;

    if-ne v0, v1, :cond_0

    .line 2222
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/cr;->e:Ljava/lang/String;
    :try_end_0
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/osp/device/DeviceException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2249
    :cond_0
    :goto_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 2224
    :catch_0
    move-exception v0

    .line 2226
    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    .line 2227
    const-string v1, "SSO_2002"

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "SSO_1000"

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2231
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/cr;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/msc/c/c;->e(Landroid/content/Context;)V

    .line 2235
    iget-object v0, p0, Lcom/osp/app/signin/cr;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/msc/c/e;->i(Landroid/content/Context;)V

    .line 2237
    iget-object v0, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/osp/app/util/d;->b:Z

    .line 2239
    :cond_2
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/cr;->e:Ljava/lang/String;

    goto :goto_0

    .line 2240
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/device/DeviceException;->printStackTrace()V

    .line 2243
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/cr;->e:Ljava/lang/String;

    goto :goto_0

    .line 2244
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/social/member/MemberServiceException;->printStackTrace()V

    .line 2247
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/cr;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2254
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 2259
    const-string v0, "Success"

    iget-object v1, p0, Lcom/osp/app/signin/cr;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2261
    iget-object v0, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    invoke-virtual {v0}, Lcom/msc/a/g;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2263
    iget-object v0, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget-boolean v0, v0, Lcom/osp/app/util/d;->k:Z

    if-eqz v0, :cond_1

    .line 2265
    iget-object v0, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2267
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2268
    const-string v1, "bg_result"

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2271
    const-string v1, "request_code"

    iget-object v2, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget v2, v2, Lcom/osp/app/util/d;->q:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2273
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2274
    iget-object v1, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2277
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2278
    iget-object v1, p0, Lcom/osp/app/signin/cr;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v2, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget-wide v2, v2, Lcom/osp/app/util/d;->r:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/osp/app/signin/BackgroundModeService;->a(Landroid/content/Intent;J)V

    .line 2279
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sendBroadcast to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2289
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/cr;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget v1, v1, Lcom/osp/app/util/d;->s:I

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BackgroundModeService;->stopSelf(I)V

    .line 2400
    :goto_1
    return-void

    .line 2282
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "cancel broadcast - no package name"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2287
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/cr;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cr;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    invoke-static {v0, v1, v2}, Lcom/osp/app/signin/BackgroundModeService;->a(Lcom/osp/app/signin/BackgroundModeService;Ljava/lang/String;Lcom/osp/app/util/d;)V

    goto :goto_0

    .line 2293
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget-boolean v0, v0, Lcom/osp/app/util/d;->a:Z

    if-eqz v0, :cond_3

    .line 2296
    iget-object v0, p0, Lcom/osp/app/signin/cr;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 2298
    iget-object v1, p0, Lcom/osp/app/signin/cr;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1}, Lcom/msc/c/e;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 2299
    iget-object v1, p0, Lcom/osp/app/signin/cr;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1}, Lcom/msc/c/c;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 2306
    :try_start_0
    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v1

    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/msc/c/a;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4, v0}, Lcom/msc/c/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2307
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v6

    const-string v7, "EmailID"

    invoke-virtual {v6, v7}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " encrypted."

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5

    move-object v4, v1

    .line 2325
    :goto_2
    :try_start_2
    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v0

    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/c/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/msc/c/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v1

    .line 2326
    :try_start_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v3, "external_b encrypted."

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    move-object v3, v1

    .line 2334
    :goto_3
    :try_start_4
    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v0

    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/c/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Lcom/msc/c/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    move-result-object v0

    .line 2335
    :try_start_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "BMS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v5

    const-string v6, "key_user_id"

    invoke-virtual {v5, v6}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " encrypted."

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 2342
    :goto_4
    iget-object v1, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 2344
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2345
    const-string v2, "bg_result"

    const/4 v5, -0x1

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2346
    const-string v2, "apps_id"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2352
    const-string v2, "apps_birthdate"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2353
    const-string v2, "apps_userid"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2354
    const-string v0, "signUpInfo"

    const-string v2, ""

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2357
    const-string v0, "request_code"

    iget-object v2, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget v2, v2, Lcom/osp/app/util/d;->q:I

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2359
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2360
    iget-object v0, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2362
    iget-object v0, p0, Lcom/osp/app/signin/cr;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v2, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget-wide v2, v2, Lcom/osp/app/util/d;->r:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/osp/app/signin/BackgroundModeService;->a(Landroid/content/Intent;J)V

    .line 2363
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sendBroadcast to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2399
    :cond_3
    :goto_5
    iget-object v0, p0, Lcom/osp/app/signin/cr;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget v1, v1, Lcom/osp/app/util/d;->s:I

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BackgroundModeService;->stopSelf(I)V

    goto/16 :goto_1

    .line 2308
    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_6
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2311
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v6

    const-string v7, "EmailID"

    invoke-virtual {v6, v7}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " encrypt failed."

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v1

    goto/16 :goto_2

    .line 2327
    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_7
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2330
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v3, "external_b encrypt failed."

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v1

    goto/16 :goto_3

    .line 2336
    :catch_2
    move-exception v0

    move-object v1, v0

    move-object v0, v2

    :goto_8
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 2339
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "BMS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v5

    const-string v6, "key_user_id"

    invoke-virtual {v5, v6}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " encrypt failed."

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 2366
    :cond_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "cancel broadcast - no package name"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 2371
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget-boolean v0, v0, Lcom/osp/app/util/d;->b:Z

    if-eqz v0, :cond_6

    .line 2373
    iget-object v0, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iput-boolean v3, v0, Lcom/osp/app/util/d;->b:Z

    .line 2374
    iget-object v0, p0, Lcom/osp/app/signin/cr;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    invoke-static {v0, v3, v1}, Lcom/osp/app/signin/BackgroundModeService;->a(Lcom/osp/app/signin/BackgroundModeService;ZLcom/osp/app/util/d;)V

    goto/16 :goto_5

    .line 2377
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 2379
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2380
    const-string v1, "bg_result"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2383
    const-string v1, "request_code"

    iget-object v2, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget v2, v2, Lcom/osp/app/util/d;->q:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2385
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2386
    iget-object v1, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2390
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2391
    iget-object v1, p0, Lcom/osp/app/signin/cr;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v2, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget-wide v2, v2, Lcom/osp/app/util/d;->r:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/osp/app/signin/BackgroundModeService;->a(Landroid/content/Intent;J)V

    .line 2392
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sendBroadcast to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/cr;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 2395
    :cond_7
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "cancel broadcast - no package name"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 2336
    :catch_3
    move-exception v1

    goto/16 :goto_8

    .line 2327
    :catch_4
    move-exception v0

    goto/16 :goto_7

    .line 2308
    :catch_5
    move-exception v0

    goto/16 :goto_6
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2199
    invoke-virtual {p0}, Lcom/osp/app/signin/cr;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2199
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/cr;->a(Ljava/lang/Boolean;)V

    return-void
.end method
