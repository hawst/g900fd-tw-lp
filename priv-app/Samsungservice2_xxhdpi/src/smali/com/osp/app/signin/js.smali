.class final Lcom/osp/app/signin/js;
.super Ljava/lang/Object;
.source "SignInView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/SignInView;

.field final synthetic b:Lcom/osp/app/signin/jr;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/jr;Lcom/osp/app/signin/SignInView;)V
    .locals 0

    .prologue
    .line 6382
    iput-object p1, p0, Lcom/osp/app/signin/js;->b:Lcom/osp/app/signin/jr;

    iput-object p2, p0, Lcom/osp/app/signin/js;->a:Lcom/osp/app/signin/SignInView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 6386
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/js;->b:Lcom/osp/app/signin/jr;

    iget-object v0, v0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6395
    iget-object v0, p0, Lcom/osp/app/signin/js;->b:Lcom/osp/app/signin/jr;

    iget-object v0, v0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 6396
    iget-object v1, p0, Lcom/osp/app/signin/js;->b:Lcom/osp/app/signin/jr;

    iget-object v1, v1, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0c012a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 6403
    :goto_0
    iget-object v2, p0, Lcom/osp/app/signin/js;->b:Lcom/osp/app/signin/jr;

    new-instance v3, Lcom/osp/app/signin/jz;

    iget-object v4, p0, Lcom/osp/app/signin/js;->b:Lcom/osp/app/signin/jr;

    iget-object v4, v4, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-direct {v3, v4}, Lcom/osp/app/signin/jz;-><init>(Landroid/content/Context;)V

    iput-object v3, v2, Lcom/osp/app/signin/jr;->i:Lcom/osp/app/signin/jz;

    .line 6404
    iget-object v2, p0, Lcom/osp/app/signin/js;->b:Lcom/osp/app/signin/jr;

    iget-object v2, v2, Lcom/osp/app/signin/jr;->i:Lcom/osp/app/signin/jz;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/osp/app/signin/jz;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/osp/app/signin/kb;

    move-result-object v0

    .line 6407
    sget-object v1, Lcom/osp/app/signin/kb;->j:Lcom/osp/app/signin/kb;

    if-ne v0, v1, :cond_0

    .line 6409
    iget-object v0, p0, Lcom/osp/app/signin/js;->b:Lcom/osp/app/signin/jr;

    invoke-static {v0}, Lcom/osp/app/signin/jr;->a(Lcom/osp/app/signin/jr;)Z
    :try_end_0
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/osp/device/DeviceException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_0 .. :try_end_0} :catch_2

    .line 6435
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/osp/app/signin/js;->b:Lcom/osp/app/signin/jr;

    invoke-static {v0}, Lcom/osp/app/signin/jr;->c(Lcom/osp/app/signin/jr;)Z

    .line 6436
    :cond_1
    return-void

    .line 6399
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/osp/app/signin/js;->b:Lcom/osp/app/signin/jr;

    iget-object v0, v0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    const v1, 0x7f0c0121

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 6400
    iget-object v1, p0, Lcom/osp/app/signin/js;->b:Lcom/osp/app/signin/jr;

    iget-object v1, v1, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    const v2, 0x7f0c0122

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;
    :try_end_1
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/osp/device/DeviceException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 6411
    :catch_0
    move-exception v0

    .line 6413
    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    .line 6414
    iget-object v1, p0, Lcom/osp/app/signin/js;->b:Lcom/osp/app/signin/jr;

    new-instance v2, Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/osp/app/signin/jr;->a(Lcom/osp/app/signin/jr;Lcom/msc/c/f;)Lcom/msc/c/f;

    .line 6415
    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->a()Ljava/lang/String;

    move-result-object v0

    .line 6416
    const-string v1, "SSO_2012"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "SSO_2018"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6418
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/js;->b:Lcom/osp/app/signin/jr;

    invoke-virtual {v0}, Lcom/osp/app/signin/jr;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 6422
    iget-object v0, p0, Lcom/osp/app/signin/js;->b:Lcom/osp/app/signin/jr;

    invoke-static {v0}, Lcom/osp/app/signin/jr;->b(Lcom/osp/app/signin/jr;)V

    goto :goto_1

    .line 6425
    :catch_1
    move-exception v0

    .line 6427
    invoke-virtual {v0}, Lcom/osp/device/DeviceException;->printStackTrace()V

    .line 6428
    iget-object v1, p0, Lcom/osp/app/signin/js;->b:Lcom/osp/app/signin/jr;

    new-instance v2, Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/osp/device/DeviceException;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/osp/device/DeviceException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/osp/app/signin/jr;->b(Lcom/osp/app/signin/jr;Lcom/msc/c/f;)Lcom/msc/c/f;

    goto :goto_1

    .line 6429
    :catch_2
    move-exception v0

    .line 6431
    invoke-virtual {v0}, Lcom/osp/social/member/MemberServiceException;->printStackTrace()V

    .line 6432
    iget-object v1, p0, Lcom/osp/app/signin/js;->b:Lcom/osp/app/signin/jr;

    new-instance v2, Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/osp/social/member/MemberServiceException;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/osp/social/member/MemberServiceException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/osp/app/signin/jr;->c(Lcom/osp/app/signin/jr;Lcom/msc/c/f;)Lcom/msc/c/f;

    goto/16 :goto_1
.end method
