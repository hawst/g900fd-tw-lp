.class public Lcom/osp/app/signin/CustomTextView;
.super Landroid/widget/TextView;
.source "CustomTextView.java"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 34
    iput-object p1, p0, Lcom/osp/app/signin/CustomTextView;->a:Landroid/content/Context;

    .line 35
    invoke-direct {p0}, Lcom/osp/app/signin/CustomTextView;->b()V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    iput-object p1, p0, Lcom/osp/app/signin/CustomTextView;->a:Landroid/content/Context;

    .line 46
    invoke-direct {p0}, Lcom/osp/app/signin/CustomTextView;->b()V

    .line 47
    return-void
.end method

.method private a()F
    .locals 3

    .prologue
    .line 54
    const/4 v0, 0x0

    .line 56
    iget-object v1, p0, Lcom/osp/app/signin/CustomTextView;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/osp/app/util/x;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 58
    iget-object v1, p0, Lcom/osp/app/signin/CustomTextView;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/osp/app/util/r;->o(Landroid/content/Context;)Lcom/osp/app/util/s;

    move-result-object v1

    sget-object v2, Lcom/osp/app/util/s;->a:Lcom/osp/app/util/s;

    if-ne v1, v2, :cond_0

    .line 60
    const/high16 v0, 0x40400000    # 3.0f

    .line 70
    :cond_0
    :goto_0
    return v0

    .line 62
    :cond_1
    iget-object v1, p0, Lcom/osp/app/signin/CustomTextView;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/osp/app/util/x;->f(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/osp/app/signin/CustomTextView;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/osp/app/util/x;->e(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    :cond_2
    iget-object v1, p0, Lcom/osp/app/signin/CustomTextView;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/osp/app/util/r;->o(Landroid/content/Context;)Lcom/osp/app/util/s;

    move-result-object v1

    sget-object v2, Lcom/osp/app/util/s;->a:Lcom/osp/app/util/s;

    if-ne v1, v2, :cond_0

    .line 66
    const/high16 v0, 0x40000000    # 2.0f

    goto :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/osp/app/signin/CustomTextView;->getTextSize()F

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/osp/app/signin/CustomTextView;->a()F

    move-result v2

    invoke-virtual {p0}, Lcom/osp/app/signin/CustomTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    add-float/2addr v0, v1

    .line 79
    const/4 v1, 0x0

    invoke-super {p0, v1, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 80
    return-void
.end method


# virtual methods
.method public setTextSize(F)V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/osp/app/signin/CustomTextView;->a()F

    move-result v0

    add-float/2addr v0, p1

    invoke-super {p0, v0}, Landroid/widget/TextView;->setTextSize(F)V

    .line 91
    return-void
.end method

.method public setTextSize(IF)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 101
    .line 102
    if-nez p1, :cond_0

    .line 104
    invoke-direct {p0}, Lcom/osp/app/signin/CustomTextView;->a()F

    move-result v0

    invoke-virtual {p0}, Lcom/osp/app/signin/CustomTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v2, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    add-float/2addr v0, p2

    .line 110
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "CustomTextView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "original size = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", custom size = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    invoke-super {p0, p1, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 112
    return-void

    .line 105
    :cond_0
    if-ne p1, v2, :cond_1

    .line 107
    invoke-direct {p0}, Lcom/osp/app/signin/CustomTextView;->a()F

    move-result v0

    add-float/2addr v0, p2

    goto :goto_0

    :cond_1
    move v0, p2

    goto :goto_0
.end method
