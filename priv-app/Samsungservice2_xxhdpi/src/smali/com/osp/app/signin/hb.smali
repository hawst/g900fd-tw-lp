.class final Lcom/osp/app/signin/hb;
.super Ljava/lang/Object;
.source "SecurityInfoChangeActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/SecurityInfoChangeActivity;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/SecurityInfoChangeActivity;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/osp/app/signin/hb;->a:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 155
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 159
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/osp/app/signin/hb;->a:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Lcom/msc/a/j;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 165
    invoke-static {}, Lcom/osp/app/util/y;->a()Lcom/osp/app/util/y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/y;->c()Ljava/lang/String;

    move-result-object v0

    .line 166
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 168
    iget-object v1, p0, Lcom/osp/app/signin/hb;->a:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Lcom/msc/a/j;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/msc/a/j;->a(Ljava/lang/String;)V

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/hb;->a:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Lcom/msc/a/j;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/hb;->a:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/j;->b(Ljava/lang/String;)V

    .line 172
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/hb;->a:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->c(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 174
    iget-object v0, p0, Lcom/osp/app/signin/hb;->a:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->c(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 175
    iget-object v0, p0, Lcom/osp/app/signin/hb;->a:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->c(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 177
    :cond_2
    return-void
.end method
