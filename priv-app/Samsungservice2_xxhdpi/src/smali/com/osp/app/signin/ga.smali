.class final Lcom/osp/app/signin/ga;
.super Lcom/msc/c/b;
.source "NameValidationWebView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/NameValidationWebView;

.field private d:J


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/NameValidationWebView;)V
    .locals 0

    .prologue
    .line 1003
    iput-object p1, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    .line 1004
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 1008
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 1024
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    iget-object v1, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v1}, Lcom/osp/app/signin/NameValidationWebView;->g(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->x(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, p0}, Lcom/msc/c/g;->l(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/ga;->d:J

    iget-wide v0, p0, Lcom/osp/app/signin/ga;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/ga;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/ga;->d:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/ga;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/ga;->d:J

    sget-object v2, Lcom/msc/b/g;->d:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 1025
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 8

    .prologue
    .line 1035
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1037
    if-nez p1, :cond_1

    .line 1118
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1042
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 1044
    iget-wide v2, p0, Lcom/osp/app/signin/ga;->d:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1046
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "NVWV"

    const-string v1, "NameValidationWebView - Skip Success."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1048
    iget-object v0, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/NameValidationWebView;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 1049
    const v1, 0x132df82

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 1051
    iget-object v0, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->q(Lcom/osp/app/signin/NameValidationWebView;)I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 1053
    iget-object v0, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->r(Lcom/osp/app/signin/NameValidationWebView;)I

    .line 1054
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->p()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1056
    iget-object v0, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/msc/openprovider/b;->g(Landroid/content/Context;)V

    .line 1060
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->q(Lcom/osp/app/signin/NameValidationWebView;)I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/util/r;->t(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1062
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "NVWV"

    const-string v1, "Show MybenefitNoti"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1066
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.mybenefitwebview_internal"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1079
    iget-object v1, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 1081
    iget-object v0, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    const v4, 0x7f09004f

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/NameValidationWebView;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    const v5, 0x7f090152

    invoke-virtual {v4, v5}, Lcom/osp/app/signin/NameValidationWebView;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1085
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->q(Lcom/osp/app/signin/NameValidationWebView;)I

    move-result v0

    if-lez v0, :cond_5

    .line 1089
    iget-object v0, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    iget-object v1, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v1}, Lcom/osp/app/signin/NameValidationWebView;->q(Lcom/osp/app/signin/NameValidationWebView;)I

    move-result v1

    iget-object v2, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v3}, Lcom/osp/app/signin/NameValidationWebView;->s(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v4}, Lcom/osp/app/signin/NameValidationWebView;->t(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v5}, Lcom/osp/app/signin/NameValidationWebView;->u(Lcom/osp/app/signin/NameValidationWebView;)Z

    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1092
    iget-object v1, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v1}, Lcom/osp/app/signin/NameValidationWebView;->v(Lcom/osp/app/signin/NameValidationWebView;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1094
    const-string v1, "key_return_result"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1095
    iget-object v1, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    const/16 v2, 0xdc

    invoke-virtual {v1, v0, v2}, Lcom/osp/app/signin/NameValidationWebView;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 1035
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1099
    :cond_4
    :try_start_2
    iget-object v1, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/NameValidationWebView;->startActivity(Landroid/content/Intent;)V

    .line 1103
    :cond_5
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1104
    const-string v1, "key_name_check_familyname"

    iget-object v2, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->h(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1105
    const-string v1, "key_name_check_givenname"

    iget-object v2, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->i(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1106
    const-string v1, "key_name_check_birthdate"

    iget-object v2, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->j(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1107
    const-string v1, "key_name_check_mobile"

    iget-object v2, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->w(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1108
    const-string v1, "key_name_check_method"

    iget-object v2, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->k(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1109
    const-string v1, "key_name_check_ci"

    iget-object v2, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->l(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1110
    const-string v1, "key_name_check_di"

    iget-object v2, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->m(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1111
    const-string v1, "key_name_check_datetime"

    iget-object v2, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->n(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1112
    const-string v1, "key_name_check_gender"

    iget-object v2, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->o(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1113
    const-string v1, "key_name_check_foreigner"

    iget-object v2, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->x(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1115
    iget-object v1, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/signin/NameValidationWebView;->a(ILandroid/content/Intent;)V

    .line 1116
    iget-object v0, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/NameValidationWebView;->finish()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 1030
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 1031
    return-void
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 8

    .prologue
    .line 1122
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 1124
    if-nez p1, :cond_1

    .line 1164
    :cond_0
    :goto_0
    return-void

    .line 1129
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 1131
    iget-wide v2, p0, Lcom/osp/app/signin/ga;->d:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1134
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "NVWV"

    const-string v1, "SkipNameValidationWebView - Name Check Fail."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1136
    iget-object v0, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->q(Lcom/osp/app/signin/NameValidationWebView;)I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 1138
    iget-object v0, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->r(Lcom/osp/app/signin/NameValidationWebView;)I

    .line 1140
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->q(Lcom/osp/app/signin/NameValidationWebView;)I

    move-result v0

    if-lez v0, :cond_3

    .line 1144
    iget-object v0, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    iget-object v1, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v1}, Lcom/osp/app/signin/NameValidationWebView;->q(Lcom/osp/app/signin/NameValidationWebView;)I

    move-result v1

    iget-object v2, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v3}, Lcom/osp/app/signin/NameValidationWebView;->s(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v4}, Lcom/osp/app/signin/NameValidationWebView;->t(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v5}, Lcom/osp/app/signin/NameValidationWebView;->u(Lcom/osp/app/signin/NameValidationWebView;)Z

    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1146
    iget-object v1, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/NameValidationWebView;->startActivity(Landroid/content/Intent;)V

    .line 1149
    :cond_3
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1150
    const-string v1, "key_name_check_familyname"

    iget-object v2, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->h(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1151
    const-string v1, "key_name_check_givenname"

    iget-object v2, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->i(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1152
    const-string v1, "key_name_check_birthdate"

    iget-object v2, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->j(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1153
    const-string v1, "key_name_check_mobile"

    iget-object v2, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->w(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1154
    const-string v1, "key_name_check_method"

    iget-object v2, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->k(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1155
    const-string v1, "key_name_check_ci"

    iget-object v2, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->l(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1156
    const-string v1, "key_name_check_di"

    iget-object v2, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->m(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1157
    const-string v1, "key_name_check_datetime"

    iget-object v2, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->n(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1158
    const-string v1, "key_name_check_gender"

    iget-object v2, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->o(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1159
    const-string v1, "key_name_check_foreigner"

    iget-object v2, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->x(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1161
    iget-object v1, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/signin/NameValidationWebView;->a(ILandroid/content/Intent;)V

    .line 1162
    iget-object v0, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/NameValidationWebView;->finish()V

    goto/16 :goto_0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 1012
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 1013
    iget-object v0, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/NameValidationWebView;->b(I)V

    .line 1014
    iget-object v0, p0, Lcom/osp/app/signin/ga;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/NameValidationWebView;->finish()V

    .line 1015
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 999
    invoke-virtual {p0}, Lcom/osp/app/signin/ga;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 999
    check-cast p1, Ljava/lang/Boolean;

    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected final onPreExecute()V
    .locals 0

    .prologue
    .line 1019
    invoke-super {p0}, Lcom/msc/c/b;->onPreExecute()V

    .line 1020
    return-void
.end method
