.class final Lcom/osp/app/signin/pn;
.super Lcom/msc/c/b;
.source "UserValidateCheck.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/UserValidateCheck;

.field private d:Ljava/lang/String;

.field private e:J

.field private f:J

.field private g:I

.field private h:Ljava/lang/String;

.field private i:Z


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/UserValidateCheck;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1840
    iput-object p1, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    .line 1841
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 1828
    iput v0, p0, Lcom/osp/app/signin/pn;->g:I

    .line 1838
    iput-boolean v0, p0, Lcom/osp/app/signin/pn;->i:Z

    .line 1845
    return-void
.end method

.method public constructor <init>(Lcom/osp/app/signin/UserValidateCheck;B)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1847
    iput-object p1, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    .line 1848
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 1828
    iput v0, p0, Lcom/osp/app/signin/pn;->g:I

    .line 1838
    iput-boolean v0, p0, Lcom/osp/app/signin/pn;->i:Z

    .line 1849
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/pn;->i:Z

    .line 1853
    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    .line 2064
    :try_start_0
    new-instance v0, Lcom/osp/security/identity/d;

    iget-object v1, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-direct {v0, v1}, Lcom/osp/security/identity/d;-><init>(Landroid/content/Context;)V

    .line 2065
    new-instance v1, Lcom/osp/security/identity/m;

    invoke-direct {v1}, Lcom/osp/security/identity/m;-><init>()V

    .line 2066
    iget-object v2, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->s(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/security/identity/m;->a(Ljava/lang/String;)V

    .line 2067
    iget-object v2, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->e(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/security/identity/m;->b(Ljava/lang/String;)V

    .line 2068
    invoke-virtual {v0, v1}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/m;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2070
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/pn;->d:Ljava/lang/String;

    .line 2091
    :cond_0
    :goto_0
    return-void

    .line 2073
    :cond_1
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/pn;->d:Ljava/lang/String;
    :try_end_0
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2075
    :catch_0
    move-exception v0

    .line 2077
    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    .line 2078
    new-instance v1, Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/osp/app/signin/pn;->b:Lcom/msc/c/f;

    .line 2079
    const-string v1, "SSO_2002"

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2083
    iget-object v0, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/msc/c/c;->e(Landroid/content/Context;)V

    .line 2087
    iget-object v0, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/msc/c/e;->i(Landroid/content/Context;)V

    goto :goto_0
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 4

    .prologue
    .line 1873
    const-string v0, "ACCOUNTINFO_MODIFY"

    iget-object v1, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->n(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->o(Lcom/osp/app/signin/UserValidateCheck;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 1880
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/pn;->h:Ljava/lang/String;

    .line 1882
    new-instance v0, Lcom/msc/a/f;

    invoke-direct {v0}, Lcom/msc/a/f;-><init>()V

    .line 1883
    iget-object v1, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->d(Ljava/lang/String;)V

    .line 1884
    iget-object v1, p0, Lcom/osp/app/signin/pn;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->h(Ljava/lang/String;)V

    .line 1886
    iget-object v1, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->p(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->f(Ljava/lang/String;)V

    .line 1887
    iget-object v1, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->q(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->g(Ljava/lang/String;)V

    .line 1888
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1891
    iget-object v1, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1892
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1894
    const-string v1, "Y"

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->b(Ljava/lang/String;)V

    .line 1904
    :cond_1
    iget-object v1, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1905
    invoke-virtual {v0, v1}, Lcom/msc/a/f;->c(Ljava/lang/String;)V

    .line 1906
    const-string v1, "Y"

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->a(Ljava/lang/String;)V

    .line 1909
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v1, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1, v0, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/a/f;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/pn;->e:J

    iget-wide v0, p0, Lcom/osp/app/signin/pn;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/pn;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/pn;->e:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/pn;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/pn;->e:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 1931
    :goto_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 1912
    :cond_2
    invoke-static {}, Lcom/osp/app/util/p;->a()Lcom/osp/app/util/p;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->r(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/util/p;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/osp/app/signin/pn;->i:Z

    if-eqz v0, :cond_3

    .line 1914
    iget-object v0, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(Lcom/osp/app/signin/UserValidateCheck;Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 1915
    const/4 v0, 0x0

    .line 1918
    :try_start_0
    iget-object v1, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/a;->c()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1923
    :goto_1
    iget-object v1, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->r(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->s(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v3, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v3, v1, v2, v0, p0}, Lcom/msc/c/g;->f(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/pn;->f:J

    iget-wide v0, p0, Lcom/osp/app/signin/pn;->f:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/pn;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/pn;->f:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/pn;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/pn;->f:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto :goto_0

    .line 1919
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 1927
    :cond_3
    invoke-direct {p0}, Lcom/osp/app/signin/pn;->h()V

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 1992
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1994
    if-nez p1, :cond_1

    .line 2039
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1999
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 2000
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 2002
    iget-wide v4, p0, Lcom/osp/app/signin/pn;->e:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_3

    .line 2006
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0, v2}, Lcom/msc/c/h;->b(Landroid/content/Context;Ljava/lang/String;)Lcom/msc/a/g;

    move-result-object v0

    .line 2008
    if-eqz v0, :cond_0

    .line 2010
    iget-object v0, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/msc/openprovider/b;->a(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/pn;->g:I

    .line 2011
    iget v0, p0, Lcom/osp/app/signin/pn;->g:I

    if-lez v0, :cond_2

    .line 2013
    const-string v0, "require more process"

    iput-object v0, p0, Lcom/osp/app/signin/pn;->d:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2019
    :catch_0
    move-exception v0

    .line 2021
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2022
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/pn;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1992
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2016
    :cond_2
    :try_start_4
    invoke-direct {p0}, Lcom/osp/app/signin/pn;->h()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2024
    :cond_3
    :try_start_5
    iget-wide v4, p0, Lcom/osp/app/signin/pn;->f:J
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 2028
    :try_start_6
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->w(Lcom/osp/app/signin/UserValidateCheck;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/msc/c/h;->a(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v0

    .line 2029
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 2031
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/pn;->d:Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 2033
    :catch_1
    move-exception v0

    .line 2035
    :try_start_7
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2036
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/pn;->b:Lcom/msc/c/f;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 1936
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 1937
    iget-object v0, p0, Lcom/osp/app/signin/pn;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_7

    .line 1939
    const-string v0, "USR_3113"

    iget-object v1, p0, Lcom/osp/app/signin/pn;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1941
    iget-object v0, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    iget-object v1, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(Lcom/osp/app/signin/UserValidateCheck;Landroid/content/Context;)V

    .line 1988
    :cond_0
    :goto_0
    return-void

    .line 1943
    :cond_1
    const-string v0, "USR_3192"

    iget-object v1, p0, Lcom/osp/app/signin/pn;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1945
    iget-object v0, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    iget-object v1, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->b(Lcom/osp/app/signin/UserValidateCheck;Landroid/content/Context;)V

    goto :goto_0

    .line 1947
    :cond_2
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "USR_3174"

    iget-object v1, p0, Lcom/osp/app/signin/pn;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    const-string v0, "USR_3113"

    iget-object v1, p0, Lcom/osp/app/signin/pn;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1950
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    iget-object v1, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->b(Lcom/osp/app/signin/UserValidateCheck;Ljava/lang/String;)V

    .line 1951
    iget-object v0, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto :goto_0

    .line 1955
    :cond_5
    invoke-virtual {p0, v6}, Lcom/osp/app/signin/pn;->a(Z)V

    .line 1956
    iget-object v0, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->k(Lcom/osp/app/signin/UserValidateCheck;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->k(Lcom/osp/app/signin/UserValidateCheck;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1958
    iget-object v0, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->k(Lcom/osp/app/signin/UserValidateCheck;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 1961
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->t(Lcom/osp/app/signin/UserValidateCheck;)V

    goto :goto_0

    .line 1966
    :cond_7
    const-string v0, "Success"

    iget-object v1, p0, Lcom/osp/app/signin/pn;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1974
    iget-object v0, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->u(Lcom/osp/app/signin/UserValidateCheck;)V

    goto/16 :goto_0

    .line 1975
    :cond_8
    const-string v0, "require more process"

    iget-object v1, p0, Lcom/osp/app/signin/pn;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1977
    iget-object v0, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    iget v1, p0, Lcom/osp/app/signin/pn;->g:I

    iget-object v2, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v3}, Lcom/osp/app/signin/UserValidateCheck;->p(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v4}, Lcom/osp/app/signin/UserValidateCheck;->v(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    iget-object v7, p0, Lcom/osp/app/signin/pn;->h:Ljava/lang/String;

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1979
    iget-object v1, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    const/16 v2, 0xd2

    invoke-virtual {v1, v0, v2}, Lcom/osp/app/signin/UserValidateCheck;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1982
    :cond_9
    invoke-virtual {p0, v6}, Lcom/osp/app/signin/pn;->a(Z)V

    .line 1983
    iget-object v0, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->k(Lcom/osp/app/signin/UserValidateCheck;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->k(Lcom/osp/app/signin/UserValidateCheck;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1985
    iget-object v0, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->k(Lcom/osp/app/signin/UserValidateCheck;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 1857
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 1858
    iget-object v0, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->b(I)V

    .line 1859
    iget-object v0, p0, Lcom/osp/app/signin/pn;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    .line 1860
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1808
    invoke-virtual {p0}, Lcom/osp/app/signin/pn;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1808
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/pn;->a(Ljava/lang/Boolean;)V

    return-void
.end method
