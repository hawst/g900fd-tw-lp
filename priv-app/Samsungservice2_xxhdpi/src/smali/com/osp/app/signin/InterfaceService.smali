.class public Lcom/osp/app/signin/InterfaceService;
.super Lcom/osp/app/util/AbstractBaseService;
.source "InterfaceService.java"


# instance fields
.field private a:Lcom/msc/a/i;

.field private b:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/osp/app/util/AbstractBaseService;-><init>()V

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/InterfaceService;->a:Lcom/msc/a/i;

    .line 1413
    return-void
.end method

.method static synthetic a(Lcom/msc/a/i;)I
    .locals 1

    .prologue
    .line 54
    invoke-static {p0}, Lcom/osp/app/signin/InterfaceService;->c(Lcom/msc/a/i;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/osp/app/signin/InterfaceService;)Lcom/msc/a/i;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/osp/app/signin/InterfaceService;->a:Lcom/msc/a/i;

    return-object v0
.end method

.method static synthetic a(Lcom/osp/app/signin/InterfaceService;Lcom/msc/a/i;)Lcom/msc/a/i;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/osp/app/signin/InterfaceService;->a:Lcom/msc/a/i;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/InterfaceService;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 54
    invoke-static {p0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move v1, p1

    move-object v3, p2

    move-object v4, p3

    move v6, v5

    move-object v7, p4

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x8000000

    invoke-static {p0, v5, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const/4 v2, 0x0

    const v0, 0x7f090137

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/InterfaceService;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f090149

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/InterfaceService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIS"

    const-string v1, "Show more process remain Notification to get token"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/InterfaceService;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 54
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.commoninfopopup"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "key_popup_body"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/InterfaceService;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/InterfaceService;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 54
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.resignin_with_signout"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "client_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    const-string v2, "OSP_V02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/InterfaceService;->startActivity(Landroid/content/Intent;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIS"

    const-string v1, "Sign out and show resign in for id changed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/InterfaceService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/osp/app/signin/InterfaceService;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    .line 334
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 337
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.resignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 339
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 341
    const-string v1, "client_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 342
    const-string v1, "client_secret"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 343
    const-string v1, "OSP_VER"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 344
    const-string v1, "email_id"

    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 346
    const-string v1, "tj9u972o46"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz p4, :cond_2

    .line 348
    :cond_0
    const-string v1, "from_notification"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 350
    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 352
    const/4 v2, 0x0

    const v0, 0x7f09004f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/InterfaceService;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f09008d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/InterfaceService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    .line 355
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIS"

    const-string v1, "Show resign-in Notification"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    :cond_1
    :goto_0
    return-void

    .line 358
    :cond_2
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/InterfaceService;->startActivity(Landroid/content/Intent;)V

    .line 360
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIS"

    const-string v1, "Show session expired Activity"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/msc/a/i;)I
    .locals 3

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->d()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x2

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIS"

    const-string v2, "requirement : Acceptance of TNC"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    or-int/lit8 v0, v0, 0x4

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIS"

    const-string v2, "requirement : Name verification"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->h()Z

    move-result v1

    if-eqz v1, :cond_3

    or-int/lit8 v0, v0, 0x8

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIS"

    const-string v2, "requirement : Email validation"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/kd;->b()I

    move-result v1

    if-lez v1, :cond_4

    or-int/lit8 v0, v0, 0x10

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIS"

    const-string v2, "requirement : Mandatory input"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->k()Z

    move-result v1

    if-eqz v1, :cond_5

    or-int/lit8 v0, v0, 0x20

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIS"

    const-string v2, "requirement : Acceptance of DISCLAIMER"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    return v0
.end method

.method static synthetic b(Lcom/osp/app/signin/InterfaceService;Lcom/msc/a/i;)V
    .locals 4

    .prologue
    .line 54
    const/4 v0, 0x0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.msc.action.samsungaccount.checklistinfopopup"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v2, "check_list"

    invoke-static {p1}, Lcom/osp/app/signin/InterfaceService;->c(Lcom/msc/a/i;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    iget-boolean v2, v2, Lcom/osp/app/signin/kd;->e:Z

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    iget-boolean v2, v2, Lcom/osp/app/signin/kd;->d:Z

    if-eqz v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    const-string v2, "key_popup_require_name_field"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "key_popup_require_birth_date"

    invoke-virtual {p1}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    iget-boolean v2, v2, Lcom/osp/app/signin/kd;->b:Z

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/InterfaceService;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private static c(Lcom/msc/a/i;)I
    .locals 3

    .prologue
    .line 219
    const/4 v0, 0x0

    .line 220
    invoke-virtual {p0}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->d()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 222
    :cond_0
    const/4 v0, 0x2

    .line 223
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIS"

    const-string v2, "requirement : Acceptance of TNC"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    :cond_1
    invoke-virtual {p0}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 227
    or-int/lit8 v0, v0, 0x4

    .line 228
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIS"

    const-string v2, "requirement : Name verification"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    :cond_2
    invoke-virtual {p0}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->h()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 232
    or-int/lit8 v0, v0, 0x8

    .line 233
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIS"

    const-string v2, "requirement : Email validation"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    :cond_3
    invoke-virtual {p0}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/kd;->b()I

    move-result v1

    if-lez v1, :cond_4

    .line 237
    or-int/lit8 v0, v0, 0x10

    .line 238
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIS"

    const-string v2, "requirement : Mandatory input"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    :cond_4
    return v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 77
    invoke-super {p0}, Lcom/osp/app/util/AbstractBaseService;->onCreate()V

    .line 80
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/InterfaceService;->b:Ljava/util/HashMap;

    .line 81
    return-void
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    .line 86
    iget-object v0, p0, Lcom/osp/app/signin/InterfaceService;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v2

    .line 88
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 90
    iget-object v4, p0, Lcom/osp/app/signin/InterfaceService;->b:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 92
    iget-object v4, p0, Lcom/osp/app/signin/InterfaceService;->b:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/fr;

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/osp/app/signin/fr;->cancel(Z)Z

    .line 88
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 97
    :cond_1
    invoke-super {p0}, Lcom/osp/app/util/AbstractBaseService;->onDestroy()V

    .line 98
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 111
    if-nez p1, :cond_1

    .line 209
    :cond_0
    :goto_0
    return v9

    .line 116
    :cond_1
    const-string v0, "OSP_VER"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 117
    const-string v1, "mypackage"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 118
    const-string v2, "key_request_id"

    const-wide/16 v4, 0x0

    invoke-virtual {p1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 122
    const-string v4, "OSP_02"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 125
    const-string v4, "com.msc.action.VALIDATION_CHECK_REQUEST"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 127
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {p0, v7}, Lcom/msc/c/c;->a(Landroid/content/Context;Z)Z

    move-result v4

    if-nez v4, :cond_3

    .line 129
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 131
    const-string v5, "com.msc.action.VALIDATION_CHECK_REQUEST"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 132
    const-string v5, "result_code"

    invoke-virtual {v4, v5, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 133
    const-string v5, "error_message"

    const-string v6, "Invalid DB"

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 134
    if-eqz v1, :cond_2

    .line 136
    invoke-virtual {v4, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 140
    const-string v1, "client_id"

    const-string v5, "client_id"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 142
    invoke-static {}, Lcom/osp/app/util/e;->a()Lcom/osp/app/util/e;

    move-result-object v1

    invoke-virtual {v1, p0, v4, v2, v3}, Lcom/osp/app/util/e;->a(Landroid/content/Context;Landroid/content/Intent;J)V

    .line 145
    :cond_2
    const-string v1, "client_id"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "client_secret"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2, v0, v8}, Lcom/osp/app/signin/InterfaceService;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 149
    :cond_3
    new-instance v0, Lcom/osp/app/signin/ft;

    invoke-direct {v0, p0, p1, p3}, Lcom/osp/app/signin/ft;-><init>(Lcom/osp/app/signin/InterfaceService;Landroid/content/Intent;I)V

    .line 150
    new-array v1, v7, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/ft;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 155
    :cond_4
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-static {p0, v7}, Lcom/msc/c/c;->a(Landroid/content/Context;Z)Z

    move-result v4

    if-nez v4, :cond_7

    .line 157
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 164
    const-string v5, "com.msc.action.ACCESSTOKEN_V02_REQUEST"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 166
    const-string v5, "com.msc.action.ACCESSTOKEN_V02_RESPONSE"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 171
    :goto_1
    const-string v5, "result_code"

    invoke-virtual {v4, v5, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 172
    const-string v5, "error_message"

    const-string v6, "Invalid DB"

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 173
    if-eqz v1, :cond_5

    .line 175
    invoke-virtual {v4, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 179
    const-string v1, "client_id"

    const-string v5, "client_id"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 181
    invoke-static {}, Lcom/osp/app/util/e;->a()Lcom/osp/app/util/e;

    move-result-object v1

    invoke-virtual {v1, p0, v4, v2, v3}, Lcom/osp/app/util/e;->a(Landroid/content/Context;Landroid/content/Intent;J)V

    .line 184
    :cond_5
    const-string v1, "client_id"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "client_secret"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2, v0, v7}, Lcom/osp/app/signin/InterfaceService;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 169
    :cond_6
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v5, "Broadcast Action is wrong"

    invoke-static {v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 197
    :cond_7
    const-string v0, "com.msc.action.ACCESSTOKEN_V02_REQUEST"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 199
    new-instance v0, Lcom/osp/app/signin/fr;

    invoke-direct {v0, p0, p1, p3}, Lcom/osp/app/signin/fr;-><init>(Lcom/osp/app/signin/InterfaceService;Landroid/content/Intent;I)V

    .line 200
    new-array v1, v7, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/fr;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 201
    iget-object v1, p0, Lcom/osp/app/signin/InterfaceService;->b:Ljava/util/HashMap;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 204
    :cond_8
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Broadcast Action is wrong"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public sendBroadcast(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 102
    invoke-super {p0, p1}, Lcom/osp/app/util/AbstractBaseService;->sendBroadcast(Landroid/content/Intent;)V

    .line 104
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {p1}, Lcom/osp/app/util/aq;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 105
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Target package=["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 106
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIS"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendBroadcast Intent=["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    return-void
.end method
