.class final Lcom/osp/app/signin/dx;
.super Lcom/msc/c/b;
.source "CheckSecurityInfoActivity.java"


# instance fields
.field protected c:Ljava/lang/Runnable;

.field protected d:Ljava/lang/Runnable;

.field protected e:Ljava/lang/Thread;

.field protected f:Ljava/lang/Thread;

.field final synthetic g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Ljava/lang/String;

.field private m:Lcom/msc/a/b;

.field private n:J

.field private o:J

.field private p:J

.field private q:Z

.field private r:Lcom/msc/a/d;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:J


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/CheckSecurityInfoActivity;Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1740
    iput-object p1, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    .line 1741
    const v0, 0x7f090055

    invoke-direct {p0, p2, v0}, Lcom/msc/c/b;-><init>(Landroid/content/Context;I)V

    .line 1654
    iput-boolean v1, p0, Lcom/osp/app/signin/dx;->h:Z

    .line 1659
    iput-boolean v1, p0, Lcom/osp/app/signin/dx;->i:Z

    .line 1664
    iput-boolean v1, p0, Lcom/osp/app/signin/dx;->j:Z

    .line 1669
    iput-boolean v1, p0, Lcom/osp/app/signin/dx;->k:Z

    .line 1713
    iput-boolean v1, p0, Lcom/osp/app/signin/dx;->q:Z

    .line 1743
    new-instance v0, Lcom/osp/app/signin/dy;

    invoke-direct {v0, p0, p1, p2}, Lcom/osp/app/signin/dy;-><init>(Lcom/osp/app/signin/dx;Lcom/osp/app/signin/CheckSecurityInfoActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/app/signin/dx;->c:Ljava/lang/Runnable;

    .line 1766
    new-instance v0, Lcom/osp/app/signin/dz;

    invoke-direct {v0, p0, p1}, Lcom/osp/app/signin/dz;-><init>(Lcom/osp/app/signin/dx;Lcom/osp/app/signin/CheckSecurityInfoActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/dx;->d:Ljava/lang/Runnable;

    .line 1784
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/osp/app/signin/dx;->c:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/osp/app/signin/dx;->e:Ljava/lang/Thread;

    .line 1785
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/osp/app/signin/dx;->d:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/osp/app/signin/dx;->f:Ljava/lang/Thread;

    .line 1789
    return-void
.end method

.method private a(Lcom/msc/a/a;)V
    .locals 3

    .prologue
    .line 2176
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 2177
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    iget-object v1, p0, Lcom/osp/app/signin/dx;->t:Ljava/lang/String;

    invoke-static {v0, p1, v1, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/a/a;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/dx;->n:J

    .line 2179
    iget-wide v0, p0, Lcom/osp/app/signin/dx;->n:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/dx;->a(J)V

    .line 2180
    iget-wide v0, p0, Lcom/osp/app/signin/dx;->n:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/dx;->a(JLjava/lang/String;)V

    .line 2182
    iget-wide v0, p0, Lcom/osp/app/signin/dx;->n:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 2183
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/dx;Lcom/msc/a/a;)V
    .locals 0

    .prologue
    .line 1629
    invoke-direct {p0, p1}, Lcom/osp/app/signin/dx;->a(Lcom/msc/a/a;)V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/dx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1629
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p0

    invoke-static/range {v0 .. v7}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/osp/app/signin/cn;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/dx;->o:J

    iget-wide v0, p0, Lcom/osp/app/signin/dx;->o:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/dx;->b(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/dx;->o:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/dx;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/dx;->o:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    return-void
.end method

.method private static a(Ljava/lang/Thread;)V
    .locals 2

    .prologue
    .line 1806
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1810
    const-wide/16 v0, 0x12c

    :try_start_0
    invoke-virtual {p0, v0, v1}, Ljava/lang/Thread;->join(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1815
    :cond_0
    :goto_0
    return-void

    .line 1811
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/osp/app/signin/dx;)Z
    .locals 1

    .prologue
    .line 1629
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/dx;->h:Z

    return v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 2274
    const-string v0, "Success"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2276
    const/4 v0, 0x1

    .line 2279
    iget-object v1, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 2280
    const-string v2, "FLAG_DO_NOT_SHOW_NOTIFICATION"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 2281
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2283
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "===========================================SignIn_End PROCESSING_SUCCESS==========================================="

    invoke-static {v1}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 2284
    iget-object v1, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v1}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2286
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    iget-object v1, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->l(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 2300
    :goto_0
    if-nez v0, :cond_1

    .line 2302
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CSI"

    const-string v1, "CheckSecurityInfo SignInNewDualEnd"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2303
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "ERR_MDM_SECURITY"

    const-string v2, "fail_MDM_Security"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/dx;->b:Lcom/msc/c/f;

    .line 2304
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->b(I)V

    .line 2305
    const-string v0, "Fail"

    invoke-direct {p0, v0}, Lcom/osp/app/signin/dx;->b(Ljava/lang/String;)V

    .line 2306
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-virtual {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->finish()V

    .line 2509
    :goto_1
    return-void

    .line 2290
    :cond_0
    iget-object v1, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v1}, Lcom/osp/app/util/ad;->j(Landroid/content/Context;)V

    goto :goto_0

    .line 2317
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    sget-object v1, Lcom/msc/c/d;->c:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/dx;->m:Lcom/msc/a/b;

    invoke-virtual {v2}, Lcom/msc/a/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2318
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    sget-object v1, Lcom/msc/c/d;->d:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/dx;->m:Lcom/msc/a/b;

    invoke-virtual {v2}, Lcom/msc/a/b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2319
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    sget-object v1, Lcom/msc/c/d;->a:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/dx;->m:Lcom/msc/a/b;

    invoke-virtual {v2}, Lcom/msc/a/b;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2320
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    sget-object v1, Lcom/msc/c/d;->f:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/dx;->m:Lcom/msc/a/b;

    invoke-virtual {v2}, Lcom/msc/a/b;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2321
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    sget-object v1, Lcom/msc/c/d;->b:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v2}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->l(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2324
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    sget-object v1, Lcom/msc/c/d;->h:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v2}, Lcom/osp/common/util/i;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2326
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    sget-object v1, Lcom/msc/c/d;->i:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/osp/common/util/h;->a()Lcom/osp/common/util/h;

    iget-object v2, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v2}, Lcom/osp/common/util/h;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2328
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    sget-object v1, Lcom/msc/c/d;->g:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/dx;->m:Lcom/msc/a/b;

    invoke-virtual {v2}, Lcom/msc/a/b;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2329
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    const-string v1, "j5p7ll8g33"

    invoke-static {v0, v1}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2330
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    const-string v1, "j5p7ll8g33"

    const-string v2, "5763D0052DC1462E13751F753384E9A9"

    iget-object v3, p0, Lcom/osp/app/signin/dx;->m:Lcom/msc/a/b;

    invoke-virtual {v3}, Lcom/msc/a/b;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/dx;->m:Lcom/msc/a/b;

    invoke-virtual {v4}, Lcom/msc/a/b;->f()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2337
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    iget-object v1, p0, Lcom/osp/app/signin/dx;->s:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/msc/c/e;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 2338
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    iget-object v1, p0, Lcom/osp/app/signin/dx;->t:Ljava/lang/String;

    iget-object v2, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v2}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->l(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/dx;->m:Lcom/msc/a/b;

    invoke-virtual {v3}, Lcom/msc/a/b;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2348
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->n(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v0, "j5p7ll8g33"

    iget-object v1, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->n(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/dx;->r:Lcom/msc/a/d;

    invoke-virtual {v0}, Lcom/msc/a/d;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/dx;->r:Lcom/msc/a/d;

    invoke-virtual {v0}, Lcom/msc/a/d;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2352
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    iget-object v1, p0, Lcom/osp/app/signin/dx;->r:Lcom/msc/a/d;

    invoke-virtual {v1}, Lcom/msc/a/d;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 2361
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->r(Lcom/osp/app/signin/CheckSecurityInfoActivity;)V

    .line 2363
    invoke-static {}, Lcom/osp/app/pushmarketing/k;->a()Lcom/osp/app/pushmarketing/k;

    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/k;->c(Landroid/content/Context;)V

    .line 2367
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->b(I)V

    .line 2374
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    const/16 v1, 0x66

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    iget-object v3, v3, Lcom/osp/app/signin/CheckSecurityInfoActivity;->e:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1, v2, v3}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->a(ILandroid/content/DialogInterface$OnDismissListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 2507
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CSI"

    const-string v1, "SignInDual_End"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2332
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 2353
    :cond_4
    const-string v0, "tj9u972o46"

    iget-object v1, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->n(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2356
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    iget-object v1, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->n(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/dx;->r:Lcom/msc/a/d;

    invoke-virtual {v2}, Lcom/msc/a/d;->g()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/dx;->r:Lcom/msc/a/d;

    invoke-virtual {v3}, Lcom/msc/a/d;->j()J

    move-result-wide v3

    iget-object v5, p0, Lcom/osp/app/signin/dx;->r:Lcom/msc/a/d;

    invoke-virtual {v5}, Lcom/msc/a/d;->h()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/osp/app/signin/dx;->r:Lcom/msc/a/d;

    invoke-virtual {v6}, Lcom/msc/a/d;->k()J

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;J)V

    goto :goto_3

    .line 2446
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-virtual {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->c()V

    .line 2493
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2495
    iget-boolean v0, p0, Lcom/osp/app/signin/dx;->q:Z

    if-nez v0, :cond_7

    .line 2497
    :cond_6
    invoke-virtual {p0, v3}, Lcom/osp/app/signin/dx;->a(Z)V

    .line 2504
    :cond_7
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CSI"

    const-string v1, "SignInDual_End"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method static synthetic b(Lcom/osp/app/signin/dx;)Z
    .locals 1

    .prologue
    .line 1629
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/dx;->i:Z

    return v0
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1832
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->l(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->m(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1834
    :cond_0
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1865
    :goto_0
    return-object v0

    .line 1839
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->p(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1841
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    iget-object v1, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->l(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v2}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->m(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3, p0}, Lcom/msc/c/g;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/osp/app/signin/cn;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/dx;->u:J

    iget-wide v0, p0, Lcom/osp/app/signin/dx;->u:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/dx;->b(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/dx;->u:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/dx;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/dx;->u:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 1865
    :goto_1
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 1845
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/dx;->e:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1846
    iget-object v0, p0, Lcom/osp/app/signin/dx;->f:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1848
    :cond_3
    iget-boolean v0, p0, Lcom/osp/app/signin/dx;->h:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/osp/app/signin/dx;->i:Z

    if-nez v0, :cond_5

    .line 1850
    :cond_4
    invoke-virtual {p0}, Lcom/osp/app/signin/dx;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1852
    :cond_5
    iget-boolean v0, p0, Lcom/osp/app/signin/dx;->k:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/osp/app/signin/dx;->j:Z

    if-eqz v0, :cond_6

    .line 1858
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/dx;->l:Ljava/lang/String;

    goto :goto_1

    .line 1861
    :cond_6
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/dx;->l:Ljava/lang/String;

    goto :goto_1
.end method

.method public final a(Lcom/msc/a/f;)V
    .locals 3

    .prologue
    .line 2236
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CSI"

    const-string v1, "RequestCheckListInfo"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2238
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 2239
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0, p1, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/a/f;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/dx;->p:J

    .line 2240
    iget-wide v0, p0, Lcom/osp/app/signin/dx;->p:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/dx;->c(J)V

    .line 2241
    iget-wide v0, p0, Lcom/osp/app/signin/dx;->p:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/dx;->a(JLjava/lang/String;)V

    .line 2243
    iget-wide v0, p0, Lcom/osp/app/signin/dx;->p:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 2245
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CSI"

    const-string v1, "RequestCheckListInfo"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2246
    return-void
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 1927
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1929
    if-nez p1, :cond_1

    .line 2087
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1934
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 1935
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 1937
    iget-wide v4, p0, Lcom/osp/app/signin/dx;->n:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_6

    .line 1941
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->r(Ljava/lang/String;)Lcom/msc/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/dx;->m:Lcom/msc/a/b;

    .line 1944
    new-instance v0, Lcom/osp/common/property/a;

    iget-object v1, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-direct {v0, v1}, Lcom/osp/common/property/a;-><init>(Landroid/content/Context;)V

    .line 1945
    const-string v1, "device.registration.appid"

    invoke-virtual {v0, v1}, Lcom/osp/common/property/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1947
    const-string v1, "device.registration.appid"

    invoke-virtual {v0, v1}, Lcom/osp/common/property/a;->c(Ljava/lang/String;)V

    .line 1949
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "CSI"

    const-string v3, "save appid in property."

    invoke-static {v1, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1950
    const-string v1, "device.registration.appid"

    const-string v3, "j5p7ll8g33"

    invoke-virtual {v0, v1, v3}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1952
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->p(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1954
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/dx;->l:Ljava/lang/String;

    .line 1956
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v2}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1965
    :try_start_3
    iget-object v0, p0, Lcom/osp/app/signin/dx;->m:Lcom/msc/a/b;

    if-eqz v0, :cond_5

    .line 1967
    iget-object v0, p0, Lcom/osp/app/signin/dx;->m:Lcom/msc/a/b;

    invoke-virtual {v0}, Lcom/msc/a/b;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/dx;->m:Lcom/msc/a/b;

    invoke-virtual {v0}, Lcom/msc/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 1969
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/dx;->k:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1927
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1957
    :catch_0
    move-exception v0

    .line 1959
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1960
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/osp/app/signin/dx;->k:Z

    .line 1961
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/dx;->b:Lcom/msc/c/f;

    goto :goto_0

    .line 1972
    :cond_4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/dx;->k:Z

    .line 1973
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/dx;->b:Lcom/msc/c/f;

    .line 1974
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->b(I)V

    goto/16 :goto_0

    .line 1978
    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/dx;->k:Z

    .line 1979
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/dx;->b:Lcom/msc/c/f;

    .line 1980
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->b(I)V

    goto/16 :goto_0

    .line 1983
    :cond_6
    iget-wide v4, p0, Lcom/osp/app/signin/dx;->o:J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_8

    .line 1987
    :try_start_5
    new-instance v0, Lcom/msc/a/d;

    invoke-direct {v0}, Lcom/msc/a/d;-><init>()V

    .line 1988
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2, v0}, Lcom/msc/c/h;->a(Ljava/lang/String;Lcom/msc/a/d;)V

    .line 1989
    invoke-virtual {v0}, Lcom/msc/a/d;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/dx;->s:Ljava/lang/String;

    .line 1990
    invoke-virtual {v0}, Lcom/msc/a/d;->i()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/dx;->t:Ljava/lang/String;

    .line 1991
    iput-object v0, p0, Lcom/osp/app/signin/dx;->r:Lcom/msc/a/d;

    .line 1993
    iget-object v0, p0, Lcom/osp/app/signin/dx;->t:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/dx;->s:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/dx;->r:Lcom/msc/a/d;

    invoke-virtual {v0}, Lcom/msc/a/d;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1995
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/dx;->j:Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 2001
    :catch_1
    move-exception v0

    .line 2003
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2004
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/dx;->b:Lcom/msc/c/f;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 1998
    :cond_7
    :try_start_7
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "ERR_0000"

    const-string v2, "fail"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/dx;->b:Lcom/msc/c/f;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .line 2006
    :cond_8
    :try_start_8
    iget-wide v4, p0, Lcom/osp/app/signin/dx;->p:J
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_9

    .line 2010
    :try_start_9
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0, v2}, Lcom/msc/c/h;->b(Landroid/content/Context;Ljava/lang/String;)Lcom/msc/a/g;

    move-result-object v0

    .line 2011
    if-eqz v0, :cond_0

    .line 2013
    iget-object v1, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-virtual {v0}, Lcom/msc/a/g;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/msc/c/e;->d(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    .line 2016
    :catch_2
    move-exception v0

    :try_start_a
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 2020
    :cond_9
    iget-wide v4, p0, Lcom/osp/app/signin/dx;->u:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 2023
    new-instance v0, Lcom/msc/a/d;

    invoke-direct {v0}, Lcom/msc/a/d;-><init>()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 2026
    :try_start_b
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2, v0}, Lcom/msc/c/h;->a(Ljava/lang/String;Lcom/msc/a/d;)V

    .line 2027
    invoke-virtual {v0}, Lcom/msc/a/d;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/dx;->s:Ljava/lang/String;

    .line 2028
    invoke-virtual {v0}, Lcom/msc/a/d;->i()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/dx;->t:Ljava/lang/String;

    .line 2029
    iput-object v0, p0, Lcom/osp/app/signin/dx;->r:Lcom/msc/a/d;

    .line 2031
    iget-object v0, p0, Lcom/osp/app/signin/dx;->t:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/osp/app/signin/dx;->s:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 2033
    new-instance v0, Lcom/msc/a/a;

    invoke-direct {v0}, Lcom/msc/a/a;-><init>()V

    .line 2034
    iget-object v1, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    iget-object v2, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v2}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->l(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v3}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->m(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/msc/a/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2036
    invoke-direct {p0, v0}, Lcom/osp/app/signin/dx;->a(Lcom/msc/a/a;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_0

    .line 2042
    :catch_3
    move-exception v0

    :try_start_c
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_0

    .line 2039
    :cond_a
    :try_start_d
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "ERR_0000"

    const-string v2, "fail"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/dx;->b:Lcom/msc/c/f;
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_3
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1870
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 1889
    iget-object v0, p0, Lcom/osp/app/signin/dx;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_5

    .line 1891
    const-string v0, "SOCKET_TIMEOUT_EXCEPTION"

    iget-object v1, p0, Lcom/osp/app/signin/dx;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "CONNECT_TIMEOUT_EXCEPTION"

    iget-object v1, p0, Lcom/osp/app/signin/dx;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1893
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->b(I)V

    .line 1900
    :goto_0
    iget-boolean v0, p0, Lcom/osp/app/signin/dx;->q:Z

    if-eqz v0, :cond_1

    .line 1902
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->q(Lcom/osp/app/signin/CheckSecurityInfoActivity;)V

    .line 1905
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->p(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/osp/app/signin/gv;->a()Lcom/osp/app/signin/gs;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/gs;->a(Landroid/content/Context;Lcom/osp/app/signin/gu;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1923
    :goto_1
    return-void

    .line 1896
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->b(I)V

    goto :goto_0

    .line 1911
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->p(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "AUT_1885"

    iget-object v1, p0, Lcom/osp/app/signin/dx;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1913
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    const/16 v1, 0x67

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->a(ILandroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_1

    .line 1917
    :cond_4
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/dx;->a(Z)V

    goto :goto_1

    .line 1921
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/dx;->l:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/osp/app/signin/dx;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 2091
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 2093
    if-nez p1, :cond_1

    .line 2166
    :cond_0
    :goto_0
    return-void

    .line 2098
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 2099
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 2100
    invoke-virtual {p1}, Lcom/msc/b/c;->f()Ljava/lang/Exception;

    move-result-object v3

    .line 2102
    iget-wide v4, p0, Lcom/osp/app/signin/dx;->n:J

    cmp-long v4, v0, v4

    if-nez v4, :cond_4

    .line 2104
    if-eqz v3, :cond_2

    .line 2106
    iget-object v0, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-virtual {v0, v6}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->b(I)V

    goto :goto_0

    .line 2107
    :cond_2
    if-eqz v2, :cond_0

    .line 2110
    iget-object v0, p0, Lcom/osp/app/signin/dx;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v0

    .line 2112
    const-string v1, "SSO_8005"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v6, :cond_3

    .line 2116
    :try_start_0
    new-instance v0, Lcom/osp/security/time/a;

    iget-object v1, p0, Lcom/osp/app/signin/dx;->g:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-virtual {v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/osp/security/time/a;-><init>(Landroid/content/Context;)V

    .line 2117
    invoke-virtual {v0}, Lcom/osp/security/time/a;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2118
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 2122
    :cond_3
    const-string v1, "SSO_2204"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v6, :cond_0

    .line 2124
    iput-boolean v6, p0, Lcom/osp/app/signin/dx;->q:Z

    goto :goto_0

    .line 2134
    :cond_4
    iget-wide v4, p0, Lcom/osp/app/signin/dx;->o:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_5

    .line 2136
    const-string v0, "AUT_1885"

    iget-object v1, p0, Lcom/osp/app/signin/dx;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2138
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CSI"

    const-string v1, "PhoneID is Blocked (login failed 10times)"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2141
    :cond_5
    iget-wide v4, p0, Lcom/osp/app/signin/dx;->u:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 2143
    if-eqz v2, :cond_0

    .line 2145
    const-string v0, "AUT_1805"

    iget-object v1, p0, Lcom/osp/app/signin/dx;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2147
    iget-object v0, p0, Lcom/osp/app/signin/dx;->b:Lcom/msc/c/f;

    const-string v1, "AUT_1805"

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2148
    :cond_6
    const-string v0, "AUT_1815"

    iget-object v1, p0, Lcom/osp/app/signin/dx;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2152
    invoke-static {}, Lcom/osp/app/signin/gv;->a()Lcom/osp/app/signin/gs;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/gs;->a()V

    goto/16 :goto_0

    .line 2155
    :cond_7
    const-string v0, "AUT_1093"

    iget-object v1, p0, Lcom/osp/app/signin/dx;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "AUT_1092"

    iget-object v1, p0, Lcom/osp/app/signin/dx;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method protected final d()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1793
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 1794
    iget-object v0, p0, Lcom/osp/app/signin/dx;->e:Ljava/lang/Thread;

    invoke-static {v0}, Lcom/osp/app/signin/dx;->a(Ljava/lang/Thread;)V

    .line 1795
    iget-object v0, p0, Lcom/osp/app/signin/dx;->f:Ljava/lang/Thread;

    invoke-static {v0}, Lcom/osp/app/signin/dx;->a(Ljava/lang/Thread;)V

    .line 1796
    iput-boolean v1, p0, Lcom/osp/app/signin/dx;->h:Z

    .line 1797
    iput-boolean v1, p0, Lcom/osp/app/signin/dx;->i:Z

    .line 1798
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1629
    invoke-virtual {p0}, Lcom/osp/app/signin/dx;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1629
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/dx;->a(Ljava/lang/Boolean;)V

    return-void
.end method
