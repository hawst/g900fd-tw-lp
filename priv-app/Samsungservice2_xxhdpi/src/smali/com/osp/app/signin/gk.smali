.class final Lcom/osp/app/signin/gk;
.super Ljava/lang/Object;
.source "PasswordChangeView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Landroid/widget/EditText;

.field final synthetic b:Lcom/osp/app/signin/PasswordChangeView;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/PasswordChangeView;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/osp/app/signin/gk;->b:Lcom/osp/app/signin/PasswordChangeView;

    iput-object p2, p0, Lcom/osp/app/signin/gk;->a:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 192
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 196
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 200
    iget-object v0, p0, Lcom/osp/app/signin/gk;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 217
    iget-object v0, p0, Lcom/osp/app/signin/gk;->b:Lcom/osp/app/signin/PasswordChangeView;

    const v1, 0x7f0900c8

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/16 v4, 0x8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xf

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/PasswordChangeView;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 237
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v1, p0, Lcom/osp/app/signin/gk;->b:Lcom/osp/app/signin/PasswordChangeView;

    invoke-static {v1, v0, v5}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 240
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/gk;->b:Lcom/osp/app/signin/PasswordChangeView;

    invoke-virtual {v0}, Lcom/osp/app/signin/PasswordChangeView;->a()V

    .line 241
    return-void
.end method
