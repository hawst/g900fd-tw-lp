.class public final Lcom/osp/social/member/d;
.super Ljava/lang/Object;
.source "MemberServiceManager.java"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/osp/social/member/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lcom/osp/social/member/d;->c:Landroid/content/Context;

    .line 77
    new-instance v0, Lcom/osp/social/member/e;

    invoke-direct {v0, p1}, Lcom/osp/social/member/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/social/member/d;->d:Lcom/osp/social/member/e;

    .line 84
    const-string v0, "14eev3f64b"

    iput-object v0, p0, Lcom/osp/social/member/d;->a:Ljava/lang/String;

    .line 85
    const-string v0, "109E2830E09DB340924B8ABE0D6290C3"

    iput-object v0, p0, Lcom/osp/social/member/d;->b:Ljava/lang/String;

    .line 95
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/osp/social/member/d;->d:Lcom/osp/social/member/e;

    invoke-virtual {v0}, Lcom/osp/social/member/e;->a()V

    .line 241
    return-void
.end method

.method public final a(Lcom/osp/social/member/a;)V
    .locals 1

    .prologue
    .line 114
    invoke-virtual {p1}, Lcom/osp/social/member/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/osp/social/member/d;->b(Ljava/lang/String;)V

    .line 115
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 226
    if-nez p1, :cond_0

    .line 228
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/osp/social/member/d;->d:Lcom/osp/social/member/e;

    invoke-virtual {v0, p1}, Lcom/osp/social/member/e;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 250
    if-nez p1, :cond_0

    .line 252
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 255
    :cond_0
    iget-object v0, p0, Lcom/osp/social/member/d;->d:Lcom/osp/social/member/e;

    invoke-virtual {v0, p1}, Lcom/osp/social/member/e;->b(Ljava/lang/String;)V

    .line 256
    return-void
.end method
