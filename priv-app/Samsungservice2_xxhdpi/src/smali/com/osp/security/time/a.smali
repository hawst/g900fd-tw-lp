.class public final Lcom/osp/security/time/a;
.super Ljava/lang/Object;
.source "ServerTimeManager.java"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private c:J

.field private final d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/osp/security/time/a;->c:J

    .line 70
    iput-object p1, p0, Lcom/osp/security/time/a;->d:Landroid/content/Context;

    .line 77
    const-string v0, "14eev3f64b"

    iput-object v0, p0, Lcom/osp/security/time/a;->a:Ljava/lang/String;

    .line 78
    const-string v0, "109E2830E09DB340924B8ABE0D6290C3"

    iput-object v0, p0, Lcom/osp/security/time/a;->b:Ljava/lang/String;

    .line 86
    return-void
.end method

.method private a(J)V
    .locals 7

    .prologue
    const-wide/16 v0, 0x0

    .line 213
    .line 218
    cmp-long v2, p1, v0

    if-eqz v2, :cond_0

    .line 220
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 221
    sub-long v2, p1, v0

    iput-wide v2, p0, Lcom/osp/security/time/a;->c:J

    .line 229
    :goto_0
    new-instance v2, Lcom/osp/common/property/a;

    iget-object v3, p0, Lcom/osp/security/time/a;->d:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/osp/common/property/a;-><init>(Landroid/content/Context;)V

    .line 231
    const-string v3, "ServerTime.settingTime"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    const-string v0, "ServerTime.offsetTime"

    iget-wide v4, p0, Lcom/osp/security/time/a;->c:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    return-void

    .line 225
    :cond_0
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/osp/security/time/a;->c:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 233
    :catch_0
    move-exception v0

    .line 235
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 236
    new-instance v1, Lcom/osp/security/time/ServerTimeException;

    const-string v2, "Can\'t set the Sever Time."

    invoke-direct {v1, v2, v0}, Lcom/osp/security/time/ServerTimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a()J
    .locals 11

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 94
    .line 97
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v6, 0x3e8

    div-long/2addr v2, v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5

    .line 102
    :try_start_1
    new-instance v0, Lcom/osp/common/property/a;

    iget-object v6, p0, Lcom/osp/security/time/a;->d:Landroid/content/Context;

    invoke-direct {v0, v6}, Lcom/osp/common/property/a;-><init>(Landroid/content/Context;)V

    .line 106
    const-string v6, "ServerTime.settingTime"

    const-string v7, "0"

    invoke-virtual {v0, v6, v7}, Lcom/osp/common/property/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 107
    const-string v8, "ServerTime.offsetTime"

    const-string v9, "0"

    invoke-virtual {v0, v8, v9}, Lcom/osp/common/property/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/osp/security/time/a;->c:J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 109
    cmp-long v0, v6, v4

    if-eqz v0, :cond_0

    const-wide/32 v4, 0x15180

    add-long/2addr v4, v6

    cmp-long v0, v4, v2

    if-gez v0, :cond_1

    .line 111
    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/osp/security/time/a;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/msc/c/l;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Lcom/osp/http/impl/d;

    iget-object v5, p0, Lcom/osp/security/time/a;->d:Landroid/content/Context;

    iget-object v6, p0, Lcom/osp/security/time/a;->a:Ljava/lang/String;

    iget-object v7, p0, Lcom/osp/security/time/a;->b:Ljava/lang/String;

    invoke-direct {v4, v5, v6, v7}, Lcom/osp/http/impl/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v5, Lcom/osp/http/impl/f;->b:Lcom/osp/http/impl/f;

    const-string v6, ""

    sget-object v7, Lcom/osp/common/util/g;->a:Lcom/osp/common/util/g;

    invoke-virtual {v4, v5, v0, v6, v7}, Lcom/osp/http/impl/d;->a(Lcom/osp/http/impl/f;Ljava/lang/String;Ljava/lang/String;Lcom/osp/common/util/g;)Lorg/apache/http/HttpResponse;

    move-result-object v6

    const/4 v4, 0x0

    if-eqz v6, :cond_7

    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    if-eqz v0, :cond_6

    :try_start_3
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v5

    :goto_0
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    move-object v4, v5

    :goto_1
    const/16 v5, 0xc8

    if-ne v1, v5, :cond_3

    if-nez v4, :cond_2

    new-instance v1, Ljava/lang/Exception;

    const-string v4, "Response content is null."

    invoke-direct {v1, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catch_0
    move-exception v1

    :goto_2
    const-wide/16 v4, 0x0

    :try_start_4
    invoke-direct {p0, v4, v5}, Lcom/osp/security/time/a;->a(J)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v0, :cond_1

    :try_start_5
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    .line 114
    :cond_1
    :goto_3
    :try_start_6
    iget-wide v0, p0, Lcom/osp/security/time/a;->c:J
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    add-long/2addr v0, v2

    .line 120
    :goto_4
    return-wide v0

    .line 111
    :cond_2
    :try_start_7
    new-instance v1, Lcom/osp/security/time/b;

    invoke-direct {v1}, Lcom/osp/security/time/b;-><init>()V

    invoke-virtual {v1, v4}, Lcom/osp/security/time/b;->a(Ljava/io/InputStream;)V

    invoke-virtual {v1}, Lcom/osp/security/time/b;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-direct {p0, v4, v5}, Lcom/osp/security/time/a;->a(J)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :goto_5
    if-eqz v0, :cond_1

    :try_start_8
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    goto :goto_3

    :catch_1
    move-exception v0

    :try_start_9
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2

    goto :goto_3

    .line 117
    :catch_2
    move-exception v0

    move-wide v0, v2

    goto :goto_4

    .line 111
    :cond_3
    :try_start_a
    new-instance v1, Lcom/osp/http/impl/b;

    iget-object v5, p0, Lcom/osp/security/time/a;->d:Landroid/content/Context;

    invoke-direct {v1, v5}, Lcom/osp/http/impl/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v4}, Lcom/osp/http/impl/b;->a(Ljava/io/InputStream;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_5

    :catchall_0
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    :goto_6
    if-eqz v1, :cond_4

    :try_start_b
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_2

    :cond_4
    :goto_7
    :try_start_c
    throw v0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    :catch_4
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_2

    goto :goto_7

    .line 117
    :catch_5
    move-exception v0

    move-wide v0, v4

    goto :goto_4

    .line 111
    :catchall_1
    move-exception v0

    goto :goto_6

    :catch_6
    move-exception v0

    move-object v0, v1

    goto :goto_2

    :cond_5
    move v1, v4

    move-object v4, v5

    goto :goto_1

    :cond_6
    move-object v5, v1

    goto :goto_0

    :cond_7
    move-object v0, v1

    move-object v10, v1

    move v1, v4

    move-object v4, v10

    goto :goto_1
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 129
    :try_start_0
    new-instance v0, Lcom/osp/common/property/a;

    iget-object v1, p0, Lcom/osp/security/time/a;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/osp/common/property/a;-><init>(Landroid/content/Context;)V

    .line 131
    const-string v1, "ServerTime.settingTime"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const-string v1, "ServerTime.offsetTime"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    return-void

    .line 133
    :catch_0
    move-exception v0

    .line 135
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 136
    new-instance v1, Lcom/osp/security/time/ServerTimeException;

    const-string v2, "Can\'t set the Sever Time."

    invoke-direct {v1, v2, v0}, Lcom/osp/security/time/ServerTimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
