.class public final Lcom/osp/security/credential/a;
.super Ljava/lang/Object;
.source "CredentialManager.java"


# instance fields
.field private a:Lcom/osp/security/credential/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    :try_start_0
    new-instance v0, Lcom/osp/security/credential/b;

    invoke-direct {v0, p1}, Lcom/osp/security/credential/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/security/credential/a;->a:Lcom/osp/security/credential/b;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    return-void

    .line 45
    :catch_0
    move-exception v0

    .line 47
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 48
    new-instance v1, Lcom/osp/security/credential/CredentialException;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/osp/security/credential/CredentialException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    if-nez p1, :cond_0

    .line 76
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/osp/security/credential/a;->a:Lcom/osp/security/credential/b;

    invoke-virtual {v0, p1}, Lcom/osp/security/credential/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/osp/security/credential/a;->a:Lcom/osp/security/credential/b;

    invoke-virtual {v0}, Lcom/osp/security/credential/b;->a()V

    .line 153
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 105
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 107
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 110
    :cond_1
    iget-object v0, p0, Lcom/osp/security/credential/a;->a:Lcom/osp/security/credential/b;

    invoke-virtual {v0, p1, p2}, Lcom/osp/security/credential/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 122
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 124
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 127
    :cond_1
    iget-object v0, p0, Lcom/osp/security/credential/a;->a:Lcom/osp/security/credential/b;

    invoke-virtual {v0, p1, p2, p3}, Lcom/osp/security/credential/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    return-void
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    if-nez p1, :cond_0

    .line 91
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/osp/security/credential/a;->a:Lcom/osp/security/credential/b;

    invoke-virtual {v0, p1}, Lcom/osp/security/credential/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 138
    if-nez p1, :cond_0

    .line 140
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/osp/security/credential/a;->a:Lcom/osp/security/credential/b;

    invoke-virtual {v0, p1}, Lcom/osp/security/credential/b;->c(Ljava/lang/String;)V

    .line 144
    return-void
.end method

.method public final d(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 164
    if-nez p1, :cond_0

    .line 166
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 169
    :cond_0
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/osp/security/credential/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 177
    invoke-virtual {p0, p1}, Lcom/osp/security/credential/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 179
    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    const-string v3, ""

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eq v1, v0, :cond_1

    const-string v1, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-ne v1, v0, :cond_2

    .line 181
    :cond_1
    const/4 v0, 0x0

    .line 191
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "CM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "containsAccessToken = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    return v0

    .line 186
    :catch_0
    move-exception v0

    .line 188
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 189
    new-instance v1, Lcom/osp/security/credential/CredentialException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/osp/security/credential/CredentialException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
