.class public final Lcom/osp/security/credential/b;
.super Ljava/lang/Object;
.source "CredentialRepository.java"


# static fields
.field private static final a:Landroid/net/Uri;


# instance fields
.field private final b:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/osp/contentprovider/c;->a:Landroid/net/Uri;

    sput-object v0, Lcom/osp/security/credential/b;->a:Landroid/net/Uri;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/osp/security/credential/b;->b:Landroid/content/Context;

    .line 48
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 171
    .line 176
    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p2, v2, v0

    .line 177
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    .line 180
    iget-object v0, p0, Lcom/osp/security/credential/b;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/osp/security/credential/b;->a:Landroid/net/Uri;

    const-string v3, "appID = ?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 182
    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    .line 191
    :cond_0
    if-eqz v1, :cond_1

    .line 193
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 197
    :cond_1
    return-object v6

    .line 186
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 188
    :goto_0
    :try_start_2
    new-instance v2, Lcom/osp/security/credential/CredentialException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/osp/security/credential/CredentialException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 191
    :catchall_0
    move-exception v0

    move-object v6, v1

    :goto_1
    if-eqz v6, :cond_2

    .line 193
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 191
    :cond_2
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1

    .line 186
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private d(Ljava/lang/String;)Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 58
    .line 63
    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "appID"

    aput-object v1, v2, v0

    .line 64
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    .line 67
    iget-object v0, p0, Lcom/osp/security/credential/b;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/osp/security/credential/b;->a:Landroid/net/Uri;

    const-string v3, "appID = ?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 69
    if-eqz v1, :cond_3

    .line 71
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    move v0, v6

    .line 78
    :goto_0
    if-eqz v1, :cond_0

    .line 80
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 84
    :cond_0
    return v0

    :cond_1
    move v0, v7

    .line 71
    goto :goto_0

    .line 73
    :catch_0
    move-exception v0

    move-object v1, v8

    .line 75
    :goto_1
    :try_start_2
    new-instance v2, Lcom/osp/security/credential/CredentialException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/osp/security/credential/CredentialException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 78
    :catchall_0
    move-exception v0

    move-object v8, v1

    :goto_2
    if-eqz v8, :cond_2

    .line 80
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 78
    :cond_2
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    .line 73
    :catch_1
    move-exception v0

    goto :goto_1

    :cond_3
    move v0, v6

    goto :goto_0
.end method


# virtual methods
.method protected final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 122
    :try_start_0
    const-string v0, "oauthToken"

    invoke-direct {p0, p1, v0}, Lcom/osp/security/credential/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 132
    return-object v0

    .line 127
    :catch_0
    move-exception v0

    .line 129
    new-instance v1, Lcom/osp/security/credential/CredentialException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/osp/security/credential/CredentialException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected final a()V
    .locals 4

    .prologue
    .line 296
    iget-object v0, p0, Lcom/osp/security/credential/b;->b:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 300
    :try_start_0
    iget-object v0, p0, Lcom/osp/security/credential/b;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 301
    if-eqz v0, :cond_0

    .line 303
    sget-object v1, Lcom/osp/security/credential/b;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 310
    :cond_0
    return-void

    .line 305
    :catch_0
    move-exception v0

    .line 307
    new-instance v1, Lcom/osp/security/credential/CredentialException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/osp/security/credential/CredentialException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 210
    if-eqz p1, :cond_2

    .line 211
    :goto_0
    if-eqz p2, :cond_3

    move-object v0, p2

    .line 212
    :goto_1
    :try_start_0
    invoke-static {}, Lcom/osp/common/c/a;->a()Lcom/osp/common/c/a;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/security/credential/b;->b:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/osp/common/c/a;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 213
    const-string v2, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 215
    invoke-static {}, Lcom/osp/common/c/a;->a()Lcom/osp/common/c/a;

    invoke-static {v1, v0}, Lcom/osp/common/c/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 218
    :cond_0
    invoke-direct {p0, p1}, Lcom/osp/security/credential/b;->d(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 220
    invoke-virtual {p0, p1}, Lcom/osp/security/credential/b;->c(Ljava/lang/String;)V

    .line 223
    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 225
    const-string v2, "appID"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    const-string v2, "appSecret"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    const-string v0, "oauthToken"

    const-string v2, ""

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    const-string v0, "oauthTokenSecret"

    const-string v2, ""

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    iget-object v0, p0, Lcom/osp/security/credential/b;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/osp/security/credential/b;->a:Landroid/net/Uri;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 234
    return-void

    .line 210
    :cond_2
    const-string p1, ""

    goto :goto_0

    .line 211
    :cond_3
    const-string v0, ""
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 231
    :catch_0
    move-exception v0

    .line 233
    new-instance v1, Lcom/osp/security/credential/CredentialException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/osp/security/credential/CredentialException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 248
    if-eqz p2, :cond_1

    .line 249
    :goto_0
    if-eqz p3, :cond_2

    move-object v0, p3

    .line 250
    :goto_1
    :try_start_0
    invoke-static {}, Lcom/osp/common/c/a;->a()Lcom/osp/common/c/a;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/security/credential/b;->b:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/osp/common/c/a;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 251
    const-string v2, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 253
    invoke-static {}, Lcom/osp/common/c/a;->a()Lcom/osp/common/c/a;

    invoke-static {v1, v0}, Lcom/osp/common/c/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 256
    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 258
    const-string v2, "oauthToken"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    const-string v2, "oauthTokenSecret"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v0, v2

    .line 264
    iget-object v2, p0, Lcom/osp/security/credential/b;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/osp/security/credential/b;->a:Landroid/net/Uri;

    const-string v4, "appID = ?"

    invoke-virtual {v2, v3, v1, v4, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 268
    return-void

    .line 248
    :cond_1
    const-string p2, ""

    goto :goto_0

    .line 249
    :cond_2
    const-string v0, ""
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 265
    :catch_0
    move-exception v0

    .line 267
    new-instance v1, Lcom/osp/security/credential/CredentialException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/osp/security/credential/CredentialException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 143
    .line 147
    :try_start_0
    const-string v0, "oauthTokenSecret"

    invoke-direct {p0, p1, v0}, Lcom/osp/security/credential/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 149
    invoke-static {}, Lcom/osp/common/c/a;->a()Lcom/osp/common/c/a;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/security/credential/b;->b:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/osp/common/c/a;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 150
    if-eqz v0, :cond_0

    const-string v2, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 152
    invoke-static {}, Lcom/osp/common/c/a;->a()Lcom/osp/common/c/a;

    invoke-static {v1, v0}, Lcom/osp/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 159
    :cond_0
    return-object v0

    .line 154
    :catch_0
    move-exception v0

    .line 156
    new-instance v1, Lcom/osp/security/credential/CredentialException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/osp/security/credential/CredentialException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected final c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 280
    const/4 v0, 0x1

    :try_start_0
    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 283
    iget-object v1, p0, Lcom/osp/security/credential/b;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/osp/security/credential/b;->a:Landroid/net/Uri;

    const-string v3, "appID = ?"

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 287
    return-void

    .line 284
    :catch_0
    move-exception v0

    .line 286
    new-instance v1, Lcom/osp/security/credential/CredentialException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/osp/security/credential/CredentialException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
