.class public final Lcom/osp/security/identity/d;
.super Ljava/lang/Object;
.source "IdentityManager.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Lcom/osp/security/credential/a;

.field private d:Lcom/osp/security/identity/f;

.field private e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    :try_start_0
    iput-object p1, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    .line 157
    new-instance v0, Lcom/osp/security/credential/a;

    invoke-direct {v0, p1}, Lcom/osp/security/credential/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/security/identity/d;->c:Lcom/osp/security/credential/a;

    .line 158
    new-instance v0, Lcom/osp/security/identity/f;

    invoke-direct {v0, p1}, Lcom/osp/security/identity/f;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/security/identity/d;->d:Lcom/osp/security/identity/f;

    .line 182
    const-string v0, "j5p7ll8g33"

    iput-object v0, p0, Lcom/osp/security/identity/d;->a:Ljava/lang/String;

    .line 184
    const-string v0, "5763D0052DC1462E13751F753384E9A9"

    iput-object v0, p0, Lcom/osp/security/identity/d;->b:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    return-void

    .line 189
    :catch_0
    move-exception v0

    .line 191
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 192
    new-instance v1, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a(Landroid/content/Context;Lcom/osp/http/impl/d;)V
    .locals 3

    .prologue
    .line 406
    const-string v0, "x-osp-clientmodel"

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/osp/http/impl/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    const-string v0, "x-osp-clientversion"

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->j(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/osp/http/impl/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    const-string v0, "x-osp-clientosversion"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->l()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/osp/http/impl/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    return-void
.end method

.method private a(Lcom/osp/security/identity/e;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1816
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1818
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1821
    :cond_1
    invoke-virtual {p1}, Lcom/osp/security/identity/e;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    if-nez p2, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_3
    iget-object v1, p0, Lcom/osp/security/identity/d;->d:Lcom/osp/security/identity/f;

    invoke-virtual {v1, v0, p2}, Lcom/osp/security/identity/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1822
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "IM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "INSERT/UPDATE "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/osp/security/identity/e;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " OSP DB"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1823
    return-void
.end method

.method private a(Lcom/osp/security/identity/e;)Z
    .locals 2

    .prologue
    .line 1753
    if-nez p1, :cond_0

    .line 1755
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1758
    :cond_0
    invoke-virtual {p1}, Lcom/osp/security/identity/e;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    iget-object v1, p0, Lcom/osp/security/identity/d;->d:Lcom/osp/security/identity/f;

    invoke-virtual {v1, v0}, Lcom/osp/security/identity/f;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private b(Lcom/osp/security/identity/e;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1784
    if-nez p1, :cond_0

    .line 1786
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1789
    :cond_0
    invoke-virtual {p1}, Lcom/osp/security/identity/e;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    iget-object v1, p0, Lcom/osp/security/identity/d;->d:Lcom/osp/security/identity/f;

    invoke-virtual {v1, v0}, Lcom/osp/security/identity/f;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    sget-object v0, Lcom/osp/security/identity/e;->a:Lcom/osp/security/identity/e;

    invoke-direct {p0, v0}, Lcom/osp/security/identity/d;->b(Lcom/osp/security/identity/e;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/osp/security/identity/k;)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 624
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "IM"

    const-string v3, "requestUserAuthentication"

    const-string v4, "START"

    invoke-static {v2, v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    :try_start_0
    iget-object v2, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/msc/c/l;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 634
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/osp/security/identity/k;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 636
    invoke-static {v4}, Lcom/msc/c/l;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 642
    :goto_0
    new-instance v5, Lcom/osp/http/impl/d;

    iget-object v2, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    iget-object v6, p0, Lcom/osp/security/identity/d;->a:Ljava/lang/String;

    iget-object v7, p0, Lcom/osp/security/identity/d;->b:Ljava/lang/String;

    invoke-direct {v5, v2, v6, v7}, Lcom/osp/http/impl/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 800
    iget-object v2, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v2

    .line 802
    if-eqz v2, :cond_a

    .line 804
    invoke-virtual {v2}, Lcom/osp/device/a;->c()Ljava/lang/String;

    move-result-object v2

    .line 807
    :goto_1
    invoke-virtual {p1, v2}, Lcom/osp/security/identity/k;->e(Ljava/lang/String;)V

    .line 821
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/osp/common/util/i;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 825
    invoke-virtual {p1, v6}, Lcom/osp/security/identity/k;->f(Ljava/lang/String;)V

    .line 826
    invoke-static {}, Lcom/osp/common/util/d;->a()Lcom/osp/common/util/d;

    move-result-object v2

    iget-object v7, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-virtual {v2, v7}, Lcom/osp/common/util/d;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/osp/security/identity/k;->a(Ljava/lang/String;)V

    .line 828
    invoke-virtual {p1}, Lcom/osp/security/identity/k;->f()Ljava/lang/String;

    move-result-object v2

    .line 830
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 832
    const-string v7, "X-osp-mcc"

    invoke-virtual {v5, v7, v6}, Lcom/osp/http/impl/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 837
    iget-object v7, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-static {v7, v5}, Lcom/osp/security/identity/d;->a(Landroid/content/Context;Lcom/osp/http/impl/d;)V

    .line 838
    sget-object v7, Lcom/osp/common/util/g;->a:Lcom/osp/common/util/g;

    invoke-virtual {v5, v3, v2, v7}, Lcom/osp/http/impl/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/osp/common/util/g;)Lorg/apache/http/HttpResponse;

    move-result-object v5

    .line 844
    if-eqz v5, :cond_9

    .line 846
    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_0
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 847
    if-eqz v2, :cond_8

    .line 849
    :try_start_1
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v3

    .line 851
    :goto_2
    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    .line 852
    if-eqz v1, :cond_7

    .line 854
    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I
    :try_end_1
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    move v10, v1

    move-object v1, v2

    move v2, v10

    .line 858
    :goto_3
    const/16 v7, 0xc8

    if-ne v2, v7, :cond_6

    .line 860
    :try_start_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v7, "IM"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "requestUserAuthentication statusCode=["

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, "]"

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v7, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 861
    if-nez v3, :cond_2

    .line 863
    new-instance v0, Ljava/lang/Exception;

    const-string v2, "Response content is null."

    invoke-direct {v0, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 903
    :catch_0
    move-exception v0

    .line 905
    :goto_4
    :try_start_3
    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->printStackTrace()V

    .line 906
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "IM"

    const-string v3, "requestUserAuthentication ErrorResultException"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 918
    new-instance v2, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v0, v4}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 935
    :catchall_0
    move-exception v0

    :goto_5
    if-eqz v1, :cond_0

    .line 939
    :try_start_4
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 935
    :cond_0
    :goto_6
    throw v0

    .line 639
    :cond_1
    :try_start_5
    invoke-static {v4}, Lcom/msc/c/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    goto/16 :goto_0

    .line 866
    :cond_2
    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v2

    .line 867
    array-length v5, v2

    :goto_7
    if-ge v0, v5, :cond_4

    aget-object v7, v2, v0

    .line 869
    const-string v8, "x-osp-accessInfo"

    invoke-interface {v7}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_3

    .line 871
    new-instance v8, Lcom/osp/common/property/a;

    iget-object v9, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-direct {v8, v9}, Lcom/osp/common/property/a;-><init>(Landroid/content/Context;)V

    .line 872
    const-string v9, "uri.hostname.sub"

    invoke-interface {v7}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v9, v7}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 867
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 876
    :cond_4
    new-instance v0, Lcom/osp/security/identity/l;

    invoke-direct {v0}, Lcom/osp/security/identity/l;-><init>()V

    .line 877
    invoke-virtual {v0, v3}, Lcom/osp/security/identity/l;->a(Ljava/io/InputStream;)V

    .line 879
    invoke-virtual {v0}, Lcom/osp/security/identity/l;->a()Ljava/lang/String;

    move-result-object v2

    .line 880
    invoke-virtual {v0}, Lcom/osp/security/identity/l;->c()Ljava/lang/String;

    move-result-object v3

    .line 890
    sget-object v5, Lcom/osp/security/identity/e;->c:Lcom/osp/security/identity/e;

    invoke-direct {p0, v5, v2}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/e;Ljava/lang/String;)V

    .line 891
    sget-object v2, Lcom/osp/security/identity/e;->d:Lcom/osp/security/identity/e;

    invoke-virtual {v0}, Lcom/osp/security/identity/l;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v2, v5}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/e;Ljava/lang/String;)V

    .line 892
    sget-object v2, Lcom/osp/security/identity/e;->a:Lcom/osp/security/identity/e;

    invoke-direct {p0, v2, v3}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/e;Ljava/lang/String;)V

    .line 893
    sget-object v2, Lcom/osp/security/identity/e;->f:Lcom/osp/security/identity/e;

    invoke-virtual {v0}, Lcom/osp/security/identity/l;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v2, v0}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/e;Ljava/lang/String;)V

    .line 894
    sget-object v0, Lcom/osp/security/identity/e;->b:Lcom/osp/security/identity/e;

    invoke-virtual {p1}, Lcom/osp/security/identity/k;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/e;Ljava/lang/String;)V

    .line 896
    sget-object v0, Lcom/osp/security/identity/e;->h:Lcom/osp/security/identity/e;

    invoke-direct {p0, v0, v6}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/e;Ljava/lang/String;)V

    .line 897
    sget-object v0, Lcom/osp/security/identity/e;->i:Lcom/osp/security/identity/e;

    invoke-direct {p0, v0, v4}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/e;Ljava/lang/String;)V
    :try_end_5
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 935
    :goto_8
    if-eqz v1, :cond_5

    .line 939
    :try_start_6
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 947
    :cond_5
    :goto_9
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "IM"

    const-string v1, "requestUserAuthentication"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 948
    return-void

    .line 900
    :cond_6
    :try_start_7
    new-instance v0, Lcom/osp/http/impl/b;

    iget-object v2, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/osp/http/impl/b;-><init>(Landroid/content/Context;)V

    .line 901
    invoke-virtual {v0, v3}, Lcom/osp/http/impl/b;->a(Ljava/io/InputStream;)V
    :try_end_7
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_8

    .line 919
    :catch_1
    move-exception v0

    .line 921
    :goto_a
    :try_start_8
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 922
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "IM"

    const-string v3, "requestUserAuthentication Exception"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 932
    new-instance v2, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 940
    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 943
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "IM"

    const-string v2, "requestUserAuthentication IOException="

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 940
    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 943
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "IM"

    const-string v1, "requestUserAuthentication IOException="

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_9

    .line 935
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto/16 :goto_5

    .line 919
    :catch_4
    move-exception v0

    move-object v1, v2

    goto :goto_a

    .line 903
    :catch_5
    move-exception v0

    move-object v1, v2

    goto/16 :goto_4

    :cond_7
    move-object v1, v2

    move v2, v0

    goto/16 :goto_3

    :cond_8
    move-object v3, v1

    goto/16 :goto_2

    :cond_9
    move v2, v0

    move-object v3, v1

    goto/16 :goto_3

    :cond_a
    move-object v2, v1

    goto/16 :goto_1
.end method

.method public final a(Lcom/osp/security/identity/k;Ljava/lang/String;)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 1104
    .line 1111
    :try_start_0
    iget-object v2, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/msc/c/l;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 1112
    invoke-static {v4}, Lcom/msc/c/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1114
    new-instance v5, Lcom/osp/http/impl/d;

    iget-object v2, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    iget-object v6, p0, Lcom/osp/security/identity/d;->a:Ljava/lang/String;

    iget-object v7, p0, Lcom/osp/security/identity/d;->b:Ljava/lang/String;

    invoke-direct {v5, v2, v6, v7}, Lcom/osp/http/impl/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1117
    iget-object v2, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/osp/security/identity/k;->b(Ljava/lang/String;)V

    .line 1118
    invoke-virtual {p1, p2}, Lcom/osp/security/identity/k;->d(Ljava/lang/String;)V

    .line 1275
    iget-object v2, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v2

    .line 1276
    if-eqz v2, :cond_9

    .line 1278
    invoke-virtual {v2}, Lcom/osp/device/a;->c()Ljava/lang/String;

    move-result-object v2

    .line 1281
    :goto_0
    invoke-virtual {p1, v2}, Lcom/osp/security/identity/k;->e(Ljava/lang/String;)V

    .line 1282
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/osp/common/util/i;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 1283
    invoke-virtual {p1, v6}, Lcom/osp/security/identity/k;->f(Ljava/lang/String;)V

    .line 1285
    invoke-static {}, Lcom/osp/common/util/d;->a()Lcom/osp/common/util/d;

    move-result-object v2

    iget-object v7, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-virtual {v2, v7}, Lcom/osp/common/util/d;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/osp/security/identity/k;->a(Ljava/lang/String;)V

    .line 1286
    invoke-virtual {p1}, Lcom/osp/security/identity/k;->f()Ljava/lang/String;

    move-result-object v2

    .line 1288
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1290
    const-string v7, "X-osp-mcc"

    invoke-virtual {v5, v7, v6}, Lcom/osp/http/impl/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1295
    iget-object v7, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-static {v7, v5}, Lcom/osp/security/identity/d;->a(Landroid/content/Context;Lcom/osp/http/impl/d;)V

    .line 1296
    sget-object v7, Lcom/osp/common/util/g;->a:Lcom/osp/common/util/g;

    invoke-virtual {v5, v3, v2, v7}, Lcom/osp/http/impl/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/osp/common/util/g;)Lorg/apache/http/HttpResponse;

    move-result-object v5

    .line 1302
    if-eqz v5, :cond_8

    .line 1304
    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_0
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1305
    if-eqz v2, :cond_7

    .line 1307
    :try_start_1
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v3

    .line 1309
    :goto_1
    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    .line 1310
    if-eqz v1, :cond_6

    .line 1312
    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I
    :try_end_1
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    move v10, v1

    move-object v1, v2

    move v2, v10

    .line 1316
    :goto_2
    const/16 v7, 0xc8

    if-ne v2, v7, :cond_5

    .line 1318
    if-nez v3, :cond_1

    .line 1320
    :try_start_2
    new-instance v0, Ljava/lang/Exception;

    const-string v2, "Response content is null."

    invoke-direct {v0, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1356
    :catch_0
    move-exception v0

    .line 1358
    :goto_3
    :try_start_3
    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->printStackTrace()V

    .line 1369
    new-instance v2, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v0, v4}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1385
    :catchall_0
    move-exception v0

    :goto_4
    if-eqz v1, :cond_0

    .line 1389
    :try_start_4
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 1385
    :cond_0
    :goto_5
    throw v0

    .line 1323
    :cond_1
    :try_start_5
    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v2

    .line 1324
    array-length v5, v2

    :goto_6
    if-ge v0, v5, :cond_3

    aget-object v7, v2, v0

    .line 1326
    const-string v8, "x-osp-accessInfo"

    invoke-interface {v7}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_2

    .line 1328
    new-instance v8, Lcom/osp/common/property/a;

    iget-object v9, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-direct {v8, v9}, Lcom/osp/common/property/a;-><init>(Landroid/content/Context;)V

    .line 1329
    const-string v9, "uri.hostname.sub"

    invoke-interface {v7}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v9, v7}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1324
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1333
    :cond_3
    new-instance v0, Lcom/osp/security/identity/l;

    invoke-direct {v0}, Lcom/osp/security/identity/l;-><init>()V

    .line 1334
    invoke-virtual {v0, v3}, Lcom/osp/security/identity/l;->a(Ljava/io/InputStream;)V

    .line 1336
    invoke-virtual {v0}, Lcom/osp/security/identity/l;->c()Ljava/lang/String;

    move-result-object v2

    .line 1338
    sget-object v3, Lcom/osp/security/identity/e;->c:Lcom/osp/security/identity/e;

    invoke-virtual {v0}, Lcom/osp/security/identity/l;->a()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v3, v5}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/e;Ljava/lang/String;)V

    .line 1339
    sget-object v3, Lcom/osp/security/identity/e;->d:Lcom/osp/security/identity/e;

    invoke-virtual {v0}, Lcom/osp/security/identity/l;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v3, v5}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/e;Ljava/lang/String;)V

    .line 1340
    sget-object v3, Lcom/osp/security/identity/e;->a:Lcom/osp/security/identity/e;

    invoke-direct {p0, v3, v2}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/e;Ljava/lang/String;)V

    .line 1341
    sget-object v2, Lcom/osp/security/identity/e;->f:Lcom/osp/security/identity/e;

    invoke-virtual {v0}, Lcom/osp/security/identity/l;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v2, v0}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/e;Ljava/lang/String;)V

    .line 1342
    sget-object v0, Lcom/osp/security/identity/e;->b:Lcom/osp/security/identity/e;

    invoke-virtual {p1}, Lcom/osp/security/identity/k;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/e;Ljava/lang/String;)V

    .line 1344
    sget-object v0, Lcom/osp/security/identity/e;->h:Lcom/osp/security/identity/e;

    invoke-direct {p0, v0, v6}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/e;Ljava/lang/String;)V

    .line 1345
    sget-object v0, Lcom/osp/security/identity/e;->i:Lcom/osp/security/identity/e;

    invoke-direct {p0, v0, v4}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/e;Ljava/lang/String;)V
    :try_end_5
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1385
    :goto_7
    if-eqz v1, :cond_4

    .line 1389
    :try_start_6
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 1395
    :cond_4
    :goto_8
    return-void

    .line 1353
    :cond_5
    :try_start_7
    new-instance v0, Lcom/osp/http/impl/b;

    iget-object v2, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/osp/http/impl/b;-><init>(Landroid/content/Context;)V

    .line 1354
    invoke-virtual {v0, v3}, Lcom/osp/http/impl/b;->a(Ljava/io/InputStream;)V
    :try_end_7
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_7

    .line 1370
    :catch_1
    move-exception v0

    .line 1372
    :goto_9
    :try_start_8
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1382
    new-instance v2, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1390
    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_5

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 1385
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto/16 :goto_4

    .line 1370
    :catch_4
    move-exception v0

    move-object v1, v2

    goto :goto_9

    .line 1356
    :catch_5
    move-exception v0

    move-object v1, v2

    goto/16 :goto_3

    :cond_6
    move-object v1, v2

    move v2, v0

    goto/16 :goto_2

    :cond_7
    move-object v3, v1

    goto/16 :goto_1

    :cond_8
    move v2, v0

    move-object v3, v1

    goto/16 :goto_2

    :cond_9
    move-object v2, v1

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 273
    if-nez p1, :cond_0

    .line 275
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 277
    :cond_0
    sget-object v0, Lcom/osp/security/identity/e;->b:Lcom/osp/security/identity/e;

    invoke-direct {p0, v0, p1}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/e;Ljava/lang/String;)V

    .line 278
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1520
    .line 1524
    :try_start_0
    iget-object v0, p0, Lcom/osp/security/identity/d;->c:Lcom/osp/security/credential/a;

    iget-object v2, p0, Lcom/osp/security/identity/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/osp/security/credential/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1526
    iget-object v0, p0, Lcom/osp/security/identity/d;->c:Lcom/osp/security/credential/a;

    iget-object v2, p0, Lcom/osp/security/identity/d;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/osp/security/identity/d;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/osp/security/credential/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1528
    :cond_0
    iget-object v0, p0, Lcom/osp/security/identity/d;->c:Lcom/osp/security/credential/a;

    invoke-virtual {v0, p1, p2}, Lcom/osp/security/credential/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1533
    iget-object v0, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/msc/c/l;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1535
    new-instance v2, Lcom/osp/http/impl/d;

    iget-object v3, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-direct {v2, v3, p1, p2}, Lcom/osp/http/impl/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1537
    new-instance v3, Lcom/osp/security/identity/a;

    invoke-direct {v3}, Lcom/osp/security/identity/a;-><init>()V

    .line 1539
    sget-object v4, Lcom/osp/security/identity/e;->c:Lcom/osp/security/identity/e;

    invoke-direct {p0, v4}, Lcom/osp/security/identity/d;->b(Lcom/osp/security/identity/e;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/osp/security/identity/a;->a(Ljava/lang/String;)V

    .line 1540
    sget-object v4, Lcom/osp/security/identity/e;->d:Lcom/osp/security/identity/e;

    invoke-direct {p0, v4}, Lcom/osp/security/identity/d;->b(Lcom/osp/security/identity/e;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/osp/security/identity/a;->b(Ljava/lang/String;)V

    .line 1542
    invoke-virtual {v3}, Lcom/osp/security/identity/a;->a()Ljava/lang/String;

    move-result-object v3

    .line 1544
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1546
    sget-object v4, Lcom/osp/common/util/g;->a:Lcom/osp/common/util/g;

    invoke-virtual {v2, v0, v3, v4}, Lcom/osp/http/impl/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/osp/common/util/g;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 1549
    const/4 v0, 0x0

    .line 1552
    if-eqz v3, :cond_7

    .line 1554
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_0
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1555
    if-eqz v2, :cond_1

    .line 1557
    :try_start_1
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 1559
    :cond_1
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    .line 1560
    if-eqz v3, :cond_6

    .line 1562
    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I
    :try_end_1
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    move-object v5, v1

    move-object v1, v2

    move-object v2, v5

    .line 1566
    :goto_0
    const/16 v3, 0xc8

    if-ne v0, v3, :cond_5

    .line 1568
    if-nez v2, :cond_3

    .line 1570
    :try_start_2
    new-instance v0, Ljava/lang/Exception;

    const-string v2, "Response content is null."

    invoke-direct {v0, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1591
    :catch_0
    move-exception v0

    .line 1593
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->printStackTrace()V

    .line 1605
    new-instance v2, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v0, v4}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1621
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_2

    .line 1625
    :try_start_4
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 1621
    :cond_2
    :goto_3
    throw v0

    .line 1572
    :cond_3
    :try_start_5
    new-instance v0, Lcom/osp/security/identity/b;

    invoke-direct {v0}, Lcom/osp/security/identity/b;-><init>()V

    .line 1573
    invoke-virtual {v0, v2}, Lcom/osp/security/identity/b;->a(Ljava/io/InputStream;)V

    .line 1575
    invoke-virtual {v0}, Lcom/osp/security/identity/b;->a()Ljava/lang/String;

    move-result-object v2

    .line 1585
    iget-object v3, p0, Lcom/osp/security/identity/d;->c:Lcom/osp/security/credential/a;

    invoke-virtual {v0}, Lcom/osp/security/identity/b;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, p1, v2, v0}, Lcom/osp/security/credential/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1621
    :goto_4
    if-eqz v1, :cond_4

    .line 1625
    :try_start_6
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 1631
    :cond_4
    :goto_5
    return-void

    .line 1588
    :cond_5
    :try_start_7
    new-instance v0, Lcom/osp/http/impl/b;

    iget-object v3, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/osp/http/impl/b;-><init>(Landroid/content/Context;)V

    .line 1589
    invoke-virtual {v0, v2}, Lcom/osp/http/impl/b;->a(Ljava/io/InputStream;)V
    :try_end_7
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_4

    .line 1606
    :catch_1
    move-exception v0

    .line 1608
    :goto_6
    :try_start_8
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1618
    new-instance v2, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1626
    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 1621
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    .line 1606
    :catch_4
    move-exception v0

    move-object v1, v2

    goto :goto_6

    .line 1591
    :catch_5
    move-exception v0

    move-object v1, v2

    goto :goto_1

    :cond_6
    move-object v5, v1

    move-object v1, v2

    move-object v2, v5

    goto :goto_0

    :cond_7
    move-object v2, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1643
    .line 1651
    :try_start_0
    iget-object v0, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/msc/c/l;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1653
    new-instance v1, Lcom/osp/http/impl/d;

    iget-object v3, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-direct {v1, v3, p1, p2}, Lcom/osp/http/impl/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1655
    new-instance v3, Lcom/osp/security/identity/i;

    invoke-direct {v3}, Lcom/osp/security/identity/i;-><init>()V

    .line 1658
    iget-object v4, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-static {v4}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/osp/security/identity/i;->a(Ljava/lang/String;)V

    .line 1659
    invoke-virtual {v3, p3}, Lcom/osp/security/identity/i;->b(Ljava/lang/String;)V

    .line 1660
    invoke-virtual {v3, p4}, Lcom/osp/security/identity/i;->c(Ljava/lang/String;)V

    .line 1662
    invoke-virtual {v3}, Lcom/osp/security/identity/i;->a()Ljava/lang/String;

    move-result-object v3

    .line 1664
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1666
    sget-object v4, Lcom/osp/http/impl/f;->c:Lcom/osp/http/impl/f;

    sget-object v5, Lcom/osp/common/util/g;->c:Lcom/osp/common/util/g;

    invoke-virtual {v1, v4, v0, v3, v5}, Lcom/osp/http/impl/d;->a(Lcom/osp/http/impl/f;Ljava/lang/String;Ljava/lang/String;Lcom/osp/common/util/g;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 1670
    const/4 v0, 0x0

    .line 1672
    if-eqz v3, :cond_6

    .line 1674
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_0
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 1675
    if-eqz v1, :cond_0

    .line 1677
    :try_start_1
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v2

    .line 1679
    :cond_0
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    .line 1680
    if-eqz v3, :cond_1

    .line 1682
    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 1686
    :cond_1
    :goto_0
    const/16 v3, 0xc8

    if-ne v0, v3, :cond_5

    .line 1688
    if-nez v2, :cond_3

    .line 1690
    new-instance v0, Ljava/lang/Exception;

    const-string v2, "Response content is null."

    invoke-direct {v0, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1704
    :catch_0
    move-exception v0

    .line 1706
    :goto_1
    :try_start_2
    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->printStackTrace()V

    .line 1717
    new-instance v2, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v0, v4}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1733
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_2
    if-eqz v2, :cond_2

    .line 1737
    :try_start_3
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 1733
    :cond_2
    :goto_3
    throw v0

    .line 1692
    :cond_3
    :try_start_4
    new-instance v0, Lcom/osp/security/identity/j;

    invoke-direct {v0}, Lcom/osp/security/identity/j;-><init>()V

    .line 1693
    invoke-virtual {v0, v2}, Lcom/osp/security/identity/j;->a(Ljava/io/InputStream;)V
    :try_end_4
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1733
    :goto_4
    if-eqz v1, :cond_4

    .line 1737
    :try_start_5
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1743
    :cond_4
    :goto_5
    return-void

    .line 1701
    :cond_5
    :try_start_6
    new-instance v0, Lcom/osp/http/impl/b;

    iget-object v3, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/osp/http/impl/b;-><init>(Landroid/content/Context;)V

    .line 1702
    invoke-virtual {v0, v2}, Lcom/osp/http/impl/b;->a(Ljava/io/InputStream;)V
    :try_end_6
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_4

    .line 1718
    :catch_1
    move-exception v0

    move-object v2, v1

    .line 1720
    :goto_6
    :try_start_7
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1730
    new-instance v1, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3, v0}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 1733
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 1738
    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 1718
    :catch_4
    move-exception v0

    goto :goto_6

    .line 1704
    :catch_5
    move-exception v0

    move-object v1, v2

    goto :goto_1

    :cond_6
    move-object v1, v2

    goto :goto_0
.end method

.method public final a(Lcom/osp/security/identity/m;)Z
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 960
    .line 970
    :try_start_0
    iget-object v3, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-static {v3}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 972
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 974
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_0
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1049
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 1051
    :goto_0
    :try_start_1
    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->printStackTrace()V

    .line 1062
    new-instance v2, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v0, v4}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1078
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_1
    if-eqz v2, :cond_0

    .line 1082
    :try_start_2
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 1078
    :cond_0
    :goto_2
    throw v0

    .line 977
    :cond_1
    :try_start_3
    iget-object v4, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-static {v4, v3}, Lcom/msc/c/l;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 979
    new-instance v5, Lcom/osp/http/impl/d;

    iget-object v6, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    iget-object v7, p0, Lcom/osp/security/identity/d;->a:Ljava/lang/String;

    iget-object v8, p0, Lcom/osp/security/identity/d;->b:Ljava/lang/String;

    invoke-direct {v5, v6, v7, v8}, Lcom/osp/http/impl/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 981
    invoke-virtual {p1}, Lcom/osp/security/identity/m;->a()Ljava/lang/String;

    move-result-object v6

    .line 983
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 986
    const-string v7, "loginid"

    invoke-virtual {v5, v7, v3}, Lcom/osp/http/impl/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 987
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v7

    invoke-virtual {v7}, Lcom/osp/device/b;->a()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-static {v3}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 989
    const-string v3, "IDType"

    const-string v7, "plainID"

    invoke-virtual {v5, v3, v7}, Lcom/osp/http/impl/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 994
    :goto_3
    sget-object v3, Lcom/osp/common/util/g;->a:Lcom/osp/common/util/g;

    invoke-virtual {v5, v4, v6, v3}, Lcom/osp/http/impl/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/osp/common/util/g;)Lorg/apache/http/HttpResponse;

    move-result-object v5

    .line 1000
    if-eqz v5, :cond_b

    .line 1002
    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_3
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v3

    .line 1003
    if-eqz v3, :cond_a

    .line 1005
    :try_start_4
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v4

    .line 1007
    :goto_4
    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    .line 1008
    if-eqz v2, :cond_9

    .line 1010
    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I
    :try_end_4
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result v2

    move v10, v2

    move-object v2, v3

    move v3, v10

    .line 1014
    :goto_5
    const/16 v6, 0xc8

    if-ne v3, v6, :cond_7

    .line 1016
    if-nez v4, :cond_3

    .line 1018
    :try_start_5
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Response content is null."

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1049
    :catch_1
    move-exception v0

    move-object v1, v2

    goto :goto_0

    .line 992
    :cond_2
    :try_start_6
    const-string v3, "IDType"

    const-string v7, "emailID"

    invoke-virtual {v5, v3, v7}, Lcom/osp/http/impl/d;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_3

    .line 1063
    :catch_2
    move-exception v0

    .line 1065
    :goto_6
    :try_start_7
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1075
    new-instance v1, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3, v0}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 1078
    :catchall_1
    move-exception v0

    goto/16 :goto_1

    .line 1021
    :cond_3
    :try_start_8
    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v5

    .line 1022
    array-length v6, v5

    move v3, v1

    :goto_7
    if-ge v3, v6, :cond_5

    aget-object v7, v5, v3

    .line 1024
    const-string v8, "x-osp-accessInfo"

    invoke-interface {v7}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-ne v8, v0, :cond_4

    .line 1026
    new-instance v8, Lcom/osp/common/property/a;

    iget-object v9, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-direct {v8, v9}, Lcom/osp/common/property/a;-><init>(Landroid/content/Context;)V

    .line 1027
    const-string v9, "uri.hostname.sub"

    invoke-interface {v7}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v9, v7}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1022
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 1031
    :cond_5
    new-instance v3, Lcom/osp/security/identity/n;

    invoke-direct {v3}, Lcom/osp/security/identity/n;-><init>()V

    .line 1032
    invoke-virtual {v3, v4}, Lcom/osp/security/identity/n;->a(Ljava/io/InputStream;)V

    .line 1034
    sget-object v4, Lcom/osp/security/identity/e;->a:Lcom/osp/security/identity/e;

    invoke-virtual {v3}, Lcom/osp/security/identity/n;->a()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/e;Ljava/lang/String;)V

    .line 1035
    sget-object v4, Lcom/osp/security/identity/e;->f:Lcom/osp/security/identity/e;

    invoke-virtual {v3}, Lcom/osp/security/identity/n;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/e;Ljava/lang/String;)V

    .line 1037
    invoke-virtual {v3}, Lcom/osp/security/identity/n;->a()Ljava/lang/String;

    move-result-object v3

    .line 1039
    if-eqz v3, :cond_8

    sget-object v4, Lcom/osp/security/identity/e;->a:Lcom/osp/security/identity/e;

    invoke-direct {p0, v4}, Lcom/osp/security/identity/d;->b(Lcom/osp/security/identity/e;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_8
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result v3

    if-eqz v3, :cond_8

    .line 1078
    :goto_8
    if-eqz v2, :cond_6

    .line 1082
    :try_start_9
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    .line 1089
    :cond_6
    :goto_9
    return v0

    .line 1046
    :cond_7
    :try_start_a
    new-instance v0, Lcom/osp/http/impl/b;

    iget-object v3, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/osp/http/impl/b;-><init>(Landroid/content/Context;)V

    .line 1047
    invoke-virtual {v0, v4}, Lcom/osp/http/impl/b;->a(Ljava/io/InputStream;)V
    :try_end_a
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :cond_8
    move v0, v1

    goto :goto_8

    .line 1083
    :catch_3
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    :catch_4
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 1078
    :catchall_2
    move-exception v0

    move-object v2, v3

    goto/16 :goto_1

    .line 1063
    :catch_5
    move-exception v0

    move-object v2, v3

    goto/16 :goto_6

    .line 1049
    :catch_6
    move-exception v0

    move-object v1, v3

    goto/16 :goto_0

    :cond_9
    move-object v2, v3

    move v3, v1

    goto/16 :goto_5

    :cond_a
    move-object v4, v2

    goto/16 :goto_4

    :cond_b
    move v3, v1

    move-object v4, v2

    goto/16 :goto_5
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    sget-object v0, Lcom/osp/security/identity/e;->b:Lcom/osp/security/identity/e;

    invoke-direct {p0, v0}, Lcom/osp/security/identity/d;->b(Lcom/osp/security/identity/e;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 287
    if-nez p1, :cond_0

    .line 289
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 291
    :cond_0
    sget-object v0, Lcom/osp/security/identity/e;->g:Lcom/osp/security/identity/e;

    invoke-direct {p0, v0, p1}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/e;Ljava/lang/String;)V

    .line 292
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 223
    sget-object v0, Lcom/osp/security/identity/e;->f:Lcom/osp/security/identity/e;

    invoke-direct {p0, v0}, Lcom/osp/security/identity/d;->b(Lcom/osp/security/identity/e;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 243
    sget-object v0, Lcom/osp/security/identity/e;->c:Lcom/osp/security/identity/e;

    invoke-direct {p0, v0}, Lcom/osp/security/identity/d;->b(Lcom/osp/security/identity/e;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 253
    sget-object v0, Lcom/osp/security/identity/e;->d:Lcom/osp/security/identity/e;

    invoke-direct {p0, v0}, Lcom/osp/security/identity/d;->b(Lcom/osp/security/identity/e;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 263
    sget-object v0, Lcom/osp/security/identity/e;->g:Lcom/osp/security/identity/e;

    invoke-direct {p0, v0}, Lcom/osp/security/identity/d;->b(Lcom/osp/security/identity/e;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 302
    sget-object v0, Lcom/osp/security/identity/e;->g:Lcom/osp/security/identity/e;

    invoke-direct {p0, v0}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/e;)Z

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 324
    sget-object v0, Lcom/osp/security/identity/e;->b:Lcom/osp/security/identity/e;

    invoke-direct {p0, v0}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/e;)Z

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 336
    sget-object v0, Lcom/osp/security/identity/e;->c:Lcom/osp/security/identity/e;

    invoke-direct {p0, v0}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/e;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/osp/security/identity/e;->d:Lcom/osp/security/identity/e;

    invoke-direct {p0, v0}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/e;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()V
    .locals 4

    .prologue
    .line 354
    sget-object v0, Lcom/osp/security/identity/e;->e:Lcom/osp/security/identity/e;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {v0}, Lcom/osp/security/identity/e;->toString()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    iget-object v2, p0, Lcom/osp/security/identity/d;->d:Lcom/osp/security/identity/f;

    invoke-virtual {v2, v1}, Lcom/osp/security/identity/f;->c(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "IM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DELETE "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/osp/security/identity/e;->ordinal()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " OSP DB"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    return-void
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 385
    iget-object v0, p0, Lcom/osp/security/identity/d;->d:Lcom/osp/security/identity/f;

    invoke-virtual {v0}, Lcom/osp/security/identity/f;->a()V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "IM"

    const-string v1, "removeAll"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    return-void
.end method

.method public final l()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1405
    .line 1415
    :try_start_0
    sget-object v0, Lcom/osp/security/identity/e;->c:Lcom/osp/security/identity/e;

    invoke-direct {p0, v0}, Lcom/osp/security/identity/d;->b(Lcom/osp/security/identity/e;)Ljava/lang/String;

    move-result-object v0

    .line 1417
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1419
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_0
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1461
    :catch_0
    move-exception v0

    .line 1463
    :goto_0
    :try_start_1
    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->printStackTrace()V

    .line 1474
    new-instance v2, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v0, v4}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1490
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    .line 1494
    :try_start_2
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1490
    :cond_0
    :goto_2
    throw v0

    .line 1422
    :cond_1
    :try_start_3
    iget-object v2, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/msc/c/l;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1424
    new-instance v3, Lcom/osp/http/impl/d;

    iget-object v4, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    iget-object v5, p0, Lcom/osp/security/identity/d;->a:Ljava/lang/String;

    iget-object v6, p0, Lcom/osp/security/identity/d;->b:Ljava/lang/String;

    invoke-direct {v3, v4, v5, v6}, Lcom/osp/http/impl/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1425
    const-string v4, "authToken"

    invoke-virtual {v3, v4, v0}, Lcom/osp/http/impl/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1427
    sget-object v0, Lcom/osp/http/impl/f;->d:Lcom/osp/http/impl/f;

    const/4 v4, 0x0

    sget-object v5, Lcom/osp/common/util/g;->a:Lcom/osp/common/util/g;

    invoke-virtual {v3, v0, v2, v4, v5}, Lcom/osp/http/impl/d;->a(Lcom/osp/http/impl/f;Ljava/lang/String;Ljava/lang/String;Lcom/osp/common/util/g;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 1432
    const/4 v0, 0x0

    .line 1435
    if-eqz v3, :cond_7

    .line 1437
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_3
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    .line 1438
    if-eqz v2, :cond_2

    .line 1440
    :try_start_4
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 1442
    :cond_2
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    .line 1443
    if-eqz v3, :cond_6

    .line 1445
    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I
    :try_end_4
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v0

    move-object v7, v1

    move-object v1, v2

    move-object v2, v7

    .line 1449
    :goto_3
    const/16 v3, 0xc8

    if-ne v0, v3, :cond_3

    .line 1451
    if-nez v2, :cond_4

    .line 1453
    :try_start_5
    new-instance v0, Ljava/lang/Exception;

    const-string v2, "Response content is null."

    invoke-direct {v0, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1475
    :catch_1
    move-exception v0

    .line 1477
    :goto_4
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1487
    new-instance v2, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1458
    :cond_3
    :try_start_7
    new-instance v0, Lcom/osp/http/impl/b;

    iget-object v3, p0, Lcom/osp/security/identity/d;->e:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/osp/http/impl/b;-><init>(Landroid/content/Context;)V

    .line 1459
    invoke-virtual {v0, v2}, Lcom/osp/http/impl/b;->a(Ljava/io/InputStream;)V
    :try_end_7
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1490
    :cond_4
    if-eqz v1, :cond_5

    .line 1494
    :try_start_8
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    .line 1500
    :cond_5
    :goto_5
    return-void

    .line 1495
    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 1490
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1

    .line 1475
    :catch_4
    move-exception v0

    move-object v1, v2

    goto :goto_4

    .line 1461
    :catch_5
    move-exception v0

    move-object v1, v2

    goto/16 :goto_0

    :cond_6
    move-object v7, v1

    move-object v1, v2

    move-object v2, v7

    goto :goto_3

    :cond_7
    move-object v2, v1

    goto :goto_3
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2442
    sget-object v0, Lcom/osp/security/identity/e;->i:Lcom/osp/security/identity/e;

    invoke-direct {p0, v0}, Lcom/osp/security/identity/d;->b(Lcom/osp/security/identity/e;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
