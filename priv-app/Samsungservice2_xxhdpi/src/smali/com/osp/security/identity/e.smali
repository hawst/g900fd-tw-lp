.class final enum Lcom/osp/security/identity/e;
.super Ljava/lang/Enum;
.source "IdentityManager.java"


# static fields
.field public static final enum a:Lcom/osp/security/identity/e;

.field public static final enum b:Lcom/osp/security/identity/e;

.field public static final enum c:Lcom/osp/security/identity/e;

.field public static final enum d:Lcom/osp/security/identity/e;

.field public static final enum e:Lcom/osp/security/identity/e;

.field public static final enum f:Lcom/osp/security/identity/e;

.field public static final enum g:Lcom/osp/security/identity/e;

.field public static final enum h:Lcom/osp/security/identity/e;

.field public static final enum i:Lcom/osp/security/identity/e;

.field private static final synthetic j:[Lcom/osp/security/identity/e;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 86
    new-instance v0, Lcom/osp/security/identity/e;

    const-string v1, "UserID"

    invoke-direct {v0, v1, v3}, Lcom/osp/security/identity/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/security/identity/e;->a:Lcom/osp/security/identity/e;

    new-instance v0, Lcom/osp/security/identity/e;

    const-string v1, "EmailID"

    invoke-direct {v0, v1, v4}, Lcom/osp/security/identity/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/security/identity/e;->b:Lcom/osp/security/identity/e;

    new-instance v0, Lcom/osp/security/identity/e;

    const-string v1, "AuthToken"

    invoke-direct {v0, v1, v5}, Lcom/osp/security/identity/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/security/identity/e;->c:Lcom/osp/security/identity/e;

    new-instance v0, Lcom/osp/security/identity/e;

    const-string v1, "AuthTokenSecret"

    invoke-direct {v0, v1, v6}, Lcom/osp/security/identity/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/security/identity/e;->d:Lcom/osp/security/identity/e;

    new-instance v0, Lcom/osp/security/identity/e;

    const-string v1, "Password"

    invoke-direct {v0, v1, v7}, Lcom/osp/security/identity/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/security/identity/e;->e:Lcom/osp/security/identity/e;

    new-instance v0, Lcom/osp/security/identity/e;

    const-string v1, "BirthDate"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/osp/security/identity/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/security/identity/e;->f:Lcom/osp/security/identity/e;

    new-instance v0, Lcom/osp/security/identity/e;

    const-string v1, "UserDeviceID"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/osp/security/identity/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/security/identity/e;->g:Lcom/osp/security/identity/e;

    new-instance v0, Lcom/osp/security/identity/e;

    const-string v1, "MobileCountryCode"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/osp/security/identity/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/security/identity/e;->h:Lcom/osp/security/identity/e;

    new-instance v0, Lcom/osp/security/identity/e;

    const-string v1, "ServerUrl"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/osp/security/identity/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/security/identity/e;->i:Lcom/osp/security/identity/e;

    .line 84
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/osp/security/identity/e;

    sget-object v1, Lcom/osp/security/identity/e;->a:Lcom/osp/security/identity/e;

    aput-object v1, v0, v3

    sget-object v1, Lcom/osp/security/identity/e;->b:Lcom/osp/security/identity/e;

    aput-object v1, v0, v4

    sget-object v1, Lcom/osp/security/identity/e;->c:Lcom/osp/security/identity/e;

    aput-object v1, v0, v5

    sget-object v1, Lcom/osp/security/identity/e;->d:Lcom/osp/security/identity/e;

    aput-object v1, v0, v6

    sget-object v1, Lcom/osp/security/identity/e;->e:Lcom/osp/security/identity/e;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/osp/security/identity/e;->f:Lcom/osp/security/identity/e;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/osp/security/identity/e;->g:Lcom/osp/security/identity/e;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/osp/security/identity/e;->h:Lcom/osp/security/identity/e;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/osp/security/identity/e;->i:Lcom/osp/security/identity/e;

    aput-object v2, v0, v1

    sput-object v0, Lcom/osp/security/identity/e;->j:[Lcom/osp/security/identity/e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/osp/security/identity/e;
    .locals 1

    .prologue
    .line 84
    const-class v0, Lcom/osp/security/identity/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/osp/security/identity/e;

    return-object v0
.end method

.method public static values()[Lcom/osp/security/identity/e;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/osp/security/identity/e;->j:[Lcom/osp/security/identity/e;

    invoke-virtual {v0}, [Lcom/osp/security/identity/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/osp/security/identity/e;

    return-object v0
.end method
