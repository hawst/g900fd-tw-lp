.class public final Lcom/osp/security/identity/m;
.super Ljava/lang/Object;
.source "UserValidationRequest.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/security/identity/m;->a:Ljava/lang/String;

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/security/identity/m;->b:Ljava/lang/String;

    .line 43
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 94
    :try_start_0
    new-instance v0, Lcom/msc/c/q;

    invoke-direct {v0}, Lcom/msc/c/q;-><init>()V

    .line 100
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 102
    invoke-virtual {v0, v1}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 103
    const-string v2, "UTF-8"

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 105
    const-string v2, ""

    const-string v3, "userAuthRequest"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    const-string v2, ""

    const-string v3, "loginID"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    iget-object v2, p0, Lcom/osp/security/identity/m;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 109
    const-string v2, ""

    const-string v3, "loginID"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const-string v2, ""

    const-string v3, "password"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    iget-object v2, p0, Lcom/osp/security/identity/m;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->c(Ljava/lang/String;)V

    .line 113
    const-string v2, ""

    const-string v3, "password"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const-string v2, ""

    const-string v3, "userAuthRequest"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    invoke-virtual {v0}, Lcom/msc/c/q;->a()V

    .line 118
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 124
    return-object v0

    .line 119
    :catch_0
    move-exception v0

    .line 121
    new-instance v1, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 60
    if-eqz p1, :cond_0

    .line 62
    iput-object p1, p0, Lcom/osp/security/identity/m;->a:Ljava/lang/String;

    .line 64
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 81
    if-eqz p1, :cond_0

    .line 83
    iput-object p1, p0, Lcom/osp/security/identity/m;->b:Ljava/lang/String;

    .line 85
    :cond_0
    return-void
.end method
