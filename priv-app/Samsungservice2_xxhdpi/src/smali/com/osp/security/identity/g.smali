.class public final Lcom/osp/security/identity/g;
.super Ljava/lang/Object;
.source "IdentityValueValidator.java"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/osp/security/identity/g;->a:Ljava/lang/ref/WeakReference;

    .line 59
    if-nez p1, :cond_0

    .line 61
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "IdentityValueValidator context is null"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 63
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/osp/security/identity/h;
    .locals 2

    .prologue
    .line 72
    sget-object v0, Lcom/osp/security/identity/h;->d:Lcom/osp/security/identity/h;

    .line 74
    const/4 v1, 0x0

    .line 77
    if-eqz p1, :cond_0

    .line 79
    :try_start_0
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 81
    :cond_0
    invoke-static {}, Lcom/google/i18n/phonenumbers/h;->a()Lcom/google/i18n/phonenumbers/h;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/i18n/phonenumbers/h;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/i18n/phonenumbers/d;

    .line 82
    invoke-virtual {v0}, Lcom/google/i18n/phonenumbers/d;->a()Lcom/google/i18n/phonenumbers/ac;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 87
    :goto_0
    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/i18n/phonenumbers/h;->a()Lcom/google/i18n/phonenumbers/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/i18n/phonenumbers/h;->b(Lcom/google/i18n/phonenumbers/ac;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89
    sget-object v0, Lcom/osp/security/identity/h;->a:Lcom/osp/security/identity/h;

    .line 94
    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    .line 92
    :cond_1
    sget-object v0, Lcom/osp/security/identity/h;->i:Lcom/osp/security/identity/h;

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Z)Lcom/osp/security/identity/h;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 104
    sget-object v0, Lcom/osp/security/identity/h;->d:Lcom/osp/security/identity/h;

    .line 108
    const/4 v0, -0x1

    .line 121
    if-eqz p0, :cond_0

    :try_start_0
    const-string v1, "@"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 123
    const-string v0, "@"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 126
    :cond_0
    if-lez v0, :cond_2

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 146
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 148
    :cond_1
    sget-object v0, Lcom/osp/security/identity/h;->d:Lcom/osp/security/identity/h;

    .line 224
    :goto_1
    return-object v0

    .line 126
    :cond_2
    const-string v0, ""

    goto :goto_0

    .line 149
    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_4

    .line 151
    sget-object v0, Lcom/osp/security/identity/h;->c:Lcom/osp/security/identity/h;

    goto :goto_1

    .line 152
    :cond_4
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x32

    if-le v1, v2, :cond_5

    .line 154
    sget-object v0, Lcom/osp/security/identity/h;->b:Lcom/osp/security/identity/h;

    goto :goto_1

    .line 155
    :cond_5
    const-string v1, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@.-_+"

    invoke-static {v1, v0}, Lcom/osp/security/identity/g;->e(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 158
    sget-object v0, Lcom/osp/security/identity/h;->e:Lcom/osp/security/identity/h;

    goto :goto_1

    .line 168
    :cond_6
    const-string v1, " "

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-ne v1, v3, :cond_7

    .line 170
    sget-object v0, Lcom/osp/security/identity/h;->e:Lcom/osp/security/identity/h;

    goto :goto_1

    .line 172
    :cond_7
    if-eqz p1, :cond_8

    const-string v1, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/security/identity/g;->e(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 174
    sget-object v0, Lcom/osp/security/identity/h;->d:Lcom/osp/security/identity/h;

    goto :goto_1

    .line 192
    :cond_8
    const-string v1, ".."

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 196
    sget-object v0, Lcom/osp/security/identity/h;->d:Lcom/osp/security/identity/h;

    goto :goto_1

    .line 207
    :cond_9
    if-eqz p1, :cond_b

    const-string v1, "admin"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "administrator"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "samsungapps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "supervisor"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "tizenaccount"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "tizenstore"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "tizen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 212
    :cond_a
    sget-object v0, Lcom/osp/security/identity/h;->f:Lcom/osp/security/identity/h;

    goto/16 :goto_1

    .line 217
    :cond_b
    sget-object v0, Lcom/osp/security/identity/h;->a:Lcom/osp/security/identity/h;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 219
    :catch_0
    move-exception v0

    .line 221
    new-instance v1, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 420
    .line 424
    :try_start_0
    iget-object v0, p0, Lcom/osp/security/identity/g;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 426
    iget-object v0, p0, Lcom/osp/security/identity/g;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 428
    invoke-static {v0}, Lcom/msc/c/e;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 429
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {v0}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 430
    if-eqz v0, :cond_0

    .line 432
    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 434
    :cond_0
    invoke-static {}, Lcom/google/i18n/phonenumbers/h;->a()Lcom/google/i18n/phonenumbers/h;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/google/i18n/phonenumbers/h;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/i18n/phonenumbers/d;

    .line 435
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/i18n/phonenumbers/d;->a()Lcom/google/i18n/phonenumbers/ac;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/i18n/phonenumbers/ac;->b()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 442
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "original number = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", getLocalPhoneNumber = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 444
    return-object v0

    :catch_0
    move-exception v0

    :cond_1
    move-object v0, p1

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)Lcom/osp/security/identity/h;
    .locals 3

    .prologue
    .line 468
    sget-object v0, Lcom/osp/security/identity/h;->d:Lcom/osp/security/identity/h;

    .line 472
    :try_start_0
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 474
    sget-object v0, Lcom/osp/security/identity/h;->a:Lcom/osp/security/identity/h;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 481
    :cond_0
    return-object v0

    .line 476
    :catch_0
    move-exception v0

    .line 478
    new-instance v1, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static d(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 455
    invoke-static {p0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 457
    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method

.method private static e(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 575
    const/4 v0, 0x1

    .line 579
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    move v2, v1

    .line 580
    :goto_0
    if-ge v2, v3, :cond_1

    .line 582
    add-int/lit8 v4, v2, 0x1

    invoke-virtual {p1, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    move v0, v1

    .line 580
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 587
    :catch_0
    move-exception v0

    .line 589
    new-instance v1, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 592
    :cond_1
    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/osp/security/identity/h;
    .locals 5

    .prologue
    .line 523
    sget-object v1, Lcom/osp/security/identity/h;->d:Lcom/osp/security/identity/h;

    .line 528
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 529
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 531
    iget-object v0, p0, Lcom/osp/security/identity/g;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 533
    iget-object v0, p0, Lcom/osp/security/identity/g;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 535
    if-eqz v0, :cond_0

    .line 538
    invoke-static {v0}, Lcom/osp/app/util/ad;->q(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 540
    new-instance v2, Ljava/text/SimpleDateFormat;

    invoke-direct {v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 541
    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 543
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 545
    sget-object v0, Lcom/osp/security/identity/h;->a:Lcom/osp/security/identity/h;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 554
    :goto_0
    return-object v0

    .line 549
    :catch_0
    move-exception v0

    .line 551
    new-instance v1, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Lcom/osp/security/identity/h;
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v4, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 303
    sget-object v0, Lcom/osp/security/identity/h;->d:Lcom/osp/security/identity/h;

    .line 305
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 412
    :cond_0
    :goto_0
    return-object v0

    .line 321
    :cond_1
    :try_start_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v5

    .line 329
    const-string v0, "@"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 331
    const-string v0, "@"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    move v3, v0

    .line 334
    :goto_1
    const/16 v0, 0x8

    if-ge v5, v0, :cond_2

    .line 336
    sget-object v0, Lcom/osp/security/identity/h;->c:Lcom/osp/security/identity/h;

    .line 372
    :goto_2
    sget-object v5, Lcom/osp/security/identity/h;->a:Lcom/osp/security/identity/h;

    if-ne v0, v5, :cond_0

    .line 381
    if-le v3, v2, :cond_7

    const/4 v0, 0x0

    invoke-virtual {p1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 383
    sget-object v0, Lcom/osp/security/identity/h;->g:Lcom/osp/security/identity/h;

    goto :goto_0

    .line 337
    :cond_2
    const/16 v0, 0xf

    if-le v5, v0, :cond_3

    .line 339
    sget-object v0, Lcom/osp/security/identity/h;->b:Lcom/osp/security/identity/h;

    goto :goto_2

    .line 340
    :cond_3
    const-string v0, "[a-zA-Z0-9\\~\\!\\@\\#\\$\\%\\^\\&\\*\\-\\_\\+\\=\\|\\\'\\;\\:\\[\\]\\{\\}\\(\\)\\<\\>\\,\\.\\/\\?\\\"\\\\]{2,100}"

    invoke-static {v0, p2}, Lcom/osp/security/identity/g;->d(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 344
    sget-object v0, Lcom/osp/security/identity/h;->f:Lcom/osp/security/identity/h;

    goto :goto_2

    .line 345
    :cond_4
    const-string v0, "[0-9\\~\\!\\@\\#\\$\\%\\^\\&\\*\\-\\_\\+\\=\\|\\\'\\;\\:\\[\\]\\{\\}\\(\\)\\<\\>\\,\\.\\/\\?\\\"\\\\]{1,100}"

    invoke-static {v0, p2}, Lcom/osp/security/identity/g;->d(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "[a-zA-Z\\~\\!\\@\\#\\$\\%\\^\\&\\*\\-\\_\\+\\=\\|\\\'\\;\\:\\[\\]\\{\\}\\(\\)\\<\\>\\,\\.\\/\\?\\\"\\\\]{1,100}"

    invoke-static {v0, p2}, Lcom/osp/security/identity/g;->d(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 348
    :cond_5
    sget-object v0, Lcom/osp/security/identity/h;->h:Lcom/osp/security/identity/h;

    goto :goto_2

    .line 351
    :cond_6
    sget-object v0, Lcom/osp/security/identity/h;->a:Lcom/osp/security/identity/h;

    goto :goto_2

    .line 384
    :cond_7
    if-ne v3, v4, :cond_8

    invoke-direct {p0, p1}, Lcom/osp/security/identity/g;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 386
    sget-object v0, Lcom/osp/security/identity/h;->g:Lcom/osp/security/identity/h;

    goto :goto_0

    .line 387
    :cond_8
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v5

    move v3, v2

    move v0, v1

    :goto_3
    if-ge v3, v5, :cond_f

    add-int/lit8 v6, v3, -0x1

    invoke-virtual {p2, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-virtual {p2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-static {v6}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v8

    if-eqz v8, :cond_9

    invoke-static {v7}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v8

    if-nez v8, :cond_b

    :cond_9
    move v0, v1

    :cond_a
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_b
    sub-int v8, v6, v7

    if-ne v8, v4, :cond_d

    add-int/lit8 v0, v0, -0x1

    :goto_4
    if-ge v0, v9, :cond_c

    const/4 v6, -0x2

    if-gt v0, v6, :cond_a

    :cond_c
    :goto_5
    if-eqz v1, :cond_12

    .line 389
    sget-object v0, Lcom/osp/security/identity/h;->a:Lcom/osp/security/identity/h;

    goto/16 :goto_0

    .line 387
    :cond_d
    sub-int/2addr v6, v7

    if-ne v6, v2, :cond_e

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_e
    move v0, v1

    goto :goto_4

    :cond_f
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    move v3, v2

    move v0, v1

    :goto_6
    if-ge v3, v4, :cond_11

    add-int/lit8 v5, v3, -0x1

    invoke-virtual {p2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {p2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v6

    sub-int/2addr v5, v6

    if-nez v5, :cond_10

    add-int/lit8 v0, v0, 0x1

    :goto_7
    if-ge v0, v9, :cond_c

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_10
    move v0, v1

    goto :goto_7

    :cond_11
    move v1, v2

    goto :goto_5

    .line 392
    :cond_12
    sget-object v0, Lcom/osp/security/identity/h;->e:Lcom/osp/security/identity/h;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 408
    :catch_0
    move-exception v0

    .line 410
    new-instance v1, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_13
    move v3, v4

    goto/16 :goto_1
.end method
