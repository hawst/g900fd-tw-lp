.class public final Lcom/osp/security/identity/i;
.super Ljava/lang/Object;
.source "PasswordChangeRequest.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/security/identity/i;->a:Ljava/lang/String;

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/security/identity/i;->b:Ljava/lang/String;

    .line 49
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/security/identity/i;->c:Ljava/lang/String;

    .line 50
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 122
    :try_start_0
    new-instance v0, Lcom/msc/c/q;

    invoke-direct {v0}, Lcom/msc/c/q;-><init>()V

    .line 128
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 130
    invoke-virtual {v0, v1}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 131
    const-string v2, "UTF-8"

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 132
    const-string v2, ""

    const-string v3, "userPasswordUpdate"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const-string v2, ""

    const-string v3, "loginID"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    iget-object v2, p0, Lcom/osp/security/identity/i;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 135
    const-string v2, ""

    const-string v3, "loginID"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string v2, ""

    const-string v3, "currentPassword"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "<![CDATA["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/osp/security/identity/i;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]]>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 140
    const-string v2, ""

    const-string v3, "currentPassword"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    const-string v2, ""

    const-string v3, "newPassword"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "<![CDATA["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/osp/security/identity/i;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]]>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 146
    const-string v2, ""

    const-string v3, "newPassword"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-string v2, ""

    const-string v3, "userPasswordUpdate"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    invoke-virtual {v0}, Lcom/msc/c/q;->a()V

    .line 150
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 156
    return-object v0

    .line 151
    :catch_0
    move-exception v0

    .line 153
    new-instance v1, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 67
    if-eqz p1, :cond_0

    .line 69
    iput-object p1, p0, Lcom/osp/security/identity/i;->a:Ljava/lang/String;

    .line 71
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 88
    if-eqz p1, :cond_0

    .line 90
    iput-object p1, p0, Lcom/osp/security/identity/i;->b:Ljava/lang/String;

    .line 92
    :cond_0
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 109
    if-eqz p1, :cond_0

    .line 111
    iput-object p1, p0, Lcom/osp/security/identity/i;->c:Ljava/lang/String;

    .line 113
    :cond_0
    return-void
.end method
