.class public final Lcom/osp/security/identity/k;
.super Ljava/lang/Object;
.source "UserAuthRequest.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/security/identity/k;->a:Ljava/lang/String;

    .line 96
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/security/identity/k;->b:Ljava/lang/String;

    .line 97
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/security/identity/k;->c:Ljava/lang/String;

    .line 98
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/security/identity/k;->d:Ljava/lang/String;

    .line 99
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/security/identity/k;->e:Ljava/lang/String;

    .line 100
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/security/identity/k;->f:Ljava/lang/String;

    .line 101
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/security/identity/k;->g:Ljava/lang/String;

    .line 102
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/security/identity/k;->h:Ljava/lang/String;

    .line 103
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/security/identity/k;->j:Ljava/lang/String;

    .line 104
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/security/identity/k;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/osp/security/identity/k;->i:Ljava/lang/String;

    .line 88
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 205
    const-string v0, "Y"

    iput-object v0, p0, Lcom/osp/security/identity/k;->e:Ljava/lang/String;

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 130
    if-eqz p1, :cond_0

    .line 132
    iput-object p1, p0, Lcom/osp/security/identity/k;->a:Ljava/lang/String;

    .line 134
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 229
    const-string v0, "Y"

    iput-object v0, p0, Lcom/osp/security/identity/k;->f:Ljava/lang/String;

    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 142
    if-eqz p1, :cond_0

    .line 144
    iput-object p1, p0, Lcom/osp/security/identity/k;->j:Ljava/lang/String;

    .line 146
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 253
    const-string v0, "Y"

    iput-object v0, p0, Lcom/osp/security/identity/k;->g:Ljava/lang/String;

    .line 258
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 163
    if-eqz p1, :cond_0

    .line 165
    iput-object p1, p0, Lcom/osp/security/identity/k;->b:Ljava/lang/String;

    .line 167
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 277
    const-string v0, "Y"

    iput-object v0, p0, Lcom/osp/security/identity/k;->h:Ljava/lang/String;

    .line 282
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 184
    if-eqz p1, :cond_0

    .line 186
    iput-object p1, p0, Lcom/osp/security/identity/k;->c:Ljava/lang/String;

    .line 188
    :cond_0
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 4

    .prologue
    .line 314
    :try_start_0
    new-instance v0, Lcom/msc/c/q;

    invoke-direct {v0}, Lcom/msc/c/q;-><init>()V

    .line 320
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 322
    invoke-virtual {v0, v1}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 323
    const-string v2, "UTF-8"

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 324
    const-string v2, ""

    const-string v3, "userAuthRequest"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    const-string v2, ""

    const-string v3, "loginID"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    iget-object v2, p0, Lcom/osp/security/identity/k;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 327
    const-string v2, ""

    const-string v3, "loginID"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    const-string v2, ""

    const-string v3, "password"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    iget-object v2, p0, Lcom/osp/security/identity/k;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->c(Ljava/lang/String;)V

    .line 333
    const-string v2, ""

    const-string v3, "password"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    const-string v2, ""

    const-string v3, "duid"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/osp/security/identity/k;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/security/identity/k;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 337
    const-string v2, ""

    const-string v3, "duid"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/osp/security/identity/k;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 341
    const-string v2, ""

    const-string v3, "checkTelephoneNumberValidation"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    const-string v2, "Y"

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 343
    const-string v2, ""

    const-string v3, "checkTelephoneNumberValidation"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    const-string v2, ""

    const-string v3, "countryCallingCode"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    iget-object v2, p0, Lcom/osp/security/identity/k;->j:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 347
    const-string v2, ""

    const-string v3, "countryCallingCode"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    :cond_0
    const-string v2, ""

    const-string v3, "userAuthRequest"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    invoke-virtual {v0}, Lcom/msc/c/q;->a()V

    .line 375
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 381
    return-object v0

    .line 376
    :catch_0
    move-exception v0

    .line 378
    new-instance v1, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 301
    if-eqz p1, :cond_0

    .line 303
    iput-object p1, p0, Lcom/osp/security/identity/k;->d:Ljava/lang/String;

    .line 305
    :cond_0
    return-void
.end method
