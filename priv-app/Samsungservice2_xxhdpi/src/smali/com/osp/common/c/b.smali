.class public final Lcom/osp/common/c/b;
.super Ljava/lang/Object;
.source "ShaDigest.java"


# static fields
.field private static c:Lcom/osp/common/c/b;


# instance fields
.field private final a:[C

.field private final b:[C


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-array v0, v1, [C

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/osp/common/c/b;->a:[C

    .line 31
    new-array v0, v1, [C

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/osp/common/c/b;->b:[C

    .line 42
    return-void

    .line 26
    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x61s
        0x62s
        0x63s
        0x64s
        0x65s
        0x66s
    .end array-data

    .line 31
    :array_1
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
    .end array-data
.end method

.method public static a()Lcom/osp/common/c/b;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/osp/common/c/b;->c:Lcom/osp/common/c/b;

    if-eqz v0, :cond_0

    .line 52
    sget-object v0, Lcom/osp/common/c/b;->c:Lcom/osp/common/c/b;

    .line 56
    :goto_0
    return-object v0

    .line 55
    :cond_0
    new-instance v0, Lcom/osp/common/c/b;

    invoke-direct {v0}, Lcom/osp/common/c/b;-><init>()V

    .line 56
    sput-object v0, Lcom/osp/common/c/b;->c:Lcom/osp/common/c/b;

    goto :goto_0
.end method

.method private static a([B)[B
    .locals 1

    .prologue
    .line 111
    :try_start_0
    const-string v0, "SHA-1"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 116
    invoke-virtual {v0, p0}, Ljava/security/MessageDigest;->update([B)V

    .line 118
    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 125
    return-object v0

    .line 119
    :catch_0
    move-exception v0

    .line 121
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 122
    throw v0
.end method

.method private static b(Ljava/lang/String;)[B
    .locals 1

    .prologue
    .line 89
    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 100
    return-object v0

    .line 94
    :catch_0
    move-exception v0

    .line 96
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 97
    throw v0
.end method

.method private b([B)[C
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 148
    .line 152
    :try_start_0
    iget-object v2, p0, Lcom/osp/common/c/b;->a:[C

    .line 154
    array-length v3, p1

    .line 155
    shl-int/lit8 v1, v3, 0x1

    new-array v4, v1, [C

    move v1, v0

    .line 157
    :goto_0
    if-ge v1, v3, :cond_0

    .line 159
    add-int/lit8 v5, v0, 0x1

    aget-byte v6, p1, v1

    and-int/lit16 v6, v6, 0xf0

    ushr-int/lit8 v6, v6, 0x4

    aget-char v6, v2, v6

    aput-char v6, v4, v0

    .line 160
    add-int/lit8 v0, v5, 0x1

    aget-byte v6, p1, v1

    and-int/lit8 v6, v6, 0xf

    aget-char v6, v2, v6

    aput-char v6, v4, v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 157
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 162
    :catch_0
    move-exception v0

    .line 164
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 165
    throw v0

    .line 168
    :cond_0
    return-object v4
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 78
    new-instance v0, Ljava/lang/String;

    invoke-static {p1}, Lcom/osp/common/c/b;->b(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1}, Lcom/osp/common/c/b;->a([B)[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/osp/common/c/b;->b([B)[C

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    return-object v0
.end method
