.class public final Lcom/osp/common/c/a;
.super Ljava/lang/Object;
.source "AESCryptoV01.java"


# static fields
.field private static final a:[B

.field private static e:Lcom/osp/common/c/a;


# instance fields
.field private final b:[[I

.field private final c:[[B

.field private final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/osp/common/c/a;->a:[B

    return-void

    :array_0
    .array-data 1
        0x3et
        -0x4bt
        0x1t
        0x45t
        -0x1ct
        -0x8t
        0x75t
        0x3ft
        0x8t
        -0x63t
        -0x61t
        0x57t
        0x3bt
        0x63t
        -0x11t
        0x4bt
    .end array-data
.end method

.method private constructor <init>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const/16 v0, 0x1d

    new-array v0, v0, [[I

    new-array v1, v3, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v4

    new-array v1, v3, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v5

    new-array v1, v3, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v3

    new-array v1, v3, [I

    fill-array-data v1, :array_3

    aput-object v1, v0, v6

    new-array v1, v3, [I

    fill-array-data v1, :array_4

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v3, [I

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [I

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v3, [I

    fill-array-data v2, :array_7

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v3, [I

    fill-array-data v2, :array_8

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v3, [I

    fill-array-data v2, :array_9

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v3, [I

    fill-array-data v2, :array_a

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v3, [I

    fill-array-data v2, :array_b

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v3, [I

    fill-array-data v2, :array_c

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v3, [I

    fill-array-data v2, :array_d

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-array v2, v3, [I

    fill-array-data v2, :array_e

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-array v2, v3, [I

    fill-array-data v2, :array_f

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-array v2, v3, [I

    fill-array-data v2, :array_10

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-array v2, v3, [I

    fill-array-data v2, :array_11

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-array v2, v3, [I

    fill-array-data v2, :array_12

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-array v2, v3, [I

    fill-array-data v2, :array_13

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-array v2, v3, [I

    fill-array-data v2, :array_14

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-array v2, v3, [I

    fill-array-data v2, :array_15

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-array v2, v3, [I

    fill-array-data v2, :array_16

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-array v2, v3, [I

    fill-array-data v2, :array_17

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-array v2, v3, [I

    fill-array-data v2, :array_18

    aput-object v2, v0, v1

    const/16 v1, 0x19

    new-array v2, v3, [I

    fill-array-data v2, :array_19

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    new-array v2, v3, [I

    fill-array-data v2, :array_1a

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    new-array v2, v3, [I

    fill-array-data v2, :array_1b

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    new-array v2, v3, [I

    fill-array-data v2, :array_1c

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/osp/common/c/a;->b:[[I

    .line 51
    const/16 v0, 0x1d

    new-array v0, v0, [[B

    new-array v1, v3, [B

    fill-array-data v1, :array_1d

    aput-object v1, v0, v4

    new-array v1, v3, [B

    fill-array-data v1, :array_1e

    aput-object v1, v0, v5

    new-array v1, v3, [B

    fill-array-data v1, :array_1f

    aput-object v1, v0, v3

    new-array v1, v3, [B

    fill-array-data v1, :array_20

    aput-object v1, v0, v6

    new-array v1, v3, [B

    fill-array-data v1, :array_21

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v3, [B

    fill-array-data v2, :array_22

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [B

    fill-array-data v2, :array_23

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v3, [B

    fill-array-data v2, :array_24

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v3, [B

    fill-array-data v2, :array_25

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v3, [B

    fill-array-data v2, :array_26

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v3, [B

    fill-array-data v2, :array_27

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v3, [B

    fill-array-data v2, :array_28

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v3, [B

    fill-array-data v2, :array_29

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v3, [B

    fill-array-data v2, :array_2a

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-array v2, v3, [B

    fill-array-data v2, :array_2b

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-array v2, v3, [B

    fill-array-data v2, :array_2c

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-array v2, v3, [B

    fill-array-data v2, :array_2d

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-array v2, v3, [B

    fill-array-data v2, :array_2e

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-array v2, v3, [B

    fill-array-data v2, :array_2f

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-array v2, v3, [B

    fill-array-data v2, :array_30

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-array v2, v3, [B

    fill-array-data v2, :array_31

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-array v2, v3, [B

    fill-array-data v2, :array_32

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-array v2, v3, [B

    fill-array-data v2, :array_33

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-array v2, v3, [B

    fill-array-data v2, :array_34

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-array v2, v3, [B

    fill-array-data v2, :array_35

    aput-object v2, v0, v1

    const/16 v1, 0x19

    new-array v2, v3, [B

    fill-array-data v2, :array_36

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    new-array v2, v3, [B

    fill-array-data v2, :array_37

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    new-array v2, v3, [B

    fill-array-data v2, :array_38

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    new-array v2, v3, [B

    fill-array-data v2, :array_39

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/osp/common/c/a;->c:[[B

    .line 58
    const v0, 0x9ba7382

    iget-object v1, p0, Lcom/osp/common/c/a;->b:[[I

    iget-object v2, p0, Lcom/osp/common/c/a;->c:[[B

    invoke-static {v0, v1, v2}, Lcom/osp/common/c/a;->a(I[[I[[B)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/common/c/a;->d:Ljava/lang/String;

    .line 69
    return-void

    .line 45
    :array_0
    .array-data 4
        0x10
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x4
        0x6
    .end array-data

    :array_2
    .array-data 4
        0xb
        0x1
    .end array-data

    :array_3
    .array-data 4
        0x6
        0x1
    .end array-data

    :array_4
    .array-data 4
        0xa
        0x0
    .end array-data

    :array_5
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_6
    .array-data 4
        0xc
        0x3
    .end array-data

    :array_7
    .array-data 4
        0x0
        0x18
    .end array-data

    :array_8
    .array-data 4
        0x12
        0x0
    .end array-data

    :array_9
    .array-data 4
        0x2
        0x0
    .end array-data

    :array_a
    .array-data 4
        0x0
        0x13
    .end array-data

    :array_b
    .array-data 4
        0xd
        0x7
    .end array-data

    :array_c
    .array-data 4
        0x0
        0x15
    .end array-data

    :array_d
    .array-data 4
        0x3
        0x0
    .end array-data

    :array_e
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_f
    .array-data 4
        0xa
        0x1a
    .end array-data

    :array_10
    .array-data 4
        0x0
        0x14
    .end array-data

    :array_11
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_12
    .array-data 4
        0x0
        0xf
    .end array-data

    :array_13
    .array-data 4
        0xb
        0x0
    .end array-data

    :array_14
    .array-data 4
        0x9
        0x0
    .end array-data

    :array_15
    .array-data 4
        0x0
        0x2
    .end array-data

    :array_16
    .array-data 4
        0x8
        0x0
    .end array-data

    :array_17
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_18
    .array-data 4
        0xf
        0x18
    .end array-data

    :array_19
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_1a
    .array-data 4
        0x0
        0x16
    .end array-data

    :array_1b
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_1c
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 51
    :array_1d
    .array-data 1
        0x43t
        0x6et
    .end array-data

    nop

    :array_1e
    .array-data 1
        0x5ft
        0x45t
    .end array-data

    nop

    :array_1f
    .array-data 1
        0x45t
        0x54t
    .end array-data

    nop

    :array_20
    .array-data 1
        0x43t
        0x5ft
    .end array-data

    nop

    :array_21
    .array-data 1
        0x4bt
        0x49t
    .end array-data

    nop

    :array_22
    .array-data 1
        0x5bt
        0x79t
    .end array-data

    nop

    :array_23
    .array-data 1
        0x52t
        0x4et
    .end array-data

    nop

    :array_24
    .array-data 1
        0x34t
        0x53t
    .end array-data

    nop

    :array_25
    .array-data 1
        0x45t
        0x6ft
    .end array-data

    nop

    :array_26
    .array-data 1
        0x54t
        0x55t
    .end array-data

    nop

    :array_27
    .array-data 1
        0x4dt
        0x45t
    .end array-data

    nop

    :array_28
    .array-data 1
        0x4et
        0x5ft
    .end array-data

    nop

    :array_29
    .array-data 1
        0x39t
        0x59t
    .end array-data

    nop

    :array_2a
    .array-data 1
        0x54t
        0x65t
    .end array-data

    nop

    :array_2b
    .array-data 1
        0x77t
        0x30t
    .end array-data

    nop

    :array_2c
    .array-data 1
        0x54t
        0x44t
    .end array-data

    nop

    :array_2d
    .array-data 1
        0x3dt
        0x4ft
    .end array-data

    nop

    :array_2e
    .array-data 1
        0x76t
        0x50t
    .end array-data

    nop

    :array_2f
    .array-data 1
        0x3ct
        0x58t
    .end array-data

    nop

    :array_30
    .array-data 1
        0x59t
        0x4bt
    .end array-data

    nop

    :array_31
    .array-data 1
        0x4et
        0x4bt
    .end array-data

    nop

    :array_32
    .array-data 1
        0x43t
        0x50t
    .end array-data

    nop

    :array_33
    .array-data 1
        0x54t
        0x62t
    .end array-data

    nop

    :array_34
    .array-data 1
        0x66t
        0x4et
    .end array-data

    nop

    :array_35
    .array-data 1
        0x45t
        0x45t
    .end array-data

    nop

    :array_36
    .array-data 1
        0x39t
        0x66t
    .end array-data

    nop

    :array_37
    .array-data 1
        0x68t
        0x5ft
    .end array-data

    nop

    :array_38
    .array-data 1
        0x40t
        0x5dt
    .end array-data

    nop

    :array_39
    .array-data 1
        0x70t
        0x79t
    .end array-data
.end method

.method public static a()Lcom/osp/common/c/a;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/osp/common/c/a;->e:Lcom/osp/common/c/a;

    if-eqz v0, :cond_0

    .line 79
    sget-object v0, Lcom/osp/common/c/a;->e:Lcom/osp/common/c/a;

    .line 83
    :goto_0
    return-object v0

    .line 82
    :cond_0
    new-instance v0, Lcom/osp/common/c/a;

    invoke-direct {v0}, Lcom/osp/common/c/a;-><init>()V

    .line 83
    sput-object v0, Lcom/osp/common/c/a;->e:Lcom/osp/common/c/a;

    goto :goto_0
.end method

.method private static a(I[[I[[B)Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v6, 0x1d

    const/4 v0, 0x0

    .line 96
    new-array v3, v6, [B

    move v2, v0

    .line 99
    :goto_0
    if-ge v0, v6, :cond_0

    .line 106
    and-int/lit8 v4, p0, 0x1

    .line 107
    shr-int/lit8 p0, p0, 0x1

    .line 108
    add-int/lit8 v1, v0, 0x1

    aget-object v5, p2, v2

    aget-byte v5, v5, v4

    aput-byte v5, v3, v0

    .line 109
    aget-object v0, p1, v2

    aget v0, v0, v4

    move v2, v0

    move v0, v1

    .line 110
    goto :goto_0

    .line 111
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>([B)V

    .line 113
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 125
    const/4 v0, 0x0

    .line 129
    :try_start_0
    invoke-static {}, Lcom/osp/common/b/b;->a()Lcom/osp/common/b/b;

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/common/b/b;->b([B)[B

    move-result-object v1

    .line 131
    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    const-string v3, "UTF-8"

    invoke-virtual {p0, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    const-string v4, "AES/CBC/PKCS5Padding"

    invoke-direct {v2, v3, v4}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 133
    new-instance v3, Ljavax/crypto/spec/IvParameterSpec;

    sget-object v4, Lcom/osp/common/c/a;->a:[B

    invoke-direct {v3, v4}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 135
    const-string v4, "AES/CBC/PKCS5Padding"

    invoke-static {v4}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v4

    .line 136
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v2, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 138
    if-eqz v1, :cond_0

    .line 140
    invoke-virtual {v4, v1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v1

    .line 142
    new-instance v0, Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    :cond_0
    return-object v0

    .line 145
    :catch_0
    move-exception v0

    .line 147
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 148
    throw v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 163
    :try_start_0
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    const-string v1, "UTF-8"

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    const-string v2, "AES/CBC/PKCS5Padding"

    invoke-direct {v0, v1, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 168
    new-instance v1, Ljavax/crypto/spec/IvParameterSpec;

    sget-object v2, Lcom/osp/common/c/a;->a:[B

    invoke-direct {v1, v2}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 170
    const-string v2, "AES/CBC/PKCS5Padding"

    invoke-static {v2}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v2

    .line 171
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v0, v1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 172
    const-string v0, "UTF-8"

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v2, v0}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v0

    .line 174
    new-instance v1, Ljava/lang/String;

    invoke-static {}, Lcom/osp/common/b/b;->a()Lcom/osp/common/b/b;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/osp/common/b/b;->a([B)[B

    move-result-object v0

    const-string v2, "UTF-8"

    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    return-object v1

    .line 176
    :catch_0
    move-exception v0

    .line 178
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 179
    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 193
    :try_start_0
    iget-object v0, p0, Lcom/osp/common/c/a;->d:Ljava/lang/String;

    .line 210
    invoke-static {p1}, Lcom/osp/device/d;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 273
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 275
    const-string v1, "MD5"

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 276
    const-string v2, "UTF-8"

    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/security/MessageDigest;->update([B)V

    .line 278
    new-instance v0, Ljava/lang/String;

    invoke-static {}, Lcom/osp/common/b/b;->a()Lcom/osp/common/b/b;

    move-result-object v2

    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/osp/common/b/b;->a([B)[B

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 285
    return-object v0

    .line 279
    :catch_0
    move-exception v0

    .line 281
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 282
    throw v0
.end method
