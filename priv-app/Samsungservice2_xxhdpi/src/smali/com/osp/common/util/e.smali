.class public final Lcom/osp/common/util/e;
.super Ljava/lang/Object;
.source "HeaderUtil.java"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/osp/security/time/a;

.field private final e:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lcom/osp/common/util/e;->a:Landroid/content/Context;

    .line 78
    iput-object p2, p0, Lcom/osp/common/util/e;->b:Ljava/lang/String;

    .line 79
    iput-object p3, p0, Lcom/osp/common/util/e;->c:Ljava/lang/String;

    .line 85
    new-instance v0, Lcom/osp/security/time/a;

    invoke-direct {v0, p1}, Lcom/osp/security/time/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/common/util/e;->d:Lcom/osp/security/time/a;

    .line 86
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/osp/common/util/e;->e:Ljava/util/HashMap;

    .line 96
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 7

    .prologue
    .line 358
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 359
    iget-object v0, p0, Lcom/osp/common/util/e;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 360
    const/16 v0, 0x5f

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 361
    iget-object v0, p0, Lcom/osp/common/util/e;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 363
    const-string v0, "Basic "

    .line 366
    :try_start_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "generateLicenseHeader - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 368
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/String;

    invoke-static {}, Lcom/osp/common/b/b;->a()Lcom/osp/common/b/b;

    move-result-object v4

    invoke-static {}, Lcom/osp/common/util/a;->a()Lcom/osp/common/util/a;

    iget-object v5, p0, Lcom/osp/common/util/e;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v6, 0x1

    invoke-static {v5, v1, v6}, Lcom/osp/common/util/a;->a(Ljava/lang/String;Ljava/lang/String;Z)[B

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/osp/common/b/b;->a([B)[B

    move-result-object v1

    const-string v4, "UTF-8"

    invoke-direct {v3, v1, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 374
    :goto_0
    return-object v0

    .line 369
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public final a(Lcom/osp/http/impl/f;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 172
    sget-object v0, Lcom/osp/common/util/g;->c:Lcom/osp/common/util/g;

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/osp/common/util/e;->a(Lcom/osp/http/impl/f;Ljava/lang/String;Ljava/lang/String;Lcom/osp/common/util/g;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/osp/http/impl/f;Ljava/lang/String;Ljava/lang/String;Lcom/osp/common/util/g;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 196
    const/4 v0, 0x0

    .line 204
    :try_start_0
    invoke-static {}, Lcom/osp/common/util/a;->a()Lcom/osp/common/util/a;

    invoke-static {}, Lcom/osp/common/util/a;->b()Ljava/lang/String;

    move-result-object v6

    .line 208
    const-string v3, "/security/sso/initialize/time"

    invoke-virtual {p2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 211
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v8, 0x3e8

    div-long/2addr v4, v8

    .line 216
    :goto_0
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    .line 218
    const-string v4, ""

    .line 219
    const-string v3, ""

    .line 222
    sget-object v7, Lcom/osp/common/util/f;->a:[I

    invoke-virtual {p4}, Lcom/osp/common/util/g;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    move v2, v1

    .line 266
    :goto_1
    iget-object v1, p0, Lcom/osp/common/util/e;->e:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 270
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, "="

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 214
    :cond_0
    iget-object v3, p0, Lcom/osp/common/util/e;->d:Lcom/osp/security/time/a;

    invoke-virtual {v3}, Lcom/osp/security/time/a;->a()J

    move-result-wide v4

    goto :goto_0

    .line 225
    :pswitch_0
    const-string v3, ""

    .line 226
    iget-object v2, p0, Lcom/osp/common/util/e;->c:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v4, v3

    move-object v3, v2

    move v2, v1

    .line 228
    goto :goto_1

    .line 232
    :pswitch_1
    :try_start_1
    new-instance v1, Lcom/osp/security/identity/d;

    iget-object v3, p0, Lcom/osp/common/util/e;->a:Landroid/content/Context;

    invoke-direct {v1, v3}, Lcom/osp/security/identity/d;-><init>(Landroid/content/Context;)V

    .line 233
    invoke-virtual {v1}, Lcom/osp/security/identity/d;->d()Ljava/lang/String;

    move-result-object v3

    .line 234
    invoke-virtual {v1}, Lcom/osp/security/identity/d;->e()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 240
    if-eqz v3, :cond_1

    if-eqz v1, :cond_1

    :try_start_2
    const-string v4, ""

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eq v4, v2, :cond_1

    const-string v4, ""

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-ne v4, v2, :cond_6

    .line 242
    :cond_1
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "User wasn\'t signed in."

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 325
    :catch_0
    move-exception v0

    .line 327
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 328
    throw v0

    .line 236
    :catch_1
    move-exception v0

    .line 238
    :try_start_3
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Can\'t get the User credential token."

    invoke-direct {v1, v2, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 248
    :pswitch_2
    :try_start_4
    new-instance v1, Lcom/osp/security/credential/a;

    iget-object v3, p0, Lcom/osp/common/util/e;->a:Landroid/content/Context;

    invoke-direct {v1, v3}, Lcom/osp/security/credential/a;-><init>(Landroid/content/Context;)V

    .line 249
    iget-object v3, p0, Lcom/osp/common/util/e;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/osp/security/credential/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 250
    iget-object v4, p0, Lcom/osp/common/util/e;->b:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/osp/security/credential/a;->b(Ljava/lang/String;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    move-result-object v1

    .line 256
    if-eqz v3, :cond_2

    if-eqz v1, :cond_2

    :try_start_5
    const-string v4, ""

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eq v4, v2, :cond_2

    const-string v4, ""

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-ne v4, v2, :cond_6

    .line 258
    :cond_2
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "App wasn\'t signed in"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 252
    :catch_2
    move-exception v0

    .line 254
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Can\'t get the App credential token."

    invoke-direct {v1, v2, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 278
    :cond_3
    if-eqz v0, :cond_4

    .line 280
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "?"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 284
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 286
    const-string v7, "oauth_consumer_key="

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 287
    iget-object v7, p0, Lcom/osp/common/util/e;->b:Ljava/lang/String;

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 288
    const/16 v7, 0x2c

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 289
    const-string v7, "oauth_signature_method="

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 290
    const-string v7, "HmacSHA1"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    const/16 v7, 0x2c

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 292
    const-string v7, "oauth_timestamp="

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 293
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    const/16 v5, 0x2c

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 295
    const-string v5, "oauth_nonce="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 297
    const/16 v5, 0x2c

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 298
    const-string v5, "oauth_version="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    const-string v5, "1.0"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301
    sget-object v5, Lcom/osp/common/util/g;->a:Lcom/osp/common/util/g;

    if-eq p4, v5, :cond_5

    .line 303
    const/16 v5, 0x2c

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 304
    const-string v5, "oauth_token="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 305
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    .line 309
    :cond_5
    :try_start_6
    invoke-static {}, Lcom/osp/common/util/a;->a()Lcom/osp/common/util/a;

    invoke-virtual {p1}, Lcom/osp/http/impl/f;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, p2, v0, v5, p3}, Lcom/osp/common/util/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 314
    new-instance v4, Ljava/lang/String;

    invoke-static {}, Lcom/osp/common/b/b;->a()Lcom/osp/common/b/b;

    move-result-object v5

    invoke-static {}, Lcom/osp/common/util/a;->a()Lcom/osp/common/util/a;

    invoke-static {v3, v0, v2}, Lcom/osp/common/util/a;->a(Ljava/lang/String;Ljava/lang/String;Z)[B

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/osp/common/b/b;->a([B)[B

    move-result-object v0

    const-string v2, "UTF-8"

    invoke-direct {v4, v0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    .line 319
    const/16 v0, 0x2c

    :try_start_7
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 320
    const-string v0, "oauth_signature="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 321
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 323
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 331
    return-object v0

    .line 315
    :catch_3
    move-exception v0

    .line 317
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Can\'t make a signed header."

    invoke-direct {v1, v2, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    :cond_6
    move-object v4, v3

    move-object v3, v1

    goto/16 :goto_1

    .line 222
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 346
    if-eqz p1, :cond_0

    .line 348
    iget-object v0, p0, Lcom/osp/common/util/e;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    :cond_0
    return-void
.end method
