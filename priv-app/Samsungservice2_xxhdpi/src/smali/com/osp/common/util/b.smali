.class final Lcom/osp/common/util/b;
.super Ljava/lang/Object;
.source "AuthUtil.java"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field final a:Ljava/util/Map$Entry;

.field private final b:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/util/Map$Entry;)V
    .locals 3

    .prologue
    .line 491
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 492
    iput-object p1, p0, Lcom/osp/common/util/b;->a:Ljava/util/Map$Entry;

    .line 493
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/common/util/b;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 494
    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/common/util/b;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 495
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/common/util/b;->b:Ljava/lang/String;

    .line 499
    return-void
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 507
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 488
    check-cast p1, Lcom/osp/common/util/b;

    iget-object v0, p0, Lcom/osp/common/util/b;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/osp/common/util/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 517
    iget-object v0, p0, Lcom/osp/common/util/b;->b:Ljava/lang/String;

    return-object v0
.end method
