.class public final Lcom/osp/common/util/d;
.super Ljava/lang/Object;
.source "DUIDUtil.java"


# static fields
.field private static a:Lcom/osp/common/util/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    sput-object v0, Lcom/osp/common/util/d;->a:Lcom/osp/common/util/d;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method

.method public static declared-synchronized a()Lcom/osp/common/util/d;
    .locals 2

    .prologue
    .line 32
    const-class v1, Lcom/osp/common/util/d;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/osp/common/util/d;->a:Lcom/osp/common/util/d;

    if-eqz v0, :cond_0

    .line 34
    sget-object v0, Lcom/osp/common/util/d;->a:Lcom/osp/common/util/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38
    :goto_0
    monitor-exit v1

    return-object v0

    .line 37
    :cond_0
    :try_start_1
    new-instance v0, Lcom/osp/common/util/d;

    invoke-direct {v0}, Lcom/osp/common/util/d;-><init>()V

    .line 38
    sput-object v0, Lcom/osp/common/util/d;->a:Lcom/osp/common/util/d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 32
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 107
    .line 111
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide v4, -0x3501454135014542L    # -1.8395617783091096E53

    xor-long/2addr v0, v4

    .line 114
    new-instance v3, Lcom/osp/common/a/a;

    invoke-direct {v3}, Lcom/osp/common/a/a;-><init>()V

    .line 115
    invoke-static {v0, v1}, Lcom/osp/common/util/d;->a(J)[B

    move-result-object v1

    move v0, v2

    .line 116
    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_0

    .line 118
    aget-byte v4, v1, v0

    invoke-virtual {v3, v4}, Lcom/osp/common/a/a;->a(B)V

    .line 116
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 120
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-static {}, Lcom/osp/common/b/a;->a()Lcom/osp/common/b/a;

    invoke-virtual {v3}, Lcom/osp/common/a/a;->a()[B

    move-result-object v1

    invoke-static {v1}, Lcom/osp/common/b/a;->a([B)[B

    move-result-object v1

    const-string v3, "UTF-8"

    invoke-direct {v0, v1, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 121
    const/4 v1, 0x0

    const/16 v3, 0xd

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 153
    :goto_1
    return-object v0

    .line 122
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 124
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "getDUIDfromIMEI NumberFormatException Occurred."

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 128
    :try_start_1
    new-instance v0, Ljava/math/BigInteger;

    const/16 v3, 0x10

    invoke-direct {v0, p0, v3}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    const-wide v4, -0x66454f2535014526L    # -9.814877617594446E-185

    invoke-static {v4, v5}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/math/BigInteger;->xor(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    .line 131
    new-instance v3, Lcom/osp/common/a/a;

    invoke-direct {v3}, Lcom/osp/common/a/a;-><init>()V

    .line 132
    invoke-static {v0}, Lcom/osp/common/util/d;->a(Ljava/math/BigInteger;)[B

    move-result-object v4

    move v0, v2

    .line 133
    :goto_2
    array-length v2, v4

    if-ge v0, v2, :cond_1

    .line 135
    aget-byte v2, v4, v0

    invoke-virtual {v3, v2}, Lcom/osp/common/a/a;->a(B)V

    .line 133
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 137
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-static {}, Lcom/osp/common/b/a;->a()Lcom/osp/common/b/a;

    invoke-virtual {v3}, Lcom/osp/common/a/a;->a()[B

    move-result-object v2

    invoke-static {v2}, Lcom/osp/common/b/a;->a([B)[B

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-direct {v0, v2, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 138
    const/4 v2, 0x0

    const/16 v3, 0xd

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 139
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "getDUIDfromIMEI with BigInteger"

    invoke-static {v2}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 142
    :catch_1
    move-exception v0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "getDUIDfromIMEI Second Exception Occurred."

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 144
    throw v1

    .line 151
    :catch_2
    move-exception v0

    .line 148
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "getDUIDfromIMEI Exception Occurred."

    invoke-static {v1}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 150
    throw v0
.end method

.method private static a(J)[B
    .locals 8

    .prologue
    const/16 v6, 0x8

    const-wide/16 v4, 0xff

    .line 276
    new-array v0, v6, [B

    const/4 v1, 0x0

    const/16 v2, 0x38

    shr-long v2, p0, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x30

    shr-long v2, p0, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x28

    shr-long v2, p0, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x20

    shr-long v2, p0, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0x18

    shr-long v2, p0, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x5

    const/16 v2, 0x10

    shr-long v2, p0, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x6

    shr-long v2, p0, v6

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x7

    and-long v2, p0, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    return-object v0
.end method

.method private static a(Ljava/math/BigInteger;)[B
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/16 v5, 0x10

    .line 287
    new-array v0, v6, [B

    const/4 v1, 0x0

    const/16 v2, 0x38

    invoke-virtual {p0, v2}, Ljava/math/BigInteger;->shiftRight(I)Ljava/math/BigInteger;

    move-result-object v2

    new-instance v3, Ljava/math/BigInteger;

    const-string v4, "ff"

    invoke-direct {v3, v4, v5}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2, v3}, Ljava/math/BigInteger;->and(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigInteger;->byteValue()B

    move-result v2

    aput-byte v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x30

    invoke-virtual {p0, v2}, Ljava/math/BigInteger;->shiftRight(I)Ljava/math/BigInteger;

    move-result-object v2

    new-instance v3, Ljava/math/BigInteger;

    const-string v4, "ff"

    invoke-direct {v3, v4, v5}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2, v3}, Ljava/math/BigInteger;->and(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigInteger;->byteValue()B

    move-result v2

    aput-byte v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x28

    invoke-virtual {p0, v2}, Ljava/math/BigInteger;->shiftRight(I)Ljava/math/BigInteger;

    move-result-object v2

    new-instance v3, Ljava/math/BigInteger;

    const-string v4, "ff"

    invoke-direct {v3, v4, v5}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2, v3}, Ljava/math/BigInteger;->and(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigInteger;->byteValue()B

    move-result v2

    aput-byte v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x20

    invoke-virtual {p0, v2}, Ljava/math/BigInteger;->shiftRight(I)Ljava/math/BigInteger;

    move-result-object v2

    new-instance v3, Ljava/math/BigInteger;

    const-string v4, "ff"

    invoke-direct {v3, v4, v5}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2, v3}, Ljava/math/BigInteger;->and(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigInteger;->byteValue()B

    move-result v2

    aput-byte v2, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0x18

    invoke-virtual {p0, v2}, Ljava/math/BigInteger;->shiftRight(I)Ljava/math/BigInteger;

    move-result-object v2

    new-instance v3, Ljava/math/BigInteger;

    const-string v4, "ff"

    invoke-direct {v3, v4, v5}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2, v3}, Ljava/math/BigInteger;->and(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigInteger;->byteValue()B

    move-result v2

    aput-byte v2, v0, v1

    const/4 v1, 0x5

    invoke-virtual {p0, v5}, Ljava/math/BigInteger;->shiftRight(I)Ljava/math/BigInteger;

    move-result-object v2

    new-instance v3, Ljava/math/BigInteger;

    const-string v4, "ff"

    invoke-direct {v3, v4, v5}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2, v3}, Ljava/math/BigInteger;->and(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigInteger;->byteValue()B

    move-result v2

    aput-byte v2, v0, v1

    const/4 v1, 0x6

    invoke-virtual {p0, v6}, Ljava/math/BigInteger;->shiftRight(I)Ljava/math/BigInteger;

    move-result-object v2

    new-instance v3, Ljava/math/BigInteger;

    const-string v4, "ff"

    invoke-direct {v3, v4, v5}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2, v3}, Ljava/math/BigInteger;->and(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigInteger;->byteValue()B

    move-result v2

    aput-byte v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Ljava/math/BigInteger;

    const-string v3, "ff"

    invoke-direct {v2, v3, v5}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v2}, Ljava/math/BigInteger;->and(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigInteger;->byteValue()B

    move-result v2

    aput-byte v2, v0, v1

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const-wide v4, -0x66454f2535014526L    # -9.814877617594446E-185

    const/4 v2, 0x0

    .line 164
    .line 168
    const/16 v0, 0x10

    :try_start_0
    invoke-static {p0, v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;I)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    xor-long/2addr v0, v4

    .line 171
    new-instance v3, Lcom/osp/common/a/a;

    invoke-direct {v3}, Lcom/osp/common/a/a;-><init>()V

    .line 172
    invoke-static {v0, v1}, Lcom/osp/common/util/d;->a(J)[B

    move-result-object v1

    move v0, v2

    .line 173
    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_0

    .line 175
    aget-byte v4, v1, v0

    invoke-virtual {v3, v4}, Lcom/osp/common/a/a;->a(B)V

    .line 173
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 177
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-static {}, Lcom/osp/common/b/a;->a()Lcom/osp/common/b/a;

    invoke-virtual {v3}, Lcom/osp/common/a/a;->a()[B

    move-result-object v1

    invoke-static {v1}, Lcom/osp/common/b/a;->a([B)[B

    move-result-object v1

    const-string v3, "UTF-8"

    invoke-direct {v0, v1, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 178
    const/4 v1, 0x0

    const/16 v3, 0xd

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 209
    :goto_1
    return-object v0

    .line 179
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 181
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "getDUIDfromMEID NumberFormatException Occurred."

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 184
    :try_start_1
    new-instance v0, Ljava/math/BigInteger;

    const/16 v3, 0x10

    invoke-direct {v0, p0, v3}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    const-wide v4, -0x66454f2535014526L    # -9.814877617594446E-185

    invoke-static {v4, v5}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/math/BigInteger;->xor(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    .line 187
    new-instance v3, Lcom/osp/common/a/a;

    invoke-direct {v3}, Lcom/osp/common/a/a;-><init>()V

    .line 188
    invoke-static {v0}, Lcom/osp/common/util/d;->a(Ljava/math/BigInteger;)[B

    move-result-object v4

    move v0, v2

    .line 189
    :goto_2
    array-length v2, v4

    if-ge v0, v2, :cond_1

    .line 191
    aget-byte v2, v4, v0

    invoke-virtual {v3, v2}, Lcom/osp/common/a/a;->a(B)V

    .line 189
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 193
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-static {}, Lcom/osp/common/b/a;->a()Lcom/osp/common/b/a;

    invoke-virtual {v3}, Lcom/osp/common/a/a;->a()[B

    move-result-object v2

    invoke-static {v2}, Lcom/osp/common/b/a;->a([B)[B

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-direct {v0, v2, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 194
    const/4 v2, 0x0

    const/16 v3, 0xd

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 195
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "getDUIDfromMEID with BigInteger"

    invoke-static {v2}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 198
    :catch_1
    move-exception v0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "getDUIDfromMEID Second Exception Occurred."

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 200
    throw v1

    .line 207
    :catch_2
    move-exception v0

    .line 204
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "getDUIDfromMEID Exception Occurred."

    invoke-static {v1}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 206
    throw v0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const-wide v4, 0xabbadacafebabeL

    const/4 v2, 0x0

    .line 221
    .line 225
    const/16 v0, 0x10

    :try_start_0
    invoke-static {p0, v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;I)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    xor-long/2addr v0, v4

    .line 228
    new-instance v3, Lcom/osp/common/a/a;

    invoke-direct {v3}, Lcom/osp/common/a/a;-><init>()V

    .line 229
    invoke-static {v0, v1}, Lcom/osp/common/util/d;->a(J)[B

    move-result-object v1

    move v0, v2

    .line 230
    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_0

    .line 232
    aget-byte v4, v1, v0

    invoke-virtual {v3, v4}, Lcom/osp/common/a/a;->a(B)V

    .line 230
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 234
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-static {}, Lcom/osp/common/b/a;->a()Lcom/osp/common/b/a;

    invoke-virtual {v3}, Lcom/osp/common/a/a;->a()[B

    move-result-object v1

    invoke-static {v1}, Lcom/osp/common/b/a;->a([B)[B

    move-result-object v1

    const-string v3, "UTF-8"

    invoke-direct {v0, v1, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 235
    const/4 v1, 0x0

    const/16 v3, 0xd

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 266
    :goto_1
    return-object v0

    .line 236
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 238
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "getDUIDfromTWID NumberFormatException Occurred."

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 241
    :try_start_1
    new-instance v0, Ljava/math/BigInteger;

    const/16 v3, 0x10

    invoke-direct {v0, p0, v3}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    const-wide v4, 0xabbadacafebabeL

    invoke-static {v4, v5}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/math/BigInteger;->xor(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    .line 244
    new-instance v3, Lcom/osp/common/a/a;

    invoke-direct {v3}, Lcom/osp/common/a/a;-><init>()V

    .line 245
    invoke-static {v0}, Lcom/osp/common/util/d;->a(Ljava/math/BigInteger;)[B

    move-result-object v4

    move v0, v2

    .line 246
    :goto_2
    array-length v2, v4

    if-ge v0, v2, :cond_1

    .line 248
    aget-byte v2, v4, v0

    invoke-virtual {v3, v2}, Lcom/osp/common/a/a;->a(B)V

    .line 246
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 250
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-static {}, Lcom/osp/common/b/a;->a()Lcom/osp/common/b/a;

    invoke-virtual {v3}, Lcom/osp/common/a/a;->a()[B

    move-result-object v2

    invoke-static {v2}, Lcom/osp/common/b/a;->a([B)[B

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-direct {v0, v2, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 251
    const/4 v2, 0x0

    const/16 v3, 0xd

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 252
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "getDUIDfromTWID with BigInteger"

    invoke-static {v2}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 255
    :catch_1
    move-exception v0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "getDUIDfromTWID Second Exception Occurred."

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 257
    throw v1

    .line 264
    :catch_2
    move-exception v0

    .line 261
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "getDUIDfromTWID Exception Occurred."

    invoke-static {v1}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 263
    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 52
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;)J

    move-result-wide v0

    .line 88
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 90
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "USERHANDLE"

    const-string v1, "UserHandle is Empty"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v0, ""
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    :goto_0
    monitor-exit p0

    return-object v0

    .line 94
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "USERHANDLE"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "UserHandle : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 52
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
