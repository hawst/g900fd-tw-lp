.class public final Lcom/osp/common/util/h;
.super Ljava/lang/Object;
.source "ServerURIUtil.java"


# static fields
.field private static a:Lcom/osp/common/util/h;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    return-void
.end method

.method public static a()Lcom/osp/common/util/h;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/osp/common/util/h;->a:Lcom/osp/common/util/h;

    if-eqz v0, :cond_0

    .line 45
    sget-object v0, Lcom/osp/common/util/h;->a:Lcom/osp/common/util/h;

    .line 49
    :goto_0
    return-object v0

    .line 48
    :cond_0
    new-instance v0, Lcom/osp/common/util/h;

    invoke-direct {v0}, Lcom/osp/common/util/h;-><init>()V

    .line 49
    sput-object v0, Lcom/osp/common/util/h;->a:Lcom/osp/common/util/h;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 61
    .line 62
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 72
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 74
    const-string v0, "www.ospserver.net"

    .line 75
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/osp/app/util/ah;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 76
    const-string v2, "www.ospserver.net"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 78
    const-string v2, "460"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "461"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 80
    :cond_0
    const-string v0, "chn.ospserver.net"

    .line 126
    :cond_1
    :goto_0
    return-object v0

    .line 97
    :cond_2
    :try_start_0
    const-string v0, "460"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "461"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 99
    :cond_3
    const-string v0, "chn"

    .line 105
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".ospserver.net"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 109
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 119
    :catch_0
    move-exception v0

    .line 121
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 122
    throw v0

    .line 102
    :cond_4
    :try_start_1
    new-instance v0, Lcom/osp/common/property/a;

    invoke-direct {v0, p0}, Lcom/osp/common/property/a;-><init>(Landroid/content/Context;)V

    .line 103
    const-string v1, "uri.hostname.sub"

    const-string v2, "www"

    invoke-virtual {v0, v1, v2}, Lcom/osp/common/property/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1
.end method
