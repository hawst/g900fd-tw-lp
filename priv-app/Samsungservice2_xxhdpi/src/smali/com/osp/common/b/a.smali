.class public final Lcom/osp/common/b/a;
.super Ljava/lang/Object;
.source "Base32.java"


# static fields
.field private static final a:[B

.field private static final b:[B

.field private static c:Lcom/osp/common/b/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/16 v0, 0x20

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/osp/common/b/a;->a:[B

    .line 29
    const/16 v0, 0x5b

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/osp/common/b/a;->b:[B

    return-void

    .line 23
    nop

    :array_0
    .array-data 1
        0x41t
        0x42t
        0x43t
        0x44t
        0x45t
        0x46t
        0x47t
        0x48t
        0x49t
        0x4at
        0x4bt
        0x4ct
        0x4dt
        0x4et
        0x4ft
        0x50t
        0x51t
        0x52t
        0x53t
        0x54t
        0x55t
        0x56t
        0x57t
        0x58t
        0x59t
        0x5at
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
    .end array-data

    .line 29
    :array_1
    .array-data 1
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        0x1at
        0x1bt
        0x1ct
        0x1dt
        0x1et
        0x1ft
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        0x0t
        0x1t
        0x2t
        0x3t
        0x4t
        0x5t
        0x6t
        0x7t
        0x8t
        0x9t
        0xat
        0xbt
        0xct
        0xdt
        0xet
        0xft
        0x10t
        0x11t
        0x12t
        0x13t
        0x14t
        0x15t
        0x16t
        0x17t
        0x18t
        0x19t
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    return-void
.end method

.method public static a()Lcom/osp/common/b/a;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/osp/common/b/a;->c:Lcom/osp/common/b/a;

    if-eqz v0, :cond_0

    .line 49
    sget-object v0, Lcom/osp/common/b/a;->c:Lcom/osp/common/b/a;

    .line 53
    :goto_0
    return-object v0

    .line 52
    :cond_0
    new-instance v0, Lcom/osp/common/b/a;

    invoke-direct {v0}, Lcom/osp/common/b/a;-><init>()V

    .line 53
    sput-object v0, Lcom/osp/common/b/a;->c:Lcom/osp/common/b/a;

    goto :goto_0
.end method

.method public static a([B)[B
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v2, 0x0

    .line 63
    :try_start_0
    array-length v4, p0

    .line 68
    add-int/lit8 v0, v4, 0x4

    div-int/lit8 v0, v0, 0x5

    mul-int/lit8 v0, v0, 0x5

    sub-int v5, v0, v4

    .line 69
    add-int/lit8 v0, v4, 0x4

    add-int/lit8 v1, v4, 0x4

    rem-int/lit8 v1, v1, 0x5

    sub-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x8

    div-int/lit8 v0, v0, 0x5

    .line 71
    new-array v1, v0, [B

    .line 73
    const/4 v0, 0x5

    new-array v6, v0, [B

    move v0, v2

    move v3, v2

    .line 75
    :goto_0
    add-int v7, v4, v5

    if-ge v3, v7, :cond_2

    .line 77
    if-ge v3, v4, :cond_1

    .line 79
    rem-int/lit8 v7, v3, 0x5

    aget-byte v8, p0, v3

    aput-byte v8, v6, v7

    .line 84
    :goto_1
    rem-int/lit8 v7, v3, 0x5

    if-ne v7, v11, :cond_0

    .line 86
    add-int/lit8 v7, v0, 0x1

    sget-object v8, Lcom/osp/common/b/a;->a:[B

    const/4 v9, 0x0

    aget-byte v9, v6, v9

    shr-int/lit8 v9, v9, 0x3

    and-int/lit8 v9, v9, 0x1f

    aget-byte v8, v8, v9

    aput-byte v8, v1, v0

    .line 87
    add-int/lit8 v0, v7, 0x1

    sget-object v8, Lcom/osp/common/b/a;->a:[B

    const/4 v9, 0x0

    aget-byte v9, v6, v9

    shl-int/lit8 v9, v9, 0x2

    and-int/lit8 v9, v9, 0x1c

    const/4 v10, 0x1

    aget-byte v10, v6, v10

    shr-int/lit8 v10, v10, 0x6

    and-int/lit8 v10, v10, 0x3

    or-int/2addr v9, v10

    aget-byte v8, v8, v9

    aput-byte v8, v1, v7

    .line 88
    add-int/lit8 v7, v0, 0x1

    sget-object v8, Lcom/osp/common/b/a;->a:[B

    const/4 v9, 0x1

    aget-byte v9, v6, v9

    shr-int/lit8 v9, v9, 0x1

    and-int/lit8 v9, v9, 0x1f

    aget-byte v8, v8, v9

    aput-byte v8, v1, v0

    .line 89
    add-int/lit8 v0, v7, 0x1

    sget-object v8, Lcom/osp/common/b/a;->a:[B

    const/4 v9, 0x1

    aget-byte v9, v6, v9

    shl-int/lit8 v9, v9, 0x4

    and-int/lit8 v9, v9, 0x10

    const/4 v10, 0x2

    aget-byte v10, v6, v10

    shr-int/lit8 v10, v10, 0x4

    and-int/lit8 v10, v10, 0xf

    or-int/2addr v9, v10

    aget-byte v8, v8, v9

    aput-byte v8, v1, v7

    .line 90
    add-int/lit8 v7, v0, 0x1

    sget-object v8, Lcom/osp/common/b/a;->a:[B

    const/4 v9, 0x2

    aget-byte v9, v6, v9

    shl-int/lit8 v9, v9, 0x1

    and-int/lit8 v9, v9, 0x1e

    const/4 v10, 0x3

    aget-byte v10, v6, v10

    shr-int/lit8 v10, v10, 0x7

    and-int/lit8 v10, v10, 0x1

    or-int/2addr v9, v10

    aget-byte v8, v8, v9

    aput-byte v8, v1, v0

    .line 91
    add-int/lit8 v0, v7, 0x1

    sget-object v8, Lcom/osp/common/b/a;->a:[B

    const/4 v9, 0x3

    aget-byte v9, v6, v9

    shr-int/lit8 v9, v9, 0x2

    and-int/lit8 v9, v9, 0x1f

    aget-byte v8, v8, v9

    aput-byte v8, v1, v7

    .line 92
    add-int/lit8 v7, v0, 0x1

    sget-object v8, Lcom/osp/common/b/a;->a:[B

    const/4 v9, 0x3

    aget-byte v9, v6, v9

    shl-int/lit8 v9, v9, 0x3

    and-int/lit8 v9, v9, 0x18

    const/4 v10, 0x4

    aget-byte v10, v6, v10

    shr-int/lit8 v10, v10, 0x5

    and-int/lit8 v10, v10, 0x7

    or-int/2addr v9, v10

    aget-byte v8, v8, v9

    aput-byte v8, v1, v0

    .line 93
    add-int/lit8 v0, v7, 0x1

    sget-object v8, Lcom/osp/common/b/a;->a:[B

    const/4 v9, 0x4

    aget-byte v9, v6, v9

    and-int/lit8 v9, v9, 0x1f

    aget-byte v8, v8, v9

    aput-byte v8, v1, v7

    .line 75
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 82
    :cond_1
    rem-int/lit8 v7, v3, 0x5

    const/4 v8, 0x0

    aput-byte v8, v6, v7

    goto/16 :goto_1

    .line 102
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    .line 105
    :goto_2
    return-object v0

    .line 96
    :cond_2
    :goto_3
    mul-int/lit8 v3, v5, 0x3

    div-int/lit8 v3, v3, 0x2

    if-ge v2, v3, :cond_3

    .line 98
    add-int/lit8 v0, v0, -0x1

    const/16 v3, 0x3d

    aput-byte v3, v1, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_3
    move-object v0, v1

    .line 103
    goto :goto_2
.end method
