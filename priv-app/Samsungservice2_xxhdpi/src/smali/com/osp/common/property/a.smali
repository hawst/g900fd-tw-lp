.class public final Lcom/osp/common/property/a;
.super Ljava/lang/Object;
.source "PropertyManager.java"


# instance fields
.field private a:Lcom/osp/common/property/b;

.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    :try_start_0
    iput-object p1, p0, Lcom/osp/common/property/a;->b:Landroid/content/Context;

    .line 51
    new-instance v0, Lcom/osp/common/property/b;

    iget-object v1, p0, Lcom/osp/common/property/a;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/osp/common/property/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/common/property/a;->a:Lcom/osp/common/property/b;

    .line 53
    const-string v0, "initialized"

    invoke-virtual {p0, v0}, Lcom/osp/common/property/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/osp/common/property/a;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    :cond_0
    return-void

    .line 57
    :catch_0
    move-exception v0

    .line 59
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 60
    new-instance v1, Lcom/osp/common/property/PropertyException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/osp/common/property/PropertyException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 105
    const/4 v0, 0x0

    .line 109
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/osp/common/property/a;->b(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/osp/common/property/PropertyException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 115
    :goto_0
    if-nez v0, :cond_0

    .line 120
    :goto_1
    return-object p2

    .line 110
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/osp/common/property/PropertyException;->printStackTrace()V

    goto :goto_0

    :cond_0
    move-object p2, v0

    goto :goto_1
.end method

.method public final a()V
    .locals 5

    .prologue
    .line 160
    const/4 v1, 0x0

    .line 164
    :try_start_0
    iget-object v0, p0, Lcom/osp/common/property/a;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 165
    invoke-virtual {v0}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 167
    const-string v2, "config.properties"

    invoke-virtual {v0, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 168
    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    .line 169
    invoke-virtual {v0, v1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    .line 171
    invoke-virtual {v0}, Ljava/util/Properties;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 173
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 174
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 176
    invoke-virtual {p0, v3}, Lcom/osp/common/property/a;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 178
    invoke-virtual {p0, v3, v0}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 183
    :catch_0
    move-exception v0

    .line 185
    :try_start_1
    new-instance v2, Lcom/osp/common/property/PropertyException;

    const-string v3, "Initializaion of property repository was failed."

    invoke-direct {v2, v3, v0}, Lcom/osp/common/property/PropertyException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 188
    :catchall_0
    move-exception v0

    .line 190
    if-eqz v1, :cond_1

    .line 192
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 197
    :cond_1
    :goto_1
    throw v0

    .line 182
    :cond_2
    :try_start_3
    const-string v0, "initialized"

    const-string v2, "true"

    invoke-virtual {p0, v0, v2}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 190
    if-eqz v1, :cond_3

    .line 192
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 198
    :cond_3
    :goto_2
    return-void

    .line 194
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 71
    if-nez p1, :cond_0

    .line 73
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/osp/common/property/a;->a:Lcom/osp/common/property/b;

    invoke-virtual {v0, p1}, Lcom/osp/common/property/b;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    if-nez p1, :cond_0

    .line 89
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/osp/common/property/a;->a:Lcom/osp/common/property/b;

    invoke-virtual {v0, p1}, Lcom/osp/common/property/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 131
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 133
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 136
    :cond_1
    iget-object v0, p0, Lcom/osp/common/property/a;->a:Lcom/osp/common/property/b;

    invoke-virtual {v0, p1, p2}, Lcom/osp/common/property/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 146
    if-nez p1, :cond_0

    .line 148
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/osp/common/property/a;->a:Lcom/osp/common/property/b;

    invoke-virtual {v0, p1}, Lcom/osp/common/property/b;->c(Ljava/lang/String;)V

    .line 152
    return-void
.end method
