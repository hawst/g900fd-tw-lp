.class public final enum Lcom/osp/http/impl/f;
.super Ljava/lang/Enum;
.source "RestClient.java"


# static fields
.field public static final enum a:Lcom/osp/http/impl/f;

.field public static final enum b:Lcom/osp/http/impl/f;

.field public static final enum c:Lcom/osp/http/impl/f;

.field public static final enum d:Lcom/osp/http/impl/f;

.field private static final synthetic e:[Lcom/osp/http/impl/f;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 67
    new-instance v0, Lcom/osp/http/impl/f;

    const-string v1, "POST"

    invoke-direct {v0, v1, v2}, Lcom/osp/http/impl/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/http/impl/f;->a:Lcom/osp/http/impl/f;

    new-instance v0, Lcom/osp/http/impl/f;

    const-string v1, "GET"

    invoke-direct {v0, v1, v3}, Lcom/osp/http/impl/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/http/impl/f;->b:Lcom/osp/http/impl/f;

    new-instance v0, Lcom/osp/http/impl/f;

    const-string v1, "PUT"

    invoke-direct {v0, v1, v4}, Lcom/osp/http/impl/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/http/impl/f;->c:Lcom/osp/http/impl/f;

    new-instance v0, Lcom/osp/http/impl/f;

    const-string v1, "DELETE"

    invoke-direct {v0, v1, v5}, Lcom/osp/http/impl/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/http/impl/f;->d:Lcom/osp/http/impl/f;

    .line 65
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/osp/http/impl/f;

    sget-object v1, Lcom/osp/http/impl/f;->a:Lcom/osp/http/impl/f;

    aput-object v1, v0, v2

    sget-object v1, Lcom/osp/http/impl/f;->b:Lcom/osp/http/impl/f;

    aput-object v1, v0, v3

    sget-object v1, Lcom/osp/http/impl/f;->c:Lcom/osp/http/impl/f;

    aput-object v1, v0, v4

    sget-object v1, Lcom/osp/http/impl/f;->d:Lcom/osp/http/impl/f;

    aput-object v1, v0, v5

    sput-object v0, Lcom/osp/http/impl/f;->e:[Lcom/osp/http/impl/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/osp/http/impl/f;
    .locals 1

    .prologue
    .line 65
    const-class v0, Lcom/osp/http/impl/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/osp/http/impl/f;

    return-object v0
.end method

.method public static values()[Lcom/osp/http/impl/f;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/osp/http/impl/f;->e:[Lcom/osp/http/impl/f;

    invoke-virtual {v0}, [Lcom/osp/http/impl/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/osp/http/impl/f;

    return-object v0
.end method
