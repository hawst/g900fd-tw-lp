.class public final Lcom/osp/http/impl/d;
.super Ljava/lang/Object;
.source "RestClient.java"


# static fields
.field private static g:Ljava/security/KeyStore;


# instance fields
.field private a:Lorg/apache/http/client/methods/HttpUriRequest;

.field private b:Lcom/osp/common/util/e;

.field private c:Ljava/util/HashMap;

.field private d:Ljava/util/HashMap;

.field private e:Ljava/util/HashMap;

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/osp/http/impl/d;->c:Ljava/util/HashMap;

    .line 145
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/osp/http/impl/d;->d:Ljava/util/HashMap;

    .line 146
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/osp/http/impl/d;->e:Ljava/util/HashMap;

    .line 148
    const v0, 0xea60

    iput v0, p0, Lcom/osp/http/impl/d;->f:I

    .line 150
    new-instance v0, Lcom/osp/common/util/e;

    invoke-direct {v0, p1, p2, p3}, Lcom/osp/common/util/e;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/http/impl/d;->b:Lcom/osp/common/util/e;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 156
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    sget-object v0, Lcom/osp/http/impl/d;->g:Ljava/security/KeyStore;

    if-nez v0, :cond_1

    .line 161
    :try_start_1
    invoke-static {}, Ljava/security/KeyStore;->getDefaultType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v0

    .line 162
    sput-object v0, Lcom/osp/http/impl/d;->g:Ljava/security/KeyStore;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 163
    const-string v0, "AndroidCAStore"

    invoke-static {v0}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v1

    .line 164
    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 165
    invoke-virtual {v1}, Ljava/security/KeyStore;->aliases()Ljava/util/Enumeration;

    move-result-object v2

    .line 166
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 168
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 169
    const-string v3, "system:"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 171
    sget-object v3, Lcom/osp/http/impl/d;->g:Ljava/security/KeyStore;

    invoke-virtual {v1, v0}, Ljava/security/KeyStore;->getCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/security/KeyStore;->setCertificateEntry(Ljava/lang/String;Ljava/security/cert/Certificate;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 176
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Failed to create a keystore containing our trusted system CAs"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 151
    :catch_1
    move-exception v0

    .line 153
    new-instance v1, Lcom/osp/http/impl/RestClientException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/osp/http/impl/RestClientException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 179
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Lcom/osp/http/impl/f;Ljava/lang/String;Ljava/lang/String;Lcom/osp/common/util/g;)Lorg/apache/http/HttpResponse;
    .locals 11

    .prologue
    const/16 v5, 0x50

    const/16 v4, 0x1bb

    const/4 v3, 0x0

    .line 279
    .line 281
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    .line 293
    :try_start_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "RC 1.0"

    const-string v2, "execute"

    const-string v6, "START"

    invoke-static {v1, v2, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    iget-object v1, p0, Lcom/osp/http/impl/d;->c:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v6, p2

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 297
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v2, "{"

    invoke-direct {v8, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, "}"

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v6, v2, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 303
    :cond_0
    iget-object v1, p0, Lcom/osp/http/impl/d;->b:Lcom/osp/common/util/e;

    invoke-virtual {v1, p1, v6, p3, p4}, Lcom/osp/common/util/e;->a(Lcom/osp/http/impl/f;Ljava/lang/String;Ljava/lang/String;Lcom/osp/common/util/g;)Ljava/lang/String;

    move-result-object v7

    .line 306
    iget-object v1, p0, Lcom/osp/http/impl/d;->d:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move-object v1, v3

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/util/Map$Entry;

    move-object v2, v0

    .line 310
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, "="

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 319
    :cond_1
    if-eqz v1, :cond_2

    .line 321
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "?"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v6, v1

    .line 324
    :cond_2
    sget-object v1, Lcom/osp/http/impl/f;->b:Lcom/osp/http/impl/f;

    if-ne p1, v1, :cond_7

    .line 326
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v6}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/osp/http/impl/d;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 360
    :cond_3
    :goto_2
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 362
    iget-object v1, p0, Lcom/osp/http/impl/d;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    const-string v2, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v8

    invoke-virtual {v8}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v1, v2, v8}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    :cond_4
    iget-object v1, p0, Lcom/osp/http/impl/d;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    const-string v2, "User-Agent"

    const-string v8, "SAMSUNG-Android"

    invoke-interface {v1, v2, v8}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    iget-object v1, p0, Lcom/osp/http/impl/d;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    const-string v2, "Accept"

    const-string v8, "*, */*"

    invoke-interface {v1, v2, v8}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    iget-object v1, p0, Lcom/osp/http/impl/d;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    const-string v2, "Accept-Encoding"

    const-string v8, "identity"

    invoke-interface {v1, v2, v8}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    sget-object v1, Lcom/osp/http/impl/f;->b:Lcom/osp/http/impl/f;

    if-eq p1, v1, :cond_5

    .line 369
    iget-object v1, p0, Lcom/osp/http/impl/d;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    const-string v2, "Content-Type"

    const-string v8, "text/xml"

    invoke-interface {v1, v2, v8}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    :cond_5
    iget-object v1, p0, Lcom/osp/http/impl/d;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    const-string v2, "Connection"

    const-string v8, "keep-alive"

    invoke-interface {v1, v2, v8}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    iget-object v1, p0, Lcom/osp/http/impl/d;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    const-string v2, "x-osp-version"

    const-string v8, "v1"

    invoke-interface {v1, v2, v8}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    iget-object v1, p0, Lcom/osp/http/impl/d;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    const-string v2, "Authorization"

    invoke-interface {v1, v2, v7}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    iget-object v1, p0, Lcom/osp/http/impl/d;->e:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/util/Map$Entry;

    move-object v2, v0

    .line 377
    iget-object v8, p0, Lcom/osp/http/impl/d;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v8, v1, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_3

    .line 558
    :catch_0
    move-exception v1

    .line 562
    :try_start_1
    iget-object v2, p0, Lcom/osp/http/impl/d;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    if-eqz v2, :cond_6

    .line 564
    iget-object v2, p0, Lcom/osp/http/impl/d;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v2}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6

    .line 570
    :cond_6
    :goto_4
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 571
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "RC 1.0"

    const-string v3, "execute IOException"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    new-instance v2, Lcom/osp/http/impl/RestClientException;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    const-string v4, "NET_0000"

    invoke-direct {v2, v3, v1, v4}, Lcom/osp/http/impl/RestClientException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v2

    .line 327
    :cond_7
    :try_start_2
    sget-object v1, Lcom/osp/http/impl/f;->d:Lcom/osp/http/impl/f;

    if-ne p1, v1, :cond_8

    .line 329
    new-instance v1, Lorg/apache/http/client/methods/HttpDelete;

    invoke-direct {v1, v6}, Lorg/apache/http/client/methods/HttpDelete;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/osp/http/impl/d;->a:Lorg/apache/http/client/methods/HttpUriRequest;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_2

    .line 573
    :catch_1
    move-exception v1

    .line 575
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "RC 1.0"

    const-string v3, "execute Exception"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    new-instance v2, Lcom/osp/http/impl/RestClientException;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/osp/http/impl/RestClientException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 330
    :cond_8
    :try_start_3
    sget-object v1, Lcom/osp/http/impl/f;->c:Lcom/osp/http/impl/f;

    if-ne p1, v1, :cond_9

    .line 332
    new-instance v1, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v1, v6}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/osp/http/impl/d;->a:Lorg/apache/http/client/methods/HttpUriRequest;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 335
    if-eqz p3, :cond_3

    .line 337
    :try_start_4
    iget-object v1, p0, Lcom/osp/http/impl/d;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    check-cast v1, Lorg/apache/http/client/methods/HttpPut;

    new-instance v2, Lorg/apache/http/entity/StringEntity;

    const-string v8, "UTF-8"

    invoke-direct {v2, p3, v8}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_4
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_2

    .line 339
    :catch_2
    move-exception v1

    :try_start_5
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto/16 :goto_2

    .line 343
    :cond_9
    sget-object v1, Lcom/osp/http/impl/f;->a:Lcom/osp/http/impl/f;

    if-ne p1, v1, :cond_a

    .line 345
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v1, v6}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/osp/http/impl/d;->a:Lorg/apache/http/client/methods/HttpUriRequest;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    .line 348
    if-eqz p3, :cond_3

    .line 350
    :try_start_6
    iget-object v1, p0, Lcom/osp/http/impl/d;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    check-cast v1, Lorg/apache/http/client/methods/HttpPost;

    new-instance v2, Lorg/apache/http/entity/StringEntity;

    const-string v8, "UTF-8"

    invoke-direct {v2, p3, v8}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_6
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_2

    .line 352
    :catch_3
    move-exception v1

    :try_start_7
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto/16 :goto_2

    .line 358
    :cond_a
    new-instance v1, Lcom/osp/http/impl/RestClientException;

    const-string v2, "Invalid HTTP method."

    invoke-direct {v1, v2}, Lcom/osp/http/impl/RestClientException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 380
    :cond_b
    new-instance v7, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v7}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 382
    iget v1, p0, Lcom/osp/http/impl/d;->f:I

    invoke-static {v7, v1}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 383
    iget v1, p0, Lcom/osp/http/impl/d;->f:I

    invoke-static {v7, v1}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 395
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 396
    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v6

    .line 397
    invoke-virtual {v2}, Landroid/net/Uri;->getPort()I

    move-result v1

    .line 398
    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    .line 400
    const/4 v8, -0x1

    if-ne v1, v8, :cond_c

    .line 402
    const-string v1, "https"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    const/4 v8, 0x1

    if-ne v1, v8, :cond_f

    move v1, v4

    .line 410
    :cond_c
    :goto_5
    new-instance v4, Lorg/apache/http/HttpHost;

    invoke-direct {v4, v6, v1, v2}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 412
    new-instance v2, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v2, v7}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/params/HttpParams;)V

    .line 413
    invoke-virtual {v2}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/conn/ClientConnectionManager;->getSchemeRegistry()Lorg/apache/http/conn/scheme/SchemeRegistry;

    move-result-object v1

    .line 414
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    move-result v5

    if-eqz v5, :cond_10

    .line 420
    :try_start_8
    invoke-static {}, Ljava/security/KeyStore;->getDefaultType()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v1

    .line 421
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 423
    new-instance v5, Lcom/msc/b/a;

    invoke-direct {v5, v1}, Lcom/msc/b/a;-><init>(Ljava/security/KeyStore;)V

    .line 424
    sget-object v1, Lorg/apache/http/conn/ssl/SSLSocketFactory;->ALLOW_ALL_HOSTNAME_VERIFIER:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    invoke-virtual {v5, v1}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->setHostnameVerifier(Lorg/apache/http/conn/ssl/X509HostnameVerifier;)V

    .line 426
    new-instance v6, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v6}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 427
    new-instance v1, Lorg/apache/http/conn/scheme/Scheme;

    const-string v8, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v9

    const/16 v10, 0x50

    invoke-direct {v1, v8, v9, v10}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v6, v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 428
    new-instance v1, Lorg/apache/http/conn/scheme/Scheme;

    const-string v8, "https"

    const/16 v9, 0x1bb

    invoke-direct {v1, v8, v5, v9}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v6, v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 430
    new-instance v1, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    invoke-direct {v1, v7, v6}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0

    move-object v3, v1

    .line 436
    :goto_6
    :try_start_9
    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-virtual {v2}, Lorg/apache/http/impl/client/DefaultHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v2

    invoke-direct {v1, v3, v2}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    move-object v2, v1

    .line 486
    :goto_7
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->g()Z

    move-result v1

    .line 487
    if-eqz v1, :cond_d

    .line 489
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "RC 1.0"

    const-string v3, "No Proxy OSP 1.0"

    invoke-static {v1, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    new-instance v1, Lorg/apache/http/HttpHost;

    sget-object v3, Lorg/apache/http/conn/params/ConnRouteParams;->NO_HOST:Lorg/apache/http/HttpHost;

    invoke-direct {v1, v3}, Lorg/apache/http/HttpHost;-><init>(Lorg/apache/http/HttpHost;)V

    .line 491
    invoke-virtual {v2}, Lorg/apache/http/impl/client/DefaultHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v3

    const-string v5, "http.route.default-proxy"

    invoke-interface {v3, v5, v1}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 494
    :cond_d
    new-instance v1, Lcom/osp/http/impl/e;

    invoke-direct {v1, p0}, Lcom/osp/http/impl/e;-><init>(Lcom/osp/http/impl/d;)V

    invoke-virtual {v2, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->addResponseInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 517
    iget-object v1, p0, Lcom/osp/http/impl/d;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-static {v1}, Lcom/osp/app/signin/SamsungService;->a(Lorg/apache/http/client/methods/HttpUriRequest;)V

    .line 518
    invoke-static {v2}, Lcom/osp/app/signin/SamsungService;->a(Lorg/apache/http/client/HttpClient;)V

    .line 520
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "================= RC Request ================="

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 521
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/osp/http/impl/d;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v3}, Lorg/apache/http/client/methods/HttpUriRequest;->getMethod()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/osp/http/impl/d;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v3}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 522
    iget-object v1, p0, Lcom/osp/http/impl/d;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v1}, Lorg/apache/http/client/methods/HttpUriRequest;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v3

    .line 524
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 525
    const/4 v1, 0x0

    :goto_8
    array-length v6, v3

    if-ge v1, v6, :cond_12

    .line 527
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v7, v3, v1

    invoke-interface {v7}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v3, v1

    invoke-interface {v7}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 528
    array-length v6, v3

    add-int/lit8 v6, v6, -0x1

    if-eq v1, v6, :cond_e

    .line 530
    const/16 v6, 0x3b

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 525
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_f
    move v1, v5

    .line 407
    goto/16 :goto_5

    .line 431
    :catch_4
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_6

    .line 438
    :cond_10
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->q()Z

    move-result v3

    if-nez v3, :cond_11

    sget-object v3, Lcom/osp/http/impl/d;->g:Ljava/security/KeyStore;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    if-eqz v3, :cond_11

    .line 442
    :try_start_a
    new-instance v3, Lorg/apache/http/conn/ssl/SSLSocketFactory;

    sget-object v5, Lcom/osp/http/impl/d;->g:Ljava/security/KeyStore;

    invoke-direct {v3, v5}, Lorg/apache/http/conn/ssl/SSLSocketFactory;-><init>(Ljava/security/KeyStore;)V

    .line 443
    new-instance v5, Lorg/apache/http/conn/scheme/Scheme;

    const-string v6, "https"

    const/16 v7, 0x1bb

    invoke-direct {v5, v6, v3, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v1, v5}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 444
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "RC 1.0"

    const-string v3, "Security=[true]"

    invoke-static {v1, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0

    goto/16 :goto_7

    .line 447
    :catch_5
    move-exception v1

    :try_start_b
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Failed to apply a keystore containing our trusted system CAs"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 451
    :cond_11
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "RC 1.0"

    const-string v3, "Security=[false]"

    invoke-static {v1, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 533
    :cond_12
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "HEADER :: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 534
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "BODY :: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 535
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "=============================================="

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 537
    iget-object v1, p0, Lcom/osp/http/impl/d;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-virtual {v2, v4, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 551
    if-eqz v1, :cond_13

    .line 553
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "RC 1.0"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "execute response : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1

    .line 580
    :goto_9
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "RC 1.0"

    const-string v3, "execute"

    const-string v4, "END"

    invoke-static {v2, v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    return-object v1

    .line 556
    :cond_13
    :try_start_c
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "RC 1.0"

    const-string v3, "execute response : null"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_1

    goto :goto_9

    .line 568
    :catch_6
    move-exception v2

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_4
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/osp/common/util/g;)Lorg/apache/http/HttpResponse;
    .locals 1

    .prologue
    .line 222
    sget-object v0, Lcom/osp/http/impl/f;->a:Lcom/osp/http/impl/f;

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/osp/http/impl/d;->a(Lcom/osp/http/impl/f;Ljava/lang/String;Ljava/lang/String;Lcom/osp/common/util/g;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 596
    if-eqz p1, :cond_0

    .line 598
    iget-object v0, p0, Lcom/osp/http/impl/d;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 600
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 614
    if-eqz p1, :cond_0

    .line 616
    iget-object v0, p0, Lcom/osp/http/impl/d;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 617
    iget-object v0, p0, Lcom/osp/http/impl/d;->b:Lcom/osp/common/util/e;

    invoke-virtual {v0, p1, p2}, Lcom/osp/common/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 619
    :cond_0
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 628
    if-eqz p1, :cond_0

    .line 630
    iget-object v0, p0, Lcom/osp/http/impl/d;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 632
    :cond_0
    return-void
.end method
