.class public final Lcom/osp/http/impl/b;
.super Ljava/lang/Object;
.source "ErrorResultHandler.java"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/osp/http/impl/b;->a:Landroid/content/Context;

    .line 41
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;)V
    .locals 3

    .prologue
    .line 52
    :try_start_0
    new-instance v0, Lcom/osp/http/impl/a;

    invoke-direct {v0}, Lcom/osp/http/impl/a;-><init>()V

    .line 53
    invoke-virtual {v0, p1}, Lcom/osp/http/impl/a;->a(Ljava/io/InputStream;)V

    .line 55
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/osp/http/impl/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/osp/http/impl/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 57
    const-string v1, "SSO_8005"

    invoke-virtual {v0}, Lcom/osp/http/impl/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 59
    new-instance v1, Lcom/osp/security/time/a;

    iget-object v2, p0, Lcom/osp/http/impl/b;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/osp/security/time/a;-><init>(Landroid/content/Context;)V

    .line 60
    invoke-virtual {v1}, Lcom/osp/security/time/a;->b()V

    .line 62
    :cond_0
    new-instance v1, Lcom/osp/http/impl/ErrorResultException;

    invoke-virtual {v0}, Lcom/osp/http/impl/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/osp/http/impl/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/osp/http/impl/ErrorResultException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 63
    :catch_0
    move-exception v0

    .line 65
    const-string v1, "PRT_3009"

    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 67
    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->printStackTrace()V

    .line 69
    :cond_1
    throw v0

    .line 70
    :catch_1
    move-exception v0

    .line 72
    new-instance v1, Lcom/osp/http/impl/ErrorResultException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/osp/http/impl/ErrorResultException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
