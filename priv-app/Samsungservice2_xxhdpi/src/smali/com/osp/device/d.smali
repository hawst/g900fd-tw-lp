.class public final Lcom/osp/device/d;
.super Ljava/lang/Object;
.source "DeviceRegistrationManager.java"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lcom/osp/device/d;->a:Landroid/content/Context;

    .line 83
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/osp/device/a;
    .locals 18

    .prologue
    .line 237
    new-instance v7, Lcom/osp/device/a;

    invoke-direct {v7}, Lcom/osp/device/a;-><init>()V

    .line 238
    invoke-static/range {p0 .. p0}, Lcom/osp/device/d;->d(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v8

    .line 239
    sget-object v1, Lcom/osp/device/c;->a:Lcom/osp/device/b;

    invoke-virtual {v1}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v3

    .line 241
    invoke-static/range {p0 .. p0}, Lcom/osp/device/d;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 243
    if-nez v1, :cond_0

    .line 246
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "DRM"

    const-string v4, "DeviceManager : device is null"

    invoke-static {v2, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v1

    .line 254
    :goto_0
    const-string v4, "PHONE DEVICE"

    .line 256
    const-string v1, "Galaxy Nexus"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 258
    const-string v1, "GT-I9250"

    move-object v6, v1

    .line 261
    :goto_1
    if-nez v6, :cond_2

    .line 263
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "DeviceManager\tdeviceModelID: null\t"

    invoke-static {v1}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 264
    new-instance v1, Lcom/osp/device/DeviceException;

    const-string v2, "Can\'t get the device model ID."

    invoke-direct {v1, v2}, Lcom/osp/device/DeviceException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 248
    :cond_0
    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "000000000000000"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, " "

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 250
    :cond_1
    sget-object v1, Lcom/osp/device/c;->a:Lcom/osp/device/b;

    invoke-static/range {p0 .. p0}, Lcom/osp/device/b;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_0

    .line 273
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/osp/device/d;->e(Landroid/content/Context;)I

    move-result v9

    .line 279
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "DRM"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "PhoneType : "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    packed-switch v9, :pswitch_data_0

    .line 330
    const-string v3, "NONE"

    .line 331
    const/4 v1, 0x0

    move-object/from16 v17, v3

    move-object v3, v4

    move-object v4, v2

    move-object/from16 v2, v17

    .line 342
    :goto_2
    invoke-static {}, Lcom/osp/app/util/r;->e()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 344
    const-string v2, "IMEI"

    .line 347
    sget-object v1, Lcom/osp/device/c;->a:Lcom/osp/device/b;

    invoke-virtual {v1}, Lcom/osp/device/b;->h()Ljava/lang/String;

    move-result-object v1

    .line 348
    const-string v5, "unknown"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 350
    const-class v1, Landroid/telephony/TelephonyManager;

    const-string v5, "getImeiInCDMAGSMPhone"

    const/4 v10, 0x0

    invoke-virtual {v1, v5, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v5, 0x0

    invoke-virtual {v1, v8, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 365
    :cond_3
    sget-object v5, Lcom/osp/device/c;->a:Lcom/osp/device/b;

    invoke-virtual {v5}, Lcom/osp/device/b;->c()Ljava/lang/String;

    move-result-object v5

    const-string v10, "KDI"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    const/4 v5, 0x1

    :goto_3
    if-nez v5, :cond_4

    invoke-static {}, Lcom/osp/app/util/r;->h()Z

    move-result v5

    if-eqz v5, :cond_1c

    .line 367
    :cond_4
    if-nez v4, :cond_5

    .line 369
    sget-object v1, Lcom/osp/device/c;->a:Lcom/osp/device/b;

    invoke-static/range {p0 .. p0}, Lcom/osp/device/b;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 371
    :cond_5
    const-string v2, "MEID"

    .line 372
    invoke-static {}, Lcom/osp/common/util/d;->a()Lcom/osp/common/util/d;

    invoke-static {v4}, Lcom/osp/common/util/d;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    .line 375
    :goto_4
    if-nez v5, :cond_6

    .line 377
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "DeviceManager\tdeviceUniqueID: \tnull\t"

    invoke-static {v1}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 380
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 381
    sget-object v11, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 382
    if-nez v11, :cond_e

    .line 384
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "DeviceManager\tdeviceName: \tnull\t"

    invoke-static {v1}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 385
    new-instance v1, Lcom/osp/device/DeviceException;

    const-string v2, "Can\'t get the device name."

    invoke-direct {v1, v2}, Lcom/osp/device/DeviceException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 283
    :pswitch_0
    if-nez v2, :cond_7

    .line 285
    sget-object v1, Lcom/osp/device/c;->a:Lcom/osp/device/b;

    invoke-static/range {p0 .. p0}, Lcom/osp/device/b;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 287
    :cond_7
    const-string v3, "IMEI"

    .line 288
    invoke-static {}, Lcom/osp/common/util/d;->a()Lcom/osp/common/util/d;

    invoke-static {v2}, Lcom/osp/common/util/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v17, v3

    move-object v3, v4

    move-object v4, v2

    move-object/from16 v2, v17

    .line 289
    goto/16 :goto_2

    .line 291
    :pswitch_1
    if-nez v2, :cond_8

    .line 293
    sget-object v1, Lcom/osp/device/c;->a:Lcom/osp/device/b;

    invoke-static/range {p0 .. p0}, Lcom/osp/device/b;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 295
    :cond_8
    const-string v3, "MEID"

    .line 296
    invoke-static {}, Lcom/osp/common/util/d;->a()Lcom/osp/common/util/d;

    invoke-static {v2}, Lcom/osp/common/util/d;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v17, v3

    move-object v3, v4

    move-object v4, v2

    move-object/from16 v2, v17

    .line 297
    goto/16 :goto_2

    .line 299
    :pswitch_2
    if-eqz v2, :cond_a

    .line 302
    const-class v1, Landroid/telephony/TelephonyManager;

    const-string v3, "getCurrentPhoneType"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v8, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 304
    const-string v4, "PHONE DEVICE"

    .line 306
    const/4 v3, 0x2

    if-ne v1, v3, :cond_9

    .line 308
    const-string v3, "MEID"

    .line 309
    invoke-static {}, Lcom/osp/common/util/d;->a()Lcom/osp/common/util/d;

    invoke-static {v2}, Lcom/osp/common/util/d;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v17, v3

    move-object v3, v4

    move-object v4, v2

    move-object/from16 v2, v17

    goto/16 :goto_2

    .line 313
    :cond_9
    const-string v3, "IMEI"

    .line 314
    invoke-static {}, Lcom/osp/common/util/d;->a()Lcom/osp/common/util/d;

    invoke-static {v2}, Lcom/osp/common/util/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v17, v3

    move-object v3, v4

    move-object v4, v2

    move-object/from16 v2, v17

    .line 316
    goto/16 :goto_2

    .line 319
    :cond_a
    sget-object v1, Lcom/osp/device/c;->a:Lcom/osp/device/b;

    invoke-virtual {v1}, Lcom/osp/device/b;->f()Ljava/lang/String;

    move-result-object v1

    .line 320
    if-eqz v1, :cond_b

    const-string v2, "unknown"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 322
    :cond_b
    sget-object v1, Lcom/osp/device/c;->a:Lcom/osp/device/b;

    invoke-static/range {p0 .. p0}, Lcom/osp/device/b;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 324
    :cond_c
    const-string v4, "PHONE DEVICE"

    .line 325
    const-string v3, "TWID"

    .line 326
    invoke-static {}, Lcom/osp/common/util/d;->a()Lcom/osp/common/util/d;

    invoke-static {v1}, Lcom/osp/common/util/d;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v17, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v1

    move-object/from16 v1, v17

    .line 328
    goto/16 :goto_2

    .line 365
    :cond_d
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 389
    :cond_e
    sget-object v1, Lcom/osp/device/c;->a:Lcom/osp/device/b;

    invoke-virtual {v1}, Lcom/osp/device/b;->d()Ljava/lang/String;

    move-result-object v1

    .line 396
    sget-object v2, Lcom/osp/device/c;->a:Lcom/osp/device/b;

    invoke-virtual {v2}, Lcom/osp/device/b;->g()Ljava/lang/String;

    move-result-object v2

    .line 398
    if-eqz v1, :cond_f

    const-string v4, "unknown"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_f

    const-string v4, ""

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_17

    :cond_f
    if-eqz v2, :cond_10

    const-string v4, ""

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 403
    :cond_10
    const-string v1, ""

    move-object v4, v1

    .line 409
    :goto_5
    packed-switch v9, :pswitch_data_1

    .line 440
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v1

    .line 451
    :goto_6
    invoke-static {}, Lcom/osp/app/util/r;->e()Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 454
    sget-object v1, Lcom/osp/device/c;->a:Lcom/osp/device/b;

    invoke-virtual {v1}, Lcom/osp/device/b;->j()Ljava/lang/String;

    move-result-object v1

    .line 455
    if-nez v1, :cond_1b

    .line 457
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 463
    :goto_7
    if-nez v2, :cond_11

    .line 465
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "DeviceManager\tphoneNumberText: \tnull\t"

    invoke-static {v1}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 467
    :cond_11
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static/range {p0 .. p0}, Lcom/osp/common/util/i;->o(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 472
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static/range {p0 .. p0}, Lcom/osp/common/util/i;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    .line 473
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static/range {p0 .. p0}, Lcom/osp/common/util/i;->k(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    .line 476
    sget-object v1, Lcom/osp/device/c;->a:Lcom/osp/device/b;

    invoke-virtual {v1}, Lcom/osp/device/b;->c()Ljava/lang/String;

    move-result-object v1

    .line 477
    if-nez v1, :cond_12

    .line 479
    const-string v1, "none"

    .line 486
    :cond_12
    sget-object v13, Lcom/osp/device/c;->a:Lcom/osp/device/b;

    invoke-virtual {v13}, Lcom/osp/device/b;->k()Ljava/lang/String;

    move-result-object v13

    .line 488
    const-string v14, "SHV-E120S"

    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_13

    const-string v14, "E120S"

    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_13

    const-string v14, "SHV-E110S"

    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_13

    const-string v14, "E110S"

    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_13

    const-string v14, "SHV-E140S"

    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_13

    const-string v14, "E140S"

    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_18

    .line 492
    :cond_13
    const-string v1, "SKT"

    .line 507
    :cond_14
    :goto_8
    const-string v14, "TFG"

    invoke-virtual {v1, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_16

    .line 509
    if-eqz v13, :cond_15

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v14, 0xe

    if-lt v1, v14, :cond_15

    .line 511
    const/16 v1, 0xb

    const/16 v14, 0xe

    invoke-virtual {v13, v1, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 512
    const-string v13, "TMM UFN UFU COB CHT SAM VMT TGU SAL NBS PBS EBE CRM"

    invoke-virtual {v13, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_16

    .line 514
    :cond_15
    const-string v1, "TFG"

    .line 526
    :cond_16
    sget-object v13, Lcom/osp/device/c;->a:Lcom/osp/device/b;

    invoke-virtual {v13}, Lcom/osp/device/b;->m()Ljava/lang/String;

    move-result-object v13

    .line 528
    const-string v14, "DEVICE_PHYSICAL_ADDRESS_TEXT"

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    .line 529
    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v15

    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/msc/c/a;->b()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v10}, Lcom/msc/c/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 530
    const-string v16, "DEVICE_PHYSICAL_ADDRESS_TEXT"

    move-object/from16 v0, v16

    invoke-interface {v14, v0, v15}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 531
    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 533
    invoke-virtual {v7, v3}, Lcom/osp/device/a;->a(Ljava/lang/String;)V

    .line 534
    invoke-virtual {v7, v6}, Lcom/osp/device/a;->b(Ljava/lang/String;)V

    .line 535
    invoke-virtual {v7, v5}, Lcom/osp/device/a;->c(Ljava/lang/String;)V

    .line 536
    invoke-virtual {v7, v10}, Lcom/osp/device/a;->d(Ljava/lang/String;)V

    .line 537
    invoke-virtual {v7, v11}, Lcom/osp/device/a;->j(Ljava/lang/String;)V

    .line 538
    invoke-virtual {v7, v4}, Lcom/osp/device/a;->k(Ljava/lang/String;)V

    .line 539
    invoke-virtual {v7, v2}, Lcom/osp/device/a;->e(Ljava/lang/String;)V

    .line 540
    invoke-virtual {v7, v8}, Lcom/osp/device/a;->f(Ljava/lang/String;)V

    .line 541
    invoke-virtual {v7, v9}, Lcom/osp/device/a;->g(Ljava/lang/String;)V

    .line 542
    invoke-virtual {v7, v12}, Lcom/osp/device/a;->h(Ljava/lang/String;)V

    .line 543
    invoke-virtual {v7, v1}, Lcom/osp/device/a;->i(Ljava/lang/String;)V

    .line 544
    invoke-virtual {v7, v13}, Lcom/osp/device/a;->l(Ljava/lang/String;)V

    .line 545
    return-object v7

    .line 406
    :cond_17
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    goto/16 :goto_5

    .line 419
    :pswitch_3
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_6

    .line 422
    :pswitch_4
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_6

    .line 431
    :pswitch_5
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_6

    .line 493
    :cond_18
    const-string v14, "SHV-E120K"

    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_19

    const-string v14, "E120K"

    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_19

    const-string v14, "SHV-E140K"

    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_19

    const-string v14, "E140K"

    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1a

    .line 495
    :cond_19
    const-string v1, "KT"

    goto/16 :goto_8

    .line 496
    :cond_1a
    invoke-static {}, Lcom/osp/app/util/r;->e()Z

    move-result v14

    if-eqz v14, :cond_14

    .line 501
    const-string v1, "LG"

    goto/16 :goto_8

    :cond_1b
    move-object v2, v1

    goto/16 :goto_7

    :cond_1c
    move-object v5, v1

    goto/16 :goto_4

    :cond_1d
    move-object v6, v3

    goto/16 :goto_1

    :cond_1e
    move-object v2, v1

    goto/16 :goto_0

    .line 280
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 409
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 672
    invoke-static {p0}, Lcom/osp/device/d;->d(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v4

    .line 673
    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    .line 686
    :try_start_0
    invoke-static {}, Lcom/osp/app/util/r;->e()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 689
    sget-object v0, Lcom/osp/device/c;->a:Lcom/osp/device/b;

    invoke-virtual {v0}, Lcom/osp/device/b;->h()Ljava/lang/String;

    move-result-object v1

    .line 690
    const-string v0, "unknown"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 692
    const-class v0, Landroid/telephony/TelephonyManager;

    const-string v2, "getImeiInCDMAGSMPhone"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v4, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 729
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 731
    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 732
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "DRM"

    const-string v2, "Get default deviceId"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 735
    :cond_0
    return-object v0

    .line 704
    :cond_1
    :try_start_1
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x13

    if-ge v3, v5, :cond_3

    sget-object v3, Lcom/osp/device/c;->a:Lcom/osp/device/b;

    invoke-virtual {v3}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x3

    new-array v6, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v7, "SM-G3812"

    aput-object v7, v6, v3

    const/4 v3, 0x1

    const-string v7, "SM-G3502U"

    aput-object v7, v6, v3

    const/4 v3, 0x2

    const-string v7, "SM-G3502"

    aput-object v7, v6, v3

    array-length v7, v6

    move v3, v2

    :goto_1
    if-ge v3, v7, :cond_3

    aget-object v8, v6, v3

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "isGetDeviceIdDsExcpetedModel : true"

    invoke-static {v3}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    move v3, v0

    :goto_2
    if-eqz v3, :cond_4

    .line 706
    const-class v0, Landroid/telephony/TelephonyManager;

    const-string v2, "getDeviceIdDs"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v3, v5

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v3

    invoke-virtual {v0, v4, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 704
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    move v3, v2

    goto :goto_2

    .line 716
    :cond_4
    invoke-static {}, Lcom/osp/app/util/r;->o()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 718
    const-string v0, "android.telephony.MSimTelephonyManager"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 719
    const-string v2, "getDeviceId"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v3, v5

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const-string v2, "phone_msim"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto/16 :goto_0

    .line 720
    :cond_5
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x13

    if-lt v3, v5, :cond_7

    sget-object v3, Lcom/osp/device/c;->a:Lcom/osp/device/b;

    invoke-virtual {v3}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x7

    new-array v6, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v7, "GT-N7102"

    aput-object v7, v6, v3

    const/4 v3, 0x1

    const-string v7, "GT-I9502"

    aput-object v7, v6, v3

    const/4 v3, 0x2

    const-string v7, "SM-N9002"

    aput-object v7, v6, v3

    const/4 v3, 0x3

    const-string v7, "SM-G9052"

    aput-object v7, v6, v3

    const/4 v3, 0x4

    const-string v7, "SM-G3812"

    aput-object v7, v6, v3

    const/4 v3, 0x5

    const-string v7, "SM-G3502U"

    aput-object v7, v6, v3

    const/4 v3, 0x6

    const-string v7, "SM-G3502"

    aput-object v7, v6, v3

    array-length v7, v6

    move v3, v2

    :goto_3
    if-ge v3, v7, :cond_7

    aget-object v8, v6, v3

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "isImeiInCDMAGSMExceptedModel : true"

    invoke-static {v2}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    :goto_4
    if-eqz v0, :cond_8

    .line 722
    const-class v0, Landroid/telephony/TelephonyManager;

    const-string v2, "getImeiInCDMAGSMPhone"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v4, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 720
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_7
    move v0, v2

    goto :goto_4

    .line 724
    :catch_0
    move-exception v0

    move-object v9, v0

    move-object v0, v1

    move-object v1, v9

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    :cond_8
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public static c(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 886
    const/4 v0, 0x0

    .line 889
    :try_start_0
    invoke-static {p0}, Lcom/osp/device/d;->b(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 895
    :goto_0
    if-nez v0, :cond_1

    .line 897
    sget-object v0, Lcom/osp/device/c;->a:Lcom/osp/device/b;

    invoke-virtual {v0}, Lcom/osp/device/b;->f()Ljava/lang/String;

    move-result-object v0

    .line 898
    if-eqz v0, :cond_0

    const-string v1, "unknown"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 900
    :cond_0
    const-string v0, "000000000000000"

    .line 904
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0

    .line 890
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private static d(Landroid/content/Context;)Landroid/telephony/TelephonyManager;
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 556
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 577
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x13

    if-ge v4, v5, :cond_2

    sget-object v4, Lcom/osp/device/c;->a:Lcom/osp/device/b;

    invoke-virtual {v4}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v5

    const/4 v4, 0x4

    new-array v6, v4, [Ljava/lang/String;

    const-string v4, "GT-N7102"

    aput-object v4, v6, v3

    const-string v4, "GT-I9502"

    aput-object v4, v6, v2

    const/4 v4, 0x2

    const-string v7, "SM-N9002"

    aput-object v7, v6, v4

    const/4 v4, 0x3

    const-string v7, "SM-G9052"

    aput-object v7, v6, v4

    array-length v7, v6

    move v4, v3

    :goto_0
    if-ge v4, v7, :cond_2

    aget-object v8, v6, v4

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "isGetFirstExcpetedModel : true"

    invoke-static {v3}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    :goto_1
    if-eqz v2, :cond_0

    .line 581
    :try_start_0
    const-class v2, Landroid/telephony/TelephonyManager;

    const-string v3, "getFirst"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/telephony/TelephonyManager;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3

    .line 608
    :cond_0
    :goto_2
    if-nez v1, :cond_3

    .line 610
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 611
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "DRM"

    const-string v3, "Get default TelephonyManager"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    if-nez v1, :cond_3

    .line 614
    new-instance v1, Lcom/osp/device/DeviceException;

    const-string v2, "Can\'t get the telephony manager."

    invoke-direct {v1, v2}, Lcom/osp/device/DeviceException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 577
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1

    .line 583
    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 586
    :catch_1
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_2

    .line 589
    :catch_2
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_2

    .line 592
    :catch_3
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_2

    .line 617
    :cond_3
    return-object v1
.end method

.method private static e(Landroid/content/Context;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 628
    .line 639
    :try_start_0
    invoke-static {}, Lcom/osp/app/util/r;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 641
    const-string v0, "android.telephony.MSimTelephonyManager"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 642
    const-string v2, "getCurrentPhoneType"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const-string v2, "phone_msim"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 652
    :goto_0
    if-nez v0, :cond_0

    .line 654
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 655
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    .line 656
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "DRM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Get default PhoneType : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    :cond_0
    return v0

    .line 645
    :cond_1
    :try_start_1
    invoke-static {p0}, Lcom/osp/device/d;->d(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    .line 646
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    goto :goto_0

    .line 648
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 95
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DRM"

    const-string v2, "requestDeviceRegistration"

    const-string v3, "START"

    invoke-static {v0, v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :try_start_0
    new-instance v3, Lcom/osp/security/identity/d;

    iget-object v0, p0, Lcom/osp/device/d;->a:Landroid/content/Context;

    invoke-direct {v3, v0}, Lcom/osp/security/identity/d;-><init>(Landroid/content/Context;)V

    .line 103
    invoke-virtual {v3}, Lcom/osp/security/identity/d;->a()Ljava/lang/String;

    move-result-object v0

    .line 105
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 107
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_0
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/osp/device/DeviceException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 190
    :catch_0
    move-exception v0

    .line 192
    :goto_0
    :try_start_1
    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->printStackTrace()V

    .line 193
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "DRM"

    const-string v3, "requestDeviceRegistration ErrorResultException"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    new-instance v2, Lcom/osp/device/DeviceException;

    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v0, v4}, Lcom/osp/device/DeviceException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 214
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    .line 218
    :try_start_2
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 214
    :cond_0
    :goto_2
    throw v0

    .line 110
    :cond_1
    :try_start_3
    iget-object v2, p0, Lcom/osp/device/d;->a:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/msc/c/l;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 112
    new-instance v4, Lcom/osp/http/impl/d;

    iget-object v5, p0, Lcom/osp/device/d;->a:Landroid/content/Context;

    invoke-direct {v4, v5, p1, p2}, Lcom/osp/http/impl/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    new-instance v5, Lcom/osp/device/e;

    invoke-direct {v5}, Lcom/osp/device/e;-><init>()V

    .line 116
    iget-object v6, p0, Lcom/osp/device/d;->a:Landroid/content/Context;

    invoke-static {v6}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v6

    .line 118
    invoke-virtual {v6}, Lcom/osp/device/a;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/osp/device/e;->b(Ljava/lang/String;)V

    .line 119
    invoke-virtual {v6}, Lcom/osp/device/a;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/osp/device/e;->c(Ljava/lang/String;)V

    .line 120
    invoke-virtual {v6}, Lcom/osp/device/a;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/osp/device/e;->d(Ljava/lang/String;)V

    .line 121
    invoke-virtual {v6}, Lcom/osp/device/a;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/osp/device/e;->e(Ljava/lang/String;)V

    .line 122
    invoke-virtual {v6}, Lcom/osp/device/a;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/osp/device/e;->k(Ljava/lang/String;)V

    .line 123
    invoke-virtual {v6}, Lcom/osp/device/a;->k()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/osp/device/e;->l(Ljava/lang/String;)V

    .line 124
    invoke-virtual {v6}, Lcom/osp/device/a;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/osp/device/e;->f(Ljava/lang/String;)V

    .line 125
    invoke-virtual {v6}, Lcom/osp/device/a;->f()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/osp/device/e;->g(Ljava/lang/String;)V

    .line 126
    invoke-virtual {v6}, Lcom/osp/device/a;->g()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/osp/device/e;->h(Ljava/lang/String;)V

    .line 127
    invoke-virtual {v6}, Lcom/osp/device/a;->h()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/osp/device/e;->i(Ljava/lang/String;)V

    .line 128
    invoke-virtual {v6}, Lcom/osp/device/a;->i()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/osp/device/e;->j(Ljava/lang/String;)V

    .line 129
    invoke-static {}, Lcom/osp/common/util/d;->a()Lcom/osp/common/util/d;

    move-result-object v6

    iget-object v7, p0, Lcom/osp/device/d;->a:Landroid/content/Context;

    invoke-virtual {v6, v7}, Lcom/osp/common/util/d;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/osp/device/e;->a(Ljava/lang/String;)V

    .line 130
    sget-object v6, Lcom/osp/device/c;->a:Lcom/osp/device/b;

    iget-object v6, p0, Lcom/osp/device/d;->a:Landroid/content/Context;

    invoke-static {v6}, Lcom/osp/device/b;->q(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/osp/device/e;->m(Ljava/lang/String;)V

    .line 131
    sget-object v6, Lcom/osp/device/c;->a:Lcom/osp/device/b;

    invoke-static {}, Lcom/osp/device/b;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/osp/device/e;->n(Ljava/lang/String;)V

    .line 133
    invoke-virtual {v5}, Lcom/osp/device/e;->a()Ljava/lang/String;

    move-result-object v5

    .line 135
    new-instance v6, Lcom/osp/common/property/a;

    iget-object v7, p0, Lcom/osp/device/d;->a:Landroid/content/Context;

    invoke-direct {v6, v7}, Lcom/osp/common/property/a;-><init>(Landroid/content/Context;)V

    .line 138
    const-string v7, "userID"

    invoke-virtual {v4, v7, v0}, Lcom/osp/http/impl/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "=====================REQUEST===================="

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 141
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 142
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 143
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "================================================"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 145
    sget-object v0, Lcom/osp/common/util/g;->c:Lcom/osp/common/util/g;

    invoke-virtual {v4, v2, v5, v0}, Lcom/osp/http/impl/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/osp/common/util/g;)Lorg/apache/http/HttpResponse;

    move-result-object v4

    .line 148
    const/4 v0, 0x0

    .line 151
    if-eqz v4, :cond_8

    .line 153
    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_3
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/osp/device/DeviceException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    .line 154
    if-eqz v2, :cond_2

    .line 156
    :try_start_4
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 158
    :cond_2
    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    .line 159
    if-eqz v4, :cond_7

    .line 161
    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I
    :try_end_4
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_4 .. :try_end_4} :catch_7
    .catch Lcom/osp/device/DeviceException; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v0

    move-object v8, v1

    move-object v1, v2

    move-object v2, v8

    .line 165
    :goto_3
    const/16 v4, 0xc8

    if-ne v0, v4, :cond_6

    .line 167
    if-nez v2, :cond_3

    .line 169
    :try_start_5
    new-instance v0, Ljava/lang/Exception;

    const-string v2, "Response content is null."

    invoke-direct {v0, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lcom/osp/device/DeviceException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 199
    :catch_1
    move-exception v0

    .line 201
    :goto_4
    :try_start_6
    invoke-virtual {v0}, Lcom/osp/device/DeviceException;->printStackTrace()V

    .line 202
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "DRM"

    const-string v3, "requestDeviceRegistration DeviceException"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    new-instance v2, Lcom/osp/device/DeviceException;

    invoke-virtual {v0}, Lcom/osp/device/DeviceException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/osp/device/DeviceException;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v0, v4}, Lcom/osp/device/DeviceException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 171
    :cond_3
    :try_start_7
    new-instance v0, Lcom/osp/device/f;

    invoke-direct {v0}, Lcom/osp/device/f;-><init>()V

    .line 172
    invoke-virtual {v0, v2}, Lcom/osp/device/f;->a(Ljava/io/InputStream;)V

    .line 174
    invoke-virtual {v0}, Lcom/osp/device/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/osp/security/identity/d;->b(Ljava/lang/String;)V

    .line 177
    const-string v0, "device.registration.appid"

    invoke-virtual {v6, v0}, Lcom/osp/common/property/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 179
    const-string v0, "device.registration.appid"

    invoke-virtual {v6, v0}, Lcom/osp/common/property/a;->c(Ljava/lang/String;)V

    .line 181
    :cond_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DRM"

    const-string v2, "save appid in property."

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const-string v0, "device.registration.appid"

    invoke-virtual {v6, v0, p1}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Lcom/osp/device/DeviceException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 214
    :goto_5
    if-eqz v1, :cond_5

    .line 218
    :try_start_8
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 226
    :cond_5
    :goto_6
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DRM"

    const-string v1, "requestDeviceRegistration"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    return-void

    .line 186
    :cond_6
    :try_start_9
    new-instance v0, Lcom/osp/http/impl/b;

    iget-object v3, p0, Lcom/osp/device/d;->a:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/osp/http/impl/b;-><init>(Landroid/content/Context;)V

    .line 187
    invoke-virtual {v0, v2}, Lcom/osp/http/impl/b;->a(Ljava/io/InputStream;)V
    :try_end_9
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Lcom/osp/device/DeviceException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_5

    .line 204
    :catch_2
    move-exception v0

    .line 206
    :goto_7
    :try_start_a
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 207
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "DRM"

    const-string v3, "requestDeviceRegistration Exception"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    new-instance v2, Lcom/osp/device/DeviceException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/osp/device/DeviceException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 219
    :catch_3
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 222
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "DRM"

    const-string v2, "requestDeviceRegistration IOException"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 219
    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 222
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DRM"

    const-string v1, "requestDeviceRegistration IOException"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 214
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto/16 :goto_1

    .line 204
    :catch_5
    move-exception v0

    move-object v1, v2

    goto :goto_7

    .line 199
    :catch_6
    move-exception v0

    move-object v1, v2

    goto/16 :goto_4

    .line 190
    :catch_7
    move-exception v0

    move-object v1, v2

    goto/16 :goto_0

    :cond_7
    move-object v8, v1

    move-object v1, v2

    move-object v2, v8

    goto/16 :goto_3

    :cond_8
    move-object v2, v1

    goto/16 :goto_3
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 748
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DRM"

    const-string v2, "requestDeviceUnregistration"

    const-string v3, "START"

    invoke-static {v0, v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    :try_start_0
    new-instance v0, Lcom/osp/security/identity/d;

    iget-object v2, p0, Lcom/osp/device/d;->a:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/osp/security/identity/d;-><init>(Landroid/content/Context;)V

    .line 756
    invoke-virtual {v0}, Lcom/osp/security/identity/d;->a()Ljava/lang/String;

    move-result-object v2

    .line 757
    invoke-virtual {v0}, Lcom/osp/security/identity/d;->f()Ljava/lang/String;

    move-result-object v3

    .line 759
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 761
    :cond_0
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_0
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 820
    :catch_0
    move-exception v0

    .line 822
    :goto_0
    :try_start_1
    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->printStackTrace()V

    .line 823
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "DRM"

    const-string v3, "requestDeviceUnregistration ErrorResultException"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 828
    new-instance v2, Lcom/osp/device/DeviceException;

    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/osp/http/impl/ErrorResultException;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v0, v4}, Lcom/osp/device/DeviceException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 839
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_1

    .line 843
    :try_start_2
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 839
    :cond_1
    :goto_2
    throw v0

    .line 764
    :cond_2
    :try_start_3
    iget-object v4, p0, Lcom/osp/device/d;->a:Landroid/content/Context;

    invoke-static {v4, v2, v3}, Lcom/msc/c/l;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 766
    new-instance v5, Lcom/osp/http/impl/d;

    iget-object v6, p0, Lcom/osp/device/d;->a:Landroid/content/Context;

    invoke-direct {v5, v6, p1, p2}, Lcom/osp/http/impl/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 768
    const-string v6, "userID"

    invoke-virtual {v5, v6, v2}, Lcom/osp/http/impl/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    const-string v6, "deviceID"

    invoke-virtual {v5, v6, v3}, Lcom/osp/http/impl/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/osp/security/identity/d;->f()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    .line 776
    :cond_3
    new-instance v0, Lcom/osp/device/DeviceException;

    const-string v2, "identityManager.getUserID() = null || identityManager.getUserDeviceID() == null "

    invoke-direct {v0, v2}, Lcom/osp/device/DeviceException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 829
    :catch_1
    move-exception v0

    .line 831
    :goto_3
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 832
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "DRM"

    const-string v3, "requestDeviceUnregistration Exception"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 836
    new-instance v2, Lcom/osp/device/DeviceException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/osp/device/DeviceException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 781
    :cond_4
    :try_start_5
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 783
    sget-object v0, Lcom/osp/http/impl/f;->d:Lcom/osp/http/impl/f;

    const/4 v2, 0x0

    sget-object v3, Lcom/osp/common/util/g;->c:Lcom/osp/common/util/g;

    invoke-virtual {v5, v0, v4, v2, v3}, Lcom/osp/http/impl/d;->a(Lcom/osp/http/impl/f;Ljava/lang/String;Ljava/lang/String;Lcom/osp/common/util/g;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    move-object v3, v0

    .line 787
    :goto_4
    const/4 v0, 0x0

    .line 790
    if-eqz v3, :cond_a

    .line 792
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_5
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v2

    .line 793
    if-eqz v2, :cond_5

    .line 795
    :try_start_6
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 797
    :cond_5
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    .line 798
    if-eqz v3, :cond_9

    .line 800
    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I
    :try_end_6
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result v0

    move-object v7, v2

    move-object v2, v1

    move-object v1, v7

    .line 804
    :goto_5
    const/16 v3, 0xc8

    if-ne v0, v3, :cond_8

    .line 806
    if-nez v2, :cond_6

    .line 808
    :try_start_7
    new-instance v0, Ljava/lang/Exception;

    const-string v2, "Response content is null."

    invoke-direct {v0, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 811
    :cond_6
    new-instance v0, Lcom/osp/common/property/a;

    iget-object v2, p0, Lcom/osp/device/d;->a:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/osp/common/property/a;-><init>(Landroid/content/Context;)V

    .line 812
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "DRM"

    const-string v3, "remove appid form property."

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    const-string v2, "device.registration.appid"

    invoke-virtual {v0, v2}, Lcom/osp/common/property/a;->c(Ljava/lang/String;)V
    :try_end_7
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 839
    :goto_6
    if-eqz v1, :cond_7

    .line 843
    :try_start_8
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    .line 851
    :cond_7
    :goto_7
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DRM"

    const-string v1, "requestDeviceUnregistration"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 852
    return-void

    .line 817
    :cond_8
    :try_start_9
    new-instance v0, Lcom/osp/http/impl/b;

    iget-object v3, p0, Lcom/osp/device/d;->a:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/osp/http/impl/b;-><init>(Landroid/content/Context;)V

    .line 818
    invoke-virtual {v0, v2}, Lcom/osp/http/impl/b;->a(Ljava/io/InputStream;)V
    :try_end_9
    .catch Lcom/osp/http/impl/ErrorResultException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_6

    .line 844
    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 847
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "DRM"

    const-string v2, "requestDeviceUnregistration IOException"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 844
    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 847
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DRM"

    const-string v1, "requestDeviceUnregistration IOException"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 839
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto/16 :goto_1

    .line 829
    :catch_4
    move-exception v0

    move-object v1, v2

    goto/16 :goto_3

    .line 820
    :catch_5
    move-exception v0

    move-object v1, v2

    goto/16 :goto_0

    :cond_9
    move-object v7, v2

    move-object v2, v1

    move-object v1, v7

    goto :goto_5

    :cond_a
    move-object v2, v1

    goto :goto_5

    :cond_b
    move-object v3, v1

    goto/16 :goto_4
.end method
