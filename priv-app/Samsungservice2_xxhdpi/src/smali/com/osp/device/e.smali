.class public final Lcom/osp/device/e;
.super Ljava/lang/Object;
.source "DeviceRegistrationRequest.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/e;->a:Ljava/lang/String;

    .line 106
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/e;->b:Ljava/lang/String;

    .line 107
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/e;->c:Ljava/lang/String;

    .line 108
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/e;->d:Ljava/lang/String;

    .line 109
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/e;->e:Ljava/lang/String;

    .line 110
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/e;->f:Ljava/lang/String;

    .line 111
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/e;->g:Ljava/lang/String;

    .line 112
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/e;->h:Ljava/lang/String;

    .line 113
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/e;->i:Ljava/lang/String;

    .line 114
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/e;->j:Ljava/lang/String;

    .line 116
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/e;->n:Ljava/lang/String;

    .line 117
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/e;->l:Ljava/lang/String;

    .line 118
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/e;->m:Ljava/lang/String;

    .line 119
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 401
    :try_start_0
    new-instance v0, Lcom/msc/c/q;

    invoke-direct {v0}, Lcom/msc/c/q;-><init>()V

    .line 407
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 409
    invoke-virtual {v0, v1}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 410
    const-string v2, "UTF-8"

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 411
    const-string v2, ""

    const-string v3, "userDeviceCreate"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    const-string v2, ""

    const-string v3, "deviceTypeCode"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    iget-object v2, p0, Lcom/osp/device/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 414
    const-string v2, ""

    const-string v3, "deviceTypeCode"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    const-string v2, ""

    const-string v3, "deviceModelID"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    iget-object v2, p0, Lcom/osp/device/e;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 418
    const-string v2, ""

    const-string v3, "deviceModelID"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    const-string v2, ""

    const-string v3, "deviceUniqueID"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    iget-object v2, p0, Lcom/osp/device/e;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 422
    const-string v2, ""

    const-string v3, "deviceUniqueID"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    const-string v2, ""

    const-string v3, "devicePhysicalAddressText"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    iget-object v2, p0, Lcom/osp/device/e;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 426
    const-string v2, ""

    const-string v3, "devicePhysicalAddressText"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    const-string v2, ""

    const-string v3, "deviceNetworkAddressText"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    iget-object v2, p0, Lcom/osp/device/e;->l:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 430
    const-string v2, ""

    const-string v3, "deviceNetworkAddressText"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    const-string v2, ""

    const-string v3, "deviceSerialNumberText"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    iget-object v2, p0, Lcom/osp/device/e;->m:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 434
    const-string v2, ""

    const-string v3, "deviceSerialNumberText"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    const-string v2, ""

    const-string v3, "phoneNumberText"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    iget-object v2, p0, Lcom/osp/device/e;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 438
    const-string v2, ""

    const-string v3, "phoneNumberText"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    const-string v2, ""

    const-string v3, "telephonyMobileCountryCode"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    iget-object v2, p0, Lcom/osp/device/e;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 442
    const-string v2, ""

    const-string v3, "telephonyMobileCountryCode"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    const-string v2, ""

    const-string v3, "mobileCountryCode"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    iget-object v2, p0, Lcom/osp/device/e;->g:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 446
    const-string v2, ""

    const-string v3, "mobileCountryCode"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    const-string v2, ""

    const-string v3, "mobileNetworkCode"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    iget-object v2, p0, Lcom/osp/device/e;->h:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 450
    const-string v2, ""

    const-string v3, "mobileNetworkCode"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    const-string v2, ""

    const-string v3, "customerCode"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    iget-object v2, p0, Lcom/osp/device/e;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 454
    const-string v2, ""

    const-string v3, "customerCode"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    const-string v2, ""

    const-string v3, "deviceName"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    iget-object v2, p0, Lcom/osp/device/e;->j:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 458
    const-string v2, ""

    const-string v3, "deviceName"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    iget-object v2, p0, Lcom/osp/device/e;->k:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/osp/device/e;->k:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 462
    const-string v2, ""

    const-string v3, "deviceMultiUserID"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    iget-object v2, p0, Lcom/osp/device/e;->k:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 464
    const-string v2, ""

    const-string v3, "deviceMultiUserID"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    :cond_0
    const-string v2, ""

    const-string v3, "softwareVersion"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    iget-object v2, p0, Lcom/osp/device/e;->n:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 473
    const-string v2, ""

    const-string v3, "softwareVersion"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    const-string v2, ""

    const-string v3, "userDeviceCreate"

    invoke-virtual {v0, v2, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    invoke-virtual {v0}, Lcom/msc/c/q;->a()V

    .line 478
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 484
    return-object v0

    .line 479
    :catch_0
    move-exception v0

    .line 481
    new-instance v1, Lcom/osp/device/DeviceException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/osp/device/DeviceException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/osp/device/e;->k:Ljava/lang/String;

    .line 91
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 136
    if-eqz p1, :cond_0

    .line 138
    iput-object p1, p0, Lcom/osp/device/e;->a:Ljava/lang/String;

    .line 140
    :cond_0
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 157
    if-eqz p1, :cond_0

    .line 159
    iput-object p1, p0, Lcom/osp/device/e;->b:Ljava/lang/String;

    .line 161
    :cond_0
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 178
    if-eqz p1, :cond_0

    .line 180
    iput-object p1, p0, Lcom/osp/device/e;->c:Ljava/lang/String;

    .line 182
    :cond_0
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 199
    if-eqz p1, :cond_0

    .line 201
    iput-object p1, p0, Lcom/osp/device/e;->d:Ljava/lang/String;

    .line 203
    :cond_0
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 220
    if-eqz p1, :cond_0

    .line 222
    iput-object p1, p0, Lcom/osp/device/e;->e:Ljava/lang/String;

    .line 224
    :cond_0
    return-void
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 241
    if-eqz p1, :cond_0

    .line 243
    iput-object p1, p0, Lcom/osp/device/e;->f:Ljava/lang/String;

    .line 245
    :cond_0
    return-void
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 262
    if-eqz p1, :cond_0

    .line 264
    iput-object p1, p0, Lcom/osp/device/e;->g:Ljava/lang/String;

    .line 266
    :cond_0
    return-void
.end method

.method public final i(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 283
    if-eqz p1, :cond_0

    .line 285
    iput-object p1, p0, Lcom/osp/device/e;->h:Ljava/lang/String;

    .line 287
    :cond_0
    return-void
.end method

.method public final j(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 304
    if-eqz p1, :cond_0

    .line 306
    iput-object p1, p0, Lcom/osp/device/e;->i:Ljava/lang/String;

    .line 308
    :cond_0
    return-void
.end method

.method public final k(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 325
    if-eqz p1, :cond_0

    .line 327
    iput-object p1, p0, Lcom/osp/device/e;->j:Ljava/lang/String;

    .line 329
    :cond_0
    return-void
.end method

.method public final l(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 346
    if-eqz p1, :cond_0

    .line 348
    iput-object p1, p0, Lcom/osp/device/e;->n:Ljava/lang/String;

    .line 350
    :cond_0
    return-void
.end method

.method public final m(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 358
    if-eqz p1, :cond_0

    .line 360
    iput-object p1, p0, Lcom/osp/device/e;->l:Ljava/lang/String;

    .line 362
    :cond_0
    return-void
.end method

.method public final n(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 379
    if-eqz p1, :cond_0

    .line 381
    iput-object p1, p0, Lcom/osp/device/e;->m:Ljava/lang/String;

    .line 383
    :cond_0
    return-void
.end method
