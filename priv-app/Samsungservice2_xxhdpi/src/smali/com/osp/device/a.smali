.class public final Lcom/osp/device/a;
.super Ljava/lang/Object;
.source "DeviceInfo.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/a;->a:Ljava/lang/String;

    .line 78
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/a;->b:Ljava/lang/String;

    .line 79
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/a;->c:Ljava/lang/String;

    .line 80
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/a;->d:Ljava/lang/String;

    .line 81
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/a;->e:Ljava/lang/String;

    .line 82
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/a;->f:Ljava/lang/String;

    .line 83
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/a;->g:Ljava/lang/String;

    .line 84
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/a;->h:Ljava/lang/String;

    .line 85
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/a;->i:Ljava/lang/String;

    .line 86
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/a;->j:Ljava/lang/String;

    .line 88
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/a;->k:Ljava/lang/String;

    .line 89
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/device/a;->l:Ljava/lang/String;

    .line 90
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/osp/device/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 107
    if-eqz p1, :cond_0

    .line 109
    iput-object p1, p0, Lcom/osp/device/a;->a:Ljava/lang/String;

    .line 111
    :cond_0
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/device/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 128
    if-eqz p1, :cond_0

    .line 130
    iput-object p1, p0, Lcom/osp/device/a;->b:Ljava/lang/String;

    .line 132
    :cond_0
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/osp/device/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 149
    if-eqz p1, :cond_0

    .line 151
    iput-object p1, p0, Lcom/osp/device/a;->c:Ljava/lang/String;

    .line 153
    :cond_0
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/osp/device/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 170
    if-eqz p1, :cond_0

    .line 172
    iput-object p1, p0, Lcom/osp/device/a;->d:Ljava/lang/String;

    .line 174
    :cond_0
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/osp/device/a;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 191
    if-eqz p1, :cond_0

    .line 193
    iput-object p1, p0, Lcom/osp/device/a;->e:Ljava/lang/String;

    .line 195
    :cond_0
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/osp/device/a;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 212
    if-eqz p1, :cond_0

    .line 214
    iput-object p1, p0, Lcom/osp/device/a;->f:Ljava/lang/String;

    .line 216
    :cond_0
    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/osp/device/a;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 233
    if-eqz p1, :cond_0

    .line 235
    iput-object p1, p0, Lcom/osp/device/a;->g:Ljava/lang/String;

    .line 237
    :cond_0
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/osp/device/a;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 254
    if-eqz p1, :cond_0

    .line 256
    iput-object p1, p0, Lcom/osp/device/a;->h:Ljava/lang/String;

    .line 258
    :cond_0
    return-void
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/osp/device/a;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final i(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 275
    if-eqz p1, :cond_0

    .line 277
    iput-object p1, p0, Lcom/osp/device/a;->i:Ljava/lang/String;

    .line 279
    :cond_0
    return-void
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/osp/device/a;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final j(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 296
    if-eqz p1, :cond_0

    .line 298
    iput-object p1, p0, Lcom/osp/device/a;->j:Ljava/lang/String;

    .line 300
    :cond_0
    return-void
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/osp/device/a;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final k(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 317
    if-eqz p1, :cond_0

    .line 319
    iput-object p1, p0, Lcom/osp/device/a;->k:Ljava/lang/String;

    .line 321
    :cond_0
    return-void
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/osp/device/a;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final l(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 338
    if-eqz p1, :cond_0

    .line 340
    iput-object p1, p0, Lcom/osp/device/a;->l:Ljava/lang/String;

    .line 342
    :cond_0
    return-void
.end method
