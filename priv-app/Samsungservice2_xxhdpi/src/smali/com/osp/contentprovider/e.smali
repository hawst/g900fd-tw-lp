.class final Lcom/osp/contentprovider/e;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "OSPContentProvider.java"


# instance fields
.field final synthetic a:Lcom/osp/contentprovider/OSPContentProvider;


# direct methods
.method constructor <init>(Lcom/osp/contentprovider/OSPContentProvider;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 111
    iput-object p1, p0, Lcom/osp/contentprovider/e;->a:Lcom/osp/contentprovider/OSPContentProvider;

    .line 112
    const-string v0, "osp.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 113
    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 117
    monitor-enter p1

    .line 119
    :try_start_0
    iget-object v0, p0, Lcom/osp/contentprovider/e;->a:Lcom/osp/contentprovider/OSPContentProvider;

    invoke-static {v0}, Lcom/osp/contentprovider/OSPContentProvider;->a(Lcom/osp/contentprovider/OSPContentProvider;)Lcom/osp/contentprovider/b;

    invoke-static {p1}, Lcom/osp/contentprovider/b;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 120
    iget-object v0, p0, Lcom/osp/contentprovider/e;->a:Lcom/osp/contentprovider/OSPContentProvider;

    invoke-static {v0}, Lcom/osp/contentprovider/OSPContentProvider;->b(Lcom/osp/contentprovider/OSPContentProvider;)Lcom/osp/contentprovider/c;

    invoke-static {p1}, Lcom/osp/contentprovider/c;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 121
    iget-object v0, p0, Lcom/osp/contentprovider/e;->a:Lcom/osp/contentprovider/OSPContentProvider;

    invoke-static {v0}, Lcom/osp/contentprovider/OSPContentProvider;->c(Lcom/osp/contentprovider/OSPContentProvider;)Lcom/osp/contentprovider/d;

    invoke-static {p1}, Lcom/osp/contentprovider/d;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 122
    iget-object v0, p0, Lcom/osp/contentprovider/e;->a:Lcom/osp/contentprovider/OSPContentProvider;

    invoke-static {v0}, Lcom/osp/contentprovider/OSPContentProvider;->d(Lcom/osp/contentprovider/OSPContentProvider;)Lcom/osp/contentprovider/f;

    invoke-static {p1}, Lcom/osp/contentprovider/f;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 123
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p1

    throw v0
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1

    .prologue
    .line 128
    monitor-enter p1

    .line 130
    :try_start_0
    iget-object v0, p0, Lcom/osp/contentprovider/e;->a:Lcom/osp/contentprovider/OSPContentProvider;

    invoke-static {v0}, Lcom/osp/contentprovider/OSPContentProvider;->a(Lcom/osp/contentprovider/OSPContentProvider;)Lcom/osp/contentprovider/b;

    invoke-static {p1}, Lcom/osp/contentprovider/b;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 131
    iget-object v0, p0, Lcom/osp/contentprovider/e;->a:Lcom/osp/contentprovider/OSPContentProvider;

    invoke-static {v0}, Lcom/osp/contentprovider/OSPContentProvider;->b(Lcom/osp/contentprovider/OSPContentProvider;)Lcom/osp/contentprovider/c;

    invoke-static {p1}, Lcom/osp/contentprovider/c;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 132
    iget-object v0, p0, Lcom/osp/contentprovider/e;->a:Lcom/osp/contentprovider/OSPContentProvider;

    invoke-static {v0}, Lcom/osp/contentprovider/OSPContentProvider;->c(Lcom/osp/contentprovider/OSPContentProvider;)Lcom/osp/contentprovider/d;

    invoke-static {p1}, Lcom/osp/contentprovider/d;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 133
    iget-object v0, p0, Lcom/osp/contentprovider/e;->a:Lcom/osp/contentprovider/OSPContentProvider;

    invoke-static {v0}, Lcom/osp/contentprovider/OSPContentProvider;->d(Lcom/osp/contentprovider/OSPContentProvider;)Lcom/osp/contentprovider/f;

    invoke-static {p1}, Lcom/osp/contentprovider/f;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 134
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p1

    throw v0
.end method
