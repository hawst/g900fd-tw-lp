.class public Lcom/osp/contentprovider/OSPContentProvider;
.super Landroid/content/ContentProvider;
.source "OSPContentProvider.java"


# instance fields
.field private final a:Lcom/osp/contentprovider/b;

.field private final b:Lcom/osp/contentprovider/c;

.field private final c:Lcom/osp/contentprovider/d;

.field private final d:Lcom/osp/contentprovider/f;

.field private e:Lcom/osp/contentprovider/e;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 65
    new-instance v0, Lcom/osp/contentprovider/b;

    invoke-direct {v0}, Lcom/osp/contentprovider/b;-><init>()V

    iput-object v0, p0, Lcom/osp/contentprovider/OSPContentProvider;->a:Lcom/osp/contentprovider/b;

    .line 66
    new-instance v0, Lcom/osp/contentprovider/c;

    invoke-direct {v0}, Lcom/osp/contentprovider/c;-><init>()V

    iput-object v0, p0, Lcom/osp/contentprovider/OSPContentProvider;->b:Lcom/osp/contentprovider/c;

    .line 67
    new-instance v0, Lcom/osp/contentprovider/d;

    invoke-direct {v0}, Lcom/osp/contentprovider/d;-><init>()V

    iput-object v0, p0, Lcom/osp/contentprovider/OSPContentProvider;->c:Lcom/osp/contentprovider/d;

    .line 68
    new-instance v0, Lcom/osp/contentprovider/f;

    invoke-direct {v0}, Lcom/osp/contentprovider/f;-><init>()V

    iput-object v0, p0, Lcom/osp/contentprovider/OSPContentProvider;->d:Lcom/osp/contentprovider/f;

    .line 69
    return-void
.end method

.method private a(Landroid/net/Uri;)Lcom/osp/contentprovider/a;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 78
    const/4 v0, 0x0

    .line 80
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 82
    const-string v2, "/credential"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v3, :cond_1

    .line 84
    iget-object v0, p0, Lcom/osp/contentprovider/OSPContentProvider;->b:Lcom/osp/contentprovider/c;

    .line 96
    :cond_0
    :goto_0
    return-object v0

    .line 85
    :cond_1
    const-string v2, "/identity"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v3, :cond_2

    .line 87
    iget-object v0, p0, Lcom/osp/contentprovider/OSPContentProvider;->c:Lcom/osp/contentprovider/d;

    goto :goto_0

    .line 88
    :cond_2
    const-string v2, "/applist"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v3, :cond_3

    .line 90
    iget-object v0, p0, Lcom/osp/contentprovider/OSPContentProvider;->a:Lcom/osp/contentprovider/b;

    goto :goto_0

    .line 91
    :cond_3
    const-string v2, "/property"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v3, :cond_0

    .line 93
    iget-object v0, p0, Lcom/osp/contentprovider/OSPContentProvider;->d:Lcom/osp/contentprovider/f;

    goto :goto_0
.end method

.method static synthetic a(Lcom/osp/contentprovider/OSPContentProvider;)Lcom/osp/contentprovider/b;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/osp/contentprovider/OSPContentProvider;->a:Lcom/osp/contentprovider/b;

    return-object v0
.end method

.method static synthetic b(Lcom/osp/contentprovider/OSPContentProvider;)Lcom/osp/contentprovider/c;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/osp/contentprovider/OSPContentProvider;->b:Lcom/osp/contentprovider/c;

    return-object v0
.end method

.method static synthetic c(Lcom/osp/contentprovider/OSPContentProvider;)Lcom/osp/contentprovider/d;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/osp/contentprovider/OSPContentProvider;->c:Lcom/osp/contentprovider/d;

    return-object v0
.end method

.method static synthetic d(Lcom/osp/contentprovider/OSPContentProvider;)Lcom/osp/contentprovider/f;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/osp/contentprovider/OSPContentProvider;->d:Lcom/osp/contentprovider/f;

    return-object v0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 7

    .prologue
    .line 263
    iget-object v6, p0, Lcom/osp/contentprovider/OSPContentProvider;->e:Lcom/osp/contentprovider/e;

    monitor-enter v6

    .line 265
    :try_start_0
    iget-object v0, p0, Lcom/osp/contentprovider/OSPContentProvider;->e:Lcom/osp/contentprovider/e;

    invoke-virtual {v0}, Lcom/osp/contentprovider/e;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 266
    invoke-direct {p0, p1}, Lcom/osp/contentprovider/OSPContentProvider;->a(Landroid/net/Uri;)Lcom/osp/contentprovider/a;

    move-result-object v0

    .line 267
    invoke-virtual {p0}, Lcom/osp/contentprovider/OSPContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/osp/contentprovider/a;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 268
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    invoke-direct {p0, p1}, Lcom/osp/contentprovider/OSPContentProvider;->a(Landroid/net/Uri;)Lcom/osp/contentprovider/a;

    move-result-object v0

    .line 198
    invoke-virtual {v0}, Lcom/osp/contentprovider/a;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 212
    iget-object v1, p0, Lcom/osp/contentprovider/OSPContentProvider;->e:Lcom/osp/contentprovider/e;

    monitor-enter v1

    .line 214
    :try_start_0
    iget-object v0, p0, Lcom/osp/contentprovider/OSPContentProvider;->e:Lcom/osp/contentprovider/e;

    invoke-virtual {v0}, Lcom/osp/contentprovider/e;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 215
    invoke-direct {p0, p1}, Lcom/osp/contentprovider/OSPContentProvider;->a(Landroid/net/Uri;)Lcom/osp/contentprovider/a;

    move-result-object v2

    .line 216
    invoke-virtual {p0}, Lcom/osp/contentprovider/OSPContentProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v0, p1, p2}, Lcom/osp/contentprovider/a;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 217
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 150
    new-instance v0, Lcom/osp/contentprovider/e;

    invoke-virtual {p0}, Lcom/osp/contentprovider/OSPContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/osp/contentprovider/e;-><init>(Lcom/osp/contentprovider/OSPContentProvider;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/contentprovider/OSPContentProvider;->e:Lcom/osp/contentprovider/e;

    .line 151
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    .line 177
    iget-object v7, p0, Lcom/osp/contentprovider/OSPContentProvider;->e:Lcom/osp/contentprovider/e;

    monitor-enter v7

    .line 179
    :try_start_0
    iget-object v0, p0, Lcom/osp/contentprovider/OSPContentProvider;->e:Lcom/osp/contentprovider/e;

    invoke-virtual {v0}, Lcom/osp/contentprovider/e;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 180
    invoke-direct {p0, p1}, Lcom/osp/contentprovider/OSPContentProvider;->a(Landroid/net/Uri;)Lcom/osp/contentprovider/a;

    move-result-object v0

    .line 182
    invoke-virtual {p0}, Lcom/osp/contentprovider/OSPContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/osp/contentprovider/a;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 183
    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 8

    .prologue
    .line 236
    iget-object v7, p0, Lcom/osp/contentprovider/OSPContentProvider;->e:Lcom/osp/contentprovider/e;

    monitor-enter v7

    .line 238
    :try_start_0
    iget-object v0, p0, Lcom/osp/contentprovider/OSPContentProvider;->e:Lcom/osp/contentprovider/e;

    invoke-virtual {v0}, Lcom/osp/contentprovider/e;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 239
    invoke-direct {p0, p1}, Lcom/osp/contentprovider/OSPContentProvider;->a(Landroid/net/Uri;)Lcom/osp/contentprovider/a;

    move-result-object v0

    .line 240
    invoke-virtual {p0}, Lcom/osp/contentprovider/OSPContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/osp/contentprovider/a;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 241
    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0
.end method
