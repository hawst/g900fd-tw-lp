.class public final Lcom/msc/a/a;
.super Ljava/lang/Object;
.source "AppAuthenticationInfo.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Lcom/osp/social/member/b;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/a/a;->a:Ljava/lang/String;

    .line 116
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/a/a;->b:Ljava/lang/String;

    .line 117
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/a/a;->c:Ljava/lang/String;

    .line 118
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/a/a;->d:Ljava/lang/String;

    .line 119
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/a/a;->h:Ljava/lang/String;

    .line 120
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/a/a;->i:Ljava/lang/String;

    .line 121
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/a/a;->j:Ljava/lang/String;

    .line 122
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/a/a;->k:Ljava/lang/String;

    .line 123
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/a/a;->l:Ljava/lang/String;

    .line 124
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/a/a;->m:Ljava/lang/String;

    .line 125
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/a/a;->n:Ljava/lang/String;

    .line 126
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/a/a;->o:Ljava/lang/String;

    .line 127
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/a/a;->p:Ljava/lang/String;

    .line 128
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/a/a;->q:Ljava/lang/String;

    .line 130
    const-string v0, "N"

    iput-object v0, p0, Lcom/msc/a/a;->r:Ljava/lang/String;

    .line 131
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/a/a;->e:Ljava/lang/String;

    .line 132
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/a/a;->f:Ljava/lang/String;

    .line 133
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/a/a;->s:Ljava/lang/String;

    .line 134
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/msc/a/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 510
    :try_start_0
    iput-object p2, p0, Lcom/msc/a/a;->b:Ljava/lang/String;

    .line 511
    iput-object p3, p0, Lcom/msc/a/a;->c:Ljava/lang/String;

    .line 515
    new-instance v0, Lcom/osp/social/member/b;

    invoke-direct {v0}, Lcom/osp/social/member/b;-><init>()V

    .line 516
    iput-object v0, p0, Lcom/msc/a/a;->g:Lcom/osp/social/member/b;

    .line 518
    const-string v1, "N/A"

    invoke-virtual {v0, v1}, Lcom/osp/social/member/b;->a(Ljava/lang/String;)V

    .line 519
    const-string v1, "Device"

    invoke-virtual {v0, v1}, Lcom/osp/social/member/b;->b(Ljava/lang/String;)V

    .line 521
    new-instance v0, Lcom/osp/social/member/c;

    invoke-direct {v0}, Lcom/osp/social/member/c;-><init>()V

    .line 522
    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/osp/social/member/c;->a(Ljava/lang/String;)V

    .line 523
    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/osp/social/member/c;->b(Ljava/lang/String;)V

    .line 525
    invoke-static {p1}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v0

    .line 527
    invoke-virtual {v0}, Lcom/osp/device/a;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/msc/a/a;->d:Ljava/lang/String;

    .line 528
    invoke-virtual {v0}, Lcom/osp/device/a;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/msc/a/a;->h:Ljava/lang/String;

    .line 529
    invoke-virtual {v0}, Lcom/osp/device/a;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/msc/a/a;->i:Ljava/lang/String;

    .line 530
    invoke-virtual {v0}, Lcom/osp/device/a;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/msc/a/a;->j:Ljava/lang/String;

    .line 531
    invoke-virtual {v0}, Lcom/osp/device/a;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/msc/a/a;->k:Ljava/lang/String;

    .line 532
    invoke-virtual {v0}, Lcom/osp/device/a;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/msc/a/a;->p:Ljava/lang/String;

    .line 533
    invoke-virtual {v0}, Lcom/osp/device/a;->k()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/msc/a/a;->q:Ljava/lang/String;

    .line 534
    invoke-virtual {v0}, Lcom/osp/device/a;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/msc/a/a;->l:Ljava/lang/String;

    .line 535
    invoke-virtual {v0}, Lcom/osp/device/a;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/msc/a/a;->m:Ljava/lang/String;

    .line 536
    invoke-virtual {v0}, Lcom/osp/device/a;->h()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/msc/a/a;->n:Ljava/lang/String;

    .line 537
    invoke-virtual {v0}, Lcom/osp/device/a;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/msc/a/a;->o:Ljava/lang/String;

    .line 538
    const-string v0, "j5p7ll8g33"

    iput-object v0, p0, Lcom/msc/a/a;->e:Ljava/lang/String;

    .line 539
    const-string v0, "5763D0052DC1462E13751F753384E9A9"

    iput-object v0, p0, Lcom/msc/a/a;->f:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 545
    :goto_0
    return-void

    .line 541
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 424
    iput-object p1, p0, Lcom/msc/a/a;->s:Ljava/lang/String;

    .line 425
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/msc/a/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/msc/a/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/msc/a/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/msc/a/a;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/msc/a/a;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/msc/a/a;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/msc/a/a;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/msc/a/a;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/msc/a/a;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lcom/msc/a/a;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lcom/msc/a/a;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/msc/a/a;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lcom/msc/a/a;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lcom/msc/a/a;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lcom/msc/a/a;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Lcom/osp/social/member/b;
    .locals 1

    .prologue
    .line 442
    iget-object v0, p0, Lcom/msc/a/a;->g:Lcom/osp/social/member/b;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lcom/msc/a/a;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 484
    iget-object v0, p0, Lcom/msc/a/a;->f:Ljava/lang/String;

    return-object v0
.end method
