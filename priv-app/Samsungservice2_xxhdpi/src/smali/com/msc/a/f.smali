.class public final Lcom/msc/a/f;
.super Ljava/lang/Object;
.source "CheckListRequest.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 326
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 328
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/msc/a/f;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/msc/a/f;->o:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 241
    iput-boolean p1, p0, Lcom/msc/a/f;->d:Z

    .line 242
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 196
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/msc/a/f;->b:Z

    .line 197
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/msc/a/f;->m:Ljava/lang/String;

    .line 125
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 259
    iput-boolean p1, p0, Lcom/msc/a/f;->e:Z

    .line 260
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/msc/a/f;->g:Z

    .line 206
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/msc/a/f;->n:Ljava/lang/String;

    .line 143
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 223
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/msc/a/f;->c:Z

    .line 224
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/msc/a/f;->a:Ljava/lang/String;

    .line 161
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 277
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/msc/a/f;->f:Z

    .line 278
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/msc/a/f;->l:Ljava/lang/String;

    .line 170
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 4

    .prologue
    .line 332
    :try_start_0
    new-instance v1, Lcom/msc/c/q;

    invoke-direct {v1}, Lcom/msc/c/q;-><init>()V

    .line 338
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 340
    invoke-virtual {v1, v2}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 341
    const-string v0, "UTF-8"

    invoke-virtual {v1, v0}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 346
    const-string v0, ""

    const-string v3, "UserTNCRequestVO"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    const-string v0, ""

    const-string v3, "loginID"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    iget-object v0, p0, Lcom/msc/a/f;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 350
    const-string v0, ""

    const-string v3, "loginID"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    const-string v0, ""

    const-string v3, "tncAccepted"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    iget-boolean v0, p0, Lcom/msc/a/f;->b:Z

    if-eqz v0, :cond_a

    const-string v0, "Y"

    :goto_0
    invoke-virtual {v1, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 354
    const-string v0, ""

    const-string v3, "tncAccepted"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    const-string v0, ""

    const-string v3, "privacyAccepted"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    iget-boolean v0, p0, Lcom/msc/a/f;->c:Z

    if-eqz v0, :cond_b

    const-string v0, "Y"

    :goto_1
    invoke-virtual {v1, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 358
    const-string v0, ""

    const-string v3, "privacyAccepted"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    const-string v0, ""

    const-string v3, "dataCollectionAccepted"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    iget-boolean v0, p0, Lcom/msc/a/f;->d:Z

    if-eqz v0, :cond_c

    const-string v0, "Y"

    :goto_2
    invoke-virtual {v1, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 364
    const-string v0, ""

    const-string v3, "dataCollectionAccepted"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    iget-object v0, p0, Lcom/msc/a/f;->j:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "450"

    iget-object v3, p0, Lcom/msc/a/f;->j:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "460"

    iget-object v3, p0, Lcom/msc/a/f;->j:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "461"

    iget-object v3, p0, Lcom/msc/a/f;->j:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 369
    :cond_0
    const-string v0, ""

    const-string v3, "onwardTransferAccepted"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    iget-boolean v0, p0, Lcom/msc/a/f;->e:Z

    if-eqz v0, :cond_d

    const-string v0, "Y"

    :goto_3
    invoke-virtual {v1, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 371
    const-string v0, ""

    const-string v3, "onwardTransferAccepted"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    :cond_1
    const-string v0, ""

    const-string v3, "checkCountryCode"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    iget-boolean v0, p0, Lcom/msc/a/f;->f:Z

    if-eqz v0, :cond_e

    const-string v0, "Y"

    :goto_4
    invoke-virtual {v1, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 377
    const-string v0, ""

    const-string v3, "checkCountryCode"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    const-string v0, ""

    const-string v3, "checkNameCheck"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    const-string v0, "Y"

    invoke-virtual {v1, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 382
    const-string v0, ""

    const-string v3, "checkNameCheck"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    const-string v0, ""

    const-string v3, "checkEmailValidation"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    const-string v0, "Y"

    invoke-virtual {v1, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 387
    const-string v0, ""

    const-string v3, "checkEmailValidation"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    iget-object v0, p0, Lcom/msc/a/f;->h:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 391
    const-string v0, ""

    const-string v3, "appID"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    iget-object v0, p0, Lcom/msc/a/f;->h:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 393
    const-string v0, ""

    const-string v3, "appID"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    const-string v0, ""

    const-string v3, "disclaimer"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    iget-boolean v0, p0, Lcom/msc/a/f;->g:Z

    if-eqz v0, :cond_f

    const-string v0, "Y"

    :goto_5
    invoke-virtual {v1, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 397
    const-string v0, ""

    const-string v3, "disclaimer"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    :cond_2
    iget-object v0, p0, Lcom/msc/a/f;->i:Ljava/lang/String;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/msc/a/f;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_10

    .line 402
    const-string v0, ""

    const-string v3, "package"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    iget-object v0, p0, Lcom/msc/a/f;->i:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 404
    const-string v0, ""

    const-string v3, "package"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    :cond_3
    :goto_6
    iget-object v0, p0, Lcom/msc/a/f;->j:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 417
    const-string v0, ""

    const-string v3, "mobileCountryCode"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    iget-object v0, p0, Lcom/msc/a/f;->j:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 419
    const-string v0, ""

    const-string v3, "mobileCountryCode"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    :cond_4
    iget-object v0, p0, Lcom/msc/a/f;->l:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 424
    const-string v0, ""

    const-string v3, "password"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    iget-object v0, p0, Lcom/msc/a/f;->l:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/msc/c/q;->c(Ljava/lang/String;)V

    .line 426
    const-string v0, ""

    const-string v3, "password"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    :cond_5
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 431
    iget-object v0, p0, Lcom/msc/a/f;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 433
    const-string v0, ""

    const-string v3, "loginIDTypeCode"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    const-string v0, "001"

    invoke-virtual {v1, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 435
    const-string v0, ""

    const-string v3, "loginIDTypeCode"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    const-string v0, ""

    const-string v3, "checkTelephoneNumberValidation"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    const-string v0, "Y"

    invoke-virtual {v1, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 439
    const-string v0, ""

    const-string v3, "checkTelephoneNumberValidation"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    iget-object v0, p0, Lcom/msc/a/f;->k:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 443
    const-string v0, ""

    const-string v3, "countryCallingCode"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    iget-object v0, p0, Lcom/msc/a/f;->k:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 445
    const-string v0, ""

    const-string v3, "countryCallingCode"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    :cond_6
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 450
    iget-object v0, p0, Lcom/msc/a/f;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 452
    const-string v0, ""

    const-string v3, "telephoneIDDuplicationCheckYNFlag"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    iget-object v0, p0, Lcom/msc/a/f;->m:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 454
    const-string v0, ""

    const-string v3, "telephoneIDDuplicationCheckYNFlag"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    :cond_7
    :goto_7
    iget-object v0, p0, Lcom/msc/a/f;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 480
    const-string v0, ""

    const-string v3, "userID"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    iget-object v0, p0, Lcom/msc/a/f;->n:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 482
    const-string v0, ""

    const-string v3, "userID"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    :cond_8
    iget-object v0, p0, Lcom/msc/a/f;->o:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 487
    const-string v0, ""

    const-string v3, "confirmChangeLoginIDYNFlag"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    iget-object v0, p0, Lcom/msc/a/f;->o:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 489
    const-string v0, ""

    const-string v3, "confirmChangeLoginIDYNFlag"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    :cond_9
    const-string v0, ""

    const-string v3, "UserTNCRequestVO"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    invoke-virtual {v1}, Lcom/msc/c/q;->a()V

    .line 499
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 505
    return-object v0

    .line 353
    :cond_a
    const-string v0, "N"

    goto/16 :goto_0

    .line 357
    :cond_b
    const-string v0, "N"

    goto/16 :goto_1

    .line 363
    :cond_c
    const-string v0, "N"

    goto/16 :goto_2

    .line 370
    :cond_d
    const-string v0, "N"

    goto/16 :goto_3

    .line 376
    :cond_e
    const-string v0, "N"

    goto/16 :goto_4

    .line 396
    :cond_f
    const-string v0, "N"

    goto/16 :goto_5

    .line 407
    :cond_10
    iget-object v0, p0, Lcom/msc/a/f;->h:Ljava/lang/String;

    if-eqz v0, :cond_3

    const-string v0, "j5p7ll8g33"

    iget-object v3, p0, Lcom/msc/a/f;->h:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 409
    const-string v0, ""

    const-string v3, "package"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    const-string v0, "com.osp.app.signin"

    invoke-virtual {v1, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 411
    const-string v0, ""

    const-string v3, "package"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_6

    .line 500
    :catch_0
    move-exception v0

    .line 502
    new-instance v1, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 466
    :cond_11
    :try_start_1
    const-string v0, ""

    const-string v3, "loginIDTypeCode"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    const-string v0, "003"

    invoke-virtual {v1, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 468
    const-string v0, ""

    const-string v3, "loginIDTypeCode"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    const-string v0, ""

    const-string v3, "checkTelephoneNumberValidation"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    const-string v0, "N"

    invoke-virtual {v1, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 472
    const-string v0, ""

    const-string v3, "checkTelephoneNumberValidation"

    invoke-virtual {v1, v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_7
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 311
    iput-object p1, p0, Lcom/msc/a/f;->h:Ljava/lang/String;

    .line 312
    return-void
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 320
    iput-object p1, p0, Lcom/msc/a/f;->i:Ljava/lang/String;

    .line 321
    return-void
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 514
    iput-object p1, p0, Lcom/msc/a/f;->j:Ljava/lang/String;

    .line 515
    return-void
.end method

.method public final i(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 532
    iput-object p1, p0, Lcom/msc/a/f;->k:Ljava/lang/String;

    .line 533
    return-void
.end method
