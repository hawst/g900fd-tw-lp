.class public final Lcom/msc/a/d;
.super Ljava/lang/Object;
.source "AuthenticationResult.java"


# instance fields
.field a:Ljava/util/ArrayList;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:J

.field private i:J

.field private j:Ljava/lang/String;

.field private k:Z

.field private l:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/a/d;->j:Ljava/lang/String;

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/a/d;->l:Ljava/lang/String;

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/msc/a/d;->a:Ljava/util/ArrayList;

    .line 59
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 1

    .prologue
    .line 220
    iput-wide p1, p0, Lcom/msc/a/d;->h:J

    .line 221
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/msc/a/d;->l:Ljava/lang/String;

    .line 101
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/msc/a/d;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/msc/a/d;->a:Ljava/util/ArrayList;

    new-instance v1, Lcom/msc/a/e;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/msc/a/e;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/msc/a/d;->k:Z

    .line 93
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/msc/a/d;->k:Z

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/msc/a/d;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final b(J)V
    .locals 1

    .prologue
    .line 229
    iput-wide p1, p0, Lcom/msc/a/d;->i:J

    .line 230
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/msc/a/d;->b:Ljava/lang/String;

    .line 158
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/msc/a/d;->a:Ljava/util/ArrayList;

    .line 109
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/msc/a/d;->c:Ljava/lang/String;

    .line 168
    return-void
.end method

.method public final d()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/msc/a/d;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/msc/a/d;->d:Ljava/lang/String;

    .line 177
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/msc/a/d;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/msc/a/d;->e:Ljava/lang/String;

    .line 186
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/msc/a/d;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 189
    iput-object p1, p0, Lcom/msc/a/d;->j:Ljava/lang/String;

    .line 190
    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/msc/a/d;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lcom/msc/a/d;->f:Ljava/lang/String;

    .line 203
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/msc/a/d;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 211
    iput-object p1, p0, Lcom/msc/a/d;->g:Ljava/lang/String;

    .line 212
    return-void
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/msc/a/d;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 301
    iget-wide v0, p0, Lcom/msc/a/d;->h:J

    return-wide v0
.end method

.method public final k()J
    .locals 2

    .prologue
    .line 310
    iget-wide v0, p0, Lcom/msc/a/d;->i:J

    return-wide v0
.end method
