.class public final Lcom/msc/a/g;
.super Ljava/lang/Object;
.source "CheckListResult.java"


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Lcom/msc/c/f;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/os/Bundle;)Lcom/msc/a/g;
    .locals 2

    .prologue
    .line 556
    new-instance v0, Lcom/msc/a/g;

    invoke-direct {v0}, Lcom/msc/a/g;-><init>()V

    .line 558
    if-eqz p0, :cond_0

    .line 560
    const-string v1, "REQUIRE_TNC_ACCEPTED"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/msc/a/g;->a:Z

    .line 561
    const-string v1, "PRIVACY_ACCEPTED"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/msc/a/g;->b:Z

    .line 562
    const-string v1, "DATA_COLLECTION_ACCEPTED"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/msc/a/g;->c:Z

    .line 563
    const-string v1, "ONWARD_TRANSFER_ACCEPTED"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/msc/a/g;->d:Z

    .line 564
    const-string v1, "MOBILE_COUNTRY_CODE"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/msc/a/g;->e:Ljava/lang/String;

    .line 565
    const-string v1, "USER_COUNTRY_CODE"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/msc/a/g;->f:Ljava/lang/String;

    .line 566
    const-string v1, "REQUIRE_NAME_CHECK"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/msc/a/g;->g:Z

    .line 567
    const-string v1, "REQUIRE_EMAIL_VALIDATION"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/msc/a/g;->h:Z

    .line 568
    const-string v1, "IS_3RD_PARTY"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/msc/a/g;->i:Z

    .line 569
    const-string v1, "REQUIRE_DISCLAIMER"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/msc/a/g;->j:Z

    .line 570
    const-string v1, "USER_ID"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/msc/a/g;->k:Ljava/lang/String;

    .line 571
    const-string v1, "LOGIN_ID"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/msc/a/g;->l:Ljava/lang/String;

    .line 573
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/msc/c/f;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/msc/a/g;->m:Lcom/msc/c/f;

    return-object v0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 173
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 176
    iget-boolean v0, p0, Lcom/msc/a/g;->a:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/msc/a/g;->b:Z

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iget-boolean v1, p0, Lcom/msc/a/g;->g:Z

    iget-boolean v2, p0, Lcom/msc/a/g;->h:Z

    invoke-static {p1, v0, v1, v2}, Lcom/msc/openprovider/b;->a(Landroid/content/Context;ZZZ)V

    .line 179
    :cond_1
    return-void

    .line 176
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/msc/c/f;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/msc/a/g;->m:Lcom/msc/c/f;

    .line 163
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lcom/msc/a/g;->k:Ljava/lang/String;

    .line 197
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 232
    iput-boolean p1, p0, Lcom/msc/a/g;->a:Z

    .line 233
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/msc/a/g;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 214
    iput-object p1, p0, Lcom/msc/a/g;->l:Ljava/lang/String;

    .line 215
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 250
    iput-boolean p1, p0, Lcom/msc/a/g;->b:Z

    .line 251
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/msc/a/g;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 327
    iput-object p1, p0, Lcom/msc/a/g;->e:Ljava/lang/String;

    .line 328
    return-void
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 268
    iput-boolean p1, p0, Lcom/msc/a/g;->c:Z

    .line 269
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 348
    iput-object p1, p0, Lcom/msc/a/g;->f:Ljava/lang/String;

    .line 349
    return-void
.end method

.method public final d(Z)V
    .locals 0

    .prologue
    .line 286
    iput-boolean p1, p0, Lcom/msc/a/g;->d:Z

    .line 287
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 223
    iget-boolean v0, p0, Lcom/msc/a/g;->a:Z

    return v0
.end method

.method public final e(Z)V
    .locals 0

    .prologue
    .line 368
    iput-boolean p1, p0, Lcom/msc/a/g;->g:Z

    .line 369
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 241
    iget-boolean v0, p0, Lcom/msc/a/g;->b:Z

    return v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/msc/a/g;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f(Z)V
    .locals 0

    .prologue
    .line 413
    iput-boolean p1, p0, Lcom/msc/a/g;->i:Z

    .line 414
    return-void
.end method

.method public final g(Z)V
    .locals 0

    .prologue
    .line 422
    iput-boolean p1, p0, Lcom/msc/a/g;->j:Z

    .line 423
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 359
    iget-boolean v0, p0, Lcom/msc/a/g;->g:Z

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 377
    iget-boolean v0, p0, Lcom/msc/a/g;->h:Z

    return v0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 386
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/msc/a/g;->h:Z

    .line 387
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 395
    iget-boolean v0, p0, Lcom/msc/a/g;->i:Z

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 404
    iget-boolean v0, p0, Lcom/msc/a/g;->j:Z

    return v0
.end method

.method public final l()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 582
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 583
    const-string v1, "REQUIRE_TNC_ACCEPTED"

    iget-boolean v2, p0, Lcom/msc/a/g;->a:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 584
    const-string v1, "PRIVACY_ACCEPTED"

    iget-boolean v2, p0, Lcom/msc/a/g;->b:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 585
    const-string v1, "DATA_COLLECTION_ACCEPTED"

    iget-boolean v2, p0, Lcom/msc/a/g;->c:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 586
    const-string v1, "ONWARD_TRANSFER_ACCEPTED"

    iget-boolean v2, p0, Lcom/msc/a/g;->d:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 587
    const-string v1, "MOBILE_COUNTRY_CODE"

    iget-object v2, p0, Lcom/msc/a/g;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    const-string v1, "USER_COUNTRY_CODE"

    iget-object v2, p0, Lcom/msc/a/g;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    const-string v1, "REQUIRE_NAME_CHECK"

    iget-boolean v2, p0, Lcom/msc/a/g;->g:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 590
    const-string v1, "REQUIRE_EMAIL_VALIDATION"

    iget-boolean v2, p0, Lcom/msc/a/g;->h:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 591
    const-string v1, "IS_3RD_PARTY"

    iget-boolean v2, p0, Lcom/msc/a/g;->i:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 592
    const-string v1, "REQUIRE_DISCLAIMER"

    iget-boolean v2, p0, Lcom/msc/a/g;->j:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 593
    const-string v1, "USER_ID"

    iget-object v2, p0, Lcom/msc/a/g;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    const-string v1, "LOGIN_ID"

    iget-object v2, p0, Lcom/msc/a/g;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    return-object v0
.end method
