.class public final Lcom/msc/a/c;
.super Ljava/lang/Object;
.source "AuthWithTncMandatoryRequestParams.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 1

    .prologue
    .line 489
    iget-object v0, p0, Lcom/msc/a/c;->t:Ljava/lang/String;

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .locals 1

    .prologue
    .line 498
    iget-object v0, p0, Lcom/msc/a/c;->u:Ljava/lang/String;

    return-object v0
.end method

.method public final C()Ljava/lang/String;
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lcom/msc/a/c;->v:Ljava/lang/String;

    return-object v0
.end method

.method public final D()Ljava/lang/String;
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/msc/a/c;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/msc/a/c;->d:Z

    .line 130
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/msc/a/c;->a:Ljava/lang/String;

    .line 112
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 264
    iput-boolean p1, p0, Lcom/msc/a/c;->i:Z

    .line 265
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 174
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/msc/a/c;->h:Z

    .line 175
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/msc/a/c;->b:Ljava/lang/String;

    .line 121
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 273
    iput-boolean p1, p0, Lcom/msc/a/c;->j:Z

    .line 274
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/msc/a/c;->c:Ljava/lang/String;

    .line 202
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/msc/a/c;->r:Ljava/lang/String;

    .line 139
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/msc/a/c;->m:Z

    .line 211
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, Lcom/msc/a/c;->o:Ljava/lang/String;

    .line 184
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 282
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/msc/a/c;->k:Z

    .line 283
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lcom/msc/a/c;->q:Ljava/lang/String;

    .line 193
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 291
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/msc/a/c;->l:Z

    .line 292
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 219
    iput-object p1, p0, Lcom/msc/a/c;->p:Ljava/lang/String;

    .line 220
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 309
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/msc/a/c;->n:Z

    .line 310
    return-void
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 228
    iput-object p1, p0, Lcom/msc/a/c;->s:Ljava/lang/String;

    .line 229
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/msc/a/c;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lcom/msc/a/c;->t:Ljava/lang/String;

    .line 238
    return-void
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/msc/a/c;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final i(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Lcom/msc/a/c;->u:Ljava/lang/String;

    .line 247
    return-void
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/msc/a/c;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final j(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 255
    iput-object p1, p0, Lcom/msc/a/c;->v:Ljava/lang/String;

    .line 256
    return-void
.end method

.method public final k(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 300
    iput-object p1, p0, Lcom/msc/a/c;->w:Ljava/lang/String;

    .line 301
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 345
    iget-boolean v0, p0, Lcom/msc/a/c;->d:Z

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 354
    iget-boolean v0, p0, Lcom/msc/a/c;->e:Z

    return v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 363
    iget-boolean v0, p0, Lcom/msc/a/c;->f:Z

    return v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 372
    iget-boolean v0, p0, Lcom/msc/a/c;->g:Z

    return v0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 381
    iget-boolean v0, p0, Lcom/msc/a/c;->h:Z

    return v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 390
    iget-boolean v0, p0, Lcom/msc/a/c;->i:Z

    return v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 399
    iget-boolean v0, p0, Lcom/msc/a/c;->j:Z

    return v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 408
    iget-boolean v0, p0, Lcom/msc/a/c;->k:Z

    return v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 417
    iget-boolean v0, p0, Lcom/msc/a/c;->l:Z

    return v0
.end method

.method public final t()Z
    .locals 1

    .prologue
    .line 426
    iget-boolean v0, p0, Lcom/msc/a/c;->m:Z

    return v0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 435
    iget-boolean v0, p0, Lcom/msc/a/c;->n:Z

    return v0
.end method

.method public final v()Ljava/lang/String;
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lcom/msc/a/c;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final w()Ljava/lang/String;
    .locals 1

    .prologue
    .line 453
    iget-object v0, p0, Lcom/msc/a/c;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final x()Ljava/lang/String;
    .locals 1

    .prologue
    .line 462
    iget-object v0, p0, Lcom/msc/a/c;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final y()Ljava/lang/String;
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/msc/a/c;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final z()Ljava/lang/String;
    .locals 1

    .prologue
    .line 480
    iget-object v0, p0, Lcom/msc/a/c;->s:Ljava/lang/String;

    return-object v0
.end method
