.class public final Lcom/msc/a/h;
.super Ljava/lang/Object;
.source "PreProcessRequest.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Z

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 360
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/msc/a/h;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/msc/a/h;->q:Ljava/lang/String;

    .line 112
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 282
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/msc/a/h;->f:Z

    .line 283
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/msc/a/h;->o:Ljava/lang/String;

    .line 130
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 300
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/msc/a/h;->l:Z

    .line 301
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/msc/a/h;->p:Ljava/lang/String;

    .line 148
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 6

    .prologue
    .line 364
    new-instance v2, Lcom/msc/c/q;

    invoke-direct {v2}, Lcom/msc/c/q;-><init>()V

    .line 368
    new-instance v3, Ljava/io/StringWriter;

    invoke-direct {v3}, Ljava/io/StringWriter;-><init>()V

    .line 370
    invoke-virtual {v2, v3}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 371
    const-string v0, "UTF-8"

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 373
    const-string v0, ""

    const-string v1, "TncMandatoryRequestVO"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    const-string v0, ""

    const-string v1, "loginID"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    iget-object v0, p0, Lcom/msc/a/h;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 377
    const-string v0, ""

    const-string v1, "loginID"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    const-string v0, ""

    const-string v1, "tncAccepted"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    iget-boolean v0, p0, Lcom/msc/a/h;->b:Z

    if-eqz v0, :cond_b

    const-string v0, "Y"

    :goto_0
    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 381
    const-string v0, ""

    const-string v1, "tncAccepted"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    const-string v0, ""

    const-string v1, "privacyAccepted"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    iget-boolean v0, p0, Lcom/msc/a/h;->c:Z

    if-eqz v0, :cond_c

    const-string v0, "Y"

    :goto_1
    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 385
    const-string v0, ""

    const-string v1, "privacyAccepted"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    const-string v0, ""

    const-string v1, "dataCollectionAccepted"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    iget-boolean v0, p0, Lcom/msc/a/h;->d:Z

    if-eqz v0, :cond_d

    const-string v0, "Y"

    :goto_2
    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 391
    const-string v0, ""

    const-string v1, "dataCollectionAccepted"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    iget-object v0, p0, Lcom/msc/a/h;->m:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "450"

    iget-object v1, p0, Lcom/msc/a/h;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "460"

    iget-object v1, p0, Lcom/msc/a/h;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "461"

    iget-object v1, p0, Lcom/msc/a/h;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 396
    :cond_0
    const-string v0, ""

    const-string v1, "onwardTransferAccepted"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    iget-boolean v0, p0, Lcom/msc/a/h;->e:Z

    if-eqz v0, :cond_e

    const-string v0, "Y"

    :goto_3
    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 398
    const-string v0, ""

    const-string v1, "onwardTransferAccepted"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    :cond_1
    const-string v0, ""

    const-string v1, "checkCountryCode"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    iget-boolean v0, p0, Lcom/msc/a/h;->f:Z

    if-eqz v0, :cond_f

    const-string v0, "Y"

    :goto_4
    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 403
    const-string v0, ""

    const-string v1, "checkCountryCode"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    const-string v0, ""

    const-string v1, "checkNameCheck"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    const-string v0, "Y"

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 407
    const-string v0, ""

    const-string v1, "checkNameCheck"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    const-string v0, ""

    const-string v1, "checkEmailValidation"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    const-string v0, "Y"

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 411
    const-string v0, ""

    const-string v1, "checkEmailValidation"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    iget-object v0, p0, Lcom/msc/a/h;->g:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/msc/a/h;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 415
    const-string v0, ""

    const-string v1, "appID"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    iget-object v0, p0, Lcom/msc/a/h;->g:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 417
    const-string v0, ""

    const-string v1, "appID"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    const-string v0, ""

    const-string v1, "disclaimer"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    iget-boolean v0, p0, Lcom/msc/a/h;->l:Z

    if-eqz v0, :cond_10

    const-string v0, "Y"

    :goto_5
    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 421
    const-string v0, ""

    const-string v1, "disclaimer"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    :cond_2
    iget-object v0, p0, Lcom/msc/a/h;->n:Ljava/lang/String;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/msc/a/h;->n:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_11

    .line 426
    const-string v0, ""

    const-string v1, "package"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    iget-object v0, p0, Lcom/msc/a/h;->n:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 428
    const-string v0, ""

    const-string v1, "package"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    :cond_3
    :goto_6
    const-string v0, ""

    const-string v1, "mandatoryServiceID"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    iget-object v0, p0, Lcom/msc/a/h;->h:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 441
    const-string v0, ""

    const-string v1, "mandatoryServiceID"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    const-string v0, ""

    const-string v1, "langCode"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    iget-object v0, p0, Lcom/msc/a/h;->i:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 445
    const-string v0, ""

    const-string v1, "langCode"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    iget-object v0, p0, Lcom/msc/a/h;->m:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 449
    const-string v0, ""

    const-string v1, "mobileCountryCode"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    iget-object v0, p0, Lcom/msc/a/h;->m:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 451
    const-string v0, ""

    const-string v1, "mobileCountryCode"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    :cond_4
    iget-object v0, p0, Lcom/msc/a/h;->j:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 456
    const-string v0, ""

    const-string v1, "password"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    iget-object v0, p0, Lcom/msc/a/h;->j:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->c(Ljava/lang/String;)V

    .line 458
    const-string v0, ""

    const-string v1, "password"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    :cond_5
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 463
    const-string v1, "003"

    .line 464
    const-string v0, "N"

    .line 465
    iget-object v4, p0, Lcom/msc/a/h;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 467
    const-string v1, "001"

    .line 468
    const-string v0, "Y"

    .line 470
    :cond_6
    const-string v4, ""

    const-string v5, "loginIDTypeCode"

    invoke-virtual {v2, v4, v5}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    invoke-virtual {v2, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 472
    const-string v1, ""

    const-string v4, "loginIDTypeCode"

    invoke-virtual {v2, v1, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    const-string v1, ""

    const-string v4, "checkTelephoneNumberValidation"

    invoke-virtual {v2, v1, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 476
    const-string v0, ""

    const-string v1, "checkTelephoneNumberValidation"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    iget-object v0, p0, Lcom/msc/a/h;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 480
    const-string v0, ""

    const-string v1, "countryCallingCode"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    iget-object v0, p0, Lcom/msc/a/h;->k:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 482
    const-string v0, ""

    const-string v1, "countryCallingCode"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    :cond_7
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 487
    iget-object v0, p0, Lcom/msc/a/h;->o:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 489
    const-string v0, ""

    const-string v1, "telephoneIDDuplicationCheckYNFlag"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    iget-object v0, p0, Lcom/msc/a/h;->o:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 491
    const-string v0, ""

    const-string v1, "telephoneIDDuplicationCheckYNFlag"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    :cond_8
    iget-object v0, p0, Lcom/msc/a/h;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 507
    const-string v0, ""

    const-string v1, "userID"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    iget-object v0, p0, Lcom/msc/a/h;->p:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 509
    const-string v0, ""

    const-string v1, "userID"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    :cond_9
    iget-object v0, p0, Lcom/msc/a/h;->q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 514
    const-string v0, ""

    const-string v1, "confirmChangeLoginIDYNFlag"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    iget-object v0, p0, Lcom/msc/a/h;->q:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 516
    const-string v0, ""

    const-string v1, "confirmChangeLoginIDYNFlag"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    :cond_a
    const-string v0, ""

    const-string v1, "TncMandatoryRequestVO"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    invoke-virtual {v2}, Lcom/msc/c/q;->a()V

    .line 523
    invoke-virtual {v3}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 524
    return-object v0

    .line 380
    :cond_b
    const-string v0, "N"

    goto/16 :goto_0

    .line 384
    :cond_c
    const-string v0, "N"

    goto/16 :goto_1

    .line 390
    :cond_d
    const-string v0, "N"

    goto/16 :goto_2

    .line 397
    :cond_e
    const-string v0, "N"

    goto/16 :goto_3

    .line 402
    :cond_f
    const-string v0, "N"

    goto/16 :goto_4

    .line 420
    :cond_10
    const-string v0, "N"

    goto/16 :goto_5

    .line 431
    :cond_11
    iget-object v0, p0, Lcom/msc/a/h;->g:Ljava/lang/String;

    if-eqz v0, :cond_3

    const-string v0, "j5p7ll8g33"

    iget-object v1, p0, Lcom/msc/a/h;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 433
    const-string v0, ""

    const-string v1, "package"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    const-string v0, "com.osp.app.signin"

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 435
    const-string v0, ""

    const-string v1, "package"

    invoke-virtual {v2, v0, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/msc/a/h;->a:Ljava/lang/String;

    .line 166
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lcom/msc/a/h;->i:Ljava/lang/String;

    .line 193
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 201
    iput-object p1, p0, Lcom/msc/a/h;->j:Ljava/lang/String;

    .line 202
    return-void
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, Lcom/msc/a/h;->k:Ljava/lang/String;

    .line 211
    return-void
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 325
    iput-object p1, p0, Lcom/msc/a/h;->g:Ljava/lang/String;

    .line 326
    return-void
.end method

.method public final i(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 334
    iput-object p1, p0, Lcom/msc/a/h;->n:Ljava/lang/String;

    .line 335
    return-void
.end method

.method public final j(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 343
    iput-object p1, p0, Lcom/msc/a/h;->h:Ljava/lang/String;

    .line 344
    return-void
.end method

.method public final k(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 533
    iput-object p1, p0, Lcom/msc/a/h;->m:Ljava/lang/String;

    .line 534
    return-void
.end method
