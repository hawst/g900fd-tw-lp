.class final Lcom/msc/b/e;
.super Ljava/lang/Object;
.source "HttpRestClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/msc/b/d;

.field private final synthetic b:Lorg/apache/http/client/methods/HttpUriRequest;


# direct methods
.method constructor <init>(Lcom/msc/b/d;Lorg/apache/http/client/methods/HttpUriRequest;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/msc/b/e;->a:Lcom/msc/b/d;

    iput-object p2, p0, Lcom/msc/b/e;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 783
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 787
    iget-object v0, p0, Lcom/msc/b/e;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/msc/b/e;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 789
    iget-object v0, p0, Lcom/msc/b/e;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    .line 790
    iget-object v0, p0, Lcom/msc/b/e;->a:Lcom/msc/b/d;

    invoke-static {v0}, Lcom/msc/b/d;->a(Lcom/msc/b/d;)V

    .line 791
    const-string v0, "SA"

    const-string v1, "request abort"

    invoke-static {v0, v1}, Lcom/msc/b/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 792
    iget-object v0, p0, Lcom/msc/b/e;->a:Lcom/msc/b/d;

    invoke-static {v0}, Lcom/msc/b/d;->b(Lcom/msc/b/d;)Lcom/msc/b/h;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 794
    new-instance v0, Lcom/msc/b/c;

    invoke-direct {v0}, Lcom/msc/b/c;-><init>()V

    .line 795
    iget-object v1, p0, Lcom/msc/b/e;->a:Lcom/msc/b/d;

    invoke-static {v1}, Lcom/msc/b/d;->c(Lcom/msc/b/d;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/c;->a(J)V

    .line 796
    iget-object v1, p0, Lcom/msc/b/e;->a:Lcom/msc/b/d;

    invoke-static {v1}, Lcom/msc/b/d;->d(Lcom/msc/b/d;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/b/c;->a(Ljava/lang/String;)V

    .line 798
    iget-object v1, p0, Lcom/msc/b/e;->a:Lcom/msc/b/d;

    invoke-static {v1}, Lcom/msc/b/d;->b(Lcom/msc/b/d;)Lcom/msc/b/h;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/msc/b/h;->c(Lcom/msc/b/c;)V

    .line 801
    :cond_0
    return-void
.end method
