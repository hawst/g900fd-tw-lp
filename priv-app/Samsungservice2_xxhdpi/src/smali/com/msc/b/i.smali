.class public final Lcom/msc/b/i;
.super Ljava/lang/Object;
.source "TransactionManager.java"


# static fields
.field private static a:Lcom/msc/b/i;

.field private static c:J


# instance fields
.field private final b:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/msc/b/i;->c:J

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/msc/b/i;->b:Ljava/util/HashMap;

    .line 41
    return-void
.end method

.method public static declared-synchronized a()Lcom/msc/b/i;
    .locals 2

    .prologue
    .line 49
    const-class v1, Lcom/msc/b/i;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/msc/b/i;->a:Lcom/msc/b/i;

    if-eqz v0, :cond_0

    .line 51
    sget-object v0, Lcom/msc/b/i;->a:Lcom/msc/b/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    :goto_0
    monitor-exit v1

    return-object v0

    .line 54
    :cond_0
    :try_start_1
    new-instance v0, Lcom/msc/b/i;

    invoke-direct {v0}, Lcom/msc/b/i;-><init>()V

    .line 56
    sput-object v0, Lcom/msc/b/i;->a:Lcom/msc/b/i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Z)V
    .locals 0

    .prologue
    .line 65
    invoke-static {p0}, Lcom/msc/b/d;->b(Z)V

    .line 66
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;
    .locals 7

    .prologue
    .line 80
    monitor-enter p0

    :try_start_0
    sget-wide v0, Lcom/msc/b/i;->c:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    sput-wide v0, Lcom/msc/b/i;->c:J

    .line 82
    new-instance v1, Lcom/msc/b/d;

    sget-wide v2, Lcom/msc/b/i;->c:J

    move-object v4, p1

    move-object v5, p5

    move v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/msc/b/d;-><init>(JLjava/lang/String;Lcom/msc/b/h;Z)V

    .line 84
    if-eqz p4, :cond_0

    .line 86
    invoke-virtual {v1}, Lcom/msc/b/d;->c()V

    .line 88
    :cond_0
    invoke-virtual {v1, p3}, Lcom/msc/b/d;->a(Z)V

    .line 90
    iget-object v0, p0, Lcom/msc/b/i;->b:Ljava/util/HashMap;

    sget-wide v2, Lcom/msc/b/i;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    monitor-exit p0

    return-object v1

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(J)V
    .locals 3

    .prologue
    .line 103
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/msc/b/i;->b:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/msc/b/i;->b:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/msc/b/d;

    .line 108
    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {v0}, Lcom/msc/b/d;->d()V

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/msc/b/i;->b:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    :cond_1
    monitor-exit p0

    return-void

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(JLcom/msc/b/g;)V
    .locals 3

    .prologue
    .line 153
    iget-object v0, p0, Lcom/msc/b/i;->b:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/msc/b/i;->b:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/msc/b/d;

    .line 157
    if-eqz v0, :cond_0

    .line 159
    invoke-virtual {v0, p3}, Lcom/msc/b/d;->a(Lcom/msc/b/g;)V

    .line 163
    :cond_0
    return-void
.end method

.method public final declared-synchronized b(J)V
    .locals 3

    .prologue
    .line 124
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/msc/b/i;->b:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/msc/b/i;->b:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    :cond_0
    monitor-exit p0

    return-void

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
