.class public final enum Lcom/msc/b/g;
.super Ljava/lang/Enum;
.source "HttpRestClient.java"


# static fields
.field public static final enum a:Lcom/msc/b/g;

.field public static final enum b:Lcom/msc/b/g;

.field public static final enum c:Lcom/msc/b/g;

.field public static final enum d:Lcom/msc/b/g;

.field private static final synthetic e:[Lcom/msc/b/g;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 102
    new-instance v0, Lcom/msc/b/g;

    const-string v1, "DELETE"

    invoke-direct {v0, v1, v2}, Lcom/msc/b/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/msc/b/g;->a:Lcom/msc/b/g;

    new-instance v0, Lcom/msc/b/g;

    const-string v1, "GET"

    invoke-direct {v0, v1, v3}, Lcom/msc/b/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    new-instance v0, Lcom/msc/b/g;

    const-string v1, "POST"

    invoke-direct {v0, v1, v4}, Lcom/msc/b/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    new-instance v0, Lcom/msc/b/g;

    const-string v1, "PUT"

    invoke-direct {v0, v1, v5}, Lcom/msc/b/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/msc/b/g;->d:Lcom/msc/b/g;

    .line 100
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/msc/b/g;

    sget-object v1, Lcom/msc/b/g;->a:Lcom/msc/b/g;

    aput-object v1, v0, v2

    sget-object v1, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    aput-object v1, v0, v3

    sget-object v1, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    aput-object v1, v0, v4

    sget-object v1, Lcom/msc/b/g;->d:Lcom/msc/b/g;

    aput-object v1, v0, v5

    sput-object v0, Lcom/msc/b/g;->e:[Lcom/msc/b/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/msc/b/g;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/msc/b/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/msc/b/g;

    return-object v0
.end method

.method public static values()[Lcom/msc/b/g;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/msc/b/g;->e:[Lcom/msc/b/g;

    array-length v1, v0

    new-array v2, v1, [Lcom/msc/b/g;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
