.class public final Lcom/msc/b/d;
.super Ljava/lang/Object;
.source "HttpRestClient.java"


# static fields
.field private static j:Z

.field private static m:Z

.field private static o:Ljava/security/KeyStore;

.field private static synthetic p:[I


# instance fields
.field private final a:Ljava/util/ArrayList;

.field private final b:Ljava/util/ArrayList;

.field private final c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Lorg/json/JSONObject;

.field private f:Lorg/apache/http/client/HttpClient;

.field private final g:Lcom/msc/b/h;

.field private h:Lorg/apache/http/client/methods/HttpUriRequest;

.field private i:Z

.field private final k:J

.field private l:Z

.field private n:Z


# direct methods
.method constructor <init>(JLjava/lang/String;Lcom/msc/b/h;Z)V
    .locals 5

    .prologue
    const v3, 0xea60

    const/4 v1, 0x0

    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    iput-object p3, p0, Lcom/msc/b/d;->c:Ljava/lang/String;

    .line 178
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/msc/b/d;->b:Ljava/util/ArrayList;

    .line 179
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/msc/b/d;->a:Ljava/util/ArrayList;

    .line 180
    iput-object v1, p0, Lcom/msc/b/d;->d:Ljava/lang/String;

    .line 181
    iput-wide p1, p0, Lcom/msc/b/d;->k:J

    .line 182
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/msc/b/d;->n:Z

    .line 184
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 185
    const-string v1, "http.protocol.version"

    sget-object v2, Lorg/apache/http/HttpVersion;->HTTP_1_1:Lorg/apache/http/HttpVersion;

    invoke-interface {v0, v1, v2}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 186
    invoke-static {v0, v3}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 187
    invoke-static {v0, v3}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 188
    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v1, v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/params/HttpParams;)V

    iput-object v1, p0, Lcom/msc/b/d;->f:Lorg/apache/http/client/HttpClient;

    .line 190
    iput-object p4, p0, Lcom/msc/b/d;->g:Lcom/msc/b/h;

    .line 192
    sput-boolean p5, Lcom/msc/b/d;->m:Z

    .line 194
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    sget-object v0, Lcom/msc/b/d;->o:Ljava/security/KeyStore;

    if-nez v0, :cond_1

    .line 199
    :try_start_0
    invoke-static {}, Ljava/security/KeyStore;->getDefaultType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v0

    .line 200
    sput-object v0, Lcom/msc/b/d;->o:Ljava/security/KeyStore;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 201
    const-string v0, "AndroidCAStore"

    invoke-static {v0}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v1

    .line 202
    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 203
    invoke-virtual {v1}, Ljava/security/KeyStore;->aliases()Ljava/util/Enumeration;

    move-result-object v2

    .line 204
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-nez v0, :cond_2

    .line 217
    :cond_1
    return-void

    .line 206
    :cond_2
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 207
    const-string v3, "system:"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 209
    sget-object v3, Lcom/msc/b/d;->o:Ljava/security/KeyStore;

    invoke-virtual {v1, v0}, Ljava/security/KeyStore;->getCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/security/KeyStore;->setCertificateEntry(Ljava/lang/String;Ljava/security/cert/Certificate;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 214
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Failed to create a keystore containing our trusted system CAs"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic a(Lcom/msc/b/d;)V
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/msc/b/d;->h:Lorg/apache/http/client/methods/HttpUriRequest;

    return-void
.end method

.method private a(Lorg/apache/http/HttpResponse;)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/16 v9, 0xc8

    const/4 v0, 0x0

    .line 475
    .line 481
    new-instance v4, Lcom/msc/b/c;

    invoke-direct {v4}, Lcom/msc/b/c;-><init>()V

    .line 485
    iget-wide v2, p0, Lcom/msc/b/d;->k:J

    invoke-virtual {v4, v2, v3}, Lcom/msc/b/c;->a(J)V

    .line 486
    iget-object v2, p0, Lcom/msc/b/d;->c:Ljava/lang/String;

    invoke-virtual {v4, v2}, Lcom/msc/b/c;->a(Ljava/lang/String;)V

    .line 488
    if-eqz p1, :cond_9

    .line 491
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 492
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v5

    .line 493
    array-length v6, v5

    move v2, v0

    :goto_0
    if-lt v2, v6, :cond_2

    .line 497
    invoke-virtual {v4, v3}, Lcom/msc/b/c;->a(Ljava/util/HashMap;)V

    .line 499
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 501
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    .line 502
    if-eqz v3, :cond_8

    .line 504
    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    .line 507
    :goto_1
    if-eqz v2, :cond_7

    .line 512
    :try_start_0
    iget-boolean v0, p0, Lcom/msc/b/d;->l:Z

    if-eqz v0, :cond_3

    if-ne v3, v9, :cond_3

    .line 514
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;
    :try_end_0
    .catch Lorg/apache/http/ParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 516
    :try_start_1
    invoke-virtual {v4, v2}, Lcom/msc/b/c;->a(Ljava/io/InputStream;)V
    :try_end_1
    .catch Lorg/apache/http/ParseException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move v0, v3

    .line 537
    :goto_2
    if-ne v0, v9, :cond_5

    .line 539
    if-nez v1, :cond_4

    if-nez v2, :cond_4

    .line 541
    const-string v1, "SA"

    const-string v2, "====================RESPONSE===================="

    invoke-static {v1, v2}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    const-string v1, "SA"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "StatusCode=["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    const-string v0, "SA"

    const-string v1, "content is null"

    invoke-static {v0, v1}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    const-string v0, "SA"

    const-string v1, "================================================"

    invoke-static {v0, v1}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    iget-object v0, p0, Lcom/msc/b/d;->g:Lcom/msc/b/h;

    if-eqz v0, :cond_0

    .line 547
    iget-object v0, p0, Lcom/msc/b/d;->g:Lcom/msc/b/h;

    invoke-interface {v0, v4}, Lcom/msc/b/h;->b(Lcom/msc/b/c;)V

    .line 549
    :cond_0
    const-string v0, "SA"

    const-string v1, "Status code is 200, but content is null"

    invoke-static {v0, v1}, Lcom/msc/b/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    :cond_1
    :goto_3
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    iget-wide v2, p0, Lcom/msc/b/d;->k:J

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/i;->b(J)V

    .line 575
    return-void

    .line 493
    :cond_2
    aget-object v7, v5, v2

    .line 495
    invoke-interface {v7}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v8, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 493
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 521
    :cond_3
    :try_start_2
    const-string v0, "UTF-8"

    invoke-static {v2, v0}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Lorg/apache/http/ParseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v2

    .line 523
    :try_start_3
    invoke-virtual {v4, v2}, Lcom/msc/b/c;->b(Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/apache/http/ParseException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    move v0, v3

    move-object v10, v2

    move-object v2, v1

    move-object v1, v10

    .line 526
    goto :goto_2

    .line 528
    :catch_0
    move-exception v0

    move-object v2, v1

    :goto_4
    invoke-virtual {v0}, Lorg/apache/http/ParseException;->printStackTrace()V

    move v0, v3

    goto :goto_2

    .line 529
    :catch_1
    move-exception v0

    move-object v2, v1

    :goto_5
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move v0, v3

    goto :goto_2

    .line 552
    :cond_4
    const-string v2, "SA"

    const-string v3, "====================RESPONSE===================="

    invoke-static {v2, v3}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    const-string v2, "SA"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "StatusCode=["

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    const-string v0, "SA"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    const-string v0, "SA"

    const-string v1, "================================================"

    invoke-static {v0, v1}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    iget-object v0, p0, Lcom/msc/b/d;->g:Lcom/msc/b/h;

    if-eqz v0, :cond_1

    .line 558
    iget-object v0, p0, Lcom/msc/b/d;->g:Lcom/msc/b/h;

    invoke-interface {v0, v4}, Lcom/msc/b/h;->a(Lcom/msc/b/c;)V

    goto :goto_3

    .line 563
    :cond_5
    const-string v2, "SA"

    const-string v3, "====================RESPONSE===================="

    invoke-static {v2, v3}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    const-string v2, "SA"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "StatusCode=["

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    const-string v0, "SA"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    const-string v0, "SA"

    const-string v1, "================================================"

    invoke-static {v0, v1}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    iget-object v0, p0, Lcom/msc/b/d;->g:Lcom/msc/b/h;

    if-eqz v0, :cond_6

    .line 569
    iget-object v0, p0, Lcom/msc/b/d;->g:Lcom/msc/b/h;

    invoke-interface {v0, v4}, Lcom/msc/b/h;->b(Lcom/msc/b/c;)V

    .line 571
    :cond_6
    const-string v0, "SA"

    const-string v1, "Request error occured"

    invoke-static {v0, v1}, Lcom/msc/b/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 529
    :catch_2
    move-exception v0

    goto/16 :goto_5

    :catch_3
    move-exception v0

    move-object v10, v2

    move-object v2, v1

    move-object v1, v10

    goto/16 :goto_5

    .line 528
    :catch_4
    move-exception v0

    goto/16 :goto_4

    :catch_5
    move-exception v0

    move-object v10, v2

    move-object v2, v1

    move-object v1, v10

    goto/16 :goto_4

    :cond_7
    move v0, v3

    move-object v2, v1

    goto/16 :goto_2

    :cond_8
    move v3, v0

    goto/16 :goto_1

    :cond_9
    move-object v2, v1

    goto/16 :goto_2
.end method

.method private static a(Lorg/apache/http/client/HttpClient;)V
    .locals 1

    .prologue
    .line 458
    .line 459
    if-eqz p0, :cond_0

    .line 461
    invoke-interface {p0}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    .line 462
    if-eqz v0, :cond_0

    .line 464
    invoke-interface {v0}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 467
    :cond_0
    return-void
.end method

.method private a(Lorg/apache/http/client/methods/HttpUriRequest;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 300
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 302
    iget-object v0, p0, Lcom/msc/b/d;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/msc/b/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 304
    const-string v0, "SA"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Header=["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/msc/b/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/msc/b/d;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/msc/b/d;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 308
    const-string v0, "SA"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Param=["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/msc/b/d;->b:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    :cond_1
    iget-object v0, p0, Lcom/msc/b/d;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/msc/b/d;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 312
    const-string v0, "SA"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "XmlParam=["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/msc/b/d;->d:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    :cond_2
    iget-object v0, p0, Lcom/msc/b/d;->e:Lorg/json/JSONObject;

    if-eqz v0, :cond_3

    .line 316
    const-string v0, "SA"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "JsonObject=["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/msc/b/d;->e:Lorg/json/JSONObject;

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    :cond_3
    const-string v0, "SA"

    const-string v4, "================================================"

    invoke-static {v0, v4}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    invoke-static {p1}, Lcom/msc/b/d;->b(Lorg/apache/http/client/methods/HttpUriRequest;)Ljava/lang/String;

    move-result-object v4

    .line 322
    const-string v0, "SA"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "executeRequest ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] start"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    :try_start_0
    iput-object p1, p0, Lcom/msc/b/d;->h:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 330
    iget-boolean v0, p0, Lcom/msc/b/d;->n:Z

    if-eqz v0, :cond_4

    .line 332
    const-string v0, "SA"

    const-string v1, "re-cancelRequest"

    invoke-static {v0, v1}, Lcom/msc/b/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    invoke-virtual {p0}, Lcom/msc/b/d;->d()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 398
    iget-object v0, p0, Lcom/msc/b/d;->f:Lorg/apache/http/client/HttpClient;

    invoke-static {v0}, Lcom/msc/b/d;->a(Lorg/apache/http/client/HttpClient;)V

    .line 399
    const-string v0, "SA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "executeRequest ["

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "] end. "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/b/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    :goto_0
    return-void

    .line 337
    :cond_4
    :try_start_1
    iget-boolean v0, p0, Lcom/msc/b/d;->i:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_5

    .line 342
    :try_start_2
    invoke-static {}, Ljava/security/KeyStore;->getDefaultType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v0

    .line 343
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 345
    new-instance v5, Lcom/msc/b/a;

    invoke-direct {v5, v0}, Lcom/msc/b/a;-><init>(Ljava/security/KeyStore;)V

    .line 346
    sget-object v0, Lorg/apache/http/conn/ssl/SSLSocketFactory;->ALLOW_ALL_HOSTNAME_VERIFIER:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    invoke-virtual {v5, v0}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->setHostnameVerifier(Lorg/apache/http/conn/ssl/X509HostnameVerifier;)V

    .line 348
    new-instance v6, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v6}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 349
    new-instance v0, Lorg/apache/http/conn/scheme/Scheme;

    const-string v7, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v8

    const/16 v9, 0x50

    invoke-direct {v0, v7, v8, v9}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v6, v0}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 350
    new-instance v0, Lorg/apache/http/conn/scheme/Scheme;

    const-string v7, "https"

    const/16 v8, 0x1bb

    invoke-direct {v0, v7, v5, v8}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v6, v0}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 352
    new-instance v0, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    iget-object v5, p0, Lcom/msc/b/d;->f:Lorg/apache/http/client/HttpClient;

    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v5

    invoke-direct {v0, v5, v6}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 357
    :goto_1
    :try_start_3
    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    iget-object v5, p0, Lcom/msc/b/d;->f:Lorg/apache/http/client/HttpClient;

    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v5

    invoke-direct {v1, v0, v5}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    .line 358
    iput-object v1, p0, Lcom/msc/b/d;->f:Lorg/apache/http/client/HttpClient;

    .line 376
    :goto_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 378
    iget-object v5, p0, Lcom/msc/b/d;->f:Lorg/apache/http/client/HttpClient;

    invoke-interface {v5, p1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v5

    .line 380
    const-string v6, "SA"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "["

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] api execute + "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v0, v8, v0

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/msc/b/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    if-eqz v5, :cond_8

    .line 384
    const-string v0, "SA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "executeRequest ["

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, "] httpResponse : "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/b/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    :goto_3
    invoke-direct {p0, v5}, Lcom/msc/b/d;->a(Lorg/apache/http/HttpResponse;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 398
    iget-object v0, p0, Lcom/msc/b/d;->f:Lorg/apache/http/client/HttpClient;

    invoke-static {v0}, Lcom/msc/b/d;->a(Lorg/apache/http/client/HttpClient;)V

    .line 399
    const-string v0, "SA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "executeRequest ["

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "] end. "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/b/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 353
    :catch_0
    move-exception v0

    move-object v0, v1

    goto/16 :goto_1

    .line 359
    :cond_5
    :try_start_4
    sget-boolean v0, Lcom/msc/b/d;->j:Z

    if-nez v0, :cond_7

    sget-object v0, Lcom/msc/b/d;->o:Ljava/security/KeyStore;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v0, :cond_7

    .line 363
    :try_start_5
    new-instance v0, Lorg/apache/http/conn/ssl/SSLSocketFactory;

    sget-object v1, Lcom/msc/b/d;->o:Ljava/security/KeyStore;

    invoke-direct {v0, v1}, Lorg/apache/http/conn/ssl/SSLSocketFactory;-><init>(Ljava/security/KeyStore;)V

    .line 364
    iget-object v1, p0, Lcom/msc/b/d;->f:Lorg/apache/http/client/HttpClient;

    invoke-interface {v1}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/conn/ClientConnectionManager;->getSchemeRegistry()Lorg/apache/http/conn/scheme/SchemeRegistry;

    move-result-object v1

    .line 365
    new-instance v5, Lorg/apache/http/conn/scheme/Scheme;

    const-string v6, "https"

    const/16 v7, 0x1bb

    invoke-direct {v5, v6, v0, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v1, v5}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 366
    const-string v0, "SA"

    const-string v1, "Security=[true]"

    invoke-static {v0, v1}, Lcom/msc/b/f;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_2

    .line 369
    :catch_1
    move-exception v0

    :try_start_6
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Failed to apply a keystore containing our trusted system CAs"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 391
    :catch_2
    move-exception v0

    .line 393
    :try_start_7
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v1

    iget-wide v6, p0, Lcom/msc/b/d;->k:J

    invoke-virtual {v1, v6, v7}, Lcom/msc/b/i;->b(J)V

    new-instance v1, Lcom/msc/b/c;

    invoke-direct {v1}, Lcom/msc/b/c;-><init>()V

    iget-wide v6, p0, Lcom/msc/b/d;->k:J

    invoke-virtual {v1, v6, v7}, Lcom/msc/b/c;->a(J)V

    iget-object v5, p0, Lcom/msc/b/d;->c:Ljava/lang/String;

    invoke-virtual {v1, v5}, Lcom/msc/b/c;->a(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/msc/b/c;->a(Ljava/lang/Exception;)V

    iget-object v5, p0, Lcom/msc/b/d;->g:Lcom/msc/b/h;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/msc/b/d;->g:Lcom/msc/b/h;

    invoke-interface {v5, v1}, Lcom/msc/b/h;->b(Lcom/msc/b/c;)V

    .line 395
    :cond_6
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 398
    iget-object v0, p0, Lcom/msc/b/d;->f:Lorg/apache/http/client/HttpClient;

    invoke-static {v0}, Lcom/msc/b/d;->a(Lorg/apache/http/client/HttpClient;)V

    .line 399
    const-string v0, "SA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "executeRequest ["

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "] end. "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/b/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 373
    :cond_7
    :try_start_8
    const-string v0, "SA"

    const-string v1, "Security=[false]"

    invoke-static {v0, v1}, Lcom/msc/b/f;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_2

    .line 397
    :catchall_0
    move-exception v0

    .line 398
    iget-object v1, p0, Lcom/msc/b/d;->f:Lorg/apache/http/client/HttpClient;

    invoke-static {v1}, Lcom/msc/b/d;->a(Lorg/apache/http/client/HttpClient;)V

    .line 399
    const-string v1, "SA"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "executeRequest ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] end. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v2, v6, v2

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/msc/b/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    throw v0

    .line 387
    :cond_8
    :try_start_9
    const-string v0, "SA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "executeRequest ["

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, "] httpResponse : null"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/b/f;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_3
.end method

.method private a(Lorg/apache/http/message/AbstractHttpMessage;)V
    .locals 3

    .prologue
    .line 630
    if-nez p1, :cond_1

    .line 639
    :cond_0
    return-void

    .line 635
    :cond_1
    iget-object v0, p0, Lcom/msc/b/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/NameValuePair;

    .line 637
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lorg/apache/http/message/AbstractHttpMessage;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/msc/b/d;)Lcom/msc/b/h;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/msc/b/d;->g:Lcom/msc/b/h;

    return-object v0
.end method

.method private static b(Lorg/apache/http/client/methods/HttpUriRequest;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 433
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 434
    invoke-interface {p0}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 435
    array-length v3, v2

    :goto_0
    if-lt v0, v3, :cond_0

    .line 445
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 448
    :goto_1
    return-object v0

    .line 435
    :cond_0
    aget-object v4, v2, v0

    .line 437
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x1

    if-le v5, v6, :cond_1

    .line 439
    const/4 v5, 0x0

    const/4 v6, 0x2

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 435
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 442
    :cond_1
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 448
    :catch_0
    move-exception v0

    const-string v0, ""

    goto :goto_1
.end method

.method public static b(Z)V
    .locals 0

    .prologue
    .line 821
    sput-boolean p0, Lcom/msc/b/d;->j:Z

    .line 822
    return-void
.end method

.method static synthetic c(Lcom/msc/b/d;)J
    .locals 2

    .prologue
    .line 150
    iget-wide v0, p0, Lcom/msc/b/d;->k:J

    return-wide v0
.end method

.method static synthetic d(Lcom/msc/b/d;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/msc/b/d;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e()Z
    .locals 1

    .prologue
    .line 159
    sget-boolean v0, Lcom/msc/b/d;->m:Z

    return v0
.end method

.method private static synthetic f()[I
    .locals 3

    .prologue
    .line 58
    sget-object v0, Lcom/msc/b/d;->p:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/msc/b/g;->values()[Lcom/msc/b/g;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/msc/b/g;->a:Lcom/msc/b/g;

    invoke-virtual {v1}, Lcom/msc/b/g;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-virtual {v1}, Lcom/msc/b/g;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-virtual {v1}, Lcom/msc/b/g;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Lcom/msc/b/g;->d:Lcom/msc/b/g;

    invoke-virtual {v1}, Lcom/msc/b/g;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lcom/msc/b/d;->p:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 225
    iget-wide v0, p0, Lcom/msc/b/d;->k:J

    return-wide v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/msc/b/d;->f:Lorg/apache/http/client/HttpClient;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/msc/b/d;->f:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    .line 237
    if-eqz v0, :cond_0

    .line 239
    invoke-static {v0, p1}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 240
    invoke-static {v0, p1}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 245
    :cond_0
    return-void
.end method

.method public final a(Lcom/msc/b/g;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 648
    const-string v0, "SA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Execute method=["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] start"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/b/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    const-string v0, "SA"

    const-string v1, "=====================REQUEST===================="

    invoke-static {v0, v1}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 652
    :try_start_0
    invoke-static {}, Lcom/msc/b/d;->f()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/msc/b/g;->ordinal()I

    move-result v1

    aget v0, v0, v1
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    packed-switch v0, :pswitch_data_0

    .line 759
    :goto_0
    const-string v0, "SA"

    const-string v1, "Execute end"

    invoke-static {v0, v1}, Lcom/msc/b/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 760
    return-void

    .line 656
    :pswitch_0
    :try_start_1
    const-string v0, ""

    .line 657
    iget-object v1, p0, Lcom/msc/b/d;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 659
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "?"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 660
    iget-object v0, p0, Lcom/msc/b/d;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 675
    :cond_0
    const-string v1, "SA"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GET "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/b/d;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/msc/b/d;->c:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 677
    invoke-direct {p0, v1}, Lcom/msc/b/d;->a(Lorg/apache/http/message/AbstractHttpMessage;)V

    .line 679
    invoke-direct {p0, v1}, Lcom/msc/b/d;->a(Lorg/apache/http/client/methods/HttpUriRequest;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 757
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 660
    :cond_1
    :try_start_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/NameValuePair;

    .line 664
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    const-string v4, "UTF-8"

    invoke-static {v0, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 666
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v5, :cond_2

    .line 668
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "&"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 671
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 684
    :pswitch_1
    const-string v0, "SA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PUT "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/b/d;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    new-instance v0, Lorg/apache/http/client/methods/HttpPut;

    iget-object v1, p0, Lcom/msc/b/d;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    .line 686
    invoke-direct {p0, v0}, Lcom/msc/b/d;->a(Lorg/apache/http/message/AbstractHttpMessage;)V

    .line 687
    iget-object v1, p0, Lcom/msc/b/d;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 689
    new-instance v1, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    iget-object v2, p0, Lcom/msc/b/d;->b:Ljava/util/ArrayList;

    const-string v3, "UTF-8"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 702
    :goto_2
    invoke-direct {p0, v0}, Lcom/msc/b/d;->a(Lorg/apache/http/client/methods/HttpUriRequest;)V

    goto/16 :goto_0

    .line 690
    :cond_3
    iget-object v1, p0, Lcom/msc/b/d;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 692
    new-instance v1, Lorg/apache/http/entity/StringEntity;

    iget-object v2, p0, Lcom/msc/b/d;->d:Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    goto :goto_2

    .line 694
    :cond_4
    iget-object v1, p0, Lcom/msc/b/d;->e:Lorg/json/JSONObject;

    if-eqz v1, :cond_5

    .line 696
    new-instance v1, Lorg/apache/http/entity/StringEntity;

    iget-object v2, p0, Lcom/msc/b/d;->e:Lorg/json/JSONObject;

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    goto :goto_2

    .line 700
    :cond_5
    const-string v1, "SA"

    const-string v2, "Parameter error!!!"

    invoke-static {v1, v2}, Lcom/msc/b/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 707
    :pswitch_2
    const-string v0, "SA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "POST "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/b/d;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    iget-object v1, p0, Lcom/msc/b/d;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 709
    invoke-direct {p0, v0}, Lcom/msc/b/d;->a(Lorg/apache/http/message/AbstractHttpMessage;)V

    .line 710
    iget-object v1, p0, Lcom/msc/b/d;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 712
    new-instance v1, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    iget-object v2, p0, Lcom/msc/b/d;->b:Ljava/util/ArrayList;

    const-string v3, "UTF-8"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 725
    :goto_3
    invoke-direct {p0, v0}, Lcom/msc/b/d;->a(Lorg/apache/http/client/methods/HttpUriRequest;)V

    goto/16 :goto_0

    .line 713
    :cond_6
    iget-object v1, p0, Lcom/msc/b/d;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 715
    new-instance v1, Lorg/apache/http/entity/StringEntity;

    iget-object v2, p0, Lcom/msc/b/d;->d:Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    goto :goto_3

    .line 716
    :cond_7
    iget-object v1, p0, Lcom/msc/b/d;->e:Lorg/json/JSONObject;

    if-eqz v1, :cond_8

    .line 718
    new-instance v1, Lorg/apache/http/entity/StringEntity;

    iget-object v2, p0, Lcom/msc/b/d;->e:Lorg/json/JSONObject;

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    goto :goto_3

    .line 722
    :cond_8
    const-string v1, "SA"

    const-string v2, "Parameter error!!!"

    invoke-static {v1, v2}, Lcom/msc/b/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 730
    :pswitch_3
    const-string v0, ""

    .line 731
    iget-object v1, p0, Lcom/msc/b/d;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    .line 733
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "?"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 734
    iget-object v0, p0, Lcom/msc/b/d;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_a

    move-object v0, v1

    .line 746
    :cond_9
    const-string v1, "SA"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DELETE "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/b/d;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 747
    new-instance v1, Lorg/apache/http/client/methods/HttpDelete;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/msc/b/d;->c:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpDelete;-><init>(Ljava/lang/String;)V

    .line 748
    invoke-direct {p0, v1}, Lcom/msc/b/d;->a(Lorg/apache/http/message/AbstractHttpMessage;)V

    .line 750
    invoke-direct {p0, v1}, Lcom/msc/b/d;->a(Lorg/apache/http/client/methods/HttpUriRequest;)V

    goto/16 :goto_0

    .line 734
    :cond_a
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/NameValuePair;

    .line 736
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    const-string v4, "UTF-8"

    invoke-static {v0, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 737
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v5, :cond_b

    .line 739
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "&"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_4

    .line 742
    :cond_b
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v1

    goto/16 :goto_4

    .line 652
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 613
    iget-object v0, p0, Lcom/msc/b/d;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 614
    iput-object p1, p0, Lcom/msc/b/d;->d:Ljava/lang/String;

    .line 615
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/msc/b/d;->e:Lorg/json/JSONObject;

    .line 616
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 584
    if-nez p2, :cond_0

    .line 586
    const-string v0, "SA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "addHeader - value is null : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    :cond_0
    iget-object v0, p0, Lcom/msc/b/d;->a:Ljava/util/ArrayList;

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    invoke-direct {v1, p1, p2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 589
    return-void
.end method

.method public final a(Lorg/json/JSONObject;)V
    .locals 1

    .prologue
    .line 619
    iget-object v0, p0, Lcom/msc/b/d;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 620
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/msc/b/d;->d:Ljava/lang/String;

    .line 621
    iput-object p1, p0, Lcom/msc/b/d;->e:Lorg/json/JSONObject;

    .line 622
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 811
    iput-boolean p1, p0, Lcom/msc/b/d;->i:Z

    .line 813
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 251
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/msc/b/d;->l:Z

    .line 253
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 598
    if-nez p2, :cond_0

    .line 600
    const-string v0, "SA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "addParam - value is null : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    :cond_0
    iget-object v0, p0, Lcom/msc/b/d;->b:Ljava/util/ArrayList;

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    invoke-direct {v1, p1, p2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 603
    iput-object v3, p0, Lcom/msc/b/d;->d:Ljava/lang/String;

    .line 604
    iput-object v3, p0, Lcom/msc/b/d;->e:Lorg/json/JSONObject;

    .line 605
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 279
    const-string v0, "SA"

    const-string v1, "setNoProxy()"

    invoke-static {v0, v1}, Lcom/msc/b/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    iget-object v0, p0, Lcom/msc/b/d;->f:Lorg/apache/http/client/HttpClient;

    if-nez v0, :cond_0

    .line 291
    :goto_0
    return-void

    .line 286
    :cond_0
    new-instance v0, Lorg/apache/http/HttpHost;

    sget-object v1, Lorg/apache/http/conn/params/ConnRouteParams;->NO_HOST:Lorg/apache/http/HttpHost;

    invoke-direct {v0, v1}, Lorg/apache/http/HttpHost;-><init>(Lorg/apache/http/HttpHost;)V

    .line 289
    iget-object v1, p0, Lcom/msc/b/d;->f:Lorg/apache/http/client/HttpClient;

    invoke-interface {v1}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v1

    const-string v2, "http.route.default-proxy"

    invoke-interface {v1, v2, v0}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 777
    const-string v0, "SA"

    const-string v1, "cancelRequest"

    invoke-static {v0, v1}, Lcom/msc/b/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 779
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/msc/b/d;->n:Z

    .line 781
    iget-object v0, p0, Lcom/msc/b/d;->h:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 783
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/msc/b/e;

    invoke-direct {v2, p0, v0}, Lcom/msc/b/e;-><init>(Lcom/msc/b/d;Lorg/apache/http/client/methods/HttpUriRequest;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 802
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 803
    return-void
.end method
