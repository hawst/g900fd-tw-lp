.class public final enum Lcom/msc/contentprovider/a;
.super Ljava/lang/Enum;
.source "SamsungServiceProvider.java"


# static fields
.field public static final enum a:Lcom/msc/contentprovider/a;

.field public static final enum b:Lcom/msc/contentprovider/a;

.field public static final enum c:Lcom/msc/contentprovider/a;

.field public static final enum d:Lcom/msc/contentprovider/a;

.field public static final enum e:Lcom/msc/contentprovider/a;

.field public static final enum f:Lcom/msc/contentprovider/a;

.field public static final enum g:Lcom/msc/contentprovider/a;

.field public static final enum h:Lcom/msc/contentprovider/a;

.field public static final enum i:Lcom/msc/contentprovider/a;

.field private static final synthetic j:[Lcom/msc/contentprovider/a;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 194
    new-instance v0, Lcom/msc/contentprovider/a;

    const-string v1, "ID"

    invoke-direct {v0, v1, v3}, Lcom/msc/contentprovider/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/msc/contentprovider/a;->a:Lcom/msc/contentprovider/a;

    new-instance v0, Lcom/msc/contentprovider/a;

    const-string v1, "USER_ID"

    invoke-direct {v0, v1, v4}, Lcom/msc/contentprovider/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/msc/contentprovider/a;->b:Lcom/msc/contentprovider/a;

    new-instance v0, Lcom/msc/contentprovider/a;

    const-string v1, "USERAUTH_TOKEN"

    invoke-direct {v0, v1, v5}, Lcom/msc/contentprovider/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/msc/contentprovider/a;->c:Lcom/msc/contentprovider/a;

    new-instance v0, Lcom/msc/contentprovider/a;

    const-string v1, "EMAILID"

    invoke-direct {v0, v1, v6}, Lcom/msc/contentprovider/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/msc/contentprovider/a;->d:Lcom/msc/contentprovider/a;

    new-instance v0, Lcom/msc/contentprovider/a;

    const-string v1, "USER_PASSWORD"

    invoke-direct {v0, v1, v7}, Lcom/msc/contentprovider/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/msc/contentprovider/a;->e:Lcom/msc/contentprovider/a;

    new-instance v0, Lcom/msc/contentprovider/a;

    const-string v1, "BIRTHDATE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/msc/contentprovider/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/msc/contentprovider/a;->f:Lcom/msc/contentprovider/a;

    new-instance v0, Lcom/msc/contentprovider/a;

    const-string v1, "MCC"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/msc/contentprovider/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/msc/contentprovider/a;->g:Lcom/msc/contentprovider/a;

    new-instance v0, Lcom/msc/contentprovider/a;

    const-string v1, "COUNTRY_CODE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/msc/contentprovider/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/msc/contentprovider/a;->h:Lcom/msc/contentprovider/a;

    new-instance v0, Lcom/msc/contentprovider/a;

    const-string v1, "ACCESS_TOKEN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/msc/contentprovider/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/msc/contentprovider/a;->i:Lcom/msc/contentprovider/a;

    .line 191
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/msc/contentprovider/a;

    sget-object v1, Lcom/msc/contentprovider/a;->a:Lcom/msc/contentprovider/a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/msc/contentprovider/a;->b:Lcom/msc/contentprovider/a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/msc/contentprovider/a;->c:Lcom/msc/contentprovider/a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/msc/contentprovider/a;->d:Lcom/msc/contentprovider/a;

    aput-object v1, v0, v6

    sget-object v1, Lcom/msc/contentprovider/a;->e:Lcom/msc/contentprovider/a;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/msc/contentprovider/a;->f:Lcom/msc/contentprovider/a;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/msc/contentprovider/a;->g:Lcom/msc/contentprovider/a;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/msc/contentprovider/a;->h:Lcom/msc/contentprovider/a;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/msc/contentprovider/a;->i:Lcom/msc/contentprovider/a;

    aput-object v2, v0, v1

    sput-object v0, Lcom/msc/contentprovider/a;->j:[Lcom/msc/contentprovider/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 191
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/msc/contentprovider/a;
    .locals 1

    .prologue
    .line 191
    const-class v0, Lcom/msc/contentprovider/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/msc/contentprovider/a;

    return-object v0
.end method

.method public static values()[Lcom/msc/contentprovider/a;
    .locals 1

    .prologue
    .line 191
    sget-object v0, Lcom/msc/contentprovider/a;->j:[Lcom/msc/contentprovider/a;

    invoke-virtual {v0}, [Lcom/msc/contentprovider/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/msc/contentprovider/a;

    return-object v0
.end method
