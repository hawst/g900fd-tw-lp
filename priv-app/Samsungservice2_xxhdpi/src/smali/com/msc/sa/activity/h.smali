.class final Lcom/msc/sa/activity/h;
.super Lcom/msc/c/b;
.source "RequestAccessTokenActivity.java"


# instance fields
.field final synthetic c:Lcom/msc/sa/activity/RequestAccessTokenActivity;

.field private d:J

.field private e:J

.field private f:J

.field private g:Z

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/msc/sa/activity/RequestAccessTokenActivity;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 560
    iput-object p1, p0, Lcom/msc/sa/activity/h;->c:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    .line 561
    invoke-direct {p0, p2}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 548
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/msc/sa/activity/h;->g:Z

    .line 562
    invoke-virtual {p1}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "progress_theme"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 563
    if-eqz v0, :cond_0

    .line 565
    const-string v1, "invisible"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 567
    invoke-virtual {p0}, Lcom/msc/sa/activity/h;->c()V

    .line 571
    :cond_0
    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    .line 740
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 741
    iget-object v0, p0, Lcom/msc/sa/activity/h;->c:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    const-string v1, "j5p7ll8g33"

    iget-object v2, p0, Lcom/msc/sa/activity/h;->c:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v2}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/msc/sa/activity/h;->c:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v3}, Lcom/msc/c/e;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3, p0}, Lcom/msc/c/g;->i(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/msc/sa/activity/h;->d:J

    .line 743
    iget-wide v0, p0, Lcom/msc/sa/activity/h;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/msc/sa/activity/h;->a(J)V

    .line 744
    iget-wide v0, p0, Lcom/msc/sa/activity/h;->d:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/msc/sa/activity/h;->a(JLjava/lang/String;)V

    .line 745
    iget-wide v0, p0, Lcom/msc/sa/activity/h;->d:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 746
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 605
    invoke-direct {p0}, Lcom/msc/sa/activity/h;->h()V

    .line 606
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 619
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 621
    if-nez p1, :cond_1

    .line 682
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 626
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 627
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 629
    iget-wide v4, p0, Lcom/msc/sa/activity/h;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_3

    .line 633
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->O(Ljava/lang/String;)Z

    move-result v0

    .line 635
    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 637
    const-string v0, "Success"

    iput-object v0, p0, Lcom/msc/sa/activity/h;->h:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 642
    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 645
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/msc/sa/activity/h;->h:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 619
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 640
    :cond_2
    :try_start_4
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/msc/sa/activity/h;->h:Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 648
    :cond_3
    :try_start_5
    iget-wide v4, p0, Lcom/msc/sa/activity/h;->e:J
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    .line 652
    :try_start_6
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 653
    invoke-virtual {p0}, Lcom/msc/sa/activity/h;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 657
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/msc/sa/activity/h;->c:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    const-string v1, "authorization_code"

    const-string v3, "j5p7ll8g33"

    const-string v4, "5763D0052DC1462E13751F753384E9A9"

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/msc/sa/activity/h;->f:J

    iget-wide v0, p0, Lcom/msc/sa/activity/h;->f:J

    invoke-virtual {p0, v0, v1}, Lcom/msc/sa/activity/h;->a(J)V

    iget-wide v0, p0, Lcom/msc/sa/activity/h;->f:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/msc/sa/activity/h;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/msc/sa/activity/h;->f:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 658
    :catch_1
    move-exception v0

    .line 660
    :try_start_7
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 661
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/msc/sa/activity/h;->b:Lcom/msc/c/f;

    .line 662
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/msc/sa/activity/h;->h:Ljava/lang/String;

    goto :goto_0

    .line 664
    :cond_4
    iget-wide v4, p0, Lcom/msc/sa/activity/h;->f:J
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 668
    :try_start_8
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/msc/sa/activity/h;->c:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v0, v2}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 669
    iget-object v1, p0, Lcom/msc/sa/activity/h;->c:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v1, v0}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 670
    invoke-virtual {p0}, Lcom/msc/sa/activity/h;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 674
    invoke-direct {p0}, Lcom/msc/sa/activity/h;->h()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 675
    :catch_2
    move-exception v0

    .line 677
    :try_start_9
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 678
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/msc/sa/activity/h;->b:Lcom/msc/c/f;

    .line 679
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/msc/sa/activity/h;->h:Ljava/lang/String;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    .line 575
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 577
    const-string v0, "Fail"

    iget-object v1, p0, Lcom/msc/sa/activity/h;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 579
    iget-object v0, p0, Lcom/msc/sa/activity/h;->c:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    iget-object v1, p0, Lcom/msc/sa/activity/h;->c:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v1}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(Lcom/msc/sa/activity/RequestAccessTokenActivity;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(Lcom/msc/sa/activity/RequestAccessTokenActivity;Landroid/content/Intent;)V

    .line 600
    :cond_0
    :goto_0
    return-void

    .line 581
    :cond_1
    const-string v0, "Success"

    iget-object v1, p0, Lcom/msc/sa/activity/h;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 583
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.osp.app.signin.action.EMAIL_VALIDATION_COMPLETED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->l()I

    move-result v1

    const/16 v2, 0xb

    if-le v1, v2, :cond_2

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_2
    iget-object v1, p0, Lcom/msc/sa/activity/h;->c:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v1, v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/msc/sa/activity/h;->c:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const v1, 0x132df82

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.msc.action.MARKETING_POPUP_NOTIFICATION"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.BackgroundModeService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/msc/sa/activity/h;->c:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v1, v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA"

    const-string v1, "startService MarketingPopupNotiIntent"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    :cond_3
    iget-object v0, p0, Lcom/msc/sa/activity/h;->c:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->b(Lcom/msc/sa/activity/RequestAccessTokenActivity;)V

    goto :goto_0

    .line 586
    :cond_4
    const-string v0, "require re-SignIn"

    iget-object v1, p0, Lcom/msc/sa/activity/h;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588
    invoke-virtual {p0}, Lcom/msc/sa/activity/h;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 593
    iget-object v0, p0, Lcom/msc/sa/activity/h;->c:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 594
    const-string v1, "client_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 595
    const-string v2, "client_secret"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 596
    iget-object v2, p0, Lcom/msc/sa/activity/h;->c:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    const-string v3, "OSP_02"

    invoke-static {v2, v1, v0, v3}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(Lcom/msc/sa/activity/RequestAccessTokenActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 686
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 688
    if-nez p1, :cond_1

    .line 734
    :cond_0
    :goto_0
    return-void

    .line 693
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 696
    iget-wide v2, p0, Lcom/msc/sa/activity/h;->d:J

    cmp-long v2, v0, v2

    if-nez v2, :cond_3

    .line 698
    iget-boolean v0, p0, Lcom/msc/sa/activity/h;->g:Z

    if-nez v0, :cond_2

    const-string v0, "ACF_0403"

    iget-object v1, p0, Lcom/msc/sa/activity/h;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 702
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/msc/sa/activity/h;->b:Lcom/msc/c/f;

    .line 703
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/msc/sa/activity/h;->g:Z

    .line 704
    iget-object v0, p0, Lcom/msc/sa/activity/h;->c:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 705
    invoke-virtual {p0}, Lcom/msc/sa/activity/h;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 709
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/msc/sa/activity/h;->c:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    const-string v1, "j5p7ll8g33"

    iget-object v2, p0, Lcom/msc/sa/activity/h;->c:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v2}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/msc/sa/activity/h;->e:J

    iget-wide v0, p0, Lcom/msc/sa/activity/h;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/msc/sa/activity/h;->a(J)V

    iget-wide v0, p0, Lcom/msc/sa/activity/h;->e:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/msc/sa/activity/h;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/msc/sa/activity/h;->e:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 710
    :catch_0
    move-exception v0

    .line 712
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 713
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/msc/sa/activity/h;->b:Lcom/msc/c/f;

    .line 714
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/msc/sa/activity/h;->h:Ljava/lang/String;

    goto :goto_0

    .line 718
    :cond_2
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/msc/sa/activity/h;->h:Ljava/lang/String;

    goto :goto_0

    .line 721
    :cond_3
    iget-wide v2, p0, Lcom/msc/sa/activity/h;->e:J

    cmp-long v2, v0, v2

    if-nez v2, :cond_5

    .line 723
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/msc/sa/activity/h;->h:Ljava/lang/String;

    .line 725
    const-string v0, "AUT_1302"

    iget-object v1, p0, Lcom/msc/sa/activity/h;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "AUT_1830"

    iget-object v1, p0, Lcom/msc/sa/activity/h;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 727
    :cond_4
    const-string v0, "require re-SignIn"

    iput-object v0, p0, Lcom/msc/sa/activity/h;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 730
    :cond_5
    iget-wide v2, p0, Lcom/msc/sa/activity/h;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 732
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/msc/sa/activity/h;->h:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method protected final d()V
    .locals 3

    .prologue
    .line 611
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 612
    iget-object v0, p0, Lcom/msc/sa/activity/h;->c:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(ILandroid/content/Intent;)V

    .line 613
    iget-object v0, p0, Lcom/msc/sa/activity/h;->c:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->finish()V

    .line 615
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 531
    invoke-virtual {p0}, Lcom/msc/sa/activity/h;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 531
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/msc/sa/activity/h;->a(Ljava/lang/Boolean;)V

    return-void
.end method
