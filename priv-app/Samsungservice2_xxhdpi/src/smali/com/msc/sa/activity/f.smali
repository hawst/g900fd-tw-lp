.class public final Lcom/msc/sa/activity/f;
.super Ljava/lang/Object;
.source "AbstractProcessAsyncTask.java"

# interfaces
.implements Lcom/msc/b/h;


# instance fields
.field final synthetic a:Lcom/msc/sa/activity/a;


# direct methods
.method public constructor <init>(Lcom/msc/sa/activity/a;)V
    .locals 0

    .prologue
    .line 611
    iput-object p1, p0, Lcom/msc/sa/activity/f;->a:Lcom/msc/sa/activity/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 612
    return-void
.end method


# virtual methods
.method public final a(Lcom/msc/b/c;)V
    .locals 1

    .prologue
    .line 616
    iget-object v0, p0, Lcom/msc/sa/activity/f;->a:Lcom/msc/sa/activity/a;

    iget-object v0, v0, Lcom/msc/sa/activity/a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 617
    if-nez v0, :cond_0

    .line 621
    :cond_0
    return-void
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 5

    .prologue
    .line 630
    if-nez p1, :cond_1

    .line 675
    :cond_0
    :goto_0
    return-void

    .line 635
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v2

    .line 636
    invoke-virtual {p1}, Lcom/msc/b/c;->f()Ljava/lang/Exception;

    move-result-object v1

    .line 637
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v4

    .line 639
    iget-object v0, p0, Lcom/msc/sa/activity/f;->a:Lcom/msc/sa/activity/a;

    iget-object v0, v0, Lcom/msc/sa/activity/a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 641
    if-eqz v1, :cond_2

    .line 643
    iget-object v2, p0, Lcom/msc/sa/activity/f;->a:Lcom/msc/sa/activity/a;

    new-instance v3, Lcom/msc/c/f;

    invoke-direct {v3, v1}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v3, v2, Lcom/msc/sa/activity/a;->b:Lcom/msc/c/f;

    .line 645
    if-eqz v0, :cond_0

    .line 650
    iget-object v1, p0, Lcom/msc/sa/activity/f;->a:Lcom/msc/sa/activity/a;

    iget-object v1, v1, Lcom/msc/sa/activity/a;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 652
    iget-object v1, p0, Lcom/msc/sa/activity/f;->a:Lcom/msc/sa/activity/a;

    iget-object v1, v1, Lcom/msc/sa/activity/a;->b:Lcom/msc/c/f;

    const v2, 0x7f090027

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    .line 653
    iget-object v0, p0, Lcom/msc/sa/activity/f;->a:Lcom/msc/sa/activity/a;

    iget-object v0, v0, Lcom/msc/sa/activity/a;->b:Lcom/msc/c/f;

    const-string v1, "NPC"

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 660
    :cond_2
    if-eqz v4, :cond_0

    .line 662
    iget-object v1, p0, Lcom/msc/sa/activity/f;->a:Lcom/msc/sa/activity/a;

    invoke-static {v1, v2, v3, v4}, Lcom/msc/sa/activity/a;->a(Lcom/msc/sa/activity/a;JLjava/lang/String;)V

    .line 664
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "APAT"

    const-string v2, "Server request error occured"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    if-nez v0, :cond_0

    goto :goto_0
.end method

.method public final c(Lcom/msc/b/c;)V
    .locals 0

    .prologue
    .line 625
    return-void
.end method
