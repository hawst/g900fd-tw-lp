.class public Lcom/msc/sa/activity/RequestAccessTokenActivity;
.super Lcom/osp/app/util/BaseActivity;
.source "RequestAccessTokenActivity.java"


# instance fields
.field private a:Z

.field private b:Lcom/msc/sa/b/d;

.field private c:Lcom/msc/sa/activity/g;

.field private d:Lcom/msc/sa/activity/h;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a:Z

    .line 54
    iput-object v1, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->b:Lcom/msc/sa/b/d;

    .line 59
    iput-object v1, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->c:Lcom/msc/sa/activity/g;

    .line 64
    iput-object v1, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->d:Lcom/msc/sa/activity/h;

    .line 805
    return-void
.end method

.method static synthetic a(Lcom/msc/sa/activity/RequestAccessTokenActivity;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->f()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/msc/sa/activity/RequestAccessTokenActivity;Lcom/msc/sa/b/d;)Lcom/msc/sa/b/d;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->b:Lcom/msc/sa/b/d;

    return-object p1
.end method

.method private a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 303
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA"

    const-string v1, "startEmailCheckActivity()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    const-string v0, "com.msc.action.samsungaccount.emailvalidate"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 305
    const-string v0, "email_id"

    invoke-static {p0}, Lcom/msc/c/e;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 306
    const-string v0, "is_resend"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 307
    const-string v0, "key_verifypopup_mode"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 308
    const/16 v0, 0xc9

    invoke-virtual {p0, p1, v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 309
    return-void
.end method

.method static synthetic a(Lcom/msc/sa/activity/RequestAccessTokenActivity;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/msc/sa/activity/RequestAccessTokenActivity;Lcom/msc/sa/d/d;)V
    .locals 4

    .prologue
    .line 47
    const/4 v0, 0x0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.msc.action.samsungaccount.CHECKLIST_INFO_POPUP_WITH_NONE_SINGLE_TASK"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "check_list"

    invoke-virtual {p1}, Lcom/msc/sa/d/d;->e()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/msc/sa/d/d;->k()Lcom/msc/a/i;

    move-result-object v2

    invoke-virtual {v2}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->g()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/msc/sa/d/d;->k()Lcom/msc/a/i;

    move-result-object v2

    invoke-virtual {v2}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    const-string v2, "key_popup_require_name_field"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "key_popup_require_birth_date"

    invoke-virtual {p1}, Lcom/msc/sa/d/d;->k()Lcom/msc/a/i;

    move-result-object v2

    invoke-virtual {v2}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->c()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/16 v0, 0xe3

    invoke-virtual {p0, v1, v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic a(Lcom/msc/sa/activity/RequestAccessTokenActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/msc/sa/activity/RequestAccessTokenActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 386
    const/4 v0, 0x0

    .line 387
    iget-object v1, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->b:Lcom/msc/sa/b/d;

    if-eqz v1, :cond_0

    .line 389
    iget-object v0, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->b:Lcom/msc/sa/b/d;

    invoke-virtual {v0}, Lcom/msc/sa/b/d;->a()Landroid/content/Intent;

    move-result-object v0

    .line 391
    :cond_0
    if-eqz v0, :cond_1

    .line 393
    const-string v1, "error_code"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 394
    const-string v1, "error_message"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 396
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(ILandroid/content/Intent;)V

    .line 398
    invoke-virtual {p0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->finish()V

    .line 399
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 368
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA"

    const-string v1, "startResignining()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v3, :cond_0

    .line 371
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.SIGNIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 372
    const-string v1, "client_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 373
    const-string v1, "client_secret"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 374
    const-string v1, "OSP_VER"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 375
    const-string v1, "email_id"

    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 376
    const-string v1, "require re-SignIn"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 377
    const/16 v1, 0xd9

    invoke-virtual {p0, v0, v1}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 378
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA"

    const-string v1, "Show session expired Activity"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/msc/sa/activity/RequestAccessTokenActivity;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->d()V

    return-void
.end method

.method static synthetic b(Lcom/msc/sa/activity/RequestAccessTokenActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 505
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA"

    const-string v1, "setFailedResult()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 507
    const-string v1, "error_code"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 508
    const-string v1, "error_message"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 509
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(ILandroid/content/Intent;)V

    .line 510
    invoke-virtual {p0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->finish()V

    .line 511
    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 145
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA"

    const-string v1, "startMainProcess()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    invoke-virtual {p0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 149
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "client_id"

    aput-object v2, v1, v3

    const-string v2, "client_secret"

    aput-object v2, v1, v4

    .line 151
    invoke-virtual {p0, v1}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a([Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v4, :cond_0

    .line 154
    invoke-static {p0, v3}, Lcom/msc/c/c;->a(Landroid/content/Context;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 156
    const-string v1, "client_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "client_secret"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "OSP_02"

    invoke-direct {p0, v1, v0, v2}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    :cond_0
    :goto_0
    return-void

    .line 162
    :cond_1
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 164
    const/16 v0, 0xd5

    invoke-virtual {p0, v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->c(I)V

    goto :goto_0

    .line 168
    :cond_2
    invoke-direct {p0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->d()V

    goto :goto_0
.end method

.method static synthetic c(Lcom/msc/sa/activity/RequestAccessTokenActivity;)Z
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->p()Z

    move-result v0

    return v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 178
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA"

    const-string v1, "runAccessTokenTask()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->c:Lcom/msc/sa/activity/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->c:Lcom/msc/sa/activity/g;

    invoke-virtual {v0}, Lcom/msc/sa/activity/g;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 181
    iget-object v0, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->c:Lcom/msc/sa/activity/g;

    invoke-virtual {v0}, Lcom/msc/sa/activity/g;->d()V

    .line 183
    :cond_0
    new-instance v0, Lcom/msc/sa/activity/g;

    invoke-direct {v0, p0}, Lcom/msc/sa/activity/g;-><init>(Lcom/msc/sa/activity/RequestAccessTokenActivity;)V

    iput-object v0, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->c:Lcom/msc/sa/activity/g;

    .line 184
    iget-object v0, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->c:Lcom/msc/sa/activity/g;

    invoke-virtual {v0}, Lcom/msc/sa/activity/g;->b()V

    .line 185
    return-void
.end method

.method static synthetic d(Lcom/msc/sa/activity/RequestAccessTokenActivity;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 47
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "RATA"

    const-string v2, "registerSelfUpgradeManager()"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v1

    invoke-virtual {p0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getApplication()Landroid/app/Application;

    move-result-object v2

    invoke-virtual {p0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v0, v3}, Lcom/msc/sa/selfupdate/c;->a(Landroid/app/Application;ZZLjava/lang/String;)V

    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/sa/selfupdate/c;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Self upgrade is necessary after startSelfUpgrade"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/e;->a()Lcom/osp/app/util/e;

    move-result-object v0

    const-string v1, "The upgrade process must be completed if you use Samsung account"

    invoke-virtual {v0, p0, v1}, Lcom/osp/app/util/e;->a(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method private e()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 219
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA"

    const-string v1, "startActivityWithPriority()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    invoke-direct {p0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->f()Landroid/content/Intent;

    move-result-object v1

    .line 222
    const/4 v0, 0x0

    .line 223
    iget-object v2, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->b:Lcom/msc/sa/b/d;

    if-eqz v2, :cond_0

    .line 225
    iget-object v0, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->b:Lcom/msc/sa/b/d;

    invoke-virtual {v0, p0}, Lcom/msc/sa/b/d;->a(Landroid/content/Context;)I

    move-result v0

    .line 228
    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 252
    iput-boolean v3, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a:Z

    .line 253
    invoke-direct {p0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->c()V

    .line 256
    :goto_0
    return-void

    .line 231
    :pswitch_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA"

    const-string v2, "startTnCActivity()"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "com.msc.action.samsungaccount.Update_NewTerms"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "key_tnc_update_mode"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "country_code_mcc"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v0, 0xce

    invoke-virtual {p0, v1, v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 234
    :pswitch_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA"

    const-string v2, "startNameCheckActivity()"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "com.msc.action.samsungaccount.namevalidate"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "key_user_id"

    invoke-static {p0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v0, 0xca

    invoke-virtual {p0, v1, v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 237
    :pswitch_2
    invoke-static {p0}, Lcom/osp/app/util/r;->v(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 239
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA"

    const-string v1, "runSkipEmailValidationTask()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->d:Lcom/msc/sa/activity/h;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->d:Lcom/msc/sa/activity/h;

    invoke-virtual {v0}, Lcom/msc/sa/activity/h;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->d:Lcom/msc/sa/activity/h;

    invoke-virtual {v0}, Lcom/msc/sa/activity/h;->d()V

    :cond_1
    new-instance v0, Lcom/msc/sa/activity/h;

    invoke-direct {v0, p0, p0}, Lcom/msc/sa/activity/h;-><init>(Lcom/msc/sa/activity/RequestAccessTokenActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->d:Lcom/msc/sa/activity/h;

    iget-object v0, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->d:Lcom/msc/sa/activity/h;

    invoke-virtual {v0}, Lcom/msc/sa/activity/h;->b()V

    goto :goto_0

    .line 242
    :cond_2
    invoke-direct {p0, v1}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 246
    :pswitch_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA"

    const-string v2, "startMandatoryCheckActivity()"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "com.msc.action.samsungaccount.savemandatoryinfo"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "direct_modify"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "is_authorized_internal_action_for_accountinfo"

    const-string v2, "TIME_PATTERN"

    invoke-static {v2}, Lcom/osp/app/util/ad;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v0, 0xda

    invoke-virtual {p0, v1, v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 249
    :pswitch_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA"

    const-string v2, "startTnCActivity()"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountView"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "account_mode"

    const-string v2, "AGREE_TO_DISCLAIMER"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v0, 0xd3

    invoke-virtual {p0, v1, v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 228
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method static synthetic e(Lcom/msc/sa/activity/RequestAccessTokenActivity;)Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a:Z

    return v0
.end method

.method private f()Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 332
    invoke-virtual {p0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 333
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 334
    const-string v2, "client_id"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 335
    const-string v3, "client_secret"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 337
    const-string v3, "client_id"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 338
    const-string v2, "client_secret"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 339
    const-string v0, "check_list"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 340
    const-string v0, "key_no_notification"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 341
    return-object v1
.end method

.method static synthetic f(Lcom/msc/sa/activity/RequestAccessTokenActivity;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->e()V

    return-void
.end method

.method static synthetic g(Lcom/msc/sa/activity/RequestAccessTokenActivity;)Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a:Z

    return v0
.end method


# virtual methods
.method protected final a()V
    .locals 0

    .prologue
    .line 515
    return-void
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 519
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 414
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA"

    const-string v1, "onActivityResult()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    iget-object v0, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->c:Lcom/msc/sa/activity/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->c:Lcom/msc/sa/activity/g;

    invoke-virtual {v0}, Lcom/msc/sa/activity/g;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 417
    iget-object v0, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->c:Lcom/msc/sa/activity/g;

    invoke-virtual {v0}, Lcom/msc/sa/activity/g;->d()V

    .line 418
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->c:Lcom/msc/sa/activity/g;

    .line 421
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 440
    :goto_0
    return-void

    .line 424
    :sswitch_0
    const/16 v0, 0xd5

    if-ne p1, v0, :cond_1

    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 426
    invoke-direct {p0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->c()V

    goto :goto_0

    .line 429
    :cond_1
    const-string v0, "SAC_0301"

    const-string v1, "Network is not available"

    invoke-direct {p0, v0, v1}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 434
    :sswitch_1
    invoke-virtual {p0, p2, p3}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(ILandroid/content/Intent;)V

    .line 435
    invoke-virtual {p0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->finish()V

    goto :goto_0

    .line 439
    :sswitch_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA"

    const-string v1, "onResultOK()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    sparse-switch p1, :sswitch_data_1

    invoke-direct {p0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->e()V

    goto :goto_0

    :sswitch_3
    invoke-direct {p0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->c()V

    goto :goto_0

    :sswitch_4
    const-string v0, "SAC_0204"

    const-string v1, "The certification process must be completed before you use your Samsung account"

    invoke-direct {p0, v0, v1}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_5
    invoke-direct {p0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->e()V

    goto :goto_0

    :sswitch_6
    invoke-direct {p0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->c()V

    goto :goto_0

    .line 421
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_2
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0xa -> :sswitch_1
        0xc -> :sswitch_2
    .end sparse-switch

    .line 439
    :sswitch_data_1
    .sparse-switch
        0xc9 -> :sswitch_5
        0xca -> :sswitch_5
        0xce -> :sswitch_5
        0xd5 -> :sswitch_3
        0xd9 -> :sswitch_6
        0xda -> :sswitch_5
        0xe3 -> :sswitch_4
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 494
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA"

    const-string v1, "onConfigurationChanged()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 496
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 79
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->d(I)V

    .line 81
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 82
    invoke-virtual {p0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 84
    if-nez v1, :cond_0

    .line 86
    const-string v0, "SAC_0101"

    const-string v1, "Param [%s] must not be null"

    invoke-direct {p0, v0, v1}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :goto_0
    return-void

    .line 90
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA"

    const-string v2, "isAvailableActivity()"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    if-nez v0, :cond_2

    .line 92
    const-string v0, "SAC_0106"

    const-string v1, "Did not called from Activity"

    invoke-direct {p0, v0, v1}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 90
    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    .line 96
    :cond_2
    const-string v0, "progress_theme"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->d(Ljava/lang/String;)V

    .line 101
    :goto_2
    invoke-direct {p0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->c()V

    goto :goto_0

    .line 96
    :cond_3
    invoke-virtual {p0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->k()V

    goto :goto_2
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 482
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onDestroy()V

    .line 484
    iget-object v0, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->c:Lcom/msc/sa/activity/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->c:Lcom/msc/sa/activity/g;

    invoke-virtual {v0}, Lcom/msc/sa/activity/g;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 486
    iget-object v0, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->c:Lcom/msc/sa/activity/g;

    invoke-virtual {v0}, Lcom/msc/sa/activity/g;->d()V

    .line 487
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/msc/sa/activity/RequestAccessTokenActivity;->c:Lcom/msc/sa/activity/g;

    .line 490
    :cond_0
    return-void
.end method
