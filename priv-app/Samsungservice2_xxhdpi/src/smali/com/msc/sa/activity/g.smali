.class final Lcom/msc/sa/activity/g;
.super Lcom/msc/sa/activity/a;
.source "RequestAccessTokenActivity.java"


# instance fields
.field c:Lcom/msc/sa/d/d;

.field final synthetic d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/msc/sa/activity/RequestAccessTokenActivity;)V
    .locals 2

    .prologue
    .line 833
    iput-object p1, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    .line 834
    invoke-direct {p0, p1}, Lcom/msc/sa/activity/a;-><init>(Landroid/content/Context;)V

    .line 812
    new-instance v0, Lcom/msc/sa/d/d;

    invoke-direct {v0}, Lcom/msc/sa/d/d;-><init>()V

    iput-object v0, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    .line 814
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/msc/sa/activity/g;->e:Z

    .line 835
    invoke-virtual {p1}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "progress_theme"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 836
    if-eqz v0, :cond_0

    .line 838
    const-string v1, "invisible"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 840
    invoke-virtual {p0}, Lcom/msc/sa/activity/g;->c()V

    .line 843
    :cond_0
    return-void
.end method

.method private a([Ljava/lang/String;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 1181
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA_GetAccessTokenTask"

    const-string v1, "setAdditionalInfo"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1182
    if-eqz p1, :cond_f

    .line 1184
    array-length v4, p1

    move v2, v3

    :goto_0
    if-ge v2, v4, :cond_f

    aget-object v0, p1, v2

    .line 1186
    const-string v1, "user_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1188
    const-string v0, "user_id"

    iget-object v1, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v1}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1184
    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1189
    :cond_1
    const-string v1, "birthday"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1191
    const-string v0, "birthday"

    iget-object v1, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v1}, Lcom/msc/c/e;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 1192
    :cond_2
    const-string v1, "mcc"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1194
    const-string v0, "mcc"

    iget-object v1, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v1}, Lcom/msc/c/e;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 1195
    :cond_3
    const-string v1, "server_url"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1197
    const-string v0, "server_url"

    iget-object v1, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v1}, Lcom/msc/c/c;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 1198
    :cond_4
    const-string v1, "api_server_url"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1200
    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    const-string v1, "API_SERVER"

    invoke-static {v0, v1}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1201
    const-string v1, "api_server_url"

    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 1202
    :cond_5
    const-string v1, "auth_server_url"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1204
    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    const-string v1, "AUTH_SERVER"

    invoke-static {v0, v1}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1205
    const-string v1, "auth_server_url"

    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 1206
    :cond_6
    const-string v1, "cc"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1208
    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v0}, Lcom/osp/app/util/ad;->n(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1209
    const-string v1, "cc"

    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 1210
    :cond_7
    const-string v1, "device_physical_address_text"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1212
    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    const-string v1, "DEVICE_PHYSICAL_ADDRESS_TEXT"

    invoke-virtual {v0, v1, v3}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1213
    const-string v1, "DEVICE_PHYSICAL_ADDRESS_TEXT"

    const-string v5, ""

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1214
    const/4 v0, 0x0

    .line 1217
    if-eqz v1, :cond_8

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_8

    .line 1219
    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v5

    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/msc/c/a;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v1}, Lcom/msc/c/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1230
    :goto_2
    const-string v1, "device_physical_address_text"

    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1222
    :cond_8
    :try_start_1
    iget-object v1, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v1}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v1

    .line 1223
    invoke-virtual {v1}, Lcom/osp/device/a;->d()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_2

    .line 1225
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 1231
    :cond_9
    const-string v1, "access_token_expires_in"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1233
    const-string v0, "access_token_expires_in"

    iget-object v1, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v1}, Lcom/msc/sa/d/d;->g()J

    move-result-wide v6

    invoke-virtual {p2, v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1234
    :cond_a
    const-string v1, "refresh_token"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1236
    const-string v0, "refresh_token"

    iget-object v1, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v1}, Lcom/msc/sa/d/d;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1237
    :cond_b
    const-string v1, "refresh_token_expires_in"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1239
    const-string v0, "refresh_token_expires_in"

    iget-object v1, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v1}, Lcom/msc/sa/d/d;->i()J

    move-result-wide v6

    invoke-virtual {p2, v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1240
    :cond_c
    const-string v1, "login_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 1242
    const-string v0, "login_id"

    iget-object v1, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1243
    :cond_d
    const-string v1, "login_id_type"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1245
    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1247
    const-string v0, "login_id_type"

    const-string v1, "001"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1250
    :cond_e
    const-string v0, "login_id_type"

    const-string v1, "003"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1255
    :cond_f
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 1028
    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(ILandroid/content/Intent;)V

    .line 1029
    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->g(Lcom/msc/sa/activity/RequestAccessTokenActivity;)Z

    .line 1030
    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->finish()V

    .line 1031
    return-void
.end method

.method private i()Z
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 1313
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA_GetAccessTokenTask"

    const-string v1, "setAccessTokenFromDB"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1315
    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1316
    const-string v1, "client_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1317
    iget-object v1, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v1, v0}, Lcom/osp/app/util/ad;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 1319
    if-eqz v0, :cond_0

    .line 1321
    const-string v1, "key_accesstoken"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1322
    const-string v1, "key_accesstoken_expires_in"

    invoke-virtual {v0, v1, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    .line 1323
    const-string v1, "key_refreshtoken"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1324
    const-string v1, "key_refreshtoken_expires_in"

    invoke-virtual {v0, v1, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 1325
    const-string v1, "key_accesstoken_issued_time"

    invoke-virtual {v0, v1, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 1331
    iget-object v1, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual/range {v1 .. v9}, Lcom/msc/sa/d/d;->a(Ljava/lang/String;JLjava/lang/String;JJ)V

    .line 1332
    const/4 v0, 0x1

    .line 1335
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 13

    .prologue
    .line 847
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATAGetAccessTokenTask"

    const-string v1, "doInBackground"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->c(Lcom/msc/sa/activity/RequestAccessTokenActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 851
    const-string v0, "SAC_0205"

    const-string v1, "The signature of this application is not registered with the server."

    invoke-virtual {p0, v0, v1}, Lcom/msc/sa/activity/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 852
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 895
    :goto_0
    return-object v0

    .line 864
    :cond_0
    const/4 v10, 0x0

    .line 866
    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.msc.action.samsungaccount.REQUEST_CHECKLIST_VALIDATION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 868
    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->e(Lcom/msc/sa/activity/RequestAccessTokenActivity;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/msc/sa/activity/g;->h()V

    :cond_1
    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "client_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "client_secret"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    iget-object v3, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v3}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-static {v2, v1, v0, v3, v4}, Lcom/msc/sa/c/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/msc/sa/d/d;

    move-result-object v0

    iput-object v0, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    iget-object v0, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->k()Lcom/msc/a/i;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    new-instance v2, Lcom/msc/sa/b/d;

    iget-object v3, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v3}, Lcom/msc/sa/d/d;->k()Lcom/msc/a/i;

    move-result-object v3

    iget-object v4, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v3, v4, v1}, Lcom/msc/sa/b/c;->a(Lcom/msc/a/i;Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/msc/sa/b/d;-><init>(Ljava/util/ArrayList;Ljava/lang/String;)V

    invoke-static {v0, v2}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(Lcom/msc/sa/activity/RequestAccessTokenActivity;Lcom/msc/sa/b/d;)Lcom/msc/sa/b/d;

    :cond_2
    iget-object v0, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->a()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 871
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA_GetAccessTokenTask"

    const-string v1, "isAccessTokenReusable"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "client_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v1, "expired_access_token"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    sget-object v1, Lcom/osp/app/util/af;->c:Lcom/osp/app/util/af;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Lcom/osp/app/util/af;Z)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA_GetAccessTokenTask"

    const-string v1, "isAccessTokenExist"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "client_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v1, v0}, Lcom/osp/app/util/ad;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_6

    const-string v1, "key_accesstoken"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v1, "key_accesstoken_expires_in"

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v1, "key_refreshtoken"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v1, "key_refreshtoken_expires_in"

    const-wide/16 v6, 0x0

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    const-string v1, "key_accesstoken_issued_time"

    const-wide/16 v8, 0x0

    invoke-virtual {v0, v1, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    iget-object v1, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual/range {v1 .. v9}, Lcom/msc/sa/d/d;->a(Ljava/lang/String;JLjava/lang/String;JJ)V

    const/4 v0, 0x1

    :goto_1
    const/4 v1, 0x1

    if-ne v0, v1, :cond_8

    iget-object v0, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iget-object v2, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v2}, Lcom/msc/sa/d/d;->j()J

    move-result-wide v2

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "curTimeSec = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "accessTokenIssuedTime =  "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v5}, Lcom/msc/sa/d/d;->j()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "accessTokenIssuedTime/1000 =  "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v5}, Lcom/msc/sa/d/d;->j()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "AccessTokenExpiresIn = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v5}, Lcom/msc/sa/d/d;->g()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TimeGap = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/16 v6, 0x3e8

    div-long v6, v2, v6

    sub-long v6, v0, v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v4, "RATA"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "expiredAccessToken is Empty. Check Token issuedTime. TimeGap = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/16 v6, 0x3e8

    div-long v6, v2, v6

    sub-long v6, v0, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-eqz v4, :cond_4

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    sub-long/2addr v0, v2

    iget-object v2, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v2}, Lcom/msc/sa/d/d;->g()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_7

    :cond_4
    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v0, v11}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_2
    iput-boolean v0, p0, Lcom/msc/sa/activity/g;->e:Z

    .line 873
    iget-boolean v0, p0, Lcom/msc/sa/activity/g;->e:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_10

    .line 875
    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    iget-object v1, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v1}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "client_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/openprovider/b;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/osp/app/util/ad;->a(I)Z

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->e(Lcom/msc/sa/activity/RequestAccessTokenActivity;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_9

    :cond_5
    const/4 v0, 0x1

    .line 878
    :goto_3
    iget-boolean v1, p0, Lcom/msc/sa/activity/g;->e:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_a

    const/4 v1, 0x1

    if-ne v0, v1, :cond_a

    .line 880
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA_GetAccessTokenTask"

    const-string v1, "runTokenReuseProcess"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/msc/sa/activity/g;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    .line 871
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_7
    iget-object v0, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    goto :goto_2

    :cond_8
    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v0, v11}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_2

    .line 875
    :cond_9
    const/4 v0, 0x0

    goto :goto_3

    .line 882
    :cond_a
    iget-boolean v1, p0, Lcom/msc/sa/activity/g;->e:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_d

    if-nez v0, :cond_d

    .line 884
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA_GetAccessTokenTask"

    const-string v1, "runTokenReuseWithChecklistUpdateProcess"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "client_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "client_secret"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    iget-object v3, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v3}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-static {v2, v1, v0, v3, v4}, Lcom/msc/sa/c/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/msc/sa/d/d;

    move-result-object v0

    iput-object v0, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    iget-object v0, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->k()Lcom/msc/a/i;

    move-result-object v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    new-instance v2, Lcom/msc/sa/b/d;

    iget-object v3, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v3}, Lcom/msc/sa/d/d;->k()Lcom/msc/a/i;

    move-result-object v3

    iget-object v4, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v3, v4, v1}, Lcom/msc/sa/b/c;->a(Lcom/msc/a/i;Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/msc/sa/b/d;-><init>(Ljava/util/ArrayList;Ljava/lang/String;)V

    invoke-static {v0, v2}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(Lcom/msc/sa/activity/RequestAccessTokenActivity;Lcom/msc/sa/b/d;)Lcom/msc/sa/b/d;

    :cond_b
    iget-object v0, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->a()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x0

    :goto_4
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    :cond_c
    invoke-direct {p0}, Lcom/msc/sa/activity/g;->i()Z

    move-result v0

    goto :goto_4

    .line 889
    :cond_d
    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->d(Lcom/msc/sa/activity/RequestAccessTokenActivity;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 891
    const-string v0, "SAC_0203"

    const-string v1, "The upgrade process must be completed if you want to use Samsung account"

    invoke-virtual {p0, v0, v1}, Lcom/msc/sa/activity/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 892
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    .line 895
    :cond_e
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA_GetAccessTokenTask"

    const-string v1, "runAuthWithTncMandatoryProcess"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "client_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "client_secret"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    iget-object v3, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v3}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v1, v0, v3, v4}, Lcom/msc/sa/c/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/msc/sa/d/d;

    move-result-object v0

    iput-object v0, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    iget-object v0, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->k()Lcom/msc/a/i;

    move-result-object v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    new-instance v2, Lcom/msc/sa/b/d;

    iget-object v3, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v3}, Lcom/msc/sa/d/d;->k()Lcom/msc/a/i;

    move-result-object v3

    iget-object v4, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v3, v4, v1}, Lcom/msc/sa/b/c;->a(Lcom/msc/a/i;Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/msc/sa/b/d;-><init>(Ljava/util/ArrayList;Ljava/lang/String;)V

    invoke-static {v0, v2}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(Lcom/msc/sa/activity/RequestAccessTokenActivity;Lcom/msc/sa/b/d;)Lcom/msc/sa/b/d;

    :cond_f
    iget-object v0, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->a()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    :cond_10
    move v0, v10

    goto/16 :goto_3
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 1087
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA_GetAccessTokenTask"

    const-string v1, "onPostExecute"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1088
    invoke-super {p0, p1}, Lcom/msc/sa/activity/a;->a(Ljava/lang/Boolean;)V

    .line 1089
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v9, :cond_3

    .line 1091
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATAGetAccessTokenTask"

    const-string v1, "setNoneErrorResult"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.msc.action.samsungaccount.REQUEST_CHECKLIST_VALIDATION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/msc/sa/activity/g;->h()V

    .line 1095
    :goto_0
    return-void

    .line 1091
    :cond_0
    iget-object v0, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->f()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->h()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->g()J

    move-result-wide v3

    iget-object v0, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->i()J

    move-result-wide v6

    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "client_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v8, "additional"

    invoke-virtual {v0, v8}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/msc/sa/activity/g;->e:Z

    if-nez v0, :cond_1

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA"

    const-string v9, "Token cannot reusable. Save token"

    invoke-static {v0, v9}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;J)V

    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.msc.action.ACCESSTOKEN_V02_RESPONSE"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "access_token"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-direct {p0, v8, v0}, Lcom/msc/sa/activity/g;->a([Ljava/lang/String;Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v2}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "client_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->g(Lcom/msc/sa/activity/RequestAccessTokenActivity;)Z

    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->finish()V

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.msc.action.ACCESSTOKEN_V02_RESPONSE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "error_code"

    iget-object v3, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v3}, Lcom/msc/sa/d/d;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "error_message"

    iget-object v3, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v3}, Lcom/msc/sa/d/d;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v2}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "client_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v1, v9, v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->finish()V

    goto/16 :goto_0

    .line 1094
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATAGetAccessTokenTask"

    const-string v1, "setErrorResult"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SAC_0205"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    const-string v1, "SAC_0205"

    const-string v2, "The signature of this application is not registered with the server."

    invoke-static {v0, v1, v2}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(Lcom/msc/sa/activity/RequestAccessTokenActivity;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    const-string v0, "USR_3113"

    iget-object v1, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v1}, Lcom/msc/sa/d/d;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->l()I

    move-result v0

    const/16 v1, 0x10

    if-le v0, v1, :cond_5

    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v0}, Lcom/osp/app/util/ad;->i(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    iget-object v1, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v1}, Lcom/msc/sa/d/d;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v2}, Lcom/msc/sa/d/d;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(Lcom/msc/sa/activity/RequestAccessTokenActivity;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SAC_0402"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "client_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "client_secret"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    const-string v3, "OSP_02"

    invoke-static {v2, v1, v0, v3}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(Lcom/msc/sa/activity/RequestAccessTokenActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SAC_0204"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "validation_result_only"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.msc.action.samsungaccount.REQUEST_CHECKLIST_VALIDATION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    iget-object v1, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v1}, Lcom/msc/sa/d/d;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v2}, Lcom/msc/sa/d/d;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->b(Lcom/msc/sa/activity/RequestAccessTokenActivity;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    iget-object v1, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-static {v0, v1}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(Lcom/msc/sa/activity/RequestAccessTokenActivity;Lcom/msc/sa/d/d;)V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->f(Lcom/msc/sa/activity/RequestAccessTokenActivity;)V

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SAC_0301"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    const/16 v1, 0xd5

    invoke-virtual {v0, v1}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->c(I)V

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v1}, Lcom/msc/sa/d/d;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-static {v2, v0, v1}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(Lcom/msc/sa/activity/RequestAccessTokenActivity;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 824
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATA"

    const-string v1, "setErrorResult"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 825
    iget-object v0, p0, Lcom/msc/sa/activity/g;->c:Lcom/msc/sa/d/d;

    invoke-virtual {v0, p1, p2}, Lcom/msc/sa/d/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 827
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "errorMessage-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 828
    return-void
.end method

.method protected final d()V
    .locals 3

    .prologue
    .line 1288
    invoke-super {p0}, Lcom/msc/sa/activity/a;->d()V

    .line 1289
    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->a(ILandroid/content/Intent;)V

    .line 1290
    iget-object v0, p0, Lcom/msc/sa/activity/g;->d:Lcom/msc/sa/activity/RequestAccessTokenActivity;

    invoke-virtual {v0}, Lcom/msc/sa/activity/RequestAccessTokenActivity;->finish()V

    .line 1291
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 805
    invoke-virtual {p0}, Lcom/msc/sa/activity/g;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final onCancelled()V
    .locals 2

    .prologue
    .line 947
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATAGetAccessTokenTask"

    const-string v1, "onCancelled"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 948
    invoke-super {p0}, Lcom/msc/sa/activity/a;->onCancelled()V

    .line 950
    return-void
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 805
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/msc/sa/activity/g;->a(Ljava/lang/Boolean;)V

    return-void
.end method
