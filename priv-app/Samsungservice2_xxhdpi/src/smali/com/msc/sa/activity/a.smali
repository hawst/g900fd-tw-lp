.class public abstract Lcom/msc/sa/activity/a;
.super Landroid/os/AsyncTask;
.source "AbstractProcessAsyncTask.java"


# instance fields
.field protected a:Ljava/lang/ref/WeakReference;

.field protected b:Lcom/msc/c/f;

.field private c:Landroid/app/ProgressDialog;

.field private d:Ljava/lang/String;

.field private final e:Z

.field private f:Z

.field private g:J

.field private h:J

.field private i:J

.field private j:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/msc/sa/activity/a;-><init>(Landroid/content/Context;Z)V

    .line 103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IZ)V
    .locals 1

    .prologue
    .line 129
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/msc/sa/activity/a;->a:Ljava/lang/ref/WeakReference;

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/msc/sa/activity/a;->f:Z

    .line 131
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/msc/sa/activity/a;->a:Ljava/lang/ref/WeakReference;

    .line 132
    invoke-static {p1, p2}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/msc/sa/activity/a;->d:Ljava/lang/String;

    .line 133
    iput-boolean p3, p0, Lcom/msc/sa/activity/a;->e:Z

    .line 134
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 119
    const v0, 0x7f090036

    invoke-direct {p0, p1, v0, p2}, Lcom/msc/sa/activity/a;-><init>(Landroid/content/Context;IZ)V

    .line 120
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 457
    iget-object v0, p0, Lcom/msc/sa/activity/a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 459
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 461
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v0, 0x7f09002d

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lcom/msc/sa/activity/d;

    invoke-direct {v2, p0}, Lcom/msc/sa/activity/d;-><init>(Lcom/msc/sa/activity/a;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 477
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 479
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/msc/sa/activity/a;JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 34
    iget-object v0, p0, Lcom/msc/sa/activity/a;->j:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/msc/sa/activity/a;->j:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1}, Lcom/msc/c/f;-><init>()V

    iput-object v1, p0, Lcom/msc/sa/activity/a;->b:Lcom/msc/c/f;

    iget-object v1, p0, Lcom/msc/sa/activity/a;->b:Lcom/msc/c/f;

    invoke-virtual {v1, v0, p3}, Lcom/msc/c/f;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/msc/sa/activity/a;->b:Lcom/msc/c/f;

    :cond_0
    return-void
.end method


# virtual methods
.method protected varargs abstract a()Ljava/lang/Boolean;
.end method

.method protected final a(J)V
    .locals 1

    .prologue
    .line 541
    iput-wide p1, p0, Lcom/msc/sa/activity/a;->g:J

    .line 542
    return-void
.end method

.method protected final a(JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 569
    iget-object v0, p0, Lcom/msc/sa/activity/a;->j:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 571
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/msc/sa/activity/a;->j:Ljava/util/HashMap;

    .line 573
    :cond_0
    iget-object v0, p0, Lcom/msc/sa/activity/a;->j:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 574
    return-void
.end method

.method public final a(Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 598
    iput-object p1, p0, Lcom/msc/sa/activity/a;->c:Landroid/app/ProgressDialog;

    .line 599
    return-void
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 235
    invoke-virtual {p0}, Lcom/msc/sa/activity/a;->f()V

    .line 236
    return-void
.end method

.method protected final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 268
    iput-object p1, p0, Lcom/msc/sa/activity/a;->d:Ljava/lang/String;

    .line 269
    iget-object v0, p0, Lcom/msc/sa/activity/a;->c:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/msc/sa/activity/a;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/msc/sa/activity/a;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/msc/sa/activity/a;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/msc/sa/activity/a;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 273
    :cond_0
    return-void
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 489
    iget-object v0, p0, Lcom/msc/sa/activity/a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 491
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 493
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lcom/msc/sa/activity/e;

    invoke-direct {v2, p0}, Lcom/msc/sa/activity/e;-><init>(Lcom/msc/sa/activity/a;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 509
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 511
    :cond_0
    return-void
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 2

    .prologue
    .line 429
    iget-object v0, p0, Lcom/msc/sa/activity/a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 431
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 433
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    invoke-virtual {v0, v1, p3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 445
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 447
    :cond_0
    return-void
.end method

.method protected final a(Z)V
    .locals 6

    .prologue
    const v4, 0x7f09003e

    const/4 v5, 0x1

    .line 283
    iget-object v0, p0, Lcom/msc/sa/activity/a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 285
    iget-object v1, p0, Lcom/msc/sa/activity/a;->b:Lcom/msc/c/f;

    if-nez v1, :cond_0

    .line 287
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1}, Lcom/msc/c/f;-><init>()V

    iput-object v1, p0, Lcom/msc/sa/activity/a;->b:Lcom/msc/c/f;

    .line 290
    :cond_0
    if-nez v0, :cond_2

    .line 356
    :cond_1
    :goto_0
    return-void

    .line 295
    :cond_2
    const-string v1, "No peer certificate"

    iget-object v2, p0, Lcom/msc/sa/activity/a;->b:Lcom/msc/c/f;

    invoke-virtual {v2}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 297
    iget-object v1, p0, Lcom/msc/sa/activity/a;->b:Lcom/msc/c/f;

    const v2, 0x7f090027

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    .line 298
    iget-object v1, p0, Lcom/msc/sa/activity/a;->b:Lcom/msc/c/f;

    const-string v2, "NPC"

    invoke-virtual {v1, v2}, Lcom/msc/c/f;->a(Ljava/lang/String;)V

    .line 313
    :cond_3
    :goto_1
    const/4 v1, 0x0

    .line 314
    iget-object v2, p0, Lcom/msc/sa/activity/a;->b:Lcom/msc/c/f;

    invoke-virtual {v2}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v2

    .line 315
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    .line 317
    invoke-static {}, Lcom/osp/app/util/n;->a()Lcom/osp/app/util/n;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Lcom/osp/app/util/n;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 322
    :cond_4
    :goto_2
    const-string v3, "BIL_COMMON_ERROR"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 324
    const v0, 0x7f090087

    invoke-direct {p0, v0}, Lcom/msc/sa/activity/a;->a(I)V

    goto :goto_0

    .line 299
    :cond_5
    iget-object v1, p0, Lcom/msc/sa/activity/a;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 301
    iget-object v1, p0, Lcom/msc/sa/activity/a;->b:Lcom/msc/c/f;

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    .line 303
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "APAT"

    const-string v2, "network error!"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    instance-of v1, v0, Lcom/osp/app/util/BaseActivity;

    if-eqz v1, :cond_3

    .line 307
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "APAT"

    const-string v2, "start wifi settings!"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    iget-object v1, p0, Lcom/msc/sa/activity/a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/osp/app/util/BaseActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/osp/app/util/BaseActivity;->c(I)V

    goto :goto_1

    .line 318
    :cond_6
    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/msc/sa/activity/a;->b:Lcom/msc/c/f;

    invoke-virtual {v4}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 320
    iget-object v1, p0, Lcom/msc/sa/activity/a;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 331
    :cond_7
    const-string v3, "BIL_CREATE_ERROR"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 333
    const v0, 0x7f090085

    invoke-direct {p0, v0}, Lcom/msc/sa/activity/a;->a(I)V

    goto/16 :goto_0

    .line 340
    :cond_8
    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_9

    .line 343
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    invoke-static {v0, v1, v5}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 344
    if-eqz p1, :cond_1

    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_1

    .line 346
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 350
    :cond_9
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v1, 0x7f09004a

    invoke-static {v0, v1, v5}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 351
    if-eqz p1, :cond_1

    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_1

    .line 353
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 156
    invoke-static {p0}, Lcom/msc/sa/c/d;->a(Landroid/os/AsyncTask;)V

    .line 157
    return-void
.end method

.method protected final b(J)V
    .locals 1

    .prologue
    .line 550
    iput-wide p1, p0, Lcom/msc/sa/activity/a;->h:J

    .line 551
    return-void
.end method

.method protected b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 686
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "APAT"

    const-string v1, "setErrorResult"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0, p1, p2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/msc/sa/activity/a;->b:Lcom/msc/c/f;

    .line 689
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "errorMessage-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 690
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/msc/sa/activity/a;->f:Z

    .line 164
    return-void
.end method

.method protected final c(J)V
    .locals 1

    .prologue
    .line 559
    iput-wide p1, p0, Lcom/msc/sa/activity/a;->i:J

    .line 560
    return-void
.end method

.method protected d()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 207
    invoke-virtual {p0}, Lcom/msc/sa/activity/a;->f()V

    .line 208
    iget-wide v0, p0, Lcom/msc/sa/activity/a;->g:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    .line 210
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    iget-wide v2, p0, Lcom/msc/sa/activity/a;->g:J

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/i;->a(J)V

    .line 211
    iput-wide v4, p0, Lcom/msc/sa/activity/a;->g:J

    .line 213
    :cond_0
    iget-wide v0, p0, Lcom/msc/sa/activity/a;->h:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    .line 215
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    iget-wide v2, p0, Lcom/msc/sa/activity/a;->h:J

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/i;->a(J)V

    .line 216
    iput-wide v4, p0, Lcom/msc/sa/activity/a;->h:J

    .line 218
    :cond_1
    iget-wide v0, p0, Lcom/msc/sa/activity/a;->i:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_2

    .line 220
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    iget-wide v2, p0, Lcom/msc/sa/activity/a;->i:J

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/i;->a(J)V

    .line 221
    iput-wide v4, p0, Lcom/msc/sa/activity/a;->i:J

    .line 223
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/msc/sa/activity/a;->cancel(Z)Z

    .line 224
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/msc/sa/activity/a;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final e()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x3e8

    .line 246
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    .line 248
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 249
    new-instance v1, Lcom/msc/sa/activity/c;

    invoke-direct {v1, p0}, Lcom/msc/sa/activity/c;-><init>(Lcom/msc/sa/activity/a;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 260
    :goto_0
    return-void

    .line 258
    :cond_0
    invoke-virtual {p0}, Lcom/msc/sa/activity/a;->f()V

    goto :goto_0
.end method

.method public final f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 363
    iget-boolean v0, p0, Lcom/msc/sa/activity/a;->f:Z

    if-nez v0, :cond_1

    .line 366
    iget-object v0, p0, Lcom/msc/sa/activity/a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 367
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_1

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/msc/sa/activity/a;->c:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    .line 372
    :try_start_0
    iget-object v0, p0, Lcom/msc/sa/activity/a;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Lcom/msc/sa/activity/a;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381
    :cond_0
    iput-object v2, p0, Lcom/msc/sa/activity/a;->c:Landroid/app/ProgressDialog;

    .line 385
    :cond_1
    :goto_0
    return-void

    .line 376
    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 381
    iput-object v2, p0, Lcom/msc/sa/activity/a;->c:Landroid/app/ProgressDialog;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v2, p0, Lcom/msc/sa/activity/a;->c:Landroid/app/ProgressDialog;

    throw v0
.end method

.method public final g()Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 589
    iget-object v0, p0, Lcom/msc/sa/activity/a;->c:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method protected onCancelled()V
    .locals 0

    .prologue
    .line 228
    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 34
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/msc/sa/activity/a;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 169
    iget-boolean v0, p0, Lcom/msc/sa/activity/a;->f:Z

    if-nez v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/msc/sa/activity/a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 173
    if-nez v0, :cond_1

    .line 175
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "APAT"

    const-string v1, "Context Reference is null !!!"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 179
    :cond_1
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/msc/sa/activity/a;->c:Landroid/app/ProgressDialog;

    .line 180
    iget-object v1, p0, Lcom/msc/sa/activity/a;->c:Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/msc/sa/activity/a;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 181
    iget-object v1, p0, Lcom/msc/sa/activity/a;->c:Landroid/app/ProgressDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 182
    iget-object v1, p0, Lcom/msc/sa/activity/a;->c:Landroid/app/ProgressDialog;

    iget-boolean v2, p0, Lcom/msc/sa/activity/a;->e:Z

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 183
    iget-object v1, p0, Lcom/msc/sa/activity/a;->c:Landroid/app/ProgressDialog;

    new-instance v2, Lcom/msc/sa/activity/b;

    invoke-direct {v2, p0}, Lcom/msc/sa/activity/b;-><init>(Lcom/msc/sa/activity/a;)V

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 192
    if-eqz v0, :cond_0

    :try_start_0
    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/msc/sa/activity/a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/msc/sa/activity/a;->c:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/msc/sa/activity/a;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 196
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
