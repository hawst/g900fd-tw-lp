.class final Lcom/msc/sa/selfupdate/s;
.super Ljava/lang/Object;
.source "SelfUpgradePopup.java"

# interfaces
.implements Lcom/msc/sa/selfupdate/k;


# instance fields
.field final synthetic a:Lcom/msc/sa/selfupdate/q;


# direct methods
.method constructor <init>(Lcom/msc/sa/selfupdate/q;)V
    .locals 0

    .prologue
    .line 747
    iput-object p1, p0, Lcom/msc/sa/selfupdate/s;->a:Lcom/msc/sa/selfupdate/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 757
    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    invoke-static {}, Lcom/msc/sa/selfupdate/c;->c()V

    .line 759
    iget-object v0, p0, Lcom/msc/sa/selfupdate/s;->a:Lcom/msc/sa/selfupdate/q;

    iget-object v0, v0, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0}, Lcom/osp/app/signin/SamsungService;->n()Landroid/app/Activity;

    move-result-object v0

    .line 760
    if-eqz v0, :cond_0

    .line 762
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 763
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 766
    :cond_0
    iget-object v0, p0, Lcom/msc/sa/selfupdate/s;->a:Lcom/msc/sa/selfupdate/q;

    iget-object v0, v0, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->c()V

    .line 767
    iget-object v0, p0, Lcom/msc/sa/selfupdate/s;->a:Lcom/msc/sa/selfupdate/q;

    iget-object v0, v0, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-static {v0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->g(Lcom/msc/sa/selfupdate/SelfUpgradePopup;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/msc/sa/selfupdate/t;

    invoke-direct {v1, p0}, Lcom/msc/sa/selfupdate/t;-><init>(Lcom/msc/sa/selfupdate/s;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 775
    iget-object v0, p0, Lcom/msc/sa/selfupdate/s;->a:Lcom/msc/sa/selfupdate/q;

    iget-object v0, v0, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->finish()V

    .line 777
    iget-object v0, p0, Lcom/msc/sa/selfupdate/s;->a:Lcom/msc/sa/selfupdate/q;

    iget-object v0, v0, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    const-string v1, "SELF_UPGRADE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 778
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 779
    const-string v1, "SELF_UPGRADE_START_TIME"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 780
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 782
    invoke-static {}, Lcom/msc/sa/selfupdate/x;->a()Lcom/msc/sa/selfupdate/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/x;->c()V

    .line 783
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 751
    invoke-static {}, Lcom/msc/sa/selfupdate/x;->a()Lcom/msc/sa/selfupdate/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/x;->c()V

    .line 752
    return-void
.end method
