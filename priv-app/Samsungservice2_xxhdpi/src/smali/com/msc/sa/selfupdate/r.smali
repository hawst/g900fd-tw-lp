.class final Lcom/msc/sa/selfupdate/r;
.super Ljava/lang/Object;
.source "SelfUpgradePopup.java"

# interfaces
.implements Lcom/msc/b/h;


# instance fields
.field final synthetic a:Lcom/msc/sa/selfupdate/q;


# direct methods
.method constructor <init>(Lcom/msc/sa/selfupdate/q;)V
    .locals 0

    .prologue
    .line 596
    iput-object p1, p0, Lcom/msc/sa/selfupdate/r;->a:Lcom/msc/sa/selfupdate/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/msc/b/c;)V
    .locals 18

    .prologue
    .line 601
    if-nez p1, :cond_1

    .line 691
    :cond_0
    :goto_0
    return-void

    .line 606
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/msc/b/c;->d()Ljava/io/InputStream;

    move-result-object v6

    .line 607
    invoke-virtual/range {p1 .. p1}, Lcom/msc/b/c;->c()Ljava/util/HashMap;

    move-result-object v2

    .line 609
    if-nez v6, :cond_2

    .line 613
    :try_start_0
    new-instance v2, Ljava/lang/Exception;

    const-string v3, "Response content is null."

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 614
    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 620
    :cond_2
    const-string v3, "Content-Type"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 621
    const-string v3, "/sdcard/samsungaccount"

    .line 622
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 625
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_3

    .line 627
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/msc/sa/selfupdate/r;->a:Lcom/msc/sa/selfupdate/q;

    iget-object v3, v3, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-static {v3}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->d(Lcom/msc/sa/selfupdate/SelfUpgradePopup;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    .line 628
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 629
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 632
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/msc/sa/selfupdate/r;->a:Lcom/msc/sa/selfupdate/q;

    iget-object v3, v3, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/msc/sa/selfupdate/r;->a:Lcom/msc/sa/selfupdate/q;

    iget-object v5, v5, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-static {v5}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->d(Lcom/msc/sa/selfupdate/SelfUpgradePopup;)Landroid/content/Context;

    move-result-object v5

    const-string v7, "SamsungService.apk"

    invoke-virtual {v5, v7}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v4, v3, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->a:Ljava/io/File;

    .line 634
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Content-Type is "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 636
    const-string v3, "application/octet-stream"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 638
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "SLFUPOP"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Content-Type is wrong  \t\t"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/msc/sa/selfupdate/r;->a:Lcom/msc/sa/selfupdate/q;

    iget-object v2, v2, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    iget-object v2, v2, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 641
    :cond_4
    new-instance v2, Ljava/io/BufferedInputStream;

    const/16 v3, 0x800

    invoke-direct {v2, v6, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 646
    const/16 v3, 0x800

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 647
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v7

    .line 649
    const/4 v3, 0x0

    .line 652
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/msc/sa/selfupdate/r;->a:Lcom/msc/sa/selfupdate/q;

    iget-object v4, v4, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-static {v4}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->d(Lcom/msc/sa/selfupdate/SelfUpgradePopup;)Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/msc/sa/selfupdate/r;->a:Lcom/msc/sa/selfupdate/q;

    iget-object v5, v5, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    iget-object v5, v5, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->a:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x1

    invoke-virtual {v4, v5, v8}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v3

    .line 654
    const-wide/16 v4, 0x0

    .line 655
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/msc/sa/selfupdate/r;->a:Lcom/msc/sa/selfupdate/q;

    iget-object v8, v8, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-static {v8}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->e(Lcom/msc/sa/selfupdate/SelfUpgradePopup;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 656
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v9, "SLFUPOP"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Decoded size : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 658
    :goto_1
    const/4 v9, 0x0

    const/16 v10, 0x800

    invoke-virtual {v2, v7, v9, v10}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v9

    const/4 v10, -0x1

    if-eq v9, v10, :cond_6

    .line 660
    int-to-long v10, v9

    add-long/2addr v4, v10

    .line 661
    const/4 v10, 0x0

    invoke-virtual {v3, v7, v10, v9}, Ljava/io/FileOutputStream;->write([BII)V

    .line 662
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V

    .line 663
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/msc/sa/selfupdate/r;->a:Lcom/msc/sa/selfupdate/q;

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-wide/16 v14, 0x64

    mul-long/2addr v14, v4

    int-to-long v0, v8

    move-wide/from16 v16, v0

    div-long v14, v14, v16

    long-to-int v13, v14

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Lcom/msc/sa/selfupdate/q;->a(Lcom/msc/sa/selfupdate/q;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 669
    :catch_1
    move-exception v2

    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 679
    if-eqz v6, :cond_5

    .line 681
    :try_start_3
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 683
    :cond_5
    if-eqz v3, :cond_0

    .line 685
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    .line 687
    :catch_2
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 665
    :cond_6
    long-to-int v2, v4

    :try_start_4
    div-int/2addr v2, v8

    const/4 v4, 0x1

    if-ne v2, v4, :cond_7

    .line 667
    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/msc/sa/selfupdate/r;->a:Lcom/msc/sa/selfupdate/q;

    iget-object v4, v4, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-static {v4}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->d(Lcom/msc/sa/selfupdate/SelfUpgradePopup;)Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/msc/sa/selfupdate/r;->a:Lcom/msc/sa/selfupdate/q;

    iget-object v5, v5, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    iget-object v5, v5, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->a:Ljava/io/File;

    invoke-virtual {v2, v4, v5}, Lcom/msc/sa/selfupdate/c;->a(Landroid/content/Context;Ljava/io/File;)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 679
    :cond_7
    if-eqz v6, :cond_8

    .line 681
    :try_start_5
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 683
    :cond_8
    if-eqz v3, :cond_0

    .line 685
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto/16 :goto_0

    .line 687
    :catch_3
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 672
    :catch_4
    move-exception v2

    :try_start_6
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 679
    if-eqz v6, :cond_9

    .line 681
    :try_start_7
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 683
    :cond_9
    if-eqz v3, :cond_0

    .line 685
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    goto/16 :goto_0

    .line 687
    :catch_5
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 677
    :catchall_0
    move-exception v2

    .line 679
    if-eqz v6, :cond_a

    .line 681
    :try_start_8
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 683
    :cond_a
    if-eqz v3, :cond_b

    .line 685
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    .line 690
    :cond_b
    :goto_2
    throw v2

    .line 687
    :catch_6
    move-exception v3

    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 0

    .prologue
    .line 698
    return-void
.end method

.method public final c(Lcom/msc/b/c;)V
    .locals 0

    .prologue
    .line 703
    return-void
.end method
