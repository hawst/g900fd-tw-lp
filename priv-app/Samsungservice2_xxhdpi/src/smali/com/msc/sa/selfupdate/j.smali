.class public final enum Lcom/msc/sa/selfupdate/j;
.super Ljava/lang/Enum;
.source "SelfUpgradeManager.java"


# static fields
.field public static final enum a:Lcom/msc/sa/selfupdate/j;

.field public static final enum b:Lcom/msc/sa/selfupdate/j;

.field public static final enum c:Lcom/msc/sa/selfupdate/j;

.field public static final enum d:Lcom/msc/sa/selfupdate/j;

.field private static final synthetic e:[Lcom/msc/sa/selfupdate/j;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 139
    new-instance v0, Lcom/msc/sa/selfupdate/j;

    const-string v1, "PUT"

    invoke-direct {v0, v1, v2}, Lcom/msc/sa/selfupdate/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/msc/sa/selfupdate/j;->a:Lcom/msc/sa/selfupdate/j;

    new-instance v0, Lcom/msc/sa/selfupdate/j;

    const-string v1, "GET"

    invoke-direct {v0, v1, v3}, Lcom/msc/sa/selfupdate/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/msc/sa/selfupdate/j;->b:Lcom/msc/sa/selfupdate/j;

    new-instance v0, Lcom/msc/sa/selfupdate/j;

    const-string v1, "INITIALIZE"

    invoke-direct {v0, v1, v4}, Lcom/msc/sa/selfupdate/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/msc/sa/selfupdate/j;->c:Lcom/msc/sa/selfupdate/j;

    new-instance v0, Lcom/msc/sa/selfupdate/j;

    const-string v1, "REMOVE"

    invoke-direct {v0, v1, v5}, Lcom/msc/sa/selfupdate/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/msc/sa/selfupdate/j;->d:Lcom/msc/sa/selfupdate/j;

    .line 137
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/msc/sa/selfupdate/j;

    sget-object v1, Lcom/msc/sa/selfupdate/j;->a:Lcom/msc/sa/selfupdate/j;

    aput-object v1, v0, v2

    sget-object v1, Lcom/msc/sa/selfupdate/j;->b:Lcom/msc/sa/selfupdate/j;

    aput-object v1, v0, v3

    sget-object v1, Lcom/msc/sa/selfupdate/j;->c:Lcom/msc/sa/selfupdate/j;

    aput-object v1, v0, v4

    sget-object v1, Lcom/msc/sa/selfupdate/j;->d:Lcom/msc/sa/selfupdate/j;

    aput-object v1, v0, v5

    sput-object v0, Lcom/msc/sa/selfupdate/j;->e:[Lcom/msc/sa/selfupdate/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 137
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/msc/sa/selfupdate/j;
    .locals 1

    .prologue
    .line 137
    const-class v0, Lcom/msc/sa/selfupdate/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/msc/sa/selfupdate/j;

    return-object v0
.end method

.method public static values()[Lcom/msc/sa/selfupdate/j;
    .locals 1

    .prologue
    .line 137
    sget-object v0, Lcom/msc/sa/selfupdate/j;->e:[Lcom/msc/sa/selfupdate/j;

    invoke-virtual {v0}, [Lcom/msc/sa/selfupdate/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/msc/sa/selfupdate/j;

    return-object v0
.end method
