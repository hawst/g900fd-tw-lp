.class public final Lcom/msc/sa/selfupdate/n;
.super Ljava/lang/Object;
.source "SelfUpgradeManager.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2456
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/sa/selfupdate/n;->a:Ljava/lang/String;

    .line 2457
    iput-object v1, p0, Lcom/msc/sa/selfupdate/n;->c:Ljava/lang/String;

    .line 2458
    iput-object v1, p0, Lcom/msc/sa/selfupdate/n;->d:Ljava/lang/String;

    .line 2459
    const/4 v0, -0x1

    iput v0, p0, Lcom/msc/sa/selfupdate/n;->b:I

    .line 2460
    return-void
.end method

.method static synthetic a(Lcom/msc/sa/selfupdate/n;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2427
    iget-object v0, p0, Lcom/msc/sa/selfupdate/n;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/msc/sa/selfupdate/n;I)V
    .locals 0

    .prologue
    .line 2427
    iput p1, p0, Lcom/msc/sa/selfupdate/n;->b:I

    return-void
.end method

.method static synthetic a(Lcom/msc/sa/selfupdate/n;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2427
    iput-object p1, p0, Lcom/msc/sa/selfupdate/n;->a:Ljava/lang/String;

    return-void
.end method

.method static synthetic b(Lcom/msc/sa/selfupdate/n;)I
    .locals 1

    .prologue
    .line 2427
    iget v0, p0, Lcom/msc/sa/selfupdate/n;->b:I

    return v0
.end method

.method static synthetic b(Lcom/msc/sa/selfupdate/n;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2427
    iput-object p1, p0, Lcom/msc/sa/selfupdate/n;->c:Ljava/lang/String;

    return-void
.end method

.method static synthetic c(Lcom/msc/sa/selfupdate/n;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2427
    iput-object p1, p0, Lcom/msc/sa/selfupdate/n;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2531
    iget-object v0, p0, Lcom/msc/sa/selfupdate/n;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Z)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2563
    :try_start_0
    const-string v2, "1"

    iget-object v3, p0, Lcom/msc/sa/selfupdate/n;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 2564
    if-eqz p1, :cond_2

    .line 2566
    iget-object v3, p0, Lcom/msc/sa/selfupdate/n;->c:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    move v3, v0

    .line 2567
    :goto_0
    iget-object v4, p0, Lcom/msc/sa/selfupdate/n;->d:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 2568
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v5, "SLFUCHKMGR"

    const-string v6, "*************************"

    invoke-static {v5, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2569
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v5, "SLFUCHKMGR"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "NeedUpgrade : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2570
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v5, "SLFUCHKMGR"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "HasDownloadURI : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2571
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v5, "SLFUCHKMGR"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "ContentSize : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2572
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v5, "SLFUCHKMGR"

    const-string v6, "*************************"

    invoke-static {v5, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2573
    if-eqz v2, :cond_1

    if-eqz v3, :cond_1

    if-lez v4, :cond_1

    .line 2583
    :goto_1
    return v0

    :cond_0
    move v3, v1

    .line 2566
    goto :goto_0

    :cond_1
    move v0, v1

    .line 2573
    goto :goto_1

    .line 2576
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v3, "*************************"

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2577
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "NeedUpgrade : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2578
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v3, "*************************"

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v2

    .line 2579
    goto :goto_1

    .line 2583
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2540
    iget-object v0, p0, Lcom/msc/sa/selfupdate/n;->d:Ljava/lang/String;

    return-object v0
.end method
