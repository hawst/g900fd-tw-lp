.class public Lcom/msc/sa/selfupdate/SelfUpgradePopup;
.super Lcom/osp/app/util/BaseActivity;
.source "SelfUpgradePopup.java"


# instance fields
.field a:Ljava/io/File;

.field private b:Lcom/msc/sa/selfupdate/v;

.field private c:Lcom/msc/sa/selfupdate/q;

.field private d:Landroid/app/ProgressDialog;

.field private e:Landroid/content/Context;

.field private f:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private final z:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->a:Ljava/io/File;

    .line 121
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->z:Landroid/os/Handler;

    .line 552
    return-void
.end method

.method static synthetic a(Lcom/msc/sa/selfupdate/SelfUpgradePopup;Lcom/msc/sa/selfupdate/q;)Lcom/msc/sa/selfupdate/q;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->c:Lcom/msc/sa/selfupdate/q;

    return-object p1
.end method

.method static synthetic a(Lcom/msc/sa/selfupdate/SelfUpgradePopup;)Lcom/msc/sa/selfupdate/v;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->b:Lcom/msc/sa/selfupdate/v;

    return-object v0
.end method

.method static synthetic a(Lcom/msc/sa/selfupdate/SelfUpgradePopup;Lcom/msc/sa/selfupdate/v;)Lcom/msc/sa/selfupdate/v;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->b:Lcom/msc/sa/selfupdate/v;

    return-object p1
.end method

.method static synthetic a(Lcom/msc/sa/selfupdate/SelfUpgradePopup;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->f:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/msc/sa/selfupdate/SelfUpgradePopup;)Lcom/msc/sa/selfupdate/q;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->c:Lcom/msc/sa/selfupdate/q;

    return-object v0
.end method

.method static synthetic b(Lcom/msc/sa/selfupdate/SelfUpgradePopup;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->y:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/msc/sa/selfupdate/SelfUpgradePopup;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/msc/sa/selfupdate/SelfUpgradePopup;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->e:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic e(Lcom/msc/sa/selfupdate/SelfUpgradePopup;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->y:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/msc/sa/selfupdate/SelfUpgradePopup;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->d:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic g(Lcom/msc/sa/selfupdate/SelfUpgradePopup;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->z:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 0

    .prologue
    .line 902
    return-void
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 908
    return-void
.end method

.method protected final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 458
    :try_start_0
    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->d:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 465
    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 472
    :cond_0
    iput-object v1, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->d:Landroid/app/ProgressDialog;

    .line 473
    :goto_0
    return-void

    .line 467
    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 472
    iput-object v1, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->d:Landroid/app/ProgressDialog;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v1, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->d:Landroid/app/ProgressDialog;

    throw v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 126
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUPOP"

    const-string v1, "onCreate called"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->d(I)V

    .line 129
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 130
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->requestWindowFeature(I)Z

    .line 131
    invoke-virtual {p0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 132
    const v0, 0x7f03005b

    invoke-virtual {p0, v0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->setContentView(I)V

    .line 133
    iput-object p0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->e:Landroid/content/Context;

    .line 134
    invoke-virtual {p0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 135
    const-string v1, "URI"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->f:Ljava/lang/String;

    .line 136
    const-string v1, "SIZE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->y:Ljava/lang/String;

    .line 138
    const-string v1, "UPGRADE_WITH_NOTIFICATION"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 139
    const-string v2, "key_internal_is_auto_update_popup"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 183
    if-eqz v1, :cond_3

    .line 185
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 189
    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->b:Lcom/msc/sa/selfupdate/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->b:Lcom/msc/sa/selfupdate/v;

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/v;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_1

    .line 191
    :cond_0
    new-instance v0, Lcom/msc/sa/selfupdate/v;

    invoke-direct {v0, p0, p0}, Lcom/msc/sa/selfupdate/v;-><init>(Lcom/msc/sa/selfupdate/SelfUpgradePopup;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->b:Lcom/msc/sa/selfupdate/v;

    .line 192
    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->b:Lcom/msc/sa/selfupdate/v;

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/os/AsyncTask;)V

    .line 211
    :cond_1
    :goto_0
    return-void

    .line 204
    :cond_2
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f09003e

    invoke-virtual {p0, v0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v3}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 205
    invoke-virtual {p0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->finish()V

    goto :goto_0

    .line 209
    :cond_3
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f090146

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090042

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090019

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    const v1, 0x7f090051

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    const v1, 0x7f020039

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setIcon(I)V

    :try_start_0
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    new-instance v2, Lcom/msc/sa/selfupdate/o;

    invoke-direct {v2, p0, v0}, Lcom/msc/sa/selfupdate/o;-><init>(Lcom/msc/sa/selfupdate/SelfUpgradePopup;Landroid/app/AlertDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    new-instance v2, Lcom/msc/sa/selfupdate/p;

    invoke-direct {v2, p0, v0}, Lcom/msc/sa/selfupdate/p;-><init>(Lcom/msc/sa/selfupdate/SelfUpgradePopup;Landroid/app/AlertDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 422
    packed-switch p1, :pswitch_data_0

    .line 448
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 425
    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->e:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->d:Landroid/app/ProgressDialog;

    .line 426
    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->d:Landroid/app/ProgressDialog;

    const v1, 0x7f090115

    invoke-virtual {p0, v1}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 433
    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->d:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->e:Landroid/content/Context;

    const v2, 0x7f090021

    invoke-static {v1, v2}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 435
    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->d:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 436
    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 437
    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 440
    :try_start_0
    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 445
    :goto_1
    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->d:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 441
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 422
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 879
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onDestroy()V

    .line 881
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUPOP"

    const-string v1, "onDestroy called"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 883
    invoke-static {}, Lcom/osp/app/util/e;->a()Lcom/osp/app/util/e;

    move-result-object v0

    iget-object v1, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->e:Landroid/content/Context;

    const-string v2, "The upgrade process must be completed if you use Samsung account"

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/util/e;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 885
    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->b:Lcom/msc/sa/selfupdate/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->b:Lcom/msc/sa/selfupdate/v;

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/v;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 887
    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->b:Lcom/msc/sa/selfupdate/v;

    invoke-virtual {v0, v3}, Lcom/msc/sa/selfupdate/v;->cancel(Z)Z

    .line 888
    iput-object v4, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->b:Lcom/msc/sa/selfupdate/v;

    .line 891
    :cond_0
    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->c:Lcom/msc/sa/selfupdate/q;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->c:Lcom/msc/sa/selfupdate/q;

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/q;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_1

    .line 893
    iget-object v0, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->c:Lcom/msc/sa/selfupdate/q;

    invoke-virtual {v0, v3}, Lcom/msc/sa/selfupdate/q;->cancel(Z)Z

    .line 894
    iput-object v4, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->c:Lcom/msc/sa/selfupdate/q;

    .line 896
    :cond_1
    invoke-static {}, Lcom/msc/sa/selfupdate/x;->a()Lcom/msc/sa/selfupdate/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/x;->c()V

    .line 897
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 218
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUPOP"

    const-string v1, "onNewIntent called"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    invoke-static {}, Lcom/osp/app/util/e;->a()Lcom/osp/app/util/e;

    move-result-object v0

    iget-object v1, p0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->e:Landroid/content/Context;

    const-string v2, "The upgrade process must be completed if you use Samsung account"

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/util/e;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 223
    return-void
.end method
