.class public final Lcom/msc/sa/selfupdate/c;
.super Ljava/lang/Object;
.source "SelfUpgradeManager.java"


# static fields
.field private static h:Lcom/msc/sa/selfupdate/k;


# instance fields
.field public final a:I

.field b:Lcom/msc/b/d;

.field c:Lcom/msc/b/d;

.field private d:J

.field private e:Lcom/msc/sa/selfupdate/i;

.field private f:Z

.field private g:Z

.field private i:Lcom/osp/app/signin/hq;

.field private j:Z

.field private k:I

.field private l:Lcom/msc/sa/selfupdate/n;

.field private m:Z

.field private n:J

.field private o:Ljava/lang/Thread;

.field private p:Ljava/io/File;

.field private q:Z

.field private final r:Ljava/lang/Runnable;

.field private s:Ljava/lang/Thread;

.field private final t:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x0

    sput-object v0, Lcom/msc/sa/selfupdate/c;->h:Lcom/msc/sa/selfupdate/k;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 419
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    const/4 v0, 0x2

    iput v0, p0, Lcom/msc/sa/selfupdate/c;->a:I

    .line 113
    sget-object v0, Lcom/msc/sa/selfupdate/i;->b:Lcom/msc/sa/selfupdate/i;

    iput-object v0, p0, Lcom/msc/sa/selfupdate/c;->e:Lcom/msc/sa/selfupdate/i;

    .line 118
    iput-boolean v3, p0, Lcom/msc/sa/selfupdate/c;->f:Z

    .line 123
    iput-boolean v3, p0, Lcom/msc/sa/selfupdate/c;->g:Z

    .line 130
    iput-object v2, p0, Lcom/msc/sa/selfupdate/c;->i:Lcom/osp/app/signin/hq;

    .line 322
    iput-boolean v3, p0, Lcom/msc/sa/selfupdate/c;->j:Z

    .line 332
    new-instance v0, Lcom/msc/sa/selfupdate/n;

    invoke-direct {v0}, Lcom/msc/sa/selfupdate/n;-><init>()V

    iput-object v0, p0, Lcom/msc/sa/selfupdate/c;->l:Lcom/msc/sa/selfupdate/n;

    .line 337
    iput-boolean v3, p0, Lcom/msc/sa/selfupdate/c;->m:Z

    .line 342
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/msc/sa/selfupdate/c;->n:J

    .line 347
    iput-object v2, p0, Lcom/msc/sa/selfupdate/c;->b:Lcom/msc/b/d;

    .line 352
    iput-object v2, p0, Lcom/msc/sa/selfupdate/c;->c:Lcom/msc/b/d;

    .line 357
    iput-object v2, p0, Lcom/msc/sa/selfupdate/c;->o:Ljava/lang/Thread;

    .line 362
    iput-object v2, p0, Lcom/msc/sa/selfupdate/c;->p:Ljava/io/File;

    .line 372
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/msc/sa/selfupdate/c;->q:Z

    .line 377
    new-instance v0, Lcom/msc/sa/selfupdate/d;

    invoke-direct {v0, p0}, Lcom/msc/sa/selfupdate/d;-><init>(Lcom/msc/sa/selfupdate/c;)V

    iput-object v0, p0, Lcom/msc/sa/selfupdate/c;->r:Ljava/lang/Runnable;

    .line 391
    iput-object v2, p0, Lcom/msc/sa/selfupdate/c;->s:Ljava/lang/Thread;

    .line 396
    new-instance v0, Lcom/msc/sa/selfupdate/f;

    invoke-direct {v0, p0}, Lcom/msc/sa/selfupdate/f;-><init>(Lcom/msc/sa/selfupdate/c;)V

    iput-object v0, p0, Lcom/msc/sa/selfupdate/c;->t:Ljava/lang/Runnable;

    .line 420
    iput v3, p0, Lcom/msc/sa/selfupdate/c;->k:I

    .line 421
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "constructor called"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/msc/sa/selfupdate/c;-><init>()V

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;Lcom/msc/sa/selfupdate/j;I)I
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 2711
    const-class v2, Lcom/msc/sa/selfupdate/c;

    monitor-enter v2

    :try_start_0
    const-string v3, "SELF_UPGRADE"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 2712
    sget-object v4, Lcom/msc/sa/selfupdate/j;->a:Lcom/msc/sa/selfupdate/j;

    if-ne p1, v4, :cond_0

    .line 2715
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "AUTO_UPDATE_TYPE"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2716
    invoke-static {}, Lcom/msc/sa/selfupdate/m;->a()Lcom/msc/sa/selfupdate/c;

    invoke-static {p0}, Lcom/msc/sa/selfupdate/c;->b(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2740
    :goto_0
    monitor-exit v2

    return p2

    .line 2718
    :cond_0
    :try_start_1
    sget-object v4, Lcom/msc/sa/selfupdate/j;->c:Lcom/msc/sa/selfupdate/j;

    if-ne p1, v4, :cond_2

    .line 2720
    const-string v0, "AUTO_UPDATE_TYPE"

    const/4 v4, -0x1

    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p2

    .line 2722
    if-gez p2, :cond_1

    .line 2725
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "AUTO_UPDATE_TYPE"

    const/4 v4, 0x1

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move p2, v1

    .line 2727
    :cond_1
    invoke-static {}, Lcom/msc/sa/selfupdate/m;->a()Lcom/msc/sa/selfupdate/c;

    invoke-static {p0}, Lcom/msc/sa/selfupdate/c;->b(Landroid/content/Context;)V

    .line 2728
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Auto Update Type: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2711
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 2731
    :cond_2
    :try_start_2
    sget-object v1, Lcom/msc/sa/selfupdate/j;->d:Lcom/msc/sa/selfupdate/j;

    if-ne p1, v1, :cond_3

    .line 2733
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "AUTO_UPDATE_TYPE"

    invoke-interface {v1, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2734
    invoke-static {}, Lcom/msc/sa/selfupdate/m;->a()Lcom/msc/sa/selfupdate/c;

    invoke-static {p0}, Lcom/msc/sa/selfupdate/c;->b(Landroid/content/Context;)V

    move p2, v0

    .line 2735
    goto :goto_0

    .line 2738
    :cond_3
    const-string v0, "AUTO_UPDATE_TYPE"

    invoke-interface {v3, v0, p2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p2

    .line 2739
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "updateType: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/util/ArrayList;)I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1623
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "getUpdateVersionIndex START"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1624
    const/4 v3, -0x1

    .line 1627
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    .line 1629
    :goto_0
    if-ge v2, v4, :cond_1

    .line 1631
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/msc/sa/selfupdate/n;

    invoke-static {v0}, Lcom/msc/sa/selfupdate/n;->b(Lcom/msc/sa/selfupdate/n;)I

    move-result v0

    .line 1633
    if-le v0, v1, :cond_2

    .line 1641
    invoke-static {p0}, Lcom/osp/app/util/ad;->e(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v1, v2

    .line 1629
    :goto_1
    add-int/lit8 v2, v2, 0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    .line 1647
    :cond_0
    const v5, 0x30d40

    if-le v5, v0, :cond_2

    move v1, v2

    .line 1650
    goto :goto_1

    .line 1656
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "getUpdateVersionIndex END"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1657
    return v3

    :cond_2
    move v0, v1

    move v1, v3

    goto :goto_1
.end method

.method static synthetic a(Lcom/msc/sa/selfupdate/c;I)I
    .locals 0

    .prologue
    .line 78
    iput p1, p0, Lcom/msc/sa/selfupdate/c;->k:I

    return p1
.end method

.method private static a(Lcom/osp/app/signin/SamsungService;)I
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2310
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v3, "getCurrentNetworkType START"

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2315
    if-eqz p0, :cond_3

    .line 2317
    :try_start_0
    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SamsungService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 2319
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v3

    if-ne v3, v1, :cond_1

    .line 2321
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "WIFI enabled"

    invoke-static {v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2322
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 2323
    if-eqz v0, :cond_2

    .line 2325
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSupplicantState()Landroid/net/wifi/SupplicantState;

    move-result-object v0

    .line 2326
    sget-object v3, Landroid/net/wifi/SupplicantState;->COMPLETED:Landroid/net/wifi/SupplicantState;

    if-ne v0, v3, :cond_0

    .line 2328
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WIFI MODE"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2348
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "getCurrentNetworkType result : "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2349
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "getCurrentNetworkType END"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2350
    return v2

    .line 2332
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MOBILE MODE"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    move v0, v1

    :goto_1
    move v2, v0

    .line 2336
    goto :goto_0

    .line 2338
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WIFI disenabled"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    move v2, v1

    .line 2346
    goto :goto_0

    .line 2343
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method static synthetic a(Lcom/msc/sa/selfupdate/c;)J
    .locals 2

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/msc/sa/selfupdate/c;->n:J

    return-wide v0
.end method

.method static synthetic a(Lcom/msc/sa/selfupdate/c;Lcom/osp/app/signin/hq;)Lcom/osp/app/signin/hq;
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/msc/sa/selfupdate/c;->i:Lcom/osp/app/signin/hq;

    return-object p1
.end method

.method public static a(Lcom/msc/sa/selfupdate/k;)V
    .locals 0

    .prologue
    .line 219
    sput-object p0, Lcom/msc/sa/selfupdate/c;->h:Lcom/msc/sa/selfupdate/k;

    .line 220
    return-void
.end method

.method private declared-synchronized a(Lcom/osp/app/signin/SamsungService;ZZZLcom/osp/app/signin/hq;)V
    .locals 14

    .prologue
    .line 535
    monitor-enter p0

    const/4 v2, 0x0

    :try_start_0
    iput-object v2, p0, Lcom/msc/sa/selfupdate/c;->o:Ljava/lang/Thread;

    .line 536
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/msc/sa/selfupdate/c;->s:Ljava/lang/Thread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 537
    const/4 v11, 0x0

    .line 538
    const/4 v10, 0x0

    .line 539
    const/4 v3, 0x0

    .line 540
    const/4 v9, 0x0

    .line 541
    const/4 v8, 0x0

    .line 542
    const/4 v7, 0x0

    .line 543
    const/4 v6, 0x0

    .line 544
    const/4 v5, 0x0

    .line 546
    const/4 v4, 0x0

    .line 549
    :try_start_1
    invoke-virtual {p1}, Lcom/osp/app/signin/SamsungService;->getPackageName()Ljava/lang/String;

    move-result-object v11

    .line 550
    invoke-static {p1}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    .line 551
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p1}, Lcom/osp/common/util/i;->k(Landroid/content/Context;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 552
    :try_start_2
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/device/b;->c()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v9

    .line 553
    :try_start_3
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/device/b;->e()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v8

    .line 554
    :try_start_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/osp/app/util/ad;->f(Landroid/content/Context;)I

    move-result v12

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v7

    .line 555
    :try_start_5
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/device/b;->l()I

    move-result v6

    .line 556
    const-string v4, ""
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 558
    :try_start_6
    invoke-static {p1}, Lcom/osp/app/util/ad;->e(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 560
    const-string v5, "SAC20"
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 565
    :goto_0
    if-nez v10, :cond_0

    .line 567
    :try_start_7
    const-string v10, "999"

    .line 569
    :cond_0
    if-nez v2, :cond_1

    .line 571
    const-string v2, "00"
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_1
    move-object v3, v2

    move-object v2, v8

    move-object v8, v7

    move v7, v6

    move-object v6, v5

    move-object v5, v4

    move-object v4, v10

    .line 579
    :goto_1
    :try_start_8
    invoke-static {}, Lcom/osp/app/util/ag;->a()Lcom/osp/app/util/ag;

    invoke-static {}, Lcom/osp/app/util/ag;->b()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-static {}, Lcom/osp/app/util/ag;->a()Lcom/osp/app/util/ag;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/osp/app/util/ag;->a(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 581
    invoke-static {}, Lcom/osp/app/util/ag;->a()Lcom/osp/app/util/ag;

    move-result-object v2

    iget-object v4, v2, Lcom/osp/app/util/ag;->b:Ljava/lang/String;

    .line 582
    invoke-static {}, Lcom/osp/app/util/ag;->a()Lcom/osp/app/util/ag;

    move-result-object v2

    iget-object v3, v2, Lcom/osp/app/util/ag;->c:Ljava/lang/String;

    .line 583
    const-string v2, "TST-PG-WVGA"

    .line 585
    :cond_2
    const-string v10, "SAMSUNG-"

    invoke-virtual {v2, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 587
    const-string v10, "SAMSUNG-"

    const-string v12, ""

    invoke-virtual {v2, v10, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 590
    :cond_3
    iget v10, p0, Lcom/msc/sa/selfupdate/c;->k:I

    const/4 v12, 0x2

    if-ne v10, v12, :cond_4

    .line 592
    iget-boolean v10, p0, Lcom/msc/sa/selfupdate/c;->j:Z

    if-eqz v10, :cond_a

    .line 594
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v10, "SLFUCHKMGR"

    const-string v12, "FINISH - update is necessary"

    invoke-static {v10, v12}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v10, "SLFUCHKMGR"

    const-string v12, "FINISH - requestSelfUpgradeURL : nested request finish"

    invoke-static {v10, v12}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    invoke-virtual/range {p0 .. p2}, Lcom/msc/sa/selfupdate/c;->a(Lcom/osp/app/signin/SamsungService;Z)V

    .line 605
    :cond_4
    :goto_2
    const/4 v10, 0x1

    iput v10, p0, Lcom/msc/sa/selfupdate/c;->k:I

    .line 607
    move-object/from16 v0, p5

    invoke-virtual {v0, v11}, Lcom/osp/app/signin/hq;->k(Ljava/lang/String;)V

    .line 608
    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/hq;->m(Ljava/lang/String;)V

    .line 609
    move-object/from16 v0, p5

    invoke-virtual {v0, v8}, Lcom/osp/app/signin/hq;->l(Ljava/lang/String;)V

    .line 610
    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Lcom/osp/app/signin/hq;->n(Ljava/lang/String;)V

    .line 611
    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Lcom/osp/app/signin/hq;->o(Ljava/lang/String;)V

    .line 612
    move-object/from16 v0, p5

    invoke-virtual {v0, v9}, Lcom/osp/app/signin/hq;->p(Ljava/lang/String;)V

    .line 613
    move-object/from16 v0, p5

    invoke-virtual {v0, v7}, Lcom/osp/app/signin/hq;->a(I)V

    .line 614
    move-object/from16 v0, p5

    invoke-virtual {v0, v5}, Lcom/osp/app/signin/hq;->q(Ljava/lang/String;)V

    .line 615
    move-object/from16 v0, p5

    invoke-virtual {v0, v6}, Lcom/osp/app/signin/hq;->c(Ljava/lang/String;)V

    .line 617
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 618
    new-instance v2, Lcom/msc/sa/selfupdate/g;

    move-object v3, p0

    move-object v4, p1

    move/from16 v5, p2

    move/from16 v6, p4

    move/from16 v7, p3

    move-object/from16 v8, p5

    invoke-direct/range {v2 .. v8}, Lcom/msc/sa/selfupdate/g;-><init>(Lcom/msc/sa/selfupdate/c;Lcom/osp/app/signin/SamsungService;ZZZLcom/osp/app/signin/hq;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "SRS"

    const-string v4, "prepareStubUpdatecheck"

    invoke-static {v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/msc/c/k;->f()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v3

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v5

    invoke-virtual {v5}, Lcom/osp/app/util/ah;->g()Z

    move-result v7

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v5

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v6

    move-object v8, v2

    invoke-virtual/range {v3 .. v8}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v2

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->c()Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v4

    invoke-virtual {v4}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    const-string v3, "appId"

    invoke-virtual/range {p5 .. p5}, Lcom/osp/app/signin/hq;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "deviceId"

    invoke-virtual/range {p5 .. p5}, Lcom/osp/app/signin/hq;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "versionCode"

    invoke-virtual/range {p5 .. p5}, Lcom/osp/app/signin/hq;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "mcc"

    invoke-virtual/range {p5 .. p5}, Lcom/osp/app/signin/hq;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "mnc"

    invoke-virtual/range {p5 .. p5}, Lcom/osp/app/signin/hq;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "csc"

    invoke-virtual/range {p5 .. p5}, Lcom/osp/app/signin/hq;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "sdkVer"

    invoke-virtual/range {p5 .. p5}, Lcom/osp/app/signin/hq;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "signID"

    invoke-virtual/range {p5 .. p5}, Lcom/osp/app/signin/hq;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "pd"

    invoke-virtual/range {p5 .. p5}, Lcom/osp/app/signin/hq;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/msc/sa/selfupdate/c;->b:Lcom/msc/b/d;

    .line 683
    iget-object v2, p0, Lcom/msc/sa/selfupdate/c;->b:Lcom/msc/b/d;

    invoke-virtual {v2}, Lcom/msc/b/d;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/msc/sa/selfupdate/c;->n:J
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 688
    :try_start_9
    iget-object v2, p0, Lcom/msc/sa/selfupdate/c;->o:Ljava/lang/Thread;

    if-nez v2, :cond_6

    .line 690
    new-instance v2, Ljava/lang/Thread;

    iget-object v3, p0, Lcom/msc/sa/selfupdate/c;->r:Ljava/lang/Runnable;

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v2, p0, Lcom/msc/sa/selfupdate/c;->o:Ljava/lang/Thread;

    .line 691
    iget-object v2, p0, Lcom/msc/sa/selfupdate/c;->o:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 693
    :cond_6
    iget-object v2, p0, Lcom/msc/sa/selfupdate/c;->s:Ljava/lang/Thread;

    if-nez v2, :cond_7

    .line 695
    new-instance v2, Ljava/lang/Thread;

    iget-object v3, p0, Lcom/msc/sa/selfupdate/c;->t:Ljava/lang/Runnable;

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v2, p0, Lcom/msc/sa/selfupdate/c;->s:Ljava/lang/Thread;

    .line 696
    iget-object v2, p0, Lcom/msc/sa/selfupdate/c;->s:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 699
    :cond_7
    iget-object v2, p0, Lcom/msc/sa/selfupdate/c;->o:Ljava/lang/Thread;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/msc/sa/selfupdate/c;->o:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->isAlive()Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    move/from16 v0, p3

    if-ne v0, v2, :cond_8

    .line 701
    iget-object v2, p0, Lcom/msc/sa/selfupdate/c;->o:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->join()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 708
    :cond_8
    :goto_3
    monitor-exit p0

    return-void

    .line 563
    :cond_9
    :try_start_a
    const-string v5, ""
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_6
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    .line 574
    :catch_0
    move-exception v2

    move-object v13, v4

    move-object v4, v5

    move v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v3

    move-object v3, v13

    :goto_4
    :try_start_b
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    move-object v2, v7

    move v7, v5

    move-object v5, v3

    move-object v3, v9

    move-object v9, v8

    move-object v8, v6

    move-object v6, v4

    move-object v4, v10

    goto/16 :goto_1

    .line 599
    :cond_a
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v10, "SLFUCHKMGR"

    const-string v12, "FINISH - update is unnecessary"

    invoke-static {v10, v12}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v10, "SLFUCHKMGR"

    const-string v12, "FINISH - requestSelfUpgradeURL : nested request finish"

    invoke-static {v10, v12}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_2

    .line 535
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 704
    :catch_1
    move-exception v2

    :try_start_c
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto :goto_3

    .line 574
    :catch_2
    move-exception v3

    move-object v13, v3

    move-object v3, v4

    move-object v4, v5

    move v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v2

    move-object v2, v13

    goto :goto_4

    :catch_3
    move-exception v3

    move-object v13, v3

    move-object v3, v4

    move-object v4, v5

    move v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v2

    move-object v2, v13

    goto :goto_4

    :catch_4
    move-exception v3

    move-object v13, v3

    move-object v3, v4

    move-object v4, v5

    move v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v2

    move-object v2, v13

    goto :goto_4

    :catch_5
    move-exception v3

    move-object v13, v3

    move-object v3, v4

    move-object v4, v5

    move v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v2

    move-object v2, v13

    goto :goto_4

    :catch_6
    move-exception v3

    move-object v13, v3

    move-object v3, v4

    move-object v4, v5

    move v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v2

    move-object v2, v13

    goto :goto_4

    :catch_7
    move-exception v3

    move-object v13, v3

    move-object v3, v4

    move-object v4, v5

    move v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v2

    move-object v2, v13

    goto :goto_4
.end method

.method static synthetic b(Lcom/msc/sa/selfupdate/c;)I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/msc/sa/selfupdate/c;->k:I

    return v0
.end method

.method public static b()Lcom/msc/sa/selfupdate/c;
    .locals 1

    .prologue
    .line 430
    invoke-static {}, Lcom/msc/sa/selfupdate/m;->a()Lcom/msc/sa/selfupdate/c;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/osp/app/signin/SamsungService;Z)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1668
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SLFUCHKMGR"

    const-string v2, "requestSelfUpgradeEx START"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1672
    :try_start_0
    new-instance v5, Lcom/msc/c/q;

    invoke-direct {v5}, Lcom/msc/c/q;-><init>()V

    .line 1674
    new-instance v6, Ljava/io/StringWriter;

    invoke-direct {v6}, Ljava/io/StringWriter;-><init>()V

    .line 1676
    invoke-virtual {v5, v6}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 1684
    const-string v1, "UTF-8"

    invoke-virtual {v5, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 1685
    const-string v1, ""

    const-string v2, "SamsungProtocol"

    invoke-virtual {v5, v1, v2}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1687
    const-string v1, ""

    const-string v2, "version"

    const-string v3, "1.0a"

    invoke-virtual {v5, v1, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1689
    const-string v2, ""

    const-string v3, "lang"

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "EN"

    :cond_0
    invoke-virtual {v5, v2, v3, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1691
    invoke-static {}, Lcom/osp/app/util/ag;->a()Lcom/osp/app/util/ag;

    invoke-static {}, Lcom/osp/app/util/ag;->b()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/osp/app/util/ag;->a()Lcom/osp/app/util/ag;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/app/util/ag;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1693
    const-string v3, ""

    const-string v2, "deviceModel"

    const-string v1, "TST-PG-WVGA"

    move-object v4, v5

    .line 1696
    :goto_0
    invoke-virtual {v4, v3, v2, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1699
    const-string v1, ""

    const-string v2, "networkType"

    invoke-static {p0}, Lcom/msc/sa/selfupdate/c;->a(Lcom/osp/app/signin/SamsungService;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v1, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1701
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->j(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 1702
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->k(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1704
    if-nez v2, :cond_1

    .line 1706
    const-string v2, "999"

    .line 1708
    :cond_1
    if-nez v1, :cond_2

    .line 1710
    const-string v1, "00"

    .line 1712
    :cond_2
    invoke-static {}, Lcom/osp/app/util/ag;->a()Lcom/osp/app/util/ag;

    invoke-static {}, Lcom/osp/app/util/ag;->b()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {}, Lcom/osp/app/util/ag;->a()Lcom/osp/app/util/ag;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/osp/app/util/ag;->a(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1714
    invoke-static {}, Lcom/osp/app/util/ag;->a()Lcom/osp/app/util/ag;

    move-result-object v1

    iget-object v2, v1, Lcom/osp/app/util/ag;->b:Ljava/lang/String;

    .line 1715
    invoke-static {}, Lcom/osp/app/util/ag;->a()Lcom/osp/app/util/ag;

    move-result-object v1

    iget-object v1, v1, Lcom/osp/app/util/ag;->c:Ljava/lang/String;

    .line 1718
    :cond_3
    const-string v3, ""

    const-string v4, "mcc"

    invoke-virtual {v5, v3, v4, v2}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1719
    const-string v2, ""

    const-string v3, "mnc"

    invoke-virtual {v5, v2, v3, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1720
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {}, Lcom/osp/common/util/i;->b()Ljava/lang/String;

    move-result-object v1

    .line 1728
    if-eqz p0, :cond_6

    invoke-static {p0}, Lcom/osp/app/util/r;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1730
    const-string v1, ""

    const-string v2, "csc"

    const-string v3, "secu"

    invoke-virtual {v5, v1, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1736
    :goto_1
    const-string v1, ""

    const-string v2, "openApiVersion"

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v1, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1738
    const-string v1, ""

    const-string v2, "request"

    invoke-virtual {v5, v1, v2}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1741
    const-string v1, ""

    const-string v2, "id"

    const-string v3, "2346"

    invoke-virtual {v5, v1, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1742
    const-string v1, ""

    const-string v2, "name"

    const-string v3, "updateCheck"

    invoke-virtual {v5, v1, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1743
    const-string v1, ""

    const-string v2, "numParam"

    const-string v3, "2"

    invoke-virtual {v5, v1, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1744
    const-string v1, ""

    const-string v2, "transactionId"

    const-string v3, "15"

    invoke-virtual {v5, v1, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1745
    const-string v1, ""

    const-string v2, "param"

    invoke-virtual {v5, v1, v2}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1746
    const/4 v1, 0x0

    const-string v2, "name"

    const-string v3, "imei"

    invoke-virtual {v5, v1, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1748
    if-eqz p1, :cond_8

    .line 1750
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p0, :cond_7

    invoke-static {p0}, Lcom/osp/device/d;->c(Landroid/content/Context;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_2
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 1758
    :goto_3
    const-string v1, ""

    const-string v2, "param"

    invoke-virtual {v5, v1, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1759
    const-string v1, ""

    const-string v2, "param"

    invoke-virtual {v5, v1, v2}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1760
    const/4 v1, 0x0

    const-string v2, "name"

    const-string v3, "applist"

    invoke-virtual {v5, v1, v2, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1763
    if-eqz p1, :cond_9

    .line 1765
    const-string v1, "@1"

    .line 1770
    :goto_4
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "com.osp.app.signin@"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/osp/device/b;->j(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 1772
    const-string v1, ""

    const-string v2, "param"

    invoke-virtual {v5, v1, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1773
    const-string v1, ""

    const-string v2, "request"

    invoke-virtual {v5, v1, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1774
    const-string v1, ""

    const-string v2, "SamsungProtocol"

    invoke-virtual {v5, v1, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1775
    invoke-virtual {v5}, Lcom/msc/c/q;->a()V

    .line 1776
    invoke-virtual {v6}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1781
    :goto_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SLFUCHKMGR xml : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1782
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SLFUCHKMGR"

    const-string v2, "requestSelfUpgradeEx END"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1783
    return-object v0

    .line 1696
    :cond_4
    :try_start_1
    const-string v3, ""

    const-string v2, "deviceModel"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->e()Ljava/lang/String;

    move-result-object v1

    move-object v4, v5

    goto/16 :goto_0

    :cond_5
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "SAMSUNG-"

    const-string v7, ""

    invoke-virtual {v1, v4, v7}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v4, v5

    goto/16 :goto_0

    .line 1733
    :cond_6
    const-string v2, ""

    const-string v3, "csc"

    invoke-virtual {v5, v2, v3, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 1777
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 1750
    :cond_7
    :try_start_2
    const-string v1, "0"

    goto/16 :goto_2

    .line 1754
    :cond_8
    const-string v1, "0"

    invoke-virtual {v5, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1768
    :cond_9
    const-string v1, "@0"
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_4
.end method

.method public static b(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2659
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "setAutoCheckForUpdate called"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2661
    const-string v0, "SELF_UPGRADE"

    invoke-virtual {p0, v0, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2662
    const-string v1, "AUTO_UPDATE_TYPE"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 2676
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/msc/sa/selfupdate/UpdateCheckService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2678
    const/16 v2, 0x1ae

    invoke-static {p0, v2, v1, v6}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 2680
    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 2682
    :cond_0
    const-wide/32 v0, 0x2932e00

    .line 2684
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->n()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2686
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/32 v4, 0xea60

    mul-long/2addr v0, v4

    .line 2689
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    add-long/2addr v4, v0

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v1, v3, :cond_2

    invoke-virtual {v0, v6, v4, v5, v2}, Landroid/app/AlarmManager;->setExact(IJLandroid/app/PendingIntent;)V

    .line 2691
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "setAutoCheck"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2699
    :goto_1
    return-void

    .line 2689
    :cond_2
    invoke-virtual {v0, v6, v4, v5, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0

    .line 2694
    :cond_3
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 2695
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 2696
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "cancelAutoCheck"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;Ljava/io/File;)V
    .locals 9

    .prologue
    .line 2595
    invoke-virtual {p1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 2604
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "SLFUCHKMGR File path : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2609
    :try_start_0
    new-instance v0, Lcom/msc/sa/selfupdate/l;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/msc/sa/selfupdate/l;-><init>(B)V

    .line 2617
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 2618
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "SLFUCHKMGR"

    const-string v4, "Silent Install request START"

    invoke-static {v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2619
    const-string v3, "android.content.pm.IPackageInstallObserver"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 2622
    :try_start_1
    const-class v4, Landroid/content/pm/PackageManager;

    const-string v5, "installPackage"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/net/Uri;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object v3, v6, v7

    const/4 v3, 0x2

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v6, v3

    const/4 v3, 0x3

    const-class v7, Ljava/lang/String;

    aput-object v7, v6, v3

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    const/4 v0, 0x2

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x3

    const/4 v5, 0x0

    aput-object v5, v4, v0

    invoke-virtual {v3, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 2624
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v3, "START SILENT INSTALL METHOD 1(Using Observer)"

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2631
    :goto_0
    :try_start_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v2, "Silent Install request END"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2646
    :goto_1
    return-void

    .line 2627
    :catch_0
    move-exception v0

    const-class v0, Landroid/content/pm/PackageManager;

    const-string v3, "installPackage"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/net/Uri;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-class v6, Landroid/content/pm/IPackageInstallObserver;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-class v6, Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const/4 v5, 0x0

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 2629
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v2, "START SILENT INSTALL METHOD 2(Not using Observer)"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 2632
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2635
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v2, "START SILENT INSTALL METHOD 3(using ACTION_VIEW)"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2636
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2637
    const-string v2, "application/vnd.android.package-archive"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 2638
    const/high16 v1, 0x10a00000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2639
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method static synthetic c(Lcom/msc/sa/selfupdate/c;)Ljava/lang/Thread;
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/msc/sa/selfupdate/c;->o:Ljava/lang/Thread;

    return-object v0
.end method

.method public static c()V
    .locals 1

    .prologue
    .line 2258
    new-instance v0, Lcom/msc/sa/selfupdate/c;

    invoke-direct {v0}, Lcom/msc/sa/selfupdate/c;-><init>()V

    invoke-static {v0}, Lcom/msc/sa/selfupdate/m;->a(Lcom/msc/sa/selfupdate/c;)Lcom/msc/sa/selfupdate/c;

    .line 2259
    return-void
.end method

.method static synthetic d(Lcom/msc/sa/selfupdate/c;)Ljava/lang/Thread;
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/msc/sa/selfupdate/c;->s:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic e(Lcom/msc/sa/selfupdate/c;)Lcom/osp/app/signin/hq;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/msc/sa/selfupdate/c;->i:Lcom/osp/app/signin/hq;

    return-object v0
.end method

.method static synthetic f(Lcom/msc/sa/selfupdate/c;)Z
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/msc/sa/selfupdate/c;->j:Z

    return v0
.end method

.method static synthetic g()Lcom/msc/sa/selfupdate/k;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lcom/msc/sa/selfupdate/c;->h:Lcom/msc/sa/selfupdate/k;

    return-object v0
.end method

.method static synthetic g(Lcom/msc/sa/selfupdate/c;)Lcom/msc/sa/selfupdate/n;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/msc/sa/selfupdate/c;->l:Lcom/msc/sa/selfupdate/n;

    return-object v0
.end method

.method static synthetic h()Lcom/msc/sa/selfupdate/k;
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    sput-object v0, Lcom/msc/sa/selfupdate/c;->h:Lcom/msc/sa/selfupdate/k;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/osp/app/signin/SamsungService;Lcom/msc/b/h;)Lcom/msc/sa/selfupdate/n;
    .locals 18

    .prologue
    .line 2813
    const/4 v13, 0x0

    .line 2814
    const/4 v12, 0x0

    .line 2815
    const/4 v11, 0x0

    .line 2816
    const/4 v10, 0x0

    .line 2817
    const/4 v9, 0x0

    .line 2818
    const/4 v8, 0x0

    .line 2819
    const/4 v7, 0x0

    .line 2820
    const/4 v6, 0x0

    .line 2821
    const/4 v14, 0x0

    .line 2822
    const/4 v5, 0x0

    .line 2825
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/osp/app/signin/SamsungService;->getPackageName()Ljava/lang/String;

    move-result-object v13

    .line 2826
    invoke-static/range {p1 .. p1}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    .line 2827
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static/range {p1 .. p1}, Lcom/osp/common/util/i;->k(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    .line 2828
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/osp/device/b;->c()Ljava/lang/String;

    move-result-object v10

    .line 2829
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v9

    .line 2830
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {p1 .. p1}, Lcom/osp/app/util/ad;->f(Landroid/content/Context;)I

    move-result v15

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 2831
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/osp/device/b;->l()I

    move-result v7

    .line 2832
    const-string v4, ""
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2834
    :try_start_1
    invoke-static/range {p1 .. p1}, Lcom/osp/app/util/ad;->e(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2836
    const-string v6, "SAC20"

    .line 2842
    :goto_0
    if-nez v12, :cond_0

    .line 2844
    const-string v12, "999"

    .line 2846
    :cond_0
    if-nez v11, :cond_1

    .line 2848
    const-string v11, "00"

    .line 2851
    :cond_1
    invoke-static/range {p1 .. p1}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v5

    .line 2853
    if-eqz v5, :cond_2

    .line 2855
    invoke-virtual {v5}, Lcom/osp/device/a;->d()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v14

    .line 2860
    :cond_2
    :try_start_2
    const-string v5, "MD5"

    invoke-static {v5}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v15

    .line 2861
    const-string v5, "UTF-8"

    invoke-virtual {v14, v5}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v5

    invoke-virtual {v15, v5}, Ljava/security/MessageDigest;->update([B)V

    .line 2862
    new-instance v5, Ljava/lang/String;

    invoke-static {}, Lcom/osp/common/b/b;->a()Lcom/osp/common/b/b;

    move-result-object v16

    invoke-virtual {v15}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v15

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lcom/osp/common/b/b;->a([B)[B

    move-result-object v15

    const-string v16, "UTF-8"

    move-object/from16 v0, v16

    invoke-direct {v5, v15, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    .line 2870
    :goto_1
    invoke-static {}, Lcom/osp/app/util/ag;->a()Lcom/osp/app/util/ag;

    invoke-static {}, Lcom/osp/app/util/ag;->b()Z

    move-result v14

    if-eqz v14, :cond_3

    invoke-static {}, Lcom/osp/app/util/ag;->a()Lcom/osp/app/util/ag;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Lcom/osp/app/util/ag;->a(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 2872
    invoke-static {}, Lcom/osp/app/util/ag;->a()Lcom/osp/app/util/ag;

    move-result-object v9

    iget-object v12, v9, Lcom/osp/app/util/ag;->b:Ljava/lang/String;

    .line 2873
    invoke-static {}, Lcom/osp/app/util/ag;->a()Lcom/osp/app/util/ag;

    move-result-object v9

    iget-object v11, v9, Lcom/osp/app/util/ag;->c:Ljava/lang/String;

    .line 2874
    const-string v9, "TST-PG-WVGA"

    .line 2876
    :cond_3
    const-string v14, "SAMSUNG-"

    invoke-virtual {v9, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 2878
    const-string v14, "SAMSUNG-"

    const-string v15, ""

    invoke-virtual {v9, v14, v15}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    move-object v14, v9

    .line 2883
    :goto_2
    :try_start_3
    invoke-static/range {p1 .. p1}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v9

    .line 2885
    if-eqz v9, :cond_6

    .line 2887
    invoke-virtual {v9}, Lcom/osp/device/a;->d()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v9

    .line 2892
    :goto_3
    :try_start_4
    const-string v5, "MD5"

    invoke-static {v5}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v15

    .line 2893
    const-string v5, "UTF-8"

    invoke-virtual {v9, v5}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v5

    invoke-virtual {v15, v5}, Ljava/security/MessageDigest;->update([B)V

    .line 2894
    new-instance v5, Ljava/lang/String;

    invoke-static {}, Lcom/osp/common/b/b;->a()Lcom/osp/common/b/b;

    move-result-object v16

    invoke-virtual {v15}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v15

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lcom/osp/common/b/b;->a([B)[B

    move-result-object v15

    const-string v16, "UTF-8"

    move-object/from16 v0, v16

    invoke-direct {v5, v15, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 2902
    :goto_4
    new-instance v15, Lcom/osp/app/signin/hq;

    invoke-direct {v15}, Lcom/osp/app/signin/hq;-><init>()V

    .line 2903
    invoke-virtual {v15, v13}, Lcom/osp/app/signin/hq;->k(Ljava/lang/String;)V

    .line 2904
    invoke-virtual {v15, v5}, Lcom/osp/app/signin/hq;->r(Ljava/lang/String;)V

    .line 2905
    invoke-virtual {v15, v14}, Lcom/osp/app/signin/hq;->m(Ljava/lang/String;)V

    .line 2906
    invoke-virtual {v15, v8}, Lcom/osp/app/signin/hq;->l(Ljava/lang/String;)V

    .line 2907
    invoke-virtual {v15, v12}, Lcom/osp/app/signin/hq;->n(Ljava/lang/String;)V

    .line 2908
    invoke-virtual {v15, v11}, Lcom/osp/app/signin/hq;->o(Ljava/lang/String;)V

    .line 2909
    invoke-virtual {v15, v10}, Lcom/osp/app/signin/hq;->p(Ljava/lang/String;)V

    .line 2910
    invoke-virtual {v15, v7}, Lcom/osp/app/signin/hq;->a(I)V

    .line 2911
    invoke-virtual {v15, v4}, Lcom/osp/app/signin/hq;->q(Ljava/lang/String;)V

    .line 2912
    invoke-virtual {v15, v6}, Lcom/osp/app/signin/hq;->c(Ljava/lang/String;)V

    .line 2914
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 2916
    new-instance v9, Lcom/msc/sa/selfupdate/e;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v9, v0, v1, v2}, Lcom/msc/sa/selfupdate/e;-><init>(Lcom/msc/sa/selfupdate/c;Lcom/msc/b/h;Lcom/osp/app/signin/SamsungService;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v4, "SRS"

    const-string v5, "prepareStubDownload"

    invoke-static {v4, v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/msc/c/k;->g()Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v6

    invoke-virtual {v6}, Lcom/osp/app/util/ah;->g()Z

    move-result v8

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v6

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v7

    invoke-virtual/range {v4 .. v9}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v5

    invoke-virtual {v5}, Lcom/osp/app/util/ah;->c()Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v6

    invoke-virtual {v6}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    const-string v5, "appId"

    invoke-virtual {v15}, Lcom/osp/app/signin/hq;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "encImei"

    invoke-virtual {v15}, Lcom/osp/app/signin/hq;->m()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "deviceId"

    invoke-virtual {v15}, Lcom/osp/app/signin/hq;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "mcc"

    invoke-virtual {v15}, Lcom/osp/app/signin/hq;->h()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "mnc"

    invoke-virtual {v15}, Lcom/osp/app/signin/hq;->i()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "csc"

    invoke-virtual {v15}, Lcom/osp/app/signin/hq;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "sdkVer"

    invoke-virtual {v15}, Lcom/osp/app/signin/hq;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "signID"

    invoke-virtual {v15}, Lcom/osp/app/signin/hq;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "versionCode"

    invoke-virtual {v15}, Lcom/osp/app/signin/hq;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "pd"

    invoke-virtual {v15}, Lcom/osp/app/signin/hq;->l()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/msc/sa/selfupdate/c;->b:Lcom/msc/b/d;

    .line 2969
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/msc/sa/selfupdate/c;->b:Lcom/msc/b/d;

    invoke-virtual {v4}, Lcom/msc/b/d;->a()J

    move-result-wide v4

    sget-object v6, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v4, v5, v6}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 2970
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/msc/sa/selfupdate/c;->l:Lcom/msc/sa/selfupdate/n;

    return-object v4

    .line 2839
    :cond_5
    :try_start_5
    const-string v6, ""
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    goto/16 :goto_0

    .line 2865
    :catch_0
    move-exception v4

    move-object/from16 v17, v4

    move-object v4, v5

    move-object v5, v14

    move-object/from16 v14, v17

    :goto_5
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 2897
    :catch_1
    move-exception v9

    :goto_6
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_4

    :catch_2
    move-exception v5

    move-object/from16 v17, v5

    move-object v5, v9

    move-object/from16 v9, v17

    goto :goto_6

    .line 2865
    :catch_3
    move-exception v5

    move-object/from16 v17, v5

    move-object v5, v14

    move-object/from16 v14, v17

    goto :goto_5

    :catch_4
    move-exception v5

    move-object/from16 v17, v5

    move-object v5, v14

    move-object/from16 v14, v17

    goto :goto_5

    :cond_6
    move-object v9, v5

    goto/16 :goto_3

    :cond_7
    move-object v14, v9

    goto/16 :goto_2
.end method

.method public final a(Lcom/osp/app/signin/SamsungService;Ljava/lang/String;)Lcom/msc/sa/selfupdate/n;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v0, 0x0

    .line 1475
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SLFUCHKMGR"

    const-string v2, "selfUpgradefromXML START"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1477
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v1

    .line 1478
    invoke-virtual {v1, v8}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 1479
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 1483
    new-instance v3, Ljava/io/StringReader;

    invoke-direct {v3, p2}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 1485
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1489
    :try_start_0
    new-instance v1, Ljava/lang/StringBuffer;

    const-string v5, ""

    invoke-direct {v1, v5}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1490
    new-instance v5, Ljava/io/BufferedReader;

    invoke-direct {v5, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 1492
    :goto_0
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 1494
    invoke-virtual {v1, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1601
    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1606
    invoke-virtual {v3}, Ljava/io/StringReader;->close()V

    .line 1613
    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "selfUpgradefromXML END"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1614
    iget-object v0, p0, Lcom/msc/sa/selfupdate/c;->l:Lcom/msc/sa/selfupdate/n;

    :goto_2
    return-object v0

    .line 1496
    :cond_0
    :try_start_2
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V

    .line 1498
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1500
    new-instance v5, Ljava/io/StringReader;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v5}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 1501
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    .line 1507
    :goto_3
    if-eq v1, v8, :cond_7

    .line 1509
    packed-switch v1, :pswitch_data_0

    .line 1567
    :cond_1
    :goto_4
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    goto :goto_3

    .line 1512
    :pswitch_0
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1514
    const-string v5, "errorString"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1516
    const/4 v5, 0x0

    const-string v6, "errorCode"

    invoke-interface {v2, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1517
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 1518
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v6

    .line 1519
    const-string v7, "0"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "No data"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1521
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "No available apk"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1522
    new-instance v0, Lcom/msc/sa/selfupdate/n;

    invoke-direct {v0}, Lcom/msc/sa/selfupdate/n;-><init>()V

    iput-object v0, p0, Lcom/msc/sa/selfupdate/c;->l:Lcom/msc/sa/selfupdate/n;

    .line 1523
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "selfUpgradefromXML END"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1524
    iget-object v0, p0, Lcom/msc/sa/selfupdate/c;->l:Lcom/msc/sa/selfupdate/n;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1606
    invoke-virtual {v3}, Ljava/io/StringReader;->close()V

    goto :goto_2

    .line 1528
    :cond_2
    :try_start_3
    const-string v5, "list"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1530
    new-instance v0, Lcom/msc/sa/selfupdate/n;

    invoke-direct {v0}, Lcom/msc/sa/selfupdate/n;-><init>()V

    .line 1533
    :cond_3
    const-string v5, "Value"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1535
    const/4 v1, 0x0

    const-string v5, "name"

    invoke-interface {v2, v1, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1536
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 1537
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v5

    .line 1539
    const-string v6, "upgrade"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1541
    invoke-static {v0, v5}, Lcom/msc/sa/selfupdate/n;->a(Lcom/msc/sa/selfupdate/n;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4

    .line 1606
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Ljava/io/StringReader;->close()V

    .line 1608
    throw v0

    .line 1542
    :cond_4
    :try_start_4
    const-string v6, "version"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1544
    const-string v1, "."

    const-string v6, ""

    invoke-virtual {v5, v1, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/msc/sa/selfupdate/n;->a(Lcom/msc/sa/selfupdate/n;I)V

    goto/16 :goto_4

    .line 1546
    :cond_5
    const-string v6, "downLoadURI"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1548
    invoke-static {v0, v5}, Lcom/msc/sa/selfupdate/n;->b(Lcom/msc/sa/selfupdate/n;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 1549
    :cond_6
    const-string v6, "contentSize"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1551
    invoke-static {v0, v5}, Lcom/msc/sa/selfupdate/n;->c(Lcom/msc/sa/selfupdate/n;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 1556
    :pswitch_1
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1558
    const-string v5, "list"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1560
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 1570
    :cond_7
    invoke-static {p1, v4}, Lcom/msc/sa/selfupdate/c;->a(Landroid/content/Context;Ljava/util/ArrayList;)I

    move-result v0

    .line 1571
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SLFUCHKMGR updateVersionIndex : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1573
    if-ltz v0, :cond_8

    .line 1575
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/msc/sa/selfupdate/n;

    iput-object v0, p0, Lcom/msc/sa/selfupdate/c;->l:Lcom/msc/sa/selfupdate/n;

    .line 1576
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "upgrade : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/selfupdate/c;->l:Lcom/msc/sa/selfupdate/n;

    invoke-static {v2}, Lcom/msc/sa/selfupdate/n;->a(Lcom/msc/sa/selfupdate/n;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1577
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Next Version : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/selfupdate/c;->l:Lcom/msc/sa/selfupdate/n;

    invoke-static {v2}, Lcom/msc/sa/selfupdate/n;->b(Lcom/msc/sa/selfupdate/n;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1578
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SLFUCHKMGR DownLoadURI : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/msc/sa/selfupdate/c;->l:Lcom/msc/sa/selfupdate/n;

    invoke-virtual {v1}, Lcom/msc/sa/selfupdate/n;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1579
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ContentSize : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/selfupdate/c;->l:Lcom/msc/sa/selfupdate/n;

    invoke-virtual {v2}, Lcom/msc/sa/selfupdate/n;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1594
    iget-object v0, p0, Lcom/msc/sa/selfupdate/c;->l:Lcom/msc/sa/selfupdate/n;

    invoke-static {v0}, Lcom/msc/sa/selfupdate/n;->b(Lcom/msc/sa/selfupdate/n;)I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_8

    .line 1596
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/msc/sa/selfupdate/c;->q:Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1606
    :cond_8
    invoke-virtual {v3}, Ljava/io/StringReader;->close()V

    goto/16 :goto_1

    .line 1509
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/msc/sa/selfupdate/c;->g:Z

    .line 211
    return-void
.end method

.method public final a(Landroid/app/Application;ZZLjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 442
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "startSelfUpgradeCheck called"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/osp/app/signin/SamsungService;

    if-nez v0, :cond_2

    .line 446
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "The application is not instance of SamsungService"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    :cond_1
    :goto_0
    return-void

    .line 450
    :cond_2
    iget-boolean v0, p0, Lcom/msc/sa/selfupdate/c;->m:Z

    if-eqz v0, :cond_3

    .line 452
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "Already Downloaded apk for self-upgrade"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 456
    :cond_3
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 458
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "Setupwizard mode"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move-object v1, p1

    .line 472
    check-cast v1, Lcom/osp/app/signin/SamsungService;

    .line 474
    new-instance v0, Lcom/osp/app/signin/hq;

    invoke-direct {v0}, Lcom/osp/app/signin/hq;-><init>()V

    iput-object v0, p0, Lcom/msc/sa/selfupdate/c;->i:Lcom/osp/app/signin/hq;

    .line 476
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 478
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    :goto_1
    sget-object v4, Lcom/msc/sa/selfupdate/a;->a:[Ljava/lang/String;

    array-length v4, v4

    if-ge v0, v4, :cond_5

    sget-object v4, Lcom/msc/sa/selfupdate/a;->a:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v4, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    move v2, v3

    :cond_5
    move v4, v2

    .line 481
    :goto_2
    invoke-static {}, Lcom/msc/sa/selfupdate/x;->a()Lcom/msc/sa/selfupdate/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/x;->b()V

    .line 483
    iget v0, p0, Lcom/msc/sa/selfupdate/c;->k:I

    if-nez v0, :cond_7

    .line 485
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v2, "status is STOP"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v2, "requestStubUpdateCheckURL"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    iget-object v5, p0, Lcom/msc/sa/selfupdate/c;->i:Lcom/osp/app/signin/hq;

    move-object v0, p0

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/msc/sa/selfupdate/c;->a(Lcom/osp/app/signin/SamsungService;ZZZLcom/osp/app/signin/hq;)V

    goto :goto_0

    .line 478
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 502
    :cond_7
    iget v0, p0, Lcom/msc/sa/selfupdate/c;->k:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_9

    .line 504
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v2, "status is FINISH"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    iget-boolean v0, p0, Lcom/msc/sa/selfupdate/c;->j:Z

    if-eqz v0, :cond_8

    .line 507
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v2, "FINISH - update is necessary"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    invoke-virtual {p0, v1, p2}, Lcom/msc/sa/selfupdate/c;->a(Lcom/osp/app/signin/SamsungService;Z)V

    goto/16 :goto_0

    .line 511
    :cond_8
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "FINISH - update is unnecessary"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    invoke-static {}, Lcom/msc/sa/selfupdate/x;->a()Lcom/msc/sa/selfupdate/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/x;->c()V

    goto/16 :goto_0

    .line 514
    :cond_9
    iget v0, p0, Lcom/msc/sa/selfupdate/c;->k:I

    if-ne v0, v3, :cond_1

    .line 516
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "status is RUNNING"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    :try_start_0
    iget-object v0, p0, Lcom/msc/sa/selfupdate/c;->o:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/msc/sa/selfupdate/c;->o:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_1

    if-ne p3, v3, :cond_1

    .line 521
    iget-object v0, p0, Lcom/msc/sa/selfupdate/c;->o:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 523
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 526
    invoke-static {}, Lcom/msc/sa/selfupdate/x;->a()Lcom/msc/sa/selfupdate/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/x;->c()V

    goto/16 :goto_0

    :cond_a
    move v4, v2

    goto/16 :goto_2
.end method

.method public final a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 307
    const-string v0, "SELF_UPGRADE"

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 308
    const-string v1, "AUTO_UPDATE_TYPE"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 310
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-static {p1}, Lcom/osp/app/util/ad;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 314
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    iget-wide v2, p0, Lcom/msc/sa/selfupdate/c;->d:J

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/i;->a(J)V

    .line 315
    invoke-virtual {p0}, Lcom/msc/sa/selfupdate/c;->d()V

    .line 317
    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/io/File;)V
    .locals 1

    .prologue
    .line 2414
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 2416
    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/msc/sa/selfupdate/b;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2418
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/msc/sa/selfupdate/c;->m:Z

    .line 2421
    :cond_0
    return-void
.end method

.method public final a(Lcom/osp/app/signin/SamsungService;Z)V
    .locals 8

    .prologue
    const/high16 v7, 0x10000000

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1330
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "processResponse START"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1332
    invoke-static {}, Lcom/msc/sa/selfupdate/x;->a()Lcom/msc/sa/selfupdate/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/x;->b()V

    .line 1334
    iget-object v0, p0, Lcom/msc/sa/selfupdate/c;->l:Lcom/msc/sa/selfupdate/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/msc/sa/selfupdate/c;->l:Lcom/msc/sa/selfupdate/n;

    invoke-virtual {v0, v5}, Lcom/msc/sa/selfupdate/n;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1337
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "Upgrade is necessary"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1338
    iput-boolean v6, p0, Lcom/msc/sa/selfupdate/c;->j:Z

    .line 1347
    iget-boolean v0, p0, Lcom/msc/sa/selfupdate/c;->m:Z

    if-eqz v0, :cond_1

    .line 1349
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "Already Downloaded apk for self-upgrade !"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1350
    invoke-static {}, Lcom/msc/sa/selfupdate/x;->a()Lcom/msc/sa/selfupdate/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/x;->c()V

    .line 1426
    :goto_0
    return-void

    .line 1341
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "Upgrade is NOT necessary"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1342
    iput-boolean v5, p0, Lcom/msc/sa/selfupdate/c;->j:Z

    .line 1343
    invoke-static {}, Lcom/msc/sa/selfupdate/x;->a()Lcom/msc/sa/selfupdate/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/x;->c()V

    goto :goto_0

    .line 1354
    :cond_1
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1356
    invoke-static {}, Lcom/msc/sa/selfupdate/x;->a()Lcom/msc/sa/selfupdate/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/x;->c()V

    goto :goto_0

    .line 1414
    :cond_2
    if-eqz p2, :cond_6

    .line 1417
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "showDialogActivity START"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/osp/app/signin/SamsungService;->n()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "activity is null or isFinishing"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "URI"

    iget-object v2, p0, Lcom/msc/sa/selfupdate/c;->l:Lcom/msc/sa/selfupdate/n;

    invoke-virtual {v2}, Lcom/msc/sa/selfupdate/n;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "SIZE"

    iget-object v2, p0, Lcom/msc/sa/selfupdate/c;->l:Lcom/msc/sa/selfupdate/n;

    invoke-virtual {v2}, Lcom/msc/sa/selfupdate/n;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Lcom/osp/app/signin/SamsungService;->startActivity(Landroid/content/Intent;)V

    :cond_4
    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "showDialogActivity END"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1425
    :goto_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "processResponse END"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1417
    :cond_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v2, "activity is alive"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "URI"

    iget-object v3, p0, Lcom/msc/sa/selfupdate/c;->l:Lcom/msc/sa/selfupdate/n;

    invoke-virtual {v3}, Lcom/msc/sa/selfupdate/n;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "SIZE"

    iget-object v3, p0, Lcom/msc/sa/selfupdate/c;->l:Lcom/msc/sa/selfupdate/n;

    invoke-virtual {v3}, Lcom/msc/sa/selfupdate/n;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v2, 0xd6

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    instance-of v0, v1, Lcom/osp/app/signin/SignInView;

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v2, "current activity is SignInView"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Lcom/osp/app/signin/SignInView;

    new-instance v2, Lcom/msc/sa/selfupdate/h;

    invoke-direct {v2, p0, v1}, Lcom/msc/sa/selfupdate/h;-><init>(Lcom/msc/sa/selfupdate/c;Landroid/app/Activity;)V

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/SignInView;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 1421
    :cond_6
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "showNotification START"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f09004f

    invoke-virtual {p1, v0}, Lcom/osp/app/signin/SamsungService;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f090188

    invoke-virtual {p1, v0}, Lcom/osp/app/signin/SamsungService;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "URI"

    iget-object v2, p0, Lcom/msc/sa/selfupdate/c;->l:Lcom/msc/sa/selfupdate/n;

    invoke-virtual {v2}, Lcom/msc/sa/selfupdate/n;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "SIZE"

    iget-object v2, p0, Lcom/msc/sa/selfupdate/c;->l:Lcom/msc/sa/selfupdate/n;

    invoke-virtual {v2}, Lcom/msc/sa/selfupdate/n;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "UPGRADE_WITH_NOTIFICATION"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v0, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/high16 v1, 0x8000000

    invoke-static {p1, v5, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUCHKMGR"

    const-string v1, "showNotification END"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public final d()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 2358
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "cancelSelfUpgrade called"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2359
    iget-wide v0, p0, Lcom/msc/sa/selfupdate/c;->n:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    .line 2361
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    iget-wide v2, p0, Lcom/msc/sa/selfupdate/c;->n:J

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/i;->a(J)V

    .line 2362
    iput-wide v4, p0, Lcom/msc/sa/selfupdate/c;->n:J

    .line 2363
    iget v0, p0, Lcom/msc/sa/selfupdate/c;->k:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2365
    const/4 v0, 0x0

    iput v0, p0, Lcom/msc/sa/selfupdate/c;->k:I

    .line 2368
    :cond_0
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2385
    iget-boolean v0, p0, Lcom/msc/sa/selfupdate/c;->m:Z

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 2405
    iget-boolean v0, p0, Lcom/msc/sa/selfupdate/c;->j:Z

    return v0
.end method
