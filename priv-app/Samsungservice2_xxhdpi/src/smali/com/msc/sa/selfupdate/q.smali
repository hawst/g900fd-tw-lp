.class public final Lcom/msc/sa/selfupdate/q;
.super Landroid/os/AsyncTask;
.source "SelfUpgradePopup.java"


# instance fields
.field final synthetic a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

.field private b:I


# direct methods
.method public constructor <init>(Lcom/msc/sa/selfupdate/SelfUpgradePopup;)V
    .locals 1

    .prologue
    .line 552
    iput-object p1, p0, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 554
    const/4 v0, -0x1

    iput v0, p0, Lcom/msc/sa/selfupdate/q;->b:I

    return-void
.end method

.method static synthetic a(Lcom/msc/sa/selfupdate/q;[Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 552
    invoke-virtual {p0, p1}, Lcom/msc/sa/selfupdate/q;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 552
    check-cast p1, [Ljava/lang/String;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUPOP"

    const-string v1, "doInBackground called"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "fail"

    invoke-virtual {p0}, Lcom/msc/sa/selfupdate/q;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    aget-object v1, p1, v1

    const-string v2, "install"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v6, "fail"

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    iget-object v0, p0, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-static {v0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->c(Lcom/msc/sa/selfupdate/SelfUpgradePopup;)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Lcom/msc/sa/selfupdate/r;

    invoke-direct {v5, p0}, Lcom/msc/sa/selfupdate/r;-><init>(Lcom/msc/sa/selfupdate/q;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v2, " prepareDownloadingApk"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "SRS request URI : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/msc/b/d;->b()V

    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/c;->e()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    const-string v0, "success"

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    move-object v0, v6

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 552
    check-cast p1, Ljava/lang/String;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUPOP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onPostExecute : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "success"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-static {v0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->f(Lcom/msc/sa/selfupdate/SelfUpgradePopup;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-static {v1}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->d(Lcom/msc/sa/selfupdate/SelfUpgradePopup;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f09013c

    invoke-static {v1, v2}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-static {v0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->d(Lcom/msc/sa/selfupdate/SelfUpgradePopup;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "SELF_UPGRADE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "SELF_UPGRADE_START_TIME"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUPOP"

    const-string v1, "Try to save SELF_UPGRADE_START_TIME"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/msc/sa/selfupdate/s;

    invoke-direct {v0, p0}, Lcom/msc/sa/selfupdate/s;-><init>(Lcom/msc/sa/selfupdate/q;)V

    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    invoke-static {v0}, Lcom/msc/sa/selfupdate/c;->a(Lcom/msc/sa/selfupdate/k;)V

    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    iget-object v0, p0, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    iget-object v1, p0, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    iget-object v1, v1, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->a:Ljava/io/File;

    invoke-static {v0, v1}, Lcom/msc/sa/selfupdate/c;->b(Landroid/content/Context;Ljava/io/File;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SLFUPOP"

    const-string v1, "Apk downloading is failed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0}, Lcom/osp/app/signin/SamsungService;->n()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_1

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_1
    iget-object v0, p0, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->c()V

    iget-object v0, p0, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-static {v0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->g(Lcom/msc/sa/selfupdate/SelfUpgradePopup;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/msc/sa/selfupdate/u;

    invoke-direct {v1, p0}, Lcom/msc/sa/selfupdate/u;-><init>(Lcom/msc/sa/selfupdate/q;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->finish()V

    goto :goto_0
.end method

.method protected final onPreExecute()V
    .locals 2

    .prologue
    .line 558
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 559
    iget-object v0, p0, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 561
    iget-object v0, p0, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->onCreateDialog(I)Landroid/app/Dialog;

    .line 563
    :cond_0
    return-void
.end method

.method protected final synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 552
    check-cast p1, [Ljava/lang/String;

    iget-object v0, p0, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-static {v0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->f(Lcom/msc/sa/selfupdate/SelfUpgradePopup;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iget v1, p0, Lcom/msc/sa/selfupdate/q;->b:I

    if-eq v1, v0, :cond_0

    iput v0, p0, Lcom/msc/sa/selfupdate/q;->b:I

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SLFUPOP"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onProgressUpdate : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/msc/sa/selfupdate/q;->a:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-static {v1}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->f(Lcom/msc/sa/selfupdate/SelfUpgradePopup;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setProgress(I)V

    :cond_0
    return-void
.end method
