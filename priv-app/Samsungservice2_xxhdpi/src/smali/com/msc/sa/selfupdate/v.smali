.class final Lcom/msc/sa/selfupdate/v;
.super Lcom/msc/c/b;
.source "SelfUpgradePopup.java"


# instance fields
.field final synthetic c:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

.field private final d:Lcom/msc/sa/selfupdate/c;

.field private final e:Lcom/osp/app/signin/SamsungService;

.field private f:Lcom/msc/sa/selfupdate/n;


# direct methods
.method public constructor <init>(Lcom/msc/sa/selfupdate/SelfUpgradePopup;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 487
    iput-object p1, p0, Lcom/msc/sa/selfupdate/v;->c:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    .line 488
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/msc/c/b;-><init>(Landroid/content/Context;Z)V

    .line 489
    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v0

    iput-object v0, p0, Lcom/msc/sa/selfupdate/v;->d:Lcom/msc/sa/selfupdate/c;

    .line 490
    invoke-virtual {p1}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    iput-object v0, p0, Lcom/msc/sa/selfupdate/v;->e:Lcom/osp/app/signin/SamsungService;

    .line 491
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 497
    iget-object v0, p0, Lcom/msc/sa/selfupdate/v;->d:Lcom/msc/sa/selfupdate/c;

    iget-object v1, p0, Lcom/msc/sa/selfupdate/v;->e:Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0, v1, p0}, Lcom/msc/sa/selfupdate/c;->a(Lcom/osp/app/signin/SamsungService;Lcom/msc/b/h;)Lcom/msc/sa/selfupdate/n;

    move-result-object v0

    iput-object v0, p0, Lcom/msc/sa/selfupdate/v;->f:Lcom/msc/sa/selfupdate/n;

    .line 502
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 5

    .prologue
    const v3, 0x7f09004a

    const/16 v2, 0xa

    const/4 v1, 0x1

    .line 507
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 508
    iget-object v0, p0, Lcom/msc/sa/selfupdate/v;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_1

    .line 510
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/msc/sa/selfupdate/v;->c:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3, v1}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 511
    iget-object v0, p0, Lcom/msc/sa/selfupdate/v;->c:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0}, Lcom/osp/app/signin/SamsungService;->n()Landroid/app/Activity;

    move-result-object v0

    .line 512
    if-eqz v0, :cond_0

    .line 514
    invoke-virtual {v0, v2}, Landroid/app/Activity;->setResult(I)V

    .line 515
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 517
    :cond_0
    iget-object v0, p0, Lcom/msc/sa/selfupdate/v;->c:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->finish()V

    .line 538
    :goto_0
    return-void

    .line 520
    :cond_1
    iget-object v0, p0, Lcom/msc/sa/selfupdate/v;->f:Lcom/msc/sa/selfupdate/n;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/msc/sa/selfupdate/v;->f:Lcom/msc/sa/selfupdate/n;

    invoke-virtual {v0, v1}, Lcom/msc/sa/selfupdate/n;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 522
    iget-object v0, p0, Lcom/msc/sa/selfupdate/v;->c:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    iget-object v1, p0, Lcom/msc/sa/selfupdate/v;->f:Lcom/msc/sa/selfupdate/n;

    invoke-virtual {v1}, Lcom/msc/sa/selfupdate/n;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->a(Lcom/msc/sa/selfupdate/SelfUpgradePopup;Ljava/lang/String;)Ljava/lang/String;

    .line 523
    iget-object v0, p0, Lcom/msc/sa/selfupdate/v;->c:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    iget-object v1, p0, Lcom/msc/sa/selfupdate/v;->f:Lcom/msc/sa/selfupdate/n;

    invoke-virtual {v1}, Lcom/msc/sa/selfupdate/n;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->b(Lcom/msc/sa/selfupdate/SelfUpgradePopup;Ljava/lang/String;)Ljava/lang/String;

    .line 524
    iget-object v0, p0, Lcom/msc/sa/selfupdate/v;->c:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    new-instance v1, Lcom/msc/sa/selfupdate/q;

    iget-object v2, p0, Lcom/msc/sa/selfupdate/v;->c:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-direct {v1, v2}, Lcom/msc/sa/selfupdate/q;-><init>(Lcom/msc/sa/selfupdate/SelfUpgradePopup;)V

    invoke-static {v0, v1}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->a(Lcom/msc/sa/selfupdate/SelfUpgradePopup;Lcom/msc/sa/selfupdate/q;)Lcom/msc/sa/selfupdate/q;

    .line 525
    iget-object v0, p0, Lcom/msc/sa/selfupdate/v;->c:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-static {v0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->b(Lcom/msc/sa/selfupdate/SelfUpgradePopup;)Lcom/msc/sa/selfupdate/q;

    move-result-object v0

    const-string v1, "install"

    :try_start_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_2

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-virtual {v0, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_2
    :try_start_1
    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 528
    :cond_3
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/msc/sa/selfupdate/v;->c:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3, v1}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 529
    iget-object v0, p0, Lcom/msc/sa/selfupdate/v;->c:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0}, Lcom/osp/app/signin/SamsungService;->n()Landroid/app/Activity;

    move-result-object v0

    .line 530
    if-eqz v0, :cond_4

    .line 532
    invoke-virtual {v0, v2}, Landroid/app/Activity;->setResult(I)V

    .line 533
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 535
    :cond_4
    iget-object v0, p0, Lcom/msc/sa/selfupdate/v;->c:Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/SelfUpgradePopup;->finish()V

    goto/16 :goto_0
.end method

.method public final declared-synchronized b(Lcom/msc/b/c;)V
    .locals 1

    .prologue
    .line 542
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 543
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/msc/sa/selfupdate/v;->b:Lcom/msc/c/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 544
    monitor-exit p0

    return-void

    .line 542
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 480
    invoke-virtual {p0}, Lcom/msc/sa/selfupdate/v;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 480
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/msc/sa/selfupdate/v;->a(Ljava/lang/Boolean;)V

    return-void
.end method
