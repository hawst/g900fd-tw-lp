.class public final Lcom/msc/sa/d/a;
.super Ljava/lang/Object;
.source "RequestBaseInfoVO.java"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Landroid/os/Bundle;

.field private final g:Lcom/msc/sa/a/d;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lcom/msc/sa/a/d;)V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/msc/sa/d/a;->a:Ljava/lang/String;

    .line 80
    iput p1, p0, Lcom/msc/sa/d/a;->b:I

    .line 81
    iput-object p2, p0, Lcom/msc/sa/d/a;->c:Ljava/lang/String;

    .line 82
    iput-object p3, p0, Lcom/msc/sa/d/a;->d:Ljava/lang/String;

    .line 83
    iput-object p4, p0, Lcom/msc/sa/d/a;->e:Ljava/lang/String;

    .line 84
    iput-object p5, p0, Lcom/msc/sa/d/a;->f:Landroid/os/Bundle;

    .line 85
    iput-object p6, p0, Lcom/msc/sa/d/a;->g:Lcom/msc/sa/a/d;

    .line 86
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lcom/msc/sa/d/a;->a:Ljava/lang/String;

    .line 69
    iput p2, p0, Lcom/msc/sa/d/a;->b:I

    .line 70
    iput-object p3, p0, Lcom/msc/sa/d/a;->c:Ljava/lang/String;

    .line 71
    iput-object p4, p0, Lcom/msc/sa/d/a;->d:Ljava/lang/String;

    .line 72
    iput-object p5, p0, Lcom/msc/sa/d/a;->e:Ljava/lang/String;

    .line 73
    iput-object p6, p0, Lcom/msc/sa/d/a;->f:Landroid/os/Bundle;

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/msc/sa/d/a;->g:Lcom/msc/sa/a/d;

    .line 75
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/msc/sa/d/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)[Ljava/lang/String;
    .locals 3

    .prologue
    .line 169
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RBIVO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getStringArrFromAdditionalBundleKey : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    iget-object v0, p0, Lcom/msc/sa/d/a;->f:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/msc/sa/d/a;->f:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 175
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/msc/sa/d/a;->b:I

    return v0
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 185
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RBIVO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getStringFormAdditionalBundleKey : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lcom/msc/sa/d/a;->f:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/msc/sa/d/a;->f:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 191
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/msc/sa/d/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/msc/sa/d/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/msc/sa/d/a;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/msc/sa/d/a;->f:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/msc/sa/d/a;->f:Landroid/os/Bundle;

    .line 144
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Lcom/msc/sa/a/d;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/msc/sa/d/a;->g:Lcom/msc/sa/a/d;

    return-object v0
.end method
