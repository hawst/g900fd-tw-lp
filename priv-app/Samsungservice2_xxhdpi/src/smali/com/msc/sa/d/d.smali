.class public final Lcom/msc/sa/d/d;
.super Ljava/lang/Object;
.source "ResultAuthWithTncMandatoryUtilVO.java"


# instance fields
.field private a:Z

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:Ljava/lang/String;

.field private f:J

.field private g:Ljava/lang/String;

.field private h:J

.field private i:J

.field private j:I

.field private k:Lcom/msc/a/i;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-boolean v1, p0, Lcom/msc/sa/d/d;->a:Z

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/sa/d/d;->b:Ljava/lang/String;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/sa/d/d;->c:Ljava/lang/String;

    .line 38
    iput v1, p0, Lcom/msc/sa/d/d;->d:I

    .line 43
    iput-object v2, p0, Lcom/msc/sa/d/d;->e:Ljava/lang/String;

    .line 48
    iput-wide v4, p0, Lcom/msc/sa/d/d;->f:J

    .line 53
    iput-object v2, p0, Lcom/msc/sa/d/d;->g:Ljava/lang/String;

    .line 58
    iput-wide v4, p0, Lcom/msc/sa/d/d;->h:J

    .line 63
    iput-wide v4, p0, Lcom/msc/sa/d/d;->i:J

    .line 68
    iput v1, p0, Lcom/msc/sa/d/d;->j:I

    .line 73
    iput-object v2, p0, Lcom/msc/sa/d/d;->k:Lcom/msc/a/i;

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 143
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATMUV"

    const-string v1, "setFailCheckList"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    iput p1, p0, Lcom/msc/sa/d/d;->d:I

    .line 145
    return-void
.end method

.method public final a(Lcom/msc/a/i;)V
    .locals 0

    .prologue
    .line 235
    iput-object p1, p0, Lcom/msc/sa/d/d;->k:Lcom/msc/a/i;

    .line 236
    return-void
.end method

.method public final a(Ljava/lang/String;JLjava/lang/String;JJ)V
    .locals 1

    .prologue
    .line 164
    iput-object p1, p0, Lcom/msc/sa/d/d;->e:Ljava/lang/String;

    .line 165
    iput-wide p2, p0, Lcom/msc/sa/d/d;->f:J

    .line 166
    iput-object p4, p0, Lcom/msc/sa/d/d;->g:Ljava/lang/String;

    .line 167
    iput-wide p5, p0, Lcom/msc/sa/d/d;->h:J

    .line 168
    iput-wide p7, p0, Lcom/msc/sa/d/d;->i:J

    .line 169
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 102
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATMUV"

    const-string v1, "setErrorResult"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    iput-object p1, p0, Lcom/msc/sa/d/d;->b:Ljava/lang/String;

    .line 104
    iput-object p2, p0, Lcom/msc/sa/d/d;->c:Ljava/lang/String;

    .line 105
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 81
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATMUV"

    const-string v1, "isSuccess"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    iget-boolean v0, p0, Lcom/msc/sa/d/d;->a:Z

    return v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 91
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATMUV"

    const-string v1, "setResult"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/msc/sa/d/d;->a:Z

    .line 93
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 231
    iput p1, p0, Lcom/msc/sa/d/d;->j:I

    .line 232
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 123
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATMUV"

    const-string v1, "getErrorCode"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/msc/sa/d/d;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 133
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATMUV"

    const-string v1, "getErrorMessage"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/msc/sa/d/d;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 153
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATMUV"

    const-string v1, "getFailCheckList"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    iget v0, p0, Lcom/msc/sa/d/d;->d:I

    return v0
.end method

.method public final f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 177
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATMUV"

    const-string v1, "getAccessToken"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/msc/sa/d/d;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final g()J
    .locals 2

    .prologue
    .line 187
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATMUV"

    const-string v1, "getAccessTokenExpiresIn"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    iget-wide v0, p0, Lcom/msc/sa/d/d;->f:J

    return-wide v0
.end method

.method public final h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 197
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATMUV"

    const-string v1, "getRefreshToken"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    iget-object v0, p0, Lcom/msc/sa/d/d;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final i()J
    .locals 2

    .prologue
    .line 207
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATMUV"

    const-string v1, "getRefreshTokenExpiresIn"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    iget-wide v0, p0, Lcom/msc/sa/d/d;->h:J

    return-wide v0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 217
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RATMUV"

    const-string v1, "getAccessTokenIssuedTime"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    iget-wide v0, p0, Lcom/msc/sa/d/d;->i:J

    return-wide v0
.end method

.method public final k()Lcom/msc/a/i;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/msc/sa/d/d;->k:Lcom/msc/a/i;

    return-object v0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 244
    iget v0, p0, Lcom/msc/sa/d/d;->j:I

    return v0
.end method
