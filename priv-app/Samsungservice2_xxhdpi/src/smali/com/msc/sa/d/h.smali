.class public final Lcom/msc/sa/d/h;
.super Ljava/lang/Object;
.source "ResultTncMandatoryUtilVO.java"


# instance fields
.field private a:Z

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-boolean v1, p0, Lcom/msc/sa/d/h;->a:Z

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/sa/d/h;->b:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/sa/d/h;->c:Ljava/lang/String;

    .line 37
    iput v1, p0, Lcom/msc/sa/d/h;->d:I

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 107
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RTMUV"

    const-string v1, "setFailCheckList"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    iput p1, p0, Lcom/msc/sa/d/h;->d:I

    .line 109
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 66
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RTMUV"

    const-string v1, "setErrorResult"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iput-object p1, p0, Lcom/msc/sa/d/h;->b:Ljava/lang/String;

    .line 68
    iput-object p2, p0, Lcom/msc/sa/d/h;->c:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 45
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RTMUV"

    const-string v1, "isSuccess"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    iget-boolean v0, p0, Lcom/msc/sa/d/h;->a:Z

    return v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 55
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RTMUV"

    const-string v1, "setResult"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/msc/sa/d/h;->a:Z

    .line 57
    return-void
.end method

.method public final c()Lcom/msc/c/f;
    .locals 3

    .prologue
    .line 77
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RTMUV"

    const-string v1, "getErrorResult"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    new-instance v0, Lcom/msc/c/f;

    iget-object v1, p0, Lcom/msc/sa/d/h;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/msc/sa/d/h;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 87
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RTMUV"

    const-string v1, "getErrorCode"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/msc/sa/d/h;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 97
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RTMUV"

    const-string v1, "getErrorMessage"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/msc/sa/d/h;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 2

    .prologue
    .line 117
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RTMUV"

    const-string v1, "getFailCheckList"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    iget v0, p0, Lcom/msc/sa/d/h;->d:I

    return v0
.end method
