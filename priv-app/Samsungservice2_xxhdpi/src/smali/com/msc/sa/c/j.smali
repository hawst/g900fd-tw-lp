.class public final Lcom/msc/sa/c/j;
.super Ljava/lang/Object;
.source "SignatureCheckUtil.java"


# static fields
.field private static a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    sput-boolean v0, Lcom/msc/sa/c/j;->a:Z

    return-void
.end method

.method static synthetic a()Z
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x1

    sput-boolean v0, Lcom/msc/sa/c/j;->a:Z

    return v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 109
    const-class v4, Lcom/msc/sa/c/j;

    monitor-enter v4

    :try_start_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "SCHU"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "runCheckSignature : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v6

    invoke-virtual {v6}, Lcom/osp/device/b;->l()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    if-nez p0, :cond_0

    .line 113
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SCHU"

    const-string v2, "Context is null"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "errorMessage-Internal error occurred"

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    :goto_0
    monitor-exit v4

    return v0

    .line 118
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/device/b;->l()I

    move-result v3

    const/16 v5, 0x10

    if-le v3, v5, :cond_9

    .line 120
    new-instance v5, Lcom/msc/sa/c/m;

    const/4 v3, 0x0

    invoke-direct {v5, v3}, Lcom/msc/sa/c/m;-><init>(B)V

    .line 124
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "SCHU"

    const-string v6, "getSignatureCache"

    invoke-static {v3, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p0, :cond_1

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "SCHU"

    const-string v6, "Context is null"

    invoke-static {v3, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "errorMessage-Internal error occurred"

    invoke-static {v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 125
    :goto_1
    if-eqz v2, :cond_8

    .line 127
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "SCHU"

    const-string v6, "Use cached signature"

    invoke-static {v3, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "SCHU"

    const-string v6, "checkCacheSignatureValidation"

    invoke-static {v3, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p0, :cond_3

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "SCHU"

    const-string v3, "Context is null"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "errorMessage-Internal error occurred"

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Lcom/msc/sa/c/m;->a(Z)V

    .line 131
    :goto_2
    invoke-virtual {v5}, Lcom/msc/sa/c/m;->a()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 133
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCHU"

    const-string v2, "success to check signature with cache data "

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 134
    goto :goto_0

    .line 124
    :cond_1
    invoke-static {p0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "SCHU"

    const-string v6, "no signature cache list"

    invoke-static {v3, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 109
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    :cond_2
    move-object v2, v3

    .line 124
    goto :goto_1

    .line 130
    :cond_3
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->A(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {p0, v2, p1, p2}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    :goto_3
    :try_start_3
    invoke-virtual {v5, v2}, Lcom/msc/sa/c/m;->a(Z)V

    goto :goto_2

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    instance-of v2, v2, Landroid/content/pm/PackageManager$NameNotFoundException;

    if-eqz v2, :cond_4

    invoke-virtual {v5}, Lcom/msc/sa/c/m;->c()V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "SCHU"

    const-string v3, "Try Catch NameNotFoundException"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v0

    goto :goto_3

    :cond_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "SCHU"

    const-string v3, "Try Catch exception"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v0

    goto :goto_3

    .line 137
    :cond_5
    invoke-virtual {v5}, Lcom/msc/sa/c/m;->b()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 139
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SCHU"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Name Not Found Exception has occurred. Not Installed Package"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v3

    invoke-virtual {v3, p2}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 144
    :cond_6
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "SCHU"

    const-string v3, "Not matched cache data of signature"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    sget-boolean v2, Lcom/msc/sa/c/j;->a:Z

    if-ne v2, v1, :cond_7

    .line 149
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SCHU"

    const-string v2, "Cache data was already updated by server signature data. Don\'t need to request data."

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 153
    :cond_7
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCHU"

    const-string v1, "start request signatureList"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    :cond_8
    invoke-static {p0, p1, p2}, Lcom/msc/sa/c/j;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v5, v0}, Lcom/msc/sa/c/m;->a(Z)V

    .line 159
    invoke-virtual {v5}, Lcom/msc/sa/c/m;->a()Z

    move-result v0

    goto/16 :goto_0

    .line 162
    :cond_9
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCHU"

    const-string v2, "sdk version =< 16"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 163
    goto/16 :goto_0
.end method

.method static synthetic b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SCHU"

    const-string v2, "requestPackageSignatureInfoList"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p0, :cond_0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SCHU"

    const-string v2, "Context is null"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "errorMessage-Internal error occurred"

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    new-instance v5, Lcom/msc/sa/c/m;

    invoke-direct {v5, v0}, Lcom/msc/sa/c/m;-><init>(B)V

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    new-instance v0, Lcom/msc/sa/c/l;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/msc/sa/c/l;-><init>(Landroid/content/Context;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/sa/c/m;)V

    invoke-static {p0, v0}, Lcom/msc/c/g;->c(Landroid/content/Context;Lcom/msc/b/h;)J

    move-result-wide v0

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    invoke-virtual {v5}, Lcom/msc/sa/c/m;->a()Z

    move-result v0

    goto :goto_0
.end method

.method private static c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 246
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SCHU"

    const-string v2, "requestSignatureAndWaiting"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    if-nez p0, :cond_0

    .line 250
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SCHU"

    const-string v2, "Context is null"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "errorMessage-Internal error occurred"

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 279
    :goto_0
    return v0

    .line 255
    :cond_0
    new-instance v1, Lcom/msc/sa/c/m;

    invoke-direct {v1, v0}, Lcom/msc/sa/c/m;-><init>(B)V

    .line 256
    new-instance v0, Lcom/msc/sa/c/k;

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/msc/sa/c/k;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/sa/c/m;)V

    .line 265
    new-instance v2, Ljava/lang/Thread;

    invoke-direct {v2, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 266
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 271
    :try_start_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCHU"

    const-string v3, "Checking... "

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    invoke-virtual {v2}, Ljava/lang/Thread;->join()V

    .line 273
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCHU"

    const-string v2, "END checking"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 279
    :goto_1
    invoke-virtual {v1}, Lcom/msc/sa/c/m;->a()Z

    move-result v0

    goto :goto_0

    .line 274
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method
