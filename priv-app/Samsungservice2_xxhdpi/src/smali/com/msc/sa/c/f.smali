.class public final Lcom/msc/sa/c/f;
.super Landroid/app/DialogFragment;
.source "CompatibleAPIUtil.java"


# instance fields
.field private a:Landroid/content/DialogInterface$OnDismissListener;

.field private b:Landroid/content/DialogInterface$OnCancelListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 771
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a(I)Lcom/msc/sa/c/f;
    .locals 3

    .prologue
    .line 798
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BA_DFFJ"

    const-string v1, "newInstance()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 800
    new-instance v0, Lcom/msc/sa/c/f;

    invoke-direct {v0}, Lcom/msc/sa/c/f;-><init>()V

    .line 801
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 802
    const-string v2, "dialog_type"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 803
    invoke-virtual {v0, v1}, Lcom/msc/sa/c/f;->setArguments(Landroid/os/Bundle;)V

    .line 804
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 0

    .prologue
    .line 857
    iput-object p1, p0, Lcom/msc/sa/c/f;->b:Landroid/content/DialogInterface$OnCancelListener;

    .line 859
    return-void
.end method

.method public final a(Landroid/content/DialogInterface$OnDismissListener;)V
    .locals 0

    .prologue
    .line 847
    iput-object p1, p0, Lcom/msc/sa/c/f;->a:Landroid/content/DialogInterface$OnDismissListener;

    .line 849
    return-void
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 875
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 877
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "onCancel()!!"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 879
    iget-object v0, p0, Lcom/msc/sa/c/f;->b:Landroid/content/DialogInterface$OnCancelListener;

    if-eqz v0, :cond_0

    .line 881
    iget-object v0, p0, Lcom/msc/sa/c/f;->b:Landroid/content/DialogInterface$OnCancelListener;

    invoke-interface {v0, p1}, Landroid/content/DialogInterface$OnCancelListener;->onCancel(Landroid/content/DialogInterface;)V

    .line 883
    :cond_0
    return-void
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 819
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BA_DFFJ"

    const-string v2, "onCreateDialog()"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 821
    invoke-virtual {p0}, Lcom/msc/sa/c/f;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 822
    const-string v2, "dialog_type"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 825
    invoke-virtual {p0}, Lcom/msc/sa/c/f;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 826
    instance-of v3, v0, Lcom/osp/app/util/BaseActivity;

    if-eqz v3, :cond_1

    .line 828
    check-cast v0, Lcom/osp/app/util/BaseActivity;

    invoke-virtual {v0, v2}, Lcom/osp/app/util/BaseActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 830
    if-eqz v0, :cond_0

    .line 832
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 833
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 837
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 863
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 865
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "onDismiss()!!"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 867
    iget-object v0, p0, Lcom/msc/sa/c/f;->a:Landroid/content/DialogInterface$OnDismissListener;

    if-eqz v0, :cond_0

    .line 869
    iget-object v0, p0, Lcom/msc/sa/c/f;->a:Landroid/content/DialogInterface$OnDismissListener;

    invoke-interface {v0, p1}, Landroid/content/DialogInterface$OnDismissListener;->onDismiss(Landroid/content/DialogInterface;)V

    .line 871
    :cond_0
    return-void
.end method
