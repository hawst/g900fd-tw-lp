.class final Lcom/msc/sa/c/p;
.super Lcom/msc/sa/c/q;
.source "TncMandatoryUtil.java"


# instance fields
.field final synthetic a:I

.field final synthetic b:Landroid/content/Context;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Z

.field final synthetic f:Lcom/msc/sa/d/h;


# direct methods
.method constructor <init>(Lcom/msc/sa/d/h;ILandroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLcom/msc/sa/d/h;)V
    .locals 0

    .prologue
    .line 176
    iput p2, p0, Lcom/msc/sa/c/p;->a:I

    iput-object p3, p0, Lcom/msc/sa/c/p;->b:Landroid/content/Context;

    iput-object p4, p0, Lcom/msc/sa/c/p;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/msc/sa/c/p;->d:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/msc/sa/c/p;->e:Z

    iput-object p7, p0, Lcom/msc/sa/c/p;->f:Lcom/msc/sa/d/h;

    invoke-direct {p0, p1}, Lcom/msc/sa/c/q;-><init>(Lcom/msc/sa/d/h;)V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 7

    .prologue
    .line 180
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/sa/c/q;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    if-nez p1, :cond_0

    .line 188
    :goto_0
    monitor-exit p0

    return-void

    .line 186
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v4

    .line 187
    iget v0, p0, Lcom/msc/sa/c/p;->a:I

    iget-object v1, p0, Lcom/msc/sa/c/p;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/msc/sa/c/p;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/msc/sa/c/p;->d:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/msc/sa/c/p;->e:Z

    iget-object v6, p0, Lcom/msc/sa/c/p;->f:Lcom/msc/sa/d/h;

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/o;->a(ILandroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/msc/sa/d/h;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 180
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 7

    .prologue
    .line 192
    invoke-super {p0, p1}, Lcom/msc/sa/c/q;->b(Lcom/msc/b/c;)V

    .line 193
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/msc/sa/c/p;->g:Lcom/msc/sa/d/h;

    if-eqz v0, :cond_0

    const-string v0, "USR_3174"

    iget-object v1, p0, Lcom/msc/sa/c/p;->g:Lcom/msc/sa/d/h;

    invoke-virtual {v1}, Lcom/msc/sa/d/h;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/msc/sa/c/p;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/msc/c/c;->g(Landroid/content/Context;)V

    .line 196
    iget-object v0, p0, Lcom/msc/sa/c/p;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/msc/c/e;->a(Landroid/content/Context;)Z

    .line 197
    iget-object v0, p0, Lcom/msc/sa/c/p;->f:Lcom/msc/sa/d/h;

    const-string v1, "SAC_0402"

    const-string v2, "Auth token expired"

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/d/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    iget-object v0, p0, Lcom/msc/sa/c/p;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/msc/sa/c/p;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/msc/sa/c/p;->d:Ljava/lang/String;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "TMU"

    const-string v4, "showReSignNotification"

    invoke-static {v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_1

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.msc.action.samsungaccount.resignin"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v4, 0x10000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v4, "client_id"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account_mode"

    const-string v2, ""

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "BG_WhoareU"

    const-string v2, "BG_mode"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "email_id"

    invoke-static {v0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "from_notification"

    const/4 v2, 0x1

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {v0, v1, v3, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const/4 v2, 0x0

    const v3, 0x7f09004f

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f09008d

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TMU"

    const-string v1, "Show resign-in Notification"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    :cond_0
    :goto_0
    return-void

    .line 198
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TMU"

    const-string v1, "Context is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
