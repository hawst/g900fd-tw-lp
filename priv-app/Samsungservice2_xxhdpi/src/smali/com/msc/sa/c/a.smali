.class public final Lcom/msc/sa/c/a;
.super Ljava/lang/Object;
.source "AuthWithTncMandatoryUtil.java"


# direct methods
.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/msc/sa/d/d;
    .locals 6

    .prologue
    .line 64
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATMU"

    const-string v1, "runAuthWithTncMandatoryValidation"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    new-instance v5, Lcom/msc/sa/d/d;

    invoke-direct {v5}, Lcom/msc/sa/d/d;-><init>()V

    .line 67
    invoke-virtual {v5, p4}, Lcom/msc/sa/d/d;->b(I)V

    .line 69
    if-nez p0, :cond_0

    .line 71
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATMU"

    const-string v1, "Context is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const-string v0, "SAC_0401"

    const-string v1, "Internal error occurred"

    invoke-virtual {v5, v0, v1}, Lcom/msc/sa/d/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    :goto_0
    return-object v5

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    .line 76
    invoke-static/range {v0 .. v5}, Lcom/msc/sa/c/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/msc/sa/d/d;)Z

    move-result v0

    .line 77
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "ATMU"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "reqeustAuthWithTncMandatory is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/msc/sa/d/d;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 92
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "ATMU"

    const-string v2, "requestAuthWithTncMandatory"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    if-nez p0, :cond_0

    .line 96
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "ATMU"

    const-string v2, "Context is null"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const-string v1, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-virtual {p5, v1, v2}, Lcom/msc/sa/d/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    :goto_0
    return v0

    .line 101
    :cond_0
    new-instance v7, Lcom/msc/a/c;

    invoke-direct {v7}, Lcom/msc/a/c;-><init>()V

    .line 102
    if-nez p4, :cond_3

    .line 104
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATMU"

    const-string v1, "Running Mode is AuthWithTncMandatory"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    invoke-virtual {v7, v3}, Lcom/msc/a/c;->a(Z)V

    .line 106
    invoke-virtual {v7, v3}, Lcom/msc/a/c;->b(Z)V

    .line 119
    :cond_1
    :goto_1
    if-eq p4, v3, :cond_2

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 122
    invoke-static {p0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 123
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 125
    invoke-virtual {v7}, Lcom/msc/a/c;->g()V

    .line 126
    invoke-virtual {v7, v0}, Lcom/msc/a/c;->b(Ljava/lang/String;)V

    .line 131
    :cond_2
    invoke-virtual {v7}, Lcom/msc/a/c;->a()V

    .line 132
    invoke-virtual {v7}, Lcom/msc/a/c;->e()V

    .line 133
    invoke-virtual {v7}, Lcom/msc/a/c;->f()V

    .line 134
    invoke-virtual {v7, p1}, Lcom/msc/a/c;->e(Ljava/lang/String;)V

    .line 135
    invoke-virtual {v7, p1}, Lcom/msc/a/c;->d(Ljava/lang/String;)V

    .line 136
    invoke-virtual {v7, p2}, Lcom/msc/a/c;->f(Ljava/lang/String;)V

    .line 137
    invoke-virtual {v7}, Lcom/msc/a/c;->b()V

    .line 138
    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/msc/a/c;->a(Ljava/lang/String;)V

    .line 139
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->l(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/msc/a/c;->c(Ljava/lang/String;)V

    .line 140
    invoke-static {p0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/msc/a/c;->j(Ljava/lang/String;)V

    .line 141
    invoke-virtual {v7}, Lcom/msc/a/c;->c()V

    .line 142
    invoke-virtual {v7}, Lcom/msc/a/c;->d()V

    .line 143
    invoke-virtual {v7, p3}, Lcom/msc/a/c;->k(Ljava/lang/String;)V

    .line 146
    :try_start_0
    invoke-static {p0}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/msc/a/c;->g(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    :goto_2
    const-string v0, "M"

    invoke-virtual {v7, v0}, Lcom/msc/a/c;->h(Ljava/lang/String;)V

    .line 153
    invoke-static {p0}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/msc/a/c;->i(Ljava/lang/String;)V

    .line 155
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 156
    new-instance v0, Lcom/msc/sa/c/b;

    move-object v1, p5

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p5

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/msc/sa/c/b;-><init>(Lcom/msc/sa/d/d;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/sa/d/d;I)V

    invoke-static {p0, v7, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/a/c;Lcom/msc/b/h;)J

    move-result-wide v0

    .line 187
    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 189
    invoke-virtual {p5}, Lcom/msc/sa/d/d;->a()Z

    move-result v0

    goto/16 :goto_0

    .line 107
    :cond_3
    if-ne p4, v3, :cond_4

    .line 109
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "ATMU"

    const-string v2, "Running Mode is Authentication"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    invoke-virtual {v7, v3}, Lcom/msc/a/c;->a(Z)V

    .line 111
    invoke-virtual {v7, v0}, Lcom/msc/a/c;->b(Z)V

    goto/16 :goto_1

    .line 112
    :cond_4
    const/4 v1, 0x2

    if-ne p4, v1, :cond_1

    .line 114
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "ATMU"

    const-string v2, "Running Mode is TncMandatory"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    invoke-virtual {v7, v0}, Lcom/msc/a/c;->a(Z)V

    .line 116
    invoke-virtual {v7, v3}, Lcom/msc/a/c;->b(Z)V

    goto/16 :goto_1

    .line 147
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method
