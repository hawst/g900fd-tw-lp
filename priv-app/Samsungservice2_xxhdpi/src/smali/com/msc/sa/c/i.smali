.class public final enum Lcom/msc/sa/c/i;
.super Ljava/lang/Enum;
.source "CompatibleAPIUtil.java"


# static fields
.field public static final enum a:Lcom/msc/sa/c/i;

.field public static final enum b:Lcom/msc/sa/c/i;

.field public static final enum c:Lcom/msc/sa/c/i;

.field public static final enum d:Lcom/msc/sa/c/i;

.field private static final synthetic e:[Lcom/msc/sa/c/i;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 944
    new-instance v0, Lcom/msc/sa/c/i;

    const-string v1, "BIG_PICTURE"

    invoke-direct {v0, v1, v2}, Lcom/msc/sa/c/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/msc/sa/c/i;->a:Lcom/msc/sa/c/i;

    new-instance v0, Lcom/msc/sa/c/i;

    const-string v1, "BIG_TEXT"

    invoke-direct {v0, v1, v3}, Lcom/msc/sa/c/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/msc/sa/c/i;->b:Lcom/msc/sa/c/i;

    new-instance v0, Lcom/msc/sa/c/i;

    const-string v1, "ACTION"

    invoke-direct {v0, v1, v4}, Lcom/msc/sa/c/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/msc/sa/c/i;->c:Lcom/msc/sa/c/i;

    new-instance v0, Lcom/msc/sa/c/i;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v5}, Lcom/msc/sa/c/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/msc/sa/c/i;->d:Lcom/msc/sa/c/i;

    .line 942
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/msc/sa/c/i;

    sget-object v1, Lcom/msc/sa/c/i;->a:Lcom/msc/sa/c/i;

    aput-object v1, v0, v2

    sget-object v1, Lcom/msc/sa/c/i;->b:Lcom/msc/sa/c/i;

    aput-object v1, v0, v3

    sget-object v1, Lcom/msc/sa/c/i;->c:Lcom/msc/sa/c/i;

    aput-object v1, v0, v4

    sget-object v1, Lcom/msc/sa/c/i;->d:Lcom/msc/sa/c/i;

    aput-object v1, v0, v5

    sput-object v0, Lcom/msc/sa/c/i;->e:[Lcom/msc/sa/c/i;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 942
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/msc/sa/c/i;
    .locals 1

    .prologue
    .line 942
    const-class v0, Lcom/msc/sa/c/i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/msc/sa/c/i;

    return-object v0
.end method

.method public static values()[Lcom/msc/sa/c/i;
    .locals 1

    .prologue
    .line 942
    sget-object v0, Lcom/msc/sa/c/i;->e:[Lcom/msc/sa/c/i;

    invoke-virtual {v0}, [Lcom/msc/sa/c/i;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/msc/sa/c/i;

    return-object v0
.end method
