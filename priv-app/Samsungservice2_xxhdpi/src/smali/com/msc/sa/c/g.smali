.class public final Lcom/msc/sa/c/g;
.super Ljava/lang/Object;
.source "CompatibleAPIUtil.java"


# instance fields
.field a:Ljava/util/ArrayList;

.field private final b:Ljava/lang/String;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Landroid/graphics/Bitmap;

.field private h:I

.field private i:Lcom/msc/sa/c/i;

.field private j:Landroid/app/PendingIntent;

.field private k:Landroid/app/PendingIntent;

.field private l:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 952
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 957
    const-string v0, "CMPNOTI"

    iput-object v0, p0, Lcom/msc/sa/c/g;->b:Ljava/lang/String;

    .line 962
    const/4 v0, 0x0

    iput v0, p0, Lcom/msc/sa/c/g;->c:I

    .line 966
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/sa/c/g;->d:Ljava/lang/String;

    .line 970
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/sa/c/g;->e:Ljava/lang/String;

    .line 974
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/sa/c/g;->f:Ljava/lang/String;

    .line 978
    iput-object v1, p0, Lcom/msc/sa/c/g;->g:Landroid/graphics/Bitmap;

    .line 982
    const/4 v0, 0x2

    iput v0, p0, Lcom/msc/sa/c/g;->h:I

    .line 986
    sget-object v0, Lcom/msc/sa/c/i;->d:Lcom/msc/sa/c/i;

    iput-object v0, p0, Lcom/msc/sa/c/g;->i:Lcom/msc/sa/c/i;

    .line 990
    iput-object v1, p0, Lcom/msc/sa/c/g;->j:Landroid/app/PendingIntent;

    .line 994
    iput-object v1, p0, Lcom/msc/sa/c/g;->k:Landroid/app/PendingIntent;

    .line 998
    iput-object v1, p0, Lcom/msc/sa/c/g;->l:Landroid/graphics/Bitmap;

    .line 1003
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/msc/sa/c/g;->a:Ljava/util/ArrayList;

    .line 1010
    return-void
.end method

.method private a(Landroid/app/Notification$Builder;)Landroid/app/Notification$Builder;
    .locals 5

    .prologue
    .line 1186
    iget-object v0, p0, Lcom/msc/sa/c/g;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->l()I

    move-result v0

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 1188
    iget-object v0, p0, Lcom/msc/sa/c/g;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1189
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1191
    iget-object v0, p0, Lcom/msc/sa/c/g;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/msc/sa/c/h;

    invoke-virtual {v0}, Lcom/msc/sa/c/h;->a()I

    move-result v3

    iget-object v0, p0, Lcom/msc/sa/c/g;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/msc/sa/c/h;

    invoke-virtual {v0}, Lcom/msc/sa/c/h;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/msc/sa/c/g;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/msc/sa/c/h;

    invoke-virtual {v0}, Lcom/msc/sa/c/h;->c()Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p1, v3, v4, v0}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 1189
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1196
    :cond_0
    iget-object v0, p0, Lcom/msc/sa/c/g;->j:Landroid/app/PendingIntent;

    invoke-virtual {p1, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 1197
    iget-object v0, p0, Lcom/msc/sa/c/g;->k:Landroid/app/PendingIntent;

    invoke-virtual {p1, v0}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 1200
    :cond_1
    iget-object v0, p0, Lcom/msc/sa/c/g;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1202
    return-object p1
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/app/Notification;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    const/16 v3, 0x10

    .line 1245
    if-nez p1, :cond_0

    .line 1249
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "CMPNOTI"

    const-string v2, "context is null"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1293
    :goto_0
    return-object v0

    .line 1253
    :cond_0
    iget v1, p0, Lcom/msc/sa/c/g;->c:I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/msc/sa/c/g;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_2

    .line 1255
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "CMPNOTI"

    const-string v2, "mandatory elements(small Icon ID or Titlte) not exist, return null"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1253
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 1259
    :cond_2
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->l()I

    move-result v0

    if-lt v0, v3, :cond_8

    .line 1261
    new-instance v0, Landroid/app/Notification$Builder;

    invoke-direct {v0, p1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 1263
    iget v1, p0, Lcom/msc/sa/c/g;->c:I

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/sa/c/g;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/sa/c/g;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 1265
    iget-object v1, p0, Lcom/msc/sa/c/g;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1267
    iget-object v1, p0, Lcom/msc/sa/c/g;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 1270
    :cond_3
    iget-object v1, p0, Lcom/msc/sa/c/g;->g:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_4

    .line 1272
    iget-object v1, p0, Lcom/msc/sa/c/g;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    .line 1275
    :cond_4
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->l()I

    move-result v1

    if-lt v1, v3, :cond_5

    .line 1277
    iget v1, p0, Lcom/msc/sa/c/g;->h:I

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    .line 1280
    :cond_5
    invoke-direct {p0, v0}, Lcom/msc/sa/c/g;->a(Landroid/app/Notification$Builder;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 1282
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->l()I

    move-result v1

    if-ge v1, v3, :cond_6

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v0

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lcom/msc/sa/c/g;->i:Lcom/msc/sa/c/i;

    sget-object v2, Lcom/msc/sa/c/i;->a:Lcom/msc/sa/c/i;

    if-ne v1, v2, :cond_7

    iget-object v1, p0, Lcom/msc/sa/c/g;->l:Landroid/graphics/Bitmap;

    if-nez v1, :cond_7

    sget-object v1, Lcom/msc/sa/c/i;->d:Lcom/msc/sa/c/i;

    iput-object v1, p0, Lcom/msc/sa/c/g;->i:Lcom/msc/sa/c/i;

    :cond_7
    sget-object v1, Lcom/msc/sa/c/e;->a:[I

    iget-object v2, p0, Lcom/msc/sa/c/g;->i:Lcom/msc/sa/c/i;

    invoke-virtual {v2}, Lcom/msc/sa/c/i;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_0
    new-instance v1, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v1, v0}, Landroid/app/Notification$BigTextStyle;-><init>(Landroid/app/Notification$Builder;)V

    iget-object v0, p0, Lcom/msc/sa/c/g;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Notification$BigTextStyle;->build()Landroid/app/Notification;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_1
    new-instance v1, Landroid/app/Notification$BigPictureStyle;

    invoke-direct {v1, v0}, Landroid/app/Notification$BigPictureStyle;-><init>(Landroid/app/Notification$Builder;)V

    iget-object v0, p0, Lcom/msc/sa/c/g;->l:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Landroid/app/Notification$BigPictureStyle;->bigPicture(Landroid/graphics/Bitmap;)Landroid/app/Notification$BigPictureStyle;

    move-result-object v0

    iget-object v1, p0, Lcom/msc/sa/c/g;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/Notification$BigPictureStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigPictureStyle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Notification$BigPictureStyle;->build()Landroid/app/Notification;

    move-result-object v0

    goto/16 :goto_0

    .line 1286
    :cond_8
    new-instance v0, Landroid/app/Notification;

    iget v1, p0, Lcom/msc/sa/c/g;->c:I

    iget-object v2, p0, Lcom/msc/sa/c/g;->f:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v4, v5}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 1287
    iget-object v1, p0, Lcom/msc/sa/c/g;->j:Landroid/app/PendingIntent;

    iput-object v1, v0, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 1288
    iget-object v1, p0, Lcom/msc/sa/c/g;->k:Landroid/app/PendingIntent;

    iput-object v1, v0, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    .line 1289
    iget-object v1, p0, Lcom/msc/sa/c/g;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/msc/sa/c/g;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/msc/sa/c/g;->j:Landroid/app/PendingIntent;

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    goto/16 :goto_0

    .line 1282
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1140
    const/4 v0, 0x2

    iput v0, p0, Lcom/msc/sa/c/g;->h:I

    .line 1141
    return-void
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 1086
    iput p1, p0, Lcom/msc/sa/c/g;->c:I

    .line 1087
    return-void
.end method

.method public final a(ILjava/lang/String;Landroid/app/PendingIntent;)V
    .locals 2

    .prologue
    .line 1076
    iget-object v0, p0, Lcom/msc/sa/c/g;->a:Ljava/util/ArrayList;

    new-instance v1, Lcom/msc/sa/c/h;

    invoke-direct {v1, p1, p2, p3}, Lcom/msc/sa/c/h;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1078
    return-void
.end method

.method public final a(Landroid/app/PendingIntent;)V
    .locals 0

    .prologue
    .line 1158
    iput-object p1, p0, Lcom/msc/sa/c/g;->j:Landroid/app/PendingIntent;

    .line 1159
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 1095
    iput-object p1, p0, Lcom/msc/sa/c/g;->l:Landroid/graphics/Bitmap;

    .line 1096
    return-void
.end method

.method public final a(Lcom/msc/sa/c/i;)V
    .locals 0

    .prologue
    .line 1149
    iput-object p1, p0, Lcom/msc/sa/c/g;->i:Lcom/msc/sa/c/i;

    .line 1150
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1104
    iput-object p1, p0, Lcom/msc/sa/c/g;->d:Ljava/lang/String;

    .line 1105
    return-void
.end method

.method public final b(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 1131
    iput-object p1, p0, Lcom/msc/sa/c/g;->g:Landroid/graphics/Bitmap;

    .line 1132
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1113
    iput-object p1, p0, Lcom/msc/sa/c/g;->e:Ljava/lang/String;

    .line 1114
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1122
    iput-object p1, p0, Lcom/msc/sa/c/g;->f:Ljava/lang/String;

    .line 1123
    return-void
.end method
