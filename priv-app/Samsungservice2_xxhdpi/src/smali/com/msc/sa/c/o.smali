.class public final Lcom/msc/sa/c/o;
.super Ljava/lang/Object;
.source "TncMandatoryUtil.java"


# direct methods
.method private static a(Lcom/msc/a/i;)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 387
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TMU"

    const-string v1, "getCheckList"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    const/4 v0, 0x0

    .line 389
    invoke-virtual {p0}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->d()Z

    move-result v1

    if-eq v1, v3, :cond_0

    invoke-virtual {p0}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->e()Z

    move-result v1

    if-ne v1, v3, :cond_1

    .line 391
    :cond_0
    const/4 v0, 0x2

    .line 392
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "TMU"

    const-string v2, "requirement : Acceptance of TNC"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    :cond_1
    invoke-virtual {p0}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->g()Z

    move-result v1

    if-ne v1, v3, :cond_2

    .line 396
    or-int/lit8 v0, v0, 0x4

    .line 397
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "TMU"

    const-string v2, "requirement : Name verification"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    :cond_2
    invoke-virtual {p0}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->h()Z

    move-result v1

    if-ne v1, v3, :cond_3

    .line 401
    or-int/lit8 v0, v0, 0x8

    .line 402
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "TMU"

    const-string v2, "requirement : Email validation"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    :cond_3
    invoke-virtual {p0}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/kd;->b()I

    move-result v1

    if-lez v1, :cond_4

    .line 406
    or-int/lit8 v0, v0, 0x10

    .line 407
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "TMU"

    const-string v2, "requirement : Mandatory input"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    :cond_4
    return v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/msc/sa/d/h;
    .locals 2

    .prologue
    .line 65
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TMU"

    const-string v1, "runTncMandatoryValidation [default SHOW NOTI]"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, p3, v0}, Lcom/msc/sa/c/o;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/msc/sa/d/h;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/msc/sa/d/h;
    .locals 7

    .prologue
    .line 83
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TMU"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "runTncMandatoryValidation showNoti :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    new-instance v6, Lcom/msc/sa/d/h;

    invoke-direct {v6}, Lcom/msc/sa/d/h;-><init>()V

    .line 87
    if-nez p0, :cond_0

    .line 89
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TMU"

    const-string v1, "Context is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const-string v0, "SAC_0401"

    const-string v1, "Internal error occurred"

    invoke-virtual {v6, v0, v1}, Lcom/msc/sa/d/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    :goto_0
    return-object v6

    .line 95
    :cond_0
    invoke-static {p0, p1}, Lcom/msc/openprovider/b;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/osp/app/util/ad;->a(I)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 97
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TMU"

    const-string v1, "Success to validate TncMandatory with cache data."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    invoke-virtual {v6}, Lcom/msc/sa/d/h;->b()V

    goto :goto_0

    .line 103
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TMU"

    const-string v1, "start request TncMandatory"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const/4 v0, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/o;->b(ILandroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/msc/sa/d/h;)Z

    move-result v0

    .line 105
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "TMU"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "reqeustTncMandatory is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(ILandroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/msc/sa/d/h;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 36
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "TMU"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "handleTncMandatorySuccess, type :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_9

    :try_start_0
    new-instance v8, Lcom/msc/a/i;

    invoke-direct {v8}, Lcom/msc/a/i;-><init>()V

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {p1, p2, p4, v8}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/a/i;)V

    if-nez p0, :cond_6

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "TMU"

    const-string v3, "isCheckListSuccess"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v8}, Lcom/msc/sa/c/o;->a(Lcom/msc/a/i;)I

    move-result v2

    if-lez v2, :cond_1

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "TMU"

    const-string v3, "CheckList - Fail"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    if-ne v0, v1, :cond_2

    invoke-virtual {p6}, Lcom/msc/sa/d/h;->b()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TMU"

    const-string v2, "CheckList - Success"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_2
    if-ne p5, v1, :cond_3

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TMU"

    const-string v1, "showMoreCheckListRemainNotification"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_5

    invoke-static {p1}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {p1}, Lcom/osp/app/util/ad;->j(Landroid/content/Context;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TMU"

    const-string v1, "showMoreCheckListRemainNotificationFail Samsung account is not signed in."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_2
    invoke-static {v8}, Lcom/msc/sa/c/o;->a(Lcom/msc/a/i;)I

    move-result v0

    invoke-virtual {p6, v0}, Lcom/msc/sa/d/h;->a(I)V

    const-string v0, "SAC_0204"

    const-string v1, "The certification process must be completed before you use your Samsung account"

    invoke-virtual {p6, v0, v1}, Lcom/msc/sa/d/h;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p6, v1, v0}, Lcom/msc/sa/d/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    :try_start_1
    invoke-static {p1}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v8}, Lcom/msc/sa/c/o;->a(Lcom/msc/a/i;)I

    move-result v1

    invoke-static {p1}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p1

    move-object v3, p2

    move-object v4, p3

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const/4 v2, 0x0

    const v0, 0x7f090137

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f090149

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TMU"

    const-string v1, "Show more check list remain Notification to get token"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TMU"

    const-string v1, "Context is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    if-ne p0, v1, :cond_0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "TMU"

    const-string v3, "isCheckDisclaimerSuccess"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/msc/a/g;->k()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "TMU"

    const-string v3, "CheckDisclaimer - Fail"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_3
    if-ne v0, v1, :cond_8

    invoke-virtual {p6}, Lcom/msc/sa/d/h;->b()V

    goto/16 :goto_1

    :cond_7
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TMU"

    const-string v2, "CheckDisclaimer - Success"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_3

    :cond_8
    const-string v0, "SAC_0206"

    const-string v1, "The disclaimer agreement must be completed to use Samsung account"

    invoke-virtual {p6, v0, v1}, Lcom/msc/sa/d/h;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    :cond_9
    const-string v0, "SAC_0401"

    const-string v1, "Internal error occurred"

    invoke-virtual {p6, v0, v1}, Lcom/msc/sa/d/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/msc/sa/d/h;
    .locals 7

    .prologue
    .line 116
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TMU"

    const-string v1, "run3rdPartyDisclaimerCheck"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    new-instance v6, Lcom/msc/sa/d/h;

    invoke-direct {v6}, Lcom/msc/sa/d/h;-><init>()V

    .line 120
    if-nez p0, :cond_0

    .line 122
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TMU"

    const-string v1, "Context is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v0, "SAC_0401"

    const-string v1, "Internal error occurred"

    invoke-virtual {v6, v0, v1}, Lcom/msc/sa/d/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    :goto_0
    return-object v6

    .line 127
    :cond_0
    const/4 v0, 0x1

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/o;->b(ILandroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/msc/sa/d/h;)Z

    move-result v0

    .line 128
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "TMU"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "reqeustTncMandatory is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static b(ILandroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/msc/sa/d/h;)Z
    .locals 9

    .prologue
    .line 146
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TMU"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "reqeustTncMandatory, type :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    if-eqz p1, :cond_2

    .line 150
    invoke-static {p1}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 151
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p1}, Lcom/osp/common/util/i;->l(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 153
    new-instance v8, Lcom/msc/a/h;

    invoke-direct {v8}, Lcom/msc/a/h;-><init>()V

    .line 154
    invoke-static {p1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/msc/a/h;->d(Ljava/lang/String;)V

    .line 155
    invoke-virtual {v8}, Lcom/msc/a/h;->b()V

    .line 156
    invoke-virtual {v8, p2}, Lcom/msc/a/h;->h(Ljava/lang/String;)V

    .line 157
    invoke-virtual {v8, p4}, Lcom/msc/a/h;->i(Ljava/lang/String;)V

    .line 158
    invoke-virtual {v8, p2}, Lcom/msc/a/h;->j(Ljava/lang/String;)V

    .line 159
    invoke-virtual {v8, v1}, Lcom/msc/a/h;->e(Ljava/lang/String;)V

    .line 160
    invoke-virtual {v8, v0}, Lcom/msc/a/h;->k(Ljava/lang/String;)V

    .line 161
    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    .line 163
    invoke-virtual {v8}, Lcom/msc/a/h;->c()V

    .line 165
    :cond_0
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 167
    invoke-static {p1}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 168
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 170
    const-string v1, "Y"

    invoke-virtual {v8, v1}, Lcom/msc/a/h;->b(Ljava/lang/String;)V

    .line 171
    invoke-virtual {v8, v0}, Lcom/msc/a/h;->c(Ljava/lang/String;)V

    .line 175
    :cond_1
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 176
    new-instance v0, Lcom/msc/sa/c/p;

    move-object v1, p6

    move v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/msc/sa/c/p;-><init>(Lcom/msc/sa/d/h;ILandroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLcom/msc/sa/d/h;)V

    invoke-static {p1, v8, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/a/h;Lcom/msc/b/h;)J

    move-result-wide v0

    .line 204
    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 210
    :goto_0
    invoke-virtual {p6}, Lcom/msc/sa/d/h;->a()Z

    move-result v0

    return v0

    .line 207
    :cond_2
    const-string v0, "SAC_0401"

    const-string v1, "Internal error occurred"

    invoke-virtual {p6, v0, v1}, Lcom/msc/sa/d/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
