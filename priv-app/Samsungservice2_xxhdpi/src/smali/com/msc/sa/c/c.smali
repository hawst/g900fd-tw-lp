.class Lcom/msc/sa/c/c;
.super Ljava/lang/Object;
.source "AuthWithTncMandatoryUtil.java"

# interfaces
.implements Lcom/msc/b/h;


# instance fields
.field private final a:Lcom/msc/sa/d/d;


# direct methods
.method public constructor <init>(Lcom/msc/sa/d/d;)V
    .locals 2

    .prologue
    .line 428
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 429
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATMU"

    const-string v1, "ServerResponseListener"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    iput-object p1, p0, Lcom/msc/sa/c/c;->a:Lcom/msc/sa/d/d;

    .line 431
    return-void
.end method


# virtual methods
.method public a(Lcom/msc/b/c;)V
    .locals 3

    .prologue
    .line 435
    if-nez p1, :cond_0

    .line 437
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATMU"

    const-string v1, "onRequestSuccess response Message is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    iget-object v0, p0, Lcom/msc/sa/c/c;->a:Lcom/msc/sa/d/d;

    const-string v1, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/d/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    :cond_0
    return-void
.end method

.method public b(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 450
    if-nez p1, :cond_0

    .line 452
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATMU"

    const-string v1, "onRequestfail responseMessage is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    iget-object v0, p0, Lcom/msc/sa/c/c;->a:Lcom/msc/sa/d/d;

    const-string v1, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/d/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    :goto_0
    return-void

    .line 457
    :cond_0
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v0

    .line 458
    invoke-virtual {p1}, Lcom/msc/b/c;->f()Ljava/lang/Exception;

    move-result-object v1

    .line 460
    if-eqz v1, :cond_2

    .line 462
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATMU"

    const-string v2, "Server request fail. Exception occured"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0, v1}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    .line 466
    invoke-virtual {v0}, Lcom/msc/c/f;->d()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 468
    iget-object v0, p0, Lcom/msc/sa/c/c;->a:Lcom/msc/sa/d/d;

    const-string v1, "SAC_0302"

    const-string v2, "Error occurred while connecting to SSL"

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/d/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATMU"

    const-string v1, "SSL error occured"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 473
    :cond_1
    iget-object v1, p0, Lcom/msc/sa/c/c;->a:Lcom/msc/sa/d/d;

    invoke-virtual {v0}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/msc/sa/d/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 477
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "ATMU"

    const-string v2, "Server request fail. Internal error occured"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    if-nez v0, :cond_3

    .line 481
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATMU"

    const-string v1, "content is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    iget-object v0, p0, Lcom/msc/sa/c/c;->a:Lcom/msc/sa/d/d;

    const-string v1, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/d/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 486
    :cond_3
    iget-object v1, p0, Lcom/msc/sa/c/c;->a:Lcom/msc/sa/d/d;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "ATMU"

    const-string v3, "parseFailErrorResult"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/msc/c/f;

    invoke-direct {v2}, Lcom/msc/c/f;-><init>()V

    const-string v3, "from_json"

    invoke-virtual {v2, v3, v0}, Lcom/msc/c/f;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATMU"

    const-string v2, "fail to parse error result"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-virtual {v1, v0, v2}, Lcom/msc/sa/d/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v2}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/msc/sa/d/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final c(Lcom/msc/b/c;)V
    .locals 0

    .prologue
    .line 445
    return-void
.end method
