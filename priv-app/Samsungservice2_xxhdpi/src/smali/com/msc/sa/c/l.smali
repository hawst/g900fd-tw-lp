.class final Lcom/msc/sa/c/l;
.super Lcom/msc/sa/c/n;
.source "SignatureCheckUtil.java"


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/msc/sa/c/m;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/sa/c/m;)V
    .locals 0

    .prologue
    .line 302
    iput-object p2, p0, Lcom/msc/sa/c/l;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/msc/sa/c/l;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/msc/sa/c/l;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/msc/sa/c/l;->d:Lcom/msc/sa/c/m;

    invoke-direct {p0, p1}, Lcom/msc/sa/c/n;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 306
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/sa/c/n;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 308
    if-nez p1, :cond_1

    .line 351
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 313
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCHU"

    const-string v1, "requestPackageSignatureInfoList Servier Request Success"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v0

    .line 318
    invoke-static {}, Lcom/msc/sa/c/j;->a()Z

    .line 321
    iget-object v1, p0, Lcom/msc/sa/c/l;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 322
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SCHU"

    const-string v2, "Save Signature"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 327
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v0}, Lcom/msc/c/h;->A(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 329
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v1, p0, Lcom/msc/sa/c/l;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/msc/sa/c/l;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/msc/sa/c/l;->c:Ljava/lang/String;

    invoke-static {v1, v0, v2, v3}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 331
    iget-object v1, p0, Lcom/msc/sa/c/l;->d:Lcom/msc/sa/c/m;

    invoke-virtual {v1, v0}, Lcom/msc/sa/c/m;->a(Z)V

    .line 332
    if-nez v0, :cond_0

    .line 334
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCHU"

    const-string v1, "checkSignatureValidation Server Request fail"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "errorMessage-The signature of this application is not registered with the server."

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 338
    :catch_0
    move-exception v0

    .line 340
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 341
    instance-of v0, v0, Landroid/content/pm/PackageManager$NameNotFoundException;

    if-eqz v0, :cond_2

    .line 343
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCHU"

    const-string v1, "Try Catch NameNotFoundException"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    iget-object v0, p0, Lcom/msc/sa/c/l;->d:Lcom/msc/sa/c/m;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/msc/sa/c/m;->a(Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 306
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 347
    :cond_2
    :try_start_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCHU"

    const-string v1, "Try Catch Exception"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    iget-object v0, p0, Lcom/msc/sa/c/l;->d:Lcom/msc/sa/c/m;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/msc/sa/c/m;->a(Z)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 2

    .prologue
    .line 355
    invoke-super {p0, p1}, Lcom/msc/sa/c/n;->b(Lcom/msc/b/c;)V

    .line 357
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCHU"

    const-string v1, "requestPackageSignatureInfoList Request FAIL"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    iget-object v0, p0, Lcom/msc/sa/c/l;->d:Lcom/msc/sa/c/m;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/msc/sa/c/m;->a(Z)V

    .line 359
    return-void
.end method
