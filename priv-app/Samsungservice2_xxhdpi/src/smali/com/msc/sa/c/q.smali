.class Lcom/msc/sa/c/q;
.super Ljava/lang/Object;
.source "TncMandatoryUtil.java"

# interfaces
.implements Lcom/msc/b/h;


# instance fields
.field g:Lcom/msc/sa/d/h;


# direct methods
.method constructor <init>(Lcom/msc/sa/d/h;)V
    .locals 0

    .prologue
    .line 460
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 461
    iput-object p1, p0, Lcom/msc/sa/c/q;->g:Lcom/msc/sa/d/h;

    .line 462
    return-void
.end method


# virtual methods
.method public a(Lcom/msc/b/c;)V
    .locals 3

    .prologue
    .line 466
    if-nez p1, :cond_0

    .line 468
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TMU"

    const-string v1, "onRequestSuccess response Message is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    iget-object v0, p0, Lcom/msc/sa/c/q;->g:Lcom/msc/sa/d/h;

    if-eqz v0, :cond_0

    .line 471
    iget-object v0, p0, Lcom/msc/sa/c/q;->g:Lcom/msc/sa/d/h;

    const-string v1, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/d/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    :cond_0
    return-void
.end method

.method public b(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 484
    if-nez p1, :cond_1

    .line 486
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TMU"

    const-string v1, "onRequestfail responseMessage is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    iget-object v0, p0, Lcom/msc/sa/c/q;->g:Lcom/msc/sa/d/h;

    if-eqz v0, :cond_0

    .line 489
    iget-object v0, p0, Lcom/msc/sa/c/q;->g:Lcom/msc/sa/d/h;

    const-string v1, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/d/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    :cond_0
    :goto_0
    return-void

    .line 493
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v0

    .line 494
    invoke-virtual {p1}, Lcom/msc/b/c;->f()Ljava/lang/Exception;

    move-result-object v1

    .line 496
    if-eqz v1, :cond_4

    .line 498
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TMU"

    const-string v2, "Server request fail. Exception occured"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0, v1}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    .line 502
    invoke-virtual {v0}, Lcom/msc/c/f;->d()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 504
    iget-object v0, p0, Lcom/msc/sa/c/q;->g:Lcom/msc/sa/d/h;

    if-eqz v0, :cond_2

    .line 506
    iget-object v0, p0, Lcom/msc/sa/c/q;->g:Lcom/msc/sa/d/h;

    const-string v1, "SAC_0302"

    const-string v2, "Error occurred while connecting to SSL"

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/d/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TMU"

    const-string v1, "SSL error occured"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 511
    :cond_3
    iget-object v1, p0, Lcom/msc/sa/c/q;->g:Lcom/msc/sa/d/h;

    if-eqz v1, :cond_0

    .line 513
    iget-object v1, p0, Lcom/msc/sa/c/q;->g:Lcom/msc/sa/d/h;

    invoke-virtual {v0}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/msc/sa/d/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 519
    :cond_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "TMU"

    const-string v2, "Server request fail. Internal error occured"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    if-nez v0, :cond_5

    .line 523
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TMU"

    const-string v1, "content is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    iget-object v0, p0, Lcom/msc/sa/c/q;->g:Lcom/msc/sa/d/h;

    if-eqz v0, :cond_0

    .line 526
    iget-object v0, p0, Lcom/msc/sa/c/q;->g:Lcom/msc/sa/d/h;

    const-string v1, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/d/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 530
    :cond_5
    iget-object v1, p0, Lcom/msc/sa/c/q;->g:Lcom/msc/sa/d/h;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "TMU"

    const-string v3, "parseFailErrorResult"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/msc/c/f;

    invoke-direct {v2}, Lcom/msc/c/f;-><init>()V

    const-string v3, "from_xml"

    invoke-virtual {v2, v3, v0}, Lcom/msc/c/f;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TMU"

    const-string v2, "fail to parse error result"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v1, :cond_0

    const-string v0, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-virtual {v1, v0, v2}, Lcom/msc/sa/d/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    if-eqz v1, :cond_0

    invoke-virtual {v2}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/msc/sa/d/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final c(Lcom/msc/b/c;)V
    .locals 0

    .prologue
    .line 479
    return-void
.end method
