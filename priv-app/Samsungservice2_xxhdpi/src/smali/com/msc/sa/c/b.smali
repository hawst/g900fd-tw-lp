.class final Lcom/msc/sa/c/b;
.super Lcom/msc/sa/c/c;
.source "AuthWithTncMandatoryUtil.java"


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Z

.field final synthetic e:Lcom/msc/sa/d/d;

.field final synthetic f:I


# direct methods
.method constructor <init>(Lcom/msc/sa/d/d;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/sa/d/d;I)V
    .locals 1

    .prologue
    .line 156
    iput-object p2, p0, Lcom/msc/sa/c/b;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/msc/sa/c/b;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/msc/sa/c/b;->c:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/msc/sa/c/b;->d:Z

    iput-object p5, p0, Lcom/msc/sa/c/b;->e:Lcom/msc/sa/d/d;

    iput p6, p0, Lcom/msc/sa/c/b;->f:I

    invoke-direct {p0, p1}, Lcom/msc/sa/c/c;-><init>(Lcom/msc/sa/d/d;)V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 13

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 160
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/sa/c/c;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 162
    if-nez p1, :cond_1

    .line 170
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 167
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v1

    .line 169
    iget-object v0, p0, Lcom/msc/sa/c/b;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/msc/sa/c/b;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/msc/sa/c/b;->c:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/msc/sa/c/b;->d:Z

    iget-object v10, p0, Lcom/msc/sa/c/b;->e:Lcom/msc/sa/d/d;

    iget v11, p0, Lcom/msc/sa/c/b;->f:I

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v7, "ATMU"

    const-string v8, "handleAuthWithTncMandatorySuccess"

    invoke-static {v7, v8}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v0, :cond_2

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATMU"

    const-string v1, "Context is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "SAC_0401"

    const-string v1, "Internal error occurred"

    invoke-virtual {v10, v0, v1}, Lcom/msc/sa/d/d;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 160
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 169
    :cond_2
    :try_start_2
    new-instance v7, Lcom/msc/a/d;

    invoke-direct {v7}, Lcom/msc/a/d;-><init>()V

    new-instance v8, Lcom/msc/a/i;

    invoke-direct {v8}, Lcom/msc/a/i;-><init>()V

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v0, v3, v1, v7, v8}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/a/d;Lcom/msc/a/i;)V

    invoke-virtual {v10, v8}, Lcom/msc/sa/d/d;->a(Lcom/msc/a/i;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "ATMU"

    const-string v9, "getCheckList"

    invoke-static {v1, v9}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->d()Z

    move-result v1

    if-eq v1, v5, :cond_3

    invoke-virtual {v8}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->e()Z

    move-result v1

    if-ne v1, v5, :cond_c

    :cond_3
    const/4 v1, 0x2

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v9, "ATMU"

    const-string v12, "requirement : Acceptance of TNC"

    invoke-static {v9, v12}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    invoke-virtual {v8}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v9

    invoke-virtual {v9}, Lcom/msc/a/g;->g()Z

    move-result v9

    if-ne v9, v5, :cond_4

    or-int/lit8 v1, v1, 0x4

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v9, "ATMU"

    const-string v12, "requirement : Name verification"

    invoke-static {v9, v12}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v8}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v9

    invoke-virtual {v9}, Lcom/msc/a/g;->h()Z

    move-result v9

    if-ne v9, v5, :cond_5

    or-int/lit8 v1, v1, 0x8

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v9, "ATMU"

    const-string v12, "requirement : Email validation"

    invoke-static {v9, v12}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v8}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v8

    invoke-virtual {v8}, Lcom/osp/app/signin/kd;->b()I

    move-result v8

    if-lez v8, :cond_6

    or-int/lit8 v1, v1, 0x10

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v8, "ATMU"

    const-string v9, "requirement : Mandatory input"

    invoke-static {v8, v9}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v8, "ATMU"

    const-string v9, "isCheckListSuccess"

    invoke-static {v8, v9}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-lez v1, :cond_8

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v8, "ATMU"

    const-string v9, "CheckList - Fail"

    invoke-static {v8, v9}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    if-nez v2, :cond_b

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "ATMU"

    const-string v7, "Certification process is not completed! checklist > 0"

    invoke-static {v2, v7}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-ne v6, v5, :cond_7

    invoke-static {v0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "ATMU"

    const-string v5, "showCertificationNotification"

    invoke-static {v2, v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v0, :cond_9

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "ATMU"

    const-string v3, "showNotiForCertificationFail context is null"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    :goto_3
    const-string v2, "SAC_0204"

    const-string v3, "The certification process must be completed before you use your Samsung account"

    invoke-virtual {v10, v2, v3}, Lcom/msc/sa/d/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v1}, Lcom/msc/sa/d/d;->a(I)V

    :goto_4
    if-nez v11, :cond_0

    sget-object v1, Lcom/osp/app/util/af;->c:Lcom/osp/app/util/af;

    invoke-static {v0, v1}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Lcom/osp/app/util/af;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v1, v0}, Lcom/msc/sa/d/d;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    :cond_8
    :try_start_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "ATMU"

    const-string v8, "CheckList - Success"

    invoke-static {v2, v8}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v5

    goto :goto_2

    :cond_9
    invoke-static {v0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_a

    invoke-static {v0}, Lcom/osp/app/util/ad;->j(Landroid/content/Context;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "ATMU"

    const-string v3, "showNotiForCertificationFail Samsung account is not signed in."

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_a
    invoke-static {v0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    invoke-static {v0, v3, v2, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    const/4 v4, 0x0

    const v2, 0x7f090137

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v2, 0x7f090149

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v7

    const v8, 0x132df82

    move-object v2, v0

    invoke-static/range {v2 .. v8}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "ATMU"

    const-string v3, "Show more process remain Notification to get token"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_b
    invoke-virtual {v7}, Lcom/msc/a/d;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Lcom/msc/a/d;->j()J

    move-result-wide v3

    invoke-virtual {v7}, Lcom/msc/a/d;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7}, Lcom/msc/a/d;->k()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    move-object v1, v10

    invoke-virtual/range {v1 .. v9}, Lcom/msc/sa/d/d;->a(Ljava/lang/String;JLjava/lang/String;JJ)V

    invoke-virtual {v10}, Lcom/msc/sa/d/d;->b()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_4

    :cond_c
    move v1, v2

    goto/16 :goto_1
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 174
    invoke-super {p0, p1}, Lcom/msc/sa/c/c;->b(Lcom/msc/b/c;)V

    .line 176
    if-nez p1, :cond_1

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 181
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v1

    .line 183
    iget-object v0, p0, Lcom/msc/sa/c/b;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/msc/sa/c/b;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/msc/sa/c/b;->c:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/msc/sa/c/b;->d:Z

    iget-object v5, p0, Lcom/msc/sa/c/b;->e:Lcom/msc/sa/d/d;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v6, "ATMU"

    const-string v7, "handleAuthWithTncMandatoryFail"

    invoke-static {v6, v7}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v1, :cond_0

    const-string v1, "AUT_1302"

    invoke-virtual {v5}, Lcom/msc/sa/d/d;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eq v1, v8, :cond_2

    const-string v1, "USR_3174"

    invoke-virtual {v5}, Lcom/msc/sa/d/d;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eq v1, v8, :cond_2

    const-string v1, "AUT_1830"

    invoke-virtual {v5}, Lcom/msc/sa/d/d;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v8, :cond_0

    :cond_2
    invoke-static {v0}, Lcom/msc/c/c;->g(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/msc/c/e;->a(Landroid/content/Context;)Z

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "ATMU"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Lcom/msc/sa/d/d;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " error occurred"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "SAC_0402"

    const-string v6, "Auth token expired"

    invoke-virtual {v5, v1, v6}, Lcom/msc/sa/d/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-ne v4, v8, :cond_0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "ATMU"

    const-string v4, "showResigninNotification"

    invoke-static {v1, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v0, :cond_3

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATMU"

    const-string v1, "showResignNotifation context is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.msc.action.samsungaccount.resignin"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v4, 0x10000000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v4, "client_id"

    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "client_secret"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "OSP_VER"

    const-string v3, "OSP_02"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "email_id"

    invoke-static {v0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v0, v2, v1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const/4 v2, 0x0

    const v3, 0x7f09004f

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f09008d

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATMU"

    const-string v1, "Show resign-in Notification"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
