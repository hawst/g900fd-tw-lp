.class public final Lcom/msc/sa/myprofile/d;
.super Landroid/webkit/WebChromeClient;
.source "MyProfileWebView.java"


# instance fields
.field final synthetic a:Lcom/msc/sa/myprofile/MyProfileWebView;


# direct methods
.method public constructor <init>(Lcom/msc/sa/myprofile/MyProfileWebView;)V
    .locals 0

    .prologue
    .line 642
    iput-object p1, p0, Lcom/msc/sa/myprofile/d;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method


# virtual methods
.method public final openFileChooser(Landroid/webkit/ValueCallback;)V
    .locals 1

    .prologue
    .line 646
    const-string v0, ""

    invoke-virtual {p0, p1, v0}, Lcom/msc/sa/myprofile/d;->openFileChooser(Landroid/webkit/ValueCallback;Ljava/lang/String;)V

    .line 647
    return-void
.end method

.method public final openFileChooser(Landroid/webkit/ValueCallback;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 651
    iget-object v0, p0, Lcom/msc/sa/myprofile/d;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0, p1}, Lcom/msc/sa/myprofile/MyProfileWebView;->a(Lcom/msc/sa/myprofile/MyProfileWebView;Landroid/webkit/ValueCallback;)Landroid/webkit/ValueCallback;

    .line 652
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 653
    const-string v1, "android.intent.category.OPENABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 654
    const-string v1, "*/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 655
    iget-object v1, p0, Lcom/msc/sa/myprofile/d;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    const/16 v2, 0x3e9

    invoke-virtual {v1, v0, v2}, Lcom/msc/sa/myprofile/MyProfileWebView;->startActivityForResult(Landroid/content/Intent;I)V

    .line 656
    return-void
.end method

.method public final openFileChooser(Landroid/webkit/ValueCallback;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 660
    const-string v0, ""

    invoke-virtual {p0, p1, v0}, Lcom/msc/sa/myprofile/d;->openFileChooser(Landroid/webkit/ValueCallback;Ljava/lang/String;)V

    .line 661
    return-void
.end method
