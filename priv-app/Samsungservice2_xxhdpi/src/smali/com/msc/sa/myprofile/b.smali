.class final Lcom/msc/sa/myprofile/b;
.super Ljava/lang/Object;
.source "MyProfileWebView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# instance fields
.field final synthetic a:Lcom/msc/sa/myprofile/MyProfileWebView;


# direct methods
.method constructor <init>(Lcom/msc/sa/myprofile/MyProfileWebView;)V
    .locals 0

    .prologue
    .line 476
    iput-object p1, p0, Lcom/msc/sa/myprofile/b;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 480
    const/4 v1, 0x4

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 482
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "MPW"

    const-string v2, "ProgressDialog - back key pressed."

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    :try_start_0
    iget-object v1, p0, Lcom/msc/sa/myprofile/b;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-virtual {v1}, Lcom/msc/sa/myprofile/MyProfileWebView;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/msc/sa/myprofile/b;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v1}, Lcom/msc/sa/myprofile/MyProfileWebView;->c(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/msc/sa/myprofile/b;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v1}, Lcom/msc/sa/myprofile/MyProfileWebView;->c(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 488
    iget-object v1, p0, Lcom/msc/sa/myprofile/b;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v1}, Lcom/msc/sa/myprofile/MyProfileWebView;->c(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 489
    iget-object v1, p0, Lcom/msc/sa/myprofile/b;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v1}, Lcom/msc/sa/myprofile/MyProfileWebView;->d(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/app/ProgressDialog;

    .line 490
    iget-object v1, p0, Lcom/msc/sa/myprofile/b;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v1}, Lcom/msc/sa/myprofile/MyProfileWebView;->a(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebView;->stopLoading()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 497
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/msc/sa/myprofile/b;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v1}, Lcom/msc/sa/myprofile/MyProfileWebView;->a(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 499
    iget-object v0, p0, Lcom/msc/sa/myprofile/b;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->a(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 506
    :goto_1
    const/4 v0, 0x1

    .line 509
    :cond_1
    return v0

    .line 492
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 502
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "MPW"

    const-string v2, "cannot go back."

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    iget-object v1, p0, Lcom/msc/sa/myprofile/b;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    iget-object v2, p0, Lcom/msc/sa/myprofile/b;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v2}, Lcom/msc/sa/myprofile/MyProfileWebView;->b(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/msc/sa/myprofile/MyProfileWebView;->a(ILandroid/content/Intent;)V

    .line 504
    iget-object v0, p0, Lcom/msc/sa/myprofile/b;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-virtual {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->finish()V

    goto :goto_1
.end method
