.class final Lcom/msc/sa/myprofile/c;
.super Lcom/msc/c/b;
.source "MyProfileWebView.java"


# instance fields
.field final synthetic c:Lcom/msc/sa/myprofile/MyProfileWebView;

.field private d:J

.field private e:J

.field private f:Z

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/msc/sa/myprofile/MyProfileWebView;)V
    .locals 1

    .prologue
    .line 856
    iput-object p1, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    .line 857
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 853
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/msc/sa/myprofile/c;->f:Z

    .line 859
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 4

    .prologue
    .line 879
    iget-object v0, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->b(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 881
    iget-object v0, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->b(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/msc/sa/myprofile/c;->g:Ljava/lang/String;

    .line 884
    :cond_0
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    const-string v1, "j5p7ll8g33"

    iget-object v2, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v2}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/msc/sa/myprofile/c;->d:J

    iget-wide v0, p0, Lcom/msc/sa/myprofile/c;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/msc/sa/myprofile/c;->a(J)V

    iget-wide v0, p0, Lcom/msc/sa/myprofile/c;->d:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/msc/sa/myprofile/c;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/msc/sa/myprofile/c;->d:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 885
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 933
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 935
    if-nez p1, :cond_1

    .line 987
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 940
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 941
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 943
    iget-wide v4, p0, Lcom/msc/sa/myprofile/c;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_2

    .line 947
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 948
    invoke-virtual {p0}, Lcom/msc/sa/myprofile/c;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 952
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    const-string v1, "authorization_code"

    const-string v3, "j5p7ll8g33"

    const-string v4, "5763D0052DC1462E13751F753384E9A9"

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/msc/sa/myprofile/c;->e:J

    iget-wide v0, p0, Lcom/msc/sa/myprofile/c;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/msc/sa/myprofile/c;->a(J)V

    iget-wide v0, p0, Lcom/msc/sa/myprofile/c;->e:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/msc/sa/myprofile/c;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/msc/sa/myprofile/c;->e:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 953
    :catch_0
    move-exception v0

    .line 955
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 956
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/msc/sa/myprofile/c;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 933
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 958
    :cond_2
    :try_start_4
    iget-wide v4, p0, Lcom/msc/sa/myprofile/c;->e:J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 962
    :try_start_5
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0, v2}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 963
    iget-object v1, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v1, v0}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 964
    invoke-virtual {p0}, Lcom/msc/sa/myprofile/c;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 969
    const-string v1, "com.msc.action.samsungaccount.mybenefitwebview_internal"

    iget-object v2, p0, Lcom/msc/sa/myprofile/c;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 971
    iget-object v1, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    iget-object v2, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    const-string v3, "j5p7ll8g33"

    invoke-static {v2, v3, v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->a(Lcom/msc/sa/myprofile/MyProfileWebView;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->c(Lcom/msc/sa/myprofile/MyProfileWebView;Ljava/lang/String;)Ljava/lang/String;

    .line 977
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->g(Lcom/msc/sa/myprofile/MyProfileWebView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 979
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/msc/sa/myprofile/c;->f:Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 981
    :catch_1
    move-exception v0

    .line 983
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 984
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/msc/sa/myprofile/c;->b:Lcom/msc/c/f;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 972
    :cond_4
    :try_start_7
    const-string v1, "com.msc.action.samsungaccount.myinfowebview_internal"

    iget-object v2, p0, Lcom/msc/sa/myprofile/c;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 974
    iget-object v1, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    iget-object v2, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    const-string v3, "j5p7ll8g33"

    invoke-static {v2, v3, v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->b(Lcom/msc/sa/myprofile/MyProfileWebView;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->c(Lcom/msc/sa/myprofile/MyProfileWebView;Ljava/lang/String;)Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 890
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 891
    iget-object v0, p0, Lcom/msc/sa/myprofile/c;->b:Lcom/msc/c/f;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/msc/sa/myprofile/c;->f:Z

    if-nez v0, :cond_6

    .line 893
    :cond_0
    const-string v0, "com.msc.action.samsungaccount.mybenefitwebview_internal"

    iget-object v1, p0, Lcom/msc/sa/myprofile/c;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 895
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.mybenefitwebview_internal"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 896
    iget-object v1, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    const/high16 v2, 0x8000000

    invoke-static {v1, v7, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 897
    iget-object v0, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    const v4, 0x7f09004f

    invoke-virtual {v3, v4}, Lcom/msc/sa/myprofile/MyProfileWebView;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    const v5, 0x7f090152

    invoke-virtual {v4, v5}, Lcom/msc/sa/myprofile/MyProfileWebView;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    .line 909
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-virtual {v0, v7}, Lcom/msc/sa/myprofile/MyProfileWebView;->b(I)V

    .line 910
    iget-object v0, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-virtual {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->finish()V

    .line 929
    :cond_2
    :goto_1
    return-void

    .line 899
    :cond_3
    const-string v0, "AUT_1302"

    iget-object v1, p0, Lcom/msc/sa/myprofile/c;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "AUT_1830"

    iget-object v1, p0, Lcom/msc/sa/myprofile/c;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 901
    :cond_4
    iget-object v0, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0}, Lcom/msc/c/c;->g(Landroid/content/Context;)V

    .line 902
    iget-object v0, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0}, Lcom/msc/c/e;->a(Landroid/content/Context;)Z

    .line 903
    iget-object v0, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    iget-object v1, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/sa/myprofile/MyProfileWebView;->b(Lcom/msc/sa/myprofile/MyProfileWebView;Ljava/lang/String;)V

    goto :goto_0

    .line 904
    :cond_5
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "AUT_1094"

    iget-object v1, p0, Lcom/msc/sa/myprofile/c;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 906
    iget-object v0, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->f(Lcom/msc/sa/myprofile/MyProfileWebView;)V

    goto :goto_0

    .line 921
    :cond_6
    iget-object v0, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->g(Lcom/msc/sa/myprofile/MyProfileWebView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 923
    iget-object v0, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->a(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 924
    iget-object v0, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->a(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/webkit/WebView;

    move-result-object v0

    iget-object v1, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v1}, Lcom/msc/sa/myprofile/MyProfileWebView;->g(Lcom/msc/sa/myprofile/MyProfileWebView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 925
    iget-object v0, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->a(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/webkit/WebView;

    move-result-object v0

    new-instance v1, Lcom/msc/sa/myprofile/d;

    iget-object v2, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-direct {v1, v2}, Lcom/msc/sa/myprofile/d;-><init>(Lcom/msc/sa/myprofile/MyProfileWebView;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 926
    iget-object v0, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->a(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/webkit/WebView;

    move-result-object v0

    new-instance v1, Lcom/msc/sa/myprofile/e;

    iget-object v2, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-direct {v1, v2, v7}, Lcom/msc/sa/myprofile/e;-><init>(Lcom/msc/sa/myprofile/MyProfileWebView;B)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    goto/16 :goto_1
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 991
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 993
    if-nez p1, :cond_1

    .line 995
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "onRequestFail: responseMessage is null"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 1009
    :cond_0
    :goto_0
    return-void

    .line 999
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 1000
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 1002
    iget-wide v4, p0, Lcom/msc/sa/myprofile/c;->d:J

    cmp-long v3, v0, v4

    if-eqz v3, :cond_2

    iget-wide v4, p0, Lcom/msc/sa/myprofile/c;->e:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 1004
    :cond_2
    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/msc/sa/myprofile/c;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_0

    .line 1006
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onRequestFail: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/msc/sa/myprofile/c;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 863
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 864
    iget-object v0, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/msc/sa/myprofile/MyProfileWebView;->b(I)V

    .line 865
    iget-object v0, p0, Lcom/msc/sa/myprofile/c;->c:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-virtual {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->finish()V

    .line 866
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 849
    invoke-virtual {p0}, Lcom/msc/sa/myprofile/c;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 849
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/msc/sa/myprofile/c;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected final onPreExecute()V
    .locals 0

    .prologue
    .line 870
    invoke-virtual {p0}, Lcom/msc/sa/myprofile/c;->c()V

    .line 872
    invoke-super {p0}, Lcom/msc/c/b;->onPreExecute()V

    .line 874
    return-void
.end method
