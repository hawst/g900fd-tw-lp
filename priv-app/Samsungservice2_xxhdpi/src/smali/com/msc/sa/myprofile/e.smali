.class final Lcom/msc/sa/myprofile/e;
.super Landroid/webkit/WebViewClient;
.source "MyProfileWebView.java"


# instance fields
.field final synthetic a:Lcom/msc/sa/myprofile/MyProfileWebView;


# direct methods
.method private constructor <init>(Lcom/msc/sa/myprofile/MyProfileWebView;)V
    .locals 0

    .prologue
    .line 538
    iput-object p1, p0, Lcom/msc/sa/myprofile/e;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/msc/sa/myprofile/MyProfileWebView;B)V
    .locals 0

    .prologue
    .line 538
    invoke-direct {p0, p1}, Lcom/msc/sa/myprofile/e;-><init>(Lcom/msc/sa/myprofile/MyProfileWebView;)V

    return-void
.end method


# virtual methods
.method public final onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 598
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPW"

    const-string v1, "onLoadResource"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WebViewClientClass::onLoadResource URL = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 601
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 602
    return-void
.end method

.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 578
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPW"

    const-string v1, "onPageFinished"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MyProfileWebView::onPageFinished URL = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 583
    :try_start_0
    iget-object v0, p0, Lcom/msc/sa/myprofile/e;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-virtual {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/msc/sa/myprofile/e;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->c(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/msc/sa/myprofile/e;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->c(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 585
    iget-object v0, p0, Lcom/msc/sa/myprofile/e;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->c(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 586
    iget-object v0, p0, Lcom/msc/sa/myprofile/e;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->d(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/app/ProgressDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 593
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 594
    return-void

    .line 588
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public final onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 558
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPW"

    const-string v1, "onPageStarted"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MyProfileWebView::onPageStarted URL = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 571
    iget-object v0, p0, Lcom/msc/sa/myprofile/e;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->e(Lcom/msc/sa/myprofile/MyProfileWebView;)V

    .line 573
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 574
    return-void
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 606
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPW"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onReceivedError errorCode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    :try_start_0
    iget-object v0, p0, Lcom/msc/sa/myprofile/e;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-virtual {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/msc/sa/myprofile/e;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->c(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/msc/sa/myprofile/e;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->c(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612
    iget-object v0, p0, Lcom/msc/sa/myprofile/e;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->c(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 613
    iget-object v0, p0, Lcom/msc/sa/myprofile/e;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->d(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/app/ProgressDialog;

    .line 616
    :cond_0
    const-string v0, "http://"

    invoke-virtual {p4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "https://"

    invoke-virtual {p4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 618
    :cond_1
    iget-object v0, p0, Lcom/msc/sa/myprofile/e;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->a(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/webkit/WebView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 619
    iget-object v0, p0, Lcom/msc/sa/myprofile/e;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->b(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "error_code"

    const-string v2, "SAC_0301"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 620
    iget-object v0, p0, Lcom/msc/sa/myprofile/e;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->b(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "error_message"

    const-string v2, "Network is not available"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 621
    iget-object v0, p0, Lcom/msc/sa/myprofile/e;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/msc/sa/myprofile/e;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v2}, Lcom/msc/sa/myprofile/MyProfileWebView;->b(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/myprofile/MyProfileWebView;->a(ILandroid/content/Intent;)V

    .line 622
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/msc/sa/myprofile/e;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    iget-object v1, p0, Lcom/msc/sa/myprofile/e;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    const v2, 0x7f09003e

    invoke-virtual {v1, v2}, Lcom/msc/sa/myprofile/MyProfileWebView;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 623
    iget-object v0, p0, Lcom/msc/sa/myprofile/e;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-virtual {v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 633
    :goto_0
    return-void

    .line 627
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 632
    :cond_2
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 546
    const-string v0, "http://"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "https://"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 548
    iget-object v0, p0, Lcom/msc/sa/myprofile/e;->a:Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-static {v0, p2}, Lcom/msc/sa/myprofile/MyProfileWebView;->a(Lcom/msc/sa/myprofile/MyProfileWebView;Ljava/lang/String;)V

    .line 553
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 551
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
