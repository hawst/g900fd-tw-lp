.class public Lcom/msc/sa/myprofile/MyProfileWebView;
.super Lcom/osp/app/util/BaseActivity;
.source "MyProfileWebView.java"


# static fields
.field private static final REQUEST_CODE_UPLOAD_FILES:I = 0x3e9


# instance fields
.field private A:Landroid/widget/LinearLayout;

.field private final B:Z

.field private C:Ljava/lang/String;

.field a:Lcom/msc/sa/myprofile/c;

.field private b:Landroid/content/Context;

.field private c:Landroid/app/ProgressDialog;

.field private d:Landroid/webkit/WebView;

.field private e:Landroid/webkit/ValueCallback;

.field private f:Z

.field private final y:Z

.field private z:Landroid/content/Intent;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 59
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 68
    iput-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->b:Landroid/content/Context;

    .line 73
    iput-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->c:Landroid/app/ProgressDialog;

    .line 78
    iput-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->d:Landroid/webkit/WebView;

    .line 85
    iput-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->e:Landroid/webkit/ValueCallback;

    .line 90
    iput-boolean v1, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->f:Z

    .line 95
    iput-boolean v1, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->y:Z

    .line 96
    iput-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->z:Landroid/content/Intent;

    .line 107
    iput-boolean v1, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->B:Z

    .line 122
    iput-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->C:Ljava/lang/String;

    .line 849
    return-void
.end method

.method static synthetic a(Lcom/msc/sa/myprofile/MyProfileWebView;Landroid/webkit/ValueCallback;)Landroid/webkit/ValueCallback;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->e:Landroid/webkit/ValueCallback;

    return-object p1
.end method

.method static synthetic a(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->d:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic a(Lcom/msc/sa/myprofile/MyProfileWebView;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/msc/sa/myprofile/MyProfileWebView;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 697
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPW"

    const-string v1, "makeMyBenefitURL()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    invoke-static {p0}, Lcom/msc/c/n;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "myBenefit"

    invoke-direct {p0, v0, p1, p2, v1}, Lcom/msc/sa/myprofile/MyProfileWebView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 720
    .line 726
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Android:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 730
    :try_start_0
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 731
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {}, Lcom/osp/common/util/i;->b()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 732
    :try_start_1
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->l(Landroid/content/Context;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 733
    :try_start_2
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->e()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v2

    .line 735
    if-eqz v0, :cond_0

    .line 737
    :try_start_3
    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v0

    .line 740
    :goto_0
    :try_start_4
    invoke-static {p0}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v5

    .line 741
    invoke-virtual {v5}, Lcom/osp/device/a;->d()Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    move-result-object v1

    .line 748
    :goto_1
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 749
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "?serviceID="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 750
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "&actionID="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 751
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "&countryCode="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 752
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "&csc="

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 753
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "&languageCode="

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 754
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "&tokenValue="

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 755
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "&deviceModelID="

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 756
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "&devicePhysicalAddressText="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 757
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "&osVer="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 759
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 743
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v0, v1

    :goto_2
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    :catch_1
    move-exception v0

    move-object v5, v0

    move-object v2, v1

    move-object v3, v1

    move-object v0, v1

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v5, v0

    move-object v2, v1

    move-object v0, v1

    goto :goto_2

    :catch_3
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    goto :goto_2

    :catch_4
    move-exception v5

    goto :goto_2

    :cond_0
    move-object v0, v1

    goto/16 :goto_0
.end method

.method private a(Ljava/lang/String;)Ljava/util/List;
    .locals 7

    .prologue
    .line 763
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 765
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 767
    invoke-virtual {p0}, Lcom/msc/sa/myprofile/MyProfileWebView;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/16 v2, 0x40

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 768
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPW"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Query Intent size : "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 770
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 772
    iget-object v1, v0, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    if-eqz v1, :cond_0

    .line 775
    const/4 v1, 0x0

    :goto_0
    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    invoke-virtual {v4}, Landroid/content/IntentFilter;->countDataSchemes()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 777
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v4, "MPW"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Data scheme : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v0, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    invoke-virtual {v6, v1}, Landroid/content/IntentFilter;->getDataScheme(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 775
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 781
    :cond_1
    return-object v2
.end method

.method static synthetic a(Lcom/msc/sa/myprofile/MyProfileWebView;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 59
    const-string v0, "myprofile://deeplink"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "servicename"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "servicepath"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "storename"

    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "storepath"

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "storepackagename"

    invoke-virtual {v0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, "://"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1}, Lcom/msc/sa/myprofile/MyProfileWebView;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "targetLinkUrl : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPW"

    const-string v1, "set deeplink : targetLinkUrl"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const v0, 0x14000020

    :try_start_0
    invoke-virtual {v5, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v5}, Lcom/msc/sa/myprofile/MyProfileWebView;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "storeLinkUrl : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPW"

    const-string v1, "set deeplink : targetLinkUrl"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :cond_2
    const-string v0, "myprofile://openpopup"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "url"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v1}, Lcom/msc/sa/myprofile/MyProfileWebView;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :cond_3
    const-string v0, "myprofile://close"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "close"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/msc/sa/myprofile/MyProfileWebView;->finish()V

    goto :goto_1
.end method

.method static synthetic b(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->z:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic b(Lcom/msc/sa/myprofile/MyProfileWebView;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/msc/sa/myprofile/MyProfileWebView;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 708
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPW"

    const-string v1, "makeMyInfoURL()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    invoke-static {p0}, Lcom/msc/c/n;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "myInfo"

    invoke-direct {p0, v0, p1, p2, v1}, Lcom/msc/sa/myprofile/MyProfileWebView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/msc/sa/myprofile/MyProfileWebView;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 59
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.resignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "client_id"

    const-string v2, "j5p7ll8g33"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    const-string v2, "5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p1, :cond_0

    const-string v1, "email_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    const/16 v1, 0xcd

    invoke-virtual {p0, v0, v1}, Lcom/msc/sa/myprofile/MyProfileWebView;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic c(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->c:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic c(Lcom/msc/sa/myprofile/MyProfileWebView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->C:Ljava/lang/String;

    return-object p1
.end method

.method private c()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 254
    invoke-virtual {p0}, Lcom/msc/sa/myprofile/MyProfileWebView;->getIntent()Landroid/content/Intent;

    move-result-object v2

    iput-object v2, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->z:Landroid/content/Intent;

    .line 256
    iget-object v2, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->z:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 257
    iget-object v3, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->z:Landroid/content/Intent;

    const-string v4, "client_id"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 258
    iget-object v4, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->z:Landroid/content/Intent;

    const-string v5, "access_token"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 260
    const-string v5, "com.msc.action.samsungaccount.mybenefitwebview_external"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 262
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "com.msc.action.samsungaccount.mybenefitwebview"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 263
    invoke-direct {p0, v3, v4}, Lcom/msc/sa/myprofile/MyProfileWebView;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->C:Ljava/lang/String;

    .line 297
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "MyProfileWebView::onCreate() Server URL = "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->C:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    move v0, v1

    .line 299
    :goto_1
    return v0

    .line 265
    :cond_0
    const-string v5, "com.msc.action.samsungaccount.myinfowebview_external"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 267
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "com.msc.action.samsungaccount.myinfowebview_external"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 268
    invoke-direct {p0, v3, v4}, Lcom/msc/sa/myprofile/MyProfileWebView;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->C:Ljava/lang/String;

    goto :goto_0

    .line 270
    :cond_1
    const-string v3, "com.msc.action.samsungaccount.myinfowebview_internal"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 272
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "com.msc.action.samsungaccount.myinfowebview_internal"

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 276
    new-instance v1, Lcom/msc/sa/myprofile/c;

    invoke-direct {v1, p0}, Lcom/msc/sa/myprofile/c;-><init>(Lcom/msc/sa/myprofile/MyProfileWebView;)V

    iput-object v1, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->a:Lcom/msc/sa/myprofile/c;

    .line 277
    iget-object v1, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->a:Lcom/msc/sa/myprofile/c;

    invoke-virtual {v1}, Lcom/msc/sa/myprofile/c;->b()V

    goto :goto_1

    .line 281
    :cond_2
    const-string v3, "com.msc.action.samsungaccount.mybenefitwebview_internal"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 283
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "com.msc.action.samsungaccount.mybenefitwebview_internal"

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 285
    new-instance v1, Lcom/msc/sa/myprofile/c;

    invoke-direct {v1, p0}, Lcom/msc/sa/myprofile/c;-><init>(Lcom/msc/sa/myprofile/MyProfileWebView;)V

    iput-object v1, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->a:Lcom/msc/sa/myprofile/c;

    .line 286
    iget-object v1, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->a:Lcom/msc/sa/myprofile/c;

    invoke-virtual {v1}, Lcom/msc/sa/myprofile/c;->b()V

    goto :goto_1

    .line 291
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "MPW"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onCreate() - unknown intent Action : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    iget-object v2, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->z:Landroid/content/Intent;

    invoke-virtual {p0, v1, v2}, Lcom/msc/sa/myprofile/MyProfileWebView;->a(ILandroid/content/Intent;)V

    .line 293
    invoke-virtual {p0}, Lcom/msc/sa/myprofile/MyProfileWebView;->finish()V

    goto :goto_1
.end method

.method static synthetic d(Lcom/msc/sa/myprofile/MyProfileWebView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->c:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 469
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPW"

    const-string v1, "showProgressDialog()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->c:Landroid/app/ProgressDialog;

    if-nez v0, :cond_1

    .line 473
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->c:Landroid/app/ProgressDialog;

    .line 474
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->c:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->b:Landroid/content/Context;

    const v2, 0x7f090036

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 475
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->c:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 476
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->c:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/msc/sa/myprofile/b;

    invoke-direct {v1, p0}, Lcom/msc/sa/myprofile/b;-><init>(Lcom/msc/sa/myprofile/MyProfileWebView;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 515
    :try_start_0
    invoke-virtual {p0}, Lcom/msc/sa/myprofile/MyProfileWebView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 517
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 530
    :cond_0
    :goto_0
    return-void

    .line 519
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 525
    :cond_1
    invoke-virtual {p0}, Lcom/msc/sa/myprofile/MyProfileWebView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 527
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method

.method static synthetic e(Lcom/msc/sa/myprofile/MyProfileWebView;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/msc/sa/myprofile/MyProfileWebView;->d()V

    return-void
.end method

.method static synthetic f(Lcom/msc/sa/myprofile/MyProfileWebView;)V
    .locals 3

    .prologue
    .line 59
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.resignin_with_signout"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xa

    if-le v1, v2, :cond_0

    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_0
    const-string v1, "client_id"

    const-string v2, "j5p7ll8g33"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    const-string v2, "5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v1, 0xcd

    invoke-virtual {p0, v0, v1}, Lcom/msc/sa/myprofile/MyProfileWebView;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic g(Lcom/msc/sa/myprofile/MyProfileWebView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->C:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 0

    .prologue
    .line 667
    return-void
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 673
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 347
    invoke-super {p0, p1, p2, p3}, Lcom/osp/app/util/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 348
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "WebView::onActivityResult requestCode : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ",resultCode : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 350
    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_3

    .line 352
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->e:Landroid/webkit/ValueCallback;

    if-nez v0, :cond_1

    .line 354
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPW"

    const-string v1, "onActivityResult() UploadMessage is null."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    :cond_0
    :goto_0
    return-void

    .line 358
    :cond_1
    if-eqz p3, :cond_2

    if-eq p2, v3, :cond_4

    :cond_2
    move-object v0, v1

    .line 359
    :goto_1
    iget-object v2, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->e:Landroid/webkit/ValueCallback;

    invoke-interface {v2, v0}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    .line 360
    iput-object v1, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->e:Landroid/webkit/ValueCallback;

    .line 363
    :cond_3
    const/16 v0, 0xd5

    if-ne p1, v0, :cond_0

    .line 365
    if-ne p2, v3, :cond_5

    .line 372
    invoke-direct {p0}, Lcom/msc/sa/myprofile/MyProfileWebView;->d()V

    .line 374
    invoke-direct {p0}, Lcom/msc/sa/myprofile/MyProfileWebView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 376
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->d:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 377
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->d:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 378
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->d:Landroid/webkit/WebView;

    new-instance v1, Lcom/msc/sa/myprofile/d;

    invoke-direct {v1, p0}, Lcom/msc/sa/myprofile/d;-><init>(Lcom/msc/sa/myprofile/MyProfileWebView;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 379
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->d:Landroid/webkit/WebView;

    new-instance v1, Lcom/msc/sa/myprofile/e;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/msc/sa/myprofile/e;-><init>(Lcom/msc/sa/myprofile/MyProfileWebView;B)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    goto :goto_0

    .line 358
    :cond_4
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    goto :goto_1

    .line 383
    :cond_5
    invoke-virtual {p0}, Lcom/msc/sa/myprofile/MyProfileWebView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 384
    iget-object v1, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->d:Landroid/webkit/WebView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 385
    const-string v1, "error_code"

    const-string v2, "SAC_0301"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 386
    const-string v1, "error_message"

    const-string v2, "Network is not available"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 387
    invoke-virtual {p0, v4, v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->a(ILandroid/content/Intent;)V

    .line 388
    invoke-virtual {p0}, Lcom/msc/sa/myprofile/MyProfileWebView;->finish()V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 445
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPW"

    const-string v1, "onBackPressed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->d:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->d:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 462
    :goto_0
    return-void

    .line 460
    :cond_0
    const/4 v0, -0x1

    iget-object v1, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->z:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/msc/sa/myprofile/MyProfileWebView;->a(ILandroid/content/Intent;)V

    .line 461
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 161
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPW"

    const-string v1, "onConfigurationChanged()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 169
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const v5, 0x7f0c0155

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 198
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 200
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPW"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iput-boolean v3, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->f:Z

    .line 205
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_3

    .line 207
    invoke-virtual {p0, v3}, Lcom/msc/sa/myprofile/MyProfileWebView;->requestWindowFeature(I)Z

    .line 221
    :goto_0
    const v0, 0x7f03005d

    invoke-virtual {p0, v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->setContentView(I)V

    .line 223
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->A:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    const v0, 0x7f0c0028

    invoke-virtual {p0, v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->A:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->A:Landroid/widget/LinearLayout;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f0200ff

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    :cond_0
    invoke-virtual {p0, v5}, Lcom/msc/sa/myprofile/MyProfileWebView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f020020

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    invoke-virtual {p0}, Lcom/msc/sa/myprofile/MyProfileWebView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f020030

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 225
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPW"

    const-string v1, "initComponent"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->b:Landroid/content/Context;

    const v0, 0x7f0c00fd

    invoke-virtual {p0, v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->d:Landroid/webkit/WebView;

    invoke-virtual {p0, v5}, Lcom/msc/sa/myprofile/MyProfileWebView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/msc/sa/myprofile/MyProfileWebView;->h()Z

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/view/View;Z)V

    new-instance v1, Lcom/msc/sa/myprofile/a;

    invoke-direct {v1, p0}, Lcom/msc/sa/myprofile/a;-><init>(Lcom/msc/sa/myprofile/MyProfileWebView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-boolean v1, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->f:Z

    if-ne v1, v3, :cond_5

    const v1, 0x7f0900c7

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPW"

    const-string v1, "initComponent"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 234
    const/16 v0, 0xd5

    invoke-virtual {p0, v0}, Lcom/msc/sa/myprofile/MyProfileWebView;->c(I)V

    .line 247
    :cond_2
    :goto_2
    return-void

    .line 212
    :cond_3
    invoke-virtual {p0}, Lcom/msc/sa/myprofile/MyProfileWebView;->h()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 214
    invoke-virtual {p0}, Lcom/msc/sa/myprofile/MyProfileWebView;->i()V

    goto/16 :goto_0

    .line 217
    :cond_4
    invoke-virtual {p0}, Lcom/msc/sa/myprofile/MyProfileWebView;->j()V

    goto/16 :goto_0

    .line 225
    :cond_5
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->A:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1

    .line 238
    :cond_6
    invoke-direct {p0}, Lcom/msc/sa/myprofile/MyProfileWebView;->d()V

    .line 240
    invoke-direct {p0}, Lcom/msc/sa/myprofile/MyProfileWebView;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 242
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->d:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 243
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->d:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 244
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->d:Landroid/webkit/WebView;

    new-instance v1, Lcom/msc/sa/myprofile/d;

    invoke-direct {v1, p0}, Lcom/msc/sa/myprofile/d;-><init>(Lcom/msc/sa/myprofile/MyProfileWebView;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 245
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->d:Landroid/webkit/WebView;

    new-instance v1, Lcom/msc/sa/myprofile/e;

    invoke-direct {v1, p0, v4}, Lcom/msc/sa/myprofile/e;-><init>(Lcom/msc/sa/myprofile/MyProfileWebView;B)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 397
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPW"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->a:Lcom/msc/sa/myprofile/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->a:Lcom/msc/sa/myprofile/c;

    invoke-virtual {v0}, Lcom/msc/sa/myprofile/c;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 401
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->a:Lcom/msc/sa/myprofile/c;

    invoke-virtual {v0}, Lcom/msc/sa/myprofile/c;->d()V

    .line 402
    iput-object v2, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->a:Lcom/msc/sa/myprofile/c;

    .line 405
    :cond_0
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->d:Landroid/webkit/WebView;

    if-eqz v0, :cond_1

    .line 407
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->d:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 408
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->d:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V

    .line 409
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->d:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    .line 410
    iput-object v2, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->d:Landroid/webkit/WebView;

    .line 413
    :cond_1
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->c:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 415
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 416
    iput-object v2, p0, Lcom/msc/sa/myprofile/MyProfileWebView;->c:Landroid/app/ProgressDialog;

    .line 419
    :cond_2
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onDestroy()V

    .line 420
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 424
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPW"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onPause()V

    .line 427
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 431
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPW"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onResume()V

    .line 434
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 438
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onStop()V

    .line 440
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPW"

    const-string v1, "onStop"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    return-void
.end method
