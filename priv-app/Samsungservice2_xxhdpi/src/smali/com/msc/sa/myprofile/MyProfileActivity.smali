.class public Lcom/msc/sa/myprofile/MyProfileActivity;
.super Lcom/osp/app/util/BaseActivity;
.source "MyProfileActivity.java"


# instance fields
.field private a:Landroid/content/Intent;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 12
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 15
    iput-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->a:Landroid/content/Intent;

    .line 20
    iput-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->b:Ljava/lang/String;

    .line 25
    iput-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->c:Ljava/lang/String;

    .line 30
    iput-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->d:Ljava/lang/String;

    .line 35
    iput-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->e:Ljava/lang/String;

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 204
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPA"

    const-string v1, "setFailedResult()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 206
    const-string v1, "error_code"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 207
    const-string v1, "error_message"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 208
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/msc/sa/myprofile/MyProfileActivity;->a(ILandroid/content/Intent;)V

    .line 209
    invoke-virtual {p0}, Lcom/msc/sa/myprofile/MyProfileActivity;->finish()V

    .line 210
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 153
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPA"

    const-string v1, "startMyProfileWebView()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 155
    const-string v1, "com.msc.action.samsungaccount.mybenefitwebview"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 157
    const-string v1, "com.msc.action.samsungaccount.mybenefitwebview_external"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 164
    :cond_0
    :goto_0
    const-string v1, "client_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 165
    const-string v1, "client_secret"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 166
    const-string v1, "access_token"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 167
    const/16 v1, 0xed

    invoke-virtual {p0, v0, v1}, Lcom/msc/sa/myprofile/MyProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 168
    invoke-virtual {p0}, Lcom/msc/sa/myprofile/MyProfileActivity;->finish()V

    .line 169
    return-void

    .line 159
    :cond_1
    const-string v1, "com.msc.action.samsungaccount.myinfowebview"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 161
    const-string v1, "com.msc.action.samsungaccount.myinfowebview_external"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method


# virtual methods
.method protected final a()V
    .locals 0

    .prologue
    .line 79
    return-void
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 85
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 174
    invoke-super {p0, p1, p2, p3}, Lcom/osp/app/util/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 175
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MyProfileActivity::onActivityResult requestCode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",resultCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 177
    const/16 v0, 0xcf

    if-ne p1, v0, :cond_2

    .line 179
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 181
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->e:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/msc/sa/myprofile/MyProfileActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->a:Landroid/content/Intent;

    invoke-virtual {p0, p2, v0}, Lcom/msc/sa/myprofile/MyProfileActivity;->setResult(ILandroid/content/Intent;)V

    .line 186
    invoke-virtual {p0}, Lcom/msc/sa/myprofile/MyProfileActivity;->finish()V

    goto :goto_0

    .line 190
    :cond_2
    const/16 v0, 0xed

    if-ne p1, v0, :cond_0

    .line 192
    invoke-virtual {p0, p2, p3}, Lcom/msc/sa/myprofile/MyProfileActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 39
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPA"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/msc/sa/myprofile/MyProfileActivity;->d(I)V

    .line 41
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    invoke-virtual {p0}, Lcom/msc/sa/myprofile/MyProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 44
    if-nez v0, :cond_0

    .line 46
    const-string v0, "SAC_0101"

    const-string v1, "Param [%s] must not be null"

    invoke-direct {p0, v0, v1}, Lcom/msc/sa/myprofile/MyProfileActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    :goto_0
    return-void

    .line 57
    :cond_0
    invoke-virtual {p0}, Lcom/msc/sa/myprofile/MyProfileActivity;->p()Z

    move-result v1

    if-nez v1, :cond_1

    .line 59
    const-string v0, "SAC_0205"

    const-string v1, "The signature of this application is not registered with the server."

    invoke-direct {p0, v0, v1}, Lcom/msc/sa/myprofile/MyProfileActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 63
    :cond_1
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 65
    const-string v0, "SAC_0102"

    const-string v1, "Samsung account does not exist"

    invoke-direct {p0, v0, v1}, Lcom/msc/sa/myprofile/MyProfileActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 69
    :cond_2
    const-string v1, "progress_theme"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, v0}, Lcom/msc/sa/myprofile/MyProfileActivity;->d(Ljava/lang/String;)V

    .line 71
    :goto_1
    invoke-virtual {p0}, Lcom/msc/sa/myprofile/MyProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->a:Landroid/content/Intent;

    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->a:Landroid/content/Intent;

    const-string v1, "client_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->a:Landroid/content/Intent;

    const-string v1, "client_secret"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->a:Landroid/content/Intent;

    const-string v1, "access_token"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->b:Ljava/lang/String;

    if-nez v0, :cond_4

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPA"

    const-string v1, "onCreate() - intent Action is null."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->a:Landroid/content/Intent;

    invoke-virtual {p0, v3, v0}, Lcom/msc/sa/myprofile/MyProfileActivity;->a(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/msc/sa/myprofile/MyProfileActivity;->finish()V

    goto :goto_0

    .line 69
    :cond_3
    invoke-virtual {p0}, Lcom/msc/sa/myprofile/MyProfileActivity;->k()V

    goto :goto_1

    .line 71
    :cond_4
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPA"

    const-string v1, "onCreate() - service param is null."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->a:Landroid/content/Intent;

    const-string v1, "error_code"

    const-string v2, "SAC_0101"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->a:Landroid/content/Intent;

    const-string v1, "error_message"

    const-string v2, "Param [%s] must not be null"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->a:Landroid/content/Intent;

    invoke-virtual {p0, v3, v0}, Lcom/msc/sa/myprofile/MyProfileActivity;->a(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/msc/sa/myprofile/MyProfileActivity;->finish()V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MPA"

    const-string v1, "onCreate() - service token is null."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->a:Landroid/content/Intent;

    const-string v1, "error_code"

    const-string v2, "SAC_0101"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->a:Landroid/content/Intent;

    const-string v1, "error_message"

    const-string v2, "Param [%s] must not be null"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->a:Landroid/content/Intent;

    invoke-virtual {p0, v3, v0}, Lcom/msc/sa/myprofile/MyProfileActivity;->a(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/msc/sa/myprofile/MyProfileActivity;->finish()V

    goto/16 :goto_0

    :cond_7
    const-string v0, "com.msc.action.samsungaccount.myinfowebview"

    iget-object v1, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->d:Ljava/lang/String;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "MPA"

    const-string v3, "startConfirmPassword()"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "com.osp.app.signin"

    const-string v4, "com.osp.app.signin.AccountView"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "client_id"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "client_secret"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "OSP_VER"

    const-string v1, "OSP_02"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "account_mode"

    const-string v1, "ACCOUNT_VERIFY"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v0, 0xcf

    invoke-virtual {p0, v2, v0}, Lcom/msc/sa/myprofile/MyProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/msc/sa/myprofile/MyProfileActivity;->e:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/msc/sa/myprofile/MyProfileActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
