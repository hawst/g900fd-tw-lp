.class public final Lcom/msc/sa/b/c;
.super Ljava/lang/Object;
.source "ChecklistManager.java"


# static fields
.field private static final a:[I

.field private static final b:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 24
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/msc/sa/b/c;->a:[I

    .line 25
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/msc/sa/b/c;->b:[I

    return-void

    .line 24
    :array_0
    .array-data 4
        0x2
        0x4
        0x8
        0x10
    .end array-data

    .line 25
    :array_1
    .array-data 4
        0x2
        0x4
        0x8
        0x10
    .end array-data
.end method

.method public static a(Lcom/msc/a/i;Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    const/4 v8, 0x1

    .line 154
    sget-object v3, Lcom/msc/sa/b/c;->a:[I

    .line 161
    invoke-virtual {p0}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/a/g;->d()Z

    move-result v0

    if-eq v0, v8, :cond_0

    invoke-virtual {p0}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/a/g;->e()Z

    move-result v0

    if-ne v0, v8, :cond_9

    :cond_0
    invoke-static {p1}, Lcom/msc/openprovider/b;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CM"

    const-string v4, "requirement : Acceptance of TNC"

    invoke-static {v0, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    :goto_0
    invoke-virtual {p0}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/msc/a/g;->g()Z

    move-result v4

    if-ne v4, v8, :cond_1

    invoke-static {p1}, Lcom/msc/openprovider/b;->c(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v4, "CM"

    const-string v5, "requirement : Name verification"

    invoke-static {v4, v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/msc/a/g;->h()Z

    move-result v4

    if-ne v4, v8, :cond_2

    invoke-static {p1}, Lcom/msc/openprovider/b;->d(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v4, "CM"

    const-string v5, "requirement : Email validation"

    invoke-static {v4, v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v4

    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->b()I

    move-result v4

    if-lez v4, :cond_3

    invoke-static {p1, p2}, Lcom/msc/openprovider/b;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v4, "CM"

    const-string v5, "requirement : Mandatory input"

    invoke-static {v4, v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    :cond_3
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    array-length v5, v3

    :goto_1
    if-ge v1, v5, :cond_8

    aget v6, v3, v1

    and-int/lit8 v7, v0, 0x2

    if-ne v7, v6, :cond_4

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    and-int/lit8 v7, v0, 0x4

    if-ne v7, v6, :cond_5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    and-int/lit8 v7, v0, 0x8

    if-ne v7, v6, :cond_6

    const/4 v7, 0x3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    and-int/lit8 v7, v0, 0x10

    if-ne v7, v6, :cond_7

    const/4 v6, 0x4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 168
    :cond_8
    return-object v4

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method
