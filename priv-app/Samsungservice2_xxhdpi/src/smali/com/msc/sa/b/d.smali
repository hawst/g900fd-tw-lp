.class public final Lcom/msc/sa/b/d;
.super Ljava/lang/Object;
.source "ChecklistStepUtil.java"


# instance fields
.field a:Ljava/util/ArrayList;

.field b:I

.field c:Ljava/lang/String;

.field d:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object v1, p0, Lcom/msc/sa/b/d;->a:Ljava/util/ArrayList;

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/msc/sa/b/d;->b:I

    .line 36
    iput-object v1, p0, Lcom/msc/sa/b/d;->c:Ljava/lang/String;

    .line 41
    iput-object v1, p0, Lcom/msc/sa/b/d;->d:Landroid/content/Intent;

    .line 51
    iput-object p1, p0, Lcom/msc/sa/b/d;->a:Ljava/util/ArrayList;

    .line 52
    iput-object p2, p0, Lcom/msc/sa/b/d;->c:Ljava/lang/String;

    .line 53
    iput-object v1, p0, Lcom/msc/sa/b/d;->d:Landroid/content/Intent;

    .line 54
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)I
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 77
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CMUgetNextCheckItem"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 78
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 79
    iget-object v0, p0, Lcom/msc/sa/b/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    move v0, v1

    .line 106
    :goto_0
    return v0

    .line 83
    :cond_0
    iget v0, p0, Lcom/msc/sa/b/d;->b:I

    if-eqz v0, :cond_1

    .line 85
    iget-object v0, p0, Lcom/msc/sa/b/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 88
    :cond_1
    iput v1, p0, Lcom/msc/sa/b/d;->b:I

    .line 89
    iget-object v0, p0, Lcom/msc/sa/b/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v1

    .line 90
    :goto_1
    if-ge v3, v5, :cond_2

    .line 92
    iget-object v0, p0, Lcom/msc/sa/b/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 93
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CMUisRequired"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    packed-switch v6, :pswitch_data_0

    move v0, v1

    :goto_2
    if-eqz v0, :cond_4

    .line 95
    iput v6, p0, Lcom/msc/sa/b/d;->b:I

    .line 102
    :cond_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_3
    if-nez v0, :cond_5

    .line 104
    iget-object v1, p0, Lcom/msc/sa/b/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 102
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    .line 93
    :pswitch_0
    invoke-static {p1}, Lcom/msc/openprovider/b;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    goto :goto_2

    :pswitch_1
    invoke-static {p1}, Lcom/msc/openprovider/b;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    goto :goto_2

    :pswitch_2
    invoke-static {p1}, Lcom/msc/openprovider/b;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    goto :goto_2

    :pswitch_3
    iget-object v0, p0, Lcom/msc/sa/b/d;->c:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/msc/openprovider/b;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_2

    .line 99
    :cond_4
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 106
    :cond_5
    iget v0, p0, Lcom/msc/sa/b/d;->b:I

    goto :goto_0

    .line 93
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a()Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 129
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CMUgetIntentOfLeftChecklist"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 130
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 131
    iget-object v0, p0, Lcom/msc/sa/b/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 132
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    .line 134
    iget-object v0, p0, Lcom/msc/sa/b/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v5, :cond_0

    .line 136
    const-string v0, "tnc_acceptance_required"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/msc/sa/b/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v4, 0x2

    if-ne v0, v4, :cond_1

    .line 140
    const-string v0, "name_verification_required"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 142
    :cond_1
    iget-object v0, p0, Lcom/msc/sa/b/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v4, 0x3

    if-ne v0, v4, :cond_2

    .line 144
    const-string v0, "email_validation_required"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 146
    :cond_2
    iget-object v0, p0, Lcom/msc/sa/b/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v4, 0x4

    if-ne v0, v4, :cond_3

    .line 148
    const-string v0, "mandatory_input_required"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 132
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 151
    :cond_4
    return-object v2
.end method
