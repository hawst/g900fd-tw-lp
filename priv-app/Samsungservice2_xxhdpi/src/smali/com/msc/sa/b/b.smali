.class public final Lcom/msc/sa/b/b;
.super Ljava/lang/Object;
.source "CallbackManager.java"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/msc/sa/a/a;

.field private e:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/sa/a/a;)V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/msc/sa/b/b;->a:Ljava/lang/String;

    .line 70
    iput-object p2, p0, Lcom/msc/sa/b/b;->b:Ljava/lang/String;

    .line 71
    iput-object p3, p0, Lcom/msc/sa/b/b;->c:Ljava/lang/String;

    .line 72
    iput-object p4, p0, Lcom/msc/sa/b/b;->d:Lcom/msc/sa/a/a;

    .line 73
    const/4 v0, 0x0

    iput v0, p0, Lcom/msc/sa/b/b;->e:I

    .line 74
    return-void
.end method

.method static synthetic a(Lcom/msc/sa/b/b;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/msc/sa/b/b;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/msc/sa/b/b;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 82
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CBM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setCurrentRequestType :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    iput p1, p0, Lcom/msc/sa/b/b;->e:I

    .line 84
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/msc/sa/b/b;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/msc/sa/b/b;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lcom/msc/sa/a/a;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/msc/sa/b/b;->d:Lcom/msc/sa/a/a;

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/msc/sa/b/b;->e:I

    return v0
.end method
