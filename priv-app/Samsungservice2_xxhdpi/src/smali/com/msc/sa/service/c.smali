.class public abstract Lcom/msc/sa/service/c;
.super Ljava/lang/Object;
.source "AbstractProcessRunnable.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field protected a:Lcom/msc/c/f;

.field protected final b:Lcom/msc/sa/d/a;

.field final c:Ljava/lang/String;

.field private final d:Ljava/lang/ref/WeakReference;

.field private e:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/msc/sa/service/c;->d:Ljava/lang/ref/WeakReference;

    .line 93
    invoke-static {}, Lcom/msc/sa/b/a;->a()Lcom/msc/sa/b/a;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/msc/sa/b/a;->b(Ljava/lang/String;)Lcom/msc/sa/b/b;

    move-result-object v7

    .line 94
    if-eqz v7, :cond_2

    .line 97
    const/4 v0, 0x4

    if-ne p2, v0, :cond_1

    if-eqz p5, :cond_1

    const-string v0, "authcode_replaceable"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 99
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "APR"

    const-string v1, "[InAIDL-AbstractProcessRunnable]Replace clientID & clientSecret for RequestAuthCode."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const-string v0, "replaceable_client_id"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 102
    const-string v0, "replaceable_client_secret"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 104
    new-instance v0, Lcom/msc/sa/d/a;

    invoke-virtual {v7}, Lcom/msc/sa/b/b;->c()Ljava/lang/String;

    move-result-object v5

    move-object v1, p4

    move v2, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/msc/sa/d/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/msc/sa/service/c;->b:Lcom/msc/sa/d/a;

    .line 110
    :goto_0
    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v0

    invoke-virtual {v7}, Lcom/msc/sa/b/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/msc/sa/service/c;->c:Ljava/lang/String;

    .line 119
    :goto_1
    if-eqz v7, :cond_0

    invoke-virtual {v7, p2}, Lcom/msc/sa/b/b;->a(I)V

    .line 120
    :cond_0
    return-void

    .line 107
    :cond_1
    new-instance v0, Lcom/msc/sa/d/a;

    invoke-virtual {v7}, Lcom/msc/sa/b/b;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7}, Lcom/msc/sa/b/b;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7}, Lcom/msc/sa/b/b;->c()Ljava/lang/String;

    move-result-object v5

    move-object v1, p4

    move v2, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/msc/sa/d/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/msc/sa/service/c;->b:Lcom/msc/sa/d/a;

    goto :goto_0

    .line 113
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "APR"

    const-string v1, "[InAIDL-AbstractProcessRunnable]CallbackInfo is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    iput-object v2, p0, Lcom/msc/sa/service/c;->b:Lcom/msc/sa/d/a;

    .line 115
    iput-object v2, p0, Lcom/msc/sa/service/c;->c:Ljava/lang/String;

    goto :goto_1
.end method

.method static synthetic a(Lcom/msc/sa/service/c;JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 39
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "APR"

    const-string v1, "[InAIDL-AbstractProcessRunnable]parseFailErrorResult"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/msc/sa/service/c;->e:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/msc/sa/service/c;->e:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1}, Lcom/msc/c/f;-><init>()V

    iput-object v1, p0, Lcom/msc/sa/service/c;->a:Lcom/msc/c/f;

    iget-object v1, p0, Lcom/msc/sa/service/c;->a:Lcom/msc/c/f;

    invoke-virtual {v1, v0, p3}, Lcom/msc/c/f;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/msc/sa/service/c;->a:Lcom/msc/c/f;

    :cond_0
    return-void
.end method


# virtual methods
.method protected final a()Landroid/content/Context;
    .locals 3

    .prologue
    .line 165
    const/4 v0, 0x0

    .line 166
    iget-object v1, p0, Lcom/msc/sa/service/c;->d:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    .line 168
    iget-object v0, p0, Lcom/msc/sa/service/c;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 170
    :cond_0
    if-nez v0, :cond_1

    .line 172
    const-string v1, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-virtual {p0, v1, v2}, Lcom/msc/sa/service/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "APR"

    const-string v2, "context is null"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    :cond_1
    return-object v0
.end method

.method protected final a(JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 152
    iget-object v0, p0, Lcom/msc/sa/service/c;->e:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 154
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/msc/sa/service/c;->e:Ljava/util/HashMap;

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/msc/sa/service/c;->e:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 582
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "APR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AbstractProcessRunnable]showResigninNotification. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    if-nez p1, :cond_0

    .line 586
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "APR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AbstractProcessRunnable]showResignNotifation context is null. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    :goto_0
    return-void

    .line 590
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.resignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 592
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 594
    const-string v1, "client_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 595
    const-string v1, "client_secret"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 596
    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 597
    const-string v1, "email_id"

    invoke-static {p1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 599
    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 601
    const/4 v2, 0x0

    const v0, 0x7f09004f

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f09008d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    .line 604
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "APR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AbstractProcessRunnable]Show resign-in Notification. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final a(Landroid/os/Bundle;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 542
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "APR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AbstractProcessRunnable]setAdditionalCheckListResult. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "APR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AbstractProcessRunnable]checklistResult : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Package = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/sa/service/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    and-int/lit8 v0, p2, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 547
    const-string v0, "tnc_acceptance_required"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 553
    :goto_0
    and-int/lit8 v0, p2, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 555
    const-string v0, "name_verification_required"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 561
    :goto_1
    and-int/lit8 v0, p2, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 563
    const-string v0, "email_validation_required"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 569
    :goto_2
    and-int/lit8 v0, p2, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 571
    const-string v0, "mandatory_input_required"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 576
    :goto_3
    return-void

    .line 550
    :cond_0
    const-string v0, "tnc_acceptance_required"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 558
    :cond_1
    const-string v0, "name_verification_required"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1

    .line 566
    :cond_2
    const-string v0, "email_validation_required"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_2

    .line 574
    :cond_3
    const-string v0, "mandatory_input_required"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_3
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 340
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "APR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AbstractProcessRunnable]setErrorResult Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0, p1, p2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/msc/sa/service/c;->a:Lcom/msc/c/f;

    .line 343
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[InAIDL-AbstractProcessRunnable]errorMessage = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Package = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/msc/sa/service/c;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 344
    return-void
.end method

.method public abstract a(ZLcom/msc/sa/b/b;)V
.end method

.method public abstract b()Z
.end method

.method protected final c()V
    .locals 4

    .prologue
    .line 504
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "APR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AbstractProcessRunnable]handleDrawalAccount. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    invoke-virtual {p0}, Lcom/msc/sa/service/c;->a()Landroid/content/Context;

    move-result-object v0

    .line 507
    if-nez v0, :cond_1

    .line 509
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "APR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AbstractProcessRunnable]Context is null. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    const-string v0, "SAC_0401"

    const-string v1, "Internal error occurred"

    invoke-virtual {p0, v0, v1}, Lcom/msc/sa/service/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    :cond_0
    :goto_0
    return-void

    .line 514
    :cond_1
    const-string v1, "USR_3113"

    iget-object v2, p0, Lcom/msc/sa/service/c;->a:Lcom/msc/c/f;

    invoke-virtual {v2}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->l()I

    move-result v1

    const/16 v2, 0x10

    if-le v1, v2, :cond_0

    .line 516
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "APR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-AbstractProcessRunnable]USR_3113 Error occurred. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/c;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    invoke-static {v0}, Lcom/osp/app/util/ad;->i(Landroid/content/Context;)V

    .line 519
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/msc/sa/service/d;

    invoke-direct {v2, p0, v0}, Lcom/msc/sa/service/d;-><init>(Lcom/msc/sa/service/c;Landroid/content/Context;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public final run()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 181
    iget-object v0, p0, Lcom/msc/sa/service/c;->b:Lcom/msc/sa/d/a;

    if-nez v0, :cond_1

    .line 216
    :cond_0
    :goto_0
    return-void

    .line 186
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "APR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-AbstractProcessRunnable]isAvailableRunningProcess start Package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/c;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/msc/sa/service/c;->a()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_3

    move v0, v1

    .line 188
    :goto_1
    if-ne v0, v2, :cond_2

    .line 190
    invoke-virtual {p0}, Lcom/msc/sa/service/c;->b()Z

    move-result v0

    .line 193
    :cond_2
    invoke-static {}, Lcom/msc/sa/b/a;->a()Lcom/msc/sa/b/a;

    move-result-object v3

    iget-object v4, p0, Lcom/msc/sa/service/c;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v4}, Lcom/msc/sa/d/a;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/msc/sa/b/a;->b(Ljava/lang/String;)Lcom/msc/sa/b/b;

    move-result-object v4

    .line 195
    if-nez v4, :cond_9

    .line 197
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "APR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AbstractProcessRunnable]CallbackInfo is null. finish this request. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 186
    :cond_3
    iget-object v3, p0, Lcom/msc/sa/service/c;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v3}, Lcom/msc/sa/d/a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-ne v3, v2, :cond_4

    const-string v0, "SAC_0401"

    const-string v3, "Internal error occurred"

    invoke-virtual {p0, v0, v3}, Lcom/msc/sa/service/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_1

    :cond_4
    invoke-static {v0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v0, "SAC_0301"

    const-string v3, "Network is not available"

    invoke-virtual {p0, v0, v3}, Lcom/msc/sa/service/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_1

    :cond_5
    invoke-static {v0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v0, "SAC_0102"

    const-string v3, "Samsung account does not exist"

    invoke-virtual {p0, v0, v3}, Lcom/msc/sa/service/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_1

    :cond_6
    invoke-static {v0, v1}, Lcom/msc/c/c;->a(Landroid/content/Context;Z)Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/msc/sa/service/c;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v3}, Lcom/msc/sa/d/a;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/msc/sa/service/c;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v4}, Lcom/msc/sa/d/a;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v0, v3, v4}, Lcom/msc/sa/service/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "SAC_0402"

    const-string v3, "Auth token expired"

    invoke-virtual {p0, v0, v3}, Lcom/msc/sa/service/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_1

    :cond_7
    iget-object v3, p0, Lcom/msc/sa/service/c;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v3}, Lcom/msc/sa/d/a;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/msc/sa/service/c;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v4}, Lcom/msc/sa/d/a;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/msc/sa/c/j;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "APR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-AbstractProcessRunnable]Signature check Fail. Check clientID, packageName again. Package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/c;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "SAC_0205"

    const-string v3, "The signature of this application is not registered with the server."

    invoke-virtual {p0, v0, v3}, Lcom/msc/sa/service/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto/16 :goto_1

    :cond_8
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "APR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-AbstractProcessRunnable]isAvailableRunningProcess end. Package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/c;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    goto/16 :goto_1

    .line 202
    :cond_9
    if-eqz v4, :cond_c

    invoke-virtual {v4}, Lcom/msc/sa/b/b;->e()I

    move-result v3

    if-eqz v3, :cond_c

    invoke-virtual {v4, v1}, Lcom/msc/sa/b/b;->a(I)V

    move v3, v2

    .line 204
    :goto_2
    if-eqz v3, :cond_0

    .line 210
    if-ne v0, v2, :cond_b

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "APR"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "[InAIDL-AbstractProcessRunnable]isSamsungAccountSingnedin Package = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/msc/sa/service/c;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/msc/sa/service/c;->a()Landroid/content/Context;

    move-result-object v3

    if-nez v3, :cond_d

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "APR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "[InAIDL-AbstractProcessRunnable]Context is null Package = "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/msc/sa/service/c;->c:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v1

    :cond_a
    :goto_3
    if-nez v2, :cond_b

    move v0, v1

    .line 215
    :cond_b
    invoke-virtual {p0, v0, v4}, Lcom/msc/sa/service/c;->a(ZLcom/msc/sa/b/b;)V

    goto/16 :goto_0

    :cond_c
    move v3, v1

    .line 202
    goto :goto_2

    .line 210
    :cond_d
    invoke-static {v3}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string v2, "SAC_0102"

    const-string v3, "Samsung account does not exist"

    invoke-virtual {p0, v2, v3}, Lcom/msc/sa/service/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v1

    goto :goto_3
.end method
