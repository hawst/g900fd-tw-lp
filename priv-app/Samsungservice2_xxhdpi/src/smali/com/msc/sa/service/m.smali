.class public Lcom/msc/sa/service/m;
.super Ljava/lang/Object;
.source "GearLogCollectingRunnable.java"

# interfaces
.implements Lcom/msc/b/h;


# instance fields
.field final synthetic b:Lcom/msc/sa/service/k;


# direct methods
.method protected constructor <init>(Lcom/msc/sa/service/k;)V
    .locals 0

    .prologue
    .line 398
    iput-object p1, p0, Lcom/msc/sa/service/m;->b:Lcom/msc/sa/service/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/msc/b/c;)V
    .locals 3

    .prologue
    .line 402
    if-nez p1, :cond_0

    .line 404
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "GLCR"

    const-string v1, "[ExAIDL-GearLogService]onRequestSuccess response Message is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    iget-object v0, p0, Lcom/msc/sa/service/m;->b:Lcom/msc/sa/service/k;

    const-string v1, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/service/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    :cond_0
    return-void
.end method

.method public b(Lcom/msc/b/c;)V
    .locals 5

    .prologue
    .line 417
    if-nez p1, :cond_1

    .line 419
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "GLCR"

    const-string v1, "[ExAIDL-GearLogService]onRequestfail responseMessage is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    iget-object v0, p0, Lcom/msc/sa/service/m;->b:Lcom/msc/sa/service/k;

    const-string v1, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/service/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    :cond_0
    :goto_0
    return-void

    .line 424
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 425
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 426
    invoke-virtual {p1}, Lcom/msc/b/c;->f()Ljava/lang/Exception;

    move-result-object v3

    .line 428
    if-eqz v3, :cond_3

    .line 430
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "GLCR"

    const-string v1, "[ExAIDL-GearLogService]Server request fail. Exception occured"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    iget-object v0, p0, Lcom/msc/sa/service/m;->b:Lcom/msc/sa/service/k;

    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v3}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, v0, Lcom/msc/sa/service/k;->a:Lcom/msc/c/f;

    .line 434
    iget-object v0, p0, Lcom/msc/sa/service/m;->b:Lcom/msc/sa/service/k;

    invoke-virtual {v0}, Lcom/msc/sa/service/k;->a()Landroid/content/Context;

    move-result-object v0

    .line 435
    if-nez v0, :cond_2

    .line 437
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "GLCR"

    const-string v1, "[ExAIDL-GearLogService]Context is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    iget-object v0, p0, Lcom/msc/sa/service/m;->b:Lcom/msc/sa/service/k;

    const-string v1, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/service/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 442
    :cond_2
    iget-object v0, p0, Lcom/msc/sa/service/m;->b:Lcom/msc/sa/service/k;

    iget-object v0, v0, Lcom/msc/sa/service/k;->a:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->d()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 444
    iget-object v0, p0, Lcom/msc/sa/service/m;->b:Lcom/msc/sa/service/k;

    const-string v1, "SAC_0302"

    const-string v2, "Error occurred while connecting to SSL"

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/service/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "GLCR"

    const-string v1, "[ExAIDL-GearLogService]SSL error occured"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 450
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "GLCR"

    const-string v4, "[ExAIDL-GearLogService]Server request fail. Internal error occured"

    invoke-static {v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    if-nez v2, :cond_4

    .line 454
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "GLCR"

    const-string v1, "[ExAIDL-GearLogService]content is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    iget-object v0, p0, Lcom/msc/sa/service/m;->b:Lcom/msc/sa/service/k;

    const-string v1, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/service/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 459
    :cond_4
    iget-object v3, p0, Lcom/msc/sa/service/m;->b:Lcom/msc/sa/service/k;

    invoke-static {v3, v0, v1, v2}, Lcom/msc/sa/service/k;->a(Lcom/msc/sa/service/k;JLjava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final c(Lcom/msc/b/c;)V
    .locals 0

    .prologue
    .line 412
    return-void
.end method
