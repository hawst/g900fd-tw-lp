.class public Lcom/msc/sa/service/RequestService;
.super Lcom/osp/app/util/AbstractBaseService;
.source "RequestService.java"


# instance fields
.field private a:Lcom/msc/sa/service/b;

.field private b:Lcom/msc/sa/service/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/osp/app/util/AbstractBaseService;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "RS"

    const-string v2, "OnBind"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    if-nez p1, :cond_0

    .line 38
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "RS"

    const-string v2, "Intent is null"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    :goto_0
    return-object v0

    .line 42
    :cond_0
    const-string v1, "com.msc.action.samsungaccount.REQUEST_SERVICE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 44
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RS"

    const-string v1, "[InAIDL] Service binded"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/msc/sa/service/RequestService;->a:Lcom/msc/sa/service/b;

    goto :goto_0

    .line 48
    :cond_1
    const-string v1, "com.msc.action.samsungaccount.REQUEST_EXPANDABLE_SERVICE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 50
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RS"

    const-string v1, "[ExAIDL] Service binded"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/msc/sa/service/RequestService;->b:Lcom/msc/sa/service/a;

    goto :goto_0

    .line 54
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "RS"

    const-string v2, "AIDL Nothing Binded return null"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0}, Lcom/osp/app/util/AbstractBaseService;->onCreate()V

    .line 63
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RS"

    const-string v1, "OnCreate Service"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    new-instance v0, Lcom/msc/sa/service/b;

    invoke-direct {v0, p0}, Lcom/msc/sa/service/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/msc/sa/service/RequestService;->a:Lcom/msc/sa/service/b;

    .line 65
    new-instance v0, Lcom/msc/sa/service/a;

    invoke-direct {v0, p0}, Lcom/msc/sa/service/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/msc/sa/service/RequestService;->b:Lcom/msc/sa/service/a;

    .line 66
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 71
    invoke-super {p0}, Lcom/osp/app/util/AbstractBaseService;->onDestroy()V

    .line 72
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RS"

    const-string v1, "OnDestroy Service"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    iput-object v2, p0, Lcom/msc/sa/service/RequestService;->a:Lcom/msc/sa/service/b;

    .line 74
    iput-object v2, p0, Lcom/msc/sa/service/RequestService;->b:Lcom/msc/sa/service/a;

    .line 75
    return-void
.end method
