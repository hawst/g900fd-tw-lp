.class public final Lcom/msc/sa/service/s;
.super Lcom/msc/sa/service/c;
.source "SCloudAccessTokenRunnable.java"


# instance fields
.field private d:Lcom/msc/sa/d/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 64
    const/4 v2, 0x5

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/msc/sa/service/c;-><init>(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;)V

    .line 65
    return-void
.end method

.method static synthetic a(Lcom/msc/sa/service/s;Ljava/lang/String;Lcom/msc/sa/service/e;)V
    .locals 8

    .prologue
    .line 38
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-SCloudAccessTokenRunnable]handleAccessTokenSuccess. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {p1}, Lcom/msc/c/h;->v(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/msc/sa/d/b;

    invoke-direct {v1, v0}, Lcom/msc/sa/d/b;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/msc/sa/service/s;->d:Lcom/msc/sa/d/b;

    invoke-virtual {p0}, Lcom/msc/sa/service/s;->a()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/msc/sa/service/s;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v1}, Lcom/msc/sa/d/a;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/sa/service/s;->d:Lcom/msc/sa/d/b;

    invoke-virtual {v2}, Lcom/msc/sa/d/b;->a()Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/msc/sa/service/s;->d:Lcom/msc/sa/d/b;

    invoke-virtual {v0}, Lcom/msc/sa/d/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/msc/c/f;

    const-string v1, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/msc/sa/service/s;->a:Lcom/msc/c/f;

    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/msc/sa/service/s;->a:Lcom/msc/c/f;

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/msc/sa/service/e;->a(Z)V

    goto :goto_1
.end method

.method private e()Z
    .locals 4

    .prologue
    .line 291
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-SCloudAccessTokenRunnable]checkTncMandatoryValidation. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    invoke-virtual {p0}, Lcom/msc/sa/service/s;->a()Landroid/content/Context;

    move-result-object v0

    .line 294
    if-nez v0, :cond_0

    .line 296
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-SCloudAccessTokenRunnable]Context is null. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/msc/sa/service/s;->a:Lcom/msc/c/f;

    .line 298
    const/4 v0, 0x0

    .line 309
    :goto_0
    return v0

    .line 301
    :cond_0
    iget-object v1, p0, Lcom/msc/sa/service/s;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v1}, Lcom/msc/sa/d/a;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/sa/service/s;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v2}, Lcom/msc/sa/d/a;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/msc/sa/service/s;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v3}, Lcom/msc/sa/d/a;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/msc/sa/c/o;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/msc/sa/d/h;

    move-result-object v0

    .line 304
    invoke-virtual {v0}, Lcom/msc/sa/d/h;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 306
    invoke-virtual {v0}, Lcom/msc/sa/d/h;->c()Lcom/msc/c/f;

    move-result-object v1

    iput-object v1, p0, Lcom/msc/sa/service/s;->a:Lcom/msc/c/f;

    .line 309
    :cond_1
    invoke-virtual {v0}, Lcom/msc/sa/d/h;->a()Z

    move-result v0

    goto :goto_0
.end method

.method private f()V
    .locals 7

    .prologue
    .line 481
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-SCloudAccessTokenRunnable]showReSignNotification. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    invoke-virtual {p0}, Lcom/msc/sa/service/s;->a()Landroid/content/Context;

    move-result-object v0

    .line 484
    if-eqz v0, :cond_0

    .line 487
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.msc.action.samsungaccount.resignin"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 488
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 489
    const-string v2, "client_id"

    iget-object v3, p0, Lcom/msc/sa/service/s;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v3}, Lcom/msc/sa/d/a;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 490
    const-string v2, "client_secret"

    iget-object v3, p0, Lcom/msc/sa/service/s;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v3}, Lcom/msc/sa/d/a;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 491
    const-string v2, "account_mode"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 492
    const-string v2, "OSP_VER"

    const-string v3, "OSP_02"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 493
    const-string v2, "BG_WhoareU"

    const-string v3, "BG_mode"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 495
    const-string v2, "email_id"

    invoke-static {v0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 496
    const-string v2, "from_notification"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 498
    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v0, v2, v1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 500
    const/4 v2, 0x0

    const v3, 0x7f09004f

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f09008d

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    .line 507
    :goto_0
    return-void

    .line 505
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-SCloudAccessTokenRunnable]Context is null. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(ZLcom/msc/sa/b/b;)V
    .locals 5

    .prologue
    .line 103
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-SCloudAccessTokenRunnable]onProcessFinished. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    if-eqz p1, :cond_1

    .line 107
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-SCloudAccessTokenRunnable]onServerProcessSuccessed start. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/msc/sa/b/b;->d()Lcom/msc/sa/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "access_token"

    iget-object v3, p0, Lcom/msc/sa/service/s;->d:Lcom/msc/sa/d/b;

    invoke-virtual {v3}, Lcom/msc/sa/d/b;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/s;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v2}, Lcom/msc/sa/d/a;->b()I

    move-result v2

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3, v1}, Lcom/msc/sa/a/a;->e(IZLandroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-SCloudAccessTokenRunnable]onServerProcessSuccessed finish. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    :goto_1
    return-void

    .line 107
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 110
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-SCloudAccessTokenRunnable]onServerProcessFailed start. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/msc/sa/b/b;->d()Lcom/msc/sa/a/a;

    move-result-object v0

    if-eqz v0, :cond_3

    :try_start_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "authcode"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/s;->a:Lcom/msc/c/f;

    if-nez v2, :cond_2

    new-instance v2, Lcom/msc/c/f;

    const-string v3, "SAC_0401"

    const-string v4, "Internal error occurred"

    invoke-direct {v2, v3, v4}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/msc/sa/service/s;->a:Lcom/msc/c/f;

    :cond_2
    const-string v2, "error_code"

    iget-object v3, p0, Lcom/msc/sa/service/s;->a:Lcom/msc/c/f;

    invoke-virtual {v3}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "error_message"

    iget-object v3, p0, Lcom/msc/sa/service/s;->a:Lcom/msc/c/f;

    invoke-virtual {v3}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "SATR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-SCloudAccessTokenRunnable] Process Failed Error_code "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/s;->a:Lcom/msc/c/f;

    invoke-virtual {v4}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Error_message "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/msc/sa/service/s;->a:Lcom/msc/c/f;

    invoke-virtual {v4}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Package = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/s;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v2}, Lcom/msc/sa/d/a;->b()I

    move-result v2

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3, v1}, Lcom/msc/sa/a/a;->e(IZLandroid/os/Bundle;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_3
    :goto_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-SCloudAccessTokenRunnable]onServerProcessFailed finish. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method public final b()Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v8, 0x0

    .line 183
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-SCloudAccessTokenRunnable]runProcess start. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-SCloudAccessTokenRunnable]checkSelfUpgradeIsDone. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/msc/sa/service/s;->a()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    const-string v3, "samsungaccount.FORCE_BACKGROUND_UPDATE"

    invoke-virtual {v2, v0, v8, v1, v3}, Lcom/msc/sa/selfupdate/c;->a(Landroid/app/Application;ZZLjava/lang/String;)V

    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/c;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-SCloudAccessTokenRunnable]NEED_SELF_UPGRADE. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "SAC_0203"

    const-string v2, "The upgrade process must be completed if you want to use Samsung account"

    invoke-virtual {p0, v0, v2}, Lcom/msc/sa/service/s;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    .line 188
    :goto_0
    if-nez v0, :cond_2

    .line 190
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-SCloudAccessTokenRunnable]need Self Upgrade. FIAL. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-SCloudAccessTokenRunnable]runProcess end. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    :goto_1
    return v8

    .line 184
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-SCloudAccessTokenRunnable] Upgrade check Success. SA version is the latest. mPackge = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/msc/c/f;

    const-string v2, "SAC_0401"

    const-string v3, "Internal error occurred"

    invoke-direct {v0, v2, v3}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/msc/sa/service/s;->a:Lcom/msc/c/f;

    move v0, v8

    goto :goto_0

    .line 195
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-SCloudAccessTokenRunnable]runSCloudAccessTokenProcess start. package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-SCloudAccessTokenRunnable]runAccessTokenCacheProcess. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/msc/sa/service/s;->a()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/msc/sa/service/s;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v2}, Lcom/msc/sa/d/a;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/util/ad;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v2, "key_accesstoken"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/msc/sa/service/s;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v0}, Lcom/msc/sa/d/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "SATR"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[InAIDL-SCloudAccessTokenRunnable]isAccessTokenReusable. Package = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/msc/sa/service/s;->b:Lcom/msc/sa/d/a;

    const-string v4, "expired_access_token"

    invoke-virtual {v3, v4}, Lcom/msc/sa/d/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p0}, Lcom/msc/sa/service/s;->a()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/osp/app/util/ad;->k(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v0, v1

    :goto_2
    if-ne v0, v1, :cond_5

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-SCloudAccessTokenRunnable]Reuse access token. Package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/msc/sa/d/b;

    invoke-direct {v0, v2}, Lcom/msc/sa/d/b;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/msc/sa/service/s;->d:Lcom/msc/sa/d/b;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-SCloudAccessTokenRunnable]Success to get cache accesstoken. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    :goto_3
    if-ne v0, v1, :cond_6

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-SCloudAccessTokenRunnable]runSCloudAccessTokenProcess end. package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/msc/sa/service/s;->e()Z

    move-result v0

    .line 196
    :goto_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SATR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-SCloudAccessTokenRunnable]runProcess end. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v8, v0

    .line 197
    goto/16 :goto_1

    .line 195
    :cond_3
    invoke-virtual {p0}, Lcom/msc/sa/service/s;->a()Landroid/content/Context;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-static {v3, v0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;Ljava/lang/String;)V

    :cond_4
    move v0, v8

    goto/16 :goto_2

    :cond_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-SCloudAccessTokenRunnable]should request accessToken. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    goto :goto_3

    :cond_6
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-SCloudAccessTokenRunnable]runAccessTokenRequestProcess. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/msc/sa/service/s;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v8}, Lcom/msc/c/c;->a(Landroid/content/Context;Z)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-direct {p0}, Lcom/msc/sa/service/s;->f()V

    new-instance v0, Lcom/msc/c/f;

    const-string v1, "SAC_0402"

    const-string v2, "Auth token expired"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/msc/sa/service/s;->a:Lcom/msc/c/f;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-SCloudAccessTokenRunnable]check authToken FAIL. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    :goto_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SATR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-SCloudAccessTokenRunnable]runSCloudAccessTokenProcess end. package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_7
    invoke-direct {p0}, Lcom/msc/sa/service/s;->e()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-SCloudAccessTokenRunnable]Email validation check fail. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    goto :goto_5

    :cond_8
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-SCloudAccessTokenRunnable]requestAccessToken. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, Lcom/msc/sa/service/e;

    invoke-direct {v9}, Lcom/msc/sa/service/e;-><init>()V

    invoke-virtual {p0}, Lcom/msc/sa/service/s;->a()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "SATR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-SCloudAccessTokenRunnable]getRedirectUrl. Package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, ""

    invoke-virtual {p0}, Lcom/msc/sa/service/s;->a()Landroid/content/Context;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-static {v2}, Lcom/osp/app/util/r;->d(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/msc/sa/service/s;->b:Lcom/msc/sa/d/a;

    const-string v3, "redirect_url_chn"

    invoke-virtual {v2, v3}, Lcom/msc/sa/d/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :cond_9
    :goto_6
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "SATR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-SCloudAccessTokenRunnable]RedirectUrl :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_b

    const-string v0, "Param [%s] must not be null"

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "redirect_url_default , redirect_url_chn"

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/msc/c/f;

    const-string v2, "SAC_0101"

    invoke-direct {v1, v2, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/msc/sa/service/s;->a:Lcom/msc/c/f;

    move v0, v8

    :goto_7
    if-nez v0, :cond_d

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-SCloudAccessTokenRunnable]request Authcode FAIL. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    goto/16 :goto_5

    :cond_a
    iget-object v2, p0, Lcom/msc/sa/service/s;->b:Lcom/msc/sa/d/a;

    const-string v3, "redirect_url_default"

    invoke-virtual {v2, v3}, Lcom/msc/sa/d/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_6

    :cond_b
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v1, p0, Lcom/msc/sa/service/s;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v1}, Lcom/msc/sa/d/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "REQUEST_ACCESSTOKEN"

    iget-object v4, p0, Lcom/msc/sa/service/s;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v4}, Lcom/msc/sa/d/a;->e()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    new-instance v7, Lcom/msc/sa/service/t;

    invoke-direct {v7, p0, v9}, Lcom/msc/sa/service/t;-><init>(Lcom/msc/sa/service/s;Lcom/msc/sa/service/e;)V

    invoke-static/range {v0 .. v7}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/msc/sa/service/s;->a(JLjava/lang/String;)V

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    :goto_8
    invoke-virtual {v9}, Lcom/msc/sa/service/e;->a()Z

    move-result v0

    goto :goto_7

    :cond_c
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/msc/sa/service/s;->a:Lcom/msc/c/f;

    goto :goto_8

    :cond_d
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SATR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-SCloudAccessTokenRunnable]success to get accesstoken from server. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5
.end method

.method protected final d()V
    .locals 7

    .prologue
    .line 371
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-SCloudAccessTokenRunnable]handleAccessTokenFail. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    const-string v0, "AUT_1302"

    iget-object v1, p0, Lcom/msc/sa/service/s;->a:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "AUT_1830"

    iget-object v1, p0, Lcom/msc/sa/service/s;->a:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 375
    :cond_0
    invoke-virtual {p0}, Lcom/msc/sa/service/s;->a()Landroid/content/Context;

    move-result-object v0

    .line 376
    if-eqz v0, :cond_1

    .line 378
    invoke-static {v0}, Lcom/msc/c/c;->g(Landroid/content/Context;)V

    .line 379
    invoke-static {v0}, Lcom/msc/c/e;->a(Landroid/content/Context;)Z

    .line 381
    :cond_1
    invoke-direct {p0}, Lcom/msc/sa/service/s;->f()V

    .line 382
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "SAC_0402"

    const-string v2, "Auth token expired"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/msc/sa/service/s;->a:Lcom/msc/c/f;

    .line 388
    :cond_2
    :goto_0
    return-void

    .line 383
    :cond_3
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "AUT_1094"

    iget-object v1, p0, Lcom/msc/sa/service/s;->a:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 385
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-SCloudAccessTokenRunnable]showReSignWithSignOutNotification. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/msc/sa/service/s;->a()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_4

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.msc.action.samsungaccount.resignin_with_signout"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v2, "client_id"

    iget-object v3, p0, Lcom/msc/sa/service/s;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v3}, Lcom/msc/sa/d/a;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "client_secret"

    iget-object v3, p0, Lcom/msc/sa/service/s;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v3}, Lcom/msc/sa/d/a;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "account_mode"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "OSP_VER"

    const-string v3, "OSP_02"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "BG_WhoareU"

    const-string v3, "BG_mode"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "from_notification"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v0, v2, v1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const/4 v2, 0x0

    const v3, 0x7f09004f

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f09008d

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-SCloudAccessTokenRunnable]Context is null. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/s;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
