.class final Lcom/msc/sa/service/h;
.super Lcom/msc/c/b;
.source "AccessTokenRunnable.java"


# instance fields
.field final synthetic c:Lcom/msc/sa/service/g;

.field private d:J

.field private e:J

.field private f:J

.field private final g:Lcom/msc/sa/b/b;

.field private h:Z

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/msc/sa/service/g;Landroid/content/Context;Lcom/msc/sa/b/b;)V
    .locals 1

    .prologue
    .line 582
    iput-object p1, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    .line 583
    invoke-direct {p0, p2}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 570
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/msc/sa/service/h;->h:Z

    .line 584
    invoke-virtual {p0}, Lcom/msc/sa/service/h;->c()V

    .line 585
    iput-object p3, p0, Lcom/msc/sa/service/h;->g:Lcom/msc/sa/b/b;

    .line 586
    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    .line 751
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 752
    iget-object v0, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    invoke-virtual {v0}, Lcom/msc/sa/service/g;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "j5p7ll8g33"

    iget-object v2, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    invoke-virtual {v2}, Lcom/msc/sa/service/g;->a()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    invoke-virtual {v3}, Lcom/msc/sa/service/g;->a()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/msc/c/e;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3, p0}, Lcom/msc/c/g;->i(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/msc/sa/service/h;->d:J

    .line 754
    iget-wide v0, p0, Lcom/msc/sa/service/h;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/msc/sa/service/h;->a(J)V

    .line 755
    iget-wide v0, p0, Lcom/msc/sa/service/h;->d:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/msc/sa/service/h;->a(JLjava/lang/String;)V

    .line 756
    iget-wide v0, p0, Lcom/msc/sa/service/h;->d:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 757
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 615
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AccessTokenRunnable]SkipEmailValidtionTask doInBackground. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    iget-object v2, v2, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    invoke-direct {p0}, Lcom/msc/sa/service/h;->h()V

    .line 618
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 630
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V

    .line 631
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AccessTokenRunnable]SkipEmailValidtionTask onRequestSuccess. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    iget-object v2, v2, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 632
    if-nez p1, :cond_1

    .line 693
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 637
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 638
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 640
    iget-wide v4, p0, Lcom/msc/sa/service/h;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_3

    .line 644
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->O(Ljava/lang/String;)Z

    move-result v0

    .line 645
    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 647
    const-string v0, "Success"

    iput-object v0, p0, Lcom/msc/sa/service/h;->i:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 653
    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 656
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/msc/sa/service/h;->i:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 630
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 650
    :cond_2
    :try_start_4
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/msc/sa/service/h;->i:Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 659
    :cond_3
    :try_start_5
    iget-wide v4, p0, Lcom/msc/sa/service/h;->e:J
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    .line 663
    :try_start_6
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 664
    invoke-virtual {p0}, Lcom/msc/sa/service/h;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 668
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    invoke-virtual {v0}, Lcom/msc/sa/service/g;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "authorization_code"

    const-string v3, "j5p7ll8g33"

    const-string v4, "5763D0052DC1462E13751F753384E9A9"

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/msc/sa/service/h;->f:J

    iget-wide v0, p0, Lcom/msc/sa/service/h;->f:J

    invoke-virtual {p0, v0, v1}, Lcom/msc/sa/service/h;->a(J)V

    iget-wide v0, p0, Lcom/msc/sa/service/h;->f:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/msc/sa/service/h;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/msc/sa/service/h;->f:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 669
    :catch_1
    move-exception v0

    .line 671
    :try_start_7
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 672
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/msc/sa/service/h;->b:Lcom/msc/c/f;

    .line 673
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/msc/sa/service/h;->i:Ljava/lang/String;

    goto :goto_0

    .line 675
    :cond_4
    iget-wide v4, p0, Lcom/msc/sa/service/h;->f:J
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 679
    :try_start_8
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    invoke-virtual {v0}, Lcom/msc/sa/service/g;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 680
    iget-object v1, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    invoke-virtual {v1}, Lcom/msc/sa/service/g;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 681
    invoke-virtual {p0}, Lcom/msc/sa/service/h;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 685
    invoke-direct {p0}, Lcom/msc/sa/service/h;->h()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 686
    :catch_2
    move-exception v0

    .line 688
    :try_start_9
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 689
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/msc/sa/service/h;->b:Lcom/msc/c/f;

    .line 690
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/msc/sa/service/h;->i:Ljava/lang/String;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 590
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 591
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AccessTokenRunnable]SkipEmailValidtionTask onPostExecute. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    iget-object v2, v2, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    const-string v0, "Fail"

    iget-object v1, p0, Lcom/msc/sa/service/h;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 595
    iget-object v0, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    iget-object v1, p0, Lcom/msc/sa/service/h;->g:Lcom/msc/sa/b/b;

    invoke-static {v0, v1}, Lcom/msc/sa/service/g;->a(Lcom/msc/sa/service/g;Lcom/msc/sa/b/b;)V

    .line 611
    :cond_0
    :goto_0
    return-void

    .line 597
    :cond_1
    const-string v0, "Success"

    iget-object v1, p0, Lcom/msc/sa/service/h;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 599
    iget-object v0, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    invoke-virtual {v0}, Lcom/msc/sa/service/g;->a()Landroid/content/Context;

    move-result-object v1

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.osp.app.signin.action.EMAIL_VALIDATION_COMPLETED"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->l()I

    move-result v2

    const/16 v3, 0xb

    if-le v2, v3, :cond_2

    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_2
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const-string v0, "notification"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const v2, 0x132df82

    invoke-virtual {v0, v2}, Landroid/app/NotificationManager;->cancel(I)V

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.msc.action.MARKETING_POPUP_NOTIFICATION"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "com.osp.app.signin"

    const-string v3, "com.osp.app.signin.BackgroundModeService"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    const-string v1, "startService MarketingPopupNotiIntent"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    :cond_3
    iget-object v0, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    iget-object v0, v0, Lcom/msc/sa/service/g;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v0}, Lcom/msc/sa/d/a;->f()Landroid/os/Bundle;

    move-result-object v0

    .line 602
    const-string v1, "key_retry_skip_email_validation"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 603
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/msc/sa/service/g;

    iget-object v3, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    invoke-virtual {v3}, Lcom/msc/sa/service/g;->a()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    iget-object v4, v4, Lcom/msc/sa/service/g;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v4}, Lcom/msc/sa/d/a;->b()I

    move-result v4

    iget-object v5, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    iget-object v5, v5, Lcom/msc/sa/service/g;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v5}, Lcom/msc/sa/d/a;->a()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5, v0}, Lcom/msc/sa/service/g;-><init>(Landroid/content/Context;ILjava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 604
    :cond_4
    const-string v0, "require re-SignIn"

    iget-object v1, p0, Lcom/msc/sa/service/h;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 606
    iget-object v0, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    iget-object v1, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    invoke-virtual {v1}, Lcom/msc/sa/service/g;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    iget-object v2, v2, Lcom/msc/sa/service/g;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v2}, Lcom/msc/sa/d/a;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    iget-object v3, v3, Lcom/msc/sa/service/g;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v3}, Lcom/msc/sa/d/a;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/msc/sa/service/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    const-string v0, "SAC_0402"

    const-string v1, "Auth token expired"

    invoke-virtual {p0, v0, v1}, Lcom/msc/sa/service/h;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    iget-object v0, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    iget-object v1, p0, Lcom/msc/sa/service/h;->g:Lcom/msc/sa/b/b;

    invoke-static {v0, v1}, Lcom/msc/sa/service/g;->a(Lcom/msc/sa/service/g;Lcom/msc/sa/b/b;)V

    goto/16 :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 697
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 698
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AccessTokenRunnable]SkipEmailValidtionTask onRequestFail. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    iget-object v2, v2, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    if-nez p1, :cond_1

    .line 745
    :cond_0
    :goto_0
    return-void

    .line 704
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 707
    iget-wide v2, p0, Lcom/msc/sa/service/h;->d:J

    cmp-long v2, v0, v2

    if-nez v2, :cond_3

    .line 709
    iget-boolean v0, p0, Lcom/msc/sa/service/h;->h:Z

    if-nez v0, :cond_2

    const-string v0, "ACF_0403"

    iget-object v1, p0, Lcom/msc/sa/service/h;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 713
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/msc/sa/service/h;->b:Lcom/msc/c/f;

    .line 714
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/msc/sa/service/h;->h:Z

    .line 715
    iget-object v0, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    invoke-virtual {v0}, Lcom/msc/sa/service/g;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 716
    invoke-virtual {p0}, Lcom/msc/sa/service/h;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 720
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    invoke-virtual {v0}, Lcom/msc/sa/service/g;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "j5p7ll8g33"

    iget-object v2, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    invoke-virtual {v2}, Lcom/msc/sa/service/g;->a()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/msc/sa/service/h;->e:J

    iget-wide v0, p0, Lcom/msc/sa/service/h;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/msc/sa/service/h;->a(J)V

    iget-wide v0, p0, Lcom/msc/sa/service/h;->e:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/msc/sa/service/h;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/msc/sa/service/h;->e:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 721
    :catch_0
    move-exception v0

    .line 723
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 724
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/msc/sa/service/h;->b:Lcom/msc/c/f;

    .line 725
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/msc/sa/service/h;->i:Ljava/lang/String;

    goto :goto_0

    .line 729
    :cond_2
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/msc/sa/service/h;->i:Ljava/lang/String;

    goto :goto_0

    .line 732
    :cond_3
    iget-wide v2, p0, Lcom/msc/sa/service/h;->e:J

    cmp-long v2, v0, v2

    if-nez v2, :cond_5

    .line 734
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/msc/sa/service/h;->i:Ljava/lang/String;

    .line 736
    const-string v0, "AUT_1302"

    iget-object v1, p0, Lcom/msc/sa/service/h;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "AUT_1830"

    iget-object v1, p0, Lcom/msc/sa/service/h;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 738
    :cond_4
    const-string v0, "require re-SignIn"

    iput-object v0, p0, Lcom/msc/sa/service/h;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 741
    :cond_5
    iget-wide v2, p0, Lcom/msc/sa/service/h;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 743
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/msc/sa/service/h;->i:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method protected final d()V
    .locals 3

    .prologue
    .line 623
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AccessTokenRunnable]SkipEmailValidtionTask cancelTask. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/h;->c:Lcom/msc/sa/service/g;

    iget-object v2, v2, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 626
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 548
    invoke-virtual {p0}, Lcom/msc/sa/service/h;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 548
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/msc/sa/service/h;->a(Ljava/lang/Boolean;)V

    return-void
.end method
