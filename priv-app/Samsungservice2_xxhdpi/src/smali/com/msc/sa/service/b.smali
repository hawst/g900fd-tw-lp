.class public final Lcom/msc/sa/service/b;
.super Lcom/msc/sa/a/j;
.source "AIDLInterfaceBinder.java"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/msc/sa/a/j;-><init>()V

    .line 42
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/msc/sa/service/b;->a:Ljava/lang/ref/WeakReference;

    .line 43
    return-void
.end method

.method private static a(Ljava/lang/String;IILjava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 354
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIB"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL]sendParameterEmptyFailCallback - emptyParam :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    invoke-static {}, Lcom/msc/sa/b/a;->a()Lcom/msc/sa/b/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/msc/sa/b/a;->b(Ljava/lang/String;)Lcom/msc/sa/b/b;

    move-result-object v0

    .line 357
    if-nez v0, :cond_0

    .line 359
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIB"

    const-string v1, "[InAIDL]callbackinfo is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    :goto_0
    return-void

    .line 364
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 365
    const-string v2, "Param [%s] must not be null"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p3, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 366
    const-string v3, "error_code"

    const-string v4, "SAC_0101"

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    const-string v3, "error_message"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 376
    :pswitch_0
    :try_start_0
    invoke-virtual {v0}, Lcom/msc/sa/b/b;->d()Lcom/msc/sa/a/a;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, p2, v2, v1}, Lcom/msc/sa/a/a;->e(IZLandroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 384
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 379
    :pswitch_1
    :try_start_1
    invoke-virtual {v0}, Lcom/msc/sa/b/b;->d()Lcom/msc/sa/a/a;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, p2, v2, v1}, Lcom/msc/sa/a/a;->f(IZLandroid/os/Bundle;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 373
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(ILjava/lang/String;I)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 272
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AIB"

    const-string v2, "[InAIDL]checkRequestCondition "

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    invoke-static {}, Lcom/msc/sa/b/a;->a()Lcom/msc/sa/b/a;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/msc/sa/b/a;->b(Ljava/lang/String;)Lcom/msc/sa/b/b;

    move-result-object v1

    .line 276
    if-nez v1, :cond_1

    .line 278
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AIB"

    const-string v2, "[InAIDL]invalid registrationCode ERROR"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    :cond_0
    :goto_0
    return v0

    .line 282
    :cond_1
    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v2

    invoke-virtual {v1}, Lcom/msc/sa/b/b;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 285
    invoke-virtual {v1}, Lcom/msc/sa/b/b;->e()I

    move-result v3

    if-eqz v3, :cond_3

    .line 287
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "AIB"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[InAIDL]Request is running. service App can request after finishing of current request. package = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    const-string v2, "SAC_0501"

    const-string v3, "Request Fail. Should request after finishing of current request."

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v4, "AIB"

    const-string v5, "[InAIDL]sendRequestFailCallback"

    invoke-static {v4, v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v1, :cond_2

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AIB"

    const-string v2, "[InAIDL]callbackinfo is null"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v5, "error_code"

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "error_message"

    invoke-virtual {v4, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    packed-switch p3, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    :try_start_0
    invoke-virtual {v1}, Lcom/msc/sa/b/b;->d()Lcom/msc/sa/a/a;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, p1, v2, v4}, Lcom/msc/sa/a/a;->a(IZLandroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    :try_start_1
    invoke-virtual {v1}, Lcom/msc/sa/b/b;->d()Lcom/msc/sa/a/a;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, p1, v2, v4}, Lcom/msc/sa/a/a;->b(IZLandroid/os/Bundle;)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {v1}, Lcom/msc/sa/b/b;->d()Lcom/msc/sa/a/a;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, p1, v2, v4}, Lcom/msc/sa/a/a;->c(IZLandroid/os/Bundle;)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {v1}, Lcom/msc/sa/b/b;->d()Lcom/msc/sa/a/a;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, p1, v2, v4}, Lcom/msc/sa/a/a;->d(IZLandroid/os/Bundle;)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {v1}, Lcom/msc/sa/b/b;->d()Lcom/msc/sa/a/a;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, p1, v2, v4}, Lcom/msc/sa/a/a;->e(IZLandroid/os/Bundle;)V

    goto/16 :goto_0

    :pswitch_5
    invoke-virtual {v1}, Lcom/msc/sa/b/b;->d()Lcom/msc/sa/a/a;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, p1, v2, v4}, Lcom/msc/sa/a/a;->f(IZLandroid/os/Bundle;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 294
    :cond_3
    iget-object v1, p0, Lcom/msc/sa/service/b;->a:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/msc/sa/service/b;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 299
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 288
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/sa/a/a;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 48
    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 50
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIB"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL]registerCallback start. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/msc/sa/service/b;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/msc/sa/service/b;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/msc/sa/service/b;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 53
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIB"

    const-string v1, "[InAIDL]context is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const/4 v0, 0x0

    .line 60
    :goto_0
    return-object v0

    .line 57
    :cond_1
    invoke-static {}, Lcom/msc/sa/b/a;->a()Lcom/msc/sa/b/a;

    move-result-object v0

    iget-object v1, p0, Lcom/msc/sa/service/b;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/msc/sa/b/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/sa/a/a;)Ljava/lang/String;

    move-result-object v0

    .line 59
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AIB"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL]registerCallback end. registrationCode :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Package = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(ILjava/lang/String;Landroid/os/Bundle;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 76
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIB"

    const-string v2, "[InAIDL]requestAccesstoken"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {p3}, Lcom/osp/app/util/aq;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    .line 79
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "AIB"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL] BundleInfo = ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    invoke-direct {p0, p1, p2, v1}, Lcom/msc/sa/service/b;->a(ILjava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    const/4 v0, 0x0

    .line 93
    :goto_0
    return v0

    .line 87
    :cond_0
    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v0

    invoke-static {}, Lcom/msc/sa/b/a;->a()Lcom/msc/sa/b/a;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/msc/sa/b/a;->b(Ljava/lang/String;)Lcom/msc/sa/b/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/msc/sa/b/b;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 90
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "AIB"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL]Start AccessTokenRunnable Thread. package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/msc/sa/service/g;

    iget-object v0, p0, Lcom/msc/sa/service/b;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v3, v0, p1, p2, p3}, Lcom/msc/sa/service/g;-><init>(Landroid/content/Context;ILjava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    move v0, v1

    .line 93
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 67
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIB"

    const-string v1, "[InAIDL]unregistCallback start "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    invoke-static {}, Lcom/msc/sa/b/a;->a()Lcom/msc/sa/b/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/msc/sa/b/a;->a(Ljava/lang/String;)Z

    move-result v0

    .line 69
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AIB"

    const-string v2, "[InAIDL]unregistCallback end "

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    return v0
.end method

.method public final b(ILjava/lang/String;Landroid/os/Bundle;)Z
    .locals 7

    .prologue
    const/4 v4, 0x2

    .line 99
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIB"

    const-string v1, "[InAIDL]requestChecklistValidation"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {p3}, Lcom/osp/app/util/aq;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    .line 101
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AIB"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL] BundleInfo = ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    invoke-direct {p0, p1, p2, v4}, Lcom/msc/sa/service/b;->a(ILjava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 105
    const/4 v0, 0x0

    .line 115
    :goto_0
    return v0

    .line 109
    :cond_0
    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v0

    invoke-static {}, Lcom/msc/sa/b/a;->a()Lcom/msc/sa/b/a;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/msc/sa/b/a;->b(Ljava/lang/String;)Lcom/msc/sa/b/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/sa/b/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AIB"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL] Start ValidationRunnalbe Thread. package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    new-instance v6, Ljava/lang/Thread;

    new-instance v0, Lcom/msc/sa/service/u;

    iget-object v1, p0, Lcom/msc/sa/service/b;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    move v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/msc/sa/service/u;-><init>(Landroid/content/Context;ILjava/lang/String;ILandroid/os/Bundle;)V

    invoke-direct {v6, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 115
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c(ILjava/lang/String;Landroid/os/Bundle;)Z
    .locals 7

    .prologue
    const/4 v4, 0x3

    .line 121
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIB"

    const-string v1, "[InAIDL]requestDisclaimerAgreement"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {p3}, Lcom/osp/app/util/aq;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    .line 123
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AIB"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL] BundleInfo = ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-direct {p0, p1, p2, v4}, Lcom/msc/sa/service/b;->a(ILjava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    const/4 v0, 0x0

    .line 137
    :goto_0
    return v0

    .line 131
    :cond_0
    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v0

    invoke-static {}, Lcom/msc/sa/b/a;->a()Lcom/msc/sa/b/a;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/msc/sa/b/a;->b(Ljava/lang/String;)Lcom/msc/sa/b/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/sa/b/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 134
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AIB"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL] Start DisclaimerRunnalbe Thread. package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    new-instance v6, Ljava/lang/Thread;

    new-instance v0, Lcom/msc/sa/service/u;

    iget-object v1, p0, Lcom/msc/sa/service/b;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    move v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/msc/sa/service/u;-><init>(Landroid/content/Context;ILjava/lang/String;ILandroid/os/Bundle;)V

    invoke-direct {v6, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 137
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final d(ILjava/lang/String;Landroid/os/Bundle;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 143
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIB"

    const-string v2, "[InAIDL]requestAuthCode"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {p3}, Lcom/osp/app/util/aq;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    .line 145
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "AIB"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL] BundleInfo = ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const/4 v0, 0x4

    invoke-direct {p0, p1, p2, v0}, Lcom/msc/sa/service/b;->a(ILjava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 149
    const/4 v0, 0x0

    .line 172
    :goto_0
    return v0

    .line 160
    :cond_0
    if-eqz p3, :cond_1

    .line 162
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIB"

    const-string v2, "[InAIDL]checkReplaceableClientInfoForRequestAuthCode"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "replaceable_client_id"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "replaceable_client_secret"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIB"

    const-string v2, "[InAIDL]Signature check Success about replaceable information."

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "authcode_replaceable"

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 166
    :cond_1
    :goto_1
    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v0

    invoke-static {}, Lcom/msc/sa/b/a;->a()Lcom/msc/sa/b/a;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/msc/sa/b/a;->b(Ljava/lang/String;)Lcom/msc/sa/b/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/msc/sa/b/b;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 169
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "AIB"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL] Start AuthCodeRunnalbe Thread. package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/msc/sa/service/i;

    iget-object v0, p0, Lcom/msc/sa/service/b;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v3, v0, p1, p2, p3}, Lcom/msc/sa/service/i;-><init>(Landroid/content/Context;ILjava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    move v0, v1

    .line 172
    goto :goto_0

    .line 162
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIB"

    const-string v2, "[InAIDL]replaceableClientID or replaceableClientSecret is empty. Run with registered ClientID & Secret"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final e(ILjava/lang/String;Landroid/os/Bundle;)Z
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v0, 0x0

    .line 178
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AIB"

    const-string v2, "[InAIDL]requestSCloudAccessToken"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {p3}, Lcom/osp/app/util/aq;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    .line 180
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "AIB"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL] BundleInfo = ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "]"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    invoke-direct {p0, p1, p2, v5}, Lcom/msc/sa/service/b;->a(ILjava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 207
    :goto_0
    return v0

    .line 188
    :cond_0
    if-nez p3, :cond_1

    .line 190
    const-string v1, "data"

    invoke-static {p2, v5, p1, v1}, Lcom/msc/sa/service/b;->a(Ljava/lang/String;IILjava/lang/String;)V

    goto :goto_0

    .line 193
    :cond_1
    const-string v1, "redirect_url_default"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "redirect_url_chn"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 195
    :cond_2
    const-string v1, "redirect_url_default , redirect_url_chn"

    invoke-static {p2, v5, p1, v1}, Lcom/msc/sa/service/b;->a(Ljava/lang/String;IILjava/lang/String;)V

    goto :goto_0

    .line 201
    :cond_3
    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v0

    invoke-static {}, Lcom/msc/sa/b/a;->a()Lcom/msc/sa/b/a;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/msc/sa/b/a;->b(Ljava/lang/String;)Lcom/msc/sa/b/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/sa/b/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 204
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AIB"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL] Start SCloudAccessTokenRunnalbe Thread. package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/msc/sa/service/s;

    iget-object v0, p0, Lcom/msc/sa/service/b;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v2, v0, p1, p2, p3}, Lcom/msc/sa/service/s;-><init>(Landroid/content/Context;ILjava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 207
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final f(ILjava/lang/String;Landroid/os/Bundle;)Z
    .locals 6

    .prologue
    const/4 v5, 0x6

    const/4 v0, 0x0

    .line 212
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AIB"

    const-string v2, "[InAIDL]requestPasswordConfirmation "

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {p3}, Lcom/osp/app/util/aq;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    .line 214
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "AIB"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL] BundleInfo = ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "]"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    invoke-direct {p0, p1, p2, v5}, Lcom/msc/sa/service/b;->a(ILjava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 241
    :goto_0
    return v0

    .line 222
    :cond_0
    if-nez p3, :cond_1

    .line 224
    const-string v1, "data"

    invoke-static {p2, v5, p1, v1}, Lcom/msc/sa/service/b;->a(Ljava/lang/String;IILjava/lang/String;)V

    goto :goto_0

    .line 227
    :cond_1
    const-string v1, "password"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 229
    const-string v1, "password"

    invoke-static {p2, v5, p1, v1}, Lcom/msc/sa/service/b;->a(Ljava/lang/String;IILjava/lang/String;)V

    goto :goto_0

    .line 235
    :cond_2
    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v0

    invoke-static {}, Lcom/msc/sa/b/a;->a()Lcom/msc/sa/b/a;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/msc/sa/b/a;->b(Ljava/lang/String;)Lcom/msc/sa/b/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/sa/b/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 238
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AIB"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL] Start PasswordConfirmationRunnalbe Thread. package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/msc/sa/service/n;

    iget-object v0, p0, Lcom/msc/sa/service/b;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v2, v0, p1, p2, p3}, Lcom/msc/sa/service/n;-><init>(Landroid/content/Context;ILjava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 241
    const/4 v0, 0x1

    goto :goto_0
.end method
