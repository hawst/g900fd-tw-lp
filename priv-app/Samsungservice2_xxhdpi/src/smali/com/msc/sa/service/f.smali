.class public Lcom/msc/sa/service/f;
.super Ljava/lang/Object;
.source "AbstractProcessRunnable.java"

# interfaces
.implements Lcom/msc/b/h;


# instance fields
.field final synthetic a:Lcom/msc/sa/service/c;


# direct methods
.method protected constructor <init>(Lcom/msc/sa/service/c;)V
    .locals 0

    .prologue
    .line 431
    iput-object p1, p0, Lcom/msc/sa/service/f;->a:Lcom/msc/sa/service/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/msc/b/c;)V
    .locals 3

    .prologue
    .line 435
    if-nez p1, :cond_0

    .line 437
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "APR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AbstractProcessRunnable]onRequestSuccess response Message is null. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/f;->a:Lcom/msc/sa/service/c;

    iget-object v2, v2, Lcom/msc/sa/service/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    iget-object v0, p0, Lcom/msc/sa/service/f;->a:Lcom/msc/sa/service/c;

    const-string v1, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/service/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    :cond_0
    return-void
.end method

.method public b(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 450
    if-nez p1, :cond_1

    .line 452
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "APR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AbstractProcessRunnable]onRequestfail responseMessage is null. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/f;->a:Lcom/msc/sa/service/c;

    iget-object v2, v2, Lcom/msc/sa/service/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    iget-object v0, p0, Lcom/msc/sa/service/f;->a:Lcom/msc/sa/service/c;

    const-string v1, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/service/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    :cond_0
    :goto_0
    return-void

    .line 457
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 458
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 459
    invoke-virtual {p1}, Lcom/msc/b/c;->f()Ljava/lang/Exception;

    move-result-object v3

    .line 461
    if-eqz v3, :cond_3

    .line 463
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "APR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AbstractProcessRunnable]Server request fail. Exception occured. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/f;->a:Lcom/msc/sa/service/c;

    iget-object v2, v2, Lcom/msc/sa/service/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    iget-object v0, p0, Lcom/msc/sa/service/f;->a:Lcom/msc/sa/service/c;

    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v3}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, v0, Lcom/msc/sa/service/c;->a:Lcom/msc/c/f;

    .line 467
    iget-object v0, p0, Lcom/msc/sa/service/f;->a:Lcom/msc/sa/service/c;

    invoke-virtual {v0}, Lcom/msc/sa/service/c;->a()Landroid/content/Context;

    move-result-object v0

    .line 468
    if-nez v0, :cond_2

    .line 470
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "APR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AbstractProcessRunnable]Context is null. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/f;->a:Lcom/msc/sa/service/c;

    iget-object v2, v2, Lcom/msc/sa/service/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    iget-object v0, p0, Lcom/msc/sa/service/f;->a:Lcom/msc/sa/service/c;

    const-string v1, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/service/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 475
    :cond_2
    iget-object v0, p0, Lcom/msc/sa/service/f;->a:Lcom/msc/sa/service/c;

    iget-object v0, v0, Lcom/msc/sa/service/c;->a:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->d()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 477
    iget-object v0, p0, Lcom/msc/sa/service/f;->a:Lcom/msc/sa/service/c;

    const-string v1, "SAC_0302"

    const-string v2, "Error occurred while connecting to SSL"

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/service/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "APR"

    const-string v1, "[InAIDL-AbstractProcessRunnable]SSL error occured"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 483
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "APR"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[InAIDL-AbstractProcessRunnable]Server request fail. Internal error occured. Package = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/msc/sa/service/f;->a:Lcom/msc/sa/service/c;

    iget-object v5, v5, Lcom/msc/sa/service/c;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    if-nez v2, :cond_4

    .line 487
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "APR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AbstractProcessRunnable]content is null. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/f;->a:Lcom/msc/sa/service/c;

    iget-object v2, v2, Lcom/msc/sa/service/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    iget-object v0, p0, Lcom/msc/sa/service/f;->a:Lcom/msc/sa/service/c;

    const-string v1, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/service/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 492
    :cond_4
    iget-object v3, p0, Lcom/msc/sa/service/f;->a:Lcom/msc/sa/service/c;

    invoke-static {v3, v0, v1, v2}, Lcom/msc/sa/service/c;->a(Lcom/msc/sa/service/c;JLjava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final c(Lcom/msc/b/c;)V
    .locals 0

    .prologue
    .line 445
    return-void
.end method
