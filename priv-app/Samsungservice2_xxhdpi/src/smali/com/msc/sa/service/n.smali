.class public final Lcom/msc/sa/service/n;
.super Lcom/msc/sa/service/c;
.source "PasswordConfirmationRunnable.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 55
    const/4 v2, 0x6

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/msc/sa/service/c;-><init>(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;)V

    .line 56
    return-void
.end method

.method static synthetic a(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 39
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    const-string v1, "[InAIDL-PasswordConfirmRunnable]show NonExistentIDNotification"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p0, :cond_0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    const-string v1, "[InAIDL-PasswordConfirmRunnable]showNonExistentIDNotification context is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-static {p0, v2, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const/4 v2, 0x0

    const v0, 0x7f09004f

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f0900c0

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    const-string v1, "[InAIDL-PasswordConfirmRunnable]show NonExistentIDNotification End"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/msc/sa/service/n;Ljava/lang/String;Lcom/msc/sa/service/e;)V
    .locals 3

    .prologue
    .line 39
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-PasswordConfirmRunnable]handlePasswordConfirmationSuccess. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/msc/sa/service/n;->a()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_0
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {p1}, Lcom/msc/c/h;->C(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/msc/sa/service/e;->a(Z)V

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-PasswordConfirmRunnable]Confirm Success. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "SAC_0207"

    const-string v2, "Incorrect password"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/msc/sa/service/n;->a:Lcom/msc/c/f;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-PasswordConfirmRunnable]Confirm Fail. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/msc/sa/service/n;->a:Lcom/msc/c/f;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/msc/sa/service/n;->a:Lcom/msc/c/f;

    goto :goto_0
.end method

.method static synthetic a(Lcom/msc/sa/service/n;Ljava/lang/String;Lcom/msc/sa/service/e;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 39
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-PasswordConfirmRunnable]handleCheckUserStateSuccess. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {p1}, Lcom/msc/c/h;->L(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "A"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-PasswordConfirmRunnable]Server Result is \'A\' Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/msc/sa/service/e;->a(Z)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "N"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-PasswordConfirmRunnable]Server Result is \'N\' Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/msc/sa/service/n;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/ad;->i(Landroid/content/Context;)V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/msc/sa/service/p;

    invoke-direct {v1, p0}, Lcom/msc/sa/service/p;-><init>(Lcom/msc/sa/service/n;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    new-instance v0, Lcom/msc/c/f;

    const-string v1, "SAC_0107"

    const-string v2, "User id is invalid"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/msc/sa/service/n;->a:Lcom/msc/c/f;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    new-instance v0, Lcom/msc/c/f;

    const-string v1, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/msc/sa/service/n;->a:Lcom/msc/c/f;

    goto :goto_0

    :cond_1
    :try_start_1
    const-string v1, "S"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-PasswordConfirmRunnable]Server Result is \'S\' Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p3}, Lcom/msc/c/c;->g(Landroid/content/Context;)V

    invoke-static {p3}, Lcom/msc/c/e;->a(Landroid/content/Context;)Z

    iget-object v0, p0, Lcom/msc/sa/service/n;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v0}, Lcom/msc/sa/d/a;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/msc/sa/service/n;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v1}, Lcom/msc/sa/d/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p3, v0, v1}, Lcom/msc/sa/service/n;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/msc/c/f;

    const-string v1, "SAC_0402"

    const-string v2, "Auth token expired"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/msc/sa/service/n;->a:Lcom/msc/c/f;

    goto/16 :goto_0

    :cond_2
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/msc/sa/service/n;->a:Lcom/msc/c/f;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(ZLcom/msc/sa/b/b;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 353
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-PasswordConfirmRunnable]onProcessFinished. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    if-ne p1, v3, :cond_1

    .line 356
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-PasswordConfirmRunnable]onServerProcessSuccessed start. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/msc/sa/b/b;->d()Lcom/msc/sa/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/msc/sa/service/n;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v1}, Lcom/msc/sa/d/a;->b()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/msc/sa/a/a;->f(IZLandroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-PasswordConfirmRunnable]onServerProcessSuccessed finish. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    :goto_1
    return-void

    .line 356
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 359
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-PasswordConfirmRunnable]onServerProcessFailed start. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/msc/sa/b/b;->d()Lcom/msc/sa/a/a;

    move-result-object v0

    if-eqz v0, :cond_3

    :try_start_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/msc/sa/service/n;->a:Lcom/msc/c/f;

    if-nez v2, :cond_2

    new-instance v2, Lcom/msc/c/f;

    const-string v3, "SAC_0401"

    const-string v4, "Internal error occurred"

    invoke-direct {v2, v3, v4}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/msc/sa/service/n;->a:Lcom/msc/c/f;

    :cond_2
    const-string v2, "error_code"

    iget-object v3, p0, Lcom/msc/sa/service/n;->a:Lcom/msc/c/f;

    invoke-virtual {v3}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "error_message"

    iget-object v3, p0, Lcom/msc/sa/service/n;->a:Lcom/msc/c/f;

    invoke-virtual {v3}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "PCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-PasswordConfirmRunnable] Process Failed Error_code "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/n;->a:Lcom/msc/c/f;

    invoke-virtual {v4}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Error_message "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/msc/sa/service/n;->a:Lcom/msc/c/f;

    invoke-virtual {v4}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Package = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/n;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v2}, Lcom/msc/sa/d/a;->b()I

    move-result v2

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3, v1}, Lcom/msc/sa/a/a;->f(IZLandroid/os/Bundle;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_3
    :goto_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-PasswordConfirmRunnable]onServerProcessFailed finish. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method public final b()Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 60
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-PasswordConfirmRunnable]runProcess start. Package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-PasswordConfirmRunnable]runPasswordConfirmationProcess. Package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-PasswordConfirmRunnable]checkSelfUpgradeIsDone. Package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/msc/sa/service/n;->a()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v3

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v4, p0, Lcom/msc/sa/service/n;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v4}, Lcom/msc/sa/d/a;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v1, v2, v4}, Lcom/msc/sa/selfupdate/c;->a(Landroid/app/Application;ZZLjava/lang/String;)V

    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/c;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-PasswordConfirmRunnable]NEED_SELF_UPGRADE. Package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "SAC_0203"

    const-string v3, "The upgrade process must be completed if you want to use Samsung account"

    invoke-virtual {p0, v0, v3}, Lcom/msc/sa/service/n;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-PasswordConfirmRunnable]need Self Upgrade. FIAL. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 65
    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "PCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-PasswordConfirmRunnable]runProcess end. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    return v0

    .line 62
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-PasswordConfirmRunnable]Upgrade check is Success. Current SA version is the latest. Package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/msc/c/f;

    const-string v3, "SAC_0401"

    const-string v4, "Internal error occurred"

    invoke-direct {v0, v3, v4}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/msc/sa/service/n;->a:Lcom/msc/c/f;

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-PasswordConfirmRunnable]requestCheckUserStatus. Package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/msc/sa/service/e;

    invoke-direct {v0}, Lcom/msc/sa/service/e;-><init>()V

    invoke-virtual {p0}, Lcom/msc/sa/service/n;->a()Landroid/content/Context;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/osp/device/b;->a()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {v3}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-PasswordConfirmRunnable]Do not need to check UserStatus. Package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    :goto_2
    if-nez v0, :cond_7

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "PCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-PasswordConfirmRunnable]requestCheckUserStatus. FIAL. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_4
    invoke-static {v3}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-PasswordConfirmRunnable]UserID is null. Package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/msc/c/f;

    const-string v3, "SAC_0401"

    const-string v4, "Internal error occurred"

    invoke-direct {v0, v3, v4}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/msc/sa/service/n;->a:Lcom/msc/c/f;

    move v0, v1

    goto :goto_2

    :cond_5
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    const-string v5, "001"

    new-instance v6, Lcom/msc/sa/service/o;

    invoke-direct {v6, p0, v0, v3}, Lcom/msc/sa/service/o;-><init>(Lcom/msc/sa/service/n;Lcom/msc/sa/service/e;Landroid/content/Context;)V

    invoke-static {v3, v4, v5, v6}, Lcom/msc/c/g;->k(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v4

    const-string v3, "from_xml"

    invoke-virtual {p0, v4, v5, v3}, Lcom/msc/sa/service/n;->a(JLjava/lang/String;)V

    sget-object v3, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v4, v5, v3}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    :goto_3
    invoke-virtual {v0}, Lcom/msc/sa/service/e;->a()Z

    move-result v0

    goto :goto_2

    :cond_6
    new-instance v3, Lcom/msc/c/f;

    const-string v4, "SAC_0401"

    const-string v5, "Internal error occurred"

    invoke-direct {v3, v4, v5}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/msc/sa/service/n;->a:Lcom/msc/c/f;

    goto :goto_3

    :cond_7
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-PasswordConfirmRunnable]requestPasswordConfirmation. Package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/msc/sa/service/e;

    invoke-direct {v0}, Lcom/msc/sa/service/e;-><init>()V

    invoke-virtual {p0}, Lcom/msc/sa/service/n;->a()Landroid/content/Context;

    move-result-object v3

    if-eqz v3, :cond_9

    iget-object v4, p0, Lcom/msc/sa/service/n;->b:Lcom/msc/sa/d/a;

    const-string v5, "password"

    invoke-virtual {v4, v5}, Lcom/msc/sa/d/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_8

    const-string v0, "Param [%s] must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "password"

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/msc/c/f;

    const-string v3, "SAC_0101"

    invoke-direct {v2, v3, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/msc/sa/service/n;->a:Lcom/msc/c/f;

    move v0, v1

    :goto_4
    if-nez v0, :cond_a

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "PCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-PasswordConfirmRunnable]request PasswordConfirmation FAIL. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_8
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    invoke-static {v3}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    invoke-static {v3}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/msc/sa/service/r;

    invoke-direct {v2, p0, v0}, Lcom/msc/sa/service/r;-><init>(Lcom/msc/sa/service/n;Lcom/msc/sa/service/e;)V

    invoke-static {v3, v1, v4, v2}, Lcom/msc/c/g;->h(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v2

    const-string v1, "from_xml"

    invoke-virtual {p0, v2, v3, v1}, Lcom/msc/sa/service/n;->a(JLjava/lang/String;)V

    sget-object v1, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v2, v3, v1}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    :goto_5
    invoke-virtual {v0}, Lcom/msc/sa/service/e;->a()Z

    move-result v0

    goto :goto_4

    :cond_9
    new-instance v1, Lcom/msc/c/f;

    const-string v2, "SAC_0401"

    const-string v3, "Internal error occurred"

    invoke-direct {v1, v2, v3}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/msc/sa/service/n;->a:Lcom/msc/c/f;

    goto :goto_5

    :cond_a
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "PCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-PasswordConfirmRunnable]runPasswordConfirmationProcess SUCCESS. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/n;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method
