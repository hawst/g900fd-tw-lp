.class final Lcom/msc/sa/service/l;
.super Lcom/msc/sa/service/m;
.source "GearLogCollectingRunnable.java"


# instance fields
.field final synthetic a:Lcom/msc/sa/service/k;


# direct methods
.method constructor <init>(Lcom/msc/sa/service/k;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/msc/sa/service/l;->a:Lcom/msc/sa/service/k;

    invoke-direct {p0, p1}, Lcom/msc/sa/service/m;-><init>(Lcom/msc/sa/service/k;)V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 3

    .prologue
    .line 189
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/sa/service/m;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 190
    if-nez p1, :cond_1

    .line 212
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 195
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 199
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v0}, Lcom/msc/c/h;->M(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 200
    iget-object v1, p0, Lcom/msc/sa/service/l;->a:Lcom/msc/sa/service/k;

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v1, v0}, Lcom/msc/sa/service/k;->a(Lcom/msc/sa/service/k;Z)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 207
    :goto_1
    :try_start_3
    iget-object v0, p0, Lcom/msc/sa/service/l;->a:Lcom/msc/sa/service/k;

    invoke-static {v0}, Lcom/msc/sa/service/k;->a(Lcom/msc/sa/service/k;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/msc/sa/service/l;->a:Lcom/msc/sa/service/k;

    const-string v1, "SAC_0303"

    const-string v2, "Unknown Server Error"

    invoke-virtual {v0, v1, v2}, Lcom/msc/sa/service/k;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 189
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 202
    :catch_0
    move-exception v0

    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 0

    .prologue
    .line 216
    invoke-super {p0, p1}, Lcom/msc/sa/service/m;->b(Lcom/msc/b/c;)V

    .line 218
    return-void
.end method
