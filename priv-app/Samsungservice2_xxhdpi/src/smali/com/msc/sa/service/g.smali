.class public final Lcom/msc/sa/service/g;
.super Lcom/msc/sa/service/c;
.source "AccessTokenRunnable.java"


# instance fields
.field private d:Lcom/msc/sa/d/i;

.field private e:Lcom/msc/sa/d/e;

.field private f:Z

.field private g:Lcom/msc/sa/service/h;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 74
    const/4 v2, 0x1

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/msc/sa/service/c;-><init>(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;)V

    .line 53
    iput-object v6, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    .line 58
    iput-object v6, p0, Lcom/msc/sa/service/g;->e:Lcom/msc/sa/d/e;

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/msc/sa/service/g;->f:Z

    .line 65
    iput-object v6, p0, Lcom/msc/sa/service/g;->g:Lcom/msc/sa/service/h;

    .line 75
    return-void
.end method

.method private a(Lcom/msc/sa/b/b;)V
    .locals 5

    .prologue
    .line 308
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AccessTokenRunnable]handleProcessFailResult start. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    invoke-virtual {p1}, Lcom/msc/sa/b/b;->d()Lcom/msc/sa/a/a;

    move-result-object v0

    .line 311
    if-nez v0, :cond_0

    .line 339
    :goto_0
    return-void

    .line 318
    :cond_0
    :try_start_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 319
    const-string v2, "error_code"

    iget-object v3, p0, Lcom/msc/sa/service/g;->a:Lcom/msc/c/f;

    invoke-virtual {v3}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    const-string v2, "error_message"

    iget-object v3, p0, Lcom/msc/sa/service/g;->a:Lcom/msc/c/f;

    invoke-virtual {v3}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "ATR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-AccessTokenRunnable] Process Failed Error_code "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/g;->a:Lcom/msc/c/f;

    invoke-virtual {v4}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Error_message "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/msc/sa/service/g;->a:Lcom/msc/c/f;

    invoke-virtual {v4}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Package = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    iget-object v2, p0, Lcom/msc/sa/service/g;->a:Lcom/msc/c/f;

    invoke-virtual {v2}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SAC_0204"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 328
    iget-object v2, p0, Lcom/msc/sa/service/g;->e:Lcom/msc/sa/d/e;

    invoke-virtual {v2}, Lcom/msc/sa/d/e;->a()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/msc/sa/service/g;->a(Landroid/os/Bundle;I)V

    .line 331
    :cond_1
    iget-object v2, p0, Lcom/msc/sa/service/g;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v2}, Lcom/msc/sa/d/a;->b()I

    move-result v2

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3, v1}, Lcom/msc/sa/a/a;->a(IZLandroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 338
    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AccessTokenRunnable]handleProcessFailResult finish. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 333
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method static synthetic a(Lcom/msc/sa/service/g;Lcom/msc/sa/b/b;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/msc/sa/service/g;->a(Lcom/msc/sa/b/b;)V

    return-void
.end method

.method private a([Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v8, 0x1

    .line 428
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AccessTokenRunnable]setAdditionalInfo start. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    invoke-virtual {p0}, Lcom/msc/sa/service/g;->a()Landroid/content/Context;

    move-result-object v4

    .line 431
    if-eqz p1, :cond_f

    .line 433
    array-length v5, p1

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_f

    aget-object v0, p1, v2

    .line 435
    const-string v1, "user_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v8, :cond_1

    .line 437
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    const-string v1, "[InAIDL-AccessTokenRunnable]set user_id"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    const-string v0, "user_id"

    invoke-static {v4}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 440
    :cond_1
    const-string v1, "birthday"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v8, :cond_2

    .line 442
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    const-string v1, "[InAIDL-AccessTokenRunnable]set birthday"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    const-string v0, "birthday"

    invoke-static {v4}, Lcom/msc/c/e;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 445
    :cond_2
    const-string v1, "mcc"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v8, :cond_3

    .line 447
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    const-string v1, "[InAIDL-AccessTokenRunnable]set mcc"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    const-string v0, "mcc"

    invoke-static {v4}, Lcom/msc/c/e;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 450
    :cond_3
    const-string v1, "api_server_url"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v8, :cond_4

    .line 452
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    const-string v1, "[InAIDL-AccessTokenRunnable]set api_server_url"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    const-string v0, "API_SERVER"

    invoke-static {v4, v0}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 454
    const-string v1, "api_server_url"

    invoke-virtual {p2, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 456
    :cond_4
    const-string v1, "auth_server_url"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v8, :cond_5

    .line 458
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    const-string v1, "[InAIDL-AccessTokenRunnable]set auth_server_url"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    const-string v0, "AUTH_SERVER"

    invoke-static {v4, v0}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 460
    const-string v1, "auth_server_url"

    invoke-virtual {p2, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 462
    :cond_5
    const-string v1, "cc"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v8, :cond_6

    .line 464
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    const-string v1, "[InAIDL-AccessTokenRunnable]set cc"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    invoke-static {v4}, Lcom/osp/app/util/ad;->n(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 466
    const-string v1, "cc"

    invoke-virtual {p2, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 468
    :cond_6
    const-string v1, "device_physical_address_text"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v8, :cond_9

    .line 470
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    const-string v1, "[InAIDL-AccessTokenRunnable]set device_physical_address_text"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    if-eqz v4, :cond_8

    .line 473
    const-string v0, "DEVICE_PHYSICAL_ADDRESS_TEXT"

    invoke-virtual {v4, v0, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 474
    const-string v1, "DEVICE_PHYSICAL_ADDRESS_TEXT"

    const-string v6, ""

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 475
    const/4 v0, 0x0

    .line 478
    if-eqz v1, :cond_7

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_7

    .line 480
    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v6

    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v7

    invoke-virtual {v7}, Lcom/msc/c/a;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v1}, Lcom/msc/c/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 491
    :goto_2
    const-string v1, "device_physical_address_text"

    invoke-virtual {p2, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 483
    :cond_7
    :try_start_1
    invoke-static {v4}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v1

    .line 484
    invoke-virtual {v1}, Lcom/osp/device/a;->d()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_2

    .line 486
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 494
    :cond_8
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    const-string v1, "[InAIDL-AccessTokenRunnable]Context is null. Skip device_physical_address_text"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 496
    :cond_9
    const-string v1, "access_token_expires_in"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v8, :cond_a

    .line 498
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    const-string v1, "[InAIDL-AccessTokenRunnable]set access_token_expires_in"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    const-string v0, "access_token_expires_in"

    iget-object v1, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    invoke-virtual {v1}, Lcom/msc/sa/d/i;->b()J

    move-result-wide v6

    invoke-virtual {p2, v0, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto/16 :goto_1

    .line 501
    :cond_a
    const-string v1, "refresh_token"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v8, :cond_b

    .line 503
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    const-string v1, "[InAIDL-AccessTokenRunnable]set refresh_token"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    const-string v0, "refresh_token"

    iget-object v1, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    invoke-virtual {v1}, Lcom/msc/sa/d/i;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 506
    :cond_b
    const-string v1, "refresh_token_expires_in"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v8, :cond_c

    .line 508
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    const-string v1, "[InAIDL-AccessTokenRunnable]set refresh_token_expires_in"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    const-string v0, "refresh_token_expires_in"

    iget-object v1, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    invoke-virtual {v1}, Lcom/msc/sa/d/i;->d()J

    move-result-wide v6

    invoke-virtual {p2, v0, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto/16 :goto_1

    .line 511
    :cond_c
    const-string v1, "login_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v8, :cond_d

    .line 513
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    const-string v1, "[InAIDL-AccessTokenRunnable]set login_id"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    const-string v0, "login_id"

    invoke-static {v4}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 516
    :cond_d
    const-string v1, "login_id_type"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v8, :cond_0

    .line 518
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    const-string v1, "[InAIDL-AccessTokenRunnable]set login_id_type"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    invoke-static {v4}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v8, :cond_e

    .line 521
    const-string v0, "login_id_type"

    const-string v1, "001"

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 524
    :cond_e
    const-string v0, "login_id_type"

    const-string v1, "003"

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 529
    :cond_f
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AccessTokenRunnable]setAdditionalInfo finish. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    return-void
.end method


# virtual methods
.method public final a(ZLcom/msc/sa/b/b;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 220
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-AccessTokenRunnable]onProcessFinished. Package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    if-ne p1, v1, :cond_4

    .line 224
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-AccessTokenRunnable]checkAccessTokenAndRefreshToken. Package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    invoke-virtual {v0}, Lcom/msc/sa/d/i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    invoke-virtual {v0}, Lcom/msc/sa/d/i;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v1, :cond_3

    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-AccessTokenRunnable]mResultTokenInfo is null or AccessToken or RefreshToken is empty. Package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "SAC_0401"

    const-string v3, "Internal error occurred"

    invoke-virtual {p0, v0, v3}, Lcom/msc/sa/service/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 231
    :goto_0
    if-ne v0, v1, :cond_5

    .line 233
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AccessTokenRunnable]handleProcessSuccessResult start. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/msc/sa/service/g;->a()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-boolean v1, p0, Lcom/msc/sa/service/g;->f:Z

    if-nez v1, :cond_1

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "ATR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-AccessTokenRunnable]Token cannot reusable. Save token. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/msc/sa/service/g;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v1}, Lcom/msc/sa/d/a;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    invoke-virtual {v2}, Lcom/msc/sa/d/i;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    invoke-virtual {v3}, Lcom/msc/sa/d/i;->b()J

    move-result-wide v3

    iget-object v5, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    invoke-virtual {v5}, Lcom/msc/sa/d/i;->c()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    invoke-virtual {v6}, Lcom/msc/sa/d/i;->d()J

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;J)V

    :cond_1
    invoke-virtual {p2}, Lcom/msc/sa/b/b;->d()Lcom/msc/sa/a/a;

    move-result-object v0

    if-eqz v0, :cond_2

    :try_start_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "access_token"

    iget-object v3, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    invoke-virtual {v3}, Lcom/msc/sa/d/i;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/g;->b:Lcom/msc/sa/d/a;

    const-string v3, "additional"

    invoke-virtual {v2, v3}, Lcom/msc/sa/d/a;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/msc/sa/service/g;->a([Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/msc/sa/service/g;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v2}, Lcom/msc/sa/d/a;->b()I

    move-result v2

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3, v1}, Lcom/msc/sa/a/a;->a(IZLandroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AccessTokenRunnable]handleProcessSuccessResult finish. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    :cond_2
    :goto_2
    return-void

    :cond_3
    move v0, v1

    .line 224
    goto/16 :goto_0

    .line 228
    :cond_4
    invoke-virtual {p0}, Lcom/msc/sa/service/g;->c()V

    move v0, p1

    goto/16 :goto_0

    .line 233
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 236
    :cond_5
    iget-object v0, p0, Lcom/msc/sa/service/g;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v0}, Lcom/msc/sa/d/a;->f()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "key_retry_skip_email_validation"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 238
    iget-object v1, p0, Lcom/msc/sa/service/g;->e:Lcom/msc/sa/d/e;

    if-eqz v1, :cond_7

    invoke-virtual {p0}, Lcom/msc/sa/service/g;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/r;->v(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_7

    if-nez v0, :cond_7

    .line 240
    iget-object v0, p0, Lcom/msc/sa/service/g;->e:Lcom/msc/sa/d/e;

    invoke-virtual {v0}, Lcom/msc/sa/d/e;->a()I

    move-result v0

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 242
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AccessTokenRunnable]runSkipEmailValidationTask. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/msc/sa/service/g;->g:Lcom/msc/sa/service/h;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/msc/sa/service/g;->g:Lcom/msc/sa/service/h;

    invoke-virtual {v0}, Lcom/msc/sa/service/h;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/msc/sa/service/g;->g:Lcom/msc/sa/service/h;

    invoke-virtual {v0}, Lcom/msc/sa/service/h;->d()V

    :cond_6
    new-instance v0, Lcom/msc/sa/service/h;

    invoke-virtual {p0}, Lcom/msc/sa/service/g;->a()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1, p2}, Lcom/msc/sa/service/h;-><init>(Lcom/msc/sa/service/g;Landroid/content/Context;Lcom/msc/sa/b/b;)V

    iput-object v0, p0, Lcom/msc/sa/service/g;->g:Lcom/msc/sa/service/h;

    iget-object v0, p0, Lcom/msc/sa/service/g;->g:Lcom/msc/sa/service/h;

    invoke-virtual {v0}, Lcom/msc/sa/service/h;->b()V

    goto :goto_2

    .line 247
    :cond_7
    invoke-direct {p0, p2}, Lcom/msc/sa/service/g;->a(Lcom/msc/sa/b/b;)V

    goto :goto_2
.end method

.method public final b()Z
    .locals 12

    .prologue
    .line 79
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AccessTokenRunnable]runProcess start. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    invoke-virtual {p0}, Lcom/msc/sa/service/g;->a()Landroid/content/Context;

    move-result-object v10

    .line 82
    if-nez v10, :cond_0

    .line 84
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AccessTokenRunnable]Context is null. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v0, "SAC_0401"

    const-string v1, "Internal error occurred"

    invoke-virtual {p0, v0, v1}, Lcom/msc/sa/service/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AccessTokenRunnable]runProcess end. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    const/4 v0, 0x0

    .line 169
    :goto_0
    return v0

    .line 104
    :cond_0
    new-instance v0, Lcom/msc/sa/d/d;

    invoke-direct {v0}, Lcom/msc/sa/d/d;-><init>()V

    .line 106
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AccessTokenRunnable]isAccessTokenReusable. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/msc/sa/service/g;->a()Landroid/content/Context;

    move-result-object v11

    sget-object v0, Lcom/osp/app/util/af;->c:Lcom/osp/app/util/af;

    const/4 v1, 0x0

    invoke-static {v11, v0, v1}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Lcom/osp/app/util/af;Z)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AccessTokenRunnable]checkAndSetAccessToken. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/msc/sa/service/g;->a()Landroid/content/Context;

    move-result-object v1

    const/4 v0, 0x0

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/msc/sa/service/g;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v0}, Lcom/msc/sa/d/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/util/ad;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    :cond_1
    if-eqz v0, :cond_4

    const-string v1, "key_accesstoken"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v1, "key_accesstoken_expires_in"

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v1, "key_refreshtoken"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v1, "key_refreshtoken_expires_in"

    const-wide/16 v6, 0x0

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    const-string v1, "key_accesstoken_issued_time"

    const-wide/16 v8, 0x0

    invoke-virtual {v0, v1, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    iget-object v0, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    if-nez v0, :cond_2

    new-instance v1, Lcom/msc/sa/d/i;

    invoke-direct/range {v1 .. v9}, Lcom/msc/sa/d/i;-><init>(Ljava/lang/String;JLjava/lang/String;JJ)V

    iput-object v1, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    :cond_2
    const/4 v0, 0x1

    :goto_1
    const/4 v1, 0x1

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    invoke-virtual {v0}, Lcom/msc/sa/d/i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    invoke-virtual {v0}, Lcom/msc/sa/d/i;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/msc/sa/service/g;->b:Lcom/msc/sa/d/a;

    const-string v1, "expired_access_token"

    invoke-virtual {v0, v1}, Lcom/msc/sa/d/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    iget-object v1, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    invoke-virtual {v1}, Lcom/msc/sa/d/i;->e()J

    move-result-wide v4

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "curTimeSec = "

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "accessTokenIssuedTime =  "

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    invoke-virtual {v6}, Lcom/msc/sa/d/i;->e()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "accessTokenIssuedTime/1000 =  "

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    invoke-virtual {v6}, Lcom/msc/sa/d/i;->e()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "AccessTokenExpiresIn = "

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    invoke-virtual {v6}, Lcom/msc/sa/d/i;->b()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "TimeGap = "

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/16 v6, 0x3e8

    div-long v6, v4, v6

    sub-long v6, v2, v6

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "ATR"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "expiredAccessToken is Empty. Check Token issuedTime. TimeGap = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/16 v8, 0x3e8

    div-long v8, v4, v8

    sub-long v8, v2, v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_3

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    sub-long/2addr v2, v4

    iget-object v1, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    invoke-virtual {v1}, Lcom/msc/sa/d/i;->b()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-lez v1, :cond_5

    :cond_3
    iget-object v0, p0, Lcom/msc/sa/service/g;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v0}, Lcom/msc/sa/d/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v11, v0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    const/4 v0, 0x0

    :goto_2
    iput-boolean v0, p0, Lcom/msc/sa/service/g;->f:Z

    .line 107
    iget-boolean v0, p0, Lcom/msc/sa/service/g;->f:Z

    if-eqz v0, :cond_a

    .line 123
    iget-object v0, p0, Lcom/msc/sa/service/g;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v0}, Lcom/msc/sa/d/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v10, v0}, Lcom/msc/openprovider/b;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/osp/app/util/ad;->a(I)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_7

    .line 125
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AccessTokenRunnable]AccessToken reuse. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AccessTokenRunnable]runProcess end. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 106
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_5
    iget-object v1, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    invoke-virtual {v1}, Lcom/msc/sa/d/i;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x1

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/msc/sa/service/g;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v0}, Lcom/msc/sa/d/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v11, v0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    const/4 v0, 0x0

    goto :goto_2

    .line 131
    :cond_7
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AccessTokenRunnable]AccessToken reuse. But Need TncMandatory Check. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/msc/sa/service/g;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v0}, Lcom/msc/sa/d/a;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/msc/sa/service/g;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v1}, Lcom/msc/sa/d/a;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/sa/service/g;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v2}, Lcom/msc/sa/d/a;->e()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v10, v0, v1, v2, v3}, Lcom/msc/sa/c/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/msc/sa/d/d;

    move-result-object v0

    .line 166
    :goto_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "ATR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-AccessTokenRunnable]setAuthWithTncMadnatoryRequestResult. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->a()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_c

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "ATR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-AccessTokenRunnable]The result is true. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->l()I

    move-result v1

    if-eqz v1, :cond_8

    const/4 v2, 0x1

    if-ne v1, v2, :cond_9

    :cond_8
    new-instance v1, Lcom/msc/sa/d/i;

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->g()J

    move-result-wide v3

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->i()J

    move-result-wide v6

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->j()J

    move-result-wide v8

    invoke-direct/range {v1 .. v9}, Lcom/msc/sa/d/i;-><init>(Ljava/lang/String;JLjava/lang/String;JJ)V

    iput-object v1, p0, Lcom/msc/sa/service/g;->d:Lcom/msc/sa/d/i;

    .line 168
    :cond_9
    :goto_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "ATR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-AccessTokenRunnable]runProcess end. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    invoke-virtual {v0}, Lcom/msc/sa/d/d;->a()Z

    move-result v0

    goto/16 :goto_0

    .line 141
    :cond_a
    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v1

    invoke-virtual {v10}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/msc/sa/service/g;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v4}, Lcom/msc/sa/d/a;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/msc/sa/selfupdate/c;->a(Landroid/app/Application;ZZLjava/lang/String;)V

    .line 143
    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/c;->f()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 145
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AccessTokenRunnable]NEED_SELF_UPGRADE. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string v0, "SAC_0203"

    const-string v1, "The upgrade process must be completed if you want to use Samsung account"

    invoke-virtual {p0, v0, v1}, Lcom/msc/sa/service/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AccessTokenRunnable]runProcess end. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 160
    :cond_b
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-AccessTokenRunnable]AccessToken can\'t be reused. Need AuthTncMandatory Check. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lcom/msc/sa/service/g;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v0}, Lcom/msc/sa/d/a;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/msc/sa/service/g;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v1}, Lcom/msc/sa/d/a;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/sa/service/g;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v2}, Lcom/msc/sa/d/a;->e()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v10, v0, v1, v2, v3}, Lcom/msc/sa/c/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/msc/sa/d/d;

    move-result-object v0

    goto/16 :goto_3

    .line 166
    :cond_c
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "ATR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-AccessTokenRunnable]The result is false. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/g;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/msc/sa/service/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/msc/sa/d/e;

    invoke-virtual {v0}, Lcom/msc/sa/d/d;->e()I

    move-result v2

    invoke-direct {v1, v2}, Lcom/msc/sa/d/e;-><init>(I)V

    iput-object v1, p0, Lcom/msc/sa/service/g;->e:Lcom/msc/sa/d/e;

    goto/16 :goto_4
.end method
