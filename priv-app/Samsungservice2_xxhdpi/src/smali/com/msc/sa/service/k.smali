.class public final Lcom/msc/sa/service/k;
.super Ljava/lang/Object;
.source "GearLogCollectingRunnable.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field protected a:Lcom/msc/c/f;

.field protected final b:Lcom/msc/sa/d/a;

.field private final c:Ljava/lang/ref/WeakReference;

.field private d:Ljava/util/HashMap;

.field private final e:Ljava/lang/String;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lcom/msc/sa/a/d;)V
    .locals 7

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/msc/sa/service/k;->c:Ljava/lang/ref/WeakReference;

    .line 81
    new-instance v0, Lcom/msc/sa/d/a;

    move v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/msc/sa/d/a;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lcom/msc/sa/a/d;)V

    iput-object v0, p0, Lcom/msc/sa/service/k;->b:Lcom/msc/sa/d/a;

    .line 82
    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v0

    invoke-virtual {v0, p5}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/msc/sa/service/k;->e:Ljava/lang/String;

    .line 83
    return-void
.end method

.method static synthetic a(Lcom/msc/sa/service/k;JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 30
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "GLCR"

    const-string v1, "parseFailErrorResult"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/msc/sa/service/k;->d:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/msc/sa/service/k;->d:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1}, Lcom/msc/c/f;-><init>()V

    iput-object v1, p0, Lcom/msc/sa/service/k;->a:Lcom/msc/c/f;

    iget-object v1, p0, Lcom/msc/sa/service/k;->a:Lcom/msc/c/f;

    invoke-virtual {v1, v0, p3}, Lcom/msc/c/f;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/msc/sa/service/k;->a:Lcom/msc/c/f;

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/msc/sa/service/k;)Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/msc/sa/service/k;->f:Z

    return v0
.end method

.method static synthetic a(Lcom/msc/sa/service/k;Z)Z
    .locals 0

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/msc/sa/service/k;->f:Z

    return p1
.end method

.method private b()V
    .locals 4

    .prologue
    .line 251
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "GLCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[ExAIDL-GearLogService] handleFailResult start. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/k;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 254
    const-string v1, "error_code"

    iget-object v2, p0, Lcom/msc/sa/service/k;->a:Lcom/msc/c/f;

    invoke-virtual {v2}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const-string v1, "error_message"

    iget-object v2, p0, Lcom/msc/sa/service/k;->a:Lcom/msc/c/f;

    invoke-virtual {v2}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "GLCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[ExAIDL-GearLogService] Error_code "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/k;->a:Lcom/msc/c/f;

    invoke-virtual {v3}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Error_message "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/msc/sa/service/k;->a:Lcom/msc/c/f;

    invoke-virtual {v3}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Package = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/msc/sa/service/k;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    :try_start_0
    iget-object v1, p0, Lcom/msc/sa/service/k;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v1}, Lcom/msc/sa/d/a;->g()Lcom/msc/sa/a/d;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/sa/service/k;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v2}, Lcom/msc/sa/d/a;->b()I

    move-result v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, v0}, Lcom/msc/sa/a/d;->a(IZLandroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 267
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "GLCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[ExAIDL-GearLogService] handleFailResult end. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/k;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    return-void

    .line 262
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method protected final a()Landroid/content/Context;
    .locals 3

    .prologue
    .line 128
    const/4 v0, 0x0

    .line 129
    iget-object v1, p0, Lcom/msc/sa/service/k;->c:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    .line 131
    iget-object v0, p0, Lcom/msc/sa/service/k;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 133
    :cond_0
    if-nez v0, :cond_1

    .line 135
    const-string v1, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-virtual {p0, v1, v2}, Lcom/msc/sa/service/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "GLCR"

    const-string v2, "[ExAIDL-GearLogService]context is null"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    :cond_1
    return-object v0
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 355
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "GLCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[ExAIDL-GearLogService]setErrorResult. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/k;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0, p1, p2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/msc/sa/service/k;->a:Lcom/msc/c/f;

    .line 358
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[ExAIDL-GearLogService]errorMessage = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 359
    return-void
.end method

.method public final run()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 144
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "GLCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[ExAIDL-GearLogService] isAvailableRunningProcess start. Package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/k;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/msc/sa/service/k;->a()Landroid/content/Context;

    move-result-object v1

    if-nez v1, :cond_0

    move v1, v0

    :goto_0
    if-nez v1, :cond_5

    .line 146
    invoke-direct {p0}, Lcom/msc/sa/service/k;->b()V

    .line 157
    :goto_1
    return-void

    .line 144
    :cond_0
    invoke-static {v1}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v1, "SAC_0301"

    const-string v3, "Network is not available"

    invoke-virtual {p0, v1, v3}, Lcom/msc/sa/service/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-static {v1}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v1, "SAC_0102"

    const-string v3, "Samsung account does not exist"

    invoke-virtual {p0, v1, v3}, Lcom/msc/sa/service/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v0

    goto :goto_0

    :cond_2
    invoke-static {v1}, Lcom/osp/app/util/ad;->k(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v1, "SAC_0108"

    const-string v3, "Need Email Validation"

    invoke-virtual {p0, v1, v3}, Lcom/msc/sa/service/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v0

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/msc/sa/service/k;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v3}, Lcom/msc/sa/d/a;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/msc/sa/service/k;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v4}, Lcom/msc/sa/d/a;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/msc/sa/c/j;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "GLCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[ExAIDL-GearLogService]Signature check Fail. Check clientID, packageName again. Package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/k;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "SAC_0205"

    const-string v3, "The signature of this application is not registered with the server."

    invoke-virtual {p0, v1, v3}, Lcom/msc/sa/service/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v0

    goto :goto_0

    :cond_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "GLCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[ExAIDL-GearLogService] isAvailableRunningProcess end. Package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/k;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    goto/16 :goto_0

    .line 150
    :cond_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "GLCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[ExAIDL-GearLogService] RequestCreateUserSubDevice start. Package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/k;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v0, p0, Lcom/msc/sa/service/k;->f:Z

    invoke-virtual {p0}, Lcom/msc/sa/service/k;->a()Landroid/content/Context;

    move-result-object v1

    if-nez v1, :cond_6

    const-string v1, "SAC_0401"

    const-string v3, "Internal error occurred"

    invoke-virtual {p0, v1, v3}, Lcom/msc/sa/service/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    if-ne v0, v2, :cond_9

    .line 152
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "GLCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[ExAIDL-GearLogService] handleSuccessResult Start. Package="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/k;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/msc/sa/service/k;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v0}, Lcom/msc/sa/d/a;->g()Lcom/msc/sa/a/d;

    move-result-object v0

    iget-object v1, p0, Lcom/msc/sa/service/k;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v1}, Lcom/msc/sa/d/a;->b()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/msc/sa/a/d;->a(IZLandroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "GLCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[ExAIDL-GearLogService] handleSuccessResult end. Package ="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/k;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 150
    :cond_6
    invoke-static {v1}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    const-string v1, "SAC_0401"

    const-string v3, "Internal error occurred"

    invoke-virtual {p0, v1, v3}, Lcom/msc/sa/service/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_7
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/msc/sa/service/k;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v0}, Lcom/msc/sa/d/a;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/msc/sa/service/k;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v4}, Lcom/msc/sa/d/a;->f()Landroid/os/Bundle;

    move-result-object v4

    new-instance v5, Lcom/msc/sa/service/l;

    invoke-direct {v5, p0}, Lcom/msc/sa/service/l;-><init>(Lcom/msc/sa/service/k;)V

    invoke-static {v1, v0, v3, v4, v5}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lcom/msc/b/h;)J

    move-result-wide v0

    const-string v3, "from_xml"

    iget-object v4, p0, Lcom/msc/sa/service/k;->d:Ljava/util/HashMap;

    if-nez v4, :cond_8

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/msc/sa/service/k;->d:Ljava/util/HashMap;

    :cond_8
    iget-object v4, p0, Lcom/msc/sa/service/k;->d:Ljava/util/HashMap;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v3, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v3}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "GLCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "[ExAIDL-GearLogService] RequestCreateUserSubDevice end. Package = "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/k;->e:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/msc/sa/service/k;->f:Z

    goto/16 :goto_2

    .line 152
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_3

    .line 155
    :cond_9
    invoke-direct {p0}, Lcom/msc/sa/service/k;->b()V

    goto/16 :goto_1
.end method
