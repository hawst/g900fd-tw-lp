.class public final Lcom/msc/sa/service/u;
.super Lcom/msc/sa/service/c;
.source "ValidationAndDisclaimerRunnable.java"


# instance fields
.field private final d:Ljava/lang/String;

.field private e:Lcom/msc/sa/d/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;ILandroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 65
    move-object v0, p0

    move-object v1, p1

    move v2, p4

    move v3, p2

    move-object v4, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/msc/sa/service/c;-><init>(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;)V

    .line 67
    const/4 v0, 0x3

    if-ne p4, v0, :cond_0

    .line 69
    const-string v0, "DISCLAIMER_CHECK"

    iput-object v0, p0, Lcom/msc/sa/service/u;->d:Ljava/lang/String;

    .line 74
    :goto_0
    return-void

    .line 72
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/msc/sa/service/u;->d:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a(ZLcom/msc/sa/b/b;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 38
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "VCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-ValidNDisClaimerRunnable]onProcessFinished. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/u;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    if-nez p1, :cond_0

    .line 42
    invoke-virtual {p0}, Lcom/msc/sa/service/u;->c()V

    .line 45
    :cond_0
    if-ne p1, v5, :cond_3

    .line 47
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "VCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-ValidNDisClaimerRunnable]handleProcessSuccessResult start. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/u;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/msc/sa/b/b;->d()Lcom/msc/sa/a/a;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_0
    const-string v1, "DISCLAIMER_CHECK"

    iget-object v2, p0, Lcom/msc/sa/service/u;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v5, :cond_2

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "VCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-ValidNDisClaimerRunnable]onReceiveDisclaimerAgreement called. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/u;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/msc/sa/service/u;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v1}, Lcom/msc/sa/d/a;->b()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/msc/sa/a/a;->c(IZLandroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "VCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-ValidNDisClaimerRunnable]handleProcessSuccessResult end. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/u;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    :cond_1
    :goto_1
    return-void

    .line 47
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "VCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-ValidNDisClaimerRunnable]onReceiveChecklistValidation called. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/u;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/msc/sa/service/u;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v1}, Lcom/msc/sa/d/a;->b()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/msc/sa/a/a;->b(IZLandroid/os/Bundle;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 50
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "VCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-ValidNDisClaimerRunnable]handleProcessFailResult start. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/u;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/msc/sa/b/b;->d()Lcom/msc/sa/a/a;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_2
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "error_code"

    iget-object v3, p0, Lcom/msc/sa/service/u;->a:Lcom/msc/c/f;

    invoke-virtual {v3}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "error_message"

    iget-object v3, p0, Lcom/msc/sa/service/u;->a:Lcom/msc/c/f;

    invoke-virtual {v3}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "VCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-ValidNDisClaimerRunnable]Process Failed Error_code "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/u;->a:Lcom/msc/c/f;

    invoke-virtual {v4}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Error_message "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/msc/sa/service/u;->a:Lcom/msc/c/f;

    invoke-virtual {v4}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Package = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/msc/sa/service/u;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "DISCLAIMER_CHECK"

    iget-object v3, p0, Lcom/msc/sa/service/u;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v2, v5, :cond_4

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "VCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-ValidNDisClaimerRunnable]onReceiveDisclaimerAgreement called. Package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/u;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/u;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v2}, Lcom/msc/sa/d/a;->b()I

    move-result v2

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3, v1}, Lcom/msc/sa/a/a;->c(IZLandroid/os/Bundle;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "VCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[InAIDL-ValidNDisClaimerRunnable]handleProcessFailResult end. Package = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/u;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_4
    :try_start_3
    iget-object v2, p0, Lcom/msc/sa/service/u;->a:Lcom/msc/c/f;

    invoke-virtual {v2}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SAC_0204"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v2, v5, :cond_5

    iget-object v2, p0, Lcom/msc/sa/service/u;->e:Lcom/msc/sa/d/e;

    invoke-virtual {v2}, Lcom/msc/sa/d/e;->a()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/msc/sa/service/u;->a(Landroid/os/Bundle;I)V

    :cond_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "VCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-ValidNDisClaimerRunnable]onReceiveChecklistValidation called. Package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/u;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/msc/sa/service/u;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v2}, Lcom/msc/sa/d/a;->b()I

    move-result v2

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3, v1}, Lcom/msc/sa/a/a;->b(IZLandroid/os/Bundle;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 158
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "VCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-ValidNDisClaimerRunnable]runProcess start. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/u;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    invoke-virtual {p0}, Lcom/msc/sa/service/u;->a()Landroid/content/Context;

    move-result-object v1

    .line 161
    if-nez v1, :cond_0

    .line 163
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "VCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-ValidNDisClaimerRunnable]Context is null. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/u;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    const-string v1, "SAC_0401"

    const-string v2, "Internal error occurred"

    invoke-virtual {p0, v1, v2}, Lcom/msc/sa/service/u;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :goto_0
    return v0

    .line 170
    :cond_0
    const-string v2, "DISCLAIMER_CHECK"

    iget-object v3, p0, Lcom/msc/sa/service/u;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 172
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "VCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-ValidNDisClaimerRunnable]runProcess 3rdPartyDisclaimerCheck Called. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/u;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/msc/sa/service/u;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v0}, Lcom/msc/sa/d/a;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/msc/sa/service/u;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v2}, Lcom/msc/sa/d/a;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/msc/sa/service/u;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v3}, Lcom/msc/sa/d/a;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lcom/msc/sa/c/o;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/msc/sa/d/h;

    move-result-object v0

    .line 182
    :goto_1
    invoke-virtual {v0}, Lcom/msc/sa/d/h;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 184
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "VCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-ValidNDisClaimerRunnable]The result is false. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/u;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    invoke-virtual {v0}, Lcom/msc/sa/d/h;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/msc/sa/d/h;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/msc/sa/service/u;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    new-instance v1, Lcom/msc/sa/d/e;

    invoke-virtual {v0}, Lcom/msc/sa/d/h;->f()I

    move-result v2

    invoke-direct {v1, v2}, Lcom/msc/sa/d/e;-><init>(I)V

    iput-object v1, p0, Lcom/msc/sa/service/u;->e:Lcom/msc/sa/d/e;

    .line 190
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "VCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InAIDL-ValidNDisClaimerRunnable]runProcess end. Package = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/msc/sa/service/u;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    invoke-virtual {v0}, Lcom/msc/sa/d/h;->a()Z

    move-result v0

    goto/16 :goto_0

    .line 177
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "VCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[InAIDL-ValidNDisClaimerRunnable]runProcess TncMandatoryValidation Called. Package = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/msc/sa/service/u;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget-object v2, p0, Lcom/msc/sa/service/u;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v2}, Lcom/msc/sa/d/a;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/msc/sa/service/u;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v3}, Lcom/msc/sa/d/a;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/msc/sa/service/u;->b:Lcom/msc/sa/d/a;

    invoke-virtual {v4}, Lcom/msc/sa/d/a;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4, v0}, Lcom/msc/sa/c/o;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/msc/sa/d/h;

    move-result-object v0

    goto/16 :goto_1
.end method
