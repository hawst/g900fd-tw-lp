.class public final Lcom/msc/openprovider/b;
.super Ljava/lang/Object;
.source "OpenDBManager.java"


# static fields
.field private static final a:Landroid/net/Uri;

.field private static final b:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-string v0, "content://com.msc.openprovider.openContentProvider/tncRequest"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/msc/openprovider/b;->a:Landroid/net/Uri;

    .line 42
    const-string v0, "content://com.msc.openprovider.openContentProvider/mandatoryField"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/msc/openprovider/b;->b:Landroid/net/Uri;

    return-void
.end method

.method public static a(Landroid/content/Context;)I
    .locals 4

    .prologue
    .line 72
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ODM"

    const-string v1, "getTncResult"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const/16 v2, 0xe

    .line 75
    const/4 v0, 0x0

    .line 79
    :try_start_0
    invoke-static {p0}, Lcom/msc/openprovider/b;->h(Landroid/content/Context;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 80
    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-lez v0, :cond_0

    move v0, v2

    .line 82
    :goto_0
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 84
    const/4 v2, 0x4

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v0

    goto :goto_0

    :cond_0
    move v0, v2

    .line 93
    :cond_1
    if-eqz v1, :cond_2

    .line 95
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 99
    :cond_2
    :goto_1
    return v0

    .line 93
    :catch_0
    move-exception v1

    move-object v1, v0

    move v0, v2

    :goto_2
    if-eqz v1, :cond_2

    .line 95
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 93
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_3
    if-eqz v1, :cond_3

    .line 95
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 93
    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    move v0, v2

    goto :goto_2

    :catch_2
    move-exception v2

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 53
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ODM"

    const-string v1, "getCheckListFromOpenDB"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    invoke-static {p0}, Lcom/msc/openprovider/b;->a(Landroid/content/Context;)I

    move-result v0

    .line 57
    invoke-static {p0, p1}, Lcom/msc/openprovider/b;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59
    or-int/lit8 v0, v0, 0x10

    .line 62
    :cond_0
    return v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    .line 285
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 287
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ODM"

    const-string v1, "saveFieldInfoResultToOpenDB"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 290
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 292
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ODM"

    const-string v1, "AccountManager has not Samsung account"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    :cond_0
    :goto_0
    return-void

    .line 296
    :cond_1
    const-string v0, "Saving FIELD info Failed"

    .line 297
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 298
    const-string v2, "Key"

    invoke-virtual {v3, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    const-string v1, "Value1"

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 300
    const-string v1, "Value2"

    invoke-virtual {v3, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 304
    const/4 v2, 0x0

    .line 307
    :try_start_0
    invoke-static {p0, p1}, Lcom/msc/openprovider/b;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 309
    if-eqz v2, :cond_2

    .line 311
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_4

    .line 313
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    .line 314
    sget-object v5, Lcom/msc/openprovider/b;->b:Landroid/net/Uri;

    const-string v6, "Value2=?"

    invoke-virtual {v1, v5, v3, v6, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 319
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "FIELD SAVE key : fieldRequire:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 327
    :cond_2
    if-eqz v2, :cond_3

    .line 329
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 333
    :cond_3
    :goto_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "ODM"

    invoke-static {v1, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 317
    :cond_4
    :try_start_1
    sget-object v4, Lcom/msc/openprovider/b;->b:Landroid/net/Uri;

    invoke-virtual {v1, v4, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 322
    :catch_0
    move-exception v1

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 327
    if-eqz v2, :cond_3

    .line 329
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 327
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_5

    .line 329
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method

.method public static a(Landroid/content/Context;ZZZ)V
    .locals 2

    .prologue
    .line 347
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ODM"

    const-string v1, "saveTncResultToOpenDB"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351
    invoke-static {p0, p1, p2, p3}, Lcom/msc/openprovider/b;->b(Landroid/content/Context;ZZZ)V

    .line 357
    :goto_0
    return-void

    .line 354
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ODM"

    const-string v1, "Samsung account is not signed-in state"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;ZZZ)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 562
    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 563
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 565
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ODM"

    const-string v1, "AccountManager has not Samsung account"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    :cond_0
    :goto_0
    return-void

    .line 569
    :cond_1
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 572
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ODM"

    const-string v1, "makeCheckList"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    const/4 v0, 0x2

    :cond_2
    if-eqz p2, :cond_3

    or-int/lit8 v0, v0, 0x4

    :cond_3
    if-eqz p3, :cond_4

    or-int/lit8 v0, v0, 0x8

    .line 574
    :cond_4
    const-string v1, "Saving TNC info failed"

    .line 575
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 576
    const-string v5, "Key"

    invoke-virtual {v4, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    const-string v3, "Value1"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 578
    const-string v3, "Value2"

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 579
    const-string v3, "Value3"

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 580
    const-string v3, "Value"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 582
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 587
    :try_start_0
    invoke-static {p0}, Lcom/msc/openprovider/b;->h(Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v2

    .line 589
    if-eqz v2, :cond_9

    .line 591
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_6

    .line 593
    sget-object v5, Lcom/msc/openprovider/b;->a:Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v3, v5, v4, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 598
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TNC SAVE key: checkList:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 606
    :goto_2
    if-eqz v2, :cond_5

    .line 608
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 612
    :cond_5
    :goto_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "ODM"

    invoke-static {v1, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 596
    :cond_6
    :try_start_1
    sget-object v5, Lcom/msc/openprovider/b;->a:Landroid/net/Uri;

    invoke-virtual {v3, v5, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 601
    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 606
    if-eqz v2, :cond_8

    .line 608
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    move-object v0, v1

    goto :goto_3

    .line 606
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_7

    .line 608
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v0

    :cond_8
    move-object v0, v1

    goto :goto_3

    :cond_9
    move-object v0, v1

    goto :goto_2
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 110
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "ODM"

    const-string v2, "isRequireTncAgree"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const/4 v1, 0x0

    .line 117
    :try_start_0
    invoke-static {p0}, Lcom/msc/openprovider/b;->h(Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v1

    .line 119
    if-eqz v1, :cond_0

    .line 121
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-lez v2, :cond_0

    move v2, v0

    .line 123
    :goto_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 125
    const/4 v3, 0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    goto :goto_0

    :cond_0
    move v2, v0

    .line 135
    :cond_1
    if-eqz v1, :cond_2

    .line 137
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 141
    :cond_2
    :goto_1
    if-ne v2, v0, :cond_4

    :goto_2
    return v0

    .line 135
    :catch_0
    move-exception v2

    move v2, v0

    :goto_3
    if-eqz v1, :cond_2

    .line 137
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 135
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    .line 137
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 141
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 135
    :catch_1
    move-exception v3

    goto :goto_3
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 232
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "ODM"

    const-string v2, "isRequireField"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    const/4 v2, 0x0

    .line 239
    :try_start_0
    invoke-static {p0, p1}, Lcom/msc/openprovider/b;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 240
    if-eqz v2, :cond_0

    .line 242
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lez v1, :cond_0

    move v3, v0

    .line 244
    :goto_0
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 246
    const/4 v1, 0x1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    goto :goto_0

    :cond_0
    move v3, v0

    .line 255
    :cond_1
    if-eqz v2, :cond_2

    .line 257
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 261
    :cond_2
    :goto_1
    if-ne v3, v0, :cond_4

    :goto_2
    return v0

    .line 250
    :catch_0
    move-exception v1

    move v3, v0

    :goto_3
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 255
    if-eqz v2, :cond_2

    .line 257
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 255
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_3

    .line 257
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 261
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 250
    :catch_1
    move-exception v1

    goto :goto_3
.end method

.method private static c(Landroid/content/Context;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 477
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ODM"

    const-string v1, "getFieldCursor"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "Key"

    aput-object v0, v2, v3

    const-string v0, "Value1"

    aput-object v0, v2, v4

    .line 480
    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v3

    .line 481
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 486
    :try_start_0
    sget-object v1, Lcom/msc/openprovider/b;->b:Landroid/net/Uri;

    const-string v3, "Value2=?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 492
    :goto_0
    return-object v0

    .line 487
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v6

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 153
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "ODM"

    const-string v2, "isRequireNameValid"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const/4 v1, 0x0

    .line 160
    :try_start_0
    invoke-static {p0}, Lcom/msc/openprovider/b;->h(Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v1

    .line 161
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-lez v2, :cond_0

    move v2, v0

    .line 163
    :goto_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 165
    const/4 v3, 0x2

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    goto :goto_0

    :cond_0
    move v2, v0

    .line 174
    :cond_1
    if-eqz v1, :cond_2

    .line 176
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 180
    :cond_2
    :goto_1
    if-ne v2, v0, :cond_4

    :goto_2
    return v0

    .line 174
    :catch_0
    move-exception v2

    move v2, v0

    :goto_3
    if-eqz v1, :cond_2

    .line 176
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 174
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    .line 176
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 180
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 174
    :catch_1
    move-exception v3

    goto :goto_3
.end method

.method public static d(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 192
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "ODM"

    const-string v2, "isRequireEmailVerify"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    const/4 v1, 0x0

    .line 199
    :try_start_0
    invoke-static {p0}, Lcom/msc/openprovider/b;->h(Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v1

    .line 200
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-lez v2, :cond_0

    move v2, v0

    .line 202
    :goto_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 204
    const/4 v3, 0x3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    goto :goto_0

    :cond_0
    move v2, v0

    .line 213
    :cond_1
    if-eqz v1, :cond_2

    .line 215
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 219
    :cond_2
    :goto_1
    if-ne v2, v0, :cond_4

    :goto_2
    return v0

    .line 213
    :catch_0
    move-exception v2

    move v2, v0

    :goto_3
    if-eqz v1, :cond_2

    .line 215
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 213
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    .line 215
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 219
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 213
    :catch_1
    move-exception v3

    goto :goto_3
.end method

.method public static e(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 270
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ODM"

    const-string v1, "initalizeOpenDB"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p0, :cond_0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ODM"

    const-string v1, "deleteTncResultInOpenDB"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/msc/openprovider/b;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 273
    :cond_0
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p0, :cond_1

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ODM"

    const-string v1, "deleteFieldResultInOpenDB"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v1, Lcom/msc/openprovider/b;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 274
    :cond_1
    return-void
.end method

.method public static f(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 366
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ODM"

    const-string v3, "saveEmailVerifyResultToOpenDB"

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 371
    invoke-static {p0}, Lcom/msc/openprovider/b;->h(Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v4

    .line 376
    if-eqz v4, :cond_5

    .line 378
    :try_start_0
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    move v0, v1

    move v3, v1

    .line 380
    :goto_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 382
    const/4 v0, 0x1

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_0

    move v3, v1

    .line 383
    :goto_1
    const/4 v0, 0x2

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    goto :goto_0

    :cond_0
    move v3, v2

    .line 382
    goto :goto_1

    :cond_1
    move v0, v2

    .line 383
    goto :goto_0

    .line 386
    :cond_2
    const/4 v1, 0x0

    invoke-static {p0, v3, v0, v1}, Lcom/msc/openprovider/b;->b(Landroid/content/Context;ZZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 401
    :goto_2
    if-eqz v4, :cond_3

    .line 403
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 411
    :cond_3
    :goto_3
    return-void

    .line 390
    :cond_4
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ODM"

    const-string v1, "Cursor count is 0"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 396
    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 401
    if-eqz v4, :cond_3

    .line 403
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 394
    :cond_5
    :try_start_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ODM"

    const-string v1, "Cursor is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 401
    :catchall_0
    move-exception v0

    if-eqz v4, :cond_6

    .line 403
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 409
    :cond_7
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ODM"

    const-string v1, "Samsung account is not signed-in state"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public static g(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 421
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ODM"

    const-string v3, "saveNameValidResultToOpenDB"

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 426
    invoke-static {p0}, Lcom/msc/openprovider/b;->h(Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v4

    .line 431
    if-eqz v4, :cond_5

    .line 433
    :try_start_0
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    move v0, v1

    move v3, v1

    .line 435
    :goto_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 437
    const/4 v0, 0x1

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_0

    move v3, v1

    .line 438
    :goto_1
    const/4 v0, 0x3

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    goto :goto_0

    :cond_0
    move v3, v2

    .line 437
    goto :goto_1

    :cond_1
    move v0, v2

    .line 438
    goto :goto_0

    .line 441
    :cond_2
    const/4 v1, 0x0

    invoke-static {p0, v3, v1, v0}, Lcom/msc/openprovider/b;->b(Landroid/content/Context;ZZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 456
    :goto_2
    if-eqz v4, :cond_3

    .line 458
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 466
    :cond_3
    :goto_3
    return-void

    .line 445
    :cond_4
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ODM"

    const-string v1, "Cursor count is 0"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 451
    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 456
    if-eqz v4, :cond_3

    .line 458
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 449
    :cond_5
    :try_start_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ODM"

    const-string v1, "Cursor is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 456
    :catchall_0
    move-exception v0

    if-eqz v4, :cond_6

    .line 458
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 464
    :cond_7
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ODM"

    const-string v1, "Samsung account is not signed-in state"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method private static h(Landroid/content/Context;)Landroid/database/Cursor;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 503
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ODM"

    const-string v1, "getTncCursor"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "Key"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "Value1"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "Value2"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "Value3"

    aput-object v1, v2, v0

    const/4 v0, 0x4

    const-string v1, "Value"

    aput-object v1, v2, v0

    .line 507
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 512
    :try_start_0
    sget-object v1, Lcom/msc/openprovider/b;->a:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 518
    :goto_0
    return-object v0

    .line 513
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v6

    goto :goto_0
.end method
