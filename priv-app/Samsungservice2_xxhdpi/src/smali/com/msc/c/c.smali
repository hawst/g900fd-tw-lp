.class public final Lcom/msc/c/c;
.super Ljava/lang/Object;
.source "DbManager.java"


# direct methods
.method public static a(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 487
    const-string v0, ""

    .line 490
    :try_start_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 492
    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v1

    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/msc/c/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lcom/msc/c/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 498
    :cond_0
    :goto_0
    invoke-virtual {p0, p1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    return-object p0

    .line 494
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 315
    const/4 v0, 0x0

    .line 322
    :try_start_0
    new-instance v1, Lcom/osp/security/identity/d;

    invoke-direct {v1, p0}, Lcom/osp/security/identity/d;-><init>(Landroid/content/Context;)V

    .line 323
    invoke-virtual {v1}, Lcom/osp/security/identity/d;->c()Ljava/lang/String;
    :try_end_0
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 329
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DbManager::getBirthDate_v01 birthDate : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 331
    return-object v0

    .line 324
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 148
    :try_start_0
    new-instance v0, Lcom/osp/social/member/d;

    invoke-direct {v0, p0}, Lcom/osp/social/member/d;-><init>(Landroid/content/Context;)V

    .line 149
    invoke-virtual {v0, p1}, Lcom/osp/social/member/d;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    :goto_0
    return-void

    .line 150
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/social/member/MemberServiceException;->printStackTrace()V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 166
    :try_start_0
    new-instance v0, Lcom/osp/security/identity/f;

    invoke-direct {v0, p0}, Lcom/osp/security/identity/f;-><init>(Landroid/content/Context;)V

    .line 167
    invoke-virtual {v0, p1, p2}, Lcom/osp/security/identity/f;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    :goto_0
    return-void

    .line 168
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 129
    :try_start_0
    new-instance v0, Lcom/osp/security/credential/a;

    invoke-direct {v0, p0}, Lcom/osp/security/credential/a;-><init>(Landroid/content/Context;)V

    .line 130
    invoke-virtual {v0, p1, p2}, Lcom/osp/security/credential/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    invoke-virtual {v0, p1, p3, p4}, Lcom/osp/security/credential/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    :goto_0
    return-void

    .line 133
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Z)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 951
    if-nez p0, :cond_1

    .line 967
    :cond_0
    :goto_0
    return v2

    .line 956
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DBM"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Is Checking both Data V01 and V02 : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 957
    invoke-static {p0}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 960
    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "DBM"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "V02 Data valid : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 961
    if-eqz v0, :cond_5

    if-eqz p1, :cond_5

    .line 963
    invoke-static {p0}, Lcom/msc/c/c;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    move v3, v1

    .line 964
    :goto_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v4, "DBM"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "V01 Data valid : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 967
    :goto_3
    if-eqz v0, :cond_0

    if-eqz p1, :cond_2

    if-eqz v3, :cond_0

    :cond_2
    move v2, v1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 957
    goto :goto_1

    :cond_4
    move v3, v2

    .line 963
    goto :goto_2

    :cond_5
    move v3, v2

    goto :goto_3
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x3

    .line 201
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v1

    .line 209
    if-eqz v1, :cond_5

    invoke-static {p0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    const-string v2, "OSP_02"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    :cond_0
    const-string v2, "OSP_02"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0}, Lcom/msc/c/c;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    :cond_1
    const-string v2, "OSP_01"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {p0}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    :cond_2
    const-string v2, "OSP_01"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {p0}, Lcom/msc/c/c;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 215
    :cond_3
    const-string v1, "AGREE_TO_DISCLAIMER"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 217
    const/4 v0, 0x7

    .line 240
    :cond_4
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DbManager::AccountModeCheck state : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 242
    return v0

    .line 225
    :cond_5
    if-eqz v1, :cond_6

    .line 227
    invoke-static {p0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 229
    const/4 v0, 0x1

    goto :goto_0

    .line 236
    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 381
    const/4 v0, 0x0

    .line 388
    :try_start_0
    new-instance v1, Lcom/osp/security/identity/d;

    invoke-direct {v1, p0}, Lcom/osp/security/identity/d;-><init>(Landroid/content/Context;)V

    .line 389
    invoke-virtual {v1}, Lcom/osp/security/identity/d;->d()Ljava/lang/String;
    :try_end_0
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 395
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DbManager::getUserAuthTokenV01 userAuthToken : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 397
    return-object v0

    .line 390
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 749
    :try_start_0
    new-instance v0, Lcom/osp/common/property/a;

    invoke-direct {v0, p0}, Lcom/osp/common/property/a;-><init>(Landroid/content/Context;)V

    .line 750
    invoke-virtual {v0, p1}, Lcom/osp/common/property/a;->c(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/osp/common/property/PropertyException; {:try_start_0 .. :try_end_0} :catch_0

    .line 755
    :goto_0
    return-void

    .line 751
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/common/property/PropertyException;->printStackTrace()V

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 433
    const/4 v0, 0x0

    .line 440
    :try_start_0
    new-instance v1, Lcom/osp/security/identity/d;

    invoke-direct {v1, p0}, Lcom/osp/security/identity/d;-><init>(Landroid/content/Context;)V

    .line 441
    invoke-virtual {v1}, Lcom/osp/security/identity/d;->f()Ljava/lang/String;
    :try_end_0
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 447
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DbManager::getUserDeviceID UserDeviceID : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 449
    return-object v0

    .line 442
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 459
    const/4 v0, 0x0

    .line 466
    :try_start_0
    new-instance v1, Lcom/osp/security/identity/d;

    invoke-direct {v1, p0}, Lcom/osp/security/identity/d;-><init>(Landroid/content/Context;)V

    .line 467
    invoke-virtual {v1}, Lcom/osp/security/identity/d;->a()Ljava/lang/String;
    :try_end_0
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 473
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DbManager::getUserID_v01 UserID : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 475
    return-object v0

    .line 468
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    goto :goto_0
.end method

.method public static e(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 509
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DbManager::removePassword_v01"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 518
    :try_start_0
    new-instance v0, Lcom/osp/security/identity/d;

    invoke-direct {v0, p0}, Lcom/osp/security/identity/d;-><init>(Landroid/content/Context;)V

    .line 519
    invoke-virtual {v0}, Lcom/osp/security/identity/d;->j()V
    :try_end_0
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 524
    :goto_0
    return-void

    .line 520
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    goto :goto_0
.end method

.method public static f(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 924
    const/4 v0, 0x0

    .line 932
    :try_start_0
    new-instance v1, Lcom/osp/security/identity/d;

    invoke-direct {v1, p0}, Lcom/osp/security/identity/d;-><init>(Landroid/content/Context;)V

    .line 933
    invoke-virtual {v1}, Lcom/osp/security/identity/d;->m()Ljava/lang/String;
    :try_end_0
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 939
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DbManager::getServerUrl serverUrl : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 941
    return-object v0

    .line 934
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    goto :goto_0
.end method

.method public static g(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1003
    :try_start_0
    new-instance v0, Lcom/osp/security/identity/d;

    invoke-direct {v0, p0}, Lcom/osp/security/identity/d;-><init>(Landroid/content/Context;)V

    .line 1004
    invoke-virtual {v0}, Lcom/osp/security/identity/d;->k()V
    :try_end_0
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1016
    :goto_0
    :try_start_1
    new-instance v0, Lcom/osp/security/credential/a;

    invoke-direct {v0, p0}, Lcom/osp/security/credential/a;-><init>(Landroid/content/Context;)V

    .line 1017
    invoke-virtual {v0}, Lcom/osp/security/credential/a;->a()V
    :try_end_1
    .catch Lcom/osp/security/credential/CredentialException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1028
    :goto_1
    :try_start_2
    new-instance v0, Lcom/osp/social/member/d;

    invoke-direct {v0, p0}, Lcom/osp/social/member/d;-><init>(Landroid/content/Context;)V

    .line 1029
    invoke-virtual {v0}, Lcom/osp/social/member/d;->a()V
    :try_end_2
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1034
    :goto_2
    return-void

    .line 1005
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    goto :goto_0

    .line 1018
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/security/credential/CredentialException;->printStackTrace()V

    goto :goto_1

    .line 1030
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/social/member/MemberServiceException;->printStackTrace()V

    goto :goto_2
.end method
