.class public final Lcom/msc/c/h;
.super Ljava/lang/Object;
.source "HttpResponseHandler.java"


# static fields
.field private static b:Lcom/msc/c/h;


# instance fields
.field a:Lcom/osp/app/signin/kd;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1317
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    .line 108
    return-void
.end method

.method public static A(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 4466
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4467
    const/4 v3, 0x0

    .line 4473
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v2

    .line 4474
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 4475
    invoke-virtual {v2}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v6

    .line 4478
    const-string v2, "&"

    const-string v4, "&amp;"

    invoke-virtual {p0, v2, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4479
    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, v4}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 4481
    :try_start_1
    invoke-interface {v6, v2}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 4482
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v4

    move v5, v4

    move v4, v3

    move-object v3, v1

    .line 4484
    :goto_0
    if-eq v5, v8, :cond_6

    .line 4486
    packed-switch v5, :pswitch_data_0

    .line 4523
    :cond_0
    :goto_1
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v5

    goto :goto_0

    .line 4489
    :pswitch_0
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    .line 4490
    const-string v7, "usePackageInfo"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 4492
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v4

    .line 4493
    invoke-static {v4}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v4

    goto :goto_1

    .line 4494
    :cond_1
    const-string v7, "packageInfo"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 4496
    new-instance v3, Lcom/msc/c/i;

    invoke-direct {v3}, Lcom/msc/c/i;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 4527
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_2

    .line 4529
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_2
    throw v0

    .line 4498
    :cond_3
    if-eqz v3, :cond_0

    .line 4500
    :try_start_2
    const-string v7, "appID"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 4502
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/msc/c/i;->a(Lcom/msc/c/i;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    .line 4503
    :cond_4
    const-string v7, "package"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 4505
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/msc/c/i;->b(Lcom/msc/c/i;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    .line 4506
    :cond_5
    const-string v7, "signature"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 4508
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/msc/c/i;->c(Lcom/msc/c/i;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    .line 4513
    :pswitch_1
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    .line 4514
    const-string v7, "packageInfo"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    if-eqz v3, :cond_0

    .line 4516
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 4527
    :cond_6
    invoke-virtual {v2}, Ljava/io/StringReader;->close()V

    .line 4532
    if-eqz v4, :cond_7

    .line 4537
    :goto_3
    return-object v0

    :cond_7
    move-object v0, v1

    goto :goto_3

    .line 4527
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 4486
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static B(Ljava/lang/String;)Lcom/osp/app/signin/el;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 4551
    new-instance v3, Lcom/osp/app/signin/el;

    invoke-direct {v3}, Lcom/osp/app/signin/el;-><init>()V

    .line 4552
    const/4 v2, 0x0

    .line 4555
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 4556
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 4557
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 4560
    const-string v0, "&"

    const-string v1, "&amp;"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4561
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 4563
    :try_start_1
    invoke-interface {v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 4564
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 4566
    :goto_0
    if-eq v0, v5, :cond_6

    .line 4568
    packed-switch v0, :pswitch_data_0

    .line 4596
    :cond_0
    :goto_1
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 4571
    :pswitch_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 4572
    const-string v2, "imageURL"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4574
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/osp/app/signin/el;->a:Ljava/lang/String;

    .line 4576
    :cond_1
    const-string v2, "serviceName"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 4578
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/osp/app/signin/el;->b:Ljava/lang/String;

    .line 4580
    :cond_2
    const-string v2, "companyName"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 4582
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/osp/app/signin/el;->c:Ljava/lang/String;

    .line 4584
    :cond_3
    const-string v2, "thirdPartyTermsURL"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 4586
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/osp/app/signin/el;->d:Ljava/lang/String;

    .line 4588
    :cond_4
    const-string v2, "thirdPartyPPURL"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4590
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/osp/app/signin/el;->e:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 4600
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_5

    .line 4602
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_5
    throw v0

    .line 4600
    :cond_6
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 4605
    return-object v3

    .line 4600
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    .line 4568
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static C(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 4616
    const/4 v0, 0x0

    .line 4617
    const/4 v2, 0x0

    .line 4620
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v1

    .line 4621
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 4622
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 4624
    const-string v1, "&"

    const-string v5, "&amp;"

    invoke-virtual {p0, v1, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 4625
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, v5}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4626
    :try_start_1
    invoke-interface {v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 4628
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    .line 4630
    :goto_0
    if-eq v2, v3, :cond_1

    .line 4632
    packed-switch v2, :pswitch_data_0

    .line 4647
    :cond_0
    :goto_1
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    goto :goto_0

    .line 4635
    :pswitch_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 4636
    const-string v5, "boolean"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4638
    const-string v2, "true"

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    if-eqz v2, :cond_0

    move v0, v3

    .line 4640
    goto :goto_1

    .line 4651
    :cond_1
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 4657
    return v0

    .line 4651
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_2

    .line 4653
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_2
    throw v0

    .line 4651
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 4632
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static D(Ljava/lang/String;)Lcom/osp/app/signin/fv;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 4907
    new-instance v3, Lcom/osp/app/signin/fv;

    invoke-direct {v3}, Lcom/osp/app/signin/fv;-><init>()V

    .line 4908
    const/4 v2, 0x0

    .line 4911
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 4912
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 4913
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 4915
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 4917
    :try_start_1
    invoke-interface {v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 4918
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 4920
    :goto_0
    if-eq v0, v5, :cond_b

    .line 4922
    packed-switch v0, :pswitch_data_0

    .line 4971
    :cond_0
    :goto_1
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 4925
    :pswitch_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 4926
    const-string v2, "name"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4928
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/osp/app/signin/fv;->a(Ljava/lang/String;)V

    .line 4929
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/osp/app/signin/fv;->a(Ljava/lang/Boolean;)V

    .line 4931
    :cond_1
    const-string v2, "title"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 4933
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/osp/app/signin/fv;->b(Ljava/lang/String;)V

    .line 4935
    :cond_2
    const-string v2, "body"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 4937
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/osp/app/signin/fv;->c(Ljava/lang/String;)V

    .line 4939
    :cond_3
    const-string v2, "description"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 4941
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/osp/app/signin/fv;->d(Ljava/lang/String;)V

    .line 4943
    :cond_4
    const-string v2, "imageURL"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 4945
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/osp/app/signin/fv;->e(Ljava/lang/String;)V

    .line 4947
    :cond_5
    const-string v2, "welcomeContent"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 4949
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/osp/app/signin/fv;->f(Ljava/lang/String;)V

    .line 4951
    :cond_6
    const-string v2, "notificationTitle"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 4953
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/osp/app/signin/fv;->g(Ljava/lang/String;)V

    .line 4955
    :cond_7
    const-string v2, "notificationContent"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 4957
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/osp/app/signin/fv;->h(Ljava/lang/String;)V

    .line 4959
    :cond_8
    const-string v2, "serviceURL"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 4961
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/osp/app/signin/fv;->i(Ljava/lang/String;)V

    .line 4963
    :cond_9
    const-string v2, "serviceTypeCode"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4965
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/osp/app/signin/fv;->j(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 4975
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_a

    .line 4977
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_a
    throw v0

    .line 4975
    :cond_b
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 4980
    return-object v3

    .line 4975
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    .line 4922
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static E(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 5026
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 5028
    const/4 v2, 0x0

    .line 5032
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 5033
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 5034
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 5036
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 5038
    :try_start_1
    invoke-interface {v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 5039
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 5041
    :goto_0
    if-eq v0, v5, :cond_4

    .line 5043
    packed-switch v0, :pswitch_data_0

    .line 5061
    :cond_0
    :goto_1
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 5046
    :pswitch_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 5047
    const-string v2, "authenticateToken"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 5049
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 5065
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    .line 5067
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_1
    throw v0

    .line 5050
    :cond_2
    :try_start_2
    const-string v2, "prefix"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 5052
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 5053
    :cond_3
    const-string v2, "limitExpireTime"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5055
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 5065
    :cond_4
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 5071
    return-object v3

    .line 5065
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    .line 5043
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static F(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 4

    .prologue
    .line 5084
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 5086
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {p0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5088
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 5090
    const-string v2, "auth_status"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5092
    const-string v2, "auth_status"

    const-string v3, "auth_status"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5094
    :cond_0
    const-string v2, "msisdn"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 5096
    const-string v2, "msisdn"

    const-string v3, "msisdn"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5098
    :cond_1
    return-object v0
.end method

.method public static G(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 5133
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 5135
    const/4 v2, 0x0

    .line 5139
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 5140
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 5141
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 5143
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 5145
    :try_start_1
    invoke-interface {v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 5146
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 5148
    :goto_0
    if-eq v0, v5, :cond_3

    .line 5150
    packed-switch v0, :pswitch_data_0

    .line 5165
    :cond_0
    :goto_1
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 5153
    :pswitch_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 5154
    const-string v2, "boolean"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 5156
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 5169
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    .line 5171
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_1
    throw v0

    .line 5157
    :cond_2
    :try_start_2
    const-string v2, "validationTime"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5159
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 5169
    :cond_3
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 5175
    return-object v3

    .line 5169
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    .line 5150
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static H(Ljava/lang/String;)Lcom/msc/a/j;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 5186
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SaResponseHandler::parseSecurityQuestionfromXML"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5188
    new-instance v3, Lcom/msc/a/j;

    invoke-direct {v3}, Lcom/msc/a/j;-><init>()V

    .line 5189
    const/4 v2, 0x0

    .line 5192
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 5193
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 5194
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 5196
    const-string v0, "&"

    const-string v1, "&amp;"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5197
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 5198
    :try_start_1
    invoke-interface {v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 5200
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 5202
    :goto_0
    if-eq v0, v6, :cond_2

    .line 5204
    packed-switch v0, :pswitch_data_0

    .line 5217
    :cond_0
    :goto_1
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 5207
    :pswitch_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 5208
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "security question info from server is "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5209
    const-string v2, "string"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5211
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/msc/a/j;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 5221
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    .line 5223
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_1
    throw v0

    .line 5221
    :cond_2
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 5226
    return-object v3

    .line 5221
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    .line 5204
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static I(Ljava/lang/String;)Lcom/msc/a/j;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 5237
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SaResponseHandler::parseSecurityQuestionfromXML"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5239
    new-instance v3, Lcom/msc/a/j;

    invoke-direct {v3}, Lcom/msc/a/j;-><init>()V

    .line 5240
    const/4 v2, 0x0

    .line 5243
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 5244
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 5245
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 5247
    const-string v0, "&"

    const-string v1, "&amp;"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5248
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 5249
    :try_start_1
    invoke-interface {v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 5251
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 5253
    :goto_0
    if-eq v0, v6, :cond_5

    .line 5255
    packed-switch v0, :pswitch_data_0

    .line 5277
    :cond_0
    :goto_1
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 5258
    :pswitch_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 5259
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "security question info from server is "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5260
    const-string v2, "securityQuestionID"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 5262
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/msc/a/j;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 5281
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    .line 5283
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_1
    throw v0

    .line 5263
    :cond_2
    :try_start_2
    const-string v2, "securityQuestionText"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 5265
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/msc/a/j;->b(Ljava/lang/String;)V

    goto :goto_1

    .line 5266
    :cond_3
    const-string v2, "securityAnswerText"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 5268
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/msc/a/j;->c(Ljava/lang/String;)V

    goto :goto_1

    .line 5269
    :cond_4
    const-string v2, "securityToken"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5271
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/msc/a/j;->d(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 5281
    :cond_5
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 5286
    return-object v3

    .line 5281
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    .line 5255
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static J(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 5298
    .line 5302
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 5303
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 5304
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v3

    .line 5307
    const-string v0, "&"

    const-string v1, "&amp;"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5308
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5310
    :try_start_1
    invoke-interface {v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 5311
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 5312
    :goto_0
    if-eq v0, v5, :cond_1

    .line 5315
    packed-switch v0, :pswitch_data_0

    .line 5327
    :cond_0
    :goto_1
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 5318
    :pswitch_0
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 5319
    const-string v4, "boolean"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5321
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    goto :goto_1

    .line 5331
    :cond_1
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 5337
    return-object v2

    .line 5331
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_2

    .line 5333
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_2
    throw v0

    .line 5331
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 5315
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static K(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 5349
    .line 5353
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 5354
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 5355
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v3

    .line 5358
    const-string v0, "&"

    const-string v1, "&amp;"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5359
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5361
    :try_start_1
    invoke-interface {v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 5362
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    move-object v6, v2

    move v2, v0

    move-object v0, v6

    .line 5363
    :goto_0
    if-eq v2, v5, :cond_1

    .line 5366
    packed-switch v2, :pswitch_data_0

    .line 5378
    :cond_0
    :goto_1
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    goto :goto_0

    .line 5369
    :pswitch_0
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 5370
    const-string v4, "boolean"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5372
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    goto :goto_1

    .line 5382
    :cond_1
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 5387
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 5389
    const-string v0, "false"

    .line 5392
    :cond_2
    return-object v0

    .line 5382
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_3

    .line 5384
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_3
    throw v0

    .line 5382
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 5366
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static L(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 5478
    .line 5482
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 5483
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 5484
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v3

    .line 5487
    const-string v0, "&"

    const-string v1, "&amp;"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5488
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5490
    :try_start_1
    invoke-interface {v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 5491
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 5492
    :goto_0
    if-eq v0, v5, :cond_1

    .line 5495
    packed-switch v0, :pswitch_data_0

    .line 5507
    :cond_0
    :goto_1
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 5498
    :pswitch_0
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 5499
    const-string v4, "string"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5501
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    goto :goto_1

    .line 5511
    :cond_1
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 5517
    return-object v2

    .line 5511
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_2

    .line 5513
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_2
    throw v0

    .line 5511
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 5495
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static M(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 5528
    .line 5532
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 5533
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 5534
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v3

    .line 5537
    const-string v0, "&"

    const-string v1, "&amp;"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5538
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5540
    :try_start_1
    invoke-interface {v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 5541
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    move-object v6, v2

    move v2, v0

    move-object v0, v6

    .line 5542
    :goto_0
    if-eq v2, v5, :cond_1

    .line 5545
    packed-switch v2, :pswitch_data_0

    .line 5557
    :cond_0
    :goto_1
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    goto :goto_0

    .line 5548
    :pswitch_0
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 5549
    const-string v4, "boolean"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5551
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    goto :goto_1

    .line 5561
    :cond_1
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 5566
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 5568
    const-string v0, "false"

    .line 5571
    :cond_2
    return-object v0

    .line 5561
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_3

    .line 5563
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_3
    throw v0

    .line 5561
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 5545
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static N(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 5582
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SaResponseHandler::parseGetUserIDFromXML"

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5588
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v1

    .line 5589
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 5590
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 5593
    const-string v1, "&"

    const-string v3, "&amp;"

    invoke-virtual {p0, v1, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 5594
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, v3}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5596
    :try_start_1
    invoke-interface {v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 5597
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    .line 5598
    :goto_0
    if-eq v3, v0, :cond_1

    .line 5601
    packed-switch v3, :pswitch_data_0

    .line 5613
    :cond_0
    :goto_1
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    goto :goto_0

    .line 5604
    :pswitch_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 5605
    const-string v5, "boolean"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 5607
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    goto :goto_1

    .line 5617
    :cond_1
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 5623
    const-string v1, "true"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :goto_2
    return v0

    .line 5617
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    if-eqz v1, :cond_2

    .line 5619
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_2
    throw v0

    .line 5623
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 5617
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 5601
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static O(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 5635
    .line 5639
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v1

    .line 5640
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 5641
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 5644
    const-string v1, "&"

    const-string v3, "&amp;"

    invoke-virtual {p0, v1, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 5645
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, v3}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5647
    :try_start_1
    invoke-interface {v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 5648
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    .line 5649
    :goto_0
    if-eq v3, v0, :cond_1

    .line 5652
    packed-switch v3, :pswitch_data_0

    .line 5664
    :cond_0
    :goto_1
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    goto :goto_0

    .line 5655
    :pswitch_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 5656
    const-string v5, "boolean"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 5658
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    goto :goto_1

    .line 5668
    :cond_1
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 5674
    const-string v1, "true"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :goto_2
    return v0

    .line 5668
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    if-eqz v1, :cond_2

    .line 5670
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_2
    throw v0

    .line 5674
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 5668
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 5652
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static a()Lcom/msc/c/h;
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lcom/msc/c/h;->b:Lcom/msc/c/h;

    if-eqz v0, :cond_0

    .line 118
    sget-object v0, Lcom/msc/c/h;->b:Lcom/msc/c/h;

    .line 122
    :goto_0
    return-object v0

    .line 121
    :cond_0
    new-instance v0, Lcom/msc/c/h;

    invoke-direct {v0}, Lcom/msc/c/h;-><init>()V

    .line 122
    sput-object v0, Lcom/msc/c/h;->b:Lcom/msc/c/h;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 242
    .line 246
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 248
    const-string v0, "access_token"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 250
    const-string v0, "access_token"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 252
    :goto_0
    const-string v3, "refresh_token"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 254
    const-string v1, "refresh_token"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 256
    :cond_0
    const-string v3, "state"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 258
    const-string v0, "state"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 260
    :cond_1
    const-string v3, "error_description"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 262
    const-string v0, "error_description"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 264
    :cond_2
    const-string v3, "access_token_expires_in"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 266
    const-string v3, "access_token_expires_in"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 267
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 268
    const-string v4, "ACCESS_TOKEN_EXPIRED_DATE_KEY"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 269
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 272
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SaAuthManager::access_tokenfromJSON accessToken : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " refreshToken : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 274
    if-eqz v0, :cond_5

    .line 283
    :cond_4
    :goto_1
    return-object v0

    .line 278
    :cond_5
    if-eqz v1, :cond_4

    move-object v0, v1

    .line 280
    goto :goto_1

    :cond_6
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 171
    const/4 v0, 0x0

    .line 172
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {p0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 174
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 176
    const-string v2, "userauth_token"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 178
    const-string v0, "userauth_token"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 180
    :cond_0
    const-string v2, "error"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 182
    const-string v0, "error"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 184
    :cond_1
    const-string v2, "state"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 186
    const-string v0, "state"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 188
    :cond_2
    const-string v2, "error_description"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 190
    const-string v0, "error_description"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 193
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RequestSet::userauth_tokenfromJSON userauthToken : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 195
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 3663
    const/4 v0, 0x0

    .line 3665
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 3666
    const-string v1, "issuetime"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3668
    const-string v1, "issuetime"

    const-string v4, "issuetime"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p1, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3670
    :cond_0
    const-string v1, "confirmed"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3672
    const-string v0, "confirmed"

    const-string v1, "confirmed"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3673
    const-string v0, "confirmed"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 3675
    :cond_1
    const-string v1, "expiretime"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3677
    const-string v1, "expiretime"

    const-string v4, "expiretime"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p1, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3679
    :cond_2
    const-string v1, "userid"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 3681
    const-string v1, "userid"

    const-string v4, "userid"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3682
    const-string v1, "userid"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3684
    :goto_0
    const-string v4, "signerid"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 3686
    const-string v4, "signerid"

    const-string v5, "signerid"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3688
    :cond_3
    const-string v4, "signature"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 3690
    const-string v4, "signature"

    const-string v5, "signature"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3693
    :cond_4
    if-eqz v0, :cond_5

    .line 3698
    :goto_1
    return-object v1

    :cond_5
    move-object v1, v2

    goto :goto_1

    :cond_6
    move-object v1, v2

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/a/d;Lcom/msc/a/i;)V
    .locals 8

    .prologue
    .line 3272
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3274
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 3277
    const-string v0, "userInfo"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1c

    .line 3279
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, " Server Response TNC Check"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3281
    const-string v0, "userInfo"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 3282
    invoke-virtual {p4}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v4

    .line 3285
    const-string v0, "requireTncAccepted"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3287
    const-string v0, "requireTncAccepted"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3288
    const-string v5, "Y"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/msc/a/g;->a(Z)V

    .line 3289
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v5, "SRH"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "auoaau rsp TA : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "Y"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    const-string v0, "1"

    :goto_0
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3291
    :cond_0
    const-string v0, "privacyAccepted"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3293
    const-string v0, "privacyAccepted"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3294
    const-string v5, "Y"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/msc/a/g;->b(Z)V

    .line 3295
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v5, "SRH"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "auoaau rsp PA : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "Y"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    const-string v0, "1"

    :goto_1
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3297
    :cond_1
    const-string v0, "dataCollectionAccepted"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3299
    const-string v0, "dataCollectionAccepted"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3300
    const-string v5, "Y"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/msc/a/g;->c(Z)V

    .line 3301
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v5, "SRH"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "auoaau rsp DCA : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "Y"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    const-string v0, "1"

    :goto_2
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3303
    :cond_2
    const-string v0, "onwardTransferAccepted"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3305
    const-string v0, "onwardTransferAccepted"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3306
    const-string v5, "Y"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/msc/a/g;->d(Z)V

    .line 3307
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v5, "SRH"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "auoaau rsp OTA : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "Y"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_28

    const-string v0, "1"

    :goto_3
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3309
    :cond_3
    const-string v0, "userID"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3311
    const-string v0, "userID"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3312
    invoke-virtual {v4, v0}, Lcom/msc/a/g;->a(Ljava/lang/String;)V

    .line 3314
    :cond_4
    const-string v0, "mobileCountryCode"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 3316
    const-string v0, "mobileCountryCode"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3317
    invoke-virtual {v4, v0}, Lcom/msc/a/g;->c(Ljava/lang/String;)V

    .line 3318
    invoke-static {p0, v0}, Lcom/msc/c/e;->f(Landroid/content/Context;Ljava/lang/String;)V

    .line 3320
    :cond_5
    const-string v0, "requireNameCheck"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 3322
    const-string v0, "requireNameCheck"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3323
    const-string v5, "Y"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v4, v0}, Lcom/msc/a/g;->e(Z)V

    .line 3325
    :cond_6
    const-string v0, "requireEmailValidation"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 3327
    const-string v0, "requireEmailValidation"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3328
    const-string v5, "Y"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 3330
    invoke-virtual {v4}, Lcom/msc/a/g;->i()V

    .line 3341
    :cond_7
    :goto_4
    const-string v0, "is3rdParty"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 3343
    const-string v0, "is3rdParty"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3344
    const-string v5, "Y"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v4, v0}, Lcom/msc/a/g;->f(Z)V

    .line 3346
    :cond_8
    const-string v0, "requireDisclaimer"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 3348
    const-string v0, "requireDisclaimer"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3349
    const-string v5, "Y"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v4, v0}, Lcom/msc/a/g;->g(Z)V

    .line 3351
    :cond_9
    const-string v0, "userCountryCode"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 3353
    const-string v0, "userCountryCode"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3354
    invoke-virtual {v4, v0}, Lcom/msc/a/g;->d(Ljava/lang/String;)V

    .line 3362
    const-string v4, "SERVER_URL"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 3363
    const-string v5, "user_country_code"

    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/osp/app/util/i;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 3364
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 3369
    :cond_a
    const-string v0, "mandatoryCheckListVO"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1b

    .line 3371
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, " Server Response MandatoryList Check"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3372
    const-string v0, "mandatoryCheckListVO"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 3373
    invoke-virtual {p4}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v3

    .line 3375
    const-string v4, "countryCode"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 3377
    const-string v4, "countryCode"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3378
    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->s()V

    .line 3380
    :cond_b
    const-string v4, "birthDate"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 3382
    const-string v4, "birthDate"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3383
    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->t()V

    .line 3386
    :cond_c
    const-string v4, "prefixName"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 3388
    const-string v4, "prefixNa_"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3389
    const-string v4, "prefixName"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 3390
    const-string v5, "@selectionElementList"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->b(Ljava/lang/String;)V

    .line 3392
    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->u()V

    .line 3393
    const-string v4, "prefixName"

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 3395
    :cond_d
    const-string v4, "givenName"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 3397
    const-string v4, "givenNa_"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3398
    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->w()V

    .line 3399
    const-string v4, "givenName"

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 3401
    :cond_e
    const-string v4, "familyName"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 3403
    const-string v4, "familyNa_"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3404
    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->y()V

    .line 3405
    const-string v4, "familyName"

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 3407
    :cond_f
    const-string v4, "localityText"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 3409
    const-string v4, "localityText"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3410
    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->A()V

    .line 3411
    const-string v4, "localityText"

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 3413
    :cond_10
    const-string v4, "postalCodeText"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 3415
    const-string v4, "postalCodeText"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3416
    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->K()V

    .line 3417
    const-string v4, "postalCodeText"

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 3419
    :cond_11
    const-string v4, "receiveSMSPhoneNumberText"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 3421
    const-string v4, "receiveSMSPhoneNumberText"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3422
    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->M()V

    .line 3423
    const-string v4, "receiveSMSPhoneNumberText"

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 3425
    :cond_12
    const-string v4, "streetText"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 3427
    const-string v4, "streetText"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3428
    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->C()V

    .line 3429
    const-string v4, "streetText"

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 3431
    :cond_13
    const-string v4, "extendedText"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 3433
    const-string v4, "extendedText"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3434
    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->E()V

    .line 3435
    const-string v4, "extendedText"

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 3437
    :cond_14
    const-string v4, "postOfficeBoxNumberText"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 3439
    const-string v4, "postOfficeBoxNumberText"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3440
    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->G()V

    .line 3441
    const-string v4, "postOfficeBoxNumberText"

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 3443
    :cond_15
    const-string v4, "regionText"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_16

    .line 3445
    const-string v4, "regionText"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3446
    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->I()V

    .line 3447
    const-string v4, "regionText"

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 3449
    :cond_16
    const-string v4, "genderTypeCode"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 3451
    const-string v4, "genderTypeCode"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3452
    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->P()V

    .line 3453
    const-string v4, "genderTypeCode"

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 3455
    :cond_17
    const-string v4, "userDisplayName"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 3457
    const-string v4, "userDisplayNa_"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3458
    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->R()V

    .line 3459
    const-string v4, "userDisplayName"

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 3461
    :cond_18
    const-string v4, "relationshipStatusCode"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_19

    .line 3463
    const-string v4, "relationshipStatusCode"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3464
    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->T()V

    .line 3465
    const-string v4, "relationshipStatusCode"

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 3470
    :cond_19
    const-string v4, "emailReceiveYNFlag"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 3472
    const-string v0, "emReceiveYNFlag"

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3473
    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->O()V

    .line 3474
    const-string v0, "emailReceiveYNFlag"

    invoke-virtual {v3, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 3476
    :cond_1a
    const-string v0, "parseAndSaveAuthWithTncMandatory"

    invoke-static {v0, v1}, Lcom/msc/c/h;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 3479
    :cond_1b
    invoke-virtual {p4}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/msc/a/g;->a(Landroid/content/Context;)V

    .line 3480
    invoke-virtual {p4}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/osp/app/signin/kd;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 3485
    :cond_1c
    const-string v0, "token"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_24

    .line 3488
    const-string v0, "token"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 3490
    const-string v1, "userauth_token"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 3492
    const-string v1, "userauth_token"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3493
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ACCESSTOKENTEST parseAuthenticationV02 authToken : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3494
    invoke-virtual {p3, v1}, Lcom/msc/a/d;->b(Ljava/lang/String;)V

    .line 3496
    :cond_1d
    const-string v1, "access_token"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 3498
    const-string v1, "access_token"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3499
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ACCESSTOKENTEST parseAuthenticationV02 accessToken : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3500
    invoke-virtual {p3, v1}, Lcom/msc/a/d;->c(Ljava/lang/String;)V

    .line 3502
    :cond_1e
    const-string v1, "refresh_token"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 3504
    const-string v1, "refresh_token"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3505
    invoke-virtual {p3, v1}, Lcom/msc/a/d;->d(Ljava/lang/String;)V

    .line 3507
    :cond_1f
    const-string v1, "access_token_expires_in"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_20

    .line 3509
    const-string v1, "access_token_expires_in"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3510
    invoke-virtual {p3, v2, v3}, Lcom/msc/a/d;->a(J)V

    .line 3512
    :cond_20
    const-string v1, "refresh_token_expires_in"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 3514
    const-string v1, "refresh_token_expires_in"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3515
    invoke-virtual {p3, v2, v3}, Lcom/msc/a/d;->b(J)V

    .line 3517
    :cond_21
    const-string v1, "userId"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_22

    .line 3519
    const-string v1, "userId"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3520
    invoke-virtual {p3, v1}, Lcom/msc/a/d;->e(Ljava/lang/String;)V

    .line 3522
    :cond_22
    const-string v1, "state"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_23

    .line 3524
    const-string v1, "state"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3525
    invoke-virtual {p3, v1}, Lcom/msc/a/d;->g(Ljava/lang/String;)V

    .line 3527
    :cond_23
    const-string v1, "error_description"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_24

    .line 3529
    const-string v1, "error_description"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3530
    invoke-virtual {p3, v0}, Lcom/msc/a/d;->h(Ljava/lang/String;)V

    .line 3533
    :cond_24
    return-void

    .line 3289
    :cond_25
    const-string v0, "0"

    goto/16 :goto_0

    .line 3295
    :cond_26
    const-string v0, "0"

    goto/16 :goto_1

    .line 3301
    :cond_27
    const-string v0, "0"

    goto/16 :goto_2

    .line 3307
    :cond_28
    const-string v0, "0"

    goto/16 :goto_3

    .line 3335
    :cond_29
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 3336
    const-string v5, "EMAIL_VALIDATION_KEY"

    invoke-static {p0}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 3337
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_4
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/a/i;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 2777
    const/4 v2, 0x0

    .line 2778
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2779
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 2784
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 2785
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 2787
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p2}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2789
    :try_start_1
    invoke-interface {v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 2790
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 2792
    :goto_0
    if-eq v0, v7, :cond_23

    .line 2794
    packed-switch v0, :pswitch_data_0

    .line 3006
    :cond_0
    :goto_1
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 2797
    :pswitch_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 2798
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "userTelephoneIDDuplicationResultListVO"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2800
    const-string v0, "DUPLICATED_ID"

    const-string v2, "Id duplicated"

    invoke-virtual {p3, v0, v2}, Lcom/msc/a/i;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 3013
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    .line 3015
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_1
    throw v0

    .line 2801
    :cond_2
    :try_start_2
    const-string v2, "requireTncAccepted"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2803
    const-string v0, "Y"

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2805
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRH"

    const-string v2, "v2prususre rsp TA : 1"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2806
    invoke-virtual {p3}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/msc/a/g;->a(Z)V

    goto :goto_1

    .line 2809
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRH"

    const-string v2, "v2prususre rsp TA : 0"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2811
    :cond_4
    const-string v2, "privacyAccepted"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2813
    const-string v0, "Y"

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2815
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRH"

    const-string v2, "v2prususre rsp PA : 1"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2816
    invoke-virtual {p3}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/msc/a/g;->b(Z)V

    goto/16 :goto_1

    .line 2819
    :cond_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRH"

    const-string v2, "v2prususre rsp PA : 0"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2821
    :cond_6
    const-string v2, "dataCollectionAccepted"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2823
    const-string v0, "Y"

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2825
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRH"

    const-string v2, "v2prususre rsp DCA : 1"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2826
    invoke-virtual {p3}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/msc/a/g;->c(Z)V

    goto/16 :goto_1

    .line 2829
    :cond_7
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRH"

    const-string v2, "v2prususre rsp DCA : 0"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2831
    :cond_8
    const-string v2, "onwardTransferAccepted"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2833
    const-string v0, "Y"

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2835
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRH"

    const-string v2, "v2prususre rsp OTA : 1"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2836
    invoke-virtual {p3}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/msc/a/g;->d(Z)V

    goto/16 :goto_1

    .line 2839
    :cond_9
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRH"

    const-string v2, "v2prususre rsp OTA : 0"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2841
    :cond_a
    const-string v2, "mobileCountryCode"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2843
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    .line 2844
    invoke-virtual {p3}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/msc/a/g;->c(Ljava/lang/String;)V

    .line 2845
    invoke-static {p0, v0}, Lcom/msc/c/e;->f(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2865
    :cond_b
    const-string v2, "userCountryCode"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 2867
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    .line 2868
    invoke-virtual {p3}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/msc/a/g;->d(Ljava/lang/String;)V

    .line 2875
    const-string v2, "SERVER_URL"

    const/4 v5, 0x0

    invoke-virtual {p0, v2, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 2876
    const-string v5, "user_country_code"

    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/osp/app/util/i;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2877
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_1

    .line 2880
    :cond_c
    const-string v2, "requireNameCheck"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 2882
    const-string v0, "Y"

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2884
    invoke-virtual {p3}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/msc/a/g;->e(Z)V

    goto/16 :goto_1

    .line 2886
    :cond_d
    const-string v2, "requireEmailValidation"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 2888
    const-string v0, "Y"

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2890
    invoke-virtual {p3}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/a/g;->i()V

    goto/16 :goto_1

    .line 2896
    :cond_e
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 2898
    const-string v2, "EMAIL_VALIDATION_KEY"

    invoke-static {p0}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2899
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_1

    .line 2902
    :cond_f
    const-string v2, "userID"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 2904
    invoke-virtual {p3}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v0

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/msc/a/g;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2905
    :cond_10
    const-string v2, "is3rdParty"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 2907
    const-string v0, "Y"

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2909
    invoke-virtual {p3}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/msc/a/g;->f(Z)V

    goto/16 :goto_1

    .line 2911
    :cond_11
    const-string v2, "requireDisclaimer"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 2913
    const-string v0, "Y"

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2915
    invoke-virtual {p3}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/msc/a/g;->g(Z)V

    goto/16 :goto_1

    .line 2917
    :cond_12
    const-string v2, "loginID"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 2919
    invoke-virtual {p3}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v0

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/msc/a/g;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2920
    :cond_13
    const-string v2, "countryCode"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 2922
    const-string v0, "countryCode"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2923
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->s()V

    goto/16 :goto_1

    .line 2924
    :cond_14
    const-string v2, "birthDate"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 2926
    const-string v0, "birthDate"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2927
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->t()V

    goto/16 :goto_1

    .line 2928
    :cond_15
    const-string v2, "prefixName"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 2930
    const-string v2, "prefixNa_"

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2931
    const/4 v2, 0x0

    invoke-interface {v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v2

    .line 2934
    const/4 v5, 0x0

    invoke-interface {v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v5

    .line 2935
    const-string v6, "selectionElementList"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 2937
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/osp/app/signin/kd;->b(Ljava/lang/String;)V

    .line 2939
    :cond_16
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->u()V

    .line 2940
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2941
    :cond_17
    const-string v2, "givenName"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 2943
    const-string v2, "givenNa_"

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2944
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->w()V

    .line 2945
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2946
    :cond_18
    const-string v2, "familyName"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 2948
    const-string v2, "familyNa_"

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2949
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->y()V

    .line 2950
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2951
    :cond_19
    const-string v2, "localityText"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 2953
    const-string v2, "localityText"

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2954
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->A()V

    .line 2955
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2956
    :cond_1a
    const-string v2, "postalCodeText"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 2958
    const-string v2, "postalCodeText"

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2959
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->K()V

    .line 2960
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2961
    :cond_1b
    const-string v2, "receiveSMSPhoneNumberText"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 2963
    const-string v2, "receiveSMSPhoneNumberText"

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2964
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->M()V

    .line 2965
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2966
    :cond_1c
    const-string v2, "streetText"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 2968
    const-string v2, "streetText"

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2969
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->C()V

    .line 2970
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2971
    :cond_1d
    const-string v2, "extendedText"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 2973
    const-string v2, "extendedText"

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2974
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->E()V

    .line 2975
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2976
    :cond_1e
    const-string v2, "postOfficeBoxNumberText"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 2978
    const-string v2, "postOfficeBoxNumberText"

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2979
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->G()V

    .line 2980
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2981
    :cond_1f
    const-string v2, "regionText"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_20

    .line 2983
    const-string v2, "regionText"

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2984
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->I()V

    .line 2985
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2986
    :cond_20
    const-string v2, "genderTypeCode"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_21

    .line 2988
    const-string v2, "genderTypeCode"

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2989
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->P()V

    .line 2990
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2991
    :cond_21
    const-string v2, "userDisplayName"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 2993
    const-string v2, "userDisplayNa_"

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2994
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->R()V

    .line 2995
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2996
    :cond_22
    const-string v2, "relationshipStatusCode"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2998
    const-string v2, "relationshipStatusCode"

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2999
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->T()V

    .line 3000
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 3008
    :cond_23
    const-string v0, "parsePreProcessFromXML"

    invoke-static {v0, v3}, Lcom/msc/c/h;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 3009
    invoke-virtual {p3}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/msc/a/g;->a(Landroid/content/Context;)V

    .line 3010
    invoke-virtual {p3}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/osp/app/signin/kd;->a(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3013
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    return-void

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto/16 :goto_2

    .line 2794
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;Lcom/msc/a/d;)V
    .locals 4

    .prologue
    .line 3100
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 3102
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3104
    invoke-static {v0, p1}, Lcom/msc/c/h;->a(Lorg/json/JSONObject;Lcom/msc/a/d;)V

    .line 3106
    const-string v1, "status"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3108
    const-string v1, "status"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3109
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ACCESSTOKENTEST parseAuthenticationV02 status : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3110
    invoke-virtual {p1, v1}, Lcom/msc/a/d;->a(Ljava/lang/String;)V

    .line 3111
    const-string v2, "S"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "userId"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3113
    const-string v1, "userId"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3114
    invoke-virtual {p1, v1}, Lcom/msc/a/d;->e(Ljava/lang/String;)V

    .line 3117
    :cond_0
    const-string v1, "login_id"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3119
    const-string v1, "login_id"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3120
    invoke-virtual {p1, v1}, Lcom/msc/a/d;->f(Ljava/lang/String;)V

    .line 3125
    :cond_1
    const-string v1, "userauth_token"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3127
    const-string v1, "userauth_token"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3128
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ACCESSTOKENTEST parseAuthenticationV02 authToken : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3129
    invoke-virtual {p1, v1}, Lcom/msc/a/d;->b(Ljava/lang/String;)V

    .line 3131
    :cond_2
    const-string v1, "access_token"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3133
    const-string v1, "access_token"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3134
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ACCESSTOKENTEST parseAuthenticationV02 accessToken : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3135
    invoke-virtual {p1, v1}, Lcom/msc/a/d;->c(Ljava/lang/String;)V

    .line 3137
    :cond_3
    const-string v1, "refresh_token"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 3139
    const-string v1, "refresh_token"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3140
    invoke-virtual {p1, v1}, Lcom/msc/a/d;->d(Ljava/lang/String;)V

    .line 3142
    :cond_4
    const-string v1, "access_token_expires_in"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 3144
    const-string v1, "access_token_expires_in"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3145
    invoke-virtual {p1, v2, v3}, Lcom/msc/a/d;->a(J)V

    .line 3147
    :cond_5
    const-string v1, "refresh_token_expires_in"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 3149
    const-string v1, "refresh_token_expires_in"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3150
    invoke-virtual {p1, v2, v3}, Lcom/msc/a/d;->b(J)V

    .line 3152
    :cond_6
    const-string v1, "userId"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 3154
    const-string v1, "userId"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3155
    invoke-virtual {p1, v1}, Lcom/msc/a/d;->e(Ljava/lang/String;)V

    .line 3157
    :cond_7
    const-string v1, "state"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 3159
    const-string v1, "state"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3160
    invoke-virtual {p1, v1}, Lcom/msc/a/d;->g(Ljava/lang/String;)V

    .line 3162
    :cond_8
    const-string v1, "error_description"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 3164
    const-string v1, "error_description"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3165
    invoke-virtual {p1, v0}, Lcom/msc/a/d;->h(Ljava/lang/String;)V

    .line 3167
    :cond_9
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 4993
    :try_start_0
    new-instance v2, Ljava/lang/StringBuffer;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " emptyMandatoryField = ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 4994
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 4995
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 4997
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 4998
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 4999
    add-int/lit8 v0, v3, -0x1

    if-ne v0, v1, :cond_0

    .line 5001
    const-string v0, " : empty"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 4995
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 5004
    :cond_0
    const-string v0, " : empty, "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 5010
    :catch_0
    move-exception v0

    .line 5012
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/Exception;)V

    .line 5014
    :goto_2
    return-void

    .line 5007
    :cond_1
    :try_start_1
    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 5008
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRH"

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method private static a(Lorg/json/JSONObject;Lcom/msc/a/d;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 3043
    const-string v0, "isDuplicationID"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 3045
    const-string v0, "isDuplicationID"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "Y"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    .line 3046
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ACCESSTOKENTEST parseAuthenticationV02 isDuplicationID : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3047
    invoke-virtual {p1, v0}, Lcom/msc/a/d;->a(Z)V

    .line 3049
    if-eqz v0, :cond_6

    .line 3051
    const-string v0, "userIds"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 3053
    const-string v0, "userIds"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 3054
    invoke-virtual {p1}, Lcom/msc/a/d;->c()V

    .line 3056
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    move v4, v1

    .line 3057
    :goto_1
    if-ge v4, v6, :cond_6

    .line 3059
    invoke-virtual {v5, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 3060
    const-string v0, ""

    .line 3061
    const-string v2, ""

    .line 3062
    const-string v1, ""

    .line 3063
    const-string v3, ""

    .line 3064
    const-string v8, "userId"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 3066
    const-string v0, "userId"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3068
    :cond_0
    const-string v8, "login_id"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 3070
    const-string v1, "login_id"

    invoke-virtual {v7, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3072
    :cond_1
    const-string v8, "id_type"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 3074
    const-string v2, "id_type"

    invoke-virtual {v7, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3076
    :cond_2
    const-string v8, "status"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 3078
    const-string v3, "status"

    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 3081
    :cond_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 3083
    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/msc/a/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3057
    :cond_4
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    :cond_5
    move v0, v1

    .line 3045
    goto/16 :goto_0

    .line 3089
    :cond_6
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4786
    .line 4787
    if-eqz p1, :cond_4

    if-eqz p2, :cond_4

    if-eqz p3, :cond_4

    .line 4790
    const/4 v0, 0x0

    .line 4791
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/16 v2, 0x40

    invoke-virtual {v1, p3, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v5, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .line 4792
    if-eqz v5, :cond_6

    array-length v1, v5

    if-lez v1, :cond_6

    .line 4794
    array-length v0, v5

    new-array v0, v0, [I

    .line 4797
    array-length v6, v5

    move v1, v3

    move v2, v3

    :goto_0
    if-ge v1, v6, :cond_0

    aget-object v7, v5, v1

    .line 4799
    invoke-virtual {v7}, Landroid/content/pm/Signature;->hashCode()I

    move-result v7

    aput v7, v0, v2

    .line 4800
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " hash:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget v8, v0, v2

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4802
    add-int/lit8 v2, v2, 0x1

    .line 4797
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    move-object v1, v0

    .line 4807
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v3

    :cond_1
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/msc/c/i;

    .line 4812
    invoke-virtual {v0}, Lcom/msc/c/i;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "j5p7ll8g33"

    invoke-virtual {v0}, Lcom/msc/c/i;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 4814
    :cond_2
    invoke-virtual {v0}, Lcom/msc/c/i;->b()Ljava/lang/String;

    move-result-object v0

    .line 4816
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "sign:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4817
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 4819
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 4823
    if-eqz v1, :cond_1

    .line 4825
    array-length v7, v1

    move v0, v3

    :goto_3
    if-ge v0, v7, :cond_1

    aget v8, v1, v0

    .line 4827
    if-ne v8, v6, :cond_3

    move v2, v4

    .line 4830
    goto :goto_2

    .line 4825
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    move v2, v4

    .line 4842
    :cond_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRH"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "checkSignatureValidation - "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v3

    invoke-virtual {v3, p3}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4843
    return v2

    :cond_6
    move-object v1, v0

    goto/16 :goto_1
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)Lcom/msc/a/g;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 981
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SaResponseHandler::parseTermUpdateFromXML"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 983
    new-instance v3, Lcom/msc/a/g;

    invoke-direct {v3}, Lcom/msc/a/g;-><init>()V

    .line 985
    const/4 v2, 0x0

    .line 988
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 989
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 990
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 992
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 993
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 994
    :try_start_1
    new-instance v2, Ljava/io/BufferedReader;

    invoke-direct {v2, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 996
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 998
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1111
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    .line 1113
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_0
    throw v0

    .line 1000
    :cond_1
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 1002
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1004
    new-instance v2, Ljava/io/StringReader;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-interface {v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 1005
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 1007
    :goto_2
    if-eq v0, v6, :cond_13

    .line 1009
    packed-switch v0, :pswitch_data_0

    .line 1106
    :cond_2
    :goto_3
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_2

    .line 1012
    :pswitch_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1013
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "userTelephoneIDDuplicationResultListVO"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1015
    new-instance v0, Lcom/msc/c/f;

    const-string v2, "DUPLICATED_ID"

    const-string v5, "Id duplicated"

    invoke-direct {v0, v2, v5}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Lcom/msc/a/g;->a(Lcom/msc/c/f;)V

    goto :goto_3

    .line 1017
    :cond_3
    const-string v2, "requireTncAccepted"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1019
    const-string v0, "Y"

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1021
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRH"

    const-string v2, "v2prusustn rsp TA : 1"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1022
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lcom/msc/a/g;->a(Z)V

    goto :goto_3

    .line 1025
    :cond_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRH"

    const-string v2, "v2prusustn rsp TA : 0"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 1027
    :cond_5
    const-string v2, "privacyAccepted"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1029
    const-string v0, "Y"

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1031
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRH"

    const-string v2, "v2prusustn rsp PA : 1"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1032
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lcom/msc/a/g;->b(Z)V

    goto :goto_3

    .line 1035
    :cond_6
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRH"

    const-string v2, "v2prusustn rsp PA : 0"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1037
    :cond_7
    const-string v2, "dataCollectionAccepted"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1039
    const-string v0, "Y"

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1041
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRH"

    const-string v2, "v2prusustn rsp DCA : 1"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1042
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lcom/msc/a/g;->c(Z)V

    goto/16 :goto_3

    .line 1045
    :cond_8
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRH"

    const-string v2, "v2prusustn rsp DCA : 0"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1047
    :cond_9
    const-string v2, "onwardTransferAccepted"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1049
    const-string v0, "Y"

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1051
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRH"

    const-string v2, "v2prusustn rsp OTA : 1"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1052
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lcom/msc/a/g;->d(Z)V

    goto/16 :goto_3

    .line 1055
    :cond_a
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRH"

    const-string v2, "v2prusustn rsp OTA : 0"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1057
    :cond_b
    const-string v2, "mobileCountryCode"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1059
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    .line 1060
    invoke-virtual {v3, v0}, Lcom/msc/a/g;->c(Ljava/lang/String;)V

    .line 1061
    invoke-static {p0, v0}, Lcom/msc/c/e;->f(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1062
    :cond_c
    const-string v2, "requireNameCheck"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1064
    const-string v0, "Y"

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1066
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lcom/msc/a/g;->e(Z)V

    goto/16 :goto_3

    .line 1068
    :cond_d
    const-string v2, "requireEmailValidation"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1070
    const-string v0, "Y"

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1072
    invoke-virtual {v3}, Lcom/msc/a/g;->i()V

    goto/16 :goto_3

    .line 1077
    :cond_e
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1079
    const-string v2, "EMAIL_VALIDATION_KEY"

    invoke-static {p0}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1080
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_3

    .line 1083
    :cond_f
    const-string v2, "userID"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1085
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/msc/a/g;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1086
    :cond_10
    const-string v2, "is3rdParty"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 1088
    const-string v0, "Y"

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1090
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lcom/msc/a/g;->f(Z)V

    goto/16 :goto_3

    .line 1092
    :cond_11
    const-string v2, "requireDisclaimer"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 1094
    const-string v0, "Y"

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1096
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lcom/msc/a/g;->g(Z)V

    goto/16 :goto_3

    .line 1098
    :cond_12
    const-string v2, "loginID"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1100
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/msc/a/g;->b(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1108
    :cond_13
    invoke-virtual {v3, p0}, Lcom/msc/a/g;->a(Landroid/content/Context;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1111
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 1116
    return-object v3

    .line 1111
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto/16 :goto_1

    .line 1009
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 206
    const/4 v0, 0x0

    .line 207
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {p0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 209
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 211
    const-string v2, "code"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 213
    const-string v0, "code"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 215
    :cond_0
    const-string v2, "error"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 217
    const-string v0, "error"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 219
    :cond_1
    const-string v2, "state"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 221
    const-string v0, "state"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 223
    :cond_2
    const-string v2, "error_description"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 225
    const-string v0, "error_description"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 228
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SaAuthManager::AppAuthCodefromJSON AppAuthCode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 230
    return-object v0
.end method

.method public static c(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 782
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SaAuthManager::CountryInfofromXML"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 784
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 866
    :goto_0
    return-object v0

    .line 795
    :cond_1
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 796
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 797
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 798
    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 799
    :try_start_1
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v3, ""

    invoke-direct {v0, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 800
    new-instance v3, Ljava/io/BufferedReader;

    invoke-direct {v3, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 802
    :goto_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 804
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 866
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_2

    .line 868
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_2
    throw v0

    .line 806
    :cond_3
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "&"

    const-string v7, "&amp;"

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 807
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    .line 808
    invoke-virtual {v2}, Ljava/io/StringReader;->close()V

    .line 809
    new-instance v3, Ljava/io/StringReader;

    invoke-direct {v3, v5}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 810
    :try_start_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 812
    invoke-interface {v4, v3}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 814
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    .line 815
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 817
    :goto_3
    if-eq v2, v8, :cond_9

    .line 819
    packed-switch v2, :pswitch_data_0

    .line 860
    :cond_4
    :goto_4
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    goto :goto_3

    .line 823
    :pswitch_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 825
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SaAuthManager::CountryInfofromXML name : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 827
    const-string v5, "countryInfo"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 829
    new-instance v1, Lcom/osp/app/countrylist/CountryInfoItem;

    invoke-direct {v1}, Lcom/osp/app/countrylist/CountryInfoItem;-><init>()V

    goto :goto_4

    .line 866
    :catchall_1
    move-exception v0

    move-object v1, v3

    goto :goto_2

    .line 830
    :cond_5
    if-eqz v1, :cond_4

    .line 832
    const-string v5, "countryName"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 834
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    .line 835
    invoke-virtual {v1, v2}, Lcom/osp/app/countrylist/CountryInfoItem;->c(Ljava/lang/String;)V

    goto :goto_4

    .line 836
    :cond_6
    const-string v5, "nativeCountryName"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 838
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    .line 839
    invoke-virtual {v1, v2}, Lcom/osp/app/countrylist/CountryInfoItem;->e(Ljava/lang/String;)V

    goto :goto_4

    .line 840
    :cond_7
    const-string v5, "countryCode"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 842
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    .line 843
    invoke-virtual {v1, v2}, Lcom/osp/app/countrylist/CountryInfoItem;->b(Ljava/lang/String;)V

    goto :goto_4

    .line 844
    :cond_8
    const-string v5, "mobileCountryCode"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 846
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    .line 847
    invoke-virtual {v1, v2}, Lcom/osp/app/countrylist/CountryInfoItem;->d(Ljava/lang/String;)V

    goto :goto_4

    .line 852
    :pswitch_1
    const-string v2, "countryInfo"

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 854
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_4

    .line 866
    :cond_9
    invoke-virtual {v3}, Ljava/io/StringReader;->close()V

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    goto/16 :goto_2

    .line 819
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static d(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v9, 0x1

    .line 881
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SaAuthManager::CountryCallingCodeInfofromXML"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 883
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 884
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 964
    :cond_0
    :goto_0
    return-object v0

    .line 893
    :cond_1
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v1

    .line 894
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 895
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 897
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 898
    :try_start_1
    new-instance v5, Ljava/lang/StringBuffer;

    const-string v2, ""

    invoke-direct {v5, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 899
    new-instance v2, Ljava/io/BufferedReader;

    invoke-direct {v2, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 901
    :goto_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 903
    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 964
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_2

    .line 966
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_2
    throw v0

    .line 905
    :cond_3
    :try_start_2
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "&"

    const-string v8, "&amp;"

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 906
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 907
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 908
    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, v6}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 909
    :try_start_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 911
    invoke-interface {v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 913
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    .line 915
    :goto_3
    if-eq v1, v9, :cond_9

    .line 917
    packed-switch v1, :pswitch_data_0

    .line 958
    :cond_4
    :goto_4
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    goto :goto_3

    .line 921
    :pswitch_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 923
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SaAuthManager::CountryCallingCodeInfofromXML name : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 925
    const-string v5, "countryInfo"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 927
    new-instance v3, Lcom/osp/app/countrylist/CountryInfoItem;

    invoke-direct {v3}, Lcom/osp/app/countrylist/CountryInfoItem;-><init>()V

    goto :goto_4

    .line 964
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    .line 928
    :cond_5
    if-eqz v3, :cond_4

    .line 930
    const-string v5, "countryName"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 932
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v1

    .line 933
    invoke-virtual {v3, v1}, Lcom/osp/app/countrylist/CountryInfoItem;->c(Ljava/lang/String;)V

    goto :goto_4

    .line 934
    :cond_6
    const-string v5, "countryCallingCode"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 936
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v1

    .line 937
    invoke-virtual {v3, v1}, Lcom/osp/app/countrylist/CountryInfoItem;->a(Ljava/lang/String;)V

    goto :goto_4

    .line 938
    :cond_7
    const-string v5, "countryCode"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 940
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v1

    .line 941
    invoke-virtual {v3, v1}, Lcom/osp/app/countrylist/CountryInfoItem;->b(Ljava/lang/String;)V

    goto :goto_4

    .line 942
    :cond_8
    const-string v5, "alpha2countryCode"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 944
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v1

    .line 945
    invoke-virtual {v3, v1}, Lcom/osp/app/countrylist/CountryInfoItem;->f(Ljava/lang/String;)V

    goto :goto_4

    .line 950
    :pswitch_1
    const-string v1, "countryInfo"

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 952
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_4

    .line 964
    :cond_9
    invoke-virtual {v2}, Ljava/io/StringReader;->close()V

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    move-object v1, v3

    goto/16 :goto_2

    .line 917
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static e(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 1127
    .line 1132
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 1133
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 1134
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v5

    .line 1136
    const-string v0, "&"

    const-string v2, "&amp;"

    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1137
    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1139
    :try_start_1
    invoke-interface {v5, v2}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 1140
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    move-object v3, v1

    move v4, v0

    move-object v0, v1

    .line 1141
    :goto_0
    if-eq v4, v7, :cond_2

    .line 1144
    packed-switch v4, :pswitch_data_0

    .line 1159
    :cond_0
    :goto_1
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v4

    goto :goto_0

    .line 1147
    :pswitch_0
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v4

    .line 1148
    const-string v6, "ResponseValue"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1150
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 1151
    :cond_1
    const-string v6, "UserID"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1153
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    goto :goto_1

    .line 1163
    :cond_2
    invoke-virtual {v2}, Ljava/io/StringReader;->close()V

    .line 1169
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "SaAuthManager::VerifyfromXML result : "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1170
    const-string v2, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1175
    :goto_2
    return-object v0

    .line 1163
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_3

    .line 1165
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_3
    throw v0

    :cond_4
    move-object v0, v1

    .line 1175
    goto :goto_2

    .line 1163
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_3

    .line 1144
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static f(Ljava/lang/String;)Lcom/msc/sa/d/f;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 1190
    .line 1194
    new-instance v3, Lcom/msc/sa/d/f;

    invoke-direct {v3}, Lcom/msc/sa/d/f;-><init>()V

    .line 1195
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {p0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1197
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1199
    const-string v0, "isUsable"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1201
    const-string v0, "isUsable"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1203
    :goto_0
    const-string v2, "duplicationCount"

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1205
    const-string v2, "duplicationCount"

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1207
    :goto_1
    const-string v5, "maxCount"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1209
    const-string v1, "maxCount"

    invoke-virtual {v4, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1211
    :cond_0
    if-nez v0, :cond_3

    .line 1213
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SaAuthManager::IsUsableLoginIdFromXML result 0 : fail!"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1222
    :cond_1
    invoke-virtual {v3, v6}, Lcom/msc/sa/d/f;->a(Z)V

    .line 1225
    :goto_2
    if-nez v2, :cond_4

    .line 1227
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SaAuthManager::IsUsableLoginIdFromXML result 1: fail!"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1228
    invoke-virtual {v3, v6}, Lcom/msc/sa/d/f;->a(I)V

    .line 1233
    :goto_3
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1235
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/msc/sa/d/f;->b(I)V

    .line 1238
    :cond_2
    return-object v3

    .line 1217
    :cond_3
    const-string v4, "true"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1219
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lcom/msc/sa/d/f;->a(Z)V

    goto :goto_2

    .line 1231
    :cond_4
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/msc/sa/d/f;->a(I)V

    goto :goto_3

    :cond_5
    move-object v2, v1

    goto :goto_1

    :cond_6
    move-object v0, v1

    goto :goto_0
.end method

.method public static g(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 1461
    .line 1465
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 1466
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 1467
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v3

    .line 1469
    const-string v0, "&"

    const-string v1, "&amp;"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1470
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1471
    :try_start_1
    invoke-interface {v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 1473
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 1475
    :goto_0
    if-eq v0, v5, :cond_1

    .line 1477
    packed-switch v0, :pswitch_data_0

    .line 1489
    :cond_0
    :goto_1
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 1480
    :pswitch_0
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1481
    const-string v4, "boolean"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1483
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    goto :goto_1

    .line 1493
    :cond_1
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 1499
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SaAuthManager::changePasswordfromXMLuserid : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1501
    return-object v2

    .line 1493
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_2

    .line 1495
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_2
    throw v0

    .line 1493
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 1477
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static h(Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x1

    .line 1513
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "parseUserIdFromXML"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1518
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 1519
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 1520
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v3

    .line 1522
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1523
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1525
    :try_start_1
    new-instance v4, Ljava/io/BufferedReader;

    invoke-direct {v4, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 1527
    :goto_0
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 1529
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1559
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    .line 1561
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_0
    throw v0

    .line 1532
    :cond_1
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "&"

    const-string v7, "&amp;"

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1533
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    .line 1535
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1537
    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, v5}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v0}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 1539
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 1541
    :goto_2
    if-eq v0, v8, :cond_3

    .line 1543
    packed-switch v0, :pswitch_data_0

    .line 1555
    :cond_2
    :goto_3
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_2

    .line 1546
    :pswitch_0
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1547
    const-string v4, "string"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1549
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_3

    .line 1559
    :cond_3
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 1564
    return-object v2

    .line 1559
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1

    .line 1543
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static i(Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 1576
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "parseSignUpV02ResultFromXML"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1580
    const/4 v0, 0x0

    .line 1584
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v1

    .line 1585
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 1586
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v5

    .line 1588
    new-instance v3, Ljava/lang/StringBuffer;

    const-string v1, ""

    invoke-direct {v3, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1589
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1591
    :try_start_1
    new-instance v6, Ljava/io/BufferedReader;

    invoke-direct {v6, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 1593
    :goto_0
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 1595
    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1631
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    .line 1633
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_0
    throw v0

    .line 1598
    :cond_1
    :try_start_2
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "&"

    const-string v9, "&amp;"

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1599
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V

    .line 1601
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1603
    new-instance v3, Ljava/io/StringReader;

    invoke-direct {v3, v7}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-interface {v5, v3}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 1605
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    .line 1607
    :goto_2
    if-eq v3, v4, :cond_4

    .line 1609
    packed-switch v3, :pswitch_data_0

    .line 1627
    :cond_2
    :goto_3
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    goto :goto_2

    .line 1612
    :pswitch_0
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1613
    const-string v6, "userSuspendedResultVO"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    move v0, v4

    .line 1615
    goto :goto_3

    .line 1616
    :cond_3
    const-string v6, "userID"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1618
    if-nez v0, :cond_2

    .line 1620
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_3

    .line 1631
    :cond_4
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 1637
    return-object v2

    .line 1631
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1

    .line 1609
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static j(Ljava/lang/String;)Lcom/osp/app/signin/kd;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v9, 0x1

    .line 1880
    new-instance v4, Lcom/osp/app/signin/kd;

    invoke-direct {v4}, Lcom/osp/app/signin/kd;-><init>()V

    .line 1881
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 1882
    invoke-virtual {v0, v9}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 1883
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v5

    .line 1885
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SaAuthManager::infoFieldFromXML"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1892
    :try_start_0
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1893
    :try_start_1
    invoke-interface {v5, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 1894
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    move v3, v0

    move-object v0, v2

    .line 1896
    :goto_0
    if-eq v3, v9, :cond_1c

    .line 1898
    packed-switch v3, :pswitch_data_0

    .line 2047
    :goto_1
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    goto :goto_0

    .line 1901
    :pswitch_0
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1902
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "SaAuthManager::infoFieldFromXML name : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1903
    const-string v6, "prefixName"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1905
    const/4 v0, 0x0

    invoke-interface {v5, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    .line 1906
    const/4 v2, 0x0

    invoke-interface {v5, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    .line 1907
    const-string v6, "mandatory"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "1"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1909
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->v()V

    .line 1911
    :cond_0
    const/4 v6, 0x1

    invoke-interface {v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v6

    .line 1914
    const/4 v7, 0x1

    invoke-interface {v5, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v7

    .line 1915
    const-string v8, "selectionElementList"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1917
    invoke-virtual {v4, v7}, Lcom/osp/app/signin/kd;->b(Ljava/lang/String;)V

    .line 1919
    :cond_1
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->u()V

    .line 1920
    invoke-virtual {v4, v3}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 2045
    :cond_2
    :goto_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "SaAuthManager::infoFieldFromXML mValueTagName : "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2046
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "SaAuthManager::infoFieldFromXML mValueTagText : "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 2056
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_3

    .line 2058
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_3
    throw v0

    .line 1922
    :cond_4
    :try_start_2
    const-string v6, "givenName"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1924
    const/4 v0, 0x0

    invoke-interface {v5, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    .line 1925
    const/4 v2, 0x0

    invoke-interface {v5, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    .line 1926
    const-string v6, "mandatory"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    const-string v6, "1"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1928
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->x()V

    .line 1930
    :cond_5
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->w()V

    .line 1931
    invoke-virtual {v4, v3}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 1933
    :cond_6
    const-string v6, "familyName"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1935
    const/4 v0, 0x0

    invoke-interface {v5, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    .line 1936
    const/4 v2, 0x0

    invoke-interface {v5, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    .line 1937
    const-string v6, "mandatory"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    const-string v6, "1"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1939
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->z()V

    .line 1941
    :cond_7
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->y()V

    .line 1942
    invoke-virtual {v4, v3}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1943
    :cond_8
    const-string v6, "localityText"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1945
    const/4 v0, 0x0

    invoke-interface {v5, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    .line 1946
    const/4 v2, 0x0

    invoke-interface {v5, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    .line 1947
    const-string v6, "mandatory"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    const-string v6, "1"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1949
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->B()V

    .line 1951
    :cond_9
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->A()V

    .line 1952
    invoke-virtual {v4, v3}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1953
    :cond_a
    const-string v6, "postalCodeText"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 1955
    const/4 v0, 0x0

    invoke-interface {v5, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    .line 1956
    const/4 v2, 0x0

    invoke-interface {v5, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    .line 1957
    const-string v6, "mandatory"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    const-string v6, "1"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 1959
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->L()V

    .line 1961
    :cond_b
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->K()V

    .line 1962
    invoke-virtual {v4, v3}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1963
    :cond_c
    const-string v6, "receiveSMSPhoneNumberText"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 1965
    const/4 v0, 0x0

    invoke-interface {v5, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    .line 1966
    const/4 v2, 0x0

    invoke-interface {v5, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    .line 1967
    const-string v6, "mandatory"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    const-string v6, "1"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 1969
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->N()V

    .line 1971
    :cond_d
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->M()V

    .line 1972
    invoke-virtual {v4, v3}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1973
    :cond_e
    const-string v6, "streetText"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 1975
    const/4 v0, 0x0

    invoke-interface {v5, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    .line 1976
    const/4 v2, 0x0

    invoke-interface {v5, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    .line 1977
    const-string v6, "mandatory"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    const-string v6, "1"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 1979
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->D()V

    .line 1981
    :cond_f
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->C()V

    .line 1982
    invoke-virtual {v4, v3}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1983
    :cond_10
    const-string v6, "extendedText"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 1985
    const/4 v0, 0x0

    invoke-interface {v5, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    .line 1986
    const/4 v2, 0x0

    invoke-interface {v5, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    .line 1987
    const-string v6, "mandatory"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    const-string v6, "1"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 1989
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->F()V

    .line 1991
    :cond_11
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->E()V

    .line 1992
    invoke-virtual {v4, v3}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1993
    :cond_12
    const-string v6, "postOfficeBoxNumberText"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 1995
    const/4 v0, 0x0

    invoke-interface {v5, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    .line 1996
    const/4 v2, 0x0

    invoke-interface {v5, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    .line 1997
    const-string v6, "mandatory"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    const-string v6, "1"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 1999
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->H()V

    .line 2001
    :cond_13
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->G()V

    .line 2002
    invoke-virtual {v4, v3}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2003
    :cond_14
    const-string v6, "regionText"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_16

    .line 2005
    const/4 v0, 0x0

    invoke-interface {v5, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    .line 2006
    const/4 v2, 0x0

    invoke-interface {v5, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    .line 2007
    const-string v6, "mandatory"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_15

    const-string v6, "1"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 2009
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->J()V

    .line 2011
    :cond_15
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->I()V

    .line 2012
    invoke-virtual {v4, v3}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2013
    :cond_16
    const-string v6, "genderTypeCode"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_18

    .line 2015
    const/4 v0, 0x0

    invoke-interface {v5, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    .line 2016
    const/4 v2, 0x0

    invoke-interface {v5, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    .line 2017
    const-string v6, "mandatory"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_17

    const-string v6, "1"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 2019
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->Q()V

    .line 2021
    :cond_17
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->P()V

    .line 2022
    invoke-virtual {v4, v3}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2023
    :cond_18
    const-string v6, "userDisplayName"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1a

    .line 2025
    const/4 v0, 0x0

    invoke-interface {v5, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    .line 2026
    const/4 v2, 0x0

    invoke-interface {v5, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    .line 2027
    const-string v6, "mandatory"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_19

    const-string v6, "1"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_19

    .line 2029
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->S()V

    .line 2031
    :cond_19
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->R()V

    .line 2032
    invoke-virtual {v4, v3}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2033
    :cond_1a
    const-string v6, "relationshipStatusCode"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2035
    const/4 v0, 0x0

    invoke-interface {v5, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    .line 2036
    const/4 v2, 0x0

    invoke-interface {v5, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    .line 2037
    const-string v6, "mandatory"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1b

    const-string v6, "1"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1b

    .line 2039
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->U()V

    .line 2041
    :cond_1b
    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->T()V

    .line 2042
    invoke-virtual {v4, v3}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    .line 2056
    :cond_1c
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 2061
    return-object v4

    .line 2056
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto/16 :goto_3

    .line 1898
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static k(Ljava/lang/String;)Lcom/osp/app/signin/SignUpinfo;
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2071
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SaAuthManager::AccountInfofromXML"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2074
    new-instance v5, Lcom/osp/app/signin/SignUpinfo;

    invoke-direct {v5}, Lcom/osp/app/signin/SignUpinfo;-><init>()V

    .line 2075
    const/4 v2, 0x0

    .line 2078
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 2079
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 2080
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v6

    .line 2082
    const-string v0, "&"

    const-string v1, "&amp;"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2083
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2084
    :try_start_1
    invoke-interface {v6, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 2086
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    move v2, v0

    move v0, v4

    .line 2088
    :goto_0
    if-eq v2, v3, :cond_16

    .line 2090
    packed-switch v2, :pswitch_data_0

    .line 2194
    :cond_0
    :goto_1
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    goto :goto_0

    .line 2093
    :pswitch_0
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 2094
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "account info from server is "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2095
    const-string v7, "loginID"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2097
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->b(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2203
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    .line 2205
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_1
    throw v0

    .line 2098
    :cond_2
    :try_start_2
    const-string v7, "loginIDTypeCode"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2100
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->c(Ljava/lang/String;)V

    goto :goto_1

    .line 2101
    :cond_3
    const-string v7, "birthDate"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2103
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->f(Ljava/lang/String;)V

    goto :goto_1

    .line 2104
    :cond_4
    const-string v7, "countryCode"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2106
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->g(Ljava/lang/String;)V

    goto :goto_1

    .line 2107
    :cond_5
    const-string v7, "prefixName"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 2109
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->i(Ljava/lang/String;)V

    goto :goto_1

    .line 2110
    :cond_6
    const-string v7, "givenName"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 2112
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->j(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2113
    :cond_7
    const-string v7, "familyName"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 2115
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->k(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2116
    :cond_8
    const-string v7, "receiveSMSPhoneNumberText"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 2118
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->n(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2121
    :cond_9
    const-string v7, "userContactAddressVO"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 2123
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 2124
    :cond_a
    const-string v7, "localityText"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 2126
    if-ne v0, v3, :cond_0

    .line 2128
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->l(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2130
    :cond_b
    const-string v7, "postalCodeText"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 2132
    if-ne v0, v3, :cond_0

    .line 2134
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->m(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2136
    :cond_c
    const-string v7, "streetText"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 2138
    if-ne v0, v3, :cond_0

    .line 2140
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->o(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2142
    :cond_d
    const-string v7, "extendedAddress"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 2144
    if-ne v0, v3, :cond_0

    .line 2146
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->p(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2148
    :cond_e
    const-string v7, "postOfficeBoxNumberText"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 2150
    if-ne v0, v3, :cond_0

    .line 2152
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->q(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2154
    :cond_f
    const-string v7, "regionText"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 2156
    if-ne v0, v3, :cond_0

    .line 2158
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->r(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2160
    :cond_10
    const-string v7, "addressTypeSequence"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_11

    .line 2162
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    .line 2163
    if-ne v0, v3, :cond_0

    .line 2165
    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2169
    :cond_11
    const-string v7, "genderTypeCode"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_12

    .line 2171
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->s(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2172
    :cond_12
    const-string v7, "userDisplayName"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_13

    .line 2174
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->t(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2175
    :cond_13
    const-string v7, "relationshipStatusCode"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_14

    .line 2177
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->u(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2182
    :cond_14
    const-string v7, "emailReceiveYNFlag"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2185
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    .line 2186
    const-string v7, "Y"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    move v2, v3

    .line 2193
    :goto_3
    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->b(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    :cond_15
    move v2, v4

    .line 2191
    goto :goto_3

    .line 2203
    :cond_16
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 2208
    return-object v5

    .line 2203
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto/16 :goto_2

    .line 2090
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static l(Ljava/lang/String;)Lcom/osp/app/signin/hq;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 2218
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SaAuthManager::StubUpdateCheckfromXML"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2220
    new-instance v3, Lcom/osp/app/signin/hq;

    invoke-direct {v3}, Lcom/osp/app/signin/hq;-><init>()V

    .line 2221
    const/4 v2, 0x0

    .line 2224
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 2225
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 2226
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 2228
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2229
    :try_start_1
    invoke-interface {v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 2231
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 2233
    :goto_0
    if-eq v0, v6, :cond_6

    .line 2235
    packed-switch v0, :pswitch_data_0

    .line 2262
    :cond_0
    :goto_1
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 2238
    :pswitch_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 2239
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "StubUpdateCheck from server is "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2241
    const-string v2, "appId"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2243
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/osp/app/signin/hq;->k(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2266
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    .line 2268
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_1
    throw v0

    .line 2244
    :cond_2
    :try_start_2
    const-string v2, "resultCode"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2246
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/osp/app/signin/hq;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 2247
    :cond_3
    const-string v2, "resultMsg"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2249
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/osp/app/signin/hq;->e(Ljava/lang/String;)V

    goto :goto_1

    .line 2250
    :cond_4
    const-string v2, "versionCode"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2252
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/osp/app/signin/hq;->l(Ljava/lang/String;)V

    goto :goto_1

    .line 2253
    :cond_5
    const-string v2, "versionName"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2255
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/osp/app/signin/hq;->f(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 2266
    :cond_6
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 2271
    return-object v3

    .line 2266
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    .line 2235
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static m(Ljava/lang/String;)Lcom/osp/app/signin/hq;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 2281
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SaAuthManager::StubDownloadfromXML"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2283
    new-instance v3, Lcom/osp/app/signin/hq;

    invoke-direct {v3}, Lcom/osp/app/signin/hq;-><init>()V

    .line 2284
    const/4 v2, 0x0

    .line 2287
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 2288
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 2289
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 2291
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2292
    :try_start_1
    invoke-interface {v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 2294
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 2296
    :goto_0
    if-eq v0, v6, :cond_c

    .line 2298
    packed-switch v0, :pswitch_data_0

    .line 2342
    :cond_0
    :goto_1
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 2301
    :pswitch_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 2302
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "StubDownload from server is "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2303
    const-string v2, "appId"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2305
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/osp/app/signin/hq;->k(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2346
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    .line 2348
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_1
    throw v0

    .line 2306
    :cond_2
    :try_start_2
    const-string v2, "resultCode"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2308
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/osp/app/signin/hq;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 2309
    :cond_3
    const-string v2, "resultMsg"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2311
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/osp/app/signin/hq;->e(Ljava/lang/String;)V

    goto :goto_1

    .line 2312
    :cond_4
    const-string v2, "downloadURI"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2314
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/osp/app/signin/hq;->g(Ljava/lang/String;)V

    goto :goto_1

    .line 2315
    :cond_5
    const-string v2, "contentSize"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2317
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/osp/app/signin/hq;->h(Ljava/lang/String;)V

    goto :goto_1

    .line 2318
    :cond_6
    const-string v2, "deltaDownloadURI"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2320
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/osp/app/signin/hq;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2321
    :cond_7
    const-string v2, "deltaContentSize"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2323
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/osp/app/signin/hq;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2324
    :cond_8
    const-string v2, "versionCode"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 2326
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/osp/app/signin/hq;->l(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2327
    :cond_9
    const-string v2, "versionName"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2329
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/osp/app/signin/hq;->f(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2330
    :cond_a
    const-string v2, "productId"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2332
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/osp/app/signin/hq;->i(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2333
    :cond_b
    const-string v2, "productName"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2335
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/osp/app/signin/hq;->j(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 2346
    :cond_c
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 2351
    return-object v3

    .line 2346
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto/16 :goto_2

    .line 2298
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static n(Ljava/lang/String;)Lcom/osp/app/signin/SignUpinfo;
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2362
    .line 2363
    new-instance v5, Lcom/osp/app/signin/SignUpinfo;

    invoke-direct {v5}, Lcom/osp/app/signin/SignUpinfo;-><init>()V

    .line 2364
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 2365
    invoke-virtual {v0, v3}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 2366
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v6

    .line 2367
    const/4 v2, 0x0

    .line 2368
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SaAuthManager::AccountInfofromXML_V01"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2372
    :try_start_0
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2373
    :try_start_1
    invoke-interface {v6, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 2374
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    move v2, v0

    move v0, v4

    .line 2376
    :goto_0
    if-eq v2, v3, :cond_16

    .line 2378
    packed-switch v2, :pswitch_data_0

    .line 2472
    :cond_0
    :goto_1
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    goto :goto_0

    .line 2381
    :pswitch_0
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 2382
    const-string v7, "loginID"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2384
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->b(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2481
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    .line 2483
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_1
    throw v0

    .line 2385
    :cond_2
    :try_start_2
    const-string v7, "loginIDTypeCode"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2387
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->c(Ljava/lang/String;)V

    goto :goto_1

    .line 2388
    :cond_3
    const-string v7, "birthDate"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2390
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->f(Ljava/lang/String;)V

    goto :goto_1

    .line 2391
    :cond_4
    const-string v7, "countryCode"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2393
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->g(Ljava/lang/String;)V

    goto :goto_1

    .line 2394
    :cond_5
    const-string v7, "prefixName"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 2396
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->i(Ljava/lang/String;)V

    goto :goto_1

    .line 2397
    :cond_6
    const-string v7, "givenName"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 2399
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->j(Ljava/lang/String;)V

    goto :goto_1

    .line 2400
    :cond_7
    const-string v7, "familyName"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 2402
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->k(Ljava/lang/String;)V

    goto :goto_1

    .line 2403
    :cond_8
    const-string v7, "receiveSMSPhoneNumberText"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 2405
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->n(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2408
    :cond_9
    const-string v7, "userAddress"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 2410
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 2411
    :cond_a
    const-string v7, "localityText"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 2413
    if-ne v0, v3, :cond_0

    .line 2415
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->l(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2417
    :cond_b
    const-string v7, "postalCodeText"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 2419
    if-ne v0, v3, :cond_0

    .line 2421
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->m(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2423
    :cond_c
    const-string v7, "streetText"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 2425
    if-ne v0, v3, :cond_0

    .line 2427
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->o(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2429
    :cond_d
    const-string v7, "extendedText"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 2431
    if-ne v0, v3, :cond_0

    .line 2433
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->p(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2435
    :cond_e
    const-string v7, "postOfficeBoxNumberText"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 2437
    if-ne v0, v3, :cond_0

    .line 2439
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->q(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2441
    :cond_f
    const-string v7, "regionText"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 2443
    if-ne v0, v3, :cond_0

    .line 2445
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->r(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2447
    :cond_10
    const-string v7, "addressID"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_11

    .line 2449
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->w(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2452
    :cond_11
    const-string v7, "genderTypeCode"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_12

    .line 2454
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->s(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2455
    :cond_12
    const-string v7, "userDisplayName"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_13

    .line 2457
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->t(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2458
    :cond_13
    const-string v7, "relationshipStatusCode"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_14

    .line 2460
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->u(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2461
    :cond_14
    const-string v7, "emailReceiveYNFlag"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2464
    const-string v2, "Y"

    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    move v2, v3

    .line 2471
    :goto_3
    invoke-virtual {v5, v2}, Lcom/osp/app/signin/SignUpinfo;->b(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    :cond_15
    move v2, v4

    .line 2469
    goto :goto_3

    .line 2481
    :cond_16
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 2486
    return-object v5

    .line 2481
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto/16 :goto_2

    .line 2378
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static o(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 2549
    .line 2554
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v1

    .line 2555
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 2556
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 2558
    const-string v1, "&"

    const-string v3, "&amp;"

    invoke-virtual {p0, v1, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2559
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, v3}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2560
    :try_start_1
    invoke-interface {v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 2562
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    .line 2564
    :goto_0
    if-eq v3, v0, :cond_1

    .line 2566
    packed-switch v3, :pswitch_data_0

    .line 2578
    :cond_0
    :goto_1
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    goto :goto_0

    .line 2569
    :pswitch_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 2570
    const-string v5, "boolean"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2572
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    goto :goto_1

    .line 2582
    :cond_1
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 2587
    const-string v1, "true"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2592
    :goto_2
    return v0

    .line 2582
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    if-eqz v1, :cond_2

    .line 2584
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_2
    throw v0

    .line 2592
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 2582
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 2566
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static p(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 2604
    .line 2609
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v1

    .line 2610
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 2611
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 2613
    const-string v1, "&"

    const-string v3, "&amp;"

    invoke-virtual {p0, v1, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2614
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, v3}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2615
    :try_start_1
    invoke-interface {v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 2617
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    .line 2619
    :goto_0
    if-eq v3, v0, :cond_1

    .line 2621
    packed-switch v3, :pswitch_data_0

    .line 2633
    :cond_0
    :goto_1
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    goto :goto_0

    .line 2624
    :pswitch_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 2625
    const-string v5, "boolean"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2627
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    goto :goto_1

    .line 2637
    :cond_1
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 2642
    const-string v1, "true"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2647
    :goto_2
    return v0

    .line 2637
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    if-eqz v1, :cond_2

    .line 2639
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_2
    throw v0

    .line 2647
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 2637
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 2621
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static q(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2721
    const/4 v0, 0x0

    .line 2722
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {p0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2724
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2726
    const-string v2, "userId"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2728
    const-string v0, "userId"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2735
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RequestSet::user_idfromJSON user_id : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2737
    return-object v0
.end method

.method public static r(Ljava/lang/String;)Lcom/msc/a/b;
    .locals 6

    .prologue
    .line 3177
    .line 3179
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v3

    .line 3180
    new-instance v4, Lcom/msc/a/b;

    invoke-direct {v4}, Lcom/msc/a/b;-><init>()V

    .line 3181
    const/4 v2, 0x0

    .line 3184
    :try_start_0
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 3185
    :try_start_1
    invoke-interface {v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 3199
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 3200
    :goto_0
    const/4 v2, 0x1

    if-eq v0, v2, :cond_8

    .line 3202
    packed-switch v0, :pswitch_data_0

    .line 3240
    :cond_0
    :goto_1
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 3205
    :pswitch_0
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "accessToken"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3207
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    .line 3208
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "ACCESSTOKENTEST parseAuthenticationV01 accessToken : "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3209
    invoke-virtual {v4, v0}, Lcom/msc/a/b;->e(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 3245
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    .line 3247
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_1
    throw v0

    .line 3210
    :cond_2
    :try_start_2
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "accessTokenSecret"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3212
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    .line 3213
    invoke-virtual {v4, v0}, Lcom/msc/a/b;->f(Ljava/lang/String;)V

    goto :goto_1

    .line 3214
    :cond_3
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "deviceID"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3216
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    .line 3217
    invoke-virtual {v4, v0}, Lcom/msc/a/b;->g(Ljava/lang/String;)V

    goto :goto_1

    .line 3218
    :cond_4
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "authToken"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 3220
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    .line 3221
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "ACCESSTOKENTEST parseAuthenticationV01 authToken : "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3222
    invoke-virtual {v4, v0}, Lcom/msc/a/b;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 3223
    :cond_5
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "authTokenSecret"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 3225
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    .line 3226
    invoke-virtual {v4, v0}, Lcom/msc/a/b;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 3227
    :cond_6
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "userID"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 3229
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    .line 3230
    invoke-virtual {v4, v0}, Lcom/msc/a/b;->c(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 3231
    :cond_7
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "birthDate"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3233
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    .line 3234
    invoke-virtual {v4, v0}, Lcom/msc/a/b;->d(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 3245
    :cond_8
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    return-object v4

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto/16 :goto_2

    .line 3202
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static s(Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 3543
    .line 3549
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v1

    .line 3550
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 3551
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v5

    .line 3553
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3554
    :try_start_1
    invoke-interface {v5, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 3556
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    move v4, v3

    move-object v3, v2

    .line 3558
    :goto_0
    if-eq v4, v0, :cond_2

    .line 3560
    packed-switch v4, :pswitch_data_0

    .line 3575
    :cond_0
    :goto_1
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v4

    goto :goto_0

    .line 3563
    :pswitch_0
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v4

    .line 3564
    const-string v6, "authorizeCode"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 3566
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 3567
    :cond_1
    const-string v6, "authorizeDesc"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 3569
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    goto :goto_1

    .line 3579
    :cond_2
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 3584
    const-string v1, "LIC_2101"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "SUCCESS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 3589
    :goto_2
    return v0

    .line 3579
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    if-eqz v1, :cond_3

    .line 3581
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_3
    throw v0

    .line 3589
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 3579
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 3560
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static t(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 3601
    .line 3606
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 3607
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 3608
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v5

    .line 3611
    const-string v0, "&"

    const-string v2, "&amp;"

    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3612
    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3614
    :try_start_1
    invoke-interface {v5, v2}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 3615
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    move-object v3, v1

    move v4, v0

    move-object v0, v1

    .line 3616
    :goto_0
    if-eq v4, v7, :cond_2

    .line 3619
    packed-switch v4, :pswitch_data_0

    .line 3634
    :cond_0
    :goto_1
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v4

    goto :goto_0

    .line 3622
    :pswitch_0
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v4

    .line 3623
    const-string v6, "ResponseValue"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 3625
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 3626
    :cond_1
    const-string v6, "UserID"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 3628
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    goto :goto_1

    .line 3638
    :cond_2
    invoke-virtual {v2}, Ljava/io/StringReader;->close()V

    .line 3644
    const-string v2, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3649
    :goto_2
    return-object v0

    .line 3638
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_3

    .line 3640
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_3
    throw v0

    :cond_4
    move-object v0, v1

    .line 3649
    goto :goto_2

    .line 3638
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_3

    .line 3619
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static u(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 3711
    .line 3715
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 3716
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 3717
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v3

    .line 3720
    const-string v0, "&"

    const-string v1, "&amp;"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3721
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3723
    :try_start_1
    invoke-interface {v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 3724
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 3725
    :goto_0
    if-eq v0, v5, :cond_1

    .line 3728
    packed-switch v0, :pswitch_data_0

    .line 3740
    :cond_0
    :goto_1
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 3731
    :pswitch_0
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 3732
    const-string v4, "boolean"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3734
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    goto :goto_1

    .line 3744
    :cond_1
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 3750
    return-object v2

    .line 3744
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_2

    .line 3746
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_2
    throw v0

    .line 3744
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 3728
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static v(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 3994
    const/4 v0, 0x0

    .line 3995
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 3996
    const-string v2, "rcode"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3998
    const-string v0, "rcode"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4000
    :cond_0
    const-string v2, "access_token"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4002
    const-string v0, "access_token"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4005
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SaAuthManager::appAccessTokenfromJSON AppAuthCode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4007
    return-object v0
.end method

.method public static w(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 4067
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SaResponseHandler::parseMyCountryZoneFromXML"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4074
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 4075
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 4076
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v3

    .line 4079
    const-string v0, "&"

    const-string v1, "&amp;"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4080
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 4082
    :try_start_1
    invoke-interface {v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 4083
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 4084
    :goto_0
    if-eq v0, v5, :cond_2

    .line 4087
    packed-switch v0, :pswitch_data_0

    .line 4108
    :cond_0
    :goto_1
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 4090
    :pswitch_0
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 4091
    const-string v4, "countryCode"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4093
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    .line 4098
    invoke-static {}, Lcom/osp/app/bigdatalog/i;->a()Lcom/osp/app/bigdatalog/i;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/osp/app/bigdatalog/i;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 4112
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    .line 4114
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_1
    throw v0

    .line 4112
    :cond_2
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 4118
    return-object v2

    .line 4112
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    .line 4087
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static x(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x1

    .line 4129
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SaResponseHandler::parseSpecialTermsListFromXML"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4131
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 4209
    :goto_0
    return-object v0

    .line 4145
    :cond_1
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 4147
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 4148
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 4149
    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 4150
    :try_start_1
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v3, ""

    invoke-direct {v0, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 4151
    new-instance v3, Ljava/io/BufferedReader;

    invoke-direct {v3, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 4153
    :goto_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 4155
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 4209
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_2

    .line 4211
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_2
    throw v0

    .line 4157
    :cond_3
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "&"

    const-string v7, "&amp;"

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 4158
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    .line 4159
    invoke-virtual {v2}, Ljava/io/StringReader;->close()V

    .line 4160
    new-instance v3, Ljava/io/StringReader;

    invoke-direct {v3, v5}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4161
    :try_start_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4163
    invoke-interface {v4, v3}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 4165
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    .line 4166
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4168
    :goto_3
    if-eq v2, v8, :cond_7

    .line 4170
    packed-switch v2, :pswitch_data_0

    .line 4203
    :cond_4
    :goto_4
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    goto :goto_3

    .line 4174
    :pswitch_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 4176
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SaAuthManager::CountryCallingCodeInfofromXML name : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4178
    const-string v5, "specialTerms"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 4180
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    goto :goto_4

    .line 4181
    :cond_5
    if-eqz v1, :cond_4

    .line 4183
    const-string v5, "fileName"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 4185
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    .line 4186
    const/4 v5, 0x0

    aput-object v2, v1, v5

    goto :goto_4

    .line 4209
    :catchall_1
    move-exception v0

    move-object v1, v3

    goto :goto_2

    .line 4187
    :cond_6
    const-string v5, "displayName"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 4189
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    .line 4190
    const/4 v5, 0x1

    aput-object v2, v1, v5

    goto :goto_4

    .line 4195
    :pswitch_1
    const-string v2, "specialTerms"

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 4197
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_4

    .line 4209
    :cond_7
    invoke-virtual {v3}, Ljava/io/StringReader;->close()V

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    goto/16 :goto_2

    .line 4170
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static y(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 4225
    .line 4227
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v1

    .line 4231
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 4232
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 4233
    const-string v1, "PRO_MEM_1100"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4271
    :goto_0
    return-object v0

    .line 4238
    :cond_0
    const-string v1, "&"

    const-string v2, "&amp;"

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 4239
    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, v1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4240
    :try_start_1
    invoke-interface {v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 4241
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    move v3, v1

    move-object v1, v0

    .line 4242
    :goto_1
    if-eq v3, v6, :cond_3

    .line 4245
    packed-switch v3, :pswitch_data_0

    .line 4261
    :cond_1
    :goto_2
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    goto :goto_1

    .line 4248
    :pswitch_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    if-nez v3, :cond_2

    .line 4250
    invoke-virtual {v2}, Ljava/io/StringReader;->close()V

    goto :goto_0

    .line 4252
    :cond_2
    :try_start_2
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 4253
    const-string v5, "url"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 4255
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v1

    goto :goto_2

    .line 4265
    :cond_3
    invoke-virtual {v2}, Ljava/io/StringReader;->close()V

    .line 4270
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "RequestSet::user_idfromJSON userUrl : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    move-object v0, v1

    .line 4271
    goto :goto_0

    .line 4265
    :catchall_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_3
    if-eqz v1, :cond_4

    .line 4267
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_4
    throw v0

    .line 4265
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_3

    .line 4245
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static z(Ljava/lang/String;)Lcom/osp/app/signin/pz;
    .locals 3

    .prologue
    .line 4320
    new-instance v0, Lcom/osp/app/signin/pz;

    invoke-direct {v0}, Lcom/osp/app/signin/pz;-><init>()V

    .line 4321
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 4323
    const-string v2, "access_token"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4325
    const-string v2, "access_token"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/pz;->a(Ljava/lang/String;)V

    .line 4327
    :cond_0
    const-string v2, "uid"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4329
    const-string v2, "uid"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/pz;->b(Ljava/lang/String;)V

    .line 4331
    :cond_1
    const-string v2, "email"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 4333
    const-string v2, "email"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/pz;->c(Ljava/lang/String;)V

    .line 4335
    :cond_2
    const-string v2, "real_name"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 4337
    const-string v2, "real_name"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/pz;->d(Ljava/lang/String;)V

    .line 4339
    :cond_3
    const-string v2, "birthday"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 4341
    const-string v2, "birthday"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/pz;->e(Ljava/lang/String;)V

    .line 4344
    :cond_4
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/osp/app/signin/kd;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 1329
    const/4 v2, 0x0

    .line 1330
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1335
    :try_start_0
    new-instance v0, Lcom/osp/app/signin/kd;

    invoke-direct {v0}, Lcom/osp/app/signin/kd;-><init>()V

    iput-object v0, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    .line 1336
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 1337
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 1338
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 1340
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p3}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1341
    :try_start_1
    invoke-interface {v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 1342
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 1344
    :goto_0
    if-eq v0, v7, :cond_11

    .line 1346
    packed-switch v0, :pswitch_data_0

    .line 1436
    :cond_0
    :goto_1
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 1349
    :pswitch_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1350
    const-string v2, "countryCode"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1352
    iget-object v0, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->s()V

    .line 1353
    const-string v0, "countryCode"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1442
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    .line 1444
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 1447
    :cond_1
    iget-object v1, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v1, p1, p2}, Lcom/osp/app/signin/kd;->a(Landroid/content/Context;Ljava/lang/String;)V

    throw v0

    .line 1354
    :cond_2
    :try_start_2
    const-string v2, "birthDate"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1356
    iget-object v0, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->t()V

    .line 1357
    const-string v0, "birthDate"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1358
    :cond_3
    const-string v2, "prefixName"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1360
    const/4 v2, 0x0

    invoke-interface {v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v2

    .line 1363
    const/4 v5, 0x0

    invoke-interface {v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v5

    .line 1364
    const-string v6, "selectionElementList"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1366
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2, v5}, Lcom/osp/app/signin/kd;->b(Ljava/lang/String;)V

    .line 1368
    :cond_4
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->u()V

    .line 1369
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 1370
    const-string v0, "prefixNa_"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1371
    :cond_5
    const-string v2, "givenName"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1373
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->w()V

    .line 1374
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 1375
    const-string v0, "givenNa_"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1376
    :cond_6
    const-string v2, "familyName"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1378
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->y()V

    .line 1379
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 1380
    const-string v0, "familyNa_"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1381
    :cond_7
    const-string v2, "localityText"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1383
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->A()V

    .line 1384
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 1385
    const-string v0, "localityText"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1386
    :cond_8
    const-string v2, "postalCodeText"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1388
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->K()V

    .line 1389
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 1390
    const-string v0, "postalCodeText"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1391
    :cond_9
    const-string v2, "receiveSMSPhoneNumberText"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1393
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->M()V

    .line 1394
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 1395
    const-string v0, "receiveSMSPhoneNumberText"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1396
    :cond_a
    const-string v2, "streetText"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1398
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->C()V

    .line 1399
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 1400
    const-string v0, "streetText"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1401
    :cond_b
    const-string v2, "extendedText"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1403
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->E()V

    .line 1404
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 1405
    const-string v0, "extendedText"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1406
    :cond_c
    const-string v2, "postOfficeBoxNumberText"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1408
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->G()V

    .line 1409
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 1410
    const-string v0, "postOfficeBoxNumberText"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1411
    :cond_d
    const-string v2, "regionText"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 1413
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->I()V

    .line 1414
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 1415
    const-string v0, "regionText"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1416
    :cond_e
    const-string v2, "genderTypeCode"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1418
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->P()V

    .line 1419
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 1420
    const-string v0, "genderTypeCode"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1421
    :cond_f
    const-string v2, "userDisplayName"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1423
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->R()V

    .line 1424
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 1425
    const-string v0, "userDisplayNa_"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1426
    :cond_10
    const-string v2, "relationshipStatusCode"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1428
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->T()V

    .line 1429
    iget-object v2, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 1430
    const-string v0, "relationshipStatusCode"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1438
    :cond_11
    const-string v0, "parseGetEmptyMandatoryFromXml"

    invoke-static {v0, v3}, Lcom/msc/c/h;->a(Ljava/lang/String;Ljava/util/ArrayList;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1442
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 1447
    iget-object v0, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    invoke-virtual {v0, p1, p2}, Lcom/osp/app/signin/kd;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 1450
    iget-object v0, p0, Lcom/msc/c/h;->a:Lcom/osp/app/signin/kd;

    return-object v0

    .line 1442
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto/16 :goto_2

    .line 1346
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
