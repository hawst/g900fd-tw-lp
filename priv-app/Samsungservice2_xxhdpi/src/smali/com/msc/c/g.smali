.class public final Lcom/msc/c/g;
.super Ljava/lang/Object;
.source "HttpRequestSet.java"


# static fields
.field private static a:Lcom/msc/c/g;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 295
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 296
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/msc/a/a;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 4179
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " deviceRegistration go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4191
    :try_start_0
    invoke-static {p0}, Lcom/msc/c/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 4193
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/msc/a/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4195
    invoke-static {}, Lcom/osp/app/util/k;->a()Lcom/osp/app/util/k;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/app/util/k;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 4196
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 4198
    invoke-virtual {p1, v0}, Lcom/msc/a/a;->a(Ljava/lang/String;)V

    .line 4202
    :cond_0
    new-instance v2, Lcom/msc/c/q;

    invoke-direct {v2}, Lcom/msc/c/q;-><init>()V

    new-instance v3, Ljava/io/StringWriter;

    invoke-direct {v3}, Ljava/io/StringWriter;-><init>()V

    invoke-static {}, Lcom/osp/common/util/d;->a()Lcom/osp/common/util/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/common/util/d;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    invoke-static {p0}, Lcom/osp/device/b;->q(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    invoke-static {}, Lcom/osp/device/b;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v3}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    const-string v0, "UTF-8"

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "authRequest"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "loginID"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/msc/a/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "loginID"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "password"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/msc/a/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->c(Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "password"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/msc/a/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, ""

    const-string v8, "idType"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "plainID"

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "idType"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "countryCallingCode"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/msc/a/a;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "countryCallingCode"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    const-string v0, ""

    const-string v8, "duid"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/msc/a/a;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "duid"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "authToken"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/msc/a/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "authToken"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "appAccountCreate"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "appID"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/msc/a/a;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "appID"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "appSignature"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/common/c/b;->a()Lcom/osp/common/c/b;

    move-result-object v0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/msc/a/a;->r()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/msc/a/a;->s()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/osp/common/c/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "appSignature"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/msc/a/a;->q()Lcom/osp/social/member/b;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v0, ""

    const-string v8, "appUserProfile"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "appUserStatusCode"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/msc/a/a;->q()Lcom/osp/social/member/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/social/member/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "appUserStatusCode"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/msc/a/a;->q()Lcom/osp/social/member/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/social/member/b;->b()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/social/member/c;

    const-string v9, ""

    const-string v10, "attribute"

    invoke-virtual {v2, v9, v10}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, ""

    const-string v10, "attributeName"

    invoke-virtual {v2, v9, v10}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/osp/social/member/c;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    const-string v9, ""

    const-string v10, "attributeName"

    invoke-virtual {v2, v9, v10}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, ""

    const-string v10, "attributeValueText"

    invoke-virtual {v2, v9, v10}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/osp/social/member/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    const-string v0, ""

    const-string v9, "attributeValueText"

    invoke-virtual {v2, v0, v9}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v9, "attribute"

    invoke-virtual {v2, v0, v9}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 4218
    :catch_0
    move-exception v0

    move-object v3, v0

    move-object v1, v6

    move-object v2, v6

    move-object v0, v6

    :goto_2
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    move-object v3, v2

    move-object v2, v1

    move-object v1, v6

    .line 4222
    :goto_3
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v4

    invoke-virtual {v4}, Lcom/osp/app/util/ah;->c()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 4224
    const-string v4, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v5

    invoke-virtual {v5}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4226
    :cond_2
    const-string v4, "Content-Type"

    const-string v5, "text/xml"

    invoke-virtual {v2, v4, v5}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4227
    const-string v4, "Authorization"

    invoke-virtual {v2, v4, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4229
    const-string v1, "x-osp-appId"

    invoke-virtual {v2, v1, v0}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4230
    const-string v0, "x-osp-userId"

    invoke-virtual {v2, v0, p2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4231
    const-string v0, "x-osp-version"

    const-string v1, "v1"

    invoke-virtual {v2, v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4236
    invoke-static {p0, v2}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 4238
    invoke-virtual {v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 4247
    invoke-virtual {v2}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0

    .line 4202
    :cond_3
    :try_start_1
    const-string v0, ""

    const-string v8, "idType"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "emailID"

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "idType"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    const-string v0, ""

    const-string v8, "appUserProfile"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    const-string v0, ""

    const-string v8, "appAccountCreate"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "userDeviceCreate"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "deviceTypeCode"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/msc/a/a;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "deviceTypeCode"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "deviceModelID"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/msc/a/a;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "deviceModelID"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "deviceUniqueID"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/msc/a/a;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "deviceUniqueID"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "devicePhysicalAddressText"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/msc/a/a;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "devicePhysicalAddressText"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v8, "deviceNetworkAddressText"

    invoke-virtual {v2, v0, v8}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    const-string v0, ""

    const-string v5, "deviceNetworkAddressText"

    invoke-virtual {v2, v0, v5}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v5, "deviceSerialNumberText"

    invoke-virtual {v2, v0, v5}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    const-string v0, ""

    const-string v5, "deviceSerialNumberText"

    invoke-virtual {v2, v0, v5}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v5, "phoneNumberText"

    invoke-virtual {v2, v0, v5}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/msc/a/a;->i()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_7

    const-string v0, ""

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    :goto_4
    const-string v0, ""

    const-string v5, "phoneNumberText"

    invoke-virtual {v2, v0, v5}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v5, "mobileCountryCode"

    invoke-virtual {v2, v0, v5}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/msc/a/a;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    const-string v0, ""

    const-string v5, "mobileCountryCode"

    invoke-virtual {v2, v0, v5}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v5, "mobileNetworkCode"

    invoke-virtual {v2, v0, v5}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/msc/a/a;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    const-string v0, ""

    const-string v5, "mobileNetworkCode"

    invoke-virtual {v2, v0, v5}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v5, "customerCode"

    invoke-virtual {v2, v0, v5}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/msc/a/a;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    const-string v0, ""

    const-string v5, "customerCode"

    invoke-virtual {v2, v0, v5}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v5, "deviceName"

    invoke-virtual {v2, v0, v5}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/msc/a/a;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    const-string v0, ""

    const-string v5, "deviceName"

    invoke-virtual {v2, v0, v5}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v5, "softwareVersion"

    invoke-virtual {v2, v0, v5}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/msc/a/a;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    const-string v0, ""

    const-string v5, "softwareVersion"

    invoke-virtual {v2, v0, v5}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v4, :cond_6

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, ""

    const-string v5, "deviceMultiUserID"

    invoke-virtual {v2, v0, v5}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    const-string v0, ""

    const-string v4, "deviceMultiUserID"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    const-string v0, ""

    const-string v4, "serviceRequired"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/msc/a/a;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    const-string v0, ""

    const-string v4, "serviceRequired"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v4, "userDeviceCreate"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v4, "authRequest"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/msc/c/q;->a()V

    invoke-virtual {v3}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v7

    .line 4203
    :try_start_2
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 4204
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 4205
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v2

    .line 4207
    :try_start_3
    const-string v0, "j5p7ll8g33"
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 4215
    :try_start_4
    new-instance v3, Lcom/osp/common/util/e;

    const-string v4, "5763D0052DC1462E13751F753384E9A9"

    invoke-direct {v3, p0, v0, v4}, Lcom/osp/common/util/e;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 4217
    sget-object v4, Lcom/osp/http/impl/f;->a:Lcom/osp/http/impl/f;

    sget-object v5, Lcom/osp/common/util/g;->a:Lcom/osp/common/util/g;

    invoke-virtual {v3, v4, v1, v7, v5}, Lcom/osp/common/util/e;->a(Lcom/osp/http/impl/f;Ljava/lang/String;Ljava/lang/String;Lcom/osp/common/util/g;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    move-result-object v1

    move-object v3, v7

    .line 4221
    goto/16 :goto_3

    .line 4202
    :cond_7
    :try_start_5
    invoke-virtual {p1}, Lcom/msc/a/a;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_4

    .line 4218
    :catch_1
    move-exception v0

    move-object v3, v0

    move-object v1, v6

    move-object v2, v7

    move-object v0, v6

    goto/16 :goto_2

    :catch_2
    move-exception v0

    move-object v3, v0

    move-object v1, v2

    move-object v0, v6

    move-object v2, v7

    goto/16 :goto_2

    :catch_3
    move-exception v1

    move-object v3, v1

    move-object v1, v2

    move-object v2, v7

    goto/16 :goto_2
.end method

.method public static a(Landroid/content/Context;Lcom/msc/a/c;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 4259
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " authWithTncMandatory go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4260
    const-string v0, "auth.samsungosp.com"

    const-string v1, "/auth/oauth2/authWithTncMandatory"

    invoke-static {p0, v0, v1}, Lcom/msc/c/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 4273
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 4274
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 4275
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v3

    .line 4277
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4279
    const-string v0, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4281
    :cond_0
    const-string v0, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v3, v0, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4287
    invoke-static {p0, v3}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 4289
    invoke-virtual {p1}, Lcom/msc/a/c;->j()Ljava/lang/String;

    move-result-object v4

    .line 4290
    invoke-virtual {p1}, Lcom/msc/a/c;->D()Ljava/lang/String;

    move-result-object v2

    .line 4293
    const-string v5, "tnc_mandatory"

    invoke-virtual {p1}, Lcom/msc/a/c;->q()Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "Y"

    :goto_0
    invoke-virtual {v3, v5, v0}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4294
    const-string v0, "login_id"

    invoke-virtual {p1}, Lcom/msc/a/c;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4295
    const-string v0, "appId"

    invoke-virtual {p1}, Lcom/msc/a/c;->v()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4296
    const-string v5, "tnc_accepted"

    invoke-virtual {p1}, Lcom/msc/a/c;->k()Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "Y"

    :goto_1
    invoke-virtual {v3, v5, v0}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4297
    const-string v5, "privacy_accepted"

    invoke-virtual {p1}, Lcom/msc/a/c;->l()Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "Y"

    :goto_2
    invoke-virtual {v3, v5, v0}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4298
    const-string v5, "data_collection_accepted"

    invoke-virtual {p1}, Lcom/msc/a/c;->m()Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "Y"

    :goto_3
    invoke-virtual {v3, v5, v0}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4302
    invoke-static {p0}, Lcom/osp/app/util/r;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4304
    const-string v5, "onward_transfer_accepted"

    invoke-virtual {p1}, Lcom/msc/a/c;->n()Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "Y"

    :goto_4
    invoke-virtual {v3, v5, v0}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4306
    :cond_1
    const-string v5, "check_name_check"

    invoke-virtual {p1}, Lcom/msc/a/c;->r()Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "Y"

    :goto_5
    invoke-virtual {v3, v5, v0}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4307
    const-string v5, "check_email_validation"

    invoke-virtual {p1}, Lcom/msc/a/c;->s()Z

    move-result v0

    if-eqz v0, :cond_d

    const-string v0, "Y"

    :goto_6
    invoke-virtual {v3, v5, v0}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4308
    const-string v5, "check_country_code"

    invoke-virtual {p1}, Lcom/msc/a/c;->o()Z

    move-result v0

    if-eqz v0, :cond_e

    const-string v0, "Y"

    :goto_7
    invoke-virtual {v3, v5, v0}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4309
    invoke-virtual {p1}, Lcom/msc/a/c;->x()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 4311
    const-string v0, "mandatory_service_id"

    invoke-virtual {p1}, Lcom/msc/a/c;->x()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4313
    :cond_2
    const-string v0, "lang_code"

    invoke-virtual {p1}, Lcom/msc/a/c;->y()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4314
    const-string v0, "mobile_country_code"

    invoke-virtual {p1}, Lcom/msc/a/c;->C()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4316
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4318
    invoke-virtual {p1}, Lcom/msc/a/c;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 4320
    const-string v0, "login_id_type"

    const-string v5, "plain_id"

    invoke-virtual {v3, v0, v5}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4321
    const-string v0, "check_telephone_number_validation"

    const-string v5, "Y"

    invoke-virtual {v3, v0, v5}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4322
    invoke-static {}, Lcom/osp/app/util/k;->a()Lcom/osp/app/util/k;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/app/util/k;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 4323
    if-eqz v0, :cond_3

    .line 4325
    const-string v5, "countryCallingCode"

    invoke-virtual {v3, v5, v0}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4333
    :cond_3
    :goto_8
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_13

    .line 4335
    :cond_4
    const-string v0, "j5p7ll8g33"

    invoke-virtual {p1}, Lcom/msc/a/c;->v()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 4337
    const-string v0, "com.osp.app.signin"

    .line 4343
    :goto_9
    const-string v2, "package"

    invoke-virtual {v3, v2, v0}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4345
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p1}, Lcom/msc/a/c;->t()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 4347
    const-string v0, "tnc_password"

    invoke-virtual {p1}, Lcom/msc/a/c;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4351
    :cond_5
    invoke-virtual {p1}, Lcom/msc/a/c;->u()Z

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_6

    .line 4353
    const-string v0, "tnc_check_duplication_id"

    const-string v2, "Y"

    invoke-virtual {v3, v0, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4354
    const-string v0, "tnc_user_id"

    invoke-virtual {p1}, Lcom/msc/a/c;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4358
    :cond_6
    const-string v2, "authenticate"

    invoke-virtual {p1}, Lcom/msc/a/c;->p()Z

    move-result v0

    if-eqz v0, :cond_11

    const-string v0, "Y"

    :goto_a
    invoke-virtual {v3, v2, v0}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4359
    invoke-virtual {p1}, Lcom/msc/a/c;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 4364
    invoke-static {p0, v1}, Lcom/osp/app/util/ad;->f(Landroid/content/Context;Ljava/lang/String;)V

    .line 4366
    const-string v0, "username"

    invoke-virtual {p1}, Lcom/msc/a/c;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4367
    const-string v0, "password"

    invoke-virtual {v3, v0, v4}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4368
    const-string v0, "signin_client_id"

    const-string v1, "j5p7ll8g33"

    invoke-virtual {v3, v0, v1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4369
    const-string v0, "signin_client_secret"

    const-string v1, "5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v3, v0, v1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4375
    :goto_b
    const-string v0, "client_id"

    invoke-virtual {p1}, Lcom/msc/a/c;->v()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4376
    const-string v0, "client_secret"

    invoke-virtual {p1}, Lcom/msc/a/c;->w()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4377
    const-string v0, "physical_address_text"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/msc/a/c;->z()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/osp/common/util/d;->a()Lcom/osp/common/util/d;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/osp/common/util/d;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4378
    const-string v0, "service_type"

    invoke-virtual {p1}, Lcom/msc/a/c;->A()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4382
    invoke-virtual {v3}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0

    .line 4293
    :cond_7
    const-string v0, "N"

    goto/16 :goto_0

    .line 4296
    :cond_8
    const-string v0, "N"

    goto/16 :goto_1

    .line 4297
    :cond_9
    const-string v0, "N"

    goto/16 :goto_2

    .line 4298
    :cond_a
    const-string v0, "N"

    goto/16 :goto_3

    .line 4304
    :cond_b
    const-string v0, "N"

    goto/16 :goto_4

    .line 4306
    :cond_c
    const-string v0, "N"

    goto/16 :goto_5

    .line 4307
    :cond_d
    const-string v0, "N"

    goto/16 :goto_6

    .line 4308
    :cond_e
    const-string v0, "N"

    goto/16 :goto_7

    .line 4329
    :cond_f
    const-string v0, "login_id_type"

    const-string v5, "email_id"

    invoke-virtual {v3, v0, v5}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    .line 4340
    :cond_10
    const-string v0, ""

    goto/16 :goto_9

    .line 4358
    :cond_11
    const-string v0, "N"

    goto/16 :goto_a

    .line 4372
    :cond_12
    const-string v0, "userauth_token"

    invoke-virtual {p1}, Lcom/msc/a/c;->B()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_b

    :cond_13
    move-object v0, v2

    goto/16 :goto_9
.end method

.method public static a(Landroid/content/Context;Lcom/msc/a/f;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 1237
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SaAuthManager::ReqNewTerms_CheckAgree_V02"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1239
    invoke-static {p0}, Lcom/msc/c/p;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1252
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 1253
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v1

    .line 1258
    const-string v0, ""

    .line 1259
    if-eqz p1, :cond_1

    .line 1261
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/msc/a/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1263
    invoke-static {}, Lcom/osp/app/util/k;->a()Lcom/osp/app/util/k;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/osp/app/util/k;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 1264
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1266
    invoke-virtual {p1, v2}, Lcom/msc/a/f;->i(Ljava/lang/String;)V

    .line 1273
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/msc/a/f;->f()Ljava/lang/String;
    :try_end_0
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1280
    :cond_1
    :goto_0
    const-string v2, "j5p7ll8g33:5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    .line 1285
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->c()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1287
    const-string v3, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v4

    invoke-virtual {v4}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1289
    :cond_2
    const-string v3, "x-osp-appId"

    const-string v4, "j5p7ll8g33"

    invoke-virtual {v1, v3, v4}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1290
    const-string v3, "Content-Type"

    const-string v4, "text/xml"

    invoke-virtual {v1, v3, v4}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1291
    const-string v3, "Authorization"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Basic "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1296
    invoke-static {p0, v1}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 1298
    invoke-virtual {v1, v0}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 1302
    invoke-virtual {v1}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0

    .line 1274
    :catch_0
    move-exception v2

    invoke-virtual {v2}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/msc/a/h;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 3926
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " PreProcess go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3927
    invoke-static {p0}, Lcom/msc/c/p;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 3940
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 3941
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v1

    .line 3944
    const-string v0, ""

    .line 3945
    if-eqz p1, :cond_1

    .line 3947
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/msc/a/h;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3949
    invoke-static {}, Lcom/osp/app/util/k;->a()Lcom/osp/app/util/k;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/osp/app/util/k;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 3950
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 3952
    invoke-virtual {p1, v2}, Lcom/msc/a/h;->g(Ljava/lang/String;)V

    .line 3958
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/msc/a/h;->d()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 3965
    :cond_1
    :goto_0
    const-string v2, "j5p7ll8g33:5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    .line 3971
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->c()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 3973
    const-string v3, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v4

    invoke-virtual {v4}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3976
    :cond_2
    const-string v3, "x-osp-appId"

    const-string v4, "j5p7ll8g33"

    invoke-virtual {v1, v3, v4}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3977
    const-string v3, "Content-Type"

    const-string v4, "text/xml"

    invoke-virtual {v1, v3, v4}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3978
    const-string v3, "Authorization"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Basic "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3983
    invoke-static {p0, v1}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 3985
    invoke-virtual {v1, v0}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 3989
    invoke-virtual {v1}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0

    .line 3959
    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 988
    invoke-static {p0}, Lcom/msc/c/p;->w(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 991
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 992
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 1001
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 1005
    const-string v1, "j5p7ll8g33:5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    .line 1008
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1010
    const-string v2, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1012
    :cond_0
    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Basic "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1013
    const-string v1, "x-osp-appId"

    const-string v2, "j5p7ll8g33"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1067
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Landroid/content/Context;Lcom/osp/app/signin/SignUpinfo;ZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 11

    .prologue
    .line 2051
    invoke-static {p0}, Lcom/msc/c/p;->n(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 2071
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 2072
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object/from16 v5, p10

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v10

    .line 2087
    const-string v0, "j5p7ll8g33:5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 2091
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2093
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2095
    :cond_0
    const-string v1, "x-osp-appId"

    const-string v2, "j5p7ll8g33"

    invoke-virtual {v10, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2096
    const-string v1, "Content-Type"

    const-string v2, "text/xml"

    invoke-virtual {v10, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2097
    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Basic "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v1, v0}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2103
    invoke-static {p0, v10}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 2105
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "info is "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2107
    const-string v1, ""

    .line 2110
    :try_start_0
    invoke-static {p0}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/a;->d()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    move-object v0, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    .line 2116
    invoke-static/range {v0 .. v9}, Lcom/msc/c/g;->a(Lcom/osp/app/signin/SignUpinfo;Ljava/lang/String;ZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 2121
    invoke-virtual {v10}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0

    .line 2111
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 329
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, "prepareSignOut()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RequestSet::RequestSignOut userAuthToken : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 332
    const-string v0, "auth.samsungosp.com"

    const-string v1, "/auth/oauth2/deauthenticate"

    invoke-static {p0, v0, v1}, Lcom/msc/c/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 347
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "RequestSet::RequestSignOut RequestUrl : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 349
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 351
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 352
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 354
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 356
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    :cond_0
    const-string v1, "x-osp-appId"

    const-string v2, "j5p7ll8g33"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    const-string v1, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    const-string v1, "userauth_token"

    invoke-virtual {v0, v1, p1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 390
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/osp/app/signin/SignUpinfo;Lcom/msc/b/h;)J
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 3163
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " SaveAccountInfoV01 go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3178
    invoke-static {p0}, Lcom/msc/c/l;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 3180
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 3181
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 3182
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v2

    .line 3189
    :try_start_0
    new-instance v0, Lcom/osp/common/util/e;

    const-string v3, "14eev3f64b"

    const-string v4, "109E2830E09DB340924B8ABE0D6290C3"

    invoke-direct {v0, p0, v3, v4}, Lcom/osp/common/util/e;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3196
    :goto_0
    :try_start_1
    sget-object v3, Lcom/osp/http/impl/f;->c:Lcom/osp/http/impl/f;

    invoke-static {p1, p2}, Lcom/msc/c/g;->b(Ljava/lang/String;Lcom/osp/app/signin/SignUpinfo;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v1, v4}, Lcom/osp/common/util/e;->a(Lcom/osp/http/impl/f;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 3201
    :goto_1
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3203
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3205
    :cond_0
    const-string v1, "Content-Type"

    const-string v3, "text/xml"

    invoke-virtual {v2, v1, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3210
    const-string v1, "User-Agent"

    const-string v3, "SAMSUNG-Android"

    invoke-virtual {v2, v1, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3211
    const-string v1, "Accept"

    const-string v3, "*, */*"

    invoke-virtual {v2, v1, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3212
    const-string v1, "Accept-Encoding"

    const-string v3, "identity"

    invoke-virtual {v2, v1, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3213
    const-string v1, "Connection"

    const-string v3, "keep-alive"

    invoke-virtual {v2, v1, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3214
    const-string v1, "x-osp-version"

    const-string v3, "v1"

    invoke-virtual {v2, v1, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3219
    invoke-static {p0, v2}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 3221
    invoke-static {p1, p2}, Lcom/msc/c/g;->b(Ljava/lang/String;Lcom/osp/app/signin/SignUpinfo;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 3222
    const-string v1, "Authorization"

    invoke-virtual {v2, v1, v0}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3229
    invoke-virtual {v2}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0

    .line 3190
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v6

    goto :goto_0

    .line 3197
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v6

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lcom/msc/b/h;)J
    .locals 15

    .prologue
    .line 8801
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "SRS"

    const-string v3, " prepareCreateUserSubDevice go"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8802
    invoke-static {p0}, Lcom/msc/c/p;->u(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 8804
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v2

    .line 8805
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v4

    invoke-virtual {v4}, Lcom/osp/app/util/ah;->g()Z

    move-result v6

    .line 8806
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v5

    move-object/from16 v7, p4

    invoke-virtual/range {v2 .. v7}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v11

    .line 8808
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 8810
    const-string v2, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8813
    :cond_0
    const/4 v2, 0x0

    .line 8816
    :try_start_0
    invoke-static {p0}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v3

    .line 8817
    if-eqz v3, :cond_1

    .line 8819
    invoke-virtual {v3}, Lcom/osp/device/a;->c()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :cond_1
    move-object v3, v2

    .line 8826
    :goto_0
    const-string v2, "accessToken"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 8828
    const-string v4, "subDeviceVersion"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 8829
    const-string v5, "subDeviceCustomerSalesCode"

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 8830
    const-string v6, "subDeviceModelId"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 8831
    const-string v7, "subDeviceUniqueId"

    move-object/from16 v0, p3

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 8832
    const-string v8, "subDeviceManagerVersion"

    move-object/from16 v0, p3

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 8833
    const-string v9, "subDeviceMobileCountryCode"

    move-object/from16 v0, p3

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 8834
    const-string v10, "subDeviceMobileNetworkCode"

    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 8836
    const-string v12, "Content-Type"

    const-string v13, "text/xml"

    invoke-virtual {v11, v12, v13}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8837
    const-string v12, "Authorization"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "Bearer "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v12, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8838
    const-string v2, "x-osp-appId"

    move-object/from16 v0, p1

    invoke-virtual {v11, v2, v0}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8839
    const-string v2, "x-osp-userId"

    move-object/from16 v0, p2

    invoke-virtual {v11, v2, v0}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8844
    invoke-static {p0, v11}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    move-object/from16 v2, p2

    .line 8846
    invoke-static/range {v2 .. v10}, Lcom/msc/c/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 8849
    invoke-virtual {v11}, Lcom/msc/b/d;->a()J

    move-result-wide v2

    return-wide v2

    .line 8821
    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    move-object v3, v2

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/a/j;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 8275
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " prepareChangeSecurityInfo"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8276
    invoke-static {p0}, Lcom/msc/c/p;->s(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 8278
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 8279
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 8280
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 8282
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8284
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8286
    :cond_0
    const-string v1, "Content-Type"

    const-string v2, "text/xml"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8287
    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bearer "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8292
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 8294
    invoke-static {p2, p3}, Lcom/msc/c/g;->a(Ljava/lang/String;Lcom/msc/a/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 8296
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 1661
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " deviceUnregistration go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1662
    invoke-static {p0, p1, p2}, Lcom/msc/c/p;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1670
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 1671
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 1672
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 1679
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 1683
    const-string v1, "j5p7ll8g33:5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    .line 1686
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1688
    const-string v2, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1690
    :cond_0
    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Basic "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1691
    const-string v1, "x-osp-userId"

    invoke-virtual {v0, v1, p1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1732
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/osp/app/signin/SignUpinfo;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 2862
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " SaveAccountInfo go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2863
    invoke-static {p0}, Lcom/msc/c/p;->l(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 2876
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 2877
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 2878
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 2880
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2882
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2884
    :cond_0
    const-string v1, "Content-Type"

    const-string v2, "text/xml"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2885
    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bearer "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2890
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 2892
    invoke-static {p2, p3}, Lcom/msc/c/g;->a(Ljava/lang/String;Lcom/osp/app/signin/SignUpinfo;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 2899
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/osp/app/signin/cn;Lcom/msc/b/h;)J
    .locals 7

    .prologue
    .line 403
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, "prepareUserAuthentication()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    const-string v0, "auth.samsungosp.com"

    const-string v1, "/auth/oauth2/authenticate"

    invoke-static {p0, v0, v1}, Lcom/msc/c/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 405
    const/4 v0, 0x0

    .line 411
    :try_start_0
    invoke-static {p0}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v2

    .line 412
    if-eqz v2, :cond_0

    .line 414
    invoke-virtual {v2}, Lcom/osp/device/a;->c()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    move-object v6, v0

    .line 446
    :goto_0
    invoke-static {p0, v1}, Lcom/osp/app/util/ad;->f(Landroid/content/Context;Ljava/lang/String;)V

    .line 448
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 450
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 451
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 453
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 455
    const-string v2, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    :cond_1
    const-string v2, "Content-Type"

    const-string v3, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 465
    const-string v2, "grant_type"

    const-string v3, "password"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    const-string v2, "client_id"

    const-string v3, "j5p7ll8g33"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    const-string v2, "client_secret"

    const-string v3, "5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    const-string v2, "service_type"

    const-string v3, "M"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    const-string v2, "username"

    invoke-virtual {v0, v2, p1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    const-string v2, "password"

    invoke-virtual {v0, v2, p2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    const-string v2, "physical_address_text"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/osp/common/util/d;->a()Lcom/osp/common/util/d;

    move-result-object v4

    invoke-virtual {v4, p0}, Lcom/osp/common/util/d;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 475
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 477
    const-string v2, "login_id_type"

    const-string v3, "plain_id"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    const-string v2, "check_telephone_number_validation"

    const-string v3, "Y"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    invoke-static {}, Lcom/osp/app/util/k;->a()Lcom/osp/app/util/k;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/osp/app/util/k;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 480
    if-eqz v2, :cond_2

    .line 482
    const-string v3, "countryCallingCode"

    invoke-virtual {v0, v3, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    :cond_2
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 486
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Lcom/osp/app/signin/cn;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 488
    const-string v2, "check_duplication_id"

    const-string v3, "Y"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    :cond_3
    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RequestSet::RequestUserAuthentication LoginID : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 503
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RequestSet::RequestUserAuthentication password : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 504
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RequestSet::RequestUserAuthentication RequestUrl : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 524
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0

    .line 416
    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    move-object v6, v0

    goto/16 :goto_0

    .line 493
    :cond_4
    const-string v2, "login_id_type"

    const-string v3, "email_id"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 805
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    move-object v6, v4

    move-object v7, p4

    invoke-static/range {v0 .. v7}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 7

    .prologue
    .line 820
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, "prepareAccessToken()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 821
    const/4 v0, 0x0

    .line 828
    :try_start_0
    invoke-static {p0}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v1

    .line 829
    if-eqz v1, :cond_0

    .line 831
    invoke-virtual {v1}, Lcom/osp/device/a;->c()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    move-object v6, v0

    .line 859
    :goto_0
    const-string v0, "auth.samsungosp.com"

    const-string v1, "/auth/oauth2/token"

    invoke-static {p0, v0, v1}, Lcom/msc/c/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 861
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    if-eqz p4, :cond_1

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_2

    .line 863
    :cond_1
    const-string p3, "j5p7ll8g33"

    .line 864
    const-string p4, "5763D0052DC1462E13751F753384E9A9"

    .line 867
    :cond_2
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 868
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 870
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 872
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 874
    :cond_3
    const-string v1, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 879
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 881
    const-string v1, "grant_type"

    invoke-virtual {v0, v1, p1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 885
    const-string v1, "client_id"

    invoke-virtual {v0, v1, p3}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 886
    const-string v1, "client_secret"

    invoke-virtual {v0, v1, p4}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 887
    const-string v1, "code"

    invoke-virtual {v0, v1, p2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 890
    const-string v1, "physical_address_text"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/osp/common/util/d;->a()Lcom/osp/common/util/d;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/osp/common/util/d;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 915
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0

    .line 833
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    move-object v6, v0

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/msc/b/h;)J
    .locals 7

    .prologue
    .line 3719
    const-string v0, "account.samsung.com"

    const-string v1, "/membership/api/getMarketingPopup.do"

    invoke-static {p0, v0, v1}, Lcom/msc/c/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3721
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 3722
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 3723
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p7

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v6

    .line 3726
    const-string v5, ""

    .line 3730
    :try_start_0
    invoke-static {p0}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v0

    .line 3731
    invoke-virtual {v0}, Lcom/osp/device/a;->d()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 3738
    :goto_0
    if-lez p6, :cond_0

    .line 3740
    invoke-virtual {v6, p6}, Lcom/msc/b/d;->a(I)V

    .line 3743
    :cond_0
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3745
    const-string v0, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3747
    :cond_1
    const-string v0, "Content-Type"

    const-string v1, "text/xml"

    invoke-virtual {v6, v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3752
    invoke-static {p0, v6}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    .line 3754
    invoke-static/range {v0 .. v5}, Lcom/msc/c/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 3756
    invoke-virtual {v6}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0

    .line 3733
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 5226
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " ChangePassword go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5227
    invoke-static {p0}, Lcom/msc/c/p;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 5240
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 5241
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 5242
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 5244
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5246
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5248
    :cond_0
    const-string v1, "Content-Type"

    const-string v2, "text/xml"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5251
    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bearer "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5256
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 5258
    invoke-static {p0, p2, p3, p4, p5}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 5264
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/osp/app/signin/cn;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 4006
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " Authentication go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4007
    const-string v0, "auth.samsungosp.com"

    const-string v1, "/auth/oauth2/requestAuthentication"

    invoke-static {p0, v0, v1}, Lcom/msc/c/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 4020
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 4021
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 4022
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p7

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v3

    .line 4025
    const/4 v0, 0x0

    .line 4030
    :try_start_0
    invoke-static {p0}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v2

    .line 4031
    if-eqz v2, :cond_0

    .line 4033
    invoke-virtual {v2}, Lcom/osp/device/a;->c()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 4039
    :cond_0
    :goto_0
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4041
    const-string v2, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v4

    invoke-virtual {v4}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4044
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 4046
    :cond_2
    const-string p1, "j5p7ll8g33"

    .line 4047
    const-string p2, "5763D0052DC1462E13751F753384E9A9"

    .line 4054
    :cond_3
    invoke-static {p0, v3}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 4056
    const-string v2, "Content-Type"

    const-string v4, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v3, v2, v4}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4057
    const-string v2, "client_id"

    invoke-virtual {v3, v2, p1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4058
    const-string v2, "client_secret"

    invoke-virtual {v3, v2, p2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4059
    if-eqz p5, :cond_9

    invoke-virtual {p5}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_9

    .line 4061
    const-string v1, "userauth_token"

    invoke-virtual {v3, v1, p5}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4074
    :goto_1
    const-string v1, "scope"

    const-string v2, ""

    invoke-virtual {v3, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4076
    const-string v1, "physical_address_text"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/osp/common/util/d;->a()Lcom/osp/common/util/d;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/osp/common/util/d;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4077
    const-string v0, "service_type"

    const-string v1, "M"

    invoke-virtual {v3, v0, v1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4078
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 4081
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4083
    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p3

    .line 4085
    :cond_4
    invoke-static {p3}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 4087
    const-string v0, "login_id_type"

    const-string v1, "plain_id"

    invoke-virtual {v3, v0, v1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4088
    invoke-static {}, Lcom/osp/app/util/k;->a()Lcom/osp/app/util/k;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/app/util/k;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 4089
    if-eqz v0, :cond_5

    .line 4091
    const-string v1, "countryCallingCode"

    invoke-virtual {v3, v1, v0}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4093
    :cond_5
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 4095
    if-eqz p6, :cond_8

    invoke-virtual {p6}, Lcom/osp/app/signin/cn;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 4097
    const-string v0, "check_duplication_id"

    const-string v1, "Y"

    invoke-virtual {v3, v0, v1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4098
    invoke-virtual {p6}, Lcom/osp/app/signin/cn;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 4100
    const-string v0, "birth_day"

    invoke-virtual {p6}, Lcom/osp/app/signin/cn;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4102
    :cond_6
    invoke-virtual {p6}, Lcom/osp/app/signin/cn;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 4104
    const-string v0, "first_name"

    invoke-virtual {p6}, Lcom/osp/app/signin/cn;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4106
    :cond_7
    invoke-virtual {p6}, Lcom/osp/app/signin/cn;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 4108
    const-string v0, "last_name"

    invoke-virtual {p6}, Lcom/osp/app/signin/cn;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4120
    :cond_8
    :goto_2
    invoke-virtual {v3}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0

    .line 4035
    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 4067
    :cond_9
    invoke-static {p0, v1}, Lcom/osp/app/util/ad;->f(Landroid/content/Context;Ljava/lang/String;)V

    .line 4069
    const-string v1, "username"

    invoke-virtual {v3, v1, p3}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4070
    const-string v1, "password"

    invoke-virtual {v3, v1, p4}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4071
    const-string v1, "signin_client_id"

    const-string v2, "j5p7ll8g33"

    invoke-virtual {v3, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4072
    const-string v1, "signin_client_secret"

    const-string v2, "5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v3, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 4114
    :cond_a
    const-string v0, "login_id_type"

    const-string v1, "email_id"

    invoke-virtual {v3, v0, v1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 7

    .prologue
    .line 647
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, "prepareAuthCode()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    const-string v0, "auth.samsungosp.com"

    const-string v1, "/auth/oauth2/authorize"

    invoke-static {p0, v0, v1}, Lcom/msc/c/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 650
    const/4 v0, 0x0

    .line 655
    :try_start_0
    invoke-static {p0}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v2

    .line 656
    if-eqz v2, :cond_0

    .line 658
    invoke-virtual {v2}, Lcom/osp/device/a;->c()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    move-object v6, v0

    .line 688
    :goto_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 690
    const-string p1, "j5p7ll8g33"

    .line 693
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "SaAuthManager::RequestUserAuthorization userauth_token : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 694
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "SaAuthManager::RequestUserAuthorization client_id : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 695
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "SaAuthManager::RequestUserAuthorization account_mode : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 696
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "SaAuthManager::RequestUserAuthorization RequestUrl : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 698
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 699
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p7

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 701
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 703
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 709
    :cond_2
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 711
    if-eqz p3, :cond_5

    const-string v1, "REQUEST_ACCESSTOKEN"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 713
    const-string v1, "response_type"

    const-string v2, "token"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 716
    if-eqz p4, :cond_3

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 718
    const-string v1, "package_name"

    invoke-virtual {v0, v1, p4}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    :cond_3
    :goto_1
    const-string v1, "userauth_token"

    invoke-virtual {v0, v1, p2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 726
    const-string v1, "client_id"

    invoke-virtual {v0, v1, p1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 728
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 730
    const-string v1, "physical_address_text"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/osp/common/util/d;->a()Lcom/osp/common/util/d;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/osp/common/util/d;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    :goto_2
    if-eqz p3, :cond_4

    const-string v1, "REQUEST_ACCESSTOKEN"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 754
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 756
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SRS"

    const-string v2, "get redirect_uri from User"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    const-string v1, "redirect_uri"

    invoke-virtual {v0, v1, p5}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 792
    :cond_4
    :goto_3
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0

    .line 660
    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    move-object v6, v0

    goto/16 :goto_0

    .line 722
    :cond_5
    const-string v1, "response_type"

    const-string v2, "code"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 733
    :cond_6
    const-string v1, "physical_address_text"

    invoke-virtual {v0, v1, p6}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 760
    :cond_7
    invoke-static {p0}, Lcom/osp/app/util/r;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 762
    const-string v1, "redirect_uri"

    const-string v2, "https://cn.sc-auth.samsungosp.com/auth/oauth2/register"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 765
    :cond_8
    const-string v1, "redirect_uri"

    const-string v2, "https://sc-auth.samsungosp.com/auth/oauth2/register"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 6837
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, "prepareNameValidationCheckCHN"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6838
    invoke-static {p0}, Lcom/msc/c/p;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 6840
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 6841
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 6842
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p8

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 6845
    const-string v1, "j5p7ll8g33:5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    .line 6848
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 6850
    const-string v2, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6853
    :cond_0
    const-string v2, "x-osp-appId"

    const-string v3, "j5p7ll8g33"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6854
    const-string v2, "x-osp-userId"

    invoke-virtual {v0, v2, p1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6855
    const-string v2, "Content-Type"

    const-string v3, "text/xml"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6856
    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Basic "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6861
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 6863
    invoke-static/range {p1 .. p7}, Lcom/msc/c/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 6865
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 6675
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, "prepareNameValidationCheck"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6676
    invoke-static {p0}, Lcom/msc/c/p;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 6716
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 6717
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 6718
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p9

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 6721
    const-string v1, "j5p7ll8g33:5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    .line 6724
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 6726
    const-string v2, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6729
    :cond_0
    const-string v2, "x-osp-appId"

    const-string v3, "j5p7ll8g33"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6730
    const-string v2, "Content-Type"

    const-string v3, "text/xml"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6731
    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Basic "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6736
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 6738
    invoke-static/range {p1 .. p8}, Lcom/msc/c/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 6740
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;ZLcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 2560
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " GetSignUpInfoField go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2561
    const-string v0, "account.samsung.com"

    const-string v1, "/membership/api/getUserCreateField.do"

    invoke-static {p0, v0, v1}, Lcom/msc/c/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2563
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 2564
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 2565
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 2568
    if-eqz p1, :cond_0

    const-string v1, ""

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2575
    :cond_0
    const-string p1, "j5p7ll8g33"

    .line 2578
    :cond_1
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2580
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2582
    :cond_2
    const-string v1, "Content-Type"

    const-string v2, "text/xml"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2587
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 2589
    invoke-static {p0, p1, p2}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 2596
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static a()Lcom/msc/c/g;
    .locals 1

    .prologue
    .line 304
    sget-object v0, Lcom/msc/c/g;->a:Lcom/msc/c/g;

    if-eqz v0, :cond_0

    .line 306
    sget-object v0, Lcom/msc/c/g;->a:Lcom/msc/c/g;

    .line 311
    :goto_0
    return-object v0

    .line 309
    :cond_0
    new-instance v0, Lcom/msc/c/g;

    invoke-direct {v0}, Lcom/msc/c/g;-><init>()V

    sput-object v0, Lcom/msc/c/g;->a:Lcom/msc/c/g;

    .line 310
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->q()Z

    move-result v0

    invoke-static {v0}, Lcom/msc/b/i;->a(Z)V

    .line 311
    sget-object v0, Lcom/msc/c/g;->a:Lcom/msc/c/g;

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 1927
    const/4 v0, 0x0

    .line 1928
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1931
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->l(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 1934
    :try_start_0
    new-instance v3, Lcom/msc/c/q;

    invoke-direct {v3}, Lcom/msc/c/q;-><init>()V

    .line 1936
    new-instance v4, Ljava/io/StringWriter;

    invoke-direct {v4}, Ljava/io/StringWriter;-><init>()V

    .line 1938
    invoke-virtual {v3, v4}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 1939
    const-string v5, "UTF-8"

    invoke-virtual {v3, v5}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 1940
    const-string v5, ""

    const-string v6, "reqParam"

    invoke-virtual {v3, v5, v6}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1941
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v5

    invoke-virtual {v5}, Lcom/osp/device/b;->a()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {p2}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1943
    const-string v5, ""

    const-string v6, "phoneNumberID"

    invoke-virtual {v3, v5, v6}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1944
    invoke-virtual {v3, p2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 1945
    const-string v5, ""

    const-string v6, "phoneNumberID"

    invoke-virtual {v3, v5, v6}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1954
    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1956
    const-string v5, ""

    const-string v6, "country"

    invoke-virtual {v3, v5, v6}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1957
    invoke-virtual {v3, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 1958
    const-string v1, ""

    const-string v5, "country"

    invoke-virtual {v3, v1, v5}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1961
    :cond_0
    const-string v1, ""

    const-string v5, "appId"

    invoke-virtual {v3, v1, v5}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1962
    invoke-virtual {v3, p1}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 1963
    const-string v1, ""

    const-string v5, "appId"

    invoke-virtual {v3, v1, v5}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1965
    const-string v1, ""

    const-string v5, "langCode"

    invoke-virtual {v3, v1, v5}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1966
    invoke-virtual {v3, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 1967
    const-string v1, ""

    const-string v2, "langCode"

    invoke-virtual {v3, v1, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1968
    const-string v1, ""

    const-string v2, "reqParam"

    invoke-virtual {v3, v1, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1969
    invoke-virtual {v3}, Lcom/msc/c/q;->a()V

    .line 1971
    invoke-virtual {v4}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1976
    :goto_1
    return-object v0

    .line 1948
    :cond_1
    const-string v5, ""

    const-string v6, "emailId"

    invoke-virtual {v3, v5, v6}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1949
    invoke-virtual {v3, p2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 1950
    const-string v5, ""

    const-string v6, "emailId"

    invoke-virtual {v3, v5, v6}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1972
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 5769
    const/4 v1, 0x0

    .line 5772
    :try_start_0
    new-instance v0, Lcom/msc/c/q;

    invoke-direct {v0}, Lcom/msc/c/q;-><init>()V

    .line 5774
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 5776
    invoke-virtual {v0, v2}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 5777
    const-string v3, "UTF-8"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 5779
    const-string v3, ""

    const-string v4, "UserUpdateVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5780
    const-string v3, ""

    const-string v4, "userID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5781
    invoke-virtual {v0, p1}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 5782
    const-string v3, ""

    const-string v4, "userID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5784
    const-string v3, ""

    const-string v4, "userIdentificationVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5786
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/device/b;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 5788
    invoke-static {p2}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 5790
    const-string v3, ""

    const-string v4, "loginIDTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5791
    const-string v3, "001"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 5792
    const-string v3, ""

    const-string v4, "loginIDTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5806
    :goto_0
    const-string v3, ""

    const-string v4, "loginID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5807
    invoke-virtual {v0, p2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 5808
    const-string v3, ""

    const-string v4, "loginID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5810
    const-string v3, ""

    const-string v4, "userPassword"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5812
    invoke-virtual {v0, p3}, Lcom/msc/c/q;->c(Ljava/lang/String;)V

    .line 5813
    const-string v3, ""

    const-string v4, "userPassword"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5815
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/device/b;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 5817
    invoke-static {p2}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 5819
    const-string v3, ""

    const-string v4, "newLoginIDTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5820
    const-string v3, "001"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 5821
    const-string v3, ""

    const-string v4, "newLoginIDTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5836
    :goto_1
    const-string v3, ""

    const-string v4, "newUserPassword"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5837
    invoke-virtual {v0, p4}, Lcom/msc/c/q;->c(Ljava/lang/String;)V

    .line 5838
    const-string v3, ""

    const-string v4, "newUserPassword"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5840
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/device/b;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {p2}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 5842
    invoke-static {}, Lcom/osp/app/util/k;->a()Lcom/osp/app/util/k;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/osp/app/util/k;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 5843
    if-eqz v3, :cond_0

    .line 5845
    const-string v4, ""

    const-string v5, "countryCallingCode"

    invoke-virtual {v0, v4, v5}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5846
    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 5847
    const-string v3, ""

    const-string v4, "countryCallingCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5851
    :cond_0
    const-string v3, ""

    const-string v4, "userIdentificationVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5853
    const-string v3, ""

    const-string v4, "UserUpdateVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5854
    invoke-virtual {v0}, Lcom/msc/c/q;->a()V

    .line 5856
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 5857
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 5863
    :goto_2
    return-object v0

    .line 5795
    :cond_1
    :try_start_2
    const-string v3, ""

    const-string v4, "loginIDTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5796
    const-string v3, "003"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 5797
    const-string v3, ""

    const-string v4, "loginIDTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 5858
    :catch_0
    move-exception v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    :goto_3
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 5801
    :cond_2
    :try_start_3
    const-string v3, ""

    const-string v4, "loginIDTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5802
    const-string v3, "003"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 5803
    const-string v3, ""

    const-string v4, "loginIDTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5824
    :cond_3
    const-string v3, ""

    const-string v4, "newLoginIDTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5825
    const-string v3, "003"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 5826
    const-string v3, ""

    const-string v4, "newLoginIDTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 5831
    :cond_4
    const-string v3, ""

    const-string v4, "newLoginIDTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5832
    const-string v3, "003"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 5833
    const-string v3, ""

    const-string v4, "newLoginIDTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_1

    .line 5858
    :catch_1
    move-exception v1

    goto :goto_3
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 18

    .prologue
    .line 5890
    const/4 v2, 0x0

    .line 5891
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 5895
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v12

    .line 5896
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 5899
    const-string v3, "zh"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "en"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "fr"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "pt"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "es"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "ar"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_0
    move-object v10, v1

    .line 5908
    :goto_0
    const/4 v1, 0x0

    .line 5909
    invoke-static/range {p0 .. p0}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v3

    .line 5910
    if-eqz v3, :cond_e

    .line 5912
    invoke-virtual {v3}, Lcom/osp/device/a;->c()Ljava/lang/String;

    move-result-object v1

    move-object v9, v1

    .line 5918
    :goto_1
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static/range {p0 .. p0}, Lcom/osp/common/util/i;->j(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 5919
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static/range {p0 .. p0}, Lcom/osp/common/util/i;->k(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 5921
    if-nez v3, :cond_1

    .line 5923
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 5925
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {v3}, Lcom/osp/common/util/i;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 5927
    :cond_1
    if-nez v1, :cond_2

    .line 5929
    const-string v1, "00"

    .line 5931
    :cond_2
    invoke-static {}, Lcom/osp/app/util/ag;->a()Lcom/osp/app/util/ag;

    invoke-static {}, Lcom/osp/app/util/ag;->b()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-static {}, Lcom/osp/app/util/ag;->a()Lcom/osp/app/util/ag;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Lcom/osp/app/util/ag;->a(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 5933
    invoke-static {}, Lcom/osp/app/util/ag;->a()Lcom/osp/app/util/ag;

    move-result-object v1

    iget-object v3, v1, Lcom/osp/app/util/ag;->b:Ljava/lang/String;

    .line 5934
    invoke-static {}, Lcom/osp/app/util/ag;->a()Lcom/osp/app/util/ag;

    move-result-object v1

    iget-object v1, v1, Lcom/osp/app/util/ag;->c:Ljava/lang/String;

    move-object v7, v1

    move-object v8, v3

    .line 5937
    :goto_2
    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/r;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "true"

    move-object v6, v1

    .line 5941
    :goto_3
    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v3

    .line 5943
    if-eqz v3, :cond_7

    const-string v1, "true"

    move-object v5, v1

    .line 5946
    :goto_4
    if-eqz v3, :cond_8

    const-string v1, "false"

    move-object v4, v1

    .line 5947
    :goto_5
    if-eqz v3, :cond_9

    invoke-static/range {p0 .. p0}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    .line 5948
    :goto_6
    if-nez p1, :cond_c

    .line 5950
    const-string v1, ""

    .line 5957
    :goto_7
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v13

    invoke-virtual {v13}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v13

    .line 5961
    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "Samsung%20account"

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-virtual {v15, v0}, Lcom/osp/device/b;->j(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 6222
    new-instance v15, Lorg/apache/http/message/BasicNameValuePair;

    const-string v16, "serviceCd"

    const-string v17, "saccount"

    invoke-direct/range {v15 .. v17}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6223
    new-instance v15, Lorg/apache/http/message/BasicNameValuePair;

    const-string v16, "_common_country"

    move-object/from16 v0, v16

    invoke-direct {v15, v0, v12}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6224
    new-instance v12, Lorg/apache/http/message/BasicNameValuePair;

    const-string v15, "_common_lang"

    invoke-direct {v12, v15, v10}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6225
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v12, "targetUrl"

    move-object/from16 v0, p2

    invoke-direct {v10, v12, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6226
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v12, "chnlCd"

    const-string v15, "odc"

    invoke-direct {v10, v12, v15}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6228
    if-eqz p4, :cond_3

    .line 6231
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v12, "_common_check_sso_login"

    invoke-direct {v10, v12, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6232
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "_common_check_sso_logout"

    invoke-direct {v5, v10, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6233
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "guid"

    invoke-direct {v4, v5, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6234
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "tokenId"

    invoke-direct {v4, v5, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6238
    :cond_3
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "isChn"

    invoke-direct {v3, v4, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6239
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "mcc"

    invoke-direct {v3, v4, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6240
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "email"

    if-nez v1, :cond_4

    const-string v1, ""

    :cond_4
    invoke-direct {v3, v4, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6241
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "mnc"

    invoke-direct {v1, v3, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6242
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "brandNm"

    const-string v4, ""

    invoke-direct {v1, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6243
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "dvcModelCd"

    invoke-direct {v1, v3, v13}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6244
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "odcVersion"

    invoke-direct {v1, v3, v14}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6246
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v4

    .line 6247
    new-instance v5, Ljava/lang/StringBuffer;

    const-string v1, ""

    invoke-direct {v5, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 6248
    const/4 v1, 0x0

    move v3, v1

    :goto_8
    if-ge v3, v4, :cond_b

    .line 6250
    if-nez v3, :cond_a

    .line 6254
    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v6, "?"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-interface {v11, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/NameValuePair;

    invoke-interface {v1}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v6, "="

    invoke-virtual {v1, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-interface {v11, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/NameValuePair;

    invoke-interface {v1}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 6248
    :goto_9
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_8

    .line 5905
    :cond_5
    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    move-object v10, v1

    goto/16 :goto_0

    .line 5937
    :cond_6
    const-string v1, "false"

    move-object v6, v1

    goto/16 :goto_3

    .line 5943
    :cond_7
    const-string v1, "false"

    move-object v5, v1

    goto/16 :goto_4

    .line 5946
    :cond_8
    const-string v1, "true"

    move-object v4, v1

    goto/16 :goto_5

    .line 5947
    :cond_9
    const-string v1, ""

    move-object v3, v1

    goto/16 :goto_6

    .line 6259
    :cond_a
    const-string v1, "&"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-interface {v11, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/NameValuePair;

    invoke-interface {v1}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v6, "="

    invoke-virtual {v1, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-interface {v11, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/NameValuePair;

    invoke-interface {v1}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_9

    .line 6265
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    move-object v1, v2

    .line 6270
    :goto_a
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6272
    return-object v1

    .line 6263
    :cond_b
    :try_start_1
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    goto :goto_a

    :cond_c
    move-object/from16 v1, p1

    goto/16 :goto_7

    :cond_d
    move-object v7, v1

    move-object v8, v3

    goto/16 :goto_2

    :cond_e
    move-object v9, v1

    goto/16 :goto_1
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 2657
    .line 2660
    const-string v2, "WIFI"

    .line 2665
    :try_start_0
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->f(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 2666
    :try_start_1
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->k(Landroid/content/Context;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 2667
    :try_start_2
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {}, Lcom/osp/common/util/i;->b()Ljava/lang/String;

    move-result-object v2

    .line 2668
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {}, Lcom/osp/common/util/i;->c()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v1

    .line 2669
    :try_start_3
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->l(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sget-object v6, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v6}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v0

    .line 2670
    :try_start_4
    new-instance v6, Lcom/msc/c/q;

    invoke-direct {v6}, Lcom/msc/c/q;-><init>()V

    .line 2672
    new-instance v7, Ljava/io/StringWriter;

    invoke-direct {v7}, Ljava/io/StringWriter;-><init>()V

    .line 2674
    invoke-virtual {v6, v7}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 2675
    const-string v8, "UTF-8"

    invoke-virtual {v6, v8}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 2676
    const-string v8, ""

    const-string v9, "reqCreateField"

    invoke-virtual {v6, v8, v9}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2677
    if-eqz p2, :cond_0

    .line 2679
    const-string v8, ""

    const-string v9, "isModified"

    invoke-virtual {v6, v8, v9}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2680
    const-string v8, "Y"

    invoke-virtual {v6, v8}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2681
    const-string v8, ""

    const-string v9, "isModified"

    invoke-virtual {v6, v8, v9}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2685
    :cond_0
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 2687
    const-string v8, ""

    const-string v9, "country"

    invoke-virtual {v6, v8, v9}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2688
    invoke-virtual {v6, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2689
    const-string v8, ""

    const-string v9, "country"

    invoke-virtual {v6, v8, v9}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2692
    :cond_1
    const-string v8, ""

    const-string v9, "appId"

    invoke-virtual {v6, v8, v9}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2693
    invoke-virtual {v6, p1}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2694
    const-string v8, ""

    const-string v9, "appId"

    invoke-virtual {v6, v8, v9}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2696
    const-string v8, ""

    const-string v9, "mobileNetworkCode"

    invoke-virtual {v6, v8, v9}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2697
    invoke-virtual {v6, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2698
    const-string v8, ""

    const-string v9, "mobileNetworkCode"

    invoke-virtual {v6, v8, v9}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2700
    const-string v8, ""

    const-string v9, "customerSalesCode"

    invoke-virtual {v6, v8, v9}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2701
    invoke-virtual {v6, v2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2702
    const-string v8, ""

    const-string v9, "customerSalesCode"

    invoke-virtual {v6, v8, v9}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2704
    const-string v8, ""

    const-string v9, "modelId"

    invoke-virtual {v6, v8, v9}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2705
    invoke-virtual {v6, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2706
    const-string v8, ""

    const-string v9, "modelId"

    invoke-virtual {v6, v8, v9}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2708
    const-string v8, ""

    const-string v9, "langCode"

    invoke-virtual {v6, v8, v9}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2709
    invoke-virtual {v6, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2710
    const-string v8, ""

    const-string v9, "langCode"

    invoke-virtual {v6, v8, v9}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2711
    const-string v8, ""

    const-string v9, "reqCreateField"

    invoke-virtual {v6, v8, v9}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2712
    invoke-virtual {v6}, Lcom/msc/c/q;->a()V

    .line 2714
    invoke-virtual {v7}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    move-result-object v5

    .line 2722
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "SaAuthManager::ModifiyAccountInfo_V01_TO_XML result : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2723
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "SaAuthManager::ModifiyAccountInfo_V01_TO_XML mcc : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2724
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "SaAuthManager::ModifiyAccountInfo_V01_TO_XML mnc : "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2725
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SaAuthManager::ModifiyAccountInfo_V01_TO_XML csc : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2726
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SaAuthManager::ModifiyAccountInfo_V01_TO_XML modelid : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2727
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SaAuthManager::ModifiyAccountInfo_V01_TO_XML langCode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2729
    return-object v5

    .line 2716
    :catch_0
    move-exception v0

    move-object v6, v0

    move-object v1, v5

    move-object v3, v5

    move-object v4, v5

    move-object v0, v5

    :goto_1
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    move-object v6, v0

    move-object v1, v5

    move-object v3, v5

    move-object v0, v5

    goto :goto_1

    :catch_2
    move-exception v0

    move-object v6, v0

    move-object v1, v5

    move-object v0, v5

    goto :goto_1

    :catch_3
    move-exception v0

    move-object v6, v0

    move-object v0, v5

    goto :goto_1

    :catch_4
    move-exception v6

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2522
    if-eqz p1, :cond_1

    .line 2525
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "account.samsung.com"

    const-string v2, "/mobile/account/findPassword.do"

    invoke-static {p0, v1, v2}, Lcom/msc/c/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ne v0, v2, :cond_0

    const-string v0, "?osVer=JB"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2546
    :goto_1
    return-object v0

    .line 2525
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 2546
    :cond_1
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->l(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_3

    :cond_2
    const-string v1, "GB"

    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_5

    :cond_4
    const-string v0, "en"

    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "account.samsung.com"

    const-string v4, "/mobile/account/check.do?actionID=FindPassword&serviceID=account"

    invoke-static {p0, v3, v4}, Lcom/msc/c/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&countryCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&languageCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private static a(Lcom/osp/app/signin/SignUpinfo;Ljava/lang/String;ZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2134
    const/4 v1, 0x0

    .line 2137
    :try_start_0
    new-instance v2, Lcom/msc/c/q;

    invoke-direct {v2}, Lcom/msc/c/q;-><init>()V

    .line 2138
    new-instance v3, Ljava/io/StringWriter;

    invoke-direct {v3}, Ljava/io/StringWriter;-><init>()V

    .line 2140
    invoke-virtual {v2, v3}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 2141
    const-string v0, "UTF-8"

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 2142
    const-string v0, ""

    const-string v4, "UserCreateForDeviceVO"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2144
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 2146
    const-string v0, ""

    const-string v4, "loginIDTypeCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2147
    const-string v0, "001"

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2148
    const-string v0, ""

    const-string v4, "loginIDTypeCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2149
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->E()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2151
    const-string v0, ""

    const-string v4, "countryCallingCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2152
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->E()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2153
    const-string v0, ""

    const-string v4, "countryCallingCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2156
    :cond_0
    const-string v0, ""

    const-string v4, "loginID"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2157
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->F()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2158
    const-string v0, ""

    const-string v4, "loginID"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2170
    :goto_0
    const-string v0, ""

    const-string v4, "userPassword"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2172
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->c(Ljava/lang/String;)V

    .line 2173
    const-string v0, ""

    const-string v4, "userPassword"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2175
    const-string v0, ""

    const-string v4, "birthDate"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2176
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2177
    const-string v0, ""

    const-string v4, "birthDate"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2179
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2181
    const-string v0, ""

    const-string v4, "familyName"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2182
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2183
    const-string v0, ""

    const-string v4, "familyName"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2186
    :cond_1
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2188
    const-string v0, ""

    const-string v4, "givenName"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2189
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2190
    const-string v0, ""

    const-string v4, "givenName"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2193
    :cond_2
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->r()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2195
    const-string v0, ""

    const-string v4, "postalCodeText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2196
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2197
    const-string v0, ""

    const-string v4, "postalCodeText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2200
    :cond_3
    const-string v0, ""

    const-string v4, "emailReceiveYNFlag"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2201
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->l()Z

    move-result v0

    if-eqz v0, :cond_1b

    const-string v0, "Y"

    :goto_1
    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2202
    const-string v0, ""

    const-string v4, "emailReceiveYNFlag"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2204
    const-string v0, ""

    const-string v4, "countryCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2205
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2206
    const-string v0, ""

    const-string v4, "countryCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2208
    const-string v0, ""

    const-string v4, "mobileCountryCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2209
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2210
    const-string v0, ""

    const-string v4, "mobileCountryCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2212
    const-string v0, ""

    const-string v4, "tncAccepted"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213
    if-eqz p2, :cond_1c

    const-string v0, "Y"

    :goto_2
    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2214
    const-string v0, ""

    const-string v4, "tncAccepted"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2216
    const-string v0, ""

    const-string v4, "tncTypeCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2217
    const-string v0, "2"

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2218
    const-string v0, ""

    const-string v4, "tncTypeCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2220
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2222
    const-string v0, ""

    const-string v4, "prefixName"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2223
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2224
    const-string v0, ""

    const-string v4, "prefixName"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2227
    :cond_4
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->t()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2229
    const-string v0, ""

    const-string v4, "streetText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2230
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->t()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2231
    const-string v0, ""

    const-string v4, "streetText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2234
    :cond_5
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2236
    const-string v0, ""

    const-string v4, "extendedAddress"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2237
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->u()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2238
    const-string v0, ""

    const-string v4, "extendedAddress"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2241
    :cond_6
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->v()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2243
    const-string v0, ""

    const-string v4, "postOfficeBoxNumberText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2244
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->v()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2245
    const-string v0, ""

    const-string v4, "postOfficeBoxNumberText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2248
    :cond_7
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 2250
    const-string v0, ""

    const-string v4, "localityText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2251
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2252
    const-string v0, ""

    const-string v4, "localityText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2255
    :cond_8
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 2257
    const-string v0, ""

    const-string v4, "regionText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2258
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->w()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2259
    const-string v0, ""

    const-string v4, "regionText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2262
    :cond_9
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 2264
    const-string v0, ""

    const-string v4, "receiveSMSPhoneNumberText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2265
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->s()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2266
    const-string v0, ""

    const-string v4, "receiveSMSPhoneNumberText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2269
    :cond_a
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->x()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 2271
    const-string v0, ""

    const-string v4, "genderTypeCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2272
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->x()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2273
    const-string v0, ""

    const-string v4, "genderTypeCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2276
    :cond_b
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->y()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 2278
    const-string v0, ""

    const-string v4, "userDisplayName"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2279
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->y()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2280
    const-string v0, ""

    const-string v4, "userDisplayName"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2283
    :cond_c
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->z()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 2285
    const-string v0, ""

    const-string v4, "relationshipStatusCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2286
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->z()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2287
    const-string v0, ""

    const-string v4, "relationshipStatusCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290
    :cond_d
    const-string v0, ""

    const-string v4, "privacyAccepted"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2293
    if-eqz p3, :cond_1d

    const-string v0, "Y"

    :goto_3
    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2294
    const-string v0, ""

    const-string v4, "privacyAccepted"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2296
    const-string v0, ""

    const-string v4, "dataCollectionAccepted"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2297
    if-eqz p4, :cond_1e

    const-string v0, "Y"

    :goto_4
    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2298
    const-string v0, ""

    const-string v4, "dataCollectionAccepted"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2301
    const-string v0, ""

    const-string v4, "checkEmailValidation"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2310
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->m()Z

    move-result v0

    if-eqz v0, :cond_1f

    const-string v0, "Y"

    :goto_5
    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2311
    const-string v0, ""

    const-string v4, "checkEmailValidation"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2313
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_f

    const-string v0, "kor"

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    const-string v0, "KOR"

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2315
    :cond_e
    const-string v0, ""

    const-string v4, "nameCheckResultKey"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2316
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2317
    const-string v0, ""

    const-string v4, "nameCheckResultKey"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2320
    :cond_f
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->C()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 2322
    const-string v0, ""

    const-string v4, "joinChannelDetailCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2323
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2324
    const-string v0, ""

    const-string v4, "joinChannelDetailCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2327
    :cond_10
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->D()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 2329
    const-string v0, ""

    const-string v4, "joinPartnerServiceCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->D()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2331
    const-string v0, ""

    const-string v4, "joinPartnerServiceCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2334
    :cond_11
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_13

    const-string v0, "Y"

    invoke-virtual {v0, p9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 2336
    const-string v0, "kor"

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    const-string v0, "KOR"

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 2338
    :cond_12
    const-string v0, ""

    const-string v4, "userCreateNameCheckVO"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2340
    const-string v0, ""

    const-string v4, "nameCheckType"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2341
    const-string v0, "1"

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2342
    const-string v0, ""

    const-string v4, "nameCheckType"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2344
    const-string v0, ""

    const-string v4, "nameCheckFamilyName"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2345
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2346
    const-string v0, ""

    const-string v4, "nameCheckFamilyName"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2348
    const-string v0, ""

    const-string v4, "nameCheckGivenName"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2349
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->c(Ljava/lang/String;)V

    .line 2350
    const-string v0, ""

    const-string v4, "nameCheckGivenName"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2352
    const-string v0, ""

    const-string v4, "nameCheckBirthDate"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2353
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2354
    const-string v0, ""

    const-string v4, "nameCheckBirthDate"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2356
    const-string v0, ""

    const-string v4, "nameCheckCI"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2357
    invoke-virtual {v2, p7}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2358
    const-string v0, ""

    const-string v4, "nameCheckCI"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2360
    const-string v0, ""

    const-string v4, "nameCheckDI"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2361
    invoke-virtual {v2, p8}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2362
    const-string v0, ""

    const-string v4, "nameCheckDI"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2364
    const-string v0, ""

    const-string v4, "nameCheckMethod"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2365
    invoke-virtual {v2, p6}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2366
    const-string v0, ""

    const-string v4, "nameCheckMethod"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2368
    const-string v0, ""

    const-string v4, "userCreateNameCheckVO"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2385
    :cond_13
    :goto_6
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 2387
    const-string v0, ""

    const-string v4, "checkTelephoneNumberValidation"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2388
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->H()Z

    move-result v0

    if-eqz v0, :cond_22

    const-string v0, "Y"

    :goto_7
    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2389
    const-string v0, ""

    const-string v4, "checkTelephoneNumberValidation"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2391
    const-string v0, ""

    const-string v4, "telephoneNumberValidationYNFlag"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2392
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->J()Z

    move-result v0

    if-eqz v0, :cond_23

    const-string v0, "Y"

    :goto_8
    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2393
    const-string v0, ""

    const-string v4, "telephoneNumberValidationYNFlag"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2396
    const-string v0, ""

    const-string v4, "userSMSAuthenticateCheckVO"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2397
    const-string v0, ""

    const-string v4, "authenticateToken"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2398
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->N()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2399
    const-string v0, ""

    const-string v4, "authenticateToken"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2400
    const-string v0, ""

    const-string v4, "authenticateNumber"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2401
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->O()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2402
    const-string v0, ""

    const-string v4, "authenticateNumber"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2403
    const-string v0, ""

    const-string v4, "userSMSAuthenticateCheckVO"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2406
    :cond_14
    const-string v0, ""

    const-string v4, "devicePhysicalAddressText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2407
    invoke-virtual {v2, p1}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2408
    const-string v0, ""

    const-string v4, "devicePhysicalAddressText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2410
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 2412
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->L()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_15

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->M()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 2414
    const-string v0, ""

    const-string v4, "securityQuestionText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2415
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->L()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2416
    const-string v0, ""

    const-string v4, "securityQuestionText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2418
    const-string v0, ""

    const-string v4, "securityAnswerText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2419
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->M()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2420
    const-string v0, ""

    const-string v4, "securityAnswerText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2422
    const-string v0, ""

    const-string v4, "securityQuestionID"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2423
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->K()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2424
    const-string v0, ""

    const-string v4, "securityQuestionID"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2428
    :cond_15
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 2430
    const-string v0, ""

    const-string v4, "suspendPossibleYNFlag"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2431
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->b()Z

    move-result v0

    if-eqz v0, :cond_24

    const-string v0, "Y"

    :goto_9
    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2432
    const-string v0, ""

    const-string v4, "suspendPossibleYNFlag"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2437
    :cond_16
    const-string v0, "KOR"

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_17

    const-string v0, "CHN"

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 2439
    :cond_17
    const-string v0, ""

    const-string v4, "onwardTransferAccepted"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2440
    if-eqz p5, :cond_25

    const-string v0, "Y"

    :goto_a
    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2441
    const-string v0, ""

    const-string v4, "onwardTransferAccepted"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2445
    :cond_18
    invoke-static {p9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 2447
    const-string v0, ""

    const-string v4, "foreignerYNFlag"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2448
    invoke-virtual {v2, p9}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2449
    const-string v0, ""

    const-string v4, "foreignerYNFlag"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2452
    :cond_19
    const-string v0, ""

    const-string v4, "UserCreateForDeviceVO"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2453
    invoke-virtual {v2}, Lcom/msc/c/q;->a()V

    .line 2455
    invoke-virtual {v3}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2456
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 2462
    :goto_b
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SaAuthManager::SignUp_XML result : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2464
    return-object v0

    .line 2161
    :cond_1a
    :try_start_2
    const-string v0, ""

    const-string v4, "loginIDTypeCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2162
    const-string v0, "003"

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2163
    const-string v0, ""

    const-string v4, "loginIDTypeCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2165
    const-string v0, ""

    const-string v4, "loginID"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2166
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2167
    const-string v0, ""

    const-string v4, "loginID"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 2457
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    :goto_c
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_b

    .line 2201
    :cond_1b
    :try_start_3
    const-string v0, "N"

    goto/16 :goto_1

    .line 2213
    :cond_1c
    const-string v0, "N"

    goto/16 :goto_2

    .line 2293
    :cond_1d
    const-string v0, "N"

    goto/16 :goto_3

    .line 2297
    :cond_1e
    const-string v0, "N"

    goto/16 :goto_4

    .line 2310
    :cond_1f
    const-string v0, "N"

    goto/16 :goto_5

    .line 2369
    :cond_20
    const-string v0, "chn"

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_21

    const-string v0, "CHN"

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpinfo;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 2371
    :cond_21
    const-string v0, ""

    const-string v4, "userGlobalNameCheckCreateVO"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2373
    const-string v0, ""

    const-string v4, "globalNameCheckCI"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2374
    invoke-virtual {v2, p7}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2375
    const-string v0, ""

    const-string v4, "globalNameCheckCI"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2377
    const-string v0, ""

    const-string v4, "globalNameCheckMethod"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2378
    invoke-virtual {v2, p6}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2379
    const-string v0, ""

    const-string v4, "globalNameCheckMethod"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2381
    const-string v0, ""

    const-string v4, "userGlobalNameCheckCreateVO"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 2388
    :cond_22
    const-string v0, "N"

    goto/16 :goto_7

    .line 2392
    :cond_23
    const-string v0, "N"

    goto/16 :goto_8

    .line 2431
    :cond_24
    const-string v0, "N"

    goto/16 :goto_9

    .line 2440
    :cond_25
    const-string v0, "N"
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_a

    .line 2457
    :catch_1
    move-exception v1

    goto :goto_c
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 7337
    const/4 v0, 0x0

    .line 7340
    :try_start_0
    new-instance v2, Lcom/msc/c/q;

    invoke-direct {v2}, Lcom/msc/c/q;-><init>()V

    .line 7342
    new-instance v3, Ljava/io/StringWriter;

    invoke-direct {v3}, Ljava/io/StringWriter;-><init>()V

    .line 7344
    invoke-virtual {v2, v3}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 7345
    const-string v1, "UTF-8"

    invoke-virtual {v2, v1}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 7346
    const-string v1, ""

    const-string v4, "UserLoginIDVO"

    invoke-virtual {v2, v1, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7348
    const-string v1, ""

    const-string v4, "loginID"

    invoke-virtual {v2, v1, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7349
    invoke-virtual {v2, p0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 7350
    const-string v1, ""

    const-string v4, "loginID"

    invoke-virtual {v2, v1, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7352
    const-string v1, "003"

    .line 7353
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/osp/device/b;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 7355
    const-string v1, "001"

    .line 7358
    :cond_0
    const-string v4, ""

    const-string v5, "loginIDTypeCode"

    invoke-virtual {v2, v4, v5}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7359
    invoke-virtual {v2, v1}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 7360
    const-string v1, ""

    const-string v4, "loginIDTypeCode"

    invoke-virtual {v2, v1, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7362
    const-string v1, ""

    const-string v4, "UserLoginIDVO"

    invoke-virtual {v2, v1, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7363
    invoke-virtual {v2}, Lcom/msc/c/q;->a()V

    .line 7365
    invoke-virtual {v3}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7366
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 7371
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SaRequestSet::makeGetUserIDToXML result : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 7373
    return-object v0

    .line 7367
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Lcom/msc/a/j;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 8375
    const/4 v1, 0x0

    .line 8378
    :try_start_0
    new-instance v0, Lcom/msc/c/q;

    invoke-direct {v0}, Lcom/msc/c/q;-><init>()V

    .line 8380
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 8382
    invoke-virtual {v0, v2}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 8383
    const-string v3, "UTF-8"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 8385
    const-string v3, ""

    const-string v4, "UserUpdateVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8386
    const-string v3, ""

    const-string v4, "userID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8387
    invoke-virtual {v0, p0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 8388
    const-string v3, ""

    const-string v4, "userID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8390
    const-string v3, ""

    const-string v4, "userSecurityQuestionVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8391
    const-string v3, ""

    const-string v4, "securityQuestionID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8392
    invoke-virtual {p1}, Lcom/msc/a/j;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 8393
    const-string v3, ""

    const-string v4, "securityQuestionID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8395
    const-string v3, ""

    const-string v4, "securityQuestionText"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8396
    invoke-virtual {p1}, Lcom/msc/a/j;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 8397
    const-string v3, ""

    const-string v4, "securityQuestionText"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8399
    const-string v3, ""

    const-string v4, "securityAnswerText"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8400
    invoke-virtual {p1}, Lcom/msc/a/j;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 8401
    const-string v3, ""

    const-string v4, "securityAnswerText"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8403
    const-string v3, ""

    const-string v4, "userSecurityQuestionVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8405
    const-string v3, ""

    const-string v4, "UserUpdateVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8406
    invoke-virtual {v0}, Lcom/msc/c/q;->a()V

    .line 8408
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 8409
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 8415
    :goto_0
    return-object v0

    .line 8410
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;Lcom/osp/app/signin/SignUpinfo;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2911
    const/4 v1, 0x0

    .line 2915
    :try_start_0
    new-instance v0, Lcom/msc/c/q;

    invoke-direct {v0}, Lcom/msc/c/q;-><init>()V

    .line 2916
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 2918
    invoke-virtual {v0, v2}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 2919
    const-string v3, "UTF-8"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 2921
    const-string v3, ""

    const-string v4, "UserUpdateVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2922
    const-string v3, ""

    const-string v4, "userID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2923
    invoke-virtual {v0, p0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2924
    const-string v3, ""

    const-string v4, "userID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2941
    const-string v3, ""

    const-string v4, "userBaseVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2942
    const-string v3, ""

    const-string v4, "userTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2943
    const-string v3, "1"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2944
    const-string v3, ""

    const-string v4, "userTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2946
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->j()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2948
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 2950
    const-string v3, ""

    const-string v4, "countryCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2951
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2952
    const-string v3, ""

    const-string v4, "countryCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2956
    :cond_0
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->y()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2958
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->y()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 2960
    const-string v3, ""

    const-string v4, "userDisplayName"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2961
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->y()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2962
    const-string v3, ""

    const-string v4, "userDisplayName"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2966
    :cond_1
    const-string v3, ""

    const-string v4, "userStatusCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2967
    const-string v3, "1"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2968
    const-string v3, ""

    const-string v4, "userStatusCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2970
    const-string v3, ""

    const-string v4, "emailReceiveYNFlag"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2971
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->l()Z

    move-result v3

    if-eqz v3, :cond_10

    .line 2973
    const-string v3, "Y"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2978
    :goto_0
    const-string v3, ""

    const-string v4, "emailReceiveYNFlag"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2980
    const-string v3, ""

    const-string v4, "userBaseIndividualVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2982
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->p()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 2984
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    .line 2986
    const-string v3, ""

    const-string v4, "familyName"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2987
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2988
    const-string v3, ""

    const-string v4, "familyName"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2992
    :cond_2
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->o()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 2994
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_3

    .line 2996
    const-string v3, ""

    const-string v4, "givenName"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2997
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2998
    const-string v3, ""

    const-string v4, "givenName"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3002
    :cond_3
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->n()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 3004
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_4

    .line 3006
    const-string v3, ""

    const-string v4, "prefixName"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3007
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3008
    const-string v3, ""

    const-string v4, "prefixName"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3012
    :cond_4
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->x()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 3014
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->x()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_5

    .line 3016
    const-string v3, ""

    const-string v4, "genderTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3017
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->x()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3018
    const-string v3, ""

    const-string v4, "genderTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3021
    :cond_5
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->z()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 3023
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->z()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_6

    .line 3025
    const-string v3, ""

    const-string v4, "relationshipStatusCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3026
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->z()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3027
    const-string v3, ""

    const-string v4, "relationshipStatusCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3031
    :cond_6
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->i()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 3033
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_7

    .line 3035
    const-string v3, ""

    const-string v4, "birthDate"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3036
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3037
    const-string v3, ""

    const-string v4, "birthDate"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3041
    :cond_7
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->s()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 3043
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->s()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_8

    .line 3045
    const-string v3, ""

    const-string v4, "receiveSMSPhoneNumberText"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3046
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->s()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3047
    const-string v3, ""

    const-string v4, "receiveSMSPhoneNumberText"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3051
    :cond_8
    const-string v3, ""

    const-string v4, "userBaseIndividualVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3052
    const-string v3, ""

    const-string v4, "userBaseVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3054
    const-string v3, ""

    const-string v4, "userContactAddressVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3055
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->q()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_9

    .line 3057
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_9

    .line 3059
    const-string v3, ""

    const-string v4, "localityText"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3060
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3061
    const-string v3, ""

    const-string v4, "localityText"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3066
    :cond_9
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->t()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_a

    .line 3068
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->t()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_a

    .line 3070
    const-string v3, ""

    const-string v4, "streetText"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3071
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->t()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3072
    const-string v3, ""

    const-string v4, "streetText"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3075
    :cond_a
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->u()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_b

    .line 3077
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->u()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_b

    .line 3079
    const-string v3, ""

    const-string v4, "extendedAddress"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3080
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->u()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3081
    const-string v3, ""

    const-string v4, "extendedAddress"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3084
    :cond_b
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->v()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_c

    .line 3086
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->v()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_c

    .line 3088
    const-string v3, ""

    const-string v4, "postOfficeBoxNumberText"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3089
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->v()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3090
    const-string v3, ""

    const-string v4, "postOfficeBoxNumberText"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3093
    :cond_c
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->w()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_d

    .line 3095
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->w()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_d

    .line 3097
    const-string v3, ""

    const-string v4, "regionText"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3098
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->w()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3099
    const-string v3, ""

    const-string v4, "regionText"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3103
    :cond_d
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->A()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_e

    .line 3105
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->A()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_e

    .line 3107
    const-string v3, ""

    const-string v4, "addressTypeSequence"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3108
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->A()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3109
    const-string v3, ""

    const-string v4, "addressTypeSequence"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3112
    :cond_e
    const-string v3, ""

    const-string v4, "addressTypeText"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3113
    const-string v3, "ADR"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3114
    const-string v3, ""

    const-string v4, "addressTypeText"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3116
    const-string v3, ""

    const-string v4, "addressLocationTypeText"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3117
    const-string v3, "HOME"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3118
    const-string v3, ""

    const-string v4, "addressLocationTypeText"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3120
    const-string v3, ""

    const-string v4, "preferenceAddressTypeYNFlag"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3121
    const-string v3, "Y"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3122
    const-string v3, ""

    const-string v4, "preferenceAddressTypeYNFlag"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3124
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->r()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_f

    .line 3126
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->r()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_f

    .line 3128
    const-string v3, ""

    const-string v4, "postalCodeText"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3129
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->r()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3130
    const-string v3, ""

    const-string v4, "postalCodeText"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3133
    :cond_f
    const-string v3, ""

    const-string v4, "userContactAddressVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3135
    const-string v3, ""

    const-string v4, "UserUpdateVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3136
    invoke-virtual {v0}, Lcom/msc/c/q;->a()V

    .line 3138
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 3139
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 3145
    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SaAuthManager::ModifiyAccountInfo_TO_XML userid : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3148
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SaAuthManager::ModifiyAccountInfo_TO_XML result : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3150
    return-object v0

    .line 2976
    :cond_10
    :try_start_2
    const-string v3, "N"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 3140
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    :goto_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v1

    goto :goto_2
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 3835
    const/4 v1, 0x0

    .line 3839
    :try_start_0
    new-instance v0, Lcom/msc/c/q;

    invoke-direct {v0}, Lcom/msc/c/q;-><init>()V

    .line 3841
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 3843
    invoke-virtual {v0, v2}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 3844
    const-string v3, "UTF-8"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 3845
    const-string v3, ""

    const-string v4, "UserDisclaimerVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3847
    const-string v3, ""

    const-string v4, "userID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3848
    invoke-virtual {v0, p0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3849
    const-string v3, ""

    const-string v4, "userID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3851
    const-string v3, ""

    const-string v4, "appID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3852
    invoke-virtual {v0, p1}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3853
    const-string v3, ""

    const-string v4, "appID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3855
    const-string v3, ""

    const-string v4, "disclaimer"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3856
    const-string v3, "Y"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3857
    const-string v3, ""

    const-string v4, "disclaimer"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3859
    const-string v3, ""

    const-string v4, "UserDisclaimerVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3860
    invoke-virtual {v0}, Lcom/msc/c/q;->a()V

    .line 3862
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 3863
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 3869
    :goto_0
    return-object v0

    .line 3864
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2477
    const/4 v1, 0x0

    .line 2480
    :try_start_0
    new-instance v0, Lcom/msc/c/q;

    invoke-direct {v0}, Lcom/msc/c/q;-><init>()V

    .line 2482
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 2484
    invoke-virtual {v0, v2}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 2485
    const-string v3, "UTF-8"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 2487
    const-string v3, ""

    const-string v4, "UserUpdateVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2488
    const-string v3, ""

    const-string v4, "userID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2489
    invoke-virtual {v0, p0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2490
    const-string v3, ""

    const-string v4, "userID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2492
    const-string v3, ""

    const-string v4, "userBaseVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2493
    const-string v3, ""

    const-string v4, "countryCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2494
    invoke-virtual {v0, p1}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2495
    const-string v3, ""

    const-string v4, "countryCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2496
    const-string v3, ""

    const-string v4, "mobileCountryCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2497
    invoke-virtual {v0, p2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 2498
    const-string v3, ""

    const-string v4, "mobileCountryCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2499
    const-string v3, ""

    const-string v4, "userBaseVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2501
    const-string v3, ""

    const-string v4, "UserUpdateVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2502
    invoke-virtual {v0}, Lcom/msc/c/q;->a()V

    .line 2504
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2505
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 2511
    :goto_0
    return-object v0

    .line 2506
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 7932
    const/4 v0, 0x0

    .line 7935
    :try_start_0
    new-instance v1, Lcom/msc/c/q;

    invoke-direct {v1}, Lcom/msc/c/q;-><init>()V

    .line 7937
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 7939
    invoke-virtual {v1, v2}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 7940
    const-string v3, "UTF-8"

    invoke-virtual {v1, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 7941
    const-string v3, ""

    const-string v4, "UserTelephoneNumberAuthenticateValidateVO"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7943
    const-string v3, ""

    const-string v4, "countryCallingCode"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7944
    invoke-virtual {v1, p0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 7945
    const-string v3, ""

    const-string v4, "countryCallingCode"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7947
    const-string v3, ""

    const-string v4, "telephoneNumber"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7948
    invoke-virtual {v1, p1}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 7949
    const-string v3, ""

    const-string v4, "telephoneNumber"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7951
    const-string v3, ""

    const-string v4, "authenticateToken"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7952
    invoke-virtual {v1, p2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 7953
    const-string v3, ""

    const-string v4, "authenticateToken"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7955
    const-string v3, ""

    const-string v4, "authenticateNumber"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7956
    invoke-virtual {v1, p3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 7957
    const-string v3, ""

    const-string v4, "authenticateNumber"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7959
    const-string v3, ""

    const-string v4, "authenticateTypeCode"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7960
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 7961
    const-string v3, ""

    const-string v4, "authenticateTypeCode"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7963
    const-string v3, ""

    const-string v4, "UserTelephoneNumberAuthenticateValidateVO"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7964
    invoke-virtual {v1}, Lcom/msc/c/q;->a()V

    .line 7966
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7967
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 7972
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SaRequestSet::makeSMSValidateXML result : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 7974
    return-object v0

    .line 7968
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 3770
    const/4 v1, 0x0

    .line 3774
    :try_start_0
    new-instance v0, Lcom/msc/c/q;

    invoke-direct {v0}, Lcom/msc/c/q;-><init>()V

    .line 3776
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 3778
    invoke-virtual {v0, v2}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 3779
    const-string v3, "UTF-8"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 3780
    const-string v3, ""

    const-string v4, "reqParam"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3782
    const-string v3, ""

    const-string v4, "deviceModelID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3783
    invoke-virtual {v0, p0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3784
    const-string v3, ""

    const-string v4, "deviceModelID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3786
    const-string v3, ""

    const-string v4, "deviceCustomerCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3787
    invoke-virtual {v0, p1}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3788
    const-string v3, ""

    const-string v4, "deviceCustomerCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3790
    const-string v3, ""

    const-string v4, "conuntryCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3791
    invoke-virtual {v0, p2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3792
    const-string v3, ""

    const-string v4, "conuntryCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3794
    const-string v3, ""

    const-string v4, "mode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3795
    invoke-virtual {v0, p3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3796
    const-string v3, ""

    const-string v4, "mode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3798
    const-string v3, ""

    const-string v4, "languageCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3799
    invoke-virtual {v0, p4}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3800
    const-string v3, ""

    const-string v4, "languageCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3810
    const-string v3, ""

    const-string v4, "devicePhysicalAddressText"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3811
    invoke-virtual {v0, p5}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3812
    const-string v3, ""

    const-string v4, "devicePhysicalAddressText"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3814
    const-string v3, ""

    const-string v4, "reqParam"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3815
    invoke-virtual {v0}, Lcom/msc/c/q;->a()V

    .line 3817
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 3818
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 3824
    :goto_0
    return-object v0

    .line 3819
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 6871
    const/4 v1, 0x0

    .line 6874
    :try_start_0
    new-instance v0, Lcom/msc/c/q;

    invoke-direct {v0}, Lcom/msc/c/q;-><init>()V

    .line 6876
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 6878
    invoke-virtual {v0, v2}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 6879
    const-string v3, "UTF-8"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 6880
    const-string v3, ""

    const-string v4, "createNameCheckCIN"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6882
    const-string v3, ""

    const-string v4, "userID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6883
    invoke-virtual {v0, p0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 6884
    const-string v3, ""

    const-string v4, "userID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6886
    const-string v3, ""

    const-string v4, "nameCheckDateTime"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6887
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 6889
    invoke-virtual {v0, p6}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 6897
    :goto_0
    const-string v3, ""

    const-string v4, "nameCheckDateTime"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6899
    const-string v3, ""

    const-string v4, "nameCheckCI"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6900
    invoke-virtual {v0, p5}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 6901
    const-string v3, ""

    const-string v4, "nameCheckCI"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6910
    const-string v3, ""

    const-string v4, "nameCheckMethod"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6911
    invoke-virtual {v0, p4}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 6912
    const-string v3, ""

    const-string v4, "nameCheckMethod"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6914
    const-string v3, ""

    const-string v4, "familyName"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6915
    invoke-virtual {v0, p1}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 6916
    const-string v3, ""

    const-string v4, "familyName"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6918
    const-string v3, ""

    const-string v4, "givenName"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6919
    invoke-virtual {v0, p2}, Lcom/msc/c/q;->c(Ljava/lang/String;)V

    .line 6920
    const-string v3, ""

    const-string v4, "givenName"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6922
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 6924
    const-string v3, ""

    const-string v4, "birthDate"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6925
    invoke-virtual {v0, p3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 6926
    const-string v3, ""

    const-string v4, "birthDate"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6929
    :cond_0
    const-string v3, ""

    const-string v4, "createNameCheckCIN"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6930
    invoke-virtual {v0}, Lcom/msc/c/q;->a()V

    .line 6932
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 6933
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 6939
    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SaRequestSet::makeNameValidationCheckCHNToXML result : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6941
    return-object v0

    .line 6892
    :cond_1
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 6893
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 6894
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "yyyyMMddHHmmss"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 6895
    invoke-virtual {v4, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 6934
    :catch_0
    move-exception v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    :goto_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v1

    goto :goto_2
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 6759
    const/4 v1, 0x0

    .line 6762
    :try_start_0
    new-instance v0, Lcom/msc/c/q;

    invoke-direct {v0}, Lcom/msc/c/q;-><init>()V

    .line 6764
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 6766
    invoke-virtual {v0, v2}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 6767
    const-string v3, "UTF-8"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 6768
    const-string v3, ""

    const-string v4, "UserNameCheckPlusVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6770
    const-string v3, ""

    const-string v4, "familyName"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6771
    invoke-virtual {v0, p1}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 6772
    const-string v3, ""

    const-string v4, "familyName"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6774
    const-string v3, ""

    const-string v4, "givenName"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6775
    invoke-virtual {v0, p2}, Lcom/msc/c/q;->c(Ljava/lang/String;)V

    .line 6776
    const-string v3, ""

    const-string v4, "givenName"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6778
    const-string v3, ""

    const-string v4, "birthDate"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6779
    invoke-virtual {v0, p3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 6780
    const-string v3, ""

    const-string v4, "birthDate"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6782
    const-string v3, ""

    const-string v4, "userID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6783
    invoke-virtual {v0, p0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 6784
    const-string v3, ""

    const-string v4, "userID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6786
    const-string v3, ""

    const-string v4, "nameCheckMethod"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6787
    invoke-virtual {v0, p4}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 6788
    const-string v3, ""

    const-string v4, "nameCheckMethod"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6790
    const-string v3, ""

    const-string v4, "nameCheckCI"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6791
    invoke-virtual {v0, p5}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 6792
    const-string v3, ""

    const-string v4, "nameCheckCI"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6794
    const-string v3, ""

    const-string v4, "nameCheckDI"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6795
    invoke-virtual {v0, p6}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 6796
    const-string v3, ""

    const-string v4, "nameCheckDI"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6798
    if-eqz p7, :cond_0

    invoke-virtual {p7}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 6800
    const-string v3, ""

    const-string v4, "genderTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6801
    invoke-virtual {v0, p7}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 6802
    const-string v3, ""

    const-string v4, "genderTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6805
    :cond_0
    const-string v3, ""

    const-string v4, "UserNameCheckPlusVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6806
    invoke-virtual {v0}, Lcom/msc/c/q;->a()V

    .line 6808
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 6809
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 6815
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SaRequestSet::makeNameValidationCheckToXML result : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6817
    return-object v0

    .line 6810
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 8868
    const/4 v1, 0x0

    .line 8871
    :try_start_0
    new-instance v0, Lcom/msc/c/q;

    invoke-direct {v0}, Lcom/msc/c/q;-><init>()V

    .line 8873
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 8875
    invoke-virtual {v0, v2}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 8876
    const-string v3, "UTF-8"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 8878
    const-string v3, ""

    const-string v4, "SubDeviceVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8881
    const-string v3, ""

    const-string v4, "subDeviceUniqueID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8882
    invoke-virtual {v0, p5}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 8883
    const-string v3, ""

    const-string v4, "subDeviceUniqueID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8886
    const-string v3, ""

    const-string v4, "deviceUniqueID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8887
    invoke-virtual {v0, p1}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 8888
    const-string v3, ""

    const-string v4, "deviceUniqueID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8891
    const-string v3, ""

    const-string v4, "userID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8892
    invoke-virtual {v0, p0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 8893
    const-string v3, ""

    const-string v4, "userID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8896
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 8898
    const-string v3, ""

    const-string v4, "subDeviceVersion"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8899
    invoke-virtual {v0, p2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 8900
    const-string v3, ""

    const-string v4, "subDeviceVersion"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8904
    :cond_0
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 8906
    const-string v3, ""

    const-string v4, "subDeviceManagerVersion"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8907
    invoke-virtual {v0, p6}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 8908
    const-string v3, ""

    const-string v4, "subDeviceManagerVersion"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8912
    :cond_1
    const-string v3, ""

    const-string v4, "subDeviceModelID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8913
    invoke-virtual {v0, p4}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 8914
    const-string v3, ""

    const-string v4, "subDeviceModelID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8917
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 8919
    const-string v3, ""

    const-string v4, "subDeviceCustomerSalesCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8920
    invoke-virtual {v0, p3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 8921
    const-string v3, ""

    const-string v4, "subDeviceCustomerSalesCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8925
    :cond_2
    invoke-static {p7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 8927
    const-string v3, ""

    const-string v4, "subDeviceMobileCountryCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8928
    invoke-virtual {v0, p7}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 8929
    const-string v3, ""

    const-string v4, "subDeviceMobileCountryCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8933
    :cond_3
    invoke-static {p8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 8935
    const-string v3, ""

    const-string v4, "subDeviceMobileNetworkCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8936
    invoke-virtual {v0, p8}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 8937
    const-string v3, ""

    const-string v4, "subDeviceMobileNetworkCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8940
    :cond_4
    const-string v3, ""

    const-string v4, "SubDeviceVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8941
    invoke-virtual {v0}, Lcom/msc/c/q;->a()V

    .line 8943
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 8944
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 8950
    :goto_0
    return-object v0

    .line 8945
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 7744
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 7748
    :try_start_0
    const-string v0, "device_id"

    invoke-virtual {v1, v0, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 7749
    const-string v0, "imsi"

    invoke-virtual {v1, v0, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 7750
    const-string v0, "ccc"

    invoke-virtual {v1, v0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 7751
    const-string v0, "phone_number"

    invoke-virtual {v1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 7752
    const-string v0, "language"

    const-string v2, "_"

    const-string v3, "-"

    invoke-virtual {p4, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 7759
    :goto_0
    return-object v1

    .line 7754
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public static a(JLcom/msc/b/g;)V
    .locals 2

    .prologue
    .line 4747
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lcom/msc/b/i;->a(JLcom/msc/b/g;)V

    .line 4748
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/msc/b/d;)V
    .locals 3

    .prologue
    .line 316
    const-string v0, "x-osp-clientmodel"

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    const-string v0, "x-osp-clientversion"

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->j(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    const-string v0, "x-osp-clientosversion"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->l()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 6528
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, "prepareGetMyCountryZone"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6530
    invoke-static {p0}, Lcom/msc/c/p;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 6543
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 6544
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 6547
    const-string v1, "j5p7ll8g33:5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    .line 6550
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 6552
    const-string v2, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6554
    :cond_0
    const-string v2, "x-osp-appId"

    const-string v3, "j5p7ll8g33"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6555
    const-string v2, "Content-Type"

    const-string v3, "text/xml"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6556
    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Basic "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6561
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 6563
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1744
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " user deauthentication go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1745
    invoke-static {p0, p1}, Lcom/msc/c/l;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1747
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 1748
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 1749
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v2

    .line 1758
    :try_start_0
    new-instance v0, Lcom/osp/common/property/a;

    invoke-direct {v0, p0}, Lcom/osp/common/property/a;-><init>(Landroid/content/Context;)V

    .line 1759
    const-string v3, "device.registration.appid"

    const-string v4, "14eev3f64b"

    invoke-virtual {v0, v3, v4}, Lcom/osp/common/property/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1760
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "SRS"

    const-string v4, "get appID form property."

    invoke-static {v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1761
    const-string v3, "j5p7ll8g33"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1763
    new-instance v0, Lcom/osp/common/util/e;

    const-string v3, "j5p7ll8g33"

    const-string v4, "5763D0052DC1462E13751F753384E9A9"

    invoke-direct {v0, p0, v3, v4}, Lcom/osp/common/util/e;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1768
    :goto_0
    sget-object v3, Lcom/osp/http/impl/f;->d:Lcom/osp/http/impl/f;

    const/4 v4, 0x0

    sget-object v5, Lcom/osp/common/util/g;->a:Lcom/osp/common/util/g;

    invoke-virtual {v0, v3, v1, v4, v5}, Lcom/osp/common/util/e;->a(Lcom/osp/http/impl/f;Ljava/lang/String;Ljava/lang/String;Lcom/osp/common/util/g;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1773
    :goto_1
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1775
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1777
    :cond_0
    const-string v1, "User-Agent"

    const-string v3, "SAMSUNG-Android"

    invoke-virtual {v2, v1, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1778
    const-string v1, "Accept"

    const-string v3, "*, */*"

    invoke-virtual {v2, v1, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1779
    const-string v1, "Accept-Encoding"

    const-string v3, "identity"

    invoke-virtual {v2, v1, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1780
    const-string v1, "Content-Type"

    const-string v3, "text/xml"

    invoke-virtual {v2, v1, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1781
    const-string v1, "Connection"

    const-string v3, "keep-alive"

    invoke-virtual {v2, v1, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1782
    const-string v1, "x-osp-version"

    const-string v3, "v1"

    invoke-virtual {v2, v1, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1783
    const-string v1, "Authorization"

    invoke-virtual {v2, v1, v0}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1789
    invoke-static {p0, v2}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 1796
    invoke-virtual {v2}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0

    .line 1766
    :cond_1
    :try_start_1
    new-instance v0, Lcom/osp/common/util/e;

    const-string v3, "14eev3f64b"

    const-string v4, "109E2830E09DB340924B8ABE0D6290C3"

    invoke-direct {v0, p0, v3, v4}, Lcom/osp/common/util/e;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1769
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v6

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 1809
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " checkPasswordValidation go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1810
    invoke-static {p0}, Lcom/msc/c/o;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1825
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 1826
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 1827
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 1829
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1831
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1833
    :cond_0
    const-string v1, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1838
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 1840
    const-string v1, "username"

    invoke-virtual {v0, v1, p1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1841
    const-string v1, "password"

    invoke-virtual {v0, v1, p2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1843
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1845
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1847
    const-string v1, "login_id_type"

    const-string v2, "plain_id"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1848
    invoke-static {}, Lcom/osp/app/util/k;->a()Lcom/osp/app/util/k;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/app/util/k;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1849
    if-eqz v1, :cond_1

    .line 1851
    const-string v2, "countryCallingCode"

    invoke-virtual {v0, v2, v1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1866
    :cond_1
    :goto_0
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0

    .line 1855
    :cond_2
    const-string v1, "login_id_type"

    const-string v2, "email_id"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/osp/app/signin/cn;Lcom/msc/b/h;)J
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 539
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SRS"

    const-string v2, "prepareAuthenticateForLostPhone()"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    const-string v1, "auth.samsungosp.com"

    const-string v2, "/auth/oauth2/authenticateForLostPhone"

    invoke-static {p0, v1, v2}, Lcom/msc/c/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 547
    :try_start_0
    invoke-static {p0}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v3

    .line 548
    if-eqz v3, :cond_6

    .line 550
    invoke-virtual {v3}, Lcom/osp/device/a;->c()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 551
    :try_start_1
    invoke-virtual {v3}, Lcom/osp/device/a;->d()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    :goto_0
    move-object v6, v0

    move-object v7, v2

    .line 572
    :goto_1
    invoke-static {p0, v1}, Lcom/osp/app/util/ad;->f(Landroid/content/Context;Ljava/lang/String;)V

    .line 574
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 576
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 577
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 579
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 581
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    :cond_0
    const-string v1, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 592
    const-string v1, "client_id"

    const-string v2, "j5p7ll8g33"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    const-string v1, "client_secret"

    const-string v2, "5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    const-string v1, "imei"

    invoke-virtual {v0, v1, v6}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    const-string v1, "username"

    invoke-virtual {v0, v1, p1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    const-string v1, "password"

    invoke-virtual {v0, v1, p2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    const-string v1, "physical_address_text"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/osp/common/util/d;->a()Lcom/osp/common/util/d;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/osp/common/util/d;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 601
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 603
    const-string v1, "login_id_type"

    const-string v2, "plain_id"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    invoke-static {}, Lcom/osp/app/util/k;->a()Lcom/osp/app/util/k;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/app/util/k;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 605
    if-eqz v1, :cond_1

    .line 607
    const-string v2, "countryCallingCode"

    invoke-virtual {v0, v2, v1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    :cond_1
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 611
    if-eqz p3, :cond_4

    invoke-virtual {p3}, Lcom/osp/app/signin/cn;->b()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 613
    const-string v1, "check_duplication_id"

    const-string v2, "Y"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    invoke-virtual {p3}, Lcom/osp/app/signin/cn;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 616
    const-string v1, "birth_day"

    invoke-virtual {p3}, Lcom/osp/app/signin/cn;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    :cond_2
    invoke-virtual {p3}, Lcom/osp/app/signin/cn;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 620
    const-string v1, "first_name"

    invoke-virtual {p3}, Lcom/osp/app/signin/cn;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    :cond_3
    invoke-virtual {p3}, Lcom/osp/app/signin/cn;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 624
    const-string v1, "last_name"

    invoke-virtual {p3}, Lcom/osp/app/signin/cn;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    :cond_4
    :goto_2
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0

    .line 553
    :catch_0
    move-exception v2

    move-object v3, v2

    move-object v2, v0

    :goto_3
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    move-object v6, v0

    move-object v7, v2

    goto/16 :goto_1

    .line 630
    :cond_5
    const-string v1, "login_id_type"

    const-string v2, "email_id"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 553
    :catch_1
    move-exception v3

    goto :goto_3

    :cond_6
    move-object v2, v0

    goto/16 :goto_0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 3492
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " 3rd Disclaimer go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3494
    const-string v0, "account.samsung.com"

    const-string v1, "/membership/api/get3rdPartyServiceInfo.do"

    invoke-static {p0, v0, v1}, Lcom/msc/c/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3498
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 3499
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 3500
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 3502
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3504
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3506
    :cond_0
    const-string v1, "Content-Type"

    const-string v2, "text/xml"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3511
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 3513
    invoke-static {p1, p2, p3}, Lcom/msc/c/g;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 3515
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 1993
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " saffron migration go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1994
    invoke-static {p0}, Lcom/msc/c/p;->o(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 2009
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 2010
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 2011
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 2013
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2015
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2017
    :cond_0
    const-string v1, "Content-Type"

    const-string v2, "text/xml"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2018
    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bearer "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2023
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 2025
    invoke-static {p2, p3, p4}, Lcom/msc/c/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 2034
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 8504
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " prepareUpdateUserLoginID"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8506
    invoke-static {p0}, Lcom/msc/c/p;->t(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 8512
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 8513
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 8514
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p8

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 8517
    const-string v1, "j5p7ll8g33:5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    .line 8522
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 8524
    const-string v2, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8527
    :cond_0
    const-string v2, "x-osp-appId"

    const-string v3, "j5p7ll8g33"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8528
    const-string v2, "Content-Type"

    const-string v3, "text/xml"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8529
    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Basic "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8534
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 8536
    invoke-static/range {p1 .. p7}, Lcom/msc/c/g;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 8538
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SRS"

    const-string v2, " prepareUpdateUserLoginID"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8539
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method private static b()Ljava/lang/String;
    .locals 6

    .prologue
    .line 9079
    const/4 v1, 0x0

    .line 9082
    :try_start_0
    new-instance v0, Lcom/msc/c/q;

    invoke-direct {v0}, Lcom/msc/c/q;-><init>()V

    .line 9083
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 9085
    invoke-virtual {v0, v2}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 9086
    const-string v3, "UTF-8"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 9088
    const-string v3, ""

    const-string v4, "UserValidationUpdateVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9090
    const-string v3, ""

    const-string v4, "emailValidationYNFlag"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9091
    const-string v3, "Y"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 9092
    const-string v3, ""

    const-string v4, "emailValidationYNFlag"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 9094
    const-string v3, ""

    const-string v4, "checkEmailValidation"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9095
    const-string v3, "Y"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 9096
    const-string v3, ""

    const-string v4, "checkEmailValidation"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 9098
    const-string v3, ""

    const-string v4, "UserValidationUpdateVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 9100
    invoke-virtual {v0}, Lcom/msc/c/q;->a()V

    .line 9102
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 9103
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 9109
    :goto_0
    return-object v0

    .line 9104
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private static b(Ljava/lang/String;Lcom/osp/app/signin/SignUpinfo;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 3242
    const/4 v1, 0x0

    .line 3247
    :try_start_0
    new-instance v2, Lcom/msc/c/q;

    invoke-direct {v2}, Lcom/msc/c/q;-><init>()V

    .line 3248
    new-instance v3, Ljava/io/StringWriter;

    invoke-direct {v3}, Ljava/io/StringWriter;-><init>()V

    .line 3250
    invoke-virtual {v2, v3}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 3251
    const-string v0, "UTF-8"

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 3253
    const-string v0, ""

    const-string v4, "memberUpdate"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3254
    const-string v0, ""

    const-string v4, "userID"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3255
    invoke-virtual {v2, p0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3256
    const-string v0, ""

    const-string v4, "userID"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3257
    const-string v0, ""

    const-string v4, "userUpdate"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3259
    const-string v0, ""

    const-string v4, "userProfile"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3260
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 3262
    const-string v0, ""

    const-string v4, "countryCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3263
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3264
    const-string v0, ""

    const-string v4, "countryCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3268
    :cond_0
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->y()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 3270
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->y()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 3272
    const-string v0, ""

    const-string v4, "userDisplayName"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3273
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->y()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3274
    const-string v0, ""

    const-string v4, "userDisplayName"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3279
    :cond_1
    const-string v0, ""

    const-string v4, "individual"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3280
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->p()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 3282
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 3284
    const-string v0, ""

    const-string v4, "familyName"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3285
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3286
    const-string v0, ""

    const-string v4, "familyName"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3290
    :cond_2
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->o()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 3292
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 3294
    const-string v0, ""

    const-string v4, "givenName"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3295
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3296
    const-string v0, ""

    const-string v4, "givenName"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3300
    :cond_3
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 3302
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 3304
    const-string v0, ""

    const-string v4, "prefixName"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3305
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3306
    const-string v0, ""

    const-string v4, "prefixName"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3311
    :cond_4
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->x()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 3313
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->x()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_5

    .line 3315
    const-string v0, ""

    const-string v4, "genderTypeCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3316
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->x()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3317
    const-string v0, ""

    const-string v4, "genderTypeCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3320
    :cond_5
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->z()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 3322
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->z()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_6

    .line 3324
    const-string v0, ""

    const-string v4, "relationshipStatusCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3325
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->z()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3326
    const-string v0, ""

    const-string v4, "relationshipStatusCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3331
    :cond_6
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_7

    .line 3333
    const-string v0, ""

    const-string v4, "birthDate"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3334
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3335
    const-string v0, ""

    const-string v4, "birthDate"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3336
    const-string v0, ""

    const-string v4, "individual"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3340
    :cond_7
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->q()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_9

    :cond_8
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->r()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_11

    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_11

    .line 3342
    :cond_9
    const-string v0, ""

    const-string v4, "userAddress"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3343
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->B()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 3345
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->B()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_13

    .line 3347
    const-string v0, ""

    const-string v4, "cudFlag"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3348
    const-string v0, "U"

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3349
    const-string v0, ""

    const-string v4, "cudFlag"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3356
    :goto_0
    const-string v0, ""

    const-string v4, "addressID"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3357
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->B()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3358
    const-string v0, ""

    const-string v4, "addressID"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3361
    :cond_a
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->q()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 3363
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_b

    .line 3365
    const-string v0, ""

    const-string v4, "localityText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3366
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3367
    const-string v0, ""

    const-string v4, "localityText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3372
    :cond_b
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->t()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 3374
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->t()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_c

    .line 3376
    const-string v0, ""

    const-string v4, "streetText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3377
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->t()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3378
    const-string v0, ""

    const-string v4, "streetText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3381
    :cond_c
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->u()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 3383
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->u()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_d

    .line 3385
    const-string v0, ""

    const-string v4, "extendedText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3386
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->u()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3387
    const-string v0, ""

    const-string v4, "extendedText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3390
    :cond_d
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->v()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 3392
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->v()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_e

    .line 3394
    const-string v0, ""

    const-string v4, "postOfficeBoxNumberText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3395
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->v()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3396
    const-string v0, ""

    const-string v4, "postOfficeBoxNumberText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3399
    :cond_e
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->w()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 3401
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->w()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_f

    .line 3403
    const-string v0, ""

    const-string v4, "regionText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3404
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->w()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3405
    const-string v0, ""

    const-string v4, "regionText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3409
    :cond_f
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->r()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 3411
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_10

    .line 3413
    const-string v0, ""

    const-string v4, "postalCodeText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3414
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3415
    const-string v0, ""

    const-string v4, "postalCodeText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3419
    :cond_10
    const-string v0, ""

    const-string v4, "addressTypeCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3420
    const-string v0, "ADR"

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3421
    const-string v0, ""

    const-string v4, "addressTypeCode"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3423
    const-string v0, ""

    const-string v4, "locationTypeText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3424
    const-string v0, "HOME"

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3425
    const-string v0, ""

    const-string v4, "locationTypeText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3427
    const-string v0, ""

    const-string v4, "internationalYNFlag"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3428
    const-string v0, "N"

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3429
    const-string v0, ""

    const-string v4, "internationalYNFlag"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3431
    const-string v0, ""

    const-string v4, "preferedAddressYNFlag"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3432
    const-string v0, "Y"

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3433
    const-string v0, ""

    const-string v4, "preferedAddressYNFlag"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3434
    const-string v0, ""

    const-string v4, "userAddress"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3437
    :cond_11
    const-string v0, ""

    const-string v4, "userProfile"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3438
    const-string v0, ""

    const-string v4, "userPreference"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3440
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->s()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 3442
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->s()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_12

    .line 3444
    const-string v0, ""

    const-string v4, "receiveSMSPhoneNumberText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3445
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->s()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3446
    const-string v0, ""

    const-string v4, "receiveSMSPhoneNumberText"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3449
    :cond_12
    const-string v0, ""

    const-string v4, "emailReceiveYNFlag"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3451
    invoke-virtual {p1}, Lcom/osp/app/signin/SignUpinfo;->l()Z

    move-result v0

    const/4 v4, 0x1

    if-ne v0, v4, :cond_14

    .line 3453
    const-string v0, "Y"

    .line 3458
    :goto_1
    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3459
    const-string v0, ""

    const-string v4, "emailReceiveYNFlag"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3460
    const-string v0, ""

    const-string v4, "userPreference"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3462
    const-string v0, ""

    const-string v4, "userUpdate"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3463
    const-string v0, ""

    const-string v4, "memberUpdate"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3464
    invoke-virtual {v2}, Lcom/msc/c/q;->a()V

    .line 3466
    invoke-virtual {v3}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 3467
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 3473
    :goto_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SaAuthManager::ModifiyAccountInfo_V01_TO_XML userid : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3476
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SaAuthManager::ModifiyAccountInfo_V01_TO_XML result : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3478
    return-object v0

    .line 3352
    :cond_13
    :try_start_2
    const-string v0, ""

    const-string v4, "cudFlag"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3353
    const-string v0, "C"

    invoke-virtual {v2, v0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3354
    const-string v0, ""

    const-string v4, "cudFlag"

    invoke-virtual {v2, v0, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 3468
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    :goto_3
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 3456
    :cond_14
    :try_start_3
    const-string v0, "N"
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    .line 3468
    :catch_1
    move-exception v1

    goto :goto_3
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 6609
    const/4 v0, 0x0

    .line 6613
    :try_start_0
    new-instance v1, Lcom/msc/c/q;

    invoke-direct {v1}, Lcom/msc/c/q;-><init>()V

    .line 6615
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 6617
    invoke-virtual {v1, v2}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 6618
    const-string v3, "UTF-8"

    invoke-virtual {v1, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 6619
    const-string v3, ""

    const-string v4, "reqParam"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6620
    const-string v3, ""

    const-string v4, "country"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6621
    invoke-virtual {v1, p0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 6622
    const-string v3, ""

    const-string v4, "country"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6624
    const-string v3, ""

    const-string v4, "language"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6625
    invoke-virtual {v1, p1}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 6626
    const-string v3, ""

    const-string v4, "language"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6627
    const-string v3, ""

    const-string v4, "reqParam"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6628
    invoke-virtual {v1}, Lcom/msc/c/q;->a()V

    .line 6630
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 6635
    :goto_0
    return-object v0

    .line 6631
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 3880
    const/4 v1, 0x0

    .line 3884
    :try_start_0
    new-instance v0, Lcom/msc/c/q;

    invoke-direct {v0}, Lcom/msc/c/q;-><init>()V

    .line 3886
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 3888
    invoke-virtual {v0, v2}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 3889
    const-string v3, "UTF-8"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 3890
    const-string v3, ""

    const-string v4, "reqParam"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3892
    const-string v3, ""

    const-string v4, "appID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3893
    invoke-virtual {v0, p0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3894
    const-string v3, ""

    const-string v4, "appID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3896
    const-string v3, ""

    const-string v4, "country"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3897
    invoke-virtual {v0, p1}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3898
    const-string v3, ""

    const-string v4, "country"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3900
    const-string v3, ""

    const-string v4, "language"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3901
    invoke-virtual {v0, p2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 3902
    const-string v3, ""

    const-string v4, "language"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3904
    const-string v3, ""

    const-string v4, "reqParam"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3905
    invoke-virtual {v0}, Lcom/msc/c/q;->a()V

    .line 3907
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 3908
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 3914
    :goto_0
    return-object v0

    .line 3909
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 8553
    const/4 v1, 0x0

    .line 8556
    :try_start_0
    new-instance v0, Lcom/msc/c/q;

    invoke-direct {v0}, Lcom/msc/c/q;-><init>()V

    .line 8558
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 8560
    invoke-virtual {v0, v2}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 8561
    const-string v3, "UTF-8"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 8563
    const-string v3, ""

    const-string v4, "UserUpdateLoginIDVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8565
    const-string v3, ""

    const-string v4, "userID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8566
    invoke-virtual {v0, p0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 8567
    const-string v3, ""

    const-string v4, "userID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8569
    const-string v3, ""

    const-string v4, "newLoginIDTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8570
    invoke-virtual {v0, p2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 8571
    const-string v3, ""

    const-string v4, "newLoginIDTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8573
    const-string v3, ""

    const-string v4, "newLoginID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8574
    invoke-virtual {v0, p1}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 8575
    const-string v3, ""

    const-string v4, "newLoginID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8577
    const-string v3, ""

    const-string v4, "countryCallingCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8578
    invoke-virtual {v0, p3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 8579
    const-string v3, ""

    const-string v4, "countryCallingCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8581
    const-string v3, ""

    const-string v4, "suspendPossibleYNFlag"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8582
    invoke-virtual {v0, p4}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 8583
    const-string v3, ""

    const-string v4, "suspendPossibleYNFlag"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8585
    const-string v3, ""

    const-string v4, "authenticateToken"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8586
    invoke-virtual {v0, p5}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 8587
    const-string v3, ""

    const-string v4, "authenticateToken"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8589
    const-string v3, ""

    const-string v4, "authenticateNumber"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8590
    invoke-virtual {v0, p6}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 8591
    const-string v3, ""

    const-string v4, "authenticateNumber"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8593
    const-string v3, ""

    const-string v4, "UserUpdateLoginIDVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8594
    invoke-virtual {v0}, Lcom/msc/c/q;->a()V

    .line 8596
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 8597
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 8603
    :goto_0
    return-object v0

    .line 8598
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public static c(Landroid/content/Context;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 6978
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, "preparePackageInfoList"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6982
    const-string v0, "account.samsung.com"

    const-string v1, "/membership/api/getPackageInfoList.do"

    invoke-static {p0, v0, v1}, Lcom/msc/c/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 6984
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 6985
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 6986
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 6988
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6990
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6992
    :cond_0
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 2609
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " GetSignUpInfoFieldByEmailPhone go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2610
    const-string v0, "account.samsung.com"

    const-string v1, "/membership/api/getUserCreateByFieldEmailPhone.do"

    invoke-static {p0, v0, v1}, Lcom/msc/c/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2612
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 2613
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 2614
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 2617
    if-eqz p1, :cond_0

    const-string v1, ""

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2624
    :cond_0
    const-string p1, "j5p7ll8g33"

    .line 2627
    :cond_1
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2629
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2631
    :cond_2
    const-string v1, "Content-Type"

    const-string v2, "text/xml"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2636
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 2638
    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 2645
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 1879
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " checkPasswordValidation go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1880
    const-string v0, "account.samsung.com"

    const-string v1, "/membership/api/getEmptyMandatoryList.do"

    invoke-static {p0, v0, v1}, Lcom/msc/c/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1884
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 1885
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 1886
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 1888
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1890
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1892
    :cond_0
    const-string v1, "Content-Type"

    const-string v2, "text/xml"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1897
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 1899
    if-eqz p1, :cond_1

    const-string v1, ""

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1906
    :cond_1
    const-string p1, "j5p7ll8g33"

    .line 1909
    :cond_2
    invoke-static {p0, p1, p2}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 1916
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 3564
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " AgreeToDisclaimer go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3567
    invoke-static {p0}, Lcom/msc/c/p;->k(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 3569
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 3570
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 3571
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 3578
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3580
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3582
    :cond_0
    const-string v1, "x-osp-appId"

    const-string v2, "j5p7ll8g33"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3583
    const-string v1, "x-osp-userId"

    invoke-virtual {v0, v1, p2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3584
    const-string v1, "Content-Type"

    const-string v2, "text/xml"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3585
    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bearer "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3590
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 3592
    invoke-static {p2, p3}, Lcom/msc/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 3599
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 7889
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, "prepareRequestSMSAuthenticate()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7891
    invoke-static {p0}, Lcom/msc/c/p;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 7893
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 7894
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    const/4 v3, 0x0

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 7898
    const-string v1, "j5p7ll8g33:5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    .line 7902
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 7904
    const-string v2, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7906
    :cond_0
    const-string v2, "x-osp-appId"

    const-string v3, "j5p7ll8g33"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7907
    const-string v2, "Content-Type"

    const-string v3, "text/xml"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7908
    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Basic "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7913
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 7916
    invoke-static {p1, p2, p3, p4}, Lcom/msc/c/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 7918
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method private static c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 7068
    const/4 v1, 0x0

    .line 7071
    :try_start_0
    new-instance v0, Lcom/msc/c/q;

    invoke-direct {v0}, Lcom/msc/c/q;-><init>()V

    .line 7073
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 7075
    invoke-virtual {v0, v2}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 7076
    const-string v3, "UTF-8"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 7077
    const-string v3, ""

    const-string v4, "UserConfirmVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7079
    const-string v3, ""

    const-string v4, "loginID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7080
    invoke-virtual {v0, p0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 7081
    const-string v3, ""

    const-string v4, "loginID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7083
    const-string v3, ""

    const-string v4, "userPassword"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7084
    invoke-virtual {v0, p1}, Lcom/msc/c/q;->c(Ljava/lang/String;)V

    .line 7085
    const-string v3, ""

    const-string v4, "userPassword"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7087
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/device/b;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 7089
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 7091
    const-string v3, ""

    const-string v4, "loginIDTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7092
    const-string v3, "001"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 7093
    const-string v3, ""

    const-string v4, "loginIDTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7107
    :goto_0
    const-string v3, ""

    const-string v4, "UserConfirmVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7108
    invoke-virtual {v0}, Lcom/msc/c/q;->a()V

    .line 7110
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 7111
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 7117
    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SaRequestSet::makeConfirmAccountToXML result : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 7119
    return-object v0

    .line 7096
    :cond_0
    :try_start_2
    const-string v3, ""

    const-string v4, "loginIDTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7097
    const-string v3, "003"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 7098
    const-string v3, ""

    const-string v4, "loginIDTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 7112
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    :goto_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 7102
    :cond_1
    :try_start_3
    const-string v3, ""

    const-string v4, "loginIDTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7103
    const-string v3, "003"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 7104
    const-string v3, ""

    const-string v4, "loginIDTypeCode"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 7112
    :catch_1
    move-exception v1

    goto :goto_2
.end method

.method private static c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 7640
    const/4 v0, 0x0

    .line 7643
    :try_start_0
    new-instance v1, Lcom/msc/c/q;

    invoke-direct {v1}, Lcom/msc/c/q;-><init>()V

    .line 7645
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 7647
    invoke-virtual {v1, v2}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 7648
    const-string v3, "UTF-8"

    invoke-virtual {v1, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 7649
    const-string v3, ""

    const-string v4, "UserTelephoneNumberAuthenticateRequestVO"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7651
    const-string v3, ""

    const-string v4, "countryCallingCode"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7652
    invoke-virtual {v1, p0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 7653
    const-string v3, ""

    const-string v4, "countryCallingCode"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7655
    const-string v3, ""

    const-string v4, "telephoneNumber"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7656
    invoke-virtual {v1, p1}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 7657
    const-string v3, ""

    const-string v4, "telephoneNumber"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7659
    const-string v3, ""

    const-string v4, "authenticateTypeCode"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7660
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 7661
    const-string v3, ""

    const-string v4, "authenticateTypeCode"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7663
    const-string v3, ""

    const-string v4, "languageCode"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7664
    invoke-virtual {v1, p2}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 7665
    const-string v3, ""

    const-string v4, "languageCode"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7667
    const-string v3, ""

    const-string v4, "UserTelephoneNumberAuthenticateRequestVO"

    invoke-virtual {v1, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7668
    invoke-virtual {v1}, Lcom/msc/c/q;->a()V

    .line 7670
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7671
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 7676
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SaRequestSet::makeSMSAuthenticateXML result : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 7678
    return-object v0

    .line 7672
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2790
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " AccountInfoV01 go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2805
    invoke-static {p0, p1}, Lcom/msc/c/l;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2807
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 2808
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 2809
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v2

    .line 2816
    :try_start_0
    new-instance v0, Lcom/osp/common/util/e;

    const-string v3, "14eev3f64b"

    const-string v4, "109E2830E09DB340924B8ABE0D6290C3"

    invoke-direct {v0, p0, v3, v4}, Lcom/osp/common/util/e;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2823
    :goto_0
    :try_start_1
    const-string v3, "filterParam"

    const-string v4, "1111111"

    invoke-virtual {v0, v3, v4}, Lcom/osp/common/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2824
    sget-object v3, Lcom/osp/http/impl/f;->b:Lcom/osp/http/impl/f;

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v1, v4}, Lcom/osp/common/util/e;->a(Lcom/osp/http/impl/f;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 2829
    :goto_1
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2831
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2833
    :cond_0
    const-string v1, "x-osp-appId"

    const-string v3, "14eev3f64b"

    invoke-virtual {v2, v1, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2834
    const-string v1, "x-osp-userId"

    invoke-virtual {v2, v1, p1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2835
    const-string v1, "x-osp-version"

    const-string v3, "v1"

    invoke-virtual {v2, v1, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2836
    const-string v1, "Authorization"

    invoke-virtual {v2, v1, v0}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2841
    invoke-static {p0, v2}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 2848
    invoke-virtual {v2}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0

    .line 2817
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v6

    goto :goto_0

    .line 2825
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v6

    goto :goto_1
.end method

.method public static d(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 2742
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " AccountInfo go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2743
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/msc/c/p;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2756
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 2757
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 2758
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 2761
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2763
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2765
    :cond_0
    const-string v1, "Content-Type"

    const-string v2, "text/xml"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2766
    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bearer "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2771
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 2778
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static d(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 3611
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " prepareLicenseCheck go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3617
    invoke-static {p0}, Lcom/msc/c/p;->j(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 3626
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 3627
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v2

    .line 3629
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3631
    const-string v0, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3633
    :cond_0
    const-string v0, "x-osp-appId"

    invoke-virtual {v2, v0, p1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3636
    const/4 v0, 0x0

    .line 3640
    :try_start_0
    new-instance v1, Lcom/osp/common/util/e;

    invoke-direct {v1, p0, p1, p2}, Lcom/osp/common/util/e;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 3641
    invoke-virtual {v1}, Lcom/osp/common/util/e;->a()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 3646
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "authorizationHeader = "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3647
    const-string v1, "Authorization"

    invoke-virtual {v2, v1, v0}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3652
    invoke-static {p0, v2}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 3654
    const-string v0, "authToken"

    invoke-virtual {v2, v0, p3}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3656
    invoke-virtual {v2}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0

    .line 3642
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private static d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 8164
    const/4 v1, 0x0

    .line 8167
    :try_start_0
    new-instance v0, Lcom/msc/c/q;

    invoke-direct {v0}, Lcom/msc/c/q;-><init>()V

    .line 8169
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 8171
    invoke-virtual {v0, v2}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 8172
    const-string v3, "UTF-8"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 8173
    const-string v3, ""

    const-string v4, "UserSecurityQuestionVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8175
    const-string v3, ""

    const-string v4, "securityQuestionID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8176
    invoke-virtual {v0, p0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 8177
    const-string v3, ""

    const-string v4, "securityQuestionID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8179
    const-string v3, ""

    const-string v4, "securityAnswerText"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8180
    invoke-virtual {v0, p1}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 8181
    const-string v3, ""

    const-string v4, "securityAnswerText"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8183
    const-string v3, ""

    const-string v4, "UserSecurityQuestionVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8184
    invoke-virtual {v0}, Lcom/msc/c/q;->a()V

    .line 8186
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 8187
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 8193
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HttpRequestSet::makeCheckSequrityAnswerXML result : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 8195
    return-object v0

    .line 8188
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public static e(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 3677
    invoke-static {p0, p1}, Lcom/msc/c/p;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3679
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 3680
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 3681
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 3684
    const-string v1, "j5p7ll8g33:5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    .line 3688
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3690
    const-string v2, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3692
    :cond_0
    const-string v2, "x-osp-appId"

    const-string v3, "j5p7ll8g33"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3693
    const-string v2, "Content-Type"

    const-string v3, "text/xml"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3694
    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Basic "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3699
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 3701
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static e(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 4565
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " AccountVerify go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4566
    invoke-static {p0}, Lcom/msc/c/o;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 4579
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 4580
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 4581
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 4583
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4585
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4587
    :cond_0
    const-string v1, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4592
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 4594
    const-string v1, "username"

    invoke-virtual {v0, v1, p1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4595
    const-string v1, "password"

    invoke-virtual {v0, v1, p2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4597
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4599
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 4601
    const-string v1, "login_id_type"

    const-string v2, "plain_id"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4602
    invoke-static {}, Lcom/osp/app/util/k;->a()Lcom/osp/app/util/k;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/app/util/k;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 4603
    if-eqz v1, :cond_1

    .line 4605
    const-string v2, "countryCallingCode"

    invoke-virtual {v0, v2, v1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4618
    :cond_1
    :goto_0
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0

    .line 4609
    :cond_2
    const-string v1, "login_id_type"

    const-string v2, "email_id"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static e(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 4135
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " Authentication go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4136
    const-string v0, "auth.samsungosp.com"

    const-string v1, "/auth/oauth2/authenticateWithUserID"

    invoke-static {p0, v0, v1}, Lcom/msc/c/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 4138
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 4139
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 4140
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 4143
    const-string v1, "j5p7ll8g33:5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    .line 4146
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4148
    const-string v2, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4150
    :cond_0
    const-string v2, "x-osp-appId"

    const-string v3, "j5p7ll8g33"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4151
    const-string v2, "x-osp-userId"

    invoke-virtual {v0, v2, p1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4152
    const-string v2, "Content-Type"

    const-string v3, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4153
    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Basic "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4158
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 4160
    const-string v1, "user_id"

    invoke-virtual {v0, v1, p1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4161
    const-string v1, "password"

    invoke-virtual {v0, v1, p2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4162
    const-string v1, "login_id_type"

    const-string v2, "plain_id"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4163
    const-string v1, "physical_address_text"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/osp/common/util/d;->a()Lcom/osp/common/util/d;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/osp/common/util/d;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4165
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method private static e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 9002
    const/4 v1, 0x0

    .line 9005
    :try_start_0
    new-instance v0, Lcom/msc/c/q;

    invoke-direct {v0}, Lcom/msc/c/q;-><init>()V

    .line 9006
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 9008
    invoke-virtual {v0, v2}, Lcom/msc/c/q;->a(Ljava/io/StringWriter;)V

    .line 9009
    const-string v3, "UTF-8"

    invoke-virtual {v0, v3}, Lcom/msc/c/q;->a(Ljava/lang/String;)V

    .line 9011
    const-string v3, ""

    const-string v4, "UserAttributeUpdateVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9013
    const-string v3, ""

    const-string v4, "userID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9014
    invoke-virtual {v0, p0}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 9015
    const-string v3, ""

    const-string v4, "userID"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 9017
    const-string v3, ""

    const-string v4, "foreignerYNFlag"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9018
    invoke-virtual {v0, p1}, Lcom/msc/c/q;->b(Ljava/lang/String;)V

    .line 9019
    const-string v3, ""

    const-string v4, "foreignerYNFlag"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 9021
    const-string v3, ""

    const-string v4, "UserAttributeUpdateVO"

    invoke-virtual {v0, v3, v4}, Lcom/msc/c/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 9023
    invoke-virtual {v0}, Lcom/msc/c/q;->a()V

    .line 9025
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 9026
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 9032
    :goto_0
    return-object v0

    .line 9027
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public static f(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 7

    .prologue
    .line 5002
    const-string v0, "account.samsung.com"

    const-string v1, "/membership/api/getMailUrl.do"

    invoke-static {p0, v0, v1}, Lcom/msc/c/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 5009
    :try_start_0
    new-instance v0, Lcom/osp/security/identity/c;

    invoke-direct {v0}, Lcom/osp/security/identity/c;-><init>()V

    .line 5010
    invoke-virtual {v0, p1}, Lcom/osp/security/identity/c;->a(Ljava/lang/String;)V

    .line 5012
    invoke-virtual {v0}, Lcom/osp/security/identity/c;->a()Ljava/lang/String;

    move-result-object v6

    .line 5014
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 5015
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 5017
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5019
    const-string v2, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5021
    :cond_0
    const-string v2, "Content-Type"

    const-string v3, "text/xml"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5026
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 5028
    invoke-virtual {v0, v6}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 5031
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SaAuthManager::requestResendEmail body : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5032
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SaAuthManager::requestResendEmail emailID : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5033
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SaAuthManager::requestResendEmail url : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5035
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 5043
    :goto_0
    return-wide v0

    .line 5037
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 5043
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public static f(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 4631
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " AccountVerify go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4632
    const-string v0, "auth.samsungosp.com"

    const-string v1, "/auth/oauth2/getSignedConfirmationInfo"

    invoke-static {p0, v0, v1}, Lcom/msc/c/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 4645
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 4646
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 4647
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 4649
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4651
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4653
    :cond_0
    const-string v1, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4658
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 4660
    const-string v1, "username"

    invoke-virtual {v0, v1, p1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4661
    const-string v1, "password"

    invoke-virtual {v0, v1, p2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4662
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4664
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 4666
    const-string v1, "login_id_type"

    const-string v2, "plain_id"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4667
    invoke-static {}, Lcom/osp/app/util/k;->a()Lcom/osp/app/util/k;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/app/util/k;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 4668
    if-eqz v1, :cond_1

    .line 4670
    const-string v2, "countryCallingCode"

    invoke-virtual {v0, v2, v1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4683
    :cond_1
    :goto_0
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0

    .line 4674
    :cond_2
    const-string v1, "login_id_type"

    const-string v2, "email_id"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static f(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 4698
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " AccountVerify Fingerprint with Signature go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4699
    const-string v0, "auth.samsungosp.com"

    const-string v1, "/auth/oauth2/getSignedInfoByUserAuthNPhslAddrTxt"

    invoke-static {p0, v0, v1}, Lcom/msc/c/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 4712
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 4713
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 4714
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 4716
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4718
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4720
    :cond_0
    const-string v1, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4725
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 4727
    const-string v1, "userauth_token"

    invoke-virtual {v0, v1, p1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4728
    const-string v1, "login_id"

    invoke-virtual {v0, v1, p2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4729
    const-string v1, "physical_address_text"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/osp/common/util/d;->a()Lcom/osp/common/util/d;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/osp/common/util/d;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4736
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static g(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 7

    .prologue
    .line 5055
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "account.samsung.com"

    const-string v2, "/account/activation/accountActivationMailSend.do"

    invoke-static {p0, v1, v2}, Lcom/msc/c/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?emailID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 5056
    const/4 v0, 0x0

    .line 5059
    :try_start_0
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->f(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v6, v0

    .line 5083
    :goto_0
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 5084
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 5088
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SaAuthManager::requestResendEmail emailID : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5089
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SaAuthManager::requestResendEmail url : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5090
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SaAuthManager::requestResendEmail mcc : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5113
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5115
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5117
    :cond_0
    const-string v1, "acesKey"

    const-string v2, "j5p7ll8g33"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5122
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 5125
    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 5129
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0

    .line 5060
    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    move-object v6, v0

    goto/16 :goto_0
.end method

.method public static g(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 6576
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " prepareGetSpecialTermsList"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6579
    const-string v0, "account.samsung.com"

    const-string v1, "/membership/api/getSpecialTermsList2.do"

    invoke-static {p0, v0, v1}, Lcom/msc/c/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 6581
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 6582
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 6584
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6586
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6588
    :cond_0
    const-string v1, "Content-Type"

    const-string v2, "text/xml"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6593
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 6595
    invoke-static {p1, p2}, Lcom/msc/c/g;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 6597
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static g(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 7543
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, "prepareRequestSMSAuthenticate()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7545
    invoke-static {p0}, Lcom/msc/c/p;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 7546
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 7547
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    const/4 v3, 0x0

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 7551
    const-string v1, "j5p7ll8g33:5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    .line 7555
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 7557
    const-string v2, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7559
    :cond_0
    const-string v2, "x-osp-appId"

    const-string v3, "j5p7ll8g33"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7560
    const-string v2, "Content-Type"

    const-string v3, "text/xml"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7561
    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Basic "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7566
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 7569
    invoke-static {p1, p2, p3}, Lcom/msc/c/g;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 7571
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static h(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 5140
    const-string v0, "account.samsung.com"

    const-string v1, "/account/activation/sendReactivationMail.do"

    invoke-static {p0, v0, v1}, Lcom/msc/c/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 5142
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 5143
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v3

    .line 5146
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5148
    const-string v0, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5156
    :cond_0
    :try_start_0
    const-string v0, "SHA-1"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 5157
    const-string v1, "5763D0052DC1462E13751F753384E9A9"

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v1

    .line 5160
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 5161
    array-length v4, v1

    move v0, v7

    :goto_0
    if-ge v0, v4, :cond_1

    aget-byte v5, v1, v0

    .line 5163
    const-string v7, "%02x"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    aput-object v5, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5161
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5165
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 5173
    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 5175
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "j5p7ll8g33+"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5182
    :goto_2
    :try_start_1
    invoke-static {p0}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/a;->d()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 5183
    :try_start_2
    invoke-static {p0}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/a;->l()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v6

    .line 5189
    :goto_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, " prepareSendReactivationMail acesKety : "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5190
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, " prepareSendReactivationMail devicePhysicalAddressText : "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5191
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, " prepareSendReactivationMail acesKety : "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5192
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, " prepareSendReactivationMail timeZoneID : "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5193
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, " prepareSendReactivationMail mcc : "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5195
    const-string v2, "acesKey"

    invoke-virtual {v3, v2, v0}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5200
    invoke-static {p0, v3}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 5202
    const-string v0, "devicePhysicalAddressText"

    invoke-virtual {v3, v0, v1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5203
    const-string v0, "attemptedTime"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5204
    const-string v0, "timeZoneID"

    invoke-virtual {v3, v0, v6}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5205
    const-string v0, "mcc"

    invoke-virtual {v3, v0, p1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5209
    invoke-virtual {v3}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0

    .line 5167
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v6

    goto/16 :goto_1

    .line 5184
    :catch_1
    move-exception v1

    move-object v2, v1

    move-object v1, v6

    :goto_4
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_3

    :catch_2
    move-exception v2

    goto :goto_4

    :cond_2
    move-object v0, v6

    goto/16 :goto_2
.end method

.method public static h(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 7024
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " Confirm account go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7027
    invoke-static {p0}, Lcom/msc/c/p;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 7029
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 7030
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 7031
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 7033
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7035
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7037
    :cond_0
    const-string v1, "j5p7ll8g33:5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    .line 7041
    const-string v2, "Content-Type"

    const-string v3, "text/xml"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7042
    const-string v2, "x-osp-appId"

    const-string v3, "j5p7ll8g33"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7043
    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Basic "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7048
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 7050
    invoke-static {p1, p2}, Lcom/msc/c/g;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 7056
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static h(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 8125
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, "prepareCheckSecurityAnswerWithUserId"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8127
    invoke-static {p0, p1}, Lcom/msc/c/p;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 8128
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 8129
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 8132
    const-string v1, "j5p7ll8g33:5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    .line 8135
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 8137
    const-string v2, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8139
    :cond_0
    const-string v2, "Content-Type"

    const-string v3, "text/xml"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8140
    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Basic "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8141
    const-string v1, "x-osp-appId"

    const-string v2, "j5p7ll8g33"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8146
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 8148
    invoke-static {p2, p3}, Lcom/msc/c/g;->d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 8150
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static i(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 7288
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, "prepareGetUserID"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7290
    invoke-static {p0}, Lcom/msc/c/p;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 7303
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 7304
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 7307
    const-string v1, "j5p7ll8g33:5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    .line 7310
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 7312
    const-string v2, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7314
    :cond_0
    const-string v2, "x-osp-appId"

    const-string v3, "j5p7ll8g33"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7315
    const-string v2, "Content-Type"

    const-string v3, "text/xml"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7316
    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Basic "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7321
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 7323
    invoke-static {p1}, Lcom/msc/c/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 7327
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static i(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 7434
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, "prepareGetUserID"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7436
    const-string v0, "auth.samsungosp.com"

    const-string v1, "/auth/user/isUsableLoginID"

    invoke-static {p0, v0, v1}, Lcom/msc/c/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 7439
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 7440
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v1

    .line 7443
    const-string v0, "j5p7ll8g33:5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 7446
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 7448
    const-string v2, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7450
    :cond_0
    const-string v2, "x-osp-appId"

    const-string v3, "j5p7ll8g33"

    invoke-virtual {v1, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7451
    const-string v2, "Content-Type"

    const-string v3, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v1, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7452
    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Basic "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7457
    invoke-static {p0, v1}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 7459
    const-string v0, "client_id"

    const-string v2, "j5p7ll8g33"

    invoke-virtual {v1, v0, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7460
    const-string v0, "login_id"

    invoke-virtual {v1, v0, p1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7461
    const-string v0, "countryCallingCode"

    invoke-virtual {v1, v0, p2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7463
    const-string v0, "email_id"

    .line 7464
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 7466
    const-string v0, "plain_id"

    .line 7468
    :cond_1
    const-string v2, "login_id_type"

    invoke-virtual {v1, v2, v0}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7472
    invoke-virtual {v1}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static i(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 9048
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " prepareSkipEmailValidation"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9050
    invoke-static {p0, p2}, Lcom/msc/c/p;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 9052
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 9053
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 9055
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9057
    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9059
    :cond_0
    const-string v1, "Content-Type"

    const-string v2, "text/xml"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9060
    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bearer "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9061
    const-string v1, "x-osp-appId"

    invoke-virtual {v0, v1, p1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9066
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 9068
    invoke-static {}, Lcom/msc/c/g;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 9070
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static j(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 8087
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, "prepareGetSecurityQuestionIdWithUserId"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8089
    invoke-static {p0}, Lcom/msc/c/p;->r(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 8090
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 8091
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 8094
    const-string v1, "j5p7ll8g33:5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    .line 8097
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 8099
    const-string v2, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8101
    :cond_0
    const-string v2, "Content-Type"

    const-string v3, "text/xml"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8102
    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Basic "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8103
    const-string v1, "x-osp-appId"

    const-string v2, "j5p7ll8g33"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8108
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 8110
    const-string v1, "userID"

    invoke-virtual {v0, v1, p1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8112
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static j(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 8208
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, "prepareGetSecurityQuestion"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8210
    invoke-static {p0}, Lcom/msc/c/p;->q(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 8211
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 8212
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v1

    .line 8215
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8217
    const-string v0, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8219
    :cond_0
    const-string v0, "Content-Type"

    const-string v2, "text/xml"

    invoke-virtual {v1, v0, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8220
    const-string v0, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bearer "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8225
    invoke-static {p0, v1}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 8227
    const-string v0, "loginID"

    invoke-virtual {v1, v0, p2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8228
    const-string v0, "003"

    .line 8229
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p2}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 8231
    const-string v0, "001"

    .line 8233
    :cond_1
    const-string v2, "loginIDTypeCode"

    invoke-virtual {v1, v2, v0}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8234
    invoke-virtual {v1}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static k(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 7

    .prologue
    .line 9121
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " prepareLoggingCanceledStep go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9122
    const-string v0, "auth.samsungosp.com"

    const-string v1, "/auth/monitor/signup"

    invoke-static {p0, v0, v1}, Lcom/msc/c/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 9124
    const/4 v0, 0x0

    .line 9127
    :try_start_0
    invoke-static {p0}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v2

    .line 9128
    if-eqz v2, :cond_0

    .line 9130
    invoke-virtual {v2}, Lcom/osp/device/a;->c()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    move-object v6, v0

    .line 9137
    :goto_0
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 9138
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 9139
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 9141
    const-string v1, "j5p7ll8g33:5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    .line 9146
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 9148
    const-string v2, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9151
    :cond_1
    const-string v2, "x-osp-appId"

    const-string v3, "j5p7ll8g33"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9152
    const-string v2, "Content-Type"

    const-string v3, "text/xml"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9153
    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Basic "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9158
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 9160
    const-string v1, "duid"

    invoke-virtual {v0, v1, v6}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 9161
    const-string v1, "modelid"

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 9162
    const-string v1, "version"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/osp/device/b;->j(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 9163
    const-string v1, "step"

    invoke-virtual {v0, v1, p1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 9165
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 9167
    const-string v1, "oobe_yn"

    const-string v2, "Y"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 9173
    :goto_1
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0

    .line 9132
    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    move-object v6, v0

    goto/16 :goto_0

    .line 9170
    :cond_2
    const-string v1, "oobe_yn"

    const-string v2, "N"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static k(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 8336
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, "prepareCheckUserState"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8338
    invoke-static {p0, p1, p2}, Lcom/msc/c/p;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 8339
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 8340
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 8343
    const-string v1, "j5p7ll8g33:5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    .line 8346
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 8348
    const-string v2, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8350
    :cond_0
    const-string v2, "Content-Type"

    const-string v3, "text/xml"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8351
    const-string v2, "x-osp-appId"

    const-string v3, "j5p7ll8g33"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8352
    const-string v2, "x-osp-userId"

    invoke-virtual {v0, v2, p1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8353
    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Basic "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8358
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 8360
    const-string v1, "userID"

    invoke-virtual {v0, v1, p1}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8361
    const-string v1, "loginIDTypeCode"

    invoke-virtual {v0, v1, p2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8363
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static l(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 6

    .prologue
    .line 8963
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " prepareSkipNameValidation go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8964
    invoke-static {p0}, Lcom/msc/c/p;->v(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 8966
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    .line 8967
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    .line 8968
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    .line 8970
    const-string v1, "j5p7ll8g33:5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    .line 8975
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 8977
    const-string v2, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8980
    :cond_0
    const-string v2, "x-osp-appId"

    const-string v3, "j5p7ll8g33"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8981
    const-string v2, "Content-Type"

    const-string v3, "text/xml"

    invoke-virtual {v0, v2, v3}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8982
    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Basic "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8987
    invoke-static {p0, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/d;)V

    .line 8989
    invoke-static {p1, p2}, Lcom/msc/c/g;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    .line 8991
    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    return-wide v0
.end method
