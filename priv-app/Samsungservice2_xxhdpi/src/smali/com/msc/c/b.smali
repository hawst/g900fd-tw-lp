.class public abstract Lcom/msc/c/b;
.super Lcom/msc/sa/activity/a;
.source "AsyncNetworkTask.java"

# interfaces
.implements Lcom/msc/b/h;


# instance fields
.field private final c:Lcom/msc/sa/activity/f;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/msc/sa/activity/a;-><init>(Landroid/content/Context;)V

    .line 66
    new-instance v0, Lcom/msc/sa/activity/f;

    invoke-direct {v0, p0}, Lcom/msc/sa/activity/f;-><init>(Lcom/msc/sa/activity/a;)V

    iput-object v0, p0, Lcom/msc/c/b;->c:Lcom/msc/sa/activity/f;

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/msc/sa/activity/a;-><init>(Landroid/content/Context;IZ)V

    .line 89
    new-instance v0, Lcom/msc/sa/activity/f;

    invoke-direct {v0, p0}, Lcom/msc/sa/activity/f;-><init>(Lcom/msc/sa/activity/a;)V

    iput-object v0, p0, Lcom/msc/c/b;->c:Lcom/msc/sa/activity/f;

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Lcom/msc/sa/activity/a;-><init>(Landroid/content/Context;Z)V

    .line 77
    new-instance v0, Lcom/msc/sa/activity/f;

    invoke-direct {v0, p0}, Lcom/msc/sa/activity/f;-><init>(Lcom/msc/sa/activity/a;)V

    iput-object v0, p0, Lcom/msc/c/b;->c:Lcom/msc/sa/activity/f;

    .line 78
    return-void
.end method


# virtual methods
.method protected varargs abstract a()Ljava/lang/Boolean;
.end method

.method public declared-synchronized a(Lcom/msc/b/c;)V
    .locals 1

    .prologue
    .line 104
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/msc/c/b;->c:Lcom/msc/sa/activity/f;

    invoke-virtual {v0, p1}, Lcom/msc/sa/activity/f;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    monitor-exit p0

    return-void

    .line 104
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Lcom/msc/b/c;)V
    .locals 1

    .prologue
    .line 111
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/msc/c/b;->c:Lcom/msc/sa/activity/f;

    invoke-virtual {v0, p1}, Lcom/msc/sa/activity/f;->b(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113
    monitor-exit p0

    return-void

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c(Lcom/msc/b/c;)V
    .locals 1

    .prologue
    .line 117
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/msc/c/b;->c:Lcom/msc/sa/activity/f;

    invoke-virtual {v0, p1}, Lcom/msc/sa/activity/f;->c(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    monitor-exit p0

    return-void

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/msc/c/b;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
