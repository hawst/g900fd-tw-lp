.class public final Lcom/msc/c/m;
.super Ljava/lang/Object;
.source "UrlManager.java"


# direct methods
.method static synthetic a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 481
    const-string v0, "https://"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->u()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "UrlManager"

    const-string v2, "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx Property Host Test xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/osp/app/util/ah;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    invoke-static {p1}, Lcom/osp/app/util/ah;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "http://"

    :cond_0
    invoke-static {p0}, Lcom/osp/app/util/r;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p1, p2, v0}, Lcom/msc/c/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SaAuthManager::getRequestURL _BaseURL : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " _Path : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " RequestUrl : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    return-object v0

    :cond_2
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "stg-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "UrlManager"

    const-string v2, "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx Staging Mode xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-static {p0}, Lcom/osp/app/util/r;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p1, p2, v0}, Lcom/msc/c/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 593
    const-string v0, "auth.samsungosp.com"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "api.samsungosp.com"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    const-string v0, "/v2/profile/user/checkplus/mobile"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 595
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "cn-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 596
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "UrlManager"

    const-string v1, "Server host changed to cn-"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    :cond_1
    :goto_1
    return-object p2

    .line 593
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 597
    :cond_3
    const-string v0, "account.samsung.com"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "/mobile/account/check.do?actionID=AbuseOAuth2"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "/account/activation/accountActivationMailSend.do"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "/mobile/account/findPassword.do"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "/mobile/account/check.do?actionID=FindPassword&serviceID=account"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 601
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "chn."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 602
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "UrlManager"

    const-string v1, "Server host changed to chn."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
