.class public final Lcom/msc/c/a;
.super Ljava/lang/Object;
.source "AESCryptoV02.java"


# static fields
.field private static f:Lcom/msc/c/a;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:[[I

.field private final d:[[B

.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    sput-object v0, Lcom/msc/c/a;->f:Lcom/msc/c/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-string v0, "AESC"

    iput-object v0, p0, Lcom/msc/c/a;->a:Ljava/lang/String;

    .line 28
    const-string v0, "0123456789ABCDEF"

    iput-object v0, p0, Lcom/msc/c/a;->b:Ljava/lang/String;

    .line 33
    const/16 v0, 0xe

    new-array v0, v0, [[I

    new-array v1, v3, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v4

    new-array v1, v3, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v5

    new-array v1, v3, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v3

    new-array v1, v3, [I

    fill-array-data v1, :array_3

    aput-object v1, v0, v6

    new-array v1, v3, [I

    fill-array-data v1, :array_4

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v3, [I

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [I

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v3, [I

    fill-array-data v2, :array_7

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v3, [I

    fill-array-data v2, :array_8

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v3, [I

    fill-array-data v2, :array_9

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v3, [I

    fill-array-data v2, :array_a

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v3, [I

    fill-array-data v2, :array_b

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v3, [I

    fill-array-data v2, :array_c

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v3, [I

    fill-array-data v2, :array_d

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/msc/c/a;->c:[[I

    .line 38
    const/16 v0, 0xe

    new-array v0, v0, [[B

    new-array v1, v3, [B

    fill-array-data v1, :array_e

    aput-object v1, v0, v4

    new-array v1, v3, [B

    fill-array-data v1, :array_f

    aput-object v1, v0, v5

    new-array v1, v3, [B

    fill-array-data v1, :array_10

    aput-object v1, v0, v3

    new-array v1, v3, [B

    fill-array-data v1, :array_11

    aput-object v1, v0, v6

    new-array v1, v3, [B

    fill-array-data v1, :array_12

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v3, [B

    fill-array-data v2, :array_13

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [B

    fill-array-data v2, :array_14

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v3, [B

    fill-array-data v2, :array_15

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v3, [B

    fill-array-data v2, :array_16

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v3, [B

    fill-array-data v2, :array_17

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v3, [B

    fill-array-data v2, :array_18

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v3, [B

    fill-array-data v2, :array_19

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v3, [B

    fill-array-data v2, :array_1a

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v3, [B

    fill-array-data v2, :array_1b

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/msc/c/a;->d:[[B

    .line 44
    const/16 v0, 0x183

    iget-object v1, p0, Lcom/msc/c/a;->c:[[I

    iget-object v2, p0, Lcom/msc/c/a;->d:[[B

    invoke-static {v0, v1, v2}, Lcom/msc/c/a;->a(I[[I[[B)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/msc/c/a;->e:Ljava/lang/String;

    .line 55
    return-void

    .line 33
    nop

    :array_0
    .array-data 4
        0x0
        0x4
    .end array-data

    :array_1
    .array-data 4
        0x1
        0xa
    .end array-data

    :array_2
    .array-data 4
        0xb
        0x0
    .end array-data

    :array_3
    .array-data 4
        0xa
        0x0
    .end array-data

    :array_4
    .array-data 4
        0x8
        0x2
    .end array-data

    :array_5
    .array-data 4
        0x6
        0x0
    .end array-data

    :array_6
    .array-data 4
        0x2
        0x0
    .end array-data

    :array_7
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_8
    .array-data 4
        0x1
        0x0
    .end array-data

    :array_9
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_a
    .array-data 4
        0x5
        0xc
    .end array-data

    :array_b
    .array-data 4
        0x4
        0x0
    .end array-data

    :array_c
    .array-data 4
        0x3
        0x0
    .end array-data

    :array_d
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 38
    :array_e
    .array-data 1
        0x42t
        0x2at
    .end array-data

    nop

    :array_f
    .array-data 1
        0x73t
        0x75t
    .end array-data

    nop

    :array_10
    .array-data 1
        0x2at
        0x4bt
    .end array-data

    nop

    :array_11
    .array-data 1
        0x2ft
        0x3et
    .end array-data

    nop

    :array_12
    .array-data 1
        0x61t
        0x2at
    .end array-data

    nop

    :array_13
    .array-data 1
        0x73t
        0x50t
    .end array-data

    nop

    :array_14
    .array-data 1
        0x63t
        0x4at
    .end array-data

    nop

    :array_15
    .array-data 1
        0x57t
        0x69t
    .end array-data

    nop

    :array_16
    .array-data 1
        0x6dt
        0x38t
    .end array-data

    nop

    :array_17
    .array-data 1
        0x51t
        0x71t
    .end array-data

    nop

    :array_18
    .array-data 1
        0x6dt
        0x6et
    .end array-data

    nop

    :array_19
    .array-data 1
        0x73t
        0x33t
    .end array-data

    nop

    :array_1a
    .array-data 1
        0x67t
        0x51t
    .end array-data

    nop

    :array_1b
    .array-data 1
        0x62t
        0x6bt
    .end array-data
.end method

.method public static declared-synchronized a()Lcom/msc/c/a;
    .locals 2

    .prologue
    .line 63
    const-class v1, Lcom/msc/c/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/msc/c/a;->f:Lcom/msc/c/a;

    if-eqz v0, :cond_0

    .line 65
    sget-object v0, Lcom/msc/c/a;->f:Lcom/msc/c/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    :goto_0
    monitor-exit v1

    return-object v0

    .line 68
    :cond_0
    :try_start_1
    new-instance v0, Lcom/msc/c/a;

    invoke-direct {v0}, Lcom/msc/c/a;-><init>()V

    .line 69
    sput-object v0, Lcom/msc/c/a;->f:Lcom/msc/c/a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(I[[I[[B)Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v6, 0xe

    const/4 v0, 0x0

    .line 186
    new-array v3, v6, [B

    move v2, v0

    .line 189
    :goto_0
    if-ge v0, v6, :cond_0

    .line 196
    and-int/lit8 v4, p0, 0x1

    .line 197
    shr-int/lit8 p0, p0, 0x1

    .line 198
    add-int/lit8 v1, v0, 0x1

    aget-object v5, p2, v2

    aget-byte v5, v5, v4

    aput-byte v5, v3, v0

    .line 199
    aget-object v0, p1, v2

    aget v0, v0, v4

    move v2, v0

    move v0, v1

    .line 200
    goto :goto_0

    .line 201
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>([B)V

    .line 203
    return-object v0
.end method

.method private declared-synchronized a([B)[B
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 145
    monitor-enter p0

    :try_start_0
    const-string v1, "AES"

    invoke-static {v1}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;

    move-result-object v1

    .line 157
    const-string v2, "SHA1PRNG"

    const-string v3, "Crypto"

    invoke-static {v2, v3}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/SecureRandom;

    move-result-object v2

    .line 158
    if-eqz v2, :cond_0

    .line 160
    invoke-virtual {v2, p1}, Ljava/security/SecureRandom;->setSeed([B)V

    .line 163
    :cond_0
    if-eqz v1, :cond_2

    .line 165
    const/16 v3, 0x80

    invoke-virtual {v1, v3, v2}, Ljavax/crypto/KeyGenerator;->init(ILjava/security/SecureRandom;)V

    .line 166
    invoke-virtual {v1}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v1

    .line 169
    :goto_0
    if-eqz v1, :cond_1

    .line 171
    invoke-interface {v1}, Ljavax/crypto/SecretKey;->getEncoded()[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 173
    :cond_1
    monitor-exit p0

    return-object v0

    .line 145
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method private static b([B)Ljava/lang/String;
    .locals 5

    .prologue
    .line 281
    if-nez p0, :cond_0

    .line 283
    const-string v0, ""

    .line 290
    :goto_0
    return-object v0

    .line 285
    :cond_0
    new-instance v1, Ljava/lang/StringBuffer;

    array-length v0, p0

    mul-int/lit8 v0, v0, 0x2

    invoke-direct {v1, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 286
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 288
    aget-byte v2, p0, v0

    const-string v3, "0123456789ABCDEF"

    shr-int/lit8 v4, v2, 0x4

    and-int/lit8 v4, v4, 0xf

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "0123456789ABCDEF"

    and-int/lit8 v2, v2, 0xf

    invoke-virtual {v4, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 286
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 290
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 215
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/msc/c/a;->a([B)[B

    move-result-object v2

    .line 216
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    div-int/lit8 v3, v0, 0x2

    new-array v4, v3, [B

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    mul-int/lit8 v5, v0, 0x2

    mul-int/lit8 v6, v0, 0x2

    add-int/lit8 v6, v6, 0x2

    invoke-virtual {p2, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x10

    invoke-static {v5, v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->byteValue()B

    move-result v5

    aput-byte v5, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 217
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AESC"

    const-string v3, "decrypt"

    const-string v5, "START"

    invoke-static {v0, v3, v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    const-string v3, "AES"

    invoke-direct {v0, v2, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    const-string v2, "AES"

    invoke-static {v2}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v2

    if-eqz v2, :cond_3

    const/4 v3, 0x2

    invoke-virtual {v2, v3, v0}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    invoke-virtual {v2, v4}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v0

    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "AESC"

    const-string v3, "decrypt"

    const-string v4, "END"

    invoke-static {v2, v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    if-nez v0, :cond_1

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "AESC"

    const-string v3, "decrypted is null"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    :cond_1
    if-eqz v0, :cond_2

    .line 221
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    .line 224
    :cond_2
    return-object v1

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/msc/c/a;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 236
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/msc/c/a;->a([B)[B

    move-result-object v1

    .line 237
    if-nez p2, :cond_0

    .line 242
    :goto_0
    return-object v0

    .line 241
    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "AESC"

    const-string v4, "encrypt"

    const-string v5, "START"

    invoke-static {v3, v4, v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    const-string v4, "AES"

    invoke-direct {v3, v1, v4}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    const-string v1, "AES"

    invoke-static {v1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    invoke-virtual {v1, v0, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    invoke-virtual {v1, v2}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v0

    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AESC"

    const-string v2, "encrypt"

    const-string v3, "END"

    invoke-static {v1, v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    if-nez v0, :cond_2

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AESC"

    const-string v2, "encrypted is null"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    :cond_2
    invoke-static {v0}, Lcom/msc/c/a;->b([B)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
