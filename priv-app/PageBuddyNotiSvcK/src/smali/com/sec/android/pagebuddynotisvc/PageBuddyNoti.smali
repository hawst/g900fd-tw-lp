.class public Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;
.super Ljava/lang/Object;
.source "PageBuddyNoti.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti$1;,
        Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti$PkgEntry;
    }
.end annotation


# static fields
.field static MAX_RECOMMENDED_APPS:I

.field private static lastNotiType:I

.field private static mIconDpi:I

.field private static mLaunchIntent:Landroid/app/PendingIntent;

.field private static notiIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static remoteViewList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/widget/RemoteViews;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mCpMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

.field private final mPkgCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti$PkgEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final nullString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->lastNotiType:I

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->notiIdList:Ljava/util/ArrayList;

    .line 61
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->MAX_RECOMMENDED_APPS:I

    .line 66
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->remoteViewList:Ljava/util/HashMap;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cpMgr"    # Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->nullString:Ljava/lang/String;

    .line 72
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x50

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->mPkgCache:Ljava/util/HashMap;

    .line 77
    invoke-direct {p0, p1}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->resetAllNotification(Landroid/content/Context;)V

    .line 80
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->MAX_RECOMMENDED_APPS:I

    .line 88
    invoke-direct {p0, p1}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->setIconDpi(Landroid/content/Context;)I

    move-result v0

    sput v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->mIconDpi:I

    .line 89
    iput-object p2, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->mCpMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    .line 91
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->mLaunchIntent:Landroid/app/PendingIntent;

    .line 92
    return-void
.end method

.method private drawableToBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v6, 0x0

    .line 464
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v2

    const/4 v5, -0x1

    if-eq v2, v5, :cond_0

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    :goto_0
    invoke-static {v3, v4, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 466
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 467
    .local v1, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {p1, v6, v6, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 468
    invoke-virtual {p1, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 470
    const/4 v1, 0x0

    .line 472
    return-object v0

    .line 464
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    :cond_0
    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    goto :goto_0
.end method

.method private getContextualIcon(I)I
    .locals 1
    .param p1, "cpType"    # I

    .prologue
    .line 449
    packed-switch p1, :pswitch_data_0

    .line 457
    const v0, 0x7f020015

    .line 460
    .local v0, "icon":I
    :goto_0
    return v0

    .line 451
    .end local v0    # "icon":I
    :pswitch_0
    const v0, 0x7f020015

    .line 452
    .restart local v0    # "icon":I
    goto :goto_0

    .line 454
    .end local v0    # "icon":I
    :pswitch_1
    const v0, 0x7f020012

    .line 455
    .restart local v0    # "icon":I
    goto :goto_0

    .line 449
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getIcon(Landroid/content/Context;Landroid/content/Intent;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 349
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 350
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const/4 v0, 0x0

    .line 352
    .local v0, "draw":Landroid/graphics/drawable/Drawable;
    const/4 v3, 0x0

    invoke-virtual {v1, p2, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 353
    .local v2, "resolveInfo":Landroid/content/pm/ResolveInfo;
    if-eqz v2, :cond_0

    .line 354
    invoke-direct {p0, p1, v2}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->getIcon(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 356
    :cond_0
    return-object v0
.end method

.method private getIcon(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "info"    # Landroid/content/pm/ResolveInfo;

    .prologue
    .line 360
    const/4 v4, 0x0

    .line 361
    .local v4, "icon":Landroid/graphics/drawable/Drawable;
    iget-object v6, p2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-nez v6, :cond_2

    iget-object v1, p2, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    .line 362
    .local v1, "ci":Landroid/content/pm/ComponentInfo;
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 364
    .local v5, "pm":Landroid/content/pm/PackageManager;
    const/4 v0, 0x0

    .line 365
    .local v0, "appRes":Landroid/content/res/Resources;
    new-instance v2, Landroid/content/ComponentName;

    iget-object v6, v1, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    iget-object v7, v1, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    invoke-direct {v2, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    .local v2, "componentName":Landroid/content/ComponentName;
    :try_start_0
    invoke-virtual {v5, v2}, Landroid/content/pm/PackageManager;->getResourcesForActivity(Landroid/content/ComponentName;)Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 373
    :goto_1
    if-eqz v0, :cond_0

    .line 374
    :try_start_1
    invoke-virtual {p2}, Landroid/content/pm/ResolveInfo;->getIconResource()I

    move-result v6

    sget v7, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->mIconDpi:I

    invoke-virtual {v0, v6, v7}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 378
    :cond_0
    if-nez v4, :cond_1

    .line 379
    invoke-virtual {v1, v5}, Landroid/content/pm/ComponentInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 382
    :cond_1
    :goto_2
    return-object v4

    .line 361
    .end local v0    # "appRes":Landroid/content/res/Resources;
    .end local v1    # "ci":Landroid/content/pm/ComponentInfo;
    .end local v2    # "componentName":Landroid/content/ComponentName;
    .end local v5    # "pm":Landroid/content/pm/PackageManager;
    :cond_2
    iget-object v1, p2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    goto :goto_0

    .line 368
    .restart local v0    # "appRes":Landroid/content/res/Resources;
    .restart local v1    # "ci":Landroid/content/pm/ComponentInfo;
    .restart local v2    # "componentName":Landroid/content/ComponentName;
    .restart local v5    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v3

    .line 369
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 375
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v3

    .line 376
    .local v3, "e":Landroid/content/res/Resources$NotFoundException;
    :try_start_2
    invoke-virtual {v1, v5}, Landroid/content/pm/ComponentInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v4

    .line 378
    if-nez v4, :cond_1

    .line 379
    invoke-virtual {v1, v5}, Landroid/content/pm/ComponentInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    goto :goto_2

    .line 378
    .end local v3    # "e":Landroid/content/res/Resources$NotFoundException;
    :catchall_0
    move-exception v6

    if-nez v4, :cond_3

    .line 379
    invoke-virtual {v1, v5}, Landroid/content/pm/ComponentInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    :cond_3
    throw v6
.end method

.method private getImageView(I)I
    .locals 1
    .param p1, "num"    # I

    .prologue
    .line 423
    packed-switch p1, :pswitch_data_0

    .line 440
    const v0, 0x7f0b0002

    .line 444
    .local v0, "icon":I
    :goto_0
    return v0

    .line 425
    .end local v0    # "icon":I
    :pswitch_0
    const v0, 0x7f0b0002

    .line 426
    .restart local v0    # "icon":I
    goto :goto_0

    .line 428
    .end local v0    # "icon":I
    :pswitch_1
    const v0, 0x7f0b0003

    .line 429
    .restart local v0    # "icon":I
    goto :goto_0

    .line 431
    .end local v0    # "icon":I
    :pswitch_2
    const v0, 0x7f0b0004

    .line 432
    .restart local v0    # "icon":I
    goto :goto_0

    .line 434
    .end local v0    # "icon":I
    :pswitch_3
    const v0, 0x7f0b0006

    .line 435
    .restart local v0    # "icon":I
    goto :goto_0

    .line 437
    .end local v0    # "icon":I
    :pswitch_4
    const v0, 0x7f0b0008

    .line 438
    .restart local v0    # "icon":I
    goto :goto_0

    .line 423
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private getTitle(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 530
    const/4 v2, 0x0

    .line 531
    .local v2, "title":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 533
    .local v0, "pm":Landroid/content/pm/PackageManager;
    const/4 v3, 0x0

    invoke-virtual {v0, p2, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 534
    .local v1, "resolveInfo":Landroid/content/pm/ResolveInfo;
    if-eqz v1, :cond_0

    .line 535
    invoke-direct {p0, p1, v1}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->getTitle(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)Ljava/lang/String;

    move-result-object v2

    .line 537
    :cond_0
    return-object v2
.end method

.method private getTitle(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)Ljava/lang/String;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "info"    # Landroid/content/pm/ResolveInfo;

    .prologue
    .line 541
    const/4 v8, 0x0

    .line 542
    .local v8, "title":Ljava/lang/String;
    iget-object v9, p2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-nez v9, :cond_2

    iget-object v1, p2, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    .line 543
    .local v1, "ci":Landroid/content/pm/ComponentInfo;
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 545
    .local v6, "pm":Landroid/content/pm/PackageManager;
    const/4 v4, 0x0

    .line 546
    .local v4, "labelId":I
    iget-object v3, p2, Landroid/content/pm/ResolveInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    .line 547
    .local v3, "label":Ljava/lang/CharSequence;
    if-nez v3, :cond_0

    .line 548
    iget v4, p2, Landroid/content/pm/ResolveInfo;->labelRes:I

    .line 549
    if-nez v4, :cond_0

    .line 550
    iget-object v3, v1, Landroid/content/pm/ComponentInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    .line 551
    if-nez v3, :cond_0

    .line 552
    iget v4, v1, Landroid/content/pm/ComponentInfo;->labelRes:I

    .line 553
    if-nez v4, :cond_0

    .line 554
    iget-object v9, v1, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v9, Landroid/content/pm/ApplicationInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    .line 555
    if-nez v3, :cond_0

    .line 556
    iget-object v9, v1, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v4, v9, Landroid/content/pm/ApplicationInfo;->labelRes:I

    .line 562
    :cond_0
    if-eqz v3, :cond_3

    .line 563
    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    .line 590
    :cond_1
    :goto_1
    return-object v8

    .line 542
    .end local v1    # "ci":Landroid/content/pm/ComponentInfo;
    .end local v3    # "label":Ljava/lang/CharSequence;
    .end local v4    # "labelId":I
    .end local v6    # "pm":Landroid/content/pm/PackageManager;
    :cond_2
    iget-object v1, p2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    goto :goto_0

    .line 564
    .restart local v1    # "ci":Landroid/content/pm/ComponentInfo;
    .restart local v3    # "label":Ljava/lang/CharSequence;
    .restart local v4    # "labelId":I
    .restart local v6    # "pm":Landroid/content/pm/PackageManager;
    :cond_3
    if-eqz v4, :cond_1

    .line 565
    iget-object v10, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->mPkgCache:Ljava/util/HashMap;

    monitor-enter v10

    .line 567
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->mPkgCache:Ljava/util/HashMap;

    iget-object v11, v1, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v9, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti$PkgEntry;

    .line 568
    .local v5, "pkgEntry":Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti$PkgEntry;
    if-eqz v5, :cond_5

    .line 569
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti$PkgEntry;->mStrings:Landroid/util/SparseArray;
    invoke-static {v5}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti$PkgEntry;->access$000(Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti$PkgEntry;)Landroid/util/SparseArray;

    move-result-object v9

    invoke-virtual {v9, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Ljava/lang/String;

    move-object v8, v0

    .line 574
    :goto_2
    iget-object v9, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->nullString:Ljava/lang/String;

    if-ne v8, v9, :cond_6

    .line 575
    const/4 v8, 0x0

    .line 588
    :cond_4
    :goto_3
    monitor-exit v10

    goto :goto_1

    .end local v5    # "pkgEntry":Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti$PkgEntry;
    :catchall_0
    move-exception v9

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v9

    .line 571
    .restart local v5    # "pkgEntry":Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti$PkgEntry;
    :cond_5
    :try_start_1
    new-instance v5, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti$PkgEntry;

    .end local v5    # "pkgEntry":Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti$PkgEntry;
    const/4 v9, 0x0

    invoke-direct {v5, v9}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti$PkgEntry;-><init>(Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti$1;)V

    .line 572
    .restart local v5    # "pkgEntry":Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti$PkgEntry;
    iget-object v9, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->mPkgCache:Ljava/util/HashMap;

    iget-object v11, v1, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v9, v11, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 577
    :cond_6
    if-nez v8, :cond_4

    .line 579
    :try_start_2
    iget-object v9, v1, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v6, v9}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;

    move-result-object v7

    .line 580
    .local v7, "res":Landroid/content/res/Resources;
    iget v9, v1, Landroid/content/pm/ComponentInfo;->labelRes:I

    if-eqz v9, :cond_7

    iget-object v9, v1, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    :goto_4
    invoke-direct {p0, p1, v7, v4, v9}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->loadString(Landroid/content/Context;Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 581
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti$PkgEntry;->mStrings:Landroid/util/SparseArray;
    invoke-static {v5}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti$PkgEntry;->access$000(Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti$PkgEntry;)Landroid/util/SparseArray;

    move-result-object v11

    if-nez v8, :cond_8

    iget-object v9, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->nullString:Ljava/lang/String;

    :goto_5
    invoke-virtual {v11, v4, v9}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 582
    .end local v7    # "res":Landroid/content/res/Resources;
    :catch_0
    move-exception v2

    .line 585
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_3
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti$PkgEntry;->mStrings:Landroid/util/SparseArray;
    invoke-static {v5}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti$PkgEntry;->access$000(Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti$PkgEntry;)Landroid/util/SparseArray;

    move-result-object v9

    iget-object v11, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->nullString:Ljava/lang/String;

    invoke-virtual {v9, v4, v11}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 580
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v7    # "res":Landroid/content/res/Resources;
    :cond_7
    :try_start_4
    iget-object v9, v1, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_4

    :cond_8
    move-object v9, v8

    .line 581
    goto :goto_5
.end method

.method private loadString(Landroid/content/Context;Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "res"    # Landroid/content/res/Resources;
    .param p3, "resId"    # I
    .param p4, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 595
    const/4 v3, 0x0

    .line 596
    .local v3, "text":Ljava/lang/CharSequence;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 606
    .local v2, "pm":Landroid/content/pm/PackageManager;
    if-nez v3, :cond_0

    .line 607
    :try_start_0
    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 609
    :cond_0
    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 613
    .local v1, "newString":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 610
    .end local v1    # "newString":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 611
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    const/4 v1, 0x0

    .restart local v1    # "newString":Ljava/lang/String;
    goto :goto_0
.end method

.method private notificationClickEvent(Landroid/content/Context;Landroid/content/pm/PackageManager;Landroid/content/Intent;ILandroid/widget/RemoteViews;)Landroid/widget/RemoteViews;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pm"    # Landroid/content/pm/PackageManager;
    .param p3, "intent"    # Landroid/content/Intent;
    .param p4, "num"    # I
    .param p5, "notiViews"    # Landroid/widget/RemoteViews;

    .prologue
    const/4 v5, 0x0

    .line 322
    invoke-direct {p0, p4}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->getImageView(I)I

    move-result v1

    .line 324
    .local v1, "iIcon":I
    if-eqz p3, :cond_1

    .line 325
    invoke-virtual {p2, p3, v5}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    .line 327
    .local v3, "rInfo":Landroid/content/pm/ResolveInfo;
    const/high16 v4, 0x8000000

    invoke-static {p1, v5, p3, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 329
    .local v2, "pIntent":Landroid/app/PendingIntent;
    if-eqz v3, :cond_0

    .line 331
    invoke-direct {p0, p1, p3}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->getIcon(Landroid/content/Context;Landroid/content/Intent;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->drawableToBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 333
    .local v0, "bIcon":Landroid/graphics/Bitmap;
    invoke-virtual {p5, v1, v0}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 334
    invoke-virtual {p5, v1, v2}, Landroid/widget/RemoteViews;->setLaunchPendingIntent(ILandroid/app/PendingIntent;)V

    .line 335
    invoke-direct {p0, p1, p3}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->getTitle(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p5, v1, v4}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 343
    .end local v0    # "bIcon":Landroid/graphics/Bitmap;
    .end local v2    # "pIntent":Landroid/app/PendingIntent;
    .end local v3    # "rInfo":Landroid/content/pm/ResolveInfo;
    :cond_0
    :goto_0
    const/4 p2, 0x0

    .line 345
    return-object p5

    .line 340
    :cond_1
    const-string v4, "ContextualPageNotification"

    const-string v5, "notificationClickEvent : intent is NULL!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private notificationCreate(Landroid/content/Context;III)Landroid/widget/RemoteViews;
    .locals 20
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cpType"    # I
    .param p3, "icon"    # I
    .param p4, "title"    # I

    .prologue
    .line 213
    new-instance v7, Landroid/widget/RemoteViews;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/high16 v3, 0x7f030000

    invoke-direct {v7, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 215
    .local v7, "notiViews":Landroid/widget/RemoteViews;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 216
    .local v4, "pm":Landroid/content/pm/PackageManager;
    const/16 v16, 0x0

    .line 218
    .local v16, "packageName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->mCpMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->getHotseatItems(I)Ljava/util/ArrayList;

    move-result-object v13

    .line 219
    .local v13, "hotseatItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;>;"
    const v2, 0x7f0b0001

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v17, 0x7f090014

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    move-object/from16 v0, v17

    invoke-static {v3, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 223
    const v2, 0x7f0b0009

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->getContextualIcon(I)I

    move-result v3

    invoke-virtual {v7, v2, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 225
    const/4 v12, 0x0

    .line 226
    .local v12, "edit_value":Ljava/lang/String;
    packed-switch p2, :pswitch_data_0

    .line 237
    :goto_0
    new-instance v10, Landroid/content/Intent;

    const-string v2, "android.settings.CALL_RECOMMENDED_APPS_LIST"

    invoke-direct {v10, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 238
    .local v10, "edit_intent":Landroid/content/Intent;
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 239
    .local v9, "edit_bundle":Landroid/os/Bundle;
    const-string v2, "edit_value"

    invoke-virtual {v9, v2, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    const-string v2, "edit_direct"

    const/4 v3, 0x1

    invoke-virtual {v9, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 241
    invoke-virtual {v10, v9}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 242
    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    move-object/from16 v0, p1

    invoke-static {v0, v2, v10, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v11

    .line 243
    .local v11, "edit_pIntent":Landroid/app/PendingIntent;
    const v2, 0x7f0b000f

    invoke-virtual {v7, v2, v11}, Landroid/widget/RemoteViews;->setLaunchPendingIntent(ILandroid/app/PendingIntent;)V

    .line 245
    const/4 v6, 0x0

    .line 246
    .local v6, "i":I
    const/4 v6, 0x0

    :goto_1
    const/4 v2, 0x5

    if-ge v6, v2, :cond_0

    .line 247
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->getImageView(I)I

    move-result v2

    const/16 v3, 0x8

    invoke-virtual {v7, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 246
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 228
    .end local v6    # "i":I
    .end local v9    # "edit_bundle":Landroid/os/Bundle;
    .end local v10    # "edit_intent":Landroid/content/Intent;
    .end local v11    # "edit_pIntent":Landroid/app/PendingIntent;
    :pswitch_0
    const-string v12, "earphones"

    .line 229
    goto :goto_0

    .line 231
    :pswitch_1
    const-string v12, "docking"

    .line 232
    goto :goto_0

    .line 234
    :pswitch_2
    const-string v12, "roaming"

    goto :goto_0

    .line 252
    .restart local v6    # "i":I
    .restart local v9    # "edit_bundle":Landroid/os/Bundle;
    .restart local v10    # "edit_intent":Landroid/content/Intent;
    .restart local v11    # "edit_pIntent":Landroid/app/PendingIntent;
    :cond_0
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v2

    sget v3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->MAX_RECOMMENDED_APPS:I

    if-le v2, v3, :cond_2

    sget v15, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->MAX_RECOMMENDED_APPS:I

    .line 253
    .local v15, "item_count":I
    :goto_2
    const/4 v6, 0x0

    :goto_3
    if-ge v6, v15, :cond_3

    .line 254
    invoke-virtual {v13, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;

    iget-object v0, v2, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;->packageName:Ljava/lang/String;

    move-object/from16 v16, v0

    .line 255
    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v14

    .line 256
    .local v14, "intent":Landroid/content/Intent;
    new-instance v8, Landroid/content/ComponentName;

    invoke-virtual {v13, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;

    iget-object v3, v2, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;->packageName:Ljava/lang/String;

    invoke-virtual {v13, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;

    iget-object v2, v2, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;->className:Ljava/lang/String;

    invoke-direct {v8, v3, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    .local v8, "cpName":Landroid/content/ComponentName;
    if-eqz v14, :cond_1

    .line 259
    new-instance v5, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v5, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 260
    .local v5, "launch":Landroid/content/Intent;
    const-string v2, "android.intent.category.LAUNCHER"

    invoke-virtual {v5, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 261
    invoke-virtual {v5, v8}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 263
    const/high16 v2, 0x10220000

    invoke-virtual {v14, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    .line 265
    invoke-direct/range {v2 .. v7}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->notificationClickEvent(Landroid/content/Context;Landroid/content/pm/PackageManager;Landroid/content/Intent;ILandroid/widget/RemoteViews;)Landroid/widget/RemoteViews;

    move-result-object v7

    .line 266
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->getImageView(I)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v7, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 253
    .end local v5    # "launch":Landroid/content/Intent;
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 252
    .end local v8    # "cpName":Landroid/content/ComponentName;
    .end local v14    # "intent":Landroid/content/Intent;
    .end local v15    # "item_count":I
    :cond_2
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v15

    goto :goto_2

    .line 282
    .restart local v15    # "item_count":I
    :cond_3
    return-object v7

    .line 226
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private resetAllNotification(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 476
    const-string v2, "notification"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 478
    .local v1, "notiManager":Landroid/app/NotificationManager;
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x4

    if-gt v0, v2, :cond_0

    .line 479
    const v2, 0x7f020014

    add-int/2addr v2, v0

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 478
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 481
    :cond_0
    sget-object v2, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->notiIdList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 482
    const/4 v2, -0x1

    sput v2, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->lastNotiType:I

    .line 483
    const-string v2, "ContextualPageNotification"

    const-string v3, "resetAllNotification : all clear!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    return-void
.end method

.method private setIconDpi(Landroid/content/Context;)I
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v2, 0xf0

    .line 386
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 387
    .local v0, "appRes":Landroid/content/res/Resources;
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v4

    .line 389
    .local v4, "sysRes":Landroid/content/res/Resources;
    if-nez v0, :cond_0

    .line 417
    :goto_0
    return v2

    .line 392
    :cond_0
    const v7, 0x7f080008

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 393
    .local v6, "targetIconSize":I
    const/high16 v7, 0x1050000

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 394
    .local v3, "stdIconSize":I
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 396
    .local v1, "dm":Landroid/util/DisplayMetrics;
    if-ne v6, v3, :cond_1

    .line 399
    iget v2, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    .local v2, "iconDpi":I
    goto :goto_0

    .line 404
    .end local v2    # "iconDpi":I
    :cond_1
    int-to-float v7, v6

    int-to-float v8, v3

    div-float/2addr v7, v8

    iget v8, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v8, v8

    mul-float/2addr v7, v8

    float-to-int v5, v7

    .line 405
    .local v5, "targetDPI":I
    const/16 v7, 0x78

    if-gt v5, v7, :cond_2

    .line 406
    const/16 v2, 0x78

    .restart local v2    # "iconDpi":I
    goto :goto_0

    .line 407
    .end local v2    # "iconDpi":I
    :cond_2
    const/16 v7, 0xa0

    if-gt v5, v7, :cond_3

    .line 408
    const/16 v2, 0xa0

    .restart local v2    # "iconDpi":I
    goto :goto_0

    .line 409
    .end local v2    # "iconDpi":I
    :cond_3
    if-gt v5, v2, :cond_4

    .line 410
    const/16 v2, 0xf0

    .restart local v2    # "iconDpi":I
    goto :goto_0

    .line 411
    .end local v2    # "iconDpi":I
    :cond_4
    const/16 v7, 0x140

    if-gt v5, v7, :cond_5

    .line 412
    const/16 v2, 0x140

    .restart local v2    # "iconDpi":I
    goto :goto_0

    .line 414
    .end local v2    # "iconDpi":I
    :cond_5
    const/16 v2, 0x1e0

    .restart local v2    # "iconDpi":I
    goto :goto_0
.end method


# virtual methods
.method protected getLastNotificationType()I
    .locals 1

    .prologue
    .line 95
    sget v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->lastNotiType:I

    return v0
.end method

.method public notificationClear(Landroid/content/Context;II)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "notiID"    # I
    .param p3, "nextCpType"    # I

    .prologue
    const/4 v8, 0x1

    const v7, 0x7f020014

    const/4 v6, 0x0

    .line 487
    const-string v3, "notification"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    .line 489
    .local v2, "notiManager":Landroid/app/NotificationManager;
    invoke-virtual {v2, p2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 491
    sget-object v3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->notiIdList:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 492
    sget-object v3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->notiIdList:Ljava/util/ArrayList;

    sget-object v4, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->notiIdList:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 494
    :cond_0
    if-eqz p3, :cond_3

    .line 495
    sget v3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->lastNotiType:I

    if-eq v3, p3, :cond_2

    .line 496
    sget v3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->lastNotiType:I

    sub-int v4, p2, v7

    if-eq v3, v4, :cond_1

    .line 497
    sget v3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->lastNotiType:I

    invoke-virtual {p0, p1, v3, v6, v6}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->notificationSend(Landroid/content/Context;IZZ)V

    .line 499
    :cond_1
    sput p3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->lastNotiType:I

    .line 500
    invoke-virtual {p0, p1, p3, v8, v6}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->notificationSend(Landroid/content/Context;IZZ)V

    .line 502
    const-string v3, "ContextualPageNotification"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "notificationClear : move to cpType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 526
    :cond_2
    :goto_0
    const/4 v2, 0x0

    .line 527
    return-void

    .line 506
    :cond_3
    sget v3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->lastNotiType:I

    sub-int v4, p2, v7

    if-ne v3, v4, :cond_2

    .line 507
    const/4 v0, -0x1

    .line 508
    .local v0, "cpType":I
    const/4 v1, 0x4

    .local v1, "i":I
    :goto_1
    if-lez v1, :cond_5

    .line 509
    sub-int v3, p2, v7

    if-eq v1, v3, :cond_4

    sget-object v3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->notiIdList:Ljava/util/ArrayList;

    add-int v4, v1, v7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 510
    move v0, v1

    .line 508
    :cond_4
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 513
    :cond_5
    const/4 v3, -0x1

    if-eq v0, v3, :cond_6

    .line 514
    sput v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->lastNotiType:I

    .line 515
    invoke-virtual {p0, p1, v0, v8, v6}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->notificationSend(Landroid/content/Context;IZZ)V

    .line 517
    const-string v3, "ContextualPageNotification"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "notificationClear : move to cpType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 520
    :cond_6
    invoke-direct {p0, p1}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->resetAllNotification(Landroid/content/Context;)V

    .line 522
    const-string v3, "ContextualPageNotification"

    const-string v4, "notificationClear : cpType all clear"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected notificationSend(Landroid/content/Context;IZZ)V
    .locals 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cpType"    # I
    .param p3, "bVisible"    # Z
    .param p4, "isConfigChange"    # Z

    .prologue
    .line 103
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget-object v13, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 104
    .local v13, "locale":Ljava/util/Locale;
    invoke-virtual {v13}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v12

    .line 106
    .local v12, "lancode":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Landroid/os/PersonaManager;->isKioskModeEnabled(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    const v5, 0x7f020014

    add-int v14, v5, p2

    .line 112
    .local v14, "notiID":I
    const-string v5, "ContextualPageNotification"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "notificationSend cpType : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p2

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Visible = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p3

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    const-string v5, "notification"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/app/NotificationManager;

    .line 116
    .local v15, "notiManager":Landroid/app/NotificationManager;
    const/16 v19, -0x1

    .line 117
    .local v19, "tickerIcon":I
    const/16 v20, -0x1

    .line 118
    .local v20, "tickerTitle":I
    const/4 v11, -0x1

    .line 119
    .local v11, "icon":I
    const/16 v21, -0x1

    .line 120
    .local v21, "title":I
    packed-switch p2, :pswitch_data_0

    .line 146
    const v19, 0x7f020015

    .line 147
    const v20, 0x7f09000b

    .line 148
    const v11, 0x7f020007

    .line 149
    const v21, 0x7f090003

    .line 153
    :goto_1
    new-instance v10, Landroid/app/Notification$Builder;

    move-object/from16 v0, p1

    invoke-direct {v10, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 154
    .local v10, "builder":Landroid/app/Notification$Builder;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10, v5}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 155
    move/from16 v0, v19

    invoke-virtual {v10, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 156
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v10, v6, v7}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 157
    invoke-virtual {v10}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v16

    .line 159
    .local v16, "notify":Landroid/app/Notification;
    const/16 v18, 0x0

    .line 160
    .local v18, "shouldBeGone":Z
    const/4 v4, 0x0

    .line 161
    .local v4, "remoteViews":Landroid/widget/RemoteViews;
    if-eqz p3, :cond_2

    const/4 v5, 0x1

    move/from16 v0, p2

    if-ne v0, v5, :cond_4

    .line 162
    :cond_2
    new-instance v4, Landroid/widget/RemoteViews;

    .end local v4    # "remoteViews":Landroid/widget/RemoteViews;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const/high16 v6, 0x7f030000

    invoke-direct {v4, v5, v6}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 163
    .restart local v4    # "remoteViews":Landroid/widget/RemoteViews;
    const/high16 v5, 0x7f0b0000

    const/16 v6, 0x8

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 164
    const v5, -0x7f7f7f80

    move-object/from16 v0, v16

    iput v5, v0, Landroid/app/Notification;->commonValue:I

    .line 165
    const/16 v18, 0x1

    .line 194
    :goto_2
    move-object/from16 v0, v16

    iput-object v4, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 195
    sget-object v5, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->mLaunchIntent:Landroid/app/PendingIntent;

    move-object/from16 v0, v16

    iput-object v5, v0, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 196
    const/16 v5, 0x10

    move-object/from16 v0, v16

    iput v5, v0, Landroid/app/Notification;->twQuickPanelEvent:I

    .line 197
    move-object/from16 v0, v16

    iget v5, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v5, v5, 0x2

    move-object/from16 v0, v16

    iput v5, v0, Landroid/app/Notification;->flags:I

    .line 198
    const/4 v5, -0x1

    move-object/from16 v0, v16

    iput v5, v0, Landroid/app/Notification;->priority:I

    .line 200
    move-object/from16 v0, v16

    invoke-virtual {v15, v14, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 202
    const/4 v15, 0x0

    .line 203
    const/16 v16, 0x0

    .line 205
    if-eqz v18, :cond_3

    .line 206
    const/4 v4, 0x0

    .line 208
    :cond_3
    if-nez v18, :cond_0

    sget-object v5, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->notiIdList:Ljava/util/ArrayList;

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 209
    sget-object v5, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->notiIdList:Ljava/util/ArrayList;

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 122
    .end local v4    # "remoteViews":Landroid/widget/RemoteViews;
    .end local v10    # "builder":Landroid/app/Notification$Builder;
    .end local v16    # "notify":Landroid/app/Notification;
    .end local v18    # "shouldBeGone":Z
    :pswitch_0
    const v19, 0x7f020014

    .line 123
    const v20, 0x7f090009

    .line 124
    const v11, 0x7f02000e

    .line 125
    const v21, 0x7f090002

    .line 126
    goto/16 :goto_1

    .line 128
    :pswitch_1
    const v19, 0x7f020015

    .line 129
    const v20, 0x7f09000b

    .line 130
    const v11, 0x7f020007

    .line 131
    const v21, 0x7f090003

    .line 132
    goto/16 :goto_1

    .line 134
    :pswitch_2
    const v19, 0x7f020012

    .line 135
    const v20, 0x7f090013

    .line 136
    const v11, 0x7f020006

    .line 137
    const v21, 0x7f090008

    .line 138
    goto/16 :goto_1

    .line 167
    .restart local v4    # "remoteViews":Landroid/widget/RemoteViews;
    .restart local v10    # "builder":Landroid/app/Notification$Builder;
    .restart local v16    # "notify":Landroid/app/Notification;
    .restart local v18    # "shouldBeGone":Z
    :cond_4
    if-eqz p4, :cond_8

    .line 168
    sget-object v5, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->remoteViewList:Ljava/util/HashMap;

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "remoteViews":Landroid/widget/RemoteViews;
    check-cast v4, Landroid/widget/RemoteViews;

    .line 169
    .restart local v4    # "remoteViews":Landroid/widget/RemoteViews;
    if-nez v4, :cond_5

    .line 170
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, v21

    invoke-direct {v0, v1, v2, v11, v3}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->notificationCreate(Landroid/content/Context;III)Landroid/widget/RemoteViews;

    move-result-object v4

    .line 171
    const/high16 v5, 0x7f0b0000

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 172
    sget-object v5, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->remoteViewList:Ljava/util/HashMap;

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 174
    :cond_5
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080015

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 175
    .local v17, "padding_icon_right":I
    const-string v5, "ar"

    invoke-virtual {v5, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 176
    const v5, 0x7f0b000a

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    .line 179
    :goto_3
    const v5, 0x7f0b000b

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    move/from16 v8, v17

    invoke-virtual/range {v4 .. v9}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    .line 180
    const v5, 0x7f0b000c

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    move/from16 v8, v17

    invoke-virtual/range {v4 .. v9}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    .line 181
    const v5, 0x7f0b000d

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    move/from16 v8, v17

    invoke-virtual/range {v4 .. v9}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    .line 182
    const-string v5, "ar"

    invoke-virtual {v5, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 183
    const v5, 0x7f0b000e

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    move/from16 v8, v17

    invoke-virtual/range {v4 .. v9}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    goto/16 :goto_2

    .line 178
    :cond_6
    const v5, 0x7f0b000a

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    move/from16 v8, v17

    invoke-virtual/range {v4 .. v9}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    goto :goto_3

    .line 185
    :cond_7
    const v5, 0x7f0b000e

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    goto/16 :goto_2

    .line 188
    .end local v17    # "padding_icon_right":I
    :cond_8
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, v21

    invoke-direct {v0, v1, v2, v11, v3}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->notificationCreate(Landroid/content/Context;III)Landroid/widget/RemoteViews;

    move-result-object v4

    .line 189
    const/high16 v5, 0x7f0b0000

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 190
    sget-object v5, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->remoteViewList:Ljava/util/HashMap;

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 120
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected setLastNotificationType(I)V
    .locals 0
    .param p1, "cpType"    # I

    .prologue
    .line 98
    sput p1, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->lastNotiType:I

    .line 99
    return-void
.end method
