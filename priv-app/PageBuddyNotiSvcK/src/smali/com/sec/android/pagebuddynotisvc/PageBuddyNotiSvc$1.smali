.class final Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc$1;
.super Landroid/database/ContentObserver;
.source "PageBuddyNotiSvc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Landroid/os/Handler;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Handler;

    .prologue
    .line 127
    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 7
    .param p1, "selfChange"    # Z

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x3

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 129
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mResolver:Landroid/content/ContentResolver;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$100()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "recommended_apps_setting"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    # setter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isRecommendedAppsEnable:I
    invoke-static {v0}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$002(I)I

    .line 133
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$200()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 134
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isRecommendedAppsEnable:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$000()I

    move-result v0

    if-ne v0, v2, :cond_4

    .line 135
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v0

    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$300()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->setupContextualAwareHotSeat(Landroid/content/Context;I)V

    .line 136
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->addContextualPageNotification(I)V

    .line 141
    :cond_0
    :goto_0
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$200()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 142
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isRecommendedAppsEnable:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$000()I

    move-result v0

    if-ne v0, v2, :cond_5

    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$200()I

    move-result v0

    and-int/lit8 v0, v0, 0x8

    if-eq v0, v6, :cond_5

    .line 144
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v0

    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$300()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->setupContextualAwareHotSeatForManual(Landroid/content/Context;I)V

    .line 145
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->addContextualPageNotification(I)V

    .line 149
    :cond_1
    :goto_1
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$200()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 150
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isRecommendedAppsEnable:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$000()I

    move-result v0

    if-ne v0, v2, :cond_6

    .line 156
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v0

    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$300()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->setupContextualAwareHotSeatForManual(Landroid/content/Context;I)V

    .line 157
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->addContextualPageNotification(I)V

    .line 162
    :cond_2
    :goto_2
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$200()I

    move-result v0

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_3

    .line 163
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isRecommendedAppsEnable:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$000()I

    move-result v0

    if-ne v0, v2, :cond_8

    .line 164
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->removeContextualPageNotification(I)V

    .line 165
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isRecommendedAppsAutoRoaming:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$500()I

    move-result v0

    if-ne v0, v2, :cond_7

    .line 166
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v0

    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$300()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->setupContextualAwareHotSeat(Landroid/content/Context;I)V

    .line 170
    :goto_3
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->addContextualPageNotification(I)V

    .line 175
    :cond_3
    :goto_4
    return-void

    .line 139
    :cond_4
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->removeContextualPageNotification(I)V

    goto :goto_0

    .line 147
    :cond_5
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->removeContextualPageNotification(I)V

    goto :goto_1

    .line 160
    :cond_6
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->removeContextualPageNotification(I)V

    goto :goto_2

    .line 168
    :cond_7
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v0

    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$300()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->setupContextualAwareHotSeatForManual(Landroid/content/Context;I)V

    goto :goto_3

    .line 173
    :cond_8
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->removeContextualPageNotification(I)V

    goto :goto_4
.end method
