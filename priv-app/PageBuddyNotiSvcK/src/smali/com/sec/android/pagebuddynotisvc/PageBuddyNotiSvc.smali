.class public Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;
.super Landroid/app/Service;
.source "PageBuddyNotiSvc.java"


# static fields
.field private static isRecommendedAppsAutoDockings:I

.field private static isRecommendedAppsAutoRoaming:I

.field private static isRecommendedAppsEnable:I

.field private static knoxMode:I

.field private static mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

.field private static mContext:Landroid/content/Context;

.field private static mCpBitFlag:I

.field private static mResolver:Landroid/content/ContentResolver;

.field private static final mSettingObserver:Landroid/database/ContentObserver;


# instance fields
.field private final ACTION_DOC:Ljava/lang/String;

.field private final ACTION_EARPHONE:Ljava/lang/String;

.field private final ACTION_LOCAL:Ljava/lang/String;

.field private final ACTION_ROAMING:Ljava/lang/String;

.field private final ACTION_SPEN:Ljava/lang/String;

.field mCPBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mPrefs:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 59
    sput v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isRecommendedAppsEnable:I

    .line 60
    sput v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isRecommendedAppsAutoDockings:I

    .line 61
    sput v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isRecommendedAppsAutoRoaming:I

    .line 72
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->knoxMode:I

    .line 127
    new-instance v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, v1}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc$1;-><init>(Landroid/os/Handler;)V

    sput-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mSettingObserver:Landroid/database/ContentObserver;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 53
    const-string v0, "com.samsung.pen.INSERT"

    iput-object v0, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->ACTION_SPEN:Ljava/lang/String;

    .line 54
    const-string v0, "com.sec.android.contextaware.HEADSET_PLUG"

    iput-object v0, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->ACTION_EARPHONE:Ljava/lang/String;

    .line 55
    const-string v0, "android.intent.action.DOCK_EVENT"

    iput-object v0, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->ACTION_DOC:Ljava/lang/String;

    .line 56
    const-string v0, "android.intent.action.SERVICE_STATE"

    iput-object v0, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->ACTION_ROAMING:Ljava/lang/String;

    .line 57
    const-string v0, "android.intent.action.LOCALE_CHANGED"

    iput-object v0, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->ACTION_LOCAL:Ljava/lang/String;

    .line 199
    new-instance v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc$2;

    invoke-direct {v0, p0}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc$2;-><init>(Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;)V

    iput-object v0, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 40
    sget v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isRecommendedAppsEnable:I

    return v0
.end method

.method static synthetic access$002(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 40
    sput p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isRecommendedAppsEnable:I

    return p0
.end method

.method static synthetic access$100()Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 40
    sget v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I

    return v0
.end method

.method static synthetic access$272(I)I
    .locals 1
    .param p0, "x0"    # I

    .prologue
    .line 40
    sget v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I

    and-int/2addr v0, p0

    sput v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I

    return v0
.end method

.method static synthetic access$276(I)I
    .locals 1
    .param p0, "x0"    # I

    .prologue
    .line 40
    sget v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I

    or-int/2addr v0, p0

    sput v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I

    return v0
.end method

.method static synthetic access$300()Landroid/content/Context;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    return-object v0
.end method

.method static synthetic access$500()I
    .locals 1

    .prologue
    .line 40
    sget v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isRecommendedAppsAutoRoaming:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;Landroid/content/SharedPreferences;)Landroid/content/SharedPreferences;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;
    .param p1, "x1"    # Landroid/content/SharedPreferences;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mPrefs:Landroid/content/SharedPreferences;

    return-object p1
.end method


# virtual methods
.method public isKnoxMode()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 180
    sget v4, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->knoxMode:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    .line 181
    sput v3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->knoxMode:I

    .line 182
    const/4 v0, 0x0

    .line 184
    .local v0, "bundle":Landroid/os/Bundle;
    :try_start_0
    const-string v4, "isKnoxMode"

    invoke-static {p0, v4}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 186
    const-string v4, "2.0"

    const-string v5, "version"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "true"

    const-string v5, "isKnoxMode"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 188
    const/4 v4, 0x1

    sput v4, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->knoxMode:I
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    .line 196
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_0
    :goto_0
    sget v4, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->knoxMode:I

    if-ne v4, v2, :cond_1

    :goto_1
    return v2

    .line 190
    .restart local v0    # "bundle":Landroid/os/Bundle;
    :catch_0
    move-exception v1

    .line 191
    .local v1, "e":Ljava/lang/NoClassDefFoundError;
    const-string v4, "PageBuddyNotiSvc"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "not call android.os.PersonaManager."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 192
    .end local v1    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v1

    .line 193
    .local v1, "e":Ljava/lang/NoSuchMethodError;
    const-string v4, "PageBuddyNotiSvc"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "not call getKnoxInfoForApp."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "e":Ljava/lang/NoSuchMethodError;
    :cond_1
    move v2, v3

    .line 196
    goto :goto_1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 76
    const/4 v0, 0x0

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 357
    invoke-super {p0, p1}, Landroid/app/Service;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 361
    sget-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    invoke-virtual {v0}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->configChangeLastNotification()V

    .line 364
    return-void
.end method

.method public onCreate()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 81
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 83
    const-string v0, "com.sec.android.pagebuddynotisvc.prefs"

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mPrefs:Landroid/content/SharedPreferences;

    .line 84
    iget-object v0, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "cp_bit_flag"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I

    .line 86
    const-string v0, "PageBuddyNotiSvc"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate mCpBitFlag="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    new-instance v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    invoke-direct {v0, p0}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    .line 89
    invoke-virtual {p0}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sput-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mResolver:Landroid/content/ContentResolver;

    .line 90
    invoke-virtual {p0}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mContext:Landroid/content/Context;

    .line 91
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 95
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 96
    iget-object v0, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 97
    sget-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mSettingObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 98
    sget-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mSettingObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 100
    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v4, 0x1

    .line 104
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 105
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.pen.INSERT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 106
    const-string v1, "com.sec.android.contextaware.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 107
    const-string v1, "ALTIUS"

    const-string v2, "KNIGHT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "HERA"

    const-string v2, "KNIGHT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 109
    :cond_0
    const-string v1, "android.intent.action.DOCK_EVENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 110
    :cond_1
    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 113
    iget-object v1, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 116
    sget-object v1, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "recommended_apps_setting"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mSettingObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 119
    sget-object v1, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "recommended_apps_list_earphones"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mSettingObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 120
    sget-object v1, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "recommended_apps_list_dockings"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mSettingObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 124
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v1

    return v1
.end method
