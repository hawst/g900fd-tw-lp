.class public Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
.super Ljava/lang/Object;
.source "PageBuddyNotiMgr.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;
    }
.end annotation


# static fields
.field public static final IN_HOTSEAT_POSITION_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;",
            ">;"
        }
    .end annotation
.end field

.field private static MAX_AWARE_HOTSEAT_CELL_COUNT:I

.field private static final STATUS_DOCK_URI:Landroid/net/Uri;

.field private static final STATUS_EARPHONE_URI:Landroid/net/Uri;

.field private static final STATUS_ROAMING_URI:Landroid/net/Uri;

.field private static final STATUS_SPEN_URI:Landroid/net/Uri;

.field private static awareHotseatItemsMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private static kindCP:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static mCPNotification:Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mHotseatItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-string v0, "content://com.samsung.android.providers.context/app_usage/recent/spen"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->STATUS_SPEN_URI:Landroid/net/Uri;

    .line 62
    const-string v0, "content://com.samsung.android.providers.context/app_usage/recent/earphone"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->STATUS_EARPHONE_URI:Landroid/net/Uri;

    .line 63
    const-string v0, "content://com.samsung.android.providers.context/app_usage/recent/docking"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->STATUS_DOCK_URI:Landroid/net/Uri;

    .line 64
    const-string v0, "content://com.samsung.android.providers.context/app_usage/recent/roaming"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->STATUS_ROAMING_URI:Landroid/net/Uri;

    .line 65
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->awareHotseatItemsMap:Ljava/util/HashMap;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->kindCP:Ljava/util/ArrayList;

    .line 314
    new-instance v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$1;

    invoke-direct {v0}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$1;-><init>()V

    sput-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->IN_HOTSEAT_POSITION_COMPARATOR:Ljava/util/Comparator;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mHotseatItems:Ljava/util/ArrayList;

    .line 72
    iput-object p1, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mContext:Landroid/content/Context;

    .line 73
    new-instance v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;

    invoke-direct {v0, p1, p0}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;-><init>(Landroid/content/Context;Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;)V

    sput-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mCPNotification:Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;

    .line 74
    iget-object v0, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f060000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->MAX_AWARE_HOTSEAT_CELL_COUNT:I

    .line 75
    invoke-direct {p0}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->loadHotseatItem()Z

    .line 76
    sget-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->kindCP:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 77
    return-void
.end method

.method private duplicateCheckOfAwareHotseat(ILjava/util/ArrayList;Ljava/lang/String;)Z
    .locals 2
    .param p1, "cnt"    # I
    .param p3, "className"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 323
    .local p2, "awareHotseatItem":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_1

    .line 324
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;

    iget-object v1, v1, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;->className:Ljava/lang/String;

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 325
    const/4 v1, 0x1

    .line 328
    :goto_1
    return v1

    .line 323
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 328
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private loadHotseatItem()Z
    .locals 18

    .prologue
    .line 137
    const/4 v9, 0x0

    .line 139
    .local v9, "isCSC":Z
    const/4 v11, 0x0

    .line 140
    .local v11, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v12, 0x0

    .line 142
    .local v12, "resParser":Landroid/content/res/XmlResourceParser;
    const/4 v4, 0x0

    .line 143
    .local v4, "cscFileChk":Ljava/io/File;
    const/4 v2, 0x0

    .line 146
    .local v2, "cscFile":Ljava/io/FileReader;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mHotseatItems:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->clear()V

    .line 148
    new-instance v5, Ljava/io/File;

    const-string v14, "/system/csc/default_workspace_pagebuddy_cp.xml"

    invoke-direct {v5, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    .end local v4    # "cscFileChk":Ljava/io/File;
    .local v5, "cscFileChk":Ljava/io/File;
    :try_start_1
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_4

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-lez v14, :cond_4

    .line 150
    new-instance v3, Ljava/io/FileReader;

    const-string v14, "/system/csc/default_workspace_pagebuddy_cp.xml"

    invoke-direct {v3, v14}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_e
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_b
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 151
    .end local v2    # "cscFile":Ljava/io/FileReader;
    .local v3, "cscFile":Ljava/io/FileReader;
    :try_start_2
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v8

    .line 152
    .local v8, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/4 v14, 0x1

    invoke-virtual {v8, v14}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 153
    invoke-virtual {v8}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v11

    .line 155
    invoke-interface {v11, v3}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 156
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_f
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_c
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_2 .. :try_end_2} :catch_9
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 158
    const/4 v9, 0x1

    move-object v2, v3

    .end local v3    # "cscFile":Ljava/io/FileReader;
    .restart local v2    # "cscFile":Ljava/io/FileReader;
    move-object v4, v5

    .line 180
    .end local v5    # "cscFileChk":Ljava/io/File;
    .end local v8    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v4    # "cscFileChk":Ljava/io/File;
    :goto_0
    :try_start_3
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v6

    .line 183
    .local v6, "depth":I
    :cond_0
    :goto_1
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v13

    .local v13, "type":I
    const/4 v14, 0x3

    if-ne v13, v14, :cond_1

    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I
    :try_end_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v14

    if-le v14, v6, :cond_2

    :cond_1
    const/4 v14, 0x1

    if-eq v13, v14, :cond_2

    .line 185
    const/4 v14, 0x1

    if-ne v13, v14, :cond_6

    .line 199
    :cond_2
    const/4 v14, 0x1

    .line 210
    if-eqz v2, :cond_3

    .line 212
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 219
    .end local v6    # "depth":I
    .end local v13    # "type":I
    :cond_3
    :goto_2
    return v14

    .line 160
    .end local v4    # "cscFileChk":Ljava/io/File;
    .restart local v5    # "cscFileChk":Ljava/io/File;
    :cond_4
    :try_start_5
    new-instance v4, Ljava/io/File;

    const-string v14, "/system/csc/default_workspace_cp.xml"

    invoke-direct {v4, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_5
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_5 .. :try_end_5} :catch_e
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_b
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_5 .. :try_end_5} :catch_8
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 162
    .end local v5    # "cscFileChk":Ljava/io/File;
    .restart local v4    # "cscFileChk":Ljava/io/File;
    :try_start_6
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_5

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-lez v14, :cond_5

    .line 163
    new-instance v3, Ljava/io/FileReader;

    const-string v14, "/system/csc/default_workspace_cp.xml"

    invoke-direct {v3, v14}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_6
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_6 .. :try_end_6} :catch_5
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 164
    .end local v2    # "cscFile":Ljava/io/FileReader;
    .restart local v3    # "cscFile":Ljava/io/FileReader;
    :try_start_7
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v8

    .line 165
    .restart local v8    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/4 v14, 0x1

    invoke-virtual {v8, v14}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 166
    invoke-virtual {v8}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v11

    .line 168
    invoke-interface {v11, v3}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 169
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I
    :try_end_7
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_7 .. :try_end_7} :catch_10
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_d
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_7 .. :try_end_7} :catch_a
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 171
    const/4 v9, 0x1

    move-object v2, v3

    .line 172
    .end local v3    # "cscFile":Ljava/io/FileReader;
    .restart local v2    # "cscFile":Ljava/io/FileReader;
    goto :goto_0

    .line 173
    .end local v8    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    :cond_5
    :try_start_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const/high16 v15, 0x7f040000

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v12

    .line 175
    const-string v14, "favorites"

    invoke-static {v12, v14}, Lcom/android/internal/util/XmlUtils;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    .line 176
    move-object v11, v12

    goto :goto_0

    .line 188
    .restart local v6    # "depth":I
    .restart local v13    # "type":I
    :cond_6
    const/4 v14, 0x2

    if-ne v13, v14, :cond_0

    .line 192
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v10

    .line 193
    .local v10, "name":Ljava/lang/String;
    const-string v14, "hotseat"

    invoke-virtual {v14, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 194
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v11, v9}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->loadHotseatItemContainer(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;Z)V

    .line 196
    :cond_7
    const-string v14, "PageBuddyNotiMgr"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Invalid tag <"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "> detected while parsing favorites at line "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getLineNumber()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_8 .. :try_end_8} :catch_5
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_1

    .line 201
    .end local v6    # "depth":I
    .end local v10    # "name":Ljava/lang/String;
    .end local v13    # "type":I
    :catch_0
    move-exception v7

    .line 202
    .local v7, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_3
    :try_start_9
    const-string v14, "PageBuddyNotiMgr"

    const-string v15, "Got exception parsing default_workspace_cp.xml"

    invoke-static {v14, v15, v7}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 210
    if-eqz v2, :cond_8

    .line 212
    :try_start_a
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2

    .line 219
    .end local v7    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :cond_8
    :goto_4
    const/4 v14, 0x0

    goto/16 :goto_2

    .line 213
    .restart local v6    # "depth":I
    .restart local v13    # "type":I
    :catch_1
    move-exception v7

    .line 214
    .local v7, "e":Ljava/lang/Exception;
    const-string v15, "PageBuddyNotiMgr"

    const-string v16, "Got exception parsing default_workspace_cp.xml"

    move-object/from16 v0, v16

    invoke-static {v15, v0, v7}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    .line 213
    .end local v6    # "depth":I
    .end local v13    # "type":I
    .local v7, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_2
    move-exception v7

    .line 214
    .local v7, "e":Ljava/lang/Exception;
    const-string v14, "PageBuddyNotiMgr"

    const-string v15, "Got exception parsing default_workspace_cp.xml"

    invoke-static {v14, v15, v7}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 204
    .end local v7    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v7

    .line 205
    .local v7, "e":Ljava/io/IOException;
    :goto_5
    :try_start_b
    const-string v14, "PageBuddyNotiMgr"

    const-string v15, "Got exception parsing default_workspace_cp.xml"

    invoke-static {v14, v15, v7}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 210
    if-eqz v2, :cond_8

    .line 212
    :try_start_c
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_4

    goto :goto_4

    .line 213
    :catch_4
    move-exception v7

    .line 214
    .local v7, "e":Ljava/lang/Exception;
    const-string v14, "PageBuddyNotiMgr"

    const-string v15, "Got exception parsing default_workspace_cp.xml"

    invoke-static {v14, v15, v7}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 207
    .end local v7    # "e":Ljava/lang/Exception;
    :catch_5
    move-exception v7

    .line 208
    .local v7, "e":Landroid/content/res/Resources$NotFoundException;
    :goto_6
    :try_start_d
    const-string v14, "PageBuddyNotiMgr"

    const-string v15, "Got exception parsing default_workspace_cp.xml"

    invoke-static {v14, v15, v7}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 210
    if-eqz v2, :cond_8

    .line 212
    :try_start_e
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_6

    goto :goto_4

    .line 213
    :catch_6
    move-exception v7

    .line 214
    .local v7, "e":Ljava/lang/Exception;
    const-string v14, "PageBuddyNotiMgr"

    const-string v15, "Got exception parsing default_workspace_cp.xml"

    invoke-static {v14, v15, v7}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 210
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v14

    :goto_7
    if-eqz v2, :cond_9

    .line 212
    :try_start_f
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_7

    .line 215
    :cond_9
    :goto_8
    throw v14

    .line 213
    :catch_7
    move-exception v7

    .line 214
    .restart local v7    # "e":Ljava/lang/Exception;
    const-string v15, "PageBuddyNotiMgr"

    const-string v16, "Got exception parsing default_workspace_cp.xml"

    move-object/from16 v0, v16

    invoke-static {v15, v0, v7}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_8

    .line 210
    .end local v4    # "cscFileChk":Ljava/io/File;
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v5    # "cscFileChk":Ljava/io/File;
    :catchall_1
    move-exception v14

    move-object v4, v5

    .end local v5    # "cscFileChk":Ljava/io/File;
    .restart local v4    # "cscFileChk":Ljava/io/File;
    goto :goto_7

    .end local v2    # "cscFile":Ljava/io/FileReader;
    .end local v4    # "cscFileChk":Ljava/io/File;
    .restart local v3    # "cscFile":Ljava/io/FileReader;
    .restart local v5    # "cscFileChk":Ljava/io/File;
    :catchall_2
    move-exception v14

    move-object v2, v3

    .end local v3    # "cscFile":Ljava/io/FileReader;
    .restart local v2    # "cscFile":Ljava/io/FileReader;
    move-object v4, v5

    .end local v5    # "cscFileChk":Ljava/io/File;
    .restart local v4    # "cscFileChk":Ljava/io/File;
    goto :goto_7

    .end local v2    # "cscFile":Ljava/io/FileReader;
    .restart local v3    # "cscFile":Ljava/io/FileReader;
    :catchall_3
    move-exception v14

    move-object v2, v3

    .end local v3    # "cscFile":Ljava/io/FileReader;
    .restart local v2    # "cscFile":Ljava/io/FileReader;
    goto :goto_7

    .line 207
    .end local v4    # "cscFileChk":Ljava/io/File;
    .restart local v5    # "cscFileChk":Ljava/io/File;
    :catch_8
    move-exception v7

    move-object v4, v5

    .end local v5    # "cscFileChk":Ljava/io/File;
    .restart local v4    # "cscFileChk":Ljava/io/File;
    goto :goto_6

    .end local v2    # "cscFile":Ljava/io/FileReader;
    .end local v4    # "cscFileChk":Ljava/io/File;
    .restart local v3    # "cscFile":Ljava/io/FileReader;
    .restart local v5    # "cscFileChk":Ljava/io/File;
    :catch_9
    move-exception v7

    move-object v2, v3

    .end local v3    # "cscFile":Ljava/io/FileReader;
    .restart local v2    # "cscFile":Ljava/io/FileReader;
    move-object v4, v5

    .end local v5    # "cscFileChk":Ljava/io/File;
    .restart local v4    # "cscFileChk":Ljava/io/File;
    goto :goto_6

    .end local v2    # "cscFile":Ljava/io/FileReader;
    .restart local v3    # "cscFile":Ljava/io/FileReader;
    :catch_a
    move-exception v7

    move-object v2, v3

    .end local v3    # "cscFile":Ljava/io/FileReader;
    .restart local v2    # "cscFile":Ljava/io/FileReader;
    goto :goto_6

    .line 204
    .end local v4    # "cscFileChk":Ljava/io/File;
    .restart local v5    # "cscFileChk":Ljava/io/File;
    :catch_b
    move-exception v7

    move-object v4, v5

    .end local v5    # "cscFileChk":Ljava/io/File;
    .restart local v4    # "cscFileChk":Ljava/io/File;
    goto :goto_5

    .end local v2    # "cscFile":Ljava/io/FileReader;
    .end local v4    # "cscFileChk":Ljava/io/File;
    .restart local v3    # "cscFile":Ljava/io/FileReader;
    .restart local v5    # "cscFileChk":Ljava/io/File;
    :catch_c
    move-exception v7

    move-object v2, v3

    .end local v3    # "cscFile":Ljava/io/FileReader;
    .restart local v2    # "cscFile":Ljava/io/FileReader;
    move-object v4, v5

    .end local v5    # "cscFileChk":Ljava/io/File;
    .restart local v4    # "cscFileChk":Ljava/io/File;
    goto :goto_5

    .end local v2    # "cscFile":Ljava/io/FileReader;
    .restart local v3    # "cscFile":Ljava/io/FileReader;
    :catch_d
    move-exception v7

    move-object v2, v3

    .end local v3    # "cscFile":Ljava/io/FileReader;
    .restart local v2    # "cscFile":Ljava/io/FileReader;
    goto :goto_5

    .line 201
    .end local v4    # "cscFileChk":Ljava/io/File;
    .restart local v5    # "cscFileChk":Ljava/io/File;
    :catch_e
    move-exception v7

    move-object v4, v5

    .end local v5    # "cscFileChk":Ljava/io/File;
    .restart local v4    # "cscFileChk":Ljava/io/File;
    goto/16 :goto_3

    .end local v2    # "cscFile":Ljava/io/FileReader;
    .end local v4    # "cscFileChk":Ljava/io/File;
    .restart local v3    # "cscFile":Ljava/io/FileReader;
    .restart local v5    # "cscFileChk":Ljava/io/File;
    :catch_f
    move-exception v7

    move-object v2, v3

    .end local v3    # "cscFile":Ljava/io/FileReader;
    .restart local v2    # "cscFile":Ljava/io/FileReader;
    move-object v4, v5

    .end local v5    # "cscFileChk":Ljava/io/File;
    .restart local v4    # "cscFileChk":Ljava/io/File;
    goto/16 :goto_3

    .end local v2    # "cscFile":Ljava/io/FileReader;
    .restart local v3    # "cscFile":Ljava/io/FileReader;
    :catch_10
    move-exception v7

    move-object v2, v3

    .end local v3    # "cscFile":Ljava/io/FileReader;
    .restart local v2    # "cscFile":Ljava/io/FileReader;
    goto/16 :goto_3
.end method

.method private loadHotseatItemContainer(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;Z)V
    .locals 14
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p3, "isCSC"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 225
    invoke-static/range {p2 .. p2}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v2

    .line 226
    .local v2, "attrs":Landroid/util/AttributeSet;
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v8

    .line 229
    .local v8, "startDepth":I
    const/4 v6, 0x0

    .line 230
    .local v6, "pkgName":Ljava/lang/String;
    const/4 v3, 0x0

    .line 231
    .local v3, "className":Ljava/lang/String;
    const/4 v7, 0x0

    .line 232
    .local v7, "screen":Ljava/lang/String;
    const/4 v10, 0x0

    .line 234
    .local v10, "x":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v9

    .local v9, "type":I
    const/4 v11, 0x3

    if-ne v9, v11, :cond_1

    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v11

    if-le v11, v8, :cond_2

    .line 235
    :cond_1
    const/4 v11, 0x1

    if-ne v9, v11, :cond_3

    .line 270
    :cond_2
    return-void

    .line 238
    :cond_3
    const/4 v11, 0x2

    if-ne v9, v11, :cond_0

    .line 241
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    .line 245
    .local v5, "name":Ljava/lang/String;
    const/4 v11, 0x1

    move/from16 v0, p3

    if-ne v0, v11, :cond_4

    .line 246
    const/4 v1, 0x0

    .line 247
    .local v1, "a":Landroid/content/res/TypedArray;
    const/4 v11, 0x0

    const-string v12, "packageName"

    move-object/from16 v0, p2

    invoke-interface {v0, v11, v12}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 248
    const/4 v11, 0x0

    const-string v12, "className"

    move-object/from16 v0, p2

    invoke-interface {v0, v11, v12}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 249
    const/4 v11, 0x0

    const-string v12, "screen"

    move-object/from16 v0, p2

    invoke-interface {v0, v11, v12}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 250
    const/4 v11, 0x0

    const-string v12, "x"

    move-object/from16 v0, p2

    invoke-interface {v0, v11, v12}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 260
    :goto_1
    const-string v11, "favorite"

    invoke-virtual {v11, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 261
    new-instance v4, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-direct {v4, v6, v3, v11, v12}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 262
    .local v4, "hotseatItem":Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;
    iget-object v11, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mHotseatItems:Ljava/util/ArrayList;

    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 266
    .end local v4    # "hotseatItem":Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;
    :goto_2
    const/4 v11, 0x1

    move/from16 v0, p3

    if-eq v0, v11, :cond_0

    .line 267
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0

    .line 253
    .end local v1    # "a":Landroid/content/res/TypedArray;
    :cond_4
    sget-object v11, Lcom/sec/android/pagebuddynotisvc/R$styleable;->Favorite:[I

    invoke-virtual {p1, v2, v11}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 254
    .restart local v1    # "a":Landroid/content/res/TypedArray;
    const/4 v11, 0x1

    invoke-virtual {v1, v11}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 255
    const/4 v11, 0x0

    invoke-virtual {v1, v11}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 256
    const/4 v11, 0x2

    invoke-virtual {v1, v11}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 257
    const/4 v11, 0x3

    invoke-virtual {v1, v11}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto :goto_1

    .line 264
    :cond_5
    const-string v11, "PageBuddyNotiMgr"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Invalid tag <"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "> detected while parsing favorites at line "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getLineNumber()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method


# virtual methods
.method protected addContextualPageNotification(I)V
    .locals 4
    .param p1, "cpType"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 96
    if-ne p1, v3, :cond_1

    .line 97
    sget-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mCPNotification:Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;

    iget-object v1, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p1, v2, v2}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->notificationSend(Landroid/content/Context;IZZ)V

    .line 103
    :goto_0
    sget-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mCPNotification:Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;

    invoke-virtual {v0, p1}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->setLastNotificationType(I)V

    .line 105
    sget-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->kindCP:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    sget-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->kindCP:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    :cond_0
    return-void

    .line 99
    :cond_1
    invoke-virtual {p0, v2}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->checkLastNotiType(Z)V

    .line 100
    sget-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mCPNotification:Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;

    iget-object v1, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p1, v3, v2}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->notificationSend(Landroid/content/Context;IZZ)V

    goto :goto_0
.end method

.method protected checkLastNotiType(Z)V
    .locals 4
    .param p1, "isVisible"    # Z

    .prologue
    .line 427
    sget-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mCPNotification:Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;

    invoke-virtual {v0}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->getLastNotificationType()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 428
    sget-object v0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mCPNotification:Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;

    iget-object v1, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mCPNotification:Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;

    invoke-virtual {v2}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->getLastNotificationType()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, p1, v3}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->notificationSend(Landroid/content/Context;IZZ)V

    .line 429
    :cond_0
    return-void
.end method

.method public configChangeLastNotification()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 432
    sget-object v1, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mCPNotification:Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;

    invoke-virtual {v1}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->getLastNotificationType()I

    move-result v0

    .line 434
    .local v0, "type":I
    if-lez v0, :cond_0

    if-ne v0, v3, :cond_1

    .line 438
    :cond_0
    :goto_0
    return-void

    .line 437
    :cond_1
    sget-object v1, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mCPNotification:Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;

    iget-object v2, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v0, v3, v3}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->notificationSend(Landroid/content/Context;IZZ)V

    goto :goto_0
.end method

.method protected getHotseatItems(I)Ljava/util/ArrayList;
    .locals 10
    .param p1, "CPType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 274
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 275
    .local v2, "hotseatItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 277
    .local v1, "awareHotseatItem":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;>;"
    sget-object v7, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->awareHotseatItemsMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "awareHotseatItem":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;>;"
    check-cast v1, Ljava/util/ArrayList;

    .line 278
    .restart local v1    # "awareHotseatItem":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;>;"
    const/4 v4, 0x0

    .line 279
    .local v4, "j":I
    if-nez v1, :cond_0

    move v0, v8

    .line 280
    .local v0, "awareHotseatCnt":I
    :goto_0
    if-lez v0, :cond_1

    .line 282
    sget-object v7, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->IN_HOTSEAT_POSITION_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v1, v7}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 311
    .end local v1    # "awareHotseatItem":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;>;"
    :goto_1
    return-object v1

    .line 279
    .end local v0    # "awareHotseatCnt":I
    .restart local v1    # "awareHotseatItem":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;>;"
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0

    .line 285
    .restart local v0    # "awareHotseatCnt":I
    :cond_1
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    iget-object v7, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mHotseatItems:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v3, v7, :cond_2

    .line 286
    iget-object v7, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mHotseatItems:Ljava/util/ArrayList;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;

    iget v7, v7, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;->cpType:I

    if-ne v7, p1, :cond_4

    .line 287
    if-lez v0, :cond_3

    .line 288
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "j":I
    .local v5, "j":I
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 289
    add-int/lit8 v0, v0, -0x1

    add-int/lit8 v3, v3, -0x1

    .line 291
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v7

    sget v9, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->MAX_AWARE_HOTSEAT_CELL_COUNT:I

    if-lt v7, v9, :cond_7

    move v4, v5

    .line 309
    .end local v5    # "j":I
    .restart local v4    # "j":I
    :cond_2
    :goto_3
    sget-object v7, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->IN_HOTSEAT_POSITION_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v2, v7}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    move-object v1, v2

    .line 311
    goto :goto_1

    .line 294
    :cond_3
    iget-object v7, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mHotseatItems:Ljava/util/ArrayList;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;

    .line 296
    .local v6, "tmphotseatItem":Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;
    if-lez v4, :cond_5

    sget v7, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->MAX_AWARE_HOTSEAT_CELL_COUNT:I

    if-ge v4, v7, :cond_5

    iget-object v7, v6, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;->className:Ljava/lang/String;

    invoke-direct {p0, v4, v1, v7}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->duplicateCheckOfAwareHotseat(ILjava/util/ArrayList;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 285
    .end local v6    # "tmphotseatItem":Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;
    :cond_4
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 300
    .restart local v6    # "tmphotseatItem":Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;
    :cond_5
    iget v9, v6, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;->screen:I

    if-nez v1, :cond_6

    move v7, v8

    :goto_5
    add-int/2addr v7, v9

    iput v7, v6, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;->screen:I

    .line 301
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 303
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v7

    sget v9, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->MAX_AWARE_HOTSEAT_CELL_COUNT:I

    if-lt v7, v9, :cond_4

    goto :goto_3

    .line 300
    :cond_6
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v7

    goto :goto_5

    .end local v4    # "j":I
    .end local v6    # "tmphotseatItem":Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;
    .restart local v5    # "j":I
    :cond_7
    move v4, v5

    .end local v5    # "j":I
    .restart local v4    # "j":I
    goto :goto_4
.end method

.method protected removeContextualPageNotification(I)V
    .locals 6
    .param p1, "cpType"    # I

    .prologue
    .line 110
    const/4 v2, 0x0

    .line 111
    .local v2, "nextCpType":I
    sget-object v3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->kindCP:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->kindCP:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    if-ltz v3, :cond_0

    .line 112
    sget-object v3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->kindCP:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 113
    sget-object v3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->kindCP:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 115
    sget-object v3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->kindCP:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 116
    const/4 v2, 0x0

    .line 126
    :cond_0
    sget-object v3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mCPNotification:Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;

    iget-object v4, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->mContext:Landroid/content/Context;

    const v5, 0x7f020014

    add-int/2addr v5, p1

    invoke-virtual {v3, v4, v5, v2}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNoti;->notificationClear(Landroid/content/Context;II)V

    .line 127
    return-void

    .line 117
    :cond_1
    sget-object v3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->kindCP:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 118
    const/4 v2, 0x4

    .line 119
    sget-object v3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->kindCP:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 120
    .local v0, "c":Ljava/lang/Integer;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-le v2, v3, :cond_2

    .line 121
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_0
.end method

.method public setupContextualAwareHotSeat(Landroid/content/Context;I)V
    .locals 20
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mode"    # I

    .prologue
    .line 332
    const/4 v3, 0x0

    .line 333
    .local v3, "modeUri":Landroid/net/Uri;
    const-string v16, "content://com.samsung.android.providers.context.profile/app_used?device_type="

    .line 335
    .local v16, "newUri":Ljava/lang/String;
    const/16 v19, -0x1

    .line 337
    .local v19, "version":I
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v4, "com.samsung.android.providers.context"

    const/16 v5, 0x80

    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v17

    .line 339
    .local v17, "pInfo":Landroid/content/pm/PackageInfo;
    move-object/from16 v0, v17

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    move/from16 v19, v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 344
    .end local v17    # "pInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    packed-switch p2, :pswitch_data_0

    .line 355
    :goto_1
    const/4 v11, 0x0

    .line 357
    .local v11, "cursor":Landroid/database/Cursor;
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v11

    .line 362
    :goto_2
    if-eqz v11, :cond_b

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 363
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 364
    .local v8, "awareHotseatItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;>;"
    const/4 v13, 0x0

    .line 366
    .local v13, "i":I
    :cond_0
    const-string v2, "launcher_type"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_1

    .line 367
    const/4 v2, 0x2

    move/from16 v0, v19

    if-lt v0, v2, :cond_8

    const-string v2, "app_id"

    :goto_3
    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 368
    .local v15, "idxCol_pkg":I
    const/4 v2, 0x2

    move/from16 v0, v19

    if-lt v0, v2, :cond_9

    const-string v2, "app_sub_id"

    :goto_4
    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 369
    .local v14, "idxCol_cls":I
    invoke-interface {v11, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 370
    .local v18, "pkgName":Ljava/lang/String;
    invoke-interface {v11, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 372
    .local v9, "clsName":Ljava/lang/String;
    if-eqz v18, :cond_1

    const-string v2, "com.sec.android.app.launcher"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    if-nez v2, :cond_a

    .line 380
    .end local v9    # "clsName":Ljava/lang/String;
    .end local v14    # "idxCol_cls":I
    .end local v15    # "idxCol_pkg":I
    .end local v18    # "pkgName":Ljava/lang/String;
    :cond_1
    :goto_5
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    sget v2, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->MAX_AWARE_HOTSEAT_CELL_COUNT:I

    if-lt v13, v2, :cond_0

    .line 382
    :cond_2
    sget-object v2, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->awareHotseatItemsMap:Ljava/util/HashMap;

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 383
    sget-object v2, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->awareHotseatItemsMap:Ljava/util/HashMap;

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389
    .end local v8    # "awareHotseatItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;>;"
    .end local v13    # "i":I
    :goto_6
    if-eqz v11, :cond_3

    .line 390
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 391
    :cond_3
    return-void

    .line 346
    .end local v11    # "cursor":Landroid/database/Cursor;
    :pswitch_0
    const/4 v2, 0x2

    move/from16 v0, v19

    if-lt v0, v2, :cond_4

    const-string v2, "content://com.samsung.android.providers.context.profile/app_used?device_type=5"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    :goto_7
    goto/16 :goto_1

    :cond_4
    sget-object v3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->STATUS_SPEN_URI:Landroid/net/Uri;

    goto :goto_7

    .line 348
    :pswitch_1
    const/4 v2, 0x2

    move/from16 v0, v19

    if-lt v0, v2, :cond_5

    const-string v2, "content://com.samsung.android.providers.context.profile/app_used?device_type=0"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    :goto_8
    goto/16 :goto_1

    :cond_5
    sget-object v3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->STATUS_EARPHONE_URI:Landroid/net/Uri;

    goto :goto_8

    .line 350
    :pswitch_2
    const/4 v2, 0x2

    move/from16 v0, v19

    if-lt v0, v2, :cond_6

    const-string v2, "content://com.samsung.android.providers.context.profile/app_used?device_type=4"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    :goto_9
    goto/16 :goto_1

    :cond_6
    sget-object v3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->STATUS_DOCK_URI:Landroid/net/Uri;

    goto :goto_9

    .line 352
    :pswitch_3
    const/4 v2, 0x2

    move/from16 v0, v19

    if-lt v0, v2, :cond_7

    const-string v2, "content://com.samsung.android.providers.context.profile/app_used?device_type=6"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    :goto_a
    goto/16 :goto_1

    :cond_7
    sget-object v3, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->STATUS_ROAMING_URI:Landroid/net/Uri;

    goto :goto_a

    .line 358
    .restart local v11    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v12

    .line 359
    .local v12, "e":Ljava/lang/Exception;
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 367
    .end local v12    # "e":Ljava/lang/Exception;
    .restart local v8    # "awareHotseatItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;>;"
    .restart local v13    # "i":I
    :cond_8
    const-string v2, "package_name"

    goto/16 :goto_3

    .line 368
    .restart local v15    # "idxCol_pkg":I
    :cond_9
    const-string v2, "class_name"

    goto/16 :goto_4

    .line 376
    .restart local v9    # "clsName":Ljava/lang/String;
    .restart local v14    # "idxCol_cls":I
    .restart local v18    # "pkgName":Ljava/lang/String;
    :cond_a
    new-instance v10, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;

    move-object/from16 v0, v18

    move/from16 v1, p2

    invoke-direct {v10, v0, v9, v13, v1}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 377
    .local v10, "cptemp":Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;
    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 378
    add-int/lit8 v13, v13, 0x1

    goto :goto_5

    .line 386
    .end local v8    # "awareHotseatItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;>;"
    .end local v9    # "clsName":Ljava/lang/String;
    .end local v10    # "cptemp":Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;
    .end local v13    # "i":I
    .end local v14    # "idxCol_cls":I
    .end local v15    # "idxCol_pkg":I
    .end local v18    # "pkgName":Ljava/lang/String;
    :cond_b
    const-string v2, "PageBuddyNotiMgr"

    const-string v4, "[OOPS] Fail to get cursor because DB empty. "

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 340
    .end local v11    # "cursor":Landroid/database/Cursor;
    :catch_1
    move-exception v2

    goto/16 :goto_0

    .line 344
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setupContextualAwareHotSeatForManual(Landroid/content/Context;I)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mode"    # I

    .prologue
    .line 394
    const/4 v7, 0x0

    .line 395
    .local v7, "testString":Ljava/lang/String;
    packed-switch p2, :pswitch_data_0

    .line 404
    :goto_0
    const-string v8, ";"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 405
    .local v0, "appInfo":[Ljava/lang/String;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 407
    .local v2, "awareHotseatItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    array-length v8, v0

    if-ge v5, v8, :cond_2

    .line 408
    aget-object v8, v0, v5

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 409
    .local v1, "appInfoDetail":[Ljava/lang/String;
    const/4 v8, 0x0

    aget-object v6, v1, v8

    .line 410
    .local v6, "pkgName":Ljava/lang/String;
    const/4 v8, 0x1

    aget-object v3, v1, v8

    .line 412
    .local v3, "clsName":Ljava/lang/String;
    if-eqz v6, :cond_0

    const-string v8, "com.sec.android.app.launcher"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    invoke-virtual {v8, v6}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v8

    if-eqz v8, :cond_1

    const-string v8, "com.android.contacts.activities.DialtactsActivity"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 407
    :cond_0
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 397
    .end local v0    # "appInfo":[Ljava/lang/String;
    .end local v1    # "appInfoDetail":[Ljava/lang/String;
    .end local v2    # "awareHotseatItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;>;"
    .end local v3    # "clsName":Ljava/lang/String;
    .end local v5    # "i":I
    .end local v6    # "pkgName":Ljava/lang/String;
    :pswitch_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "recommended_apps_list_earphones"

    invoke-static {v8, v9}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 399
    :pswitch_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "recommended_apps_list_dockings"

    invoke-static {v8, v9}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 417
    .restart local v0    # "appInfo":[Ljava/lang/String;
    .restart local v1    # "appInfoDetail":[Ljava/lang/String;
    .restart local v2    # "awareHotseatItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;>;"
    .restart local v3    # "clsName":Ljava/lang/String;
    .restart local v5    # "i":I
    .restart local v6    # "pkgName":Ljava/lang/String;
    :cond_1
    new-instance v4, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;

    invoke-direct {v4, v6, v3, v5, p2}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 418
    .local v4, "cptemp":Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 421
    .end local v1    # "appInfoDetail":[Ljava/lang/String;
    .end local v3    # "clsName":Ljava/lang/String;
    .end local v4    # "cptemp":Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr$PageBuddyHotseatItem;
    .end local v6    # "pkgName":Ljava/lang/String;
    :cond_2
    sget-object v8, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->awareHotseatItemsMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 422
    sget-object v8, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->awareHotseatItemsMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 424
    return-void

    .line 395
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
