.class Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc$2;
.super Landroid/content/BroadcastReceiver;
.source "PageBuddyNotiSvc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;


# direct methods
.method constructor <init>(Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;)V
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc$2;->this$0:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 204
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 206
    .local v0, "action":Ljava/lang/String;
    const-string v6, "PageBuddyNotiSvc"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mCPBroadcastReceiver action="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mCpBitFlag="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$200()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mResolver:Landroid/content/ContentResolver;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$100()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "recommended_apps_setting"

    const/4 v8, 0x1

    invoke-static {v6, v7, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    # setter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isRecommendedAppsEnable:I
    invoke-static {v6}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$002(I)I

    .line 213
    const-string v6, "com.samsung.pen.INSERT"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 214
    const-string v6, "penInsert"

    const/4 v7, 0x0

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 215
    .local v1, "bPluged":Z
    if-nez v1, :cond_3

    .line 216
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isRecommendedAppsEnable:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$000()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 217
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, p1, v7}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->setupContextualAwareHotSeat(Landroid/content/Context;I)V

    .line 218
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$200()I

    move-result v6

    and-int/lit8 v6, v6, 0x1

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc$2;->this$0:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;

    invoke-virtual {v6}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isKnoxMode()Z

    move-result v6

    if-nez v6, :cond_0

    .line 219
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->addContextualPageNotification(I)V

    .line 222
    :cond_0
    const/4 v6, 0x1

    # |= operator for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I
    invoke-static {v6}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$276(I)I

    .line 347
    .end local v1    # "bPluged":Z
    :cond_1
    :goto_0
    iget-object v6, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc$2;->this$0:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;

    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mPrefs:Landroid/content/SharedPreferences;
    invoke-static {v6}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$600(Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;)Landroid/content/SharedPreferences;

    move-result-object v6

    if-nez v6, :cond_2

    .line 348
    iget-object v6, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc$2;->this$0:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;

    iget-object v7, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc$2;->this$0:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;

    const-string v8, "com.sec.android.pagebuddynotisvc.prefs"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    # setter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mPrefs:Landroid/content/SharedPreferences;
    invoke-static {v6, v7}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$602(Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;Landroid/content/SharedPreferences;)Landroid/content/SharedPreferences;

    .line 349
    :cond_2
    iget-object v6, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc$2;->this$0:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;

    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mPrefs:Landroid/content/SharedPreferences;
    invoke-static {v6}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$600(Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 350
    .local v3, "edit":Landroid/content/SharedPreferences$Editor;
    const-string v6, "cp_bit_flag"

    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$200()I

    move-result v7

    invoke-interface {v3, v6, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 351
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 352
    return-void

    .line 224
    .end local v3    # "edit":Landroid/content/SharedPreferences$Editor;
    .restart local v1    # "bPluged":Z
    :cond_3
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isRecommendedAppsEnable:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$000()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_4

    .line 225
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$200()I

    move-result v6

    and-int/lit8 v6, v6, 0x1

    const/4 v7, 0x1

    if-ne v6, v7, :cond_4

    .line 226
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->removeContextualPageNotification(I)V

    .line 229
    :cond_4
    const/4 v6, -0x2

    # &= operator for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I
    invoke-static {v6}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$272(I)I

    goto :goto_0

    .line 231
    .end local v1    # "bPluged":Z
    :cond_5
    const-string v6, "com.sec.android.contextaware.HEADSET_PLUG"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$200()I

    move-result v6

    and-int/lit8 v6, v6, 0x8

    const/16 v7, 0x8

    if-eq v6, v7, :cond_a

    .line 233
    const-string v6, "state"

    const/4 v7, 0x0

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_7

    const/4 v1, 0x1

    .line 234
    .restart local v1    # "bPluged":Z
    :goto_1
    if-eqz v1, :cond_8

    .line 235
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isRecommendedAppsEnable:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$000()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_6

    .line 236
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v6

    const/4 v7, 0x2

    invoke-virtual {v6, p1, v7}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->setupContextualAwareHotSeatForManual(Landroid/content/Context;I)V

    .line 240
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$200()I

    move-result v6

    and-int/lit8 v6, v6, 0x2

    if-nez v6, :cond_6

    iget-object v6, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc$2;->this$0:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;

    invoke-virtual {v6}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isKnoxMode()Z

    move-result v6

    if-nez v6, :cond_6

    .line 241
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v6

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->addContextualPageNotification(I)V

    .line 244
    :cond_6
    const/4 v6, 0x2

    # |= operator for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I
    invoke-static {v6}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$276(I)I

    goto/16 :goto_0

    .line 233
    .end local v1    # "bPluged":Z
    :cond_7
    const/4 v1, 0x0

    goto :goto_1

    .line 246
    .restart local v1    # "bPluged":Z
    :cond_8
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isRecommendedAppsEnable:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$000()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_9

    .line 247
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$200()I

    move-result v6

    and-int/lit8 v6, v6, 0x2

    const/4 v7, 0x2

    if-ne v6, v7, :cond_9

    .line 248
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v6

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->removeContextualPageNotification(I)V

    .line 251
    :cond_9
    const/4 v6, -0x3

    # &= operator for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I
    invoke-static {v6}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$272(I)I

    goto/16 :goto_0

    .line 253
    .end local v1    # "bPluged":Z
    :cond_a
    const-string v6, "android.intent.action.DOCK_EVENT"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 254
    const/4 v1, 0x0

    .line 256
    .restart local v1    # "bPluged":Z
    const-string v6, "android.intent.extra.DOCK_STATE"

    const/4 v7, 0x0

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 257
    .local v2, "docState":I
    packed-switch v2, :pswitch_data_0

    .line 269
    :goto_2
    :pswitch_0
    if-eqz v1, :cond_c

    .line 270
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isRecommendedAppsEnable:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$000()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_b

    .line 276
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v6

    const/4 v7, 0x3

    invoke-virtual {v6, p1, v7}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->setupContextualAwareHotSeatForManual(Landroid/content/Context;I)V

    .line 277
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$200()I

    move-result v6

    and-int/lit8 v6, v6, 0x4

    if-nez v6, :cond_b

    iget-object v6, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc$2;->this$0:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;

    invoke-virtual {v6}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isKnoxMode()Z

    move-result v6

    if-nez v6, :cond_b

    .line 278
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v6

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->addContextualPageNotification(I)V

    .line 281
    :cond_b
    const/4 v6, 0x4

    # |= operator for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I
    invoke-static {v6}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$276(I)I

    goto/16 :goto_0

    .line 263
    :pswitch_1
    const/4 v1, 0x1

    .line 264
    goto :goto_2

    .line 283
    :cond_c
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isRecommendedAppsEnable:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$000()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_d

    .line 284
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$200()I

    move-result v6

    and-int/lit8 v6, v6, 0x4

    const/4 v7, 0x4

    if-ne v6, v7, :cond_d

    .line 285
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v6

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->removeContextualPageNotification(I)V

    .line 288
    :cond_d
    const/4 v6, -0x5

    # &= operator for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I
    invoke-static {v6}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$272(I)I

    goto/16 :goto_0

    .line 290
    .end local v1    # "bPluged":Z
    .end local v2    # "docState":I
    :cond_e
    const-string v6, "android.intent.action.SERVICE_STATE"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 291
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    invoke-static {v6}, Landroid/telephony/ServiceState;->newFromBundle(Landroid/os/Bundle;)Landroid/telephony/ServiceState;

    move-result-object v5

    .line 292
    .local v5, "state":Landroid/telephony/ServiceState;
    invoke-virtual {v5}, Landroid/telephony/ServiceState;->getRoaming()Z

    move-result v4

    .line 293
    .local v4, "isRoaming":Z
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v6

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->removeContextualPageNotification(I)V

    .line 323
    if-eqz v4, :cond_11

    .line 324
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isRecommendedAppsEnable:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$000()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_f

    .line 325
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isRecommendedAppsAutoRoaming:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$500()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_10

    .line 326
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v6

    const/4 v7, 0x4

    invoke-virtual {v6, p1, v7}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->setupContextualAwareHotSeat(Landroid/content/Context;I)V

    .line 330
    :goto_3
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$200()I

    move-result v6

    and-int/lit8 v6, v6, 0x8

    if-nez v6, :cond_f

    iget-object v6, p0, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc$2;->this$0:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;

    invoke-virtual {v6}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isKnoxMode()Z

    move-result v6

    if-nez v6, :cond_f

    .line 331
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v6

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->addContextualPageNotification(I)V

    .line 334
    :cond_f
    const/16 v6, 0x8

    # |= operator for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I
    invoke-static {v6}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$276(I)I

    goto/16 :goto_0

    .line 328
    :cond_10
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v6

    const/4 v7, 0x4

    invoke-virtual {v6, p1, v7}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->setupContextualAwareHotSeatForManual(Landroid/content/Context;I)V

    goto :goto_3

    .line 336
    :cond_11
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->isRecommendedAppsEnable:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$000()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_12

    .line 337
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$200()I

    move-result v6

    and-int/lit8 v6, v6, 0x8

    const/16 v7, 0x8

    if-ne v6, v7, :cond_12

    .line 338
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v6

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->removeContextualPageNotification(I)V

    .line 341
    :cond_12
    const/16 v6, -0x9

    # &= operator for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCpBitFlag:I
    invoke-static {v6}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$272(I)I

    goto/16 :goto_0

    .line 343
    .end local v4    # "isRoaming":Z
    .end local v5    # "state":Landroid/telephony/ServiceState;
    :cond_13
    const-string v6, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 344
    # getter for: Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->mCPNotiMgr:Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;
    invoke-static {}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiSvc;->access$400()Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/sec/android/pagebuddynotisvc/PageBuddyNotiMgr;->checkLastNotiType(Z)V

    goto/16 :goto_0

    .line 257
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
