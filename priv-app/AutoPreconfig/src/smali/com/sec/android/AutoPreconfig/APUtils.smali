.class public Lcom/sec/android/AutoPreconfig/APUtils;
.super Ljava/lang/Object;
.source "APUtils.java"

# interfaces
.implements Lcom/sec/android/AutoPreconfig/Constants;


# static fields
.field private static mFeatureValues:[Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 11
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, ""

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/AutoPreconfig/APUtils;->mFeatureValues:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/AutoPreconfig/APUtils;->mContext:Landroid/content/Context;

    .line 15
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/AutoPreconfig/APUtils;->mContext:Landroid/content/Context;

    .line 16
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Common_AutoConfigurationType"

    const-string v2, "DISABLE"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/AutoPreconfig/APUtils;->mFeatureValues:[Ljava/lang/String;

    .line 17
    return-void
.end method

.method private getConvertLocale(Ljava/lang/String;)[Ljava/lang/String;
    .locals 4
    .param p1, "defLanguage"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 110
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v2

    .line 112
    .local v0, "languageInfo":[Ljava/lang/String;
    const-string v1, "_"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 113
    const-string v1, "_"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 121
    :goto_0
    return-object v0

    .line 114
    :cond_0
    sget-object v1, Lcom/sec/android/AutoPreconfig/APUtils;->LangMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 115
    sget-object v1, Lcom/sec/android/AutoPreconfig/APUtils;->LangMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 117
    :cond_1
    const-string v1, "en"

    aput-object v1, v0, v2

    .line 118
    const-string v1, "US"

    aput-object v1, v0, v3

    goto :goto_0
.end method


# virtual methods
.method public getCurrentSalescode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 53
    const-string v0, "ro.csc.sales_code"

    const-string v1, "UNKNOWN"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultSalescode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 57
    sget-object v0, Lcom/sec/android/AutoPreconfig/APUtils;->mFeatureValues:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 58
    sget-object v0, Lcom/sec/android/AutoPreconfig/APUtils;->mFeatureValues:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 60
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "UNKNOWN"

    goto :goto_0
.end method

.method public getOperatingMode()I
    .locals 3

    .prologue
    const/16 v1, 0xa

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/AutoPreconfig/APUtils;->getOperatingModeName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, "op":Ljava/lang/String;
    const-string v2, "GLOBAL"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 86
    :cond_0
    :goto_0
    return v1

    .line 77
    :cond_1
    const-string v2, "GLOBAL_SIMPLE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 78
    const/16 v1, 0xb

    goto :goto_0

    .line 79
    :cond_2
    const-string v2, "UNIVERSAL"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 80
    const/16 v1, 0xc

    goto :goto_0

    .line 81
    :cond_3
    const-string v2, "VODA"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 82
    const/16 v1, 0xd

    goto :goto_0

    .line 83
    :cond_4
    const-string v2, "Telefonica"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 84
    const/16 v1, 0xe

    goto :goto_0
.end method

.method public getOperatingModeName()Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 65
    sget-object v0, Lcom/sec/android/AutoPreconfig/APUtils;->mFeatureValues:[Ljava/lang/String;

    array-length v0, v0

    if-le v0, v1, :cond_0

    .line 66
    sget-object v0, Lcom/sec/android/AutoPreconfig/APUtils;->mFeatureValues:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 68
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "UNKNOWN"

    goto :goto_0
.end method

.method public isEnableFeature()Z
    .locals 4

    .prologue
    .line 45
    const-string v0, "DISABLE"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Common_AutoConfigurationType"

    const-string v3, "DISABLE"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    const/4 v0, 0x1

    .line 48
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLocale(Ljava/lang/String;)V
    .locals 8
    .param p1, "defLanguage"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 91
    new-array v1, v7, [Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v1, v6

    .line 93
    .local v1, "language":[Ljava/lang/String;
    const-string v3, "AutoPreconfig"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "defLanguage: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    invoke-direct {p0, p1}, Lcom/sec/android/AutoPreconfig/APUtils;->getConvertLocale(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 97
    const-string v3, "AutoPreconfig"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "language: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v1, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const-string v3, "AutoPreconfig"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "region: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v1, v7

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    new-instance v2, Ljava/util/Locale;

    aget-object v3, v1, v6

    aget-object v4, v1, v7

    invoke-direct {v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    .local v2, "locale":Ljava/util/Locale;
    invoke-static {v2}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 102
    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    .line 103
    .local v0, "config":Landroid/content/res/Configuration;
    iput-object v2, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 104
    iget-object v3, p0, Lcom/sec/android/AutoPreconfig/APUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/AutoPreconfig/APUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 106
    const-string v3, "AutoPreconfig"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Set Language: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/AutoPreconfig/APUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget-object v5, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->getDisplayLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    return-void
.end method

.method public setTestMode(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "defaultCsc"    # Ljava/lang/String;
    .param p2, "operatingMode"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 20
    const/4 v0, 0x0

    .line 22
    .local v0, "ret":Z
    sget-boolean v2, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mIsShipBuild:Z

    if-eqz v2, :cond_0

    .line 23
    const-string v2, "AutoPreconfig"

    const-string v3, "This is ship build so couldn\'t set test mode."

    invoke-static {v2, v3, v4}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;Z)V

    move v1, v0

    .line 41
    .end local v0    # "ret":Z
    .local v1, "ret":I
    :goto_0
    return v1

    .line 27
    .end local v1    # "ret":I
    .restart local v0    # "ret":Z
    :cond_0
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 28
    const-string v2, "AutoPreconfig"

    const-string v3, "Test mode isn\'t set."

    invoke-static {v2, v3, v4}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 29
    const/4 v0, 0x0

    :goto_1
    move v1, v0

    .line 41
    .restart local v1    # "ret":I
    goto :goto_0

    .line 31
    .end local v1    # "ret":I
    :cond_1
    if-eqz p1, :cond_2

    .line 32
    sget-object v2, Lcom/sec/android/AutoPreconfig/APUtils;->mFeatureValues:[Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    .line 35
    :cond_2
    if-eqz p2, :cond_3

    .line 36
    sget-object v2, Lcom/sec/android/AutoPreconfig/APUtils;->mFeatureValues:[Ljava/lang/String;

    aput-object p2, v2, v4

    .line 38
    :cond_3
    const/4 v0, 0x1

    goto :goto_1
.end method
