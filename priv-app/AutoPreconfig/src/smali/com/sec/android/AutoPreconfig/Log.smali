.class public Lcom/sec/android/AutoPreconfig/Log;
.super Ljava/lang/Object;
.source "Log.java"


# direct methods
.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "log"    # Ljava/lang/String;

    .prologue
    .line 9
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 10
    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "log"    # Ljava/lang/String;
    .param p2, "isLoggingFile"    # Z

    .prologue
    .line 28
    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 29
    if-eqz p2, :cond_0

    .line 30
    invoke-static {p1}, Lcom/sec/android/AutoPreconfig/APFileWriter;->writeToLogFile(Ljava/lang/String;)V

    .line 32
    :cond_0
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "log"    # Ljava/lang/String;

    .prologue
    .line 5
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 6
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "log"    # Ljava/lang/String;
    .param p2, "isLoggingFile"    # Z

    .prologue
    .line 21
    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 22
    if-eqz p2, :cond_0

    .line 23
    invoke-static {p1}, Lcom/sec/android/AutoPreconfig/APFileWriter;->writeToLogFile(Ljava/lang/String;)V

    .line 25
    :cond_0
    return-void
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "log"    # Ljava/lang/String;

    .prologue
    .line 13
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 14
    return-void
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "log"    # Ljava/lang/String;
    .param p2, "isLoggingFile"    # Z

    .prologue
    .line 35
    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    if-eqz p2, :cond_0

    .line 37
    invoke-static {p1}, Lcom/sec/android/AutoPreconfig/APFileWriter;->writeToLogFile(Ljava/lang/String;)V

    .line 39
    :cond_0
    return-void
.end method
