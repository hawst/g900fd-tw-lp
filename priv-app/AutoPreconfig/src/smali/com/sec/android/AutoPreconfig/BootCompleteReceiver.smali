.class public Lcom/sec/android/AutoPreconfig/BootCompleteReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BootCompleteReceiver.java"


# static fields
.field private static LOG_TAG:Ljava/lang/String;


# instance fields
.field private mApUtils:Lcom/sec/android/AutoPreconfig/APUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-string v0, "AutoPreconfigBootCompleteReceiver"

    sput-object v0, Lcom/sec/android/AutoPreconfig/BootCompleteReceiver;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/AutoPreconfig/BootCompleteReceiver;->mApUtils:Lcom/sec/android/AutoPreconfig/APUtils;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 19
    sget-object v2, Lcom/sec/android/AutoPreconfig/BootCompleteReceiver;->LOG_TAG:Ljava/lang/String;

    const-string v3, "onReceive"

    invoke-static {v2, v3}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    new-instance v2, Lcom/sec/android/AutoPreconfig/APUtils;

    invoke-direct {v2, p1}, Lcom/sec/android/AutoPreconfig/APUtils;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/AutoPreconfig/BootCompleteReceiver;->mApUtils:Lcom/sec/android/AutoPreconfig/APUtils;

    .line 21
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/BootCompleteReceiver;->mApUtils:Lcom/sec/android/AutoPreconfig/APUtils;

    invoke-virtual {v2}, Lcom/sec/android/AutoPreconfig/APUtils;->isEnableFeature()Z

    move-result v2

    if-nez v2, :cond_1

    .line 22
    sget-object v2, Lcom/sec/android/AutoPreconfig/BootCompleteReceiver;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Feature AutoPreconfig is disabled"

    invoke-static {v2, v3}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 26
    :cond_1
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    .line 27
    .local v1, "userId":I
    if-eqz v1, :cond_2

    .line 28
    sget-object v2, Lcom/sec/android/AutoPreconfig/BootCompleteReceiver;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Sub-user cannot run AutoPreconfig"

    invoke-static {v2, v3}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 32
    :cond_2
    if-nez p2, :cond_3

    .line 33
    sget-object v2, Lcom/sec/android/AutoPreconfig/BootCompleteReceiver;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Null intent was passed. skip."

    invoke-static {v2, v3}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 38
    :cond_3
    const-string v2, "android.intent.action.FACTORYRESET"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "factory"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 41
    sget-object v2, Lcom/sec/android/AutoPreconfig/BootCompleteReceiver;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Receive SEC_FACTORY_RESET_ACTION intent. Remove EFS flag."

    invoke-static {v2, v3}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    invoke-static {}, Lcom/sec/android/AutoPreconfig/APFileWriter;->removeEfsFlag()V

    goto :goto_0

    .line 43
    :cond_4
    const-string v2, "android.intent.action.CSC_MODEM_SETTING"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "SET_MODEM_INFO"

    const-string v3, "MODE"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 46
    sget-object v2, Lcom/sec/android/AutoPreconfig/BootCompleteReceiver;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Receive CscModemSettingService intent."

    invoke-static {v2, v3}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    invoke-static {p1}, Lcom/sec/android/AutoPreconfig/APPreference;->setPrefCscModemFlag(Landroid/content/Context;)V

    .line 49
    invoke-static {p1}, Lcom/sec/android/AutoPreconfig/APPreference;->getPrefCscModemBlockFlag(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 50
    sget-object v2, Lcom/sec/android/AutoPreconfig/BootCompleteReceiver;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Block android.intent.action.CSC_MODEM_SETTING"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/AutoPreconfig/BootCompleteReceiver;->abortBroadcast()V

    goto :goto_0

    .line 53
    :cond_5
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.sec.android.AutoPreconfig.AUTO_PRECONFIG"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 55
    sget-object v2, Lcom/sec/android/AutoPreconfig/BootCompleteReceiver;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Send intent to AutoPreconfigService for test purpose."

    invoke-static {v2, v3}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 57
    .local v0, "serviceIntent":Landroid/content/Intent;
    const-string v2, "com.sec.android.AutoPreconfig.AUTO_PRECONFIG"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 58
    const-string v2, "MCCMNC"

    const-string v3, "MCCMNC"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 59
    const-string v2, "GID1"

    const-string v3, "GID1"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 60
    const-string v2, "SPCODE"

    const-string v3, "SPCODE"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 61
    const-string v2, "SPNAME"

    const-string v3, "SPNAME"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 62
    const-string v2, "TEST"

    const-string v3, "TRUE"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 63
    const-string v2, "MPS"

    const-string v3, "MPS"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 64
    const-string v2, "MODE"

    const-string v3, "MODE"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0
.end method
