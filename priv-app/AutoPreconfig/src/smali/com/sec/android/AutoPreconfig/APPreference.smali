.class public Lcom/sec/android/AutoPreconfig/APPreference;
.super Ljava/lang/Object;
.source "APPreference.java"


# static fields
.field private static LOG_TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    const-string v0, "AutoPreconfig"

    sput-object v0, Lcom/sec/android/AutoPreconfig/APPreference;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public static getPrefCscModemBlockFlag(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    const-string v1, "autopreconfig.key.cscmodemsetting_block"

    invoke-static {p0, v1}, Lcom/sec/android/AutoPreconfig/APPreference;->getPreference(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 55
    .local v0, "bl":Z
    sget-object v1, Lcom/sec/android/AutoPreconfig/APPreference;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPrefCscModemFlag(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    return v0
.end method

.method public static getPrefResultFlag(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    const-string v1, "autopreconfig.key.preconfig_result"

    invoke-static {p0, v1}, Lcom/sec/android/AutoPreconfig/APPreference;->getPreference(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 33
    .local v0, "bl":Z
    sget-object v1, Lcom/sec/android/AutoPreconfig/APPreference;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPrefResultFlag(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    return v0
.end method

.method private static declared-synchronized getPreference(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 14
    const-class v2, Lcom/sec/android/AutoPreconfig/APPreference;

    monitor-enter v2

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v3, "autopreconfig.settings"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 15
    .local v0, "pref":Landroid/content/SharedPreferences;
    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    monitor-exit v2

    return v1

    .line 14
    .end local v0    # "pref":Landroid/content/SharedPreferences;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static setPrefCscModemBlockFlag(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    const-string v0, "autopreconfig.key.cscmodemsetting_block"

    invoke-static {p0, v0}, Lcom/sec/android/AutoPreconfig/APPreference;->setPreference(Landroid/content/Context;Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method public static setPrefCscModemFlag(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    const-string v0, "autopreconfig.key.cscmodemsetting_done"

    invoke-static {p0, v0}, Lcom/sec/android/AutoPreconfig/APPreference;->setPreference(Landroid/content/Context;Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method private static declared-synchronized setPreference(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v0, Lcom/sec/android/AutoPreconfig/APPreference;

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    invoke-static {p0, p1, v1}, Lcom/sec/android/AutoPreconfig/APPreference;->setPreference(Landroid/content/Context;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    monitor-exit v0

    return-void

    .line 19
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static declared-synchronized setPreference(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    .line 23
    const-class v3, Lcom/sec/android/AutoPreconfig/APPreference;

    monitor-enter v3

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v4, "autopreconfig.settings"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 24
    .local v1, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 25
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const/4 v2, 0x1

    invoke-interface {v0, p1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 26
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v2

    if-nez v2, :cond_0

    .line 27
    sget-object v2, Lcom/sec/android/AutoPreconfig/APPreference;->LOG_TAG:Ljava/lang/String;

    const-string v4, "SharedPreference failed to write."

    invoke-static {v2, v4}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 29
    :cond_0
    monitor-exit v3

    return-void

    .line 23
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "preferences":Landroid/content/SharedPreferences;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method
