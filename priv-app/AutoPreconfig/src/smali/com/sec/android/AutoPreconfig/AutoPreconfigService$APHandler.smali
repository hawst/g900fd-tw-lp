.class Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;
.super Landroid/os/Handler;
.source "AutoPreconfigService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/AutoPreconfig/AutoPreconfigService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "APHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;


# direct methods
.method public constructor <init>(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    .line 149
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 150
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x2

    const/4 v5, 0x0

    const v4, 0x7f030001

    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 154
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "error"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 155
    .local v7, "error":I
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$000(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 156
    .local v9, "res":Landroid/content/res/Resources;
    const-string v0, "AutoPreconfig"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "APHandler handleMessage msg.what = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v11}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 157
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 242
    const-string v0, "AutoPreconfig"

    const-string v1, "Unsupported item."

    invoke-static {v0, v1}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    :cond_0
    :goto_0
    return-void

    .line 159
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # invokes: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->checkCondition(Z)Z
    invoke-static {v0, v12}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$100(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 160
    new-instance v8, Landroid/os/Message;

    invoke-direct {v8}, Landroid/os/Message;-><init>()V

    .line 161
    .local v8, "msg1":Landroid/os/Message;
    iput v12, v8, Landroid/os/Message;->what:I

    .line 162
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    iput-object v0, v8, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 163
    invoke-virtual {p0, v8}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 165
    .end local v8    # "msg1":Landroid/os/Message;
    :cond_1
    const-string v0, "AutoPreconfig"

    const-string v1, "error during checking conditions."

    invoke-static {v0, v1, v11}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 166
    invoke-virtual {p0, v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 170
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    # invokes: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->getIntentValue(Landroid/content/Intent;)Z
    invoke-static {v1, v0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$200(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 171
    invoke-virtual {p0, v11}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 173
    :cond_2
    invoke-virtual {p0, v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 177
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # invokes: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->doPreconfiguration()V
    invoke-static {v0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$300(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)V

    goto :goto_0

    .line 180
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    invoke-virtual {v0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->stopSelf()V

    goto :goto_0

    .line 183
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # setter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mWaitForStartConfirmFlag:I
    invoke-static {v0, v11}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$402(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;I)I

    goto :goto_0

    .line 186
    :sswitch_5
    const-string v0, "SKIPPED"

    invoke-static {}, Lcom/sec/android/AutoPreconfig/APFileWriter;->getEfsFlag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 187
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # setter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mWaitForStartConfirmFlag:I
    invoke-static {v0, v11}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$402(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;I)I

    goto :goto_0

    .line 191
    :cond_3
    :sswitch_6
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    iget-object v1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$000(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v9, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f030004

    new-array v6, v11, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;
    invoke-static {v10}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$500(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Ljava/util/HashMap;

    move-result-object v10

    const-string v11, "Country"

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    aput-object v10, v6, v12

    invoke-virtual {v9, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->showAlertPanel(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 196
    :sswitch_7
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    iget-object v1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$000(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v9, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v2, 0x7f030002

    new-array v4, v11, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;
    invoke-static {v6}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$500(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Ljava/util/HashMap;

    move-result-object v6

    const-string v10, "Country"

    invoke-virtual {v6, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v12

    invoke-virtual {v9, v2, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move v2, v11

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->showAlertPanel(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 201
    :sswitch_8
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    iget-object v1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$000(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v9, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v2, 0x7f030003

    new-array v4, v11, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;
    invoke-static {v6}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$500(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Ljava/util/HashMap;

    move-result-object v6

    const-string v10, "Country"

    invoke-virtual {v6, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v12

    invoke-virtual {v9, v2, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move v2, v11

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->showAlertPanel(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 206
    :sswitch_9
    iget-object v1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$000(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v9, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mNetworkName:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$600(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Ljava/util/ArrayList;

    move-result-object v0

    new-array v6, v12, [Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    move-object v6, v0

    check-cast v6, [Ljava/lang/String;

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->showAlertPanel(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 211
    :sswitch_a
    const-string v0, "AutoPreconfig"

    const-string v1, "Received SET_RIL_CODE response"

    invoke-static {v0, v1}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    sget-boolean v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mIsDebuggableMonkeyBuild:Z

    if-nez v0, :cond_0

    .line 223
    if-eqz v7, :cond_4

    .line 224
    const-string v0, "AutoPreconfig"

    const-string v1, "Fail to request Preconfig."

    invoke-static {v0, v1, v11}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 226
    :cond_4
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # invokes: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->setEndModeData()V
    invoke-static {v0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$700(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)V

    goto/16 :goto_0

    .line 231
    :sswitch_b
    const-string v0, "AutoPreconfig"

    const-string v1, "Received QUERT_SERVM_DONE response"

    invoke-static {v0, v1}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    if-eqz v7, :cond_5

    .line 234
    const-string v0, "AutoPreconfig"

    const-string v1, "Fail to request Default CFG."

    invoke-static {v0, v1, v11}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 237
    :cond_5
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mIsTestMode:Z
    invoke-static {v0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$800(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Z

    move-result v0

    invoke-static {v0, v11}, Lcom/sec/android/AutoPreconfig/APFileWriter;->setEfsFlag(ZZ)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # invokes: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->runAndroidFactoryReset()V
    invoke-static {v0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$900(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)V

    goto/16 :goto_0

    .line 157
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x3 -> :sswitch_0
        0xa -> :sswitch_6
        0xb -> :sswitch_5
        0xc -> :sswitch_4
        0xd -> :sswitch_7
        0xe -> :sswitch_8
        0x64 -> :sswitch_9
        0x3e9 -> :sswitch_a
        0x3ea -> :sswitch_b
    .end sparse-switch
.end method
