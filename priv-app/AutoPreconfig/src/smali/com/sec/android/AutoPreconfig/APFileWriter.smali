.class public Lcom/sec/android/AutoPreconfig/APFileWriter;
.super Ljava/lang/Object;
.source "APFileWriter.java"


# static fields
.field private static LOG_TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-string v0, "AutoPreconfig"

    sput-object v0, Lcom/sec/android/AutoPreconfig/APFileWriter;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public static GetCurrentTime()Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v9, 0x5

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 160
    const-string v0, ""

    .line 161
    .local v0, "CurrentTime":Ljava/lang/String;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 162
    .local v1, "cal":Ljava/util/Calendar;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const-string v3, "%04d-%02d-%02d %02d:%02d:%02d.%03d"

    const/4 v4, 0x7

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v8}, Ljava/util/Calendar;->get(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    const/4 v5, 0x3

    const/16 v6, 0xb

    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const/16 v6, 0xc

    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0xd

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v9

    const/4 v5, 0x6

    const/16 v6, 0xe

    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 166
    return-object v0
.end method

.method public static getDelayTime()V
    .locals 7

    .prologue
    .line 133
    const-string v4, "/efs/imei/ap_delay.dat"

    invoke-static {v4}, Lcom/sec/android/AutoPreconfig/APFileWriter;->readEfsFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 134
    .local v3, "str":Ljava/lang/String;
    const/4 v0, 0x0

    .line 136
    .local v0, "delay":I
    if-eqz v3, :cond_0

    .line 137
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 138
    sget-object v4, Lcom/sec/android/AutoPreconfig/APFileWriter;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Delay is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v0

    .line 141
    .end local v0    # "delay":I
    .local v1, "delay":I
    :goto_0
    add-int/lit8 v0, v1, -0x1

    .end local v1    # "delay":I
    .restart local v0    # "delay":I
    if-lez v1, :cond_0

    .line 142
    :try_start_0
    sget-object v4, Lcom/sec/android/AutoPreconfig/APFileWriter;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Delay left "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " sec."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-wide/16 v4, 0x3e8

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v0

    .end local v0    # "delay":I
    .restart local v1    # "delay":I
    goto :goto_0

    .line 145
    .end local v1    # "delay":I
    .restart local v0    # "delay":I
    :catch_0
    move-exception v2

    .line 150
    :cond_0
    return-void
.end method

.method public static getEfsFlag()Ljava/lang/String;
    .locals 4

    .prologue
    .line 103
    const-string v1, "/efs/imei/ap_key.dat"

    invoke-static {v1}, Lcom/sec/android/AutoPreconfig/APFileWriter;->readEfsFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 104
    .local v0, "str":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/AutoPreconfig/APFileWriter;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getEfsFlag(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    return-object v0
.end method

.method private static declared-synchronized readEfsFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 16
    const-class v7, Lcom/sec/android/AutoPreconfig/APFileWriter;

    monitor-enter v7

    const/4 v5, 0x0

    .line 17
    .local v5, "str":Ljava/lang/String;
    const/4 v0, 0x0

    .line 20
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/InputStreamReader;

    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v6, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 24
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v0, v1

    .line 36
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    :goto_0
    monitor-exit v7

    return-object v5

    .line 25
    :catch_0
    move-exception v2

    .line 26
    .local v2, "fe":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_2
    sget-object v6, Lcom/sec/android/AutoPreconfig/APFileWriter;->LOG_TAG:Ljava/lang/String;

    const-string v8, "This file doesn\'t exist."

    invoke-static {v6, v8}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 16
    .end local v2    # "fe":Ljava/io/FileNotFoundException;
    :catchall_0
    move-exception v6

    :goto_2
    monitor-exit v7

    throw v6

    .line 27
    :catch_1
    move-exception v3

    .line 28
    .local v3, "ie":Ljava/io/IOException;
    :goto_3
    :try_start_3
    sget-object v6, Lcom/sec/android/AutoPreconfig/APFileWriter;->LOG_TAG:Ljava/lang/String;

    const-string v8, "IO exception during reading/closing file"

    invoke-static {v6, v8}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 30
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 31
    :catch_2
    move-exception v4

    .line 32
    .local v4, "ioe":Ljava/io/IOException;
    :try_start_5
    sget-object v6, Lcom/sec/android/AutoPreconfig/APFileWriter;->LOG_TAG:Ljava/lang/String;

    const-string v8, "IO exception during closing file"

    invoke-static {v6, v8}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 27
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "ie":Ljava/io/IOException;
    .end local v4    # "ioe":Ljava/io/IOException;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catch_3
    move-exception v3

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_3

    .line 25
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_1

    .line 16
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v6

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_2
.end method

.method private static declared-synchronized removeEfsFile(Ljava/lang/String;)V
    .locals 5
    .param p0, "filepath"    # Ljava/lang/String;

    .prologue
    .line 92
    const-class v2, Lcom/sec/android/AutoPreconfig/APFileWriter;

    monitor-enter v2

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 93
    .local v0, "fe":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 95
    sget-object v1, Lcom/sec/android/AutoPreconfig/APFileWriter;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Success to delete "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    :cond_0
    :goto_0
    monitor-exit v2

    return-void

    .line 97
    :cond_1
    :try_start_1
    sget-object v1, Lcom/sec/android/AutoPreconfig/APFileWriter;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Fail to delete "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 92
    .end local v0    # "fe":Ljava/io/File;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static removeEfsFlag()V
    .locals 1

    .prologue
    .line 87
    const-string v0, "/efs/imei/ap_key.dat"

    invoke-static {v0}, Lcom/sec/android/AutoPreconfig/APFileWriter;->removeEfsFile(Ljava/lang/String;)V

    .line 88
    const-string v0, "/efs/imei/ap.log"

    invoke-static {v0}, Lcom/sec/android/AutoPreconfig/APFileWriter;->removeEfsFile(Ljava/lang/String;)V

    .line 89
    return-void
.end method

.method public static setEfsFlag(ZZ)V
    .locals 3
    .param p0, "testMode"    # Z
    .param p1, "executed"    # Z

    .prologue
    const/4 v2, 0x0

    .line 109
    if-nez p0, :cond_0

    .line 110
    sget-object v0, Lcom/sec/android/AutoPreconfig/APFileWriter;->LOG_TAG:Ljava/lang/String;

    const-string v1, "EfsFlag is set."

    invoke-static {v0, v1}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    if-eqz p1, :cond_1

    .line 112
    const-string v0, "/efs/imei/ap_key.dat"

    const-string v1, "TRUE"

    invoke-static {v0, v1, v2}, Lcom/sec/android/AutoPreconfig/APFileWriter;->writeEfsFile(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    const-string v0, "/efs/imei/ap_key.dat"

    const-string v1, "SKIPPED"

    invoke-static {v0, v1, v2}, Lcom/sec/android/AutoPreconfig/APFileWriter;->writeEfsFile(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public static setMpsCode(Ljava/lang/String;)V
    .locals 3
    .param p0, "mpsCode"    # Ljava/lang/String;

    .prologue
    .line 128
    sget-object v0, Lcom/sec/android/AutoPreconfig/APFileWriter;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MpsCode is set to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const-string v0, "/efs/imei/mps_code.dat"

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcom/sec/android/AutoPreconfig/APFileWriter;->writeEfsFile(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 130
    return-void
.end method

.method private static declared-synchronized writeEfsFile(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 9
    .param p0, "fileName"    # Ljava/lang/String;
    .param p1, "contents"    # Ljava/lang/String;
    .param p2, "isContinue"    # Z

    .prologue
    .line 40
    const-class v6, Lcom/sec/android/AutoPreconfig/APFileWriter;

    monitor-enter v6

    :try_start_0
    new-instance v4, Ljava/io/File;

    const-string v5, "/efs/imei"

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 41
    .local v4, "verifyDir":Ljava/io/File;
    const/4 v2, 0x0

    .line 44
    .local v2, "out":Ljava/io/BufferedWriter;
    :try_start_1
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-nez v5, :cond_0

    .line 45
    sget-object v5, Lcom/sec/android/AutoPreconfig/APFileWriter;->LOG_TAG:Ljava/lang/String;

    const-string v7, "No directoy, make imei directoy"

    invoke-static {v5, v7}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 49
    :cond_0
    const/4 v5, 0x1

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v7}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v5

    if-nez v5, :cond_1

    .line 50
    sget-object v5, Lcom/sec/android/AutoPreconfig/APFileWriter;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed setreadable:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    :cond_1
    const/4 v5, 0x1

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v7}, Ljava/io/File;->setExecutable(ZZ)Z

    move-result v5

    if-nez v5, :cond_2

    .line 54
    sget-object v5, Lcom/sec/android/AutoPreconfig/APFileWriter;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed setexecutable:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    :cond_2
    new-instance v3, Ljava/io/BufferedWriter;

    new-instance v5, Ljava/io/OutputStreamWriter;

    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, p0, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V

    invoke-direct {v5, v7}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v3, v5}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 58
    .end local v2    # "out":Ljava/io/BufferedWriter;
    .local v3, "out":Ljava/io/BufferedWriter;
    :try_start_2
    invoke-virtual {v3, p1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 60
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 62
    .local v0, "SetPermission":Ljava/io/File;
    const/4 v5, 0x1

    const/4 v7, 0x0

    invoke-virtual {v0, v5, v7}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v5

    if-nez v5, :cond_3

    .line 63
    sget-object v5, Lcom/sec/android/AutoPreconfig/APFileWriter;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed setreadable:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    :cond_3
    const/4 v5, 0x1

    const/4 v7, 0x0

    invoke-virtual {v0, v5, v7}, Ljava/io/File;->setExecutable(ZZ)Z

    move-result v5

    if-nez v5, :cond_4

    .line 67
    sget-object v5, Lcom/sec/android/AutoPreconfig/APFileWriter;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed setexecutable:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    :cond_4
    const/4 v5, 0x1

    const/4 v7, 0x1

    invoke-virtual {v0, v5, v7}, Ljava/io/File;->setWritable(ZZ)Z

    move-result v5

    if-nez v5, :cond_5

    .line 71
    sget-object v5, Lcom/sec/android/AutoPreconfig/APFileWriter;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed setWritable:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 77
    :cond_5
    if-eqz v3, :cond_6

    .line 78
    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_6
    move-object v2, v3

    .line 84
    .end local v0    # "SetPermission":Ljava/io/File;
    .end local v3    # "out":Ljava/io/BufferedWriter;
    .restart local v2    # "out":Ljava/io/BufferedWriter;
    :cond_7
    :goto_0
    monitor-exit v6

    return-void

    .line 80
    .end local v2    # "out":Ljava/io/BufferedWriter;
    .restart local v0    # "SetPermission":Ljava/io/File;
    .restart local v3    # "out":Ljava/io/BufferedWriter;
    :catch_0
    move-exception v1

    .local v1, "e":Ljava/io/IOException;
    move-object v2, v3

    .line 81
    .end local v3    # "out":Ljava/io/BufferedWriter;
    .restart local v2    # "out":Ljava/io/BufferedWriter;
    goto :goto_0

    .line 73
    .end local v0    # "SetPermission":Ljava/io/File;
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 74
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_4
    sget-object v5, Lcom/sec/android/AutoPreconfig/APFileWriter;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "io exception is occured during writing "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 77
    if-eqz v2, :cond_7

    .line 78
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0

    .line 80
    :catch_2
    move-exception v1

    .line 81
    goto :goto_0

    .line 76
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 77
    :goto_2
    if-eqz v2, :cond_8

    .line 78
    :try_start_6
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 81
    :cond_8
    :try_start_7
    throw v5
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 40
    .end local v2    # "out":Ljava/io/BufferedWriter;
    .end local v4    # "verifyDir":Ljava/io/File;
    :catchall_1
    move-exception v5

    monitor-exit v6

    throw v5

    .line 80
    .restart local v2    # "out":Ljava/io/BufferedWriter;
    .restart local v4    # "verifyDir":Ljava/io/File;
    :catch_3
    move-exception v1

    .line 81
    .restart local v1    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 76
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "out":Ljava/io/BufferedWriter;
    .restart local v3    # "out":Ljava/io/BufferedWriter;
    :catchall_2
    move-exception v5

    move-object v2, v3

    .end local v3    # "out":Ljava/io/BufferedWriter;
    .restart local v2    # "out":Ljava/io/BufferedWriter;
    goto :goto_2

    .line 73
    .end local v2    # "out":Ljava/io/BufferedWriter;
    .restart local v3    # "out":Ljava/io/BufferedWriter;
    :catch_4
    move-exception v1

    move-object v2, v3

    .end local v3    # "out":Ljava/io/BufferedWriter;
    .restart local v2    # "out":Ljava/io/BufferedWriter;
    goto :goto_1
.end method

.method public static writeToLogFile(Ljava/lang/String;)V
    .locals 3
    .param p0, "log"    # Ljava/lang/String;

    .prologue
    .line 154
    const-string v0, ""

    .line 155
    .local v0, "str":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/AutoPreconfig/APFileWriter;->GetCurrentTime()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 156
    const-string v1, "/efs/imei/ap.log"

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lcom/sec/android/AutoPreconfig/APFileWriter;->writeEfsFile(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 157
    return-void
.end method
