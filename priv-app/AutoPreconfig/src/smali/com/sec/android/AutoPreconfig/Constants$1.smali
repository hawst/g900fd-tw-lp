.class final Lcom/sec/android/AutoPreconfig/Constants$1;
.super Ljava/util/HashMap;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/AutoPreconfig/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x2L


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 96
    const-string v0, "english"

    const-string v1, "en_GB"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    const-string v0, "english_us"

    const-string v1, "en_US"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    const-string v0, "german"

    const-string v1, "de_DE"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    const-string v0, "spanish"

    const-string v1, "es_ES"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    const-string v0, "spanish_latin"

    const-string v1, "es_US"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    const-string v0, "french"

    const-string v1, "fr_FR"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    const-string v0, "italian"

    const-string v1, "it,IT"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    const-string v0, "dutch"

    const-string v1, "nl_NL"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    const-string v0, "portuguese"

    const-string v1, "pt_PT"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    const-string v0, "portuguese_latin"

    const-string v1, "pt_BR"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    const-string v0, "polish"

    const-string v1, "pl_PL"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    const-string v0, "czech"

    const-string v1, "cs_CZ"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    const-string v0, "danish"

    const-string v1, "da_DK"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    const-string v0, "finnish"

    const-string v1, "fi_FI"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    const-string v0, "norwegian"

    const-string v1, "nb_NO"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    const-string v0, "swedish"

    const-string v1, "sv_SE"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    const-string v0, "romanian"

    const-string v1, "ro_RO"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    const-string v0, "slovakian"

    const-string v1, "sk_SK"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    const-string v0, "hungarian"

    const-string v1, "hu_HU"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    const-string v0, "serbian"

    const-string v1, "sr_RS"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    const-string v0, "greek"

    const-string v1, "el_GR"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    const-string v0, "albanian"

    const-string v1, "sq_AL"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    const-string v0, "irish"

    const-string v1, "ga_IE"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    const-string v0, "macedonian"

    const-string v1, "mk_MK"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    const-string v0, "maltese"

    const-string v1, "mt_MT"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    const-string v0, "turkish"

    const-string v1, "tr_TR"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    const-string v0, "slovenian"

    const-string v1, "sl_SI"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    const-string v0, "icelandic"

    const-string v1, "is_IS"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    const-string v0, "croatian"

    const-string v1, "hr_HR"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    const-string v0, "russian"

    const-string v1, "ru_RU"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    const-string v0, "japanese"

    const-string v1, "ja_JP"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    const-string v0, "ukrainian"

    const-string v1, "uk_UA"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    const-string v0, "kazakh"

    const-string v1, "kk_KZ"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    const-string v0, "bulgarian"

    const-string v1, "bg_BG"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    const-string v0, "urdu"

    const-string v1, "ur_PK"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    const-string v0, "arabic"

    const-string v1, "ar_AE"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    const-string v0, "farsi"

    const-string v1, "fa_FA"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    const-string v0, "thai"

    const-string v1, "th_TH"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    const-string v0, "bahasa malaysia"

    const-string v1, "ms_MY"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    const-string v0, "indonesia"

    const-string v1, "in_ID"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    const-string v0, "vietnamese"

    const-string v1, "vi_VN"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    const-string v0, "english_philippines"

    const-string v1, "en_PH"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    const-string v0, "chinese_traditional_taiwan"

    const-string v1, "zh_TW"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    const-string v0, "chinese_traditional_hongkong"

    const-string v1, "zh_HK"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    const-string v0, "korean"

    const-string v1, "ko_KR"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    const-string v0, "hebrew"

    const-string v1, "iw_IL"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    const-string v0, "chinese_traditional_taiwan"

    const-string v1, "zh_TW"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    const-string v0, "chinese_traditional_hongkong"

    const-string v1, "zh_HK"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    const-string v0, "chinese_simplified"

    const-string v1, "zh_CN"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/Constants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    return-void
.end method
