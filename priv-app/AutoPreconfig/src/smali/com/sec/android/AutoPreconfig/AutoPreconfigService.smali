.class public Lcom/sec/android/AutoPreconfig/AutoPreconfigService;
.super Landroid/app/Service;
.source "AutoPreconfigService.java"

# interfaces
.implements Lcom/sec/android/AutoPreconfig/Constants;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;,
        Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APBinder;,
        Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;
    }
.end annotation


# static fields
.field public static final mIsDebuggableMonkeyBuild:Z

.field public static final mIsShipBuild:Z


# instance fields
.field private mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

.field private mAPHandlerThread:Landroid/os/HandlerThread;

.field private mApUtils:Lcom/sec/android/AutoPreconfig/APUtils;

.field private final mBinder:Landroid/os/IBinder;

.field private mContext:Landroid/content/Context;

.field private mCscFolderList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFactoryPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

.field mFilteredWithGid1:Z

.field mFilteredWithSpCode:Z

.field mFilteredWithSpName:Z

.field private mIsSucceed:Z

.field private mIsTestMode:Z

.field private mKeyValues:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mMessenger:Landroid/os/Messenger;

.field private mNetworkName:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSalesCode:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSalesInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedID:I

.field private mWaitForChoiceFlag:Z

.field private mWaitForStartConfirmFlag:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 43
    const-string v0, "ro.monkey"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mIsDebuggableMonkeyBuild:Z

    .line 44
    const-string v0, "true"

    const-string v1, "ro.product_ship"

    const-string v2, "false"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mIsShipBuild:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mCscFolderList:Ljava/util/ArrayList;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mNetworkName:Ljava/util/ArrayList;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSalesCode:Ljava/util/ArrayList;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSalesInfoList:Ljava/util/ArrayList;

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    .line 52
    iput-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mContext:Landroid/content/Context;

    .line 53
    iput-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mApUtils:Lcom/sec/android/AutoPreconfig/APUtils;

    .line 54
    iput-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mFactoryPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    .line 56
    iput v1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mWaitForStartConfirmFlag:I

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mWaitForChoiceFlag:Z

    .line 58
    iput v1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSelectedID:I

    .line 60
    iput-boolean v1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mIsTestMode:Z

    .line 61
    iput-boolean v1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mIsSucceed:Z

    .line 63
    iput-boolean v1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mFilteredWithGid1:Z

    .line 64
    iput-boolean v1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mFilteredWithSpCode:Z

    .line 65
    iput-boolean v1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mFilteredWithSpName:Z

    .line 135
    new-instance v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APBinder;

    invoke-direct {v0, p0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APBinder;-><init>(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)V

    iput-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mBinder:Landroid/os/IBinder;

    .line 146
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/AutoPreconfig/AutoPreconfigService;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->checkCondition(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/AutoPreconfig/AutoPreconfigService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->filterMCCMNC(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSalesCode:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Lcom/sec/android/AutoPreconfig/APUtils;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mApUtils:Lcom/sec/android/AutoPreconfig/APUtils;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mWaitForChoiceFlag:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/AutoPreconfig/AutoPreconfigService;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mWaitForChoiceFlag:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSelectedID:I

    return v0
.end method

.method static synthetic access$1602(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/AutoPreconfig/AutoPreconfigService;
    .param p1, "x1"    # I

    .prologue
    .line 38
    iput p1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSelectedID:I

    return p1
.end method

.method static synthetic access$1700(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mCscFolderList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/AutoPreconfig/AutoPreconfigService;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mIsSucceed:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;Landroid/content/Intent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/AutoPreconfig/AutoPreconfigService;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->getIntentValue(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->doPreconfiguration()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mWaitForStartConfirmFlag:I

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/AutoPreconfig/AutoPreconfigService;
    .param p1, "x1"    # I

    .prologue
    .line 38
    iput p1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mWaitForStartConfirmFlag:I

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mNetworkName:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->setEndModeData()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mIsTestMode:Z

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->runAndroidFactoryReset()V

    return-void
.end method

.method private checkCondition(Z)Z
    .locals 5
    .param p1, "isTestMode"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 250
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    .line 251
    .local v0, "userId":I
    if-eqz v0, :cond_0

    .line 252
    const-string v2, "AutoPreconfig"

    const-string v3, "Sub-user cannot run AutoPreconfig"

    invoke-static {v2, v3}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    :goto_0
    return v1

    .line 257
    :cond_0
    iget-object v3, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/AutoPreconfig/APPreference;->getPrefResultFlag(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "TRUE"

    invoke-static {}, Lcom/sec/android/AutoPreconfig/APFileWriter;->getEfsFlag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    if-nez p1, :cond_2

    .line 259
    const-string v3, "AutoPreconfig"

    const-string v4, "AutoPreconfig is already Done."

    invoke-static {v3, v4, v2}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_2
    move v1, v2

    .line 263
    goto :goto_0
.end method

.method private checkSalesCode()Z
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 286
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    const-string v1, "DefaultSalesCode"

    iget-object v4, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mApUtils:Lcom/sec/android/AutoPreconfig/APUtils;

    invoke-virtual {v4}, Lcom/sec/android/AutoPreconfig/APUtils;->getDefaultSalescode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    const-string v1, "CurrentSalesCode"

    iget-object v4, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mApUtils:Lcom/sec/android/AutoPreconfig/APUtils;

    invoke-virtual {v4}, Lcom/sec/android/AutoPreconfig/APUtils;->getCurrentSalescode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    const-string v1, "DefaultSalesCode"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    const-string v4, "CurrentSalesCode"

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 290
    const-string v0, "AutoPreconfig"

    const-string v1, "Current SalesCode(%s) is matched with Default SalesCode(%s)."

    new-array v4, v5, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    const-string v6, "CurrentSalesCode"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v4, v3

    iget-object v3, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    const-string v5, "DefaultSalesCode"

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v4, v2

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;Z)V

    move v0, v2

    .line 296
    :goto_0
    return v0

    .line 294
    :cond_0
    const-string v0, "AutoPreconfig"

    const-string v1, "Default SalesCode/Current SalesCode: %s/%s is not matched."

    new-array v4, v5, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    const-string v6, "DefaultSalesCode"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v4, v3

    iget-object v5, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    const-string v6, "CurrentSalesCode"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;Z)V

    move v0, v3

    .line 296
    goto :goto_0
.end method

.method private doPreconfiguration()V
    .locals 3

    .prologue
    .line 267
    invoke-direct {p0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->checkSalesCode()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->getFolderName()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 268
    invoke-direct {p0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->requestPreconfig()V

    .line 269
    iget-boolean v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mIsTestMode:Z

    if-eqz v0, :cond_0

    .line 270
    iget-boolean v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mIsSucceed:Z

    if-eqz v0, :cond_1

    .line 271
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TestMode: AutoPreconfig succeed in setting "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSalesCode:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSelectedID:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->ToastMessage(Ljava/lang/String;)V

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 274
    :cond_1
    const-string v0, "TestMode: AutoPreconfig is failed"

    invoke-virtual {p0, v0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->ToastMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 278
    :cond_2
    const-string v0, "AutoPreconfig"

    const-string v1, "error during loading Values."

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 279
    iget-boolean v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mIsTestMode:Z

    if-eqz v0, :cond_0

    .line 280
    const-string v0, "TestMode: AutoPreconfig is failed"

    invoke-virtual {p0, v0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->ToastMessage(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private filterMCCMNC(Ljava/lang/String;)Z
    .locals 24
    .param p1, "cscName"    # Ljava/lang/String;

    .prologue
    .line 414
    const/4 v6, 0x0

    .line 415
    .local v6, "networkSize":I
    const/16 v17, 0x0

    .line 421
    .local v17, "result":Z
    const-string v19, "AutoPreconfig"

    const-string v20, "filterMCCMNC(%s)"

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-object p1, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    new-instance v3, Lcom/sec/android/AutoPreconfig/APCscParser;

    const/16 v19, 0x0

    const/16 v20, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Landroid/text/TextUtils;->substring(Ljava/lang/CharSequence;II)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v3, v0}, Lcom/sec/android/AutoPreconfig/APCscParser;-><init>(Ljava/lang/String;)V

    .line 425
    .local v3, "cscParser":Lcom/sec/android/AutoPreconfig/APCscParser;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mApUtils:Lcom/sec/android/AutoPreconfig/APUtils;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/AutoPreconfig/APUtils;->isEnableFeature()Z

    move-result v19

    if-eqz v19, :cond_2

    .line 426
    const-string v19, "FeatureSet.CscFeature_Common_AutoConfigurationType"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Lcom/sec/android/AutoPreconfig/APCscParser;->searchOthers(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v10

    .line 428
    .local v10, "nodeFeatureCsc":Lorg/w3c/dom/Node;
    if-nez v10, :cond_0

    .line 429
    const-string v19, "AutoPreconfig"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " : CscFeature is missed."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    const/16 v19, 0x0

    .line 541
    .end local v10    # "nodeFeatureCsc":Lorg/w3c/dom/Node;
    :goto_0
    return v19

    .line 433
    .restart local v10    # "nodeFeatureCsc":Lorg/w3c/dom/Node;
    :cond_0
    invoke-virtual {v3, v10}, Lcom/sec/android/AutoPreconfig/APCscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v19

    const-string v20, ","

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 434
    .local v4, "featureValue":[Ljava/lang/String;
    array-length v0, v4

    move/from16 v19, v0

    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_1

    .line 435
    const-string v19, "AutoPreconfig"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " : CscFeature has default value : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v3, v10}, Lcom/sec/android/AutoPreconfig/APCscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x0

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    const/16 v19, 0x0

    goto :goto_0

    .line 439
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    move-object/from16 v19, v0

    const-string v20, "DefaultSalesCode"

    invoke-virtual/range {v19 .. v20}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    const/16 v20, 0x0

    aget-object v20, v4, v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_3

    .line 440
    const-string v19, "AutoPreconfig"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " : Sales group(%s) is not matched."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mApUtils:Lcom/sec/android/AutoPreconfig/APUtils;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/AutoPreconfig/APUtils;->getDefaultSalescode()Ljava/lang/String;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 445
    .end local v4    # "featureValue":[Ljava/lang/String;
    .end local v10    # "nodeFeatureCsc":Lorg/w3c/dom/Node;
    :cond_2
    const-string v19, "AutoPreconfig"

    const-string v20, "Current sales code\'s CscFeature doesn\'t exist."

    invoke-static/range {v19 .. v20}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 450
    .restart local v4    # "featureValue":[Ljava/lang/String;
    .restart local v10    # "nodeFeatureCsc":Lorg/w3c/dom/Node;
    :cond_3
    const-string v19, "Settings.Main.Phone.DefLanguageNoSIM"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Lcom/sec/android/AutoPreconfig/APCscParser;->searchCustomer(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v9

    .line 451
    .local v9, "nodeDefLanguageNoSIM":Lorg/w3c/dom/Node;
    const-string v19, "GeneralInfo"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Lcom/sec/android/AutoPreconfig/APCscParser;->searchCustomer(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v11

    .line 452
    .local v11, "nodeGeneralInfo":Lorg/w3c/dom/Node;
    if-nez v11, :cond_4

    .line 453
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 455
    :cond_4
    const-string v19, "Country"

    move-object/from16 v0, v19

    invoke-virtual {v3, v11, v0}, Lcom/sec/android/AutoPreconfig/APCscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v7

    .line 456
    .local v7, "nodeCountry":Lorg/w3c/dom/Node;
    const-string v19, "NetworkInfo"

    move-object/from16 v0, v19

    invoke-virtual {v3, v11, v0}, Lcom/sec/android/AutoPreconfig/APCscParser;->searchCustomerList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v8

    .line 457
    .local v8, "nodeCustomerList":Lorg/w3c/dom/NodeList;
    if-nez v8, :cond_5

    .line 458
    const-string v19, "AutoPreconfig"

    const-string v20, "No network info"

    invoke-static/range {v19 .. v20}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 463
    :cond_5
    invoke-interface {v8}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v6

    .line 464
    const-string v19, "AutoPreconfig"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Network numbers : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    if-ge v5, v6, :cond_e

    .line 469
    invoke-interface {v8, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v19

    const-string v20, "MCCMNC"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/AutoPreconfig/APCscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v13

    .line 470
    .local v13, "nodeMccmnc":Lorg/w3c/dom/Node;
    invoke-interface {v8, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v19

    const-string v20, "NetworkName"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/AutoPreconfig/APCscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v14

    .line 471
    .local v14, "nodeNetwork":Lorg/w3c/dom/Node;
    invoke-interface {v8, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v19

    const-string v20, "SubsetCode"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/AutoPreconfig/APCscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v12

    .line 472
    .local v12, "nodeGid1":Lorg/w3c/dom/Node;
    invoke-interface {v8, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v19

    const-string v20, "SPCode"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/AutoPreconfig/APCscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v15

    .line 473
    .local v15, "nodeSpCode":Lorg/w3c/dom/Node;
    invoke-interface {v8, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v19

    const-string v20, "SPName"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/AutoPreconfig/APCscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v16

    .line 475
    .local v16, "nodeSpName":Lorg/w3c/dom/Node;
    if-eqz v13, :cond_9

    if-eqz v14, :cond_9

    .line 476
    const-string v19, "AutoPreconfig"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "MCCMNC : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v3, v13}, Lcom/sec/android/AutoPreconfig/APCscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " is found."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    const-string v19, "AutoPreconfig"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Gid1   : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v3, v12}, Lcom/sec/android/AutoPreconfig/APCscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " is found."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    const-string v19, "AutoPreconfig"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "SPCode : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v3, v15}, Lcom/sec/android/AutoPreconfig/APCscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " is found."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    const-string v19, "AutoPreconfig"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "SPName : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Lcom/sec/android/AutoPreconfig/APCscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " is found."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    move-object/from16 v19, v0

    const-string v20, "MCCMNC"

    invoke-virtual/range {v19 .. v20}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/CharSequence;

    invoke-virtual {v3, v13}, Lcom/sec/android/AutoPreconfig/APCscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_a

    .line 484
    const-string v19, "AutoPreconfig"

    const-string v20, "mccmnc is matched."

    invoke-static/range {v19 .. v20}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mApUtils:Lcom/sec/android/AutoPreconfig/APUtils;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/AutoPreconfig/APUtils;->getOperatingMode()I

    move-result v19

    const/16 v20, 0xd

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_8

    .line 493
    invoke-virtual {v3, v12}, Lcom/sec/android/AutoPreconfig/APCscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_6

    .line 494
    invoke-virtual {v3, v12}, Lcom/sec/android/AutoPreconfig/APCscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    move-object/from16 v20, v0

    const-string v21, "GID1"

    invoke-virtual/range {v20 .. v21}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_b

    .line 495
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mFilteredWithGid1:Z

    .line 496
    const-string v19, "AutoPreconfig"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "MPS has gid1 "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v3, v12}, Lcom/sec/android/AutoPreconfig/APCscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " and it is matched."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    :cond_6
    invoke-virtual {v3, v15}, Lcom/sec/android/AutoPreconfig/APCscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_7

    .line 505
    invoke-virtual {v3, v15}, Lcom/sec/android/AutoPreconfig/APCscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    move-object/from16 v20, v0

    const-string v21, "SPCODE"

    invoke-virtual/range {v20 .. v21}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_c

    .line 506
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mFilteredWithSpCode:Z

    .line 507
    const-string v19, "AutoPreconfig"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "MPS has spcode "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v3, v15}, Lcom/sec/android/AutoPreconfig/APCscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " and it is matched."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    :cond_7
    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Lcom/sec/android/AutoPreconfig/APCscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_8

    .line 516
    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Lcom/sec/android/AutoPreconfig/APCscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    move-object/from16 v20, v0

    const-string v21, "SPNAME"

    invoke-virtual/range {v20 .. v21}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_d

    .line 517
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mFilteredWithSpName:Z

    .line 518
    const-string v19, "AutoPreconfig"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "MPS has spname "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Lcom/sec/android/AutoPreconfig/APCscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " and it is matched."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    :cond_8
    new-instance v18, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;-><init>(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;Lcom/sec/android/AutoPreconfig/AutoPreconfigService$1;)V

    .line 527
    .local v18, "tmp":Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mSalesCode:Ljava/lang/String;

    .line 528
    invoke-virtual {v3, v14}, Lcom/sec/android/AutoPreconfig/APCscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mNetworkName:Ljava/lang/String;

    .line 529
    invoke-virtual {v3, v12}, Lcom/sec/android/AutoPreconfig/APCscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mGid1:Ljava/lang/String;

    .line 530
    invoke-virtual {v3, v15}, Lcom/sec/android/AutoPreconfig/APCscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mSpCode:Ljava/lang/String;

    .line 531
    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Lcom/sec/android/AutoPreconfig/APCscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mSpName:Ljava/lang/String;

    .line 532
    invoke-virtual {v3, v7}, Lcom/sec/android/AutoPreconfig/APCscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mCountry:Ljava/lang/String;

    .line 533
    invoke-virtual {v3, v9}, Lcom/sec/android/AutoPreconfig/APCscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mLanguage:Ljava/lang/String;

    .line 535
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSalesInfoList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 536
    const/16 v17, 0x1

    .line 467
    .end local v18    # "tmp":Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;
    :cond_9
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    .line 486
    :cond_a
    const-string v19, "AutoPreconfig"

    const-string v20, "mccmnc is not matched."

    invoke-static/range {v19 .. v20}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 498
    :cond_b
    const-string v19, "AutoPreconfig"

    const-string v20, "MPS has gid1 and it is not matched."

    invoke-static/range {v19 .. v20}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 509
    :cond_c
    const-string v19, "AutoPreconfig"

    const-string v20, "MPS has spcode and it is not matched."

    invoke-static/range {v19 .. v20}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 520
    :cond_d
    const-string v19, "AutoPreconfig"

    const-string v20, "MPS has spname and it is not matched."

    invoke-static/range {v19 .. v20}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .end local v12    # "nodeGid1":Lorg/w3c/dom/Node;
    .end local v13    # "nodeMccmnc":Lorg/w3c/dom/Node;
    .end local v14    # "nodeNetwork":Lorg/w3c/dom/Node;
    .end local v15    # "nodeSpCode":Lorg/w3c/dom/Node;
    .end local v16    # "nodeSpName":Lorg/w3c/dom/Node;
    :cond_e
    move/from16 v19, v17

    .line 541
    goto/16 :goto_0
.end method

.method private filterOthers()Z
    .locals 6

    .prologue
    .line 545
    const/4 v2, 0x0

    .line 549
    .local v2, "result":Z
    iget-object v3, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSalesInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 550
    const/4 v2, 0x1

    .line 585
    :cond_0
    :goto_0
    return v2

    .line 553
    :cond_1
    iget-object v3, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSalesInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "itSalesInfo":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;>;"
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 554
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;

    .line 556
    .local v0, "cur":Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;
    iget-boolean v3, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mFilteredWithGid1:Z

    if-eqz v3, :cond_3

    .line 557
    iget-object v3, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    const-string v4, "GID1"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    iget-object v4, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mGid1:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 558
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 559
    const-string v3, "AutoPreconfig"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "filtered with gid1. MPS : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Gid1 :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mGid1:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    :cond_3
    iget-boolean v3, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mFilteredWithSpCode:Z

    if-eqz v3, :cond_4

    .line 565
    iget-object v3, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    const-string v4, "SPCODE"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    iget-object v4, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mSpCode:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 566
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 567
    const-string v3, "AutoPreconfig"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "filtered with spcode. MPS : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Spcode :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mSpCode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    :cond_4
    iget-boolean v3, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mFilteredWithSpName:Z

    if-eqz v3, :cond_2

    .line 573
    iget-object v3, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    const-string v4, "SPNAME"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    iget-object v4, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mSpName:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 574
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 575
    const-string v3, "AutoPreconfig"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "filtered with spname. MPS : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Spname :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mSpName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 580
    .end local v0    # "cur":Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;
    :cond_5
    iget-object v3, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSalesInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_0

    .line 581
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method private getFolderName()Z
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 360
    new-array v1, v4, [Ljava/lang/String;

    const-string v6, ""

    aput-object v6, v1, v5

    .line 362
    .local v1, "dir":[Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    const-string v6, "/system/csc"

    invoke-direct {v2, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 364
    .local v2, "fp":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 365
    new-instance v5, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$1;

    invoke-direct {v5, p0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$1;-><init>(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)V

    invoke-virtual {v2, v5}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v1

    .line 380
    iget-object v5, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mCscFolderList:Ljava/util/ArrayList;

    invoke-static {v5, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 383
    iget-object v5, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mApUtils:Lcom/sec/android/AutoPreconfig/APUtils;

    invoke-virtual {v5}, Lcom/sec/android/AutoPreconfig/APUtils;->getOperatingMode()I

    move-result v5

    const/16 v6, 0xd

    if-eq v5, v6, :cond_0

    .line 384
    invoke-direct {p0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->filterOthers()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 385
    const-string v5, "AutoPreconfig"

    const-string v6, "found proper CSC."

    invoke-static {v5, v6}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSalesInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;

    .line 392
    .local v0, "cur":Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;
    iget-object v5, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSalesCode:Ljava/util/ArrayList;

    iget-object v6, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 393
    iget-object v5, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSalesCode:Ljava/util/ArrayList;

    iget-object v6, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 394
    iget-object v5, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mNetworkName:Ljava/util/ArrayList;

    iget-object v6, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mNetworkName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 395
    iget-object v5, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    const-string v6, "Country"

    iget-object v7, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mCountry:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 396
    iget-object v5, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    const-string v6, "Language"

    iget-object v7, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mLanguage:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 398
    const-string v5, "AutoPreconfig"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SalesCode   : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v4}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 399
    const-string v5, "AutoPreconfig"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "NetworkName : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mNetworkName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v4}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 400
    const-string v5, "AutoPreconfig"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Country     : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mCountry:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v4}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 401
    const-string v5, "AutoPreconfig"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "DefLanguage : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;->mLanguage:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v4}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 387
    .end local v0    # "cur":Lcom/sec/android/AutoPreconfig/AutoPreconfigService$SalesInfo;
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_2
    const-string v5, "AutoPreconfig"

    const-string v6, "couldn\'t found any proper CSC"

    invoke-static {v5, v6}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 407
    :cond_3
    const-string v6, "AutoPreconfig"

    const-string v7, "CSC Path is wrong."

    invoke-static {v6, v7, v4}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;Z)V

    move v4, v5

    .line 408
    :cond_4
    return v4
.end method

.method private getIntentValue(Landroid/content/Intent;)Z
    .locals 11
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 301
    const-string v6, "MCCMNC"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 302
    .local v1, "mccmnc":Ljava/lang/String;
    if-eqz v1, :cond_4

    const-string v6, ""

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 303
    iget-object v6, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    const-string v7, "MCCMNC"

    invoke-virtual {v6, v7, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    const-string v6, "GID1"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 310
    .local v0, "gid1":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v6, ""

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 311
    iget-object v6, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    const-string v7, "GID1"

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    :cond_0
    const-string v6, "SPCODE"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 315
    .local v2, "spcode":Ljava/lang/String;
    if-eqz v2, :cond_1

    const-string v6, ""

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "00"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 316
    iget-object v6, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    const-string v7, "SPCODE"

    invoke-virtual {v6, v7, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 319
    :cond_1
    const-string v6, "SPNAME"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 320
    .local v3, "spname":Ljava/lang/String;
    if-eqz v3, :cond_2

    const-string v6, ""

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 321
    iget-object v6, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    const-string v7, "SPNAME"

    invoke-virtual {v6, v7, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    :cond_2
    sget-boolean v6, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mIsShipBuild:Z

    if-nez v6, :cond_3

    const-string v6, "TRUE"

    const-string v7, "TEST"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 331
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "TestMode: AutoPreconfig test mode ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mApUtils:Lcom/sec/android/AutoPreconfig/APUtils;

    invoke-virtual {v7}, Lcom/sec/android/AutoPreconfig/APUtils;->getOperatingModeName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->ToastMessage(Ljava/lang/String;)V

    .line 332
    iput-boolean v4, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mIsTestMode:Z

    .line 334
    iget-object v6, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mApUtils:Lcom/sec/android/AutoPreconfig/APUtils;

    const-string v7, "MPS"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "MODE"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/AutoPreconfig/APUtils;->setTestMode(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 335
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "TestMode: Configurations are set with ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mApUtils:Lcom/sec/android/AutoPreconfig/APUtils;

    invoke-virtual {v7}, Lcom/sec/android/AutoPreconfig/APUtils;->getDefaultSalescode()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mApUtils:Lcom/sec/android/AutoPreconfig/APUtils;

    invoke-virtual {v7}, Lcom/sec/android/AutoPreconfig/APUtils;->getOperatingModeName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->ToastMessage(Ljava/lang/String;)V

    .line 342
    :cond_3
    iget-object v6, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    const-string v7, "MCCMNC"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 343
    const-string v6, "AutoPreconfig"

    const-string v7, "MCCMNC/GID1/SPCODE/SPNAME : %s/%s/%s/%s"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    const-string v10, "MCCMNC"

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v8, v5

    iget-object v5, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    const-string v9, "GID1"

    invoke-virtual {v5, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v8, v4

    const/4 v5, 0x2

    iget-object v9, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    const-string v10, "SPCODE"

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v8, v5

    const/4 v5, 0x3

    iget-object v9, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;

    const-string v10, "SPNAME"

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v8, v5

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5, v4}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 353
    .end local v0    # "gid1":Ljava/lang/String;
    .end local v2    # "spcode":Ljava/lang/String;
    .end local v3    # "spname":Ljava/lang/String;
    :goto_0
    return v4

    .line 305
    :cond_4
    const-string v4, "AutoPreconfig"

    const-string v6, "Mcc/mnc isn\'t passed. Cannot continue anymore."

    invoke-static {v4, v6}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v5

    .line 306
    goto :goto_0

    .line 352
    .restart local v0    # "gid1":Ljava/lang/String;
    .restart local v2    # "spcode":Ljava/lang/String;
    .restart local v3    # "spname":Ljava/lang/String;
    :cond_5
    const-string v6, "AutoPreconfig"

    const-string v7, "mccmnc is null."

    invoke-static {v6, v7, v4}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;Z)V

    move v4, v5

    .line 353
    goto :goto_0
.end method

.method private requestPreconfig()V
    .locals 2

    .prologue
    .line 589
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;

    invoke-direct {v1, p0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;-><init>(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 675
    return-void
.end method

.method private runAndroidFactoryReset()V
    .locals 2

    .prologue
    .line 829
    const-string v0, "AutoPreconfig"

    const-string v1, "Do factory reset"

    invoke-static {v0, v1}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 830
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MASTER_CLEAR"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->sendBroadcast(Landroid/content/Intent;)V

    .line 831
    return-void
.end method

.method private sendRequestToRil(Ljava/lang/String;)V
    .locals 8
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 797
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 798
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 799
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v4, 0x3

    invoke-virtual {p1, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 800
    const-string v4, "AutoPreconfig"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "set user input code : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    array-length v4, v4

    add-int/lit8 v3, v4, 0x5

    .line 804
    .local v3, "fileSize":I
    const/4 v4, 0x6

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 805
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 806
    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 807
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 808
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 809
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    array-length v6, v6

    invoke-virtual {v1, v4, v5, v6}, Ljava/io/DataOutputStream;->write([BII)V

    .line 811
    iget-object v4, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mFactoryPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    const/16 v7, 0x3e9

    invoke-virtual {v6, v7}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 816
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 821
    .end local v3    # "fileSize":I
    :goto_0
    return-void

    .line 817
    .restart local v3    # "fileSize":I
    :catch_0
    move-exception v2

    .line 818
    .local v2, "e":Ljava/io/IOException;
    goto :goto_0

    .line 812
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fileSize":I
    :catch_1
    move-exception v2

    .line 816
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 817
    :catch_2
    move-exception v2

    .line 818
    goto :goto_0

    .line 815
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 816
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 818
    throw v4

    .line 817
    :catch_3
    move-exception v2

    .line 818
    .restart local v2    # "e":Ljava/io/IOException;
    goto :goto_0
.end method

.method private setEndModeData()V
    .locals 8

    .prologue
    .line 762
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 763
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 764
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v3, 0x4

    .line 767
    .local v3, "fileSize":I
    const/16 v4, 0xc

    :try_start_0
    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 768
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 769
    const/4 v4, 0x5

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 770
    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 776
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 782
    iget-object v4, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mFactoryPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    const/16 v7, 0x3ea

    invoke-virtual {v6, v7}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    .line 783
    :goto_0
    return-void

    .line 777
    :catch_0
    move-exception v2

    .line 778
    .local v2, "e":Ljava/io/IOException;
    goto :goto_0

    .line 771
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 772
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_2
    const-string v4, "AutoPreconfig"

    const-string v5, "IOException in getServMQueryData!!!"

    invoke-static {v4, v5}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 776
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 777
    :catch_2
    move-exception v2

    .line 778
    goto :goto_0

    .line 775
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 776
    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 778
    throw v4

    .line 777
    :catch_3
    move-exception v2

    .line 778
    .restart local v2    # "e":Ljava/io/IOException;
    goto :goto_0
.end method


# virtual methods
.method ToastMessage(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 824
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 825
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 121
    if-eqz p1, :cond_0

    .line 122
    const-string v0, "AutoPreconfig"

    const-string v1, "bounded with telephony fw"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mMessenger:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 86
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 87
    const-string v0, "AutoPreconfig"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    iput-object p0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mContext:Landroid/content/Context;

    .line 90
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "APHandler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandlerThread:Landroid/os/HandlerThread;

    .line 91
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 92
    new-instance v0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    iget-object v1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;-><init>(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    .line 93
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mMessenger:Landroid/os/Messenger;

    .line 94
    new-instance v0, Lcom/samsung/android/sec_platform_library/FactoryPhone;

    iget-object v1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sec_platform_library/FactoryPhone;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mFactoryPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    .line 95
    new-instance v0, Lcom/sec/android/AutoPreconfig/APUtils;

    iget-object v1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/AutoPreconfig/APUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mApUtils:Lcom/sec/android/AutoPreconfig/APUtils;

    .line 96
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 140
    const-string v0, "AutoPreconfig"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z

    .line 142
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mFactoryPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-virtual {v0}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->disconnectFromRilService()V

    .line 143
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 144
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 100
    const-string v1, "AutoPreconfig"

    const-string v2, "onStartCommand()"

    invoke-static {v1, v2}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    if-eq p3, v3, :cond_0

    .line 102
    const-string v1, "AutoPreconfig"

    const-string v2, "Service was already running. Stop service"

    invoke-static {v1, v2}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    iget-object v1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    invoke-virtual {v2, v4}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->sendMessage(Landroid/os/Message;)Z

    .line 116
    :goto_0
    return v4

    .line 104
    :cond_0
    const-string v1, "com.sec.android.AutoPreconfig.AUTO_PRECONFIG"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, v3}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->checkCondition(Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 106
    const-string v1, "AutoPreconfig"

    const-string v2, "onStartCommand() : test mode"

    invoke-static {v1, v2, v3}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 107
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 108
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    .line 109
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 110
    iget-object v1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 112
    .end local v0    # "msg":Landroid/os/Message;
    :cond_1
    const-string v1, "AutoPreconfig"

    const-string v2, " onStartCommand : error during checking conditions."

    invoke-static {v1, v2, v3}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 113
    iget-object v1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    invoke-virtual {v2, v4}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method protected setSalesCode(Ljava/lang/String;)V
    .locals 3
    .param p1, "salesCode"    # Ljava/lang/String;

    .prologue
    .line 786
    iget-boolean v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mIsTestMode:Z

    if-eqz v0, :cond_0

    .line 787
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    iget-object v1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->sendMessage(Landroid/os/Message;)Z

    .line 794
    :goto_0
    return-void

    .line 791
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->sendRequestToRil(Ljava/lang/String;)V

    .line 793
    iget-object v0, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSalesCode:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSelectedID:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/AutoPreconfig/APFileWriter;->setMpsCode(Ljava/lang/String;)V

    goto :goto_0
.end method

.method showAlertPanel(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "text"    # Ljava/lang/String;
    .param p5, "itemList"    # [Ljava/lang/String;

    .prologue
    const v4, 0x104000a

    const/high16 v8, 0x200000

    const/16 v7, 0x7d9

    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 678
    const/4 v0, 0x0

    .line 679
    .local v0, "alertDialog":Landroid/app/AlertDialog;
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 681
    .local v1, "res":Landroid/content/res/Resources;
    packed-switch p2, :pswitch_data_0

    .line 758
    :goto_0
    return-void

    .line 683
    :pswitch_0
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, p3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x1080027

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, p4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$4;

    invoke-direct {v3, p0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$4;-><init>(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)V

    invoke-virtual {v2, v4, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/high16 v3, 0x1040000

    new-instance v4, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$3;

    invoke-direct {v4, p0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$3;-><init>(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 699
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/view/Window;->setType(I)V

    .line 700
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/view/Window;->addFlags(I)V

    .line 701
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/view/Window;->addFlags(I)V

    .line 702
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 706
    :pswitch_1
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, p3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x1080027

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, p4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$7;

    invoke-direct {v3, p0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$7;-><init>(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)V

    invoke-virtual {v2, v4, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/high16 v3, 0x1040000

    new-instance v4, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$6;

    invoke-direct {v4, p0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$6;-><init>(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f030005

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$5;

    invoke-direct {v4, p0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$5;-><init>(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 727
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/view/Window;->setType(I)V

    .line 728
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/view/Window;->addFlags(I)V

    .line 729
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/view/Window;->addFlags(I)V

    .line 730
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 734
    :pswitch_2
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, p3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$9;

    invoke-direct {v3, p0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$9;-><init>(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)V

    invoke-virtual {v2, p5, v5, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$8;

    invoke-direct {v3, p0}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$8;-><init>(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)V

    invoke-virtual {v2, v4, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 748
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/view/Window;->setType(I)V

    .line 749
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/view/Window;->addFlags(I)V

    .line 750
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/view/Window;->addFlags(I)V

    .line 751
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 681
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
