.class Lcom/sec/android/AutoPreconfig/AutoPreconfigService$1;
.super Ljava/lang/Object;
.source "AutoPreconfigService.java"

# interfaces
.implements Ljava/io/FilenameFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->getFolderName()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;


# direct methods
.method constructor <init>(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)V
    .locals 0

    .prologue
    .line 365
    iput-object p1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$1;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Ljava/io/File;Ljava/lang/String;)Z
    .locals 4
    .param p1, "dir"    # Ljava/io/File;
    .param p2, "cscName"    # Ljava/lang/String;

    .prologue
    .line 368
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/system/csc/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 369
    .local v1, "path":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 370
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 371
    const-string v2, "AutoPreconfig"

    const-string v3, "CSC Path is OK."

    invoke-static {v2, v3}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$1;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # invokes: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->filterMCCMNC(Ljava/lang/String;)Z
    invoke-static {v2, p2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1000(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;Ljava/lang/String;)Z

    move-result v2

    :goto_0
    return v2

    .line 373
    :cond_0
    const-string v2, "AutoPreconfig"

    const-string v3, "CSC Path is wrong."

    invoke-static {v2, v3}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    const/4 v2, 0x0

    goto :goto_0
.end method
