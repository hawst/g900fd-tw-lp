.class public Lcom/sec/android/AutoPreconfig/APCscParser;
.super Ljava/lang/Object;
.source "APCscParser.java"


# static fields
.field private static LOG_TAG:Ljava/lang/String;

.field private static mCustomerFileName:Ljava/lang/String;

.field private static mOthersFileName:Ljava/lang/String;


# instance fields
.field private mDocCustomer:Lorg/w3c/dom/Document;

.field private mDocOthers:Lorg/w3c/dom/Document;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-string v0, "AutoPreconfig"

    sput-object v0, Lcom/sec/android/AutoPreconfig/APCscParser;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object v0, p0, Lcom/sec/android/AutoPreconfig/APCscParser;->mDocCustomer:Lorg/w3c/dom/Document;

    .line 28
    iput-object v0, p0, Lcom/sec/android/AutoPreconfig/APCscParser;->mDocOthers:Lorg/w3c/dom/Document;

    .line 31
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "salesCode"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object v1, p0, Lcom/sec/android/AutoPreconfig/APCscParser;->mDocCustomer:Lorg/w3c/dom/Document;

    .line 28
    iput-object v1, p0, Lcom/sec/android/AutoPreconfig/APCscParser;->mDocOthers:Lorg/w3c/dom/Document;

    .line 34
    invoke-static {p1}, Lcom/sec/android/AutoPreconfig/APCscParser;->updateMPSPath(Ljava/lang/String;)V

    .line 37
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/AutoPreconfig/APCscParser;->updateDocument()V
    :try_end_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 45
    :goto_0
    return-void

    .line 38
    :catch_0
    move-exception v0

    .line 39
    .local v0, "e":Ljavax/xml/parsers/ParserConfigurationException;
    invoke-virtual {v0}, Ljavax/xml/parsers/ParserConfigurationException;->printStackTrace()V

    goto :goto_0

    .line 40
    .end local v0    # "e":Ljavax/xml/parsers/ParserConfigurationException;
    :catch_1
    move-exception v0

    .line 41
    .local v0, "e":Lorg/xml/sax/SAXException;
    invoke-virtual {v0}, Lorg/xml/sax/SAXException;->printStackTrace()V

    goto :goto_0

    .line 42
    .end local v0    # "e":Lorg/xml/sax/SAXException;
    :catch_2
    move-exception v0

    .line 43
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private updateDocument()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/xml/parsers/ParserConfigurationException;,
            Lorg/xml/sax/SAXException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v1

    .line 65
    .local v1, "customerBuilderFactory":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v1}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    .line 66
    .local v0, "customerBuilder":Ljavax/xml/parsers/DocumentBuilder;
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v5

    .line 67
    .local v5, "othersBuilderFactory":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v5}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v4

    .line 69
    .local v4, "othersBuilder":Ljavax/xml/parsers/DocumentBuilder;
    new-instance v2, Ljava/io/File;

    sget-object v6, Lcom/sec/android/AutoPreconfig/APCscParser;->mCustomerFileName:Ljava/lang/String;

    invoke-direct {v2, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 70
    .local v2, "fiCustomer":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    sget-object v6, Lcom/sec/android/AutoPreconfig/APCscParser;->mOthersFileName:Ljava/lang/String;

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 72
    .local v3, "fiOthers":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 74
    invoke-virtual {v0, v2}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/File;)Lorg/w3c/dom/Document;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/AutoPreconfig/APCscParser;->mDocCustomer:Lorg/w3c/dom/Document;

    .line 79
    :goto_0
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 81
    invoke-virtual {v4, v3}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/File;)Lorg/w3c/dom/Document;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/AutoPreconfig/APCscParser;->mDocOthers:Lorg/w3c/dom/Document;

    .line 85
    :goto_1
    return-void

    .line 76
    :cond_0
    sget-object v6, Lcom/sec/android/AutoPreconfig/APCscParser;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "updateDocument(): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/sec/android/AutoPreconfig/APCscParser;->mCustomerFileName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " is not exist"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 83
    :cond_1
    sget-object v6, Lcom/sec/android/AutoPreconfig/APCscParser;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "updateDocument(): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/sec/android/AutoPreconfig/APCscParser;->mOthersFileName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " is not exist"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static updateMPSPath(Ljava/lang/String;)V
    .locals 3
    .param p0, "salesCode"    # Ljava/lang/String;

    .prologue
    .line 48
    const-string v1, "/system/csc/customer.xml"

    sput-object v1, Lcom/sec/android/AutoPreconfig/APCscParser;->mCustomerFileName:Ljava/lang/String;

    .line 49
    const-string v1, "/system/csc/others.xml"

    sput-object v1, Lcom/sec/android/AutoPreconfig/APCscParser;->mOthersFileName:Ljava/lang/String;

    .line 51
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 52
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/system/csc/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 53
    .local v0, "fi":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 54
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/system/csc/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/system/csc/customer.xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/AutoPreconfig/APCscParser;->mCustomerFileName:Ljava/lang/String;

    .line 55
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/system/csc/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/system/csc/others.xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/AutoPreconfig/APCscParser;->mOthersFileName:Ljava/lang/String;

    .line 61
    .end local v0    # "fi":Ljava/io/File;
    :cond_0
    return-void
.end method


# virtual methods
.method public getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;
    .locals 4
    .param p1, "node"    # Lorg/w3c/dom/Node;

    .prologue
    .line 238
    if-nez p1, :cond_0

    .line 239
    const/4 v2, 0x0

    .line 250
    :goto_0
    return-object v2

    .line 242
    :cond_0
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v2

    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_2

    .line 243
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 244
    .local v1, "stringBuffer":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "idx":I
    :goto_1
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v2

    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 245
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v2

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    invoke-interface {v2}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 244
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 247
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 250
    .end local v0    # "idx":I
    .end local v1    # "stringBuffer":Ljava/lang/StringBuffer;
    :cond_2
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v2

    invoke-interface {v2}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public search(Ljava/lang/String;I)Lorg/w3c/dom/Node;
    .locals 5
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    const/4 v3, 0x0

    .line 128
    if-nez p1, :cond_1

    move-object v0, v3

    .line 157
    :cond_0
    :goto_0
    return-object v0

    .line 132
    :cond_1
    iget-object v4, p0, Lcom/sec/android/AutoPreconfig/APCscParser;->mDocCustomer:Lorg/w3c/dom/Document;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/AutoPreconfig/APCscParser;->mDocOthers:Lorg/w3c/dom/Document;

    if-nez v4, :cond_3

    :cond_2
    move-object v0, v3

    .line 133
    goto :goto_0

    .line 137
    :cond_3
    if-nez p2, :cond_5

    .line 138
    iget-object v4, p0, Lcom/sec/android/AutoPreconfig/APCscParser;->mDocCustomer:Lorg/w3c/dom/Document;

    invoke-interface {v4}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 145
    .local v0, "node":Lorg/w3c/dom/Node;
    :goto_1
    new-instance v2, Ljava/util/StringTokenizer;

    const-string v4, "."

    invoke-direct {v2, p1, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    .local v2, "tokenizer":Ljava/util/StringTokenizer;
    :cond_4
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 147
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 148
    .local v1, "token":Ljava/lang/String;
    if-nez v1, :cond_7

    move-object v0, v3

    .line 149
    goto :goto_0

    .line 139
    .end local v0    # "node":Lorg/w3c/dom/Node;
    .end local v1    # "token":Ljava/lang/String;
    .end local v2    # "tokenizer":Ljava/util/StringTokenizer;
    :cond_5
    const/4 v4, 0x1

    if-ne p2, v4, :cond_6

    .line 140
    iget-object v4, p0, Lcom/sec/android/AutoPreconfig/APCscParser;->mDocOthers:Lorg/w3c/dom/Document;

    invoke-interface {v4}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    .restart local v0    # "node":Lorg/w3c/dom/Node;
    goto :goto_1

    .end local v0    # "node":Lorg/w3c/dom/Node;
    :cond_6
    move-object v0, v3

    .line 142
    goto :goto_0

    .line 151
    .restart local v0    # "node":Lorg/w3c/dom/Node;
    .restart local v1    # "token":Ljava/lang/String;
    .restart local v2    # "tokenizer":Ljava/util/StringTokenizer;
    :cond_7
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/AutoPreconfig/APCscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 152
    if-nez v0, :cond_4

    move-object v0, v3

    .line 153
    goto :goto_0
.end method

.method public search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;
    .locals 5
    .param p1, "parent"    # Lorg/w3c/dom/Node;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 161
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 162
    .local v1, "children":Lorg/w3c/dom/NodeList;
    if-eqz v1, :cond_1

    .line 163
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v3

    .line 165
    .local v3, "n":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 166
    invoke-interface {v1, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 167
    .local v0, "child":Lorg/w3c/dom/Node;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 172
    .end local v0    # "child":Lorg/w3c/dom/Node;
    .end local v2    # "i":I
    .end local v3    # "n":I
    :goto_1
    return-object v0

    .line 165
    .restart local v0    # "child":Lorg/w3c/dom/Node;
    .restart local v2    # "i":I
    .restart local v3    # "n":I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 172
    .end local v0    # "child":Lorg/w3c/dom/Node;
    .end local v2    # "i":I
    .end local v3    # "n":I
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public searchCustomer(Ljava/lang/String;)Lorg/w3c/dom/Node;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 103
    if-nez p1, :cond_0

    .line 104
    const/4 v0, 0x0

    .line 107
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/AutoPreconfig/APCscParser;->search(Ljava/lang/String;I)Lorg/w3c/dom/Node;

    move-result-object v0

    goto :goto_0
.end method

.method public searchCustomerList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;
    .locals 1
    .param p1, "parent"    # Lorg/w3c/dom/Node;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 198
    if-nez p1, :cond_0

    .line 199
    const/4 v0, 0x0

    .line 202
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/AutoPreconfig/APCscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;I)Lorg/w3c/dom/NodeList;

    move-result-object v0

    goto :goto_0
.end method

.method public searchList(Lorg/w3c/dom/Node;Ljava/lang/String;I)Lorg/w3c/dom/NodeList;
    .locals 7
    .param p1, "parent"    # Lorg/w3c/dom/Node;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "index"    # I

    .prologue
    const/4 v5, 0x0

    .line 206
    if-nez p1, :cond_1

    .line 234
    :cond_0
    :goto_0
    return-object v5

    .line 210
    :cond_1
    iget-object v6, p0, Lcom/sec/android/AutoPreconfig/APCscParser;->mDocCustomer:Lorg/w3c/dom/Document;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/AutoPreconfig/APCscParser;->mDocOthers:Lorg/w3c/dom/Document;

    if-eqz v6, :cond_0

    .line 215
    if-nez p3, :cond_3

    .line 216
    iget-object v5, p0, Lcom/sec/android/AutoPreconfig/APCscParser;->mDocCustomer:Lorg/w3c/dom/Document;

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v3

    .line 223
    .local v3, "list":Lorg/w3c/dom/Element;
    :goto_1
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 224
    .local v1, "children":Lorg/w3c/dom/NodeList;
    if-eqz v1, :cond_4

    .line 225
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v4

    .line 226
    .local v4, "n":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-ge v2, v4, :cond_4

    .line 227
    invoke-interface {v1, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 228
    .local v0, "child":Lorg/w3c/dom/Node;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 229
    invoke-interface {v3, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 226
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 217
    .end local v0    # "child":Lorg/w3c/dom/Node;
    .end local v1    # "children":Lorg/w3c/dom/NodeList;
    .end local v2    # "i":I
    .end local v3    # "list":Lorg/w3c/dom/Element;
    .end local v4    # "n":I
    :cond_3
    const/4 v6, 0x1

    if-ne p3, v6, :cond_0

    .line 218
    iget-object v5, p0, Lcom/sec/android/AutoPreconfig/APCscParser;->mDocOthers:Lorg/w3c/dom/Document;

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v3

    .restart local v3    # "list":Lorg/w3c/dom/Element;
    goto :goto_1

    .line 234
    .restart local v1    # "children":Lorg/w3c/dom/NodeList;
    :cond_4
    invoke-interface {v3}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v5

    goto :goto_0
.end method

.method public searchOthers(Ljava/lang/String;)Lorg/w3c/dom/Node;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 111
    if-nez p1, :cond_0

    .line 112
    const/4 v0, 0x0

    .line 115
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/AutoPreconfig/APCscParser;->search(Ljava/lang/String;I)Lorg/w3c/dom/Node;

    move-result-object v0

    goto :goto_0
.end method
