.class Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;
.super Ljava/lang/Object;
.source "AutoPreconfigService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->requestPreconfig()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;


# direct methods
.method constructor <init>(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)V
    .locals 0

    .prologue
    .line 589
    iput-object p1, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 592
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSalesCode:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1200(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 594
    const-string v2, "AutoPreconfig"

    const-string v3, "There is no sales code matched with mccmnc."

    invoke-static {v2, v3, v6}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 595
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mApUtils:Lcom/sec/android/AutoPreconfig/APUtils;
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1300(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Lcom/sec/android/AutoPreconfig/APUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/AutoPreconfig/APUtils;->getOperatingMode()I

    move-result v2

    const/16 v3, 0xb

    if-ne v2, v3, :cond_0

    .line 596
    const-string v2, "AutoPreconfig"

    const-string v3, "Boot with another operator. mark SKIPPED."

    invoke-static {v2, v3, v6}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 597
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mIsTestMode:Z
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$800(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Z

    move-result v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/AutoPreconfig/APFileWriter;->setEfsFlag(ZZ)V

    .line 599
    :cond_0
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1400(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;
    invoke-static {v3}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1400(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->sendMessage(Landroid/os/Message;)Z

    .line 672
    :goto_0
    return-void

    .line 603
    :cond_1
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mApUtils:Lcom/sec/android/AutoPreconfig/APUtils;
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1300(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Lcom/sec/android/AutoPreconfig/APUtils;

    move-result-object v3

    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$500(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Ljava/util/HashMap;

    move-result-object v2

    const-string v4, "Language"

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Lcom/sec/android/AutoPreconfig/APUtils;->setLocale(Ljava/lang/String;)V

    .line 605
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1400(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;
    invoke-static {v3}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1400(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mApUtils:Lcom/sec/android/AutoPreconfig/APUtils;
    invoke-static {v4}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1300(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Lcom/sec/android/AutoPreconfig/APUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/AutoPreconfig/APUtils;->getOperatingMode()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->sendMessage(Landroid/os/Message;)Z

    .line 608
    :goto_1
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mWaitForStartConfirmFlag:I
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$400(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-ne v2, v6, :cond_2

    .line 630
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSalesCode:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1200(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, v6, :cond_7

    .line 631
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mApUtils:Lcom/sec/android/AutoPreconfig/APUtils;
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1300(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Lcom/sec/android/AutoPreconfig/APUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/AutoPreconfig/APUtils;->getOperatingMode()I

    move-result v2

    const/16 v3, 0xc

    if-ne v2, v3, :cond_5

    .line 632
    const-string v2, "AutoPreconfig"

    const-string v3, "Too many sales code was filtered."

    invoke-static {v2, v3, v6}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 633
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1400(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;
    invoke-static {v3}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1400(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 610
    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mWaitForStartConfirmFlag:I
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$400(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)I

    move-result v2

    if-ne v2, v7, :cond_3

    .line 611
    const-string v2, "AutoPreconfig"

    const-string v3, "Autopreconfig will start again later."

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/sec/android/AutoPreconfig/Log;->d(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 612
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1400(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;
    invoke-static {v3}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1400(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 624
    :catch_0
    move-exception v0

    .line 625
    .local v0, "e":Ljava/lang/InterruptedException;
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1400(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;
    invoke-static {v3}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1400(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 614
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_3
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mWaitForStartConfirmFlag:I
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$400(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_4

    .line 615
    const-string v2, "AutoPreconfig"

    const-string v3, "Autopreconfig is canceled."

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/sec/android/AutoPreconfig/Log;->e(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 616
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mIsTestMode:Z
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$800(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Z

    move-result v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/android/AutoPreconfig/APFileWriter;->setEfsFlag(ZZ)V

    .line 617
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1400(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;
    invoke-static {v3}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1400(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 621
    :cond_4
    const-wide/16 v2, 0x1f4

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 622
    const-string v2, "AutoPreconfig"

    const-string v3, "Waiting for changing state."

    invoke-static {v2, v3}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    .line 637
    :cond_5
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1400(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;
    invoke-static {v3}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1400(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    move-result-object v3

    const/16 v4, 0x64

    invoke-virtual {v3, v4}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->sendMessage(Landroid/os/Message;)Z

    .line 640
    :goto_2
    :try_start_3
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mWaitForChoiceFlag:Z
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1500(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 641
    const-string v2, "AutoPreconfig"

    const-string v3, "Waiting for selecting sales code."

    invoke-static {v2, v3}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    const-wide/16 v2, 0x1f4

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    .line 644
    :catch_1
    move-exception v0

    .line 645
    .restart local v0    # "e":Ljava/lang/InterruptedException;
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1400(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;
    invoke-static {v3}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1400(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 649
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_6
    const-string v2, "AutoPreconfig"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "selected ID: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSelectedID:I
    invoke-static {v4}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1600(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 652
    :cond_7
    sget-boolean v2, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mIsShipBuild:Z

    if-nez v2, :cond_8

    .line 653
    invoke-static {}, Lcom/sec/android/AutoPreconfig/APFileWriter;->getDelayTime()V

    .line 656
    :cond_8
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mKeyValues:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$500(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Ljava/util/HashMap;

    move-result-object v2

    const-string v3, "CurrentSalesCode"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 657
    .local v1, "keyCurSalesCode":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSalesCode:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1200(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSelectedID:I
    invoke-static {v3}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1600(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 658
    const-string v3, "AutoPreconfig"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Default salescode is same with "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSalesCode:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1200(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v5, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSelectedID:I
    invoke-static {v5}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1600(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ". So, it doesn\'t need to do factory reset."

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2, v6}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 660
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mIsTestMode:Z
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$800(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Z

    move-result v2

    invoke-static {v2, v6}, Lcom/sec/android/AutoPreconfig/APFileWriter;->setEfsFlag(ZZ)V

    .line 670
    :cond_9
    :goto_3
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # setter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mIsSucceed:Z
    invoke-static {v2, v6}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1802(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;Z)Z

    .line 671
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1400(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mAPHandler:Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;
    invoke-static {v3}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1400(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$APHandler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 662
    :cond_a
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mCscFolderList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1700(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSalesCode:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1200(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSelectedID:I
    invoke-static {v4}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1600(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 663
    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$000(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/APPreference;->setPrefCscModemBlockFlag(Landroid/content/Context;)V

    .line 664
    iget-object v3, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSalesCode:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1200(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSelectedID:I
    invoke-static {v4}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1600(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->setSalesCode(Ljava/lang/String;)V

    .line 666
    const-string v3, "AutoPreconfig"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AutoPreconfig succeed in setting "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v2, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSalesCode:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1200(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v5, p0, Lcom/sec/android/AutoPreconfig/AutoPreconfigService$2;->this$0:Lcom/sec/android/AutoPreconfig/AutoPreconfigService;

    # getter for: Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->mSelectedID:I
    invoke-static {v5}, Lcom/sec/android/AutoPreconfig/AutoPreconfigService;->access$1600(Lcom/sec/android/AutoPreconfig/AutoPreconfigService;)I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2, v6}, Lcom/sec/android/AutoPreconfig/Log;->i(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_3
.end method
