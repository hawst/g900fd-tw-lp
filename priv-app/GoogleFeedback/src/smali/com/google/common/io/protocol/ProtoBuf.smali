.class public Lcom/google/common/io/protocol/ProtoBuf;
.super Ljava/lang/Object;
.source "ProtoBuf.java"


# static fields
.field public static final FALSE:Ljava/lang/Boolean;

.field private static SMALL_NUMBERS:[Ljava/lang/Long;

.field public static final TRUE:Ljava/lang/Boolean;


# instance fields
.field private msgType:Lcom/google/common/io/protocol/ProtoBufType;

.field private final values:Ljava/util/Vector;

.field private final wireTypes:Ljava/lang/StringBuffer;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 40
    new-instance v0, Ljava/lang/Boolean;

    invoke-direct {v0, v4}, Ljava/lang/Boolean;-><init>(Z)V

    sput-object v0, Lcom/google/common/io/protocol/ProtoBuf;->FALSE:Ljava/lang/Boolean;

    .line 41
    new-instance v0, Ljava/lang/Boolean;

    invoke-direct {v0, v5}, Ljava/lang/Boolean;-><init>(Z)V

    sput-object v0, Lcom/google/common/io/protocol/ProtoBuf;->TRUE:Ljava/lang/Boolean;

    .line 58
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/Long;

    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x0

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    aput-object v1, v0, v4

    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    aput-object v1, v0, v5

    const/4 v1, 0x2

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v4, 0x2

    invoke-direct {v2, v4, v5}, Ljava/lang/Long;-><init>(J)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v4, 0x3

    invoke-direct {v2, v4, v5}, Ljava/lang/Long;-><init>(J)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v4, 0x4

    invoke-direct {v2, v4, v5}, Ljava/lang/Long;-><init>(J)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v4, 0x5

    invoke-direct {v2, v4, v5}, Ljava/lang/Long;-><init>(J)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v4, 0x6

    invoke-direct {v2, v4, v5}, Ljava/lang/Long;-><init>(J)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v4, 0x7

    invoke-direct {v2, v4, v5}, Ljava/lang/Long;-><init>(J)V

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v4, 0x8

    invoke-direct {v2, v4, v5}, Ljava/lang/Long;-><init>(J)V

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v4, 0x9

    invoke-direct {v2, v4, v5}, Ljava/lang/Long;-><init>(J)V

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v4, 0xa

    invoke-direct {v2, v4, v5}, Ljava/lang/Long;-><init>(J)V

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v4, 0xb

    invoke-direct {v2, v4, v5}, Ljava/lang/Long;-><init>(J)V

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v4, 0xc

    invoke-direct {v2, v4, v5}, Ljava/lang/Long;-><init>(J)V

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v4, 0xd

    invoke-direct {v2, v4, v5}, Ljava/lang/Long;-><init>(J)V

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v4, 0xe

    invoke-direct {v2, v4, v5}, Ljava/lang/Long;-><init>(J)V

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v4, 0xf

    invoke-direct {v2, v4, v5}, Ljava/lang/Long;-><init>(J)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/common/io/protocol/ProtoBuf;->SMALL_NUMBERS:[Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Lcom/google/common/io/protocol/ProtoBufType;)V
    .locals 1
    .param p1, "type"    # Lcom/google/common/io/protocol/ProtoBufType;

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/google/common/io/protocol/ProtoBuf;->values:Ljava/util/Vector;

    .line 71
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/google/common/io/protocol/ProtoBuf;->wireTypes:Ljava/lang/StringBuffer;

    .line 79
    iput-object p1, p0, Lcom/google/common/io/protocol/ProtoBuf;->msgType:Lcom/google/common/io/protocol/ProtoBufType;

    .line 80
    return-void
.end method

.method private assertTypeMatch(ILjava/lang/Object;)V
    .locals 4
    .param p1, "tag"    # I
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 832
    invoke-virtual {p0, p1}, Lcom/google/common/io/protocol/ProtoBuf;->getType(I)I

    move-result v0

    .line 833
    .local v0, "tagType":I
    const/16 v1, 0x10

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/common/io/protocol/ProtoBuf;->msgType:Lcom/google/common/io/protocol/ProtoBufType;

    if-nez v1, :cond_1

    .line 893
    .end local p2    # "object":Ljava/lang/Object;
    :cond_0
    :goto_0
    :pswitch_0
    :sswitch_0
    return-void

    .line 837
    .restart local p2    # "object":Ljava/lang/Object;
    :cond_1
    instance-of v1, p2, Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 838
    const/16 v1, 0x18

    if-eq v0, v1, :cond_0

    if-eqz v0, :cond_0

    .line 896
    .end local p2    # "object":Ljava/lang/Object;
    :cond_2
    :goto_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Type mismatch type:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/common/io/protocol/ProtoBuf;->msgType:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " tag:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 842
    .restart local p2    # "object":Ljava/lang/Object;
    :cond_3
    instance-of v1, p2, Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 843
    packed-switch v0, :pswitch_data_0

    :pswitch_1
    goto :goto_1

    .line 863
    :cond_4
    instance-of v1, p2, [B

    if-eqz v1, :cond_5

    .line 864
    sparse-switch v0, :sswitch_data_0

    goto :goto_1

    .line 873
    :cond_5
    instance-of v1, p2, Lcom/google/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_6

    .line 874
    sparse-switch v0, :sswitch_data_1

    goto :goto_1

    .line 881
    :sswitch_1
    iget-object v1, p0, Lcom/google/common/io/protocol/ProtoBuf;->msgType:Lcom/google/common/io/protocol/ProtoBufType;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/common/io/protocol/ProtoBuf;->msgType:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-virtual {v1, p1}, Lcom/google/common/io/protocol/ProtoBufType;->getData(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object v1, p2

    check-cast v1, Lcom/google/common/io/protocol/ProtoBuf;

    iget-object v1, v1, Lcom/google/common/io/protocol/ProtoBuf;->msgType:Lcom/google/common/io/protocol/ProtoBufType;

    if-eqz v1, :cond_0

    check-cast p2, Lcom/google/common/io/protocol/ProtoBuf;

    .end local p2    # "object":Ljava/lang/Object;
    iget-object v1, p2, Lcom/google/common/io/protocol/ProtoBuf;->msgType:Lcom/google/common/io/protocol/ProtoBufType;

    iget-object v2, p0, Lcom/google/common/io/protocol/ProtoBuf;->msgType:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-virtual {v2, p1}, Lcom/google/common/io/protocol/ProtoBufType;->getData(I)Ljava/lang/Object;

    move-result-object v2

    if-ne v1, v2, :cond_2

    goto :goto_0

    .line 887
    .restart local p2    # "object":Ljava/lang/Object;
    :cond_6
    instance-of v1, p2, Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 888
    sparse-switch v0, :sswitch_data_2

    goto :goto_1

    .line 843
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 864
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x19 -> :sswitch_0
        0x1b -> :sswitch_0
        0x1c -> :sswitch_0
        0x23 -> :sswitch_0
        0x24 -> :sswitch_0
    .end sparse-switch

    .line 874
    :sswitch_data_1
    .sparse-switch
        0x2 -> :sswitch_1
        0x3 -> :sswitch_1
        0x19 -> :sswitch_1
        0x1a -> :sswitch_1
        0x1b -> :sswitch_1
        0x1c -> :sswitch_1
    .end sparse-switch

    .line 888
    :sswitch_data_2
    .sparse-switch
        0x2 -> :sswitch_0
        0x19 -> :sswitch_0
        0x1c -> :sswitch_0
        0x24 -> :sswitch_0
    .end sparse-switch
.end method

.method private convert(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 7
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "tagType"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1046
    packed-switch p2, :pswitch_data_0

    .line 1107
    :pswitch_0
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Unsupp.Type"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1051
    :pswitch_1
    instance-of v3, p1, Ljava/lang/Boolean;

    if-eqz v3, :cond_1

    .line 1104
    .end local p1    # "obj":Ljava/lang/Object;
    :cond_0
    :goto_0
    :pswitch_2
    return-object p1

    .line 1054
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_1
    check-cast p1, Ljava/lang/Long;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v3, v4

    packed-switch v3, :pswitch_data_1

    .line 1060
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Type mismatch"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1056
    :pswitch_3
    sget-object p1, Lcom/google/common/io/protocol/ProtoBuf;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 1058
    :pswitch_4
    sget-object p1, Lcom/google/common/io/protocol/ProtoBuf;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 1070
    .restart local p1    # "obj":Ljava/lang/Object;
    :pswitch_5
    instance-of v5, p1, Ljava/lang/Boolean;

    if-eqz v5, :cond_0

    .line 1071
    sget-object v5, Lcom/google/common/io/protocol/ProtoBuf;->SMALL_NUMBERS:[Ljava/lang/Long;

    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_2

    :goto_1
    aget-object p1, v5, v3

    goto :goto_0

    :cond_2
    move v3, v4

    goto :goto_1

    .line 1076
    .restart local p1    # "obj":Ljava/lang/Object;
    :pswitch_6
    instance-of v3, p1, Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 1077
    check-cast p1, Ljava/lang/String;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/common/io/protocol/ProtoBuf;->encodeUtf8(Ljava/lang/String;)[B

    move-result-object p1

    goto :goto_0

    .line 1078
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_3
    instance-of v3, p1, Lcom/google/common/io/protocol/ProtoBuf;

    if-eqz v3, :cond_0

    .line 1079
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1081
    .local v0, "buf":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    check-cast p1, Lcom/google/common/io/protocol/ProtoBuf;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p1, v0}, Lcom/google/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V

    .line 1082
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    goto :goto_0

    .line 1083
    :catch_0
    move-exception v2

    .line 1084
    .local v2, "e":Ljava/io/IOException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1090
    .end local v0    # "buf":Ljava/io/ByteArrayOutputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local p1    # "obj":Ljava/lang/Object;
    :pswitch_7
    instance-of v5, p1, [B

    if-eqz v5, :cond_0

    .line 1091
    check-cast p1, [B

    .end local p1    # "obj":Ljava/lang/Object;
    move-object v1, p1

    check-cast v1, [B

    .line 1092
    .local v1, "data":[B
    array-length v5, v1

    invoke-static {v1, v4, v5, v3}, Lcom/google/common/io/protocol/ProtoBuf;->decodeUtf8([BIIZ)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 1097
    .end local v1    # "data":[B
    .restart local p1    # "obj":Ljava/lang/Object;
    :pswitch_8
    instance-of v3, p1, [B

    if-eqz v3, :cond_0

    .line 1099
    :try_start_1
    new-instance v3, Lcom/google/common/io/protocol/ProtoBuf;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    check-cast p1, [B

    .end local p1    # "obj":Ljava/lang/Object;
    check-cast p1, [B

    invoke-virtual {v3, p1}, Lcom/google/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/common/io/protocol/ProtoBuf;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object p1

    goto :goto_0

    .line 1100
    :catch_1
    move-exception v2

    .line 1101
    .restart local v2    # "e":Ljava/io/IOException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1046
    nop

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_1
        :pswitch_6
        :pswitch_8
        :pswitch_8
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 1054
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method static decodeUtf8([BIIZ)Ljava/lang/String;
    .locals 12
    .param p0, "data"    # [B
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "tolerant"    # Z

    .prologue
    .line 1282
    new-instance v9, Ljava/lang/StringBuffer;

    sub-int v10, p2, p1

    invoke-direct {v9, v10}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 1283
    .local v9, "sb":Ljava/lang/StringBuffer;
    move v7, p1

    .local v7, "pos":I
    move v8, v7

    .line 1285
    .end local v7    # "pos":I
    .local v8, "pos":I
    :goto_0
    if-ge v8, p2, :cond_c

    .line 1286
    add-int/lit8 v7, v8, 0x1

    .end local v8    # "pos":I
    .restart local v7    # "pos":I
    aget-byte v10, p0, v8

    and-int/lit16 v0, v10, 0xff

    .line 1287
    .local v0, "b":I
    const/16 v10, 0x7f

    if-gt v0, v10, :cond_0

    .line 1288
    int-to-char v10, v0

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :goto_1
    move v8, v7

    .line 1335
    .end local v7    # "pos":I
    .restart local v8    # "pos":I
    goto :goto_0

    .line 1289
    .end local v8    # "pos":I
    .restart local v7    # "pos":I
    :cond_0
    const/16 v10, 0xf5

    if-lt v0, v10, :cond_2

    .line 1290
    if-nez p3, :cond_1

    .line 1291
    new-instance v10, Ljava/lang/IllegalArgumentException;

    const-string v11, "Invalid UTF8"

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 1293
    :cond_1
    int-to-char v10, v0

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 1295
    :cond_2
    const/16 v1, 0xe0

    .line 1296
    .local v1, "border":I
    const/4 v3, 0x1

    .line 1297
    .local v3, "count":I
    const/16 v6, 0x80

    .line 1298
    .local v6, "minCode":I
    const/16 v5, 0x1f

    .line 1299
    .local v5, "mask":I
    :goto_2
    if-lt v0, v1, :cond_4

    .line 1300
    shr-int/lit8 v10, v1, 0x1

    or-int/lit16 v1, v10, 0x80

    .line 1301
    const/4 v10, 0x1

    if-ne v3, v10, :cond_3

    const/4 v10, 0x4

    :goto_3
    shl-int/2addr v6, v10

    .line 1302
    add-int/lit8 v3, v3, 0x1

    .line 1303
    shr-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1301
    :cond_3
    const/4 v10, 0x5

    goto :goto_3

    .line 1305
    :cond_4
    and-int v2, v0, v5

    .line 1307
    .local v2, "code":I
    const/4 v4, 0x0

    .local v4, "i":I
    move v8, v7

    .end local v7    # "pos":I
    .restart local v8    # "pos":I
    :goto_4
    if-ge v4, v3, :cond_7

    .line 1308
    shl-int/lit8 v2, v2, 0x6

    .line 1309
    if-lt v8, p2, :cond_5

    .line 1310
    if-nez p3, :cond_d

    .line 1311
    new-instance v10, Ljava/lang/IllegalArgumentException;

    const-string v11, "Invalid UTF8"

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 1315
    :cond_5
    if-nez p3, :cond_6

    aget-byte v10, p0, v8

    and-int/lit16 v10, v10, 0xc0

    const/16 v11, 0x80

    if-eq v10, v11, :cond_6

    .line 1316
    new-instance v10, Ljava/lang/IllegalArgumentException;

    const-string v11, "Invalid UTF8"

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 1318
    :cond_6
    add-int/lit8 v7, v8, 0x1

    .end local v8    # "pos":I
    .restart local v7    # "pos":I
    aget-byte v10, p0, v8

    and-int/lit8 v10, v10, 0x3f

    or-int/2addr v2, v10

    .line 1307
    :goto_5
    add-int/lit8 v4, v4, 0x1

    move v8, v7

    .end local v7    # "pos":I
    .restart local v8    # "pos":I
    goto :goto_4

    .line 1323
    :cond_7
    if-nez p3, :cond_8

    if-lt v2, v6, :cond_9

    :cond_8
    const v10, 0xd800

    if-lt v2, v10, :cond_a

    const v10, 0xdfff

    if-gt v2, v10, :cond_a

    .line 1324
    :cond_9
    new-instance v10, Ljava/lang/IllegalArgumentException;

    const-string v11, "Invalid UTF8"

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 1327
    :cond_a
    const v10, 0xffff

    if-gt v2, v10, :cond_b

    .line 1328
    int-to-char v10, v2

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v7, v8

    .end local v8    # "pos":I
    .restart local v7    # "pos":I
    goto :goto_1

    .line 1330
    .end local v7    # "pos":I
    .restart local v8    # "pos":I
    :cond_b
    const/high16 v10, 0x10000

    sub-int/2addr v2, v10

    .line 1331
    const v10, 0xd800

    shr-int/lit8 v11, v2, 0xa

    or-int/2addr v10, v11

    int-to-char v10, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1332
    const v10, 0xdc00

    and-int/lit16 v11, v2, 0x3ff

    or-int/2addr v10, v11

    int-to-char v10, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v7, v8

    .end local v8    # "pos":I
    .restart local v7    # "pos":I
    goto/16 :goto_1

    .line 1336
    .end local v0    # "b":I
    .end local v1    # "border":I
    .end local v2    # "code":I
    .end local v3    # "count":I
    .end local v4    # "i":I
    .end local v5    # "mask":I
    .end local v6    # "minCode":I
    .end local v7    # "pos":I
    .restart local v8    # "pos":I
    :cond_c
    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    return-object v10

    .restart local v0    # "b":I
    .restart local v1    # "border":I
    .restart local v2    # "code":I
    .restart local v3    # "count":I
    .restart local v4    # "i":I
    .restart local v5    # "mask":I
    .restart local v6    # "minCode":I
    :cond_d
    move v7, v8

    .end local v8    # "pos":I
    .restart local v7    # "pos":I
    goto :goto_5
.end method

.method static encodeUtf8(Ljava/lang/String;[BI)I
    .locals 9
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "buf"    # [B
    .param p2, "pos"    # I

    .prologue
    const v8, 0xd800

    const v7, 0xfc00

    .line 1209
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    .line 1210
    .local v4, "len":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v4, :cond_9

    .line 1211
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1214
    .local v0, "code":I
    if-lt v0, v8, :cond_0

    const v5, 0xdfff

    if-gt v0, v5, :cond_0

    add-int/lit8 v5, v3, 0x1

    if-ge v5, v4, :cond_0

    .line 1215
    add-int/lit8 v5, v3, 0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 1220
    .local v2, "codeLo":I
    and-int v5, v2, v7

    and-int v6, v0, v7

    xor-int/2addr v5, v6

    const/16 v6, 0x400

    if-ne v5, v6, :cond_0

    .line 1222
    add-int/lit8 v3, v3, 0x1

    .line 1225
    and-int v5, v2, v7

    if-ne v5, v8, :cond_2

    .line 1226
    move v1, v2

    .line 1227
    .local v1, "codeHi":I
    move v2, v0

    .line 1231
    :goto_1
    and-int/lit16 v5, v1, 0x3ff

    shl-int/lit8 v5, v5, 0xa

    and-int/lit16 v6, v2, 0x3ff

    or-int/2addr v5, v6

    const/high16 v6, 0x10000

    add-int v0, v5, v6

    .line 1234
    .end local v1    # "codeHi":I
    .end local v2    # "codeLo":I
    :cond_0
    const/16 v5, 0x7f

    if-gt v0, v5, :cond_3

    .line 1235
    if-eqz p1, :cond_1

    .line 1236
    int-to-byte v5, v0

    aput-byte v5, p1, p2

    .line 1238
    :cond_1
    add-int/lit8 p2, p2, 0x1

    .line 1210
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1229
    .restart local v2    # "codeLo":I
    :cond_2
    move v1, v0

    .restart local v1    # "codeHi":I
    goto :goto_1

    .line 1239
    .end local v1    # "codeHi":I
    .end local v2    # "codeLo":I
    :cond_3
    const/16 v5, 0x7ff

    if-gt v0, v5, :cond_5

    .line 1241
    if-eqz p1, :cond_4

    .line 1242
    shr-int/lit8 v5, v0, 0x6

    or-int/lit16 v5, v5, 0xc0

    int-to-byte v5, v5

    aput-byte v5, p1, p2

    .line 1243
    add-int/lit8 v5, p2, 0x1

    and-int/lit8 v6, v0, 0x3f

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, p1, v5

    .line 1245
    :cond_4
    add-int/lit8 p2, p2, 0x2

    goto :goto_2

    .line 1246
    :cond_5
    const v5, 0xffff

    if-gt v0, v5, :cond_7

    .line 1248
    if-eqz p1, :cond_6

    .line 1249
    shr-int/lit8 v5, v0, 0xc

    or-int/lit16 v5, v5, 0xe0

    int-to-byte v5, v5

    aput-byte v5, p1, p2

    .line 1250
    add-int/lit8 v5, p2, 0x1

    shr-int/lit8 v6, v0, 0x6

    and-int/lit8 v6, v6, 0x3f

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, p1, v5

    .line 1251
    add-int/lit8 v5, p2, 0x2

    and-int/lit8 v6, v0, 0x3f

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, p1, v5

    .line 1253
    :cond_6
    add-int/lit8 p2, p2, 0x3

    goto :goto_2

    .line 1255
    :cond_7
    if-eqz p1, :cond_8

    .line 1256
    shr-int/lit8 v5, v0, 0x12

    or-int/lit16 v5, v5, 0xf0

    int-to-byte v5, v5

    aput-byte v5, p1, p2

    .line 1257
    add-int/lit8 v5, p2, 0x1

    shr-int/lit8 v6, v0, 0xc

    and-int/lit8 v6, v6, 0x3f

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, p1, v5

    .line 1258
    add-int/lit8 v5, p2, 0x2

    shr-int/lit8 v6, v0, 0x6

    and-int/lit8 v6, v6, 0x3f

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, p1, v5

    .line 1259
    add-int/lit8 v5, p2, 0x3

    and-int/lit8 v6, v0, 0x3f

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, p1, v5

    .line 1261
    :cond_8
    add-int/lit8 p2, p2, 0x4

    goto :goto_2

    .line 1265
    .end local v0    # "code":I
    :cond_9
    return p2
.end method

.method static encodeUtf8(Ljava/lang/String;)[B
    .locals 4
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 1193
    const/4 v2, 0x0

    invoke-static {p0, v2, v3}, Lcom/google/common/io/protocol/ProtoBuf;->encodeUtf8(Ljava/lang/String;[BI)I

    move-result v0

    .line 1194
    .local v0, "len":I
    new-array v1, v0, [B

    .line 1195
    .local v1, "result":[B
    invoke-static {p0, v1, v3}, Lcom/google/common/io/protocol/ProtoBuf;->encodeUtf8(Ljava/lang/String;[BI)I

    .line 1196
    return-object v1
.end method

.method private getDataSize(II)I
    .locals 8
    .param p1, "tag"    # I
    .param p2, "i"    # I

    .prologue
    .line 536
    shl-int/lit8 v3, p1, 0x3

    int-to-long v6, v3

    invoke-static {v6, v7}, Lcom/google/common/io/protocol/ProtoBuf;->getVarIntSize(J)I

    move-result v2

    .line 538
    .local v2, "tagSize":I
    invoke-direct {p0, p1}, Lcom/google/common/io/protocol/ProtoBuf;->getWireType(I)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 555
    :pswitch_0
    const/16 v3, 0x10

    invoke-direct {p0, p1, p2, v3}, Lcom/google/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v1

    .line 559
    .local v1, "o":Ljava/lang/Object;
    instance-of v3, v1, [B

    if-eqz v3, :cond_1

    .line 560
    check-cast v1, [B

    .end local v1    # "o":Ljava/lang/Object;
    check-cast v1, [B

    array-length v0, v1

    .line 567
    .local v0, "contentSize":I
    :goto_0
    int-to-long v6, v0

    invoke-static {v6, v7}, Lcom/google/common/io/protocol/ProtoBuf;->getVarIntSize(J)I

    move-result v3

    add-int/2addr v3, v2

    add-int/2addr v3, v0

    .end local v0    # "contentSize":I
    :goto_1
    return v3

    .line 540
    :pswitch_1
    add-int/lit8 v3, v2, 0x4

    goto :goto_1

    .line 542
    :pswitch_2
    add-int/lit8 v3, v2, 0x8

    goto :goto_1

    .line 544
    :pswitch_3
    invoke-virtual {p0, p1, p2}, Lcom/google/common/io/protocol/ProtoBuf;->getLong(II)J

    move-result-wide v4

    .line 545
    .local v4, "value":J
    invoke-direct {p0, p1}, Lcom/google/common/io/protocol/ProtoBuf;->isZigZagEncodedType(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 546
    invoke-static {v4, v5}, Lcom/google/common/io/protocol/ProtoBuf;->zigZagEncode(J)J

    move-result-wide v4

    .line 548
    :cond_0
    invoke-static {v4, v5}, Lcom/google/common/io/protocol/ProtoBuf;->getVarIntSize(J)I

    move-result v3

    add-int/2addr v3, v2

    goto :goto_1

    .line 551
    .end local v4    # "value":J
    :pswitch_4
    invoke-virtual {p0, p1, p2}, Lcom/google/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/common/io/protocol/ProtoBuf;->getDataSize()I

    move-result v3

    add-int/2addr v3, v2

    add-int/2addr v3, v2

    goto :goto_1

    .line 561
    .restart local v1    # "o":Ljava/lang/Object;
    :cond_1
    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 562
    check-cast v1, Ljava/lang/String;

    .end local v1    # "o":Ljava/lang/Object;
    const/4 v3, 0x0

    const/4 v6, 0x0

    invoke-static {v1, v3, v6}, Lcom/google/common/io/protocol/ProtoBuf;->encodeUtf8(Ljava/lang/String;[BI)I

    move-result v0

    .restart local v0    # "contentSize":I
    goto :goto_0

    .line 564
    .end local v0    # "contentSize":I
    .restart local v1    # "o":Ljava/lang/Object;
    :cond_2
    check-cast v1, Lcom/google/common/io/protocol/ProtoBuf;

    .end local v1    # "o":Ljava/lang/Object;
    invoke-virtual {v1}, Lcom/google/common/io/protocol/ProtoBuf;->getDataSize()I

    move-result v0

    .restart local v0    # "contentSize":I
    goto :goto_0

    .line 538
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getObject(III)Ljava/lang/Object;
    .locals 4
    .param p1, "tag"    # I
    .param p2, "index"    # I
    .param p3, "desiredType"    # I

    .prologue
    .line 943
    invoke-virtual {p0, p1}, Lcom/google/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-lt p2, v3, :cond_0

    .line 944
    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v3

    .line 947
    :cond_0
    iget-object v3, p0, Lcom/google/common/io/protocol/ProtoBuf;->values:Ljava/util/Vector;

    invoke-virtual {v3, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    .line 949
    .local v0, "o":Ljava/lang/Object;
    const/4 v2, 0x0

    .line 950
    .local v2, "v":Ljava/util/Vector;
    instance-of v3, v0, Ljava/util/Vector;

    if-eqz v3, :cond_1

    move-object v2, v0

    .line 951
    check-cast v2, Ljava/util/Vector;

    .line 952
    invoke-virtual {v2, p2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    .line 955
    :cond_1
    invoke-direct {p0, v0, p3}, Lcom/google/common/io/protocol/ProtoBuf;->convert(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v1

    .line 957
    .local v1, "o2":Ljava/lang/Object;
    if-eq v1, v0, :cond_2

    if-eqz v0, :cond_2

    .line 958
    if-nez v2, :cond_3

    .line 959
    invoke-direct {p0, p1, v1}, Lcom/google/common/io/protocol/ProtoBuf;->setObject(ILjava/lang/Object;)V

    .line 965
    :cond_2
    :goto_0
    return-object v1

    .line 961
    :cond_3
    invoke-virtual {v2, v1, p2}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method private static getVarIntSize(J)I
    .locals 4
    .param p0, "i"    # J

    .prologue
    .line 575
    const-wide/16 v2, 0x0

    cmp-long v1, p0, v2

    if-gez v1, :cond_1

    .line 576
    const/16 v0, 0xa

    .line 583
    :cond_0
    return v0

    .line 578
    :cond_1
    const/4 v0, 0x1

    .line 579
    .local v0, "size":I
    :goto_0
    const-wide/16 v2, 0x80

    cmp-long v1, p0, v2

    if-ltz v1, :cond_0

    .line 580
    add-int/lit8 v0, v0, 0x1

    .line 581
    const/4 v1, 0x7

    shr-long/2addr p0, v1

    goto :goto_0
.end method

.method private final getWireType(I)I
    .locals 5
    .param p1, "tag"    # I

    .prologue
    const/16 v4, 0x2f

    .line 975
    invoke-virtual {p0, p1}, Lcom/google/common/io/protocol/ProtoBuf;->getType(I)I

    move-result v0

    .line 977
    .local v0, "tagType":I
    packed-switch v0, :pswitch_data_0

    .line 1012
    :pswitch_0
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupp.Type:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/common/io/protocol/ProtoBuf;->msgType:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 994
    :pswitch_1
    const/4 v0, 0x0

    .line 1010
    .end local v0    # "tagType":I
    :goto_0
    :pswitch_2
    return v0

    .line 1000
    .restart local v0    # "tagType":I
    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    .line 1004
    :pswitch_4
    const/4 v0, 0x1

    goto :goto_0

    .line 1008
    :pswitch_5
    const/4 v0, 0x5

    goto :goto_0

    .line 1010
    :pswitch_6
    const/4 v0, 0x3

    goto :goto_0

    .line 977
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_3
        :pswitch_6
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_5
        :pswitch_4
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private insertObject(IILjava/lang/Object;)V
    .locals 4
    .param p1, "tag"    # I
    .param p2, "index"    # I
    .param p3, "o"    # Ljava/lang/Object;

    .prologue
    .line 1021
    invoke-direct {p0, p1, p3}, Lcom/google/common/io/protocol/ProtoBuf;->assertTypeMatch(ILjava/lang/Object;)V

    .line 1023
    invoke-virtual {p0, p1}, Lcom/google/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    .line 1025
    .local v0, "count":I
    if-nez v0, :cond_0

    .line 1026
    invoke-direct {p0, p1, p3}, Lcom/google/common/io/protocol/ProtoBuf;->setObject(ILjava/lang/Object;)V

    .line 1039
    :goto_0
    return-void

    .line 1028
    :cond_0
    iget-object v3, p0, Lcom/google/common/io/protocol/ProtoBuf;->values:Ljava/util/Vector;

    invoke-virtual {v3, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    .line 1030
    .local v1, "curr":Ljava/lang/Object;
    instance-of v3, v1, Ljava/util/Vector;

    if-eqz v3, :cond_1

    move-object v2, v1

    .line 1031
    check-cast v2, Ljava/util/Vector;

    .line 1037
    .local v2, "v":Ljava/util/Vector;
    :goto_1
    invoke-virtual {v2, p3, p2}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    goto :goto_0

    .line 1033
    .end local v2    # "v":Ljava/util/Vector;
    :cond_1
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    .line 1034
    .restart local v2    # "v":Ljava/util/Vector;
    invoke-virtual {v2, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1035
    iget-object v3, p0, Lcom/google/common/io/protocol/ProtoBuf;->values:Ljava/util/Vector;

    invoke-virtual {v3, v2, p1}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    goto :goto_1
.end method

.method private isZigZagEncodedType(I)Z
    .locals 2
    .param p1, "tag"    # I

    .prologue
    .line 664
    invoke-virtual {p0, p1}, Lcom/google/common/io/protocol/ProtoBuf;->getType(I)I

    move-result v0

    .line 665
    .local v0, "declaredType":I
    const/16 v1, 0x21

    if-eq v0, v1, :cond_0

    const/16 v1, 0x22

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static readVarInt(Ljava/io/InputStream;Z)J
    .locals 8
    .param p0, "is"    # Ljava/io/InputStream;
    .param p1, "permitEOF"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1125
    const-wide/16 v2, 0x0

    .line 1126
    .local v2, "result":J
    const/4 v4, 0x0

    .line 1130
    .local v4, "shift":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v5, 0xa

    if-ge v0, v5, :cond_2

    .line 1131
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 1133
    .local v1, "in":I
    const/4 v5, -0x1

    if-ne v1, v5, :cond_1

    .line 1134
    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 1135
    const-wide/16 v6, -0x1

    .line 1148
    .end local v1    # "in":I
    :goto_1
    return-wide v6

    .line 1137
    .restart local v1    # "in":I
    :cond_0
    new-instance v5, Ljava/io/IOException;

    const-string v6, "EOF"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1140
    :cond_1
    and-int/lit8 v5, v1, 0x7f

    int-to-long v6, v5

    shl-long/2addr v6, v4

    or-long/2addr v2, v6

    .line 1142
    and-int/lit16 v5, v1, 0x80

    if-nez v5, :cond_3

    .end local v1    # "in":I
    :cond_2
    move-wide v6, v2

    .line 1148
    goto :goto_1

    .line 1146
    .restart local v1    # "in":I
    :cond_3
    add-int/lit8 v4, v4, 0x7

    .line 1130
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private setObject(ILjava/lang/Object;)V
    .locals 2
    .param p1, "tag"    # I
    .param p2, "o"    # Ljava/lang/Object;

    .prologue
    .line 1156
    iget-object v0, p0, Lcom/google/common/io/protocol/ProtoBuf;->values:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-gt v0, p1, :cond_0

    .line 1157
    iget-object v0, p0, Lcom/google/common/io/protocol/ProtoBuf;->values:Ljava/util/Vector;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->setSize(I)V

    .line 1159
    :cond_0
    if-eqz p2, :cond_1

    .line 1160
    invoke-direct {p0, p1, p2}, Lcom/google/common/io/protocol/ProtoBuf;->assertTypeMatch(ILjava/lang/Object;)V

    .line 1162
    :cond_1
    iget-object v0, p0, Lcom/google/common/io/protocol/ProtoBuf;->values:Ljava/util/Vector;

    invoke-virtual {v0, p2, p1}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    .line 1163
    return-void
.end method

.method static writeVarInt(Ljava/io/OutputStream;J)V
    .locals 5
    .param p0, "os"    # Ljava/io/OutputStream;
    .param p1, "value"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1169
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0xa

    if-ge v0, v2, :cond_0

    .line 1171
    const-wide/16 v2, 0x7f

    and-long/2addr v2, p1

    long-to-int v1, v2

    .line 1173
    .local v1, "toWrite":I
    const/4 v2, 0x7

    ushr-long/2addr p1, v2

    .line 1175
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-nez v2, :cond_1

    .line 1176
    invoke-virtual {p0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 1182
    .end local v1    # "toWrite":I
    :cond_0
    return-void

    .line 1179
    .restart local v1    # "toWrite":I
    :cond_1
    or-int/lit16 v2, v1, 0x80

    invoke-virtual {p0, v2}, Ljava/io/OutputStream;->write(I)V

    .line 1169
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static zigZagDecode(J)J
    .locals 4
    .param p0, "v"    # J

    .prologue
    .line 681
    const/4 v0, 0x1

    ushr-long v0, p0, v0

    const-wide/16 v2, 0x1

    and-long/2addr v2, p0

    neg-long v2, v2

    xor-long p0, v0, v2

    .line 682
    return-wide p0
.end method

.method private static zigZagEncode(J)J
    .locals 4
    .param p0, "v"    # J

    .prologue
    .line 673
    const/4 v0, 0x1

    shl-long v0, p0, v0

    const/16 v2, 0x3f

    ushr-long v2, p0, v2

    neg-long v2, v2

    xor-long p0, v0, v2

    .line 674
    return-wide p0
.end method


# virtual methods
.method public addString(ILjava/lang/String;)V
    .locals 1
    .param p1, "tag"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 172
    invoke-virtual {p0, p1}, Lcom/google/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/common/io/protocol/ProtoBuf;->insertString(IILjava/lang/String;)V

    .line 173
    return-void
.end method

.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 86
    iget-object v0, p0, Lcom/google/common/io/protocol/ProtoBuf;->values:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->setSize(I)V

    .line 87
    iget-object v0, p0, Lcom/google/common/io/protocol/ProtoBuf;->wireTypes:Ljava/lang/StringBuffer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 88
    return-void
.end method

.method public getCount(I)I
    .locals 3
    .param p1, "tag"    # I

    .prologue
    const/4 v1, 0x0

    .line 481
    iget-object v2, p0, Lcom/google/common/io/protocol/ProtoBuf;->values:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-lt p1, v2, :cond_1

    .line 488
    :cond_0
    :goto_0
    return v1

    .line 484
    :cond_1
    iget-object v2, p0, Lcom/google/common/io/protocol/ProtoBuf;->values:Ljava/util/Vector;

    invoke-virtual {v2, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    .line 485
    .local v0, "o":Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 488
    instance-of v1, v0, Ljava/util/Vector;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/util/Vector;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    goto :goto_0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getDataSize()I
    .locals 4

    .prologue
    .line 522
    const/4 v1, 0x0

    .line 523
    .local v1, "size":I
    const/4 v2, 0x0

    .local v2, "tag":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/common/io/protocol/ProtoBuf;->maxTag()I

    move-result v3

    if-gt v2, v3, :cond_1

    .line 524
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p0, v2}, Lcom/google/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 525
    invoke-direct {p0, v2, v0}, Lcom/google/common/io/protocol/ProtoBuf;->getDataSize(II)I

    move-result v3

    add-int/2addr v1, v3

    .line 524
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 523
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 528
    .end local v0    # "i":I
    :cond_1
    return v1
.end method

.method public getLong(II)J
    .locals 2
    .param p1, "tag"    # I
    .param p2, "index"    # I

    .prologue
    .line 231
    const/16 v0, 0x13

    invoke-direct {p0, p1, p2, v0}, Lcom/google/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getProtoBuf(II)Lcom/google/common/io/protocol/ProtoBuf;
    .locals 1
    .param p1, "tag"    # I
    .param p2, "index"    # I

    .prologue
    .line 274
    const/16 v0, 0x1a

    invoke-direct {p0, p1, p2, v0}, Lcom/google/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public getType(I)I
    .locals 5
    .param p1, "tag"    # I

    .prologue
    const/4 v2, 0x0

    const/16 v4, 0x10

    .line 499
    const/16 v1, 0x10

    .line 500
    .local v1, "tagType":I
    iget-object v3, p0, Lcom/google/common/io/protocol/ProtoBuf;->msgType:Lcom/google/common/io/protocol/ProtoBufType;

    if-eqz v3, :cond_0

    .line 501
    iget-object v3, p0, Lcom/google/common/io/protocol/ProtoBuf;->msgType:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-virtual {v3, p1}, Lcom/google/common/io/protocol/ProtoBufType;->getType(I)I

    move-result v1

    .line 504
    :cond_0
    if-ne v1, v4, :cond_1

    iget-object v3, p0, Lcom/google/common/io/protocol/ProtoBuf;->wireTypes:Ljava/lang/StringBuffer;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-ge p1, v3, :cond_1

    .line 505
    iget-object v3, p0, Lcom/google/common/io/protocol/ProtoBuf;->wireTypes:Ljava/lang/StringBuffer;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v1

    .line 508
    :cond_1
    if-ne v1, v4, :cond_3

    invoke-virtual {p0, p1}, Lcom/google/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-lez v3, :cond_3

    .line 509
    invoke-direct {p0, p1, v2, v4}, Lcom/google/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v0

    .line 511
    .local v0, "o":Ljava/lang/Object;
    instance-of v3, v0, Ljava/lang/Long;

    if-nez v3, :cond_2

    instance-of v3, v0, Ljava/lang/Boolean;

    if-eqz v3, :cond_4

    :cond_2
    move v1, v2

    .line 515
    .end local v0    # "o":Ljava/lang/Object;
    :cond_3
    :goto_0
    return v1

    .line 511
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_4
    const/4 v1, 0x2

    goto :goto_0
.end method

.method public insertString(IILjava/lang/String;)V
    .locals 0
    .param p1, "tag"    # I
    .param p2, "index"    # I
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 826
    invoke-direct {p0, p1, p2, p3}, Lcom/google/common/io/protocol/ProtoBuf;->insertObject(IILjava/lang/Object;)V

    .line 827
    return-void
.end method

.method public maxTag()I
    .locals 1

    .prologue
    .line 700
    iget-object v0, p0, Lcom/google/common/io/protocol/ProtoBuf;->values:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public outputTo(Ljava/io/OutputStream;)V
    .locals 14
    .param p1, "os"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 593
    const/4 v7, 0x0

    .local v7, "tag":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/common/io/protocol/ProtoBuf;->maxTag()I

    move-result v11

    if-gt v7, v11, :cond_6

    .line 594
    invoke-virtual {p0, v7}, Lcom/google/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v6

    .line 595
    .local v6, "size":I
    invoke-direct {p0, v7}, Lcom/google/common/io/protocol/ProtoBuf;->getWireType(I)I

    move-result v10

    .line 598
    .local v10, "wireType":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v6, :cond_5

    .line 599
    shl-int/lit8 v11, v7, 0x3

    or-int/2addr v11, v10

    int-to-long v12, v11

    invoke-static {p1, v12, v13}, Lcom/google/common/io/protocol/ProtoBuf;->writeVarInt(Ljava/io/OutputStream;J)V

    .line 601
    packed-switch v10, :pswitch_data_0

    .line 645
    :pswitch_0
    new-instance v11, Ljava/lang/IllegalArgumentException;

    invoke-direct {v11}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v11

    .line 604
    :pswitch_1
    const/16 v11, 0x13

    invoke-direct {p0, v7, v3, v11}, Lcom/google/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Long;

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 606
    .local v8, "v":J
    const/4 v11, 0x5

    if-ne v10, v11, :cond_0

    const/4 v1, 0x4

    .line 607
    .local v1, "cnt":I
    :goto_2
    const/4 v0, 0x0

    .local v0, "b":I
    :goto_3
    if-ge v0, v1, :cond_2

    .line 608
    const-wide/16 v12, 0xff

    and-long/2addr v12, v8

    long-to-int v11, v12

    invoke-virtual {p1, v11}, Ljava/io/OutputStream;->write(I)V

    .line 609
    const/16 v11, 0x8

    shr-long/2addr v8, v11

    .line 607
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 606
    .end local v0    # "b":I
    .end local v1    # "cnt":I
    :cond_0
    const/16 v1, 0x8

    goto :goto_2

    .line 614
    .end local v8    # "v":J
    :pswitch_2
    const/16 v11, 0x13

    invoke-direct {p0, v7, v3, v11}, Lcom/google/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Long;

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 615
    .restart local v8    # "v":J
    invoke-direct {p0, v7}, Lcom/google/common/io/protocol/ProtoBuf;->isZigZagEncodedType(I)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 616
    invoke-static {v8, v9}, Lcom/google/common/io/protocol/ProtoBuf;->zigZagEncode(J)J

    move-result-wide v8

    .line 618
    :cond_1
    invoke-static {p1, v8, v9}, Lcom/google/common/io/protocol/ProtoBuf;->writeVarInt(Ljava/io/OutputStream;J)V

    .line 598
    .end local v8    # "v":J
    :cond_2
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 622
    :pswitch_3
    invoke-virtual {p0, v7}, Lcom/google/common/io/protocol/ProtoBuf;->getType(I)I

    move-result v11

    const/16 v12, 0x1b

    if-ne v11, v12, :cond_3

    const/16 v11, 0x10

    :goto_5
    invoke-direct {p0, v7, v3, v11}, Lcom/google/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v5

    .line 627
    .local v5, "o":Ljava/lang/Object;
    instance-of v11, v5, [B

    if-eqz v11, :cond_4

    .line 628
    check-cast v5, [B

    .end local v5    # "o":Ljava/lang/Object;
    move-object v2, v5

    check-cast v2, [B

    .line 629
    .local v2, "data":[B
    array-length v11, v2

    int-to-long v12, v11

    invoke-static {p1, v12, v13}, Lcom/google/common/io/protocol/ProtoBuf;->writeVarInt(Ljava/io/OutputStream;J)V

    .line 630
    invoke-virtual {p1, v2}, Ljava/io/OutputStream;->write([B)V

    goto :goto_4

    .line 622
    .end local v2    # "data":[B
    :cond_3
    const/16 v11, 0x19

    goto :goto_5

    .restart local v5    # "o":Ljava/lang/Object;
    :cond_4
    move-object v4, v5

    .line 632
    check-cast v4, Lcom/google/common/io/protocol/ProtoBuf;

    .line 633
    .local v4, "msg":Lcom/google/common/io/protocol/ProtoBuf;
    invoke-virtual {v4}, Lcom/google/common/io/protocol/ProtoBuf;->getDataSize()I

    move-result v11

    int-to-long v12, v11

    invoke-static {p1, v12, v13}, Lcom/google/common/io/protocol/ProtoBuf;->writeVarInt(Ljava/io/OutputStream;J)V

    .line 634
    invoke-virtual {v4, p1}, Lcom/google/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V

    goto :goto_4

    .line 639
    .end local v4    # "msg":Lcom/google/common/io/protocol/ProtoBuf;
    .end local v5    # "o":Ljava/lang/Object;
    :pswitch_4
    const/16 v11, 0x1a

    invoke-direct {p0, v7, v3, v11}, Lcom/google/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/common/io/protocol/ProtoBuf;

    invoke-virtual {v11, p1}, Lcom/google/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V

    .line 641
    shl-int/lit8 v11, v7, 0x3

    or-int/lit8 v11, v11, 0x4

    int-to-long v12, v11

    invoke-static {p1, v12, v13}, Lcom/google/common/io/protocol/ProtoBuf;->writeVarInt(Ljava/io/OutputStream;J)V

    goto :goto_4

    .line 593
    :cond_5
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .line 649
    .end local v3    # "i":I
    .end local v6    # "size":I
    .end local v10    # "wireType":I
    :cond_6
    return-void

    .line 601
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public parse(Ljava/io/InputStream;I)I
    .locals 22
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "available"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 365
    invoke-virtual/range {p0 .. p0}, Lcom/google/common/io/protocol/ProtoBuf;->clear()V

    .line 366
    :goto_0
    if-lez p2, :cond_0

    .line 367
    const/16 v19, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-static {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->readVarInt(Ljava/io/InputStream;Z)J

    move-result-wide v12

    .line 369
    .local v12, "tagAndType":J
    const-wide/16 v20, -0x1

    cmp-long v19, v12, v20

    if-nez v19, :cond_1

    .line 452
    .end local v12    # "tagAndType":J
    :cond_0
    if-gez p2, :cond_c

    .line 453
    new-instance v19, Ljava/io/IOException;

    invoke-direct/range {v19 .. v19}, Ljava/io/IOException;-><init>()V

    throw v19

    .line 372
    .restart local v12    # "tagAndType":J
    :cond_1
    invoke-static {v12, v13}, Lcom/google/common/io/protocol/ProtoBuf;->getVarIntSize(J)I

    move-result v19

    sub-int p2, p2, v19

    .line 373
    long-to-int v0, v12

    move/from16 v19, v0

    and-int/lit8 v18, v19, 0x7

    .line 374
    .local v18, "wireType":I
    const/16 v19, 0x4

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_0

    .line 377
    const/16 v19, 0x3

    ushr-long v20, v12, v19

    move-wide/from16 v0, v20

    long-to-int v11, v0

    .line 378
    .local v11, "tag":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/common/io/protocol/ProtoBuf;->wireTypes:Ljava/lang/StringBuffer;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->length()I

    move-result v19

    move/from16 v0, v19

    if-gt v0, v11, :cond_2

    .line 379
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/common/io/protocol/ProtoBuf;->wireTypes:Ljava/lang/StringBuffer;

    move-object/from16 v19, v0

    const/16 v20, 0x10

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 381
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/common/io/protocol/ProtoBuf;->wireTypes:Ljava/lang/StringBuffer;

    move-object/from16 v19, v0

    move/from16 v0, v18

    int-to-char v0, v0

    move/from16 v20, v0

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v0, v11, v1}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 385
    packed-switch v18, :pswitch_data_0

    .line 447
    :pswitch_0
    new-instance v19, Ljava/lang/RuntimeException;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Unsupp.Type"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v19

    .line 387
    :pswitch_1
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-static {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->readVarInt(Ljava/io/InputStream;Z)J

    move-result-wide v16

    .line 388
    .local v16, "v":J
    invoke-static/range {v16 .. v17}, Lcom/google/common/io/protocol/ProtoBuf;->getVarIntSize(J)I

    move-result v19

    sub-int p2, p2, v19

    .line 389
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/google/common/io/protocol/ProtoBuf;->isZigZagEncodedType(I)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 390
    invoke-static/range {v16 .. v17}, Lcom/google/common/io/protocol/ProtoBuf;->zigZagDecode(J)J

    move-result-wide v16

    .line 392
    :cond_3
    const-wide/16 v20, 0x0

    cmp-long v19, v16, v20

    if-ltz v19, :cond_4

    sget-object v19, Lcom/google/common/io/protocol/ProtoBuf;->SMALL_NUMBERS:[Ljava/lang/Long;

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v20, v0

    cmp-long v19, v16, v20

    if-gez v19, :cond_4

    sget-object v19, Lcom/google/common/io/protocol/ProtoBuf;->SMALL_NUMBERS:[Ljava/lang/Long;

    move-wide/from16 v0, v16

    long-to-int v0, v0

    move/from16 v20, v0

    aget-object v15, v19, v20

    .line 449
    .end local v16    # "v":J
    :goto_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v19

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v11, v1, v15}, Lcom/google/common/io/protocol/ProtoBuf;->insertObject(IILjava/lang/Object;)V

    goto/16 :goto_0

    .line 392
    .restart local v16    # "v":J
    :cond_4
    new-instance v15, Ljava/lang/Long;

    invoke-direct/range {v15 .. v17}, Ljava/lang/Long;-><init>(J)V

    goto :goto_2

    .line 399
    .end local v16    # "v":J
    :pswitch_2
    const-wide/16 v16, 0x0

    .line 400
    .restart local v16    # "v":J
    const/4 v10, 0x0

    .line 401
    .local v10, "shift":I
    const/16 v19, 0x5

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_5

    const/4 v2, 0x4

    .line 402
    .local v2, "count":I
    :goto_3
    sub-int p2, p2, v2

    move v3, v2

    .line 404
    .end local v2    # "count":I
    .local v3, "count":I
    :goto_4
    add-int/lit8 v2, v3, -0x1

    .end local v3    # "count":I
    .restart local v2    # "count":I
    if-lez v3, :cond_6

    .line 405
    invoke-virtual/range {p1 .. p1}, Ljava/io/InputStream;->read()I

    move-result v19

    move/from16 v0, v19

    int-to-long v6, v0

    .line 406
    .local v6, "l":J
    shl-long v20, v6, v10

    or-long v16, v16, v20

    .line 407
    add-int/lit8 v10, v10, 0x8

    move v3, v2

    .line 408
    .end local v2    # "count":I
    .restart local v3    # "count":I
    goto :goto_4

    .line 401
    .end local v3    # "count":I
    .end local v6    # "l":J
    :cond_5
    const/16 v2, 0x8

    goto :goto_3

    .line 410
    .restart local v2    # "count":I
    :cond_6
    const-wide/16 v20, 0x0

    cmp-long v19, v16, v20

    if-ltz v19, :cond_7

    sget-object v19, Lcom/google/common/io/protocol/ProtoBuf;->SMALL_NUMBERS:[Ljava/lang/Long;

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v20, v0

    cmp-long v19, v16, v20

    if-gez v19, :cond_7

    sget-object v19, Lcom/google/common/io/protocol/ProtoBuf;->SMALL_NUMBERS:[Ljava/lang/Long;

    move-wide/from16 v0, v16

    long-to-int v0, v0

    move/from16 v20, v0

    aget-object v15, v19, v20

    .line 413
    .local v15, "value":Ljava/lang/Long;
    :goto_5
    goto :goto_2

    .line 410
    .end local v15    # "value":Ljava/lang/Long;
    :cond_7
    new-instance v15, Ljava/lang/Long;

    invoke-direct/range {v15 .. v17}, Ljava/lang/Long;-><init>(J)V

    goto :goto_5

    .line 416
    .end local v2    # "count":I
    .end local v10    # "shift":I
    .end local v16    # "v":J
    :pswitch_3
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-static {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->readVarInt(Ljava/io/InputStream;Z)J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-int v14, v0

    .line 417
    .local v14, "total":I
    int-to-long v0, v14

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Lcom/google/common/io/protocol/ProtoBuf;->getVarIntSize(J)I

    move-result v19

    sub-int p2, p2, v19

    .line 418
    sub-int p2, p2, v14

    .line 420
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/common/io/protocol/ProtoBuf;->getType(I)I

    move-result v19

    const/16 v20, 0x1b

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_8

    .line 421
    new-instance v8, Lcom/google/common/io/protocol/ProtoBuf;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/common/io/protocol/ProtoBuf;->msgType:Lcom/google/common/io/protocol/ProtoBufType;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Lcom/google/common/io/protocol/ProtoBufType;->getData(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/google/common/io/protocol/ProtoBufType;

    move-object/from16 v0, v19

    invoke-direct {v8, v0}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    .line 422
    .local v8, "msg":Lcom/google/common/io/protocol/ProtoBuf;
    move-object/from16 v0, p1

    invoke-virtual {v8, v0, v14}, Lcom/google/common/io/protocol/ProtoBuf;->parse(Ljava/io/InputStream;I)I

    .line 423
    move-object v15, v8

    .line 424
    .local v15, "value":Lcom/google/common/io/protocol/ProtoBuf;
    goto/16 :goto_2

    .line 425
    .end local v8    # "msg":Lcom/google/common/io/protocol/ProtoBuf;
    .end local v15    # "value":Lcom/google/common/io/protocol/ProtoBuf;
    :cond_8
    new-array v4, v14, [B

    .line 426
    .local v4, "data":[B
    const/4 v9, 0x0

    .line 427
    .local v9, "pos":I
    :goto_6
    if-ge v9, v14, :cond_a

    .line 428
    sub-int v19, v14, v9

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v9, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 429
    .restart local v2    # "count":I
    if-gtz v2, :cond_9

    .line 430
    new-instance v19, Ljava/io/IOException;

    const-string v20, "Unexp.EOF"

    invoke-direct/range {v19 .. v20}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v19

    .line 432
    :cond_9
    add-int/2addr v9, v2

    goto :goto_6

    .line 434
    .end local v2    # "count":I
    :cond_a
    move-object v15, v4

    .line 436
    .local v15, "value":[B
    goto/16 :goto_2

    .line 439
    .end local v4    # "data":[B
    .end local v9    # "pos":I
    .end local v14    # "total":I
    .end local v15    # "value":[B
    :pswitch_4
    new-instance v5, Lcom/google/common/io/protocol/ProtoBuf;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/common/io/protocol/ProtoBuf;->msgType:Lcom/google/common/io/protocol/ProtoBufType;

    move-object/from16 v19, v0

    if-nez v19, :cond_b

    const/16 v19, 0x0

    :goto_7
    move-object/from16 v0, v19

    invoke-direct {v5, v0}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    .line 442
    .local v5, "group":Lcom/google/common/io/protocol/ProtoBuf;
    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v5, v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->parse(Ljava/io/InputStream;I)I

    move-result p2

    .line 443
    move-object v15, v5

    .line 444
    .local v15, "value":Lcom/google/common/io/protocol/ProtoBuf;
    goto/16 :goto_2

    .line 439
    .end local v5    # "group":Lcom/google/common/io/protocol/ProtoBuf;
    .end local v15    # "value":Lcom/google/common/io/protocol/ProtoBuf;
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/common/io/protocol/ProtoBuf;->msgType:Lcom/google/common/io/protocol/ProtoBufType;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Lcom/google/common/io/protocol/ProtoBufType;->getData(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/google/common/io/protocol/ProtoBufType;

    goto :goto_7

    .line 456
    .end local v11    # "tag":I
    .end local v12    # "tagAndType":J
    .end local v18    # "wireType":I
    :cond_c
    return p2

    .line 385
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public parse([B)Lcom/google/common/io/protocol/ProtoBuf;
    .locals 2
    .param p1, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 333
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    array-length v1, p1

    invoke-virtual {p0, v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->parse(Ljava/io/InputStream;I)I

    .line 334
    return-object p0
.end method

.method public setBool(IZ)V
    .locals 1
    .param p1, "tag"    # I
    .param p2, "value"    # Z

    .prologue
    .line 707
    if-eqz p2, :cond_0

    sget-object v0, Lcom/google/common/io/protocol/ProtoBuf;->TRUE:Ljava/lang/Boolean;

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/common/io/protocol/ProtoBuf;->setObject(ILjava/lang/Object;)V

    .line 708
    return-void

    .line 707
    :cond_0
    sget-object v0, Lcom/google/common/io/protocol/ProtoBuf;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public setFloat(IF)V
    .locals 1
    .param p1, "tag"    # I
    .param p2, "value"    # F

    .prologue
    .line 743
    invoke-static {p2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    .line 744
    return-void
.end method

.method public setInt(II)V
    .locals 2
    .param p1, "tag"    # I
    .param p2, "value"    # I

    .prologue
    .line 721
    int-to-long v0, p2

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->setLong(IJ)V

    .line 722
    return-void
.end method

.method public setLong(IJ)V
    .locals 2
    .param p1, "tag"    # I
    .param p2, "value"    # J

    .prologue
    .line 728
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_0

    sget-object v0, Lcom/google/common/io/protocol/ProtoBuf;->SMALL_NUMBERS:[Ljava/lang/Long;

    array-length v0, v0

    int-to-long v0, v0

    cmp-long v0, p2, v0

    if-gez v0, :cond_0

    sget-object v0, Lcom/google/common/io/protocol/ProtoBuf;->SMALL_NUMBERS:[Ljava/lang/Long;

    long-to-int v1, p2

    aget-object v0, v0, v1

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/common/io/protocol/ProtoBuf;->setObject(ILjava/lang/Object;)V

    .line 730
    return-void

    .line 728
    :cond_0
    new-instance v0, Ljava/lang/Long;

    invoke-direct {v0, p2, p3}, Ljava/lang/Long;-><init>(J)V

    goto :goto_0
.end method

.method public setProtoBuf(ILcom/google/common/io/protocol/ProtoBuf;)V
    .locals 0
    .param p1, "tag"    # I
    .param p2, "pb"    # Lcom/google/common/io/protocol/ProtoBuf;

    .prologue
    .line 750
    invoke-direct {p0, p1, p2}, Lcom/google/common/io/protocol/ProtoBuf;->setObject(ILjava/lang/Object;)V

    .line 751
    return-void
.end method

.method public setString(ILjava/lang/String;)V
    .locals 0
    .param p1, "tag"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 769
    invoke-direct {p0, p1, p2}, Lcom/google/common/io/protocol/ProtoBuf;->setObject(ILjava/lang/Object;)V

    .line 770
    return-void
.end method
