.class public Lcom/android/externalstorage/TestDocumentsProvider;
.super Landroid/provider/DocumentsProvider;
.source "TestDocumentsProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/externalstorage/TestDocumentsProvider$CloudCursor;,
        Lcom/android/externalstorage/TestDocumentsProvider$CloudTask;
    }
.end annotation


# static fields
.field private static final DEFAULT_DOCUMENT_PROJECTION:[Ljava/lang/String;

.field private static final DEFAULT_ROOT_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mAuthority:Ljava/lang/String;

.field private mTask:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/android/externalstorage/TestDocumentsProvider$CloudTask;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 82
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "root_id"

    aput-object v1, v0, v3

    const-string v1, "flags"

    aput-object v1, v0, v4

    const-string v1, "icon"

    aput-object v1, v0, v5

    const-string v1, "title"

    aput-object v1, v0, v6

    const-string v1, "summary"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "document_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "available_bytes"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/externalstorage/TestDocumentsProvider;->DEFAULT_ROOT_PROJECTION:[Ljava/lang/String;

    .line 88
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "document_id"

    aput-object v1, v0, v3

    const-string v1, "mime_type"

    aput-object v1, v0, v4

    const-string v1, "_display_name"

    aput-object v1, v0, v5

    const-string v1, "last_modified"

    aput-object v1, v0, v6

    const-string v1, "flags"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "_size"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/externalstorage/TestDocumentsProvider;->DEFAULT_DOCUMENT_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Landroid/provider/DocumentsProvider;-><init>()V

    .line 209
    return-void
.end method

.method static synthetic access$000(Landroid/database/MatrixCursor;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Landroid/database/MatrixCursor;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 55
    invoke-static {p0, p1, p2}, Lcom/android/externalstorage/TestDocumentsProvider;->includeFile(Landroid/database/MatrixCursor;Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$100(Lcom/android/externalstorage/TestDocumentsProvider;)Ljava/lang/ref/WeakReference;
    .locals 1
    .param p0, "x0"    # Lcom/android/externalstorage/TestDocumentsProvider;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/externalstorage/TestDocumentsProvider;->mTask:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method private static includeFile(Landroid/database/MatrixCursor;Ljava/lang/String;I)V
    .locals 4
    .param p0, "result"    # Landroid/database/MatrixCursor;
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "flags"    # I

    .prologue
    .line 394
    invoke-virtual {p0}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    .line 395
    .local v0, "row":Landroid/database/MatrixCursor$RowBuilder;
    const-string v1, "document_id"

    invoke-virtual {v0, v1, p1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 396
    const-string v1, "_display_name"

    invoke-virtual {v0, v1, p1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 397
    const-string v1, "last_modified"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 398
    const-string v1, "flags"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 400
    const-string v1, "myDoc"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 401
    const-string v1, "mime_type"

    const-string v2, "vnd.android.document/directory"

    invoke-virtual {v0, v1, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 402
    const-string v1, "flags"

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 408
    :cond_0
    :goto_0
    return-void

    .line 403
    :cond_1
    const-string v1, "myNull"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 406
    const-string v1, "mime_type"

    const-string v2, "application/octet-stream"

    invoke-virtual {v0, v1, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_0
.end method

.method private static resolveDocumentProjection([Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0, "projection"    # [Ljava/lang/String;

    .prologue
    .line 100
    if-eqz p0, :cond_0

    .end local p0    # "projection":[Ljava/lang/String;
    :goto_0
    return-object p0

    .restart local p0    # "projection":[Ljava/lang/String;
    :cond_0
    sget-object p0, Lcom/android/externalstorage/TestDocumentsProvider;->DEFAULT_DOCUMENT_PROJECTION:[Ljava/lang/String;

    goto :goto_0
.end method

.method private static resolveRootProjection([Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0, "projection"    # [Ljava/lang/String;

    .prologue
    .line 95
    if-eqz p0, :cond_0

    .end local p0    # "projection":[Ljava/lang/String;
    :goto_0
    return-object p0

    .restart local p0    # "projection":[Ljava/lang/String;
    :cond_0
    sget-object p0, Lcom/android/externalstorage/TestDocumentsProvider;->DEFAULT_ROOT_PROJECTION:[Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "info"    # Landroid/content/pm/ProviderInfo;

    .prologue
    .line 107
    iget-object v0, p2, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/externalstorage/TestDocumentsProvider;->mAuthority:Ljava/lang/String;

    .line 108
    invoke-super {p0, p1, p2}, Landroid/provider/DocumentsProvider;->attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V

    .line 109
    return-void
.end method

.method public createDocument(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "parentDocumentId"    # Ljava/lang/String;
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "displayName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 160
    invoke-super {p0, p1, p2, p3}, Landroid/provider/DocumentsProvider;->createDocument(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 359
    const/4 v0, 0x1

    return v0
.end method

.method public openDocument(Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/os/ParcelFileDescriptor;
    .locals 1
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "mode"    # Ljava/lang/String;
    .param p3, "signal"    # Landroid/os/CancellationSignal;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 314
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0
.end method

.method public openDocumentThumbnail(Ljava/lang/String;Landroid/graphics/Point;Landroid/os/CancellationSignal;)Landroid/content/res/AssetFileDescriptor;
    .locals 19
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "sizeHint"    # Landroid/graphics/Point;
    .param p3, "signal"    # Landroid/os/CancellationSignal;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 325
    const/16 v3, 0x20

    const/16 v4, 0x20

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 326
    .local v15, "bitmap":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v15}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 327
    .local v2, "canvas":Landroid/graphics/Canvas;
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 328
    .local v7, "paint":Landroid/graphics/Paint;
    const v3, -0xffff01

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 329
    const/high16 v3, -0x10000

    invoke-virtual {v2, v3}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 330
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/high16 v5, 0x42000000    # 32.0f

    const/high16 v6, 0x42000000    # 32.0f

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 332
    new-instance v16, Ljava/io/ByteArrayOutputStream;

    invoke-direct/range {v16 .. v16}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 333
    .local v16, "bos":Ljava/io/ByteArrayOutputStream;
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x32

    move-object/from16 v0, v16

    invoke-virtual {v15, v3, v4, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 335
    new-instance v14, Ljava/io/ByteArrayInputStream;

    invoke-virtual/range {v16 .. v16}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    invoke-direct {v14, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 337
    .local v14, "bis":Ljava/io/ByteArrayInputStream;
    :try_start_0
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createReliablePipe()[Landroid/os/ParcelFileDescriptor;

    move-result-object v18

    .line 338
    .local v18, "fds":[Landroid/os/ParcelFileDescriptor;
    new-instance v3, Lcom/android/externalstorage/TestDocumentsProvider$3;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v3, v0, v1, v14}, Lcom/android/externalstorage/TestDocumentsProvider$3;-><init>(Lcom/android/externalstorage/TestDocumentsProvider;[Landroid/os/ParcelFileDescriptor;Ljava/io/ByteArrayInputStream;)V

    sget-object v4, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lcom/android/externalstorage/TestDocumentsProvider$3;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 351
    new-instance v8, Landroid/content/res/AssetFileDescriptor;

    const/4 v3, 0x0

    aget-object v9, v18, v3

    const-wide/16 v10, 0x0

    const-wide/16 v12, -0x1

    invoke-direct/range {v8 .. v13}, Landroid/content/res/AssetFileDescriptor;-><init>(Landroid/os/ParcelFileDescriptor;JJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v8

    .line 352
    .end local v18    # "fds":[Landroid/os/ParcelFileDescriptor;
    :catch_0
    move-exception v17

    .line 353
    .local v17, "e":Ljava/io/IOException;
    new-instance v3, Ljava/io/FileNotFoundException;

    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public queryChildDocuments(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1, "parentDocumentId"    # Ljava/lang/String;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 232
    invoke-virtual {p0}, Lcom/android/externalstorage/TestDocumentsProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 233
    .local v1, "resolver":Landroid/content/ContentResolver;
    const-string v4, "com.example.documents"

    invoke-static {v4, p1}, Landroid/provider/DocumentsContract;->buildDocumentUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 236
    .local v0, "notifyUri":Landroid/net/Uri;
    new-instance v2, Lcom/android/externalstorage/TestDocumentsProvider$CloudCursor;

    invoke-static {p2}, Lcom/android/externalstorage/TestDocumentsProvider;->resolveDocumentProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/android/externalstorage/TestDocumentsProvider$CloudCursor;-><init>([Ljava/lang/String;)V

    .line 237
    .local v2, "result":Lcom/android/externalstorage/TestDocumentsProvider$CloudCursor;
    invoke-virtual {v2, v1, v0}, Lcom/android/externalstorage/TestDocumentsProvider$CloudCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 240
    const-string v4, "myNull"

    invoke-static {v2, v4, v5}, Lcom/android/externalstorage/TestDocumentsProvider;->includeFile(Landroid/database/MatrixCursor;Ljava/lang/String;I)V

    .line 241
    const-string v4, "localfile1"

    invoke-static {v2, v4, v5}, Lcom/android/externalstorage/TestDocumentsProvider;->includeFile(Landroid/database/MatrixCursor;Ljava/lang/String;I)V

    .line 242
    const-string v4, "localfile2"

    invoke-static {v2, v4, v6}, Lcom/android/externalstorage/TestDocumentsProvider;->includeFile(Landroid/database/MatrixCursor;Ljava/lang/String;I)V

    .line 243
    const-string v4, "localfile3"

    invoke-static {v2, v4, v5}, Lcom/android/externalstorage/TestDocumentsProvider;->includeFile(Landroid/database/MatrixCursor;Ljava/lang/String;I)V

    .line 244
    const-string v4, "localfile4"

    invoke-static {v2, v4, v5}, Lcom/android/externalstorage/TestDocumentsProvider;->includeFile(Landroid/database/MatrixCursor;Ljava/lang/String;I)V

    .line 252
    monitor-enter p0

    .line 254
    :try_start_0
    iget-object v4, p0, Lcom/android/externalstorage/TestDocumentsProvider;->mTask:Ljava/lang/ref/WeakReference;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/externalstorage/TestDocumentsProvider;->mTask:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/externalstorage/TestDocumentsProvider$CloudTask;

    move-object v3, v4

    .line 255
    .local v3, "task":Lcom/android/externalstorage/TestDocumentsProvider$CloudTask;
    :goto_0
    if-nez v3, :cond_0

    .line 256
    const-string v4, "TestDocuments"

    const-string v5, "No network task found; starting!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    new-instance v3, Lcom/android/externalstorage/TestDocumentsProvider$CloudTask;

    .end local v3    # "task":Lcom/android/externalstorage/TestDocumentsProvider$CloudTask;
    invoke-direct {v3, v1, v0}, Lcom/android/externalstorage/TestDocumentsProvider$CloudTask;-><init>(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 258
    .restart local v3    # "task":Lcom/android/externalstorage/TestDocumentsProvider$CloudTask;
    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/android/externalstorage/TestDocumentsProvider;->mTask:Ljava/lang/ref/WeakReference;

    .line 259
    new-instance v4, Ljava/lang/Thread;

    invoke-direct {v4, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 262
    new-instance v4, Lcom/android/externalstorage/TestDocumentsProvider$2;

    invoke-direct {v4, p0}, Lcom/android/externalstorage/TestDocumentsProvider$2;-><init>(Lcom/android/externalstorage/TestDocumentsProvider;)V

    invoke-virtual {v4}, Lcom/android/externalstorage/TestDocumentsProvider$2;->start()V

    .line 276
    :cond_0
    invoke-virtual {v3, v2}, Lcom/android/externalstorage/TestDocumentsProvider$CloudTask;->includeIfFinished(Landroid/database/MatrixCursor;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 277
    iget-object v4, v2, Lcom/android/externalstorage/TestDocumentsProvider$CloudCursor;->extras:Landroid/os/Bundle;

    const-string v5, "info"

    const-string v6, "Everything Went Better Than Expected and this message is quite long and verbose and maybe even too long"

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    iget-object v4, v2, Lcom/android/externalstorage/TestDocumentsProvider$CloudCursor;->extras:Landroid/os/Bundle;

    const-string v5, "error"

    const-string v6, "But then again, maybe our server ran into an error, which means we\'re going to have a bad time"

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    :goto_1
    iput-object v3, v2, Lcom/android/externalstorage/TestDocumentsProvider$CloudCursor;->keepAlive:Ljava/lang/Object;

    .line 290
    monitor-exit p0

    return-object v2

    .line 254
    .end local v3    # "task":Lcom/android/externalstorage/TestDocumentsProvider$CloudTask;
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 284
    .restart local v3    # "task":Lcom/android/externalstorage/TestDocumentsProvider$CloudTask;
    :cond_2
    iget-object v4, v2, Lcom/android/externalstorage/TestDocumentsProvider$CloudCursor;->extras:Landroid/os/Bundle;

    const-string v5, "loading"

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1

    .line 291
    .end local v3    # "task":Lcom/android/externalstorage/TestDocumentsProvider$CloudTask;
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method

.method public queryDocument(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 2
    .param p1, "documentId"    # Ljava/lang/String;
    .param p2, "projection"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 150
    new-instance v0, Landroid/database/MatrixCursor;

    invoke-static {p2}, Lcom/android/externalstorage/TestDocumentsProvider;->resolveDocumentProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 151
    .local v0, "result":Landroid/database/MatrixCursor;
    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/android/externalstorage/TestDocumentsProvider;->includeFile(Landroid/database/MatrixCursor;Ljava/lang/String;I)V

    .line 152
    return-object v0
.end method

.method public queryRecentDocuments(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 4
    .param p1, "rootId"    # Ljava/lang/String;
    .param p2, "projection"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 302
    const-wide/16 v2, 0xbb8

    invoke-static {v2, v3}, Landroid/os/SystemClock;->sleep(J)V

    .line 304
    new-instance v0, Landroid/database/MatrixCursor;

    invoke-static {p2}, Lcom/android/externalstorage/TestDocumentsProvider;->resolveDocumentProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 305
    .local v0, "result":Landroid/database/MatrixCursor;
    const-string v1, "It was /worth/ the_wait for?the file:with the&incredibly long name"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/android/externalstorage/TestDocumentsProvider;->includeFile(Landroid/database/MatrixCursor;Ljava/lang/String;I)V

    .line 307
    return-object v0
.end method

.method public queryRoots([Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "projection"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 113
    const-string v2, "TestDocuments"

    const-string v3, "Someone asked for our roots!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    new-instance v0, Landroid/database/MatrixCursor;

    invoke-static {p1}, Lcom/android/externalstorage/TestDocumentsProvider;->resolveRootProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 133
    .local v0, "result":Landroid/database/MatrixCursor;
    invoke-virtual {v0}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v1

    .line 134
    .local v1, "row":Landroid/database/MatrixCursor$RowBuilder;
    const-string v2, "root_id"

    const-string v3, "myRoot"

    invoke-virtual {v1, v2, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 135
    const-string v2, "flags"

    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 136
    const-string v2, "title"

    const-string v3, "_Test title which is really long"

    invoke-virtual {v1, v2, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 137
    const-string v2, "summary"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " summary which is also super long text"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 139
    const-string v2, "document_id"

    const-string v3, "myDoc"

    invoke-virtual {v1, v2, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 140
    const-string v2, "available_bytes"

    const/16 v3, 0x400

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 141
    return-object v0
.end method
