.class Lcom/android/externalstorage/TestDocumentsProvider$CloudTask;
.super Ljava/lang/Object;
.source "TestDocumentsProvider.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/externalstorage/TestDocumentsProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CloudTask"
.end annotation


# instance fields
.field private volatile mFinished:Z

.field private final mNotifyUri:Landroid/net/Uri;

.field private final mResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    .locals 0
    .param p1, "resolver"    # Landroid/content/ContentResolver;
    .param p2, "notifyUri"    # Landroid/net/Uri;

    .prologue
    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    iput-object p1, p0, Lcom/android/externalstorage/TestDocumentsProvider$CloudTask;->mResolver:Landroid/content/ContentResolver;

    .line 177
    iput-object p2, p0, Lcom/android/externalstorage/TestDocumentsProvider$CloudTask;->mNotifyUri:Landroid/net/Uri;

    .line 178
    return-void
.end method


# virtual methods
.method public includeIfFinished(Landroid/database/MatrixCursor;)Z
    .locals 4
    .param p1, "result"    # Landroid/database/MatrixCursor;

    .prologue
    const/4 v0, 0x0

    .line 194
    const-string v1, "TestDocuments"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": includeIfFinished() found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/externalstorage/TestDocumentsProvider$CloudTask;->mFinished:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    iget-boolean v1, p0, Lcom/android/externalstorage/TestDocumentsProvider$CloudTask;->mFinished:Z

    if-eqz v1, :cond_0

    .line 196
    const-string v1, "_networkfile1"

    # invokes: Lcom/android/externalstorage/TestDocumentsProvider;->includeFile(Landroid/database/MatrixCursor;Ljava/lang/String;I)V
    invoke-static {p1, v1, v0}, Lcom/android/externalstorage/TestDocumentsProvider;->access$000(Landroid/database/MatrixCursor;Ljava/lang/String;I)V

    .line 197
    const-string v1, "_networkfile2"

    # invokes: Lcom/android/externalstorage/TestDocumentsProvider;->includeFile(Landroid/database/MatrixCursor;Ljava/lang/String;I)V
    invoke-static {p1, v1, v0}, Lcom/android/externalstorage/TestDocumentsProvider;->access$000(Landroid/database/MatrixCursor;Ljava/lang/String;I)V

    .line 198
    const-string v1, "_networkfile3"

    # invokes: Lcom/android/externalstorage/TestDocumentsProvider;->includeFile(Landroid/database/MatrixCursor;Ljava/lang/String;I)V
    invoke-static {p1, v1, v0}, Lcom/android/externalstorage/TestDocumentsProvider;->access$000(Landroid/database/MatrixCursor;Ljava/lang/String;I)V

    .line 199
    const-string v1, "_networkfile4"

    # invokes: Lcom/android/externalstorage/TestDocumentsProvider;->includeFile(Landroid/database/MatrixCursor;Ljava/lang/String;I)V
    invoke-static {p1, v1, v0}, Lcom/android/externalstorage/TestDocumentsProvider;->access$000(Landroid/database/MatrixCursor;Ljava/lang/String;I)V

    .line 200
    const-string v1, "_networkfile5"

    # invokes: Lcom/android/externalstorage/TestDocumentsProvider;->includeFile(Landroid/database/MatrixCursor;Ljava/lang/String;I)V
    invoke-static {p1, v1, v0}, Lcom/android/externalstorage/TestDocumentsProvider;->access$000(Landroid/database/MatrixCursor;Ljava/lang/String;I)V

    .line 201
    const-string v1, "_networkfile6"

    # invokes: Lcom/android/externalstorage/TestDocumentsProvider;->includeFile(Landroid/database/MatrixCursor;Ljava/lang/String;I)V
    invoke-static {p1, v1, v0}, Lcom/android/externalstorage/TestDocumentsProvider;->access$000(Landroid/database/MatrixCursor;Ljava/lang/String;I)V

    .line 202
    const/4 v0, 0x1

    .line 204
    :cond_0
    return v0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 183
    const-string v0, "TestDocuments"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": pretending to do some network!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    const-wide/16 v0, 0x7d0

    invoke-static {v0, v1}, Landroid/os/SystemClock;->sleep(J)V

    .line 185
    const-string v0, "TestDocuments"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": network done!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/externalstorage/TestDocumentsProvider$CloudTask;->mFinished:Z

    .line 190
    iget-object v0, p0, Lcom/android/externalstorage/TestDocumentsProvider$CloudTask;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/android/externalstorage/TestDocumentsProvider$CloudTask;->mNotifyUri:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 191
    return-void
.end method
