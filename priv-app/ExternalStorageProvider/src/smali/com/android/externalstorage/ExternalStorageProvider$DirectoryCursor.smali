.class Lcom/android/externalstorage/ExternalStorageProvider$DirectoryCursor;
.super Landroid/database/MatrixCursor;
.source "ExternalStorageProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/externalstorage/ExternalStorageProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DirectoryCursor"
.end annotation


# instance fields
.field private final mFile:Ljava/io/File;

.field final synthetic this$0:Lcom/android/externalstorage/ExternalStorageProvider;


# direct methods
.method public constructor <init>(Lcom/android/externalstorage/ExternalStorageProvider;[Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V
    .locals 2
    .param p2, "columnNames"    # [Ljava/lang/String;
    .param p3, "docId"    # Ljava/lang/String;
    .param p4, "file"    # Ljava/io/File;

    .prologue
    .line 605
    iput-object p1, p0, Lcom/android/externalstorage/ExternalStorageProvider$DirectoryCursor;->this$0:Lcom/android/externalstorage/ExternalStorageProvider;

    .line 606
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 608
    const-string v1, "com.android.externalstorage.documents"

    invoke-static {v1, p3}, Landroid/provider/DocumentsContract;->buildChildDocumentsUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 610
    .local v0, "notifyUri":Landroid/net/Uri;
    invoke-virtual {p1}, Lcom/android/externalstorage/ExternalStorageProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/android/externalstorage/ExternalStorageProvider$DirectoryCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 612
    iput-object p4, p0, Lcom/android/externalstorage/ExternalStorageProvider$DirectoryCursor;->mFile:Ljava/io/File;

    .line 613
    iget-object v1, p0, Lcom/android/externalstorage/ExternalStorageProvider$DirectoryCursor;->mFile:Ljava/io/File;

    # invokes: Lcom/android/externalstorage/ExternalStorageProvider;->startObserving(Ljava/io/File;Landroid/net/Uri;)V
    invoke-static {p1, v1, v0}, Lcom/android/externalstorage/ExternalStorageProvider;->access$200(Lcom/android/externalstorage/ExternalStorageProvider;Ljava/io/File;Landroid/net/Uri;)V

    .line 614
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 618
    invoke-super {p0}, Landroid/database/MatrixCursor;->close()V

    .line 619
    iget-object v0, p0, Lcom/android/externalstorage/ExternalStorageProvider$DirectoryCursor;->this$0:Lcom/android/externalstorage/ExternalStorageProvider;

    iget-object v1, p0, Lcom/android/externalstorage/ExternalStorageProvider$DirectoryCursor;->mFile:Ljava/io/File;

    # invokes: Lcom/android/externalstorage/ExternalStorageProvider;->stopObserving(Ljava/io/File;)V
    invoke-static {v0, v1}, Lcom/android/externalstorage/ExternalStorageProvider;->access$300(Lcom/android/externalstorage/ExternalStorageProvider;Ljava/io/File;)V

    .line 620
    return-void
.end method
