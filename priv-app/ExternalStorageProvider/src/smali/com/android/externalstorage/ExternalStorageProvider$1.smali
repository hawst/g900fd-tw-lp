.class Lcom/android/externalstorage/ExternalStorageProvider$1;
.super Ljava/lang/Object;
.source "ExternalStorageProvider.java"

# interfaces
.implements Landroid/os/ParcelFileDescriptor$OnCloseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/externalstorage/ExternalStorageProvider;->openDocument(Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/os/ParcelFileDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/externalstorage/ExternalStorageProvider;

.field final synthetic val$file:Ljava/io/File;


# direct methods
.method constructor <init>(Lcom/android/externalstorage/ExternalStorageProvider;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 497
    iput-object p1, p0, Lcom/android/externalstorage/ExternalStorageProvider$1;->this$0:Lcom/android/externalstorage/ExternalStorageProvider;

    iput-object p2, p0, Lcom/android/externalstorage/ExternalStorageProvider$1;->val$file:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClose(Ljava/io/IOException;)V
    .locals 2
    .param p1, "e"    # Ljava/io/IOException;

    .prologue
    .line 500
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 502
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/externalstorage/ExternalStorageProvider$1;->val$file:Ljava/io/File;

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 503
    iget-object v1, p0, Lcom/android/externalstorage/ExternalStorageProvider$1;->this$0:Lcom/android/externalstorage/ExternalStorageProvider;

    invoke-virtual {v1}, Lcom/android/externalstorage/ExternalStorageProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 504
    return-void
.end method
