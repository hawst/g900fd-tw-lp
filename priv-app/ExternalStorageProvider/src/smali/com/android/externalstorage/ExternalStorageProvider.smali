.class public Lcom/android/externalstorage/ExternalStorageProvider;
.super Landroid/provider/DocumentsProvider;
.source "ExternalStorageProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/externalstorage/ExternalStorageProvider$DirectoryCursor;,
        Lcom/android/externalstorage/ExternalStorageProvider$DirectoryObserver;,
        Lcom/android/externalstorage/ExternalStorageProvider$RootInfo;
    }
.end annotation


# static fields
.field private static final DEFAULT_DOCUMENT_PROJECTION:[Ljava/lang/String;

.field private static final DEFAULT_ROOT_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mIdToPath:Ljava/util/HashMap;
    .annotation build Lcom/android/internal/annotations/GuardedBy;
        value = "mRootsLock"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private mIdToRoot:Ljava/util/HashMap;
    .annotation build Lcom/android/internal/annotations/GuardedBy;
        value = "mRootsLock"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/externalstorage/ExternalStorageProvider$RootInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mObservers:Ljava/util/Map;
    .annotation build Lcom/android/internal/annotations/GuardedBy;
        value = "mObservers"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/io/File;",
            "Lcom/android/externalstorage/ExternalStorageProvider$DirectoryObserver;",
            ">;"
        }
    .end annotation
.end field

.field private mRoots:Ljava/util/ArrayList;
    .annotation build Lcom/android/internal/annotations/GuardedBy;
        value = "mRootsLock"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/externalstorage/ExternalStorageProvider$RootInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mRootsLock:Ljava/lang/Object;

.field private mStorageManager:Landroid/os/storage/StorageManager;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 68
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "root_id"

    aput-object v1, v0, v3

    const-string v1, "flags"

    aput-object v1, v0, v4

    const-string v1, "icon"

    aput-object v1, v0, v5

    const-string v1, "title"

    aput-object v1, v0, v6

    const-string v1, "document_id"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "available_bytes"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/externalstorage/ExternalStorageProvider;->DEFAULT_ROOT_PROJECTION:[Ljava/lang/String;

    .line 73
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "document_id"

    aput-object v1, v0, v3

    const-string v1, "mime_type"

    aput-object v1, v0, v4

    const-string v1, "_display_name"

    aput-object v1, v0, v5

    const-string v1, "last_modified"

    aput-object v1, v0, v6

    const-string v1, "flags"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "_size"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/externalstorage/ExternalStorageProvider;->DEFAULT_DOCUMENT_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Landroid/provider/DocumentsProvider;-><init>()V

    .line 90
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mRootsLock:Ljava/lang/Object;

    .line 99
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mObservers:Ljava/util/Map;

    .line 602
    return-void
.end method

.method static synthetic access$200(Lcom/android/externalstorage/ExternalStorageProvider;Ljava/io/File;Landroid/net/Uri;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/externalstorage/ExternalStorageProvider;
    .param p1, "x1"    # Ljava/io/File;
    .param p2, "x2"    # Landroid/net/Uri;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/android/externalstorage/ExternalStorageProvider;->startObserving(Ljava/io/File;Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/externalstorage/ExternalStorageProvider;Ljava/io/File;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/externalstorage/ExternalStorageProvider;
    .param p1, "x1"    # Ljava/io/File;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/android/externalstorage/ExternalStorageProvider;->stopObserving(Ljava/io/File;)V

    return-void
.end method

.method private static buildFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 3
    .param p0, "parent"    # Ljava/io/File;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "ext"    # Ljava/lang/String;

    .prologue
    .line 339
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 340
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 342
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static buildUniqueFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 10
    .param p0, "parent"    # Ljava/io/File;
    .param p1, "mimeType"    # Ljava/lang/String;
    .param p2, "displayName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 352
    const-string v8, "vnd.android.document/directory"

    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 353
    move-object v7, p2

    .line 354
    .local v7, "name":Ljava/lang/String;
    const/4 v0, 0x0

    .line 386
    .local v0, "ext":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-static {p0, v7, v0}, Lcom/android/externalstorage/ExternalStorageProvider;->buildFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 389
    .local v2, "file":Ljava/io/File;
    const/4 v5, 0x0

    .line 390
    .local v5, "n":I
    :goto_1
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 391
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "n":I
    .local v6, "n":I
    const/16 v8, 0x20

    if-lt v5, v8, :cond_4

    .line 392
    new-instance v8, Ljava/io/FileNotFoundException;

    const-string v9, "Failed to create unique file"

    invoke-direct {v8, v9}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 359
    .end local v0    # "ext":Ljava/lang/String;
    .end local v2    # "file":Ljava/io/File;
    .end local v6    # "n":I
    .end local v7    # "name":Ljava/lang/String;
    :cond_1
    const/16 v8, 0x2e

    invoke-virtual {p2, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 360
    .local v3, "lastDot":I
    if-ltz v3, :cond_3

    .line 361
    const/4 v8, 0x0

    invoke-virtual {p2, v8, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 362
    .restart local v7    # "name":Ljava/lang/String;
    add-int/lit8 v8, v3, 0x1

    invoke-virtual {p2, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 363
    .restart local v0    # "ext":Ljava/lang/String;
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 371
    .local v4, "mimeTypeFromExt":Ljava/lang/String;
    :goto_2
    if-nez v4, :cond_2

    .line 372
    const-string v4, "application/octet-stream"

    .line 375
    :cond_2
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v8

    invoke-virtual {v8, p1}, Landroid/webkit/MimeTypeMap;->getExtensionFromMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 377
    .local v1, "extFromMimeType":Ljava/lang/String;
    invoke-static {p1, v4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-static {v0, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 381
    move-object v7, p2

    .line 382
    move-object v0, v1

    goto :goto_0

    .line 366
    .end local v0    # "ext":Ljava/lang/String;
    .end local v1    # "extFromMimeType":Ljava/lang/String;
    .end local v4    # "mimeTypeFromExt":Ljava/lang/String;
    .end local v7    # "name":Ljava/lang/String;
    :cond_3
    move-object v7, p2

    .line 367
    .restart local v7    # "name":Ljava/lang/String;
    const/4 v0, 0x0

    .line 368
    .restart local v0    # "ext":Ljava/lang/String;
    const/4 v4, 0x0

    .restart local v4    # "mimeTypeFromExt":Ljava/lang/String;
    goto :goto_2

    .line 394
    .end local v3    # "lastDot":I
    .end local v4    # "mimeTypeFromExt":Ljava/lang/String;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v6    # "n":I
    :cond_4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v8, v0}, Lcom/android/externalstorage/ExternalStorageProvider;->buildFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    move v5, v6

    .end local v6    # "n":I
    .restart local v5    # "n":I
    goto :goto_1

    .line 397
    :cond_5
    return-object v2
.end method

.method private getDocIdForFile(Ljava/io/File;)Ljava/lang/String;
    .locals 8
    .param p1, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 184
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 187
    .local v2, "path":Ljava/lang/String;
    const/4 v1, 0x0

    .line 188
    .local v1, "mostSpecific":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/io/File;>;"
    iget-object v6, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mRootsLock:Ljava/lang/Object;

    monitor-enter v6

    .line 189
    :try_start_0
    iget-object v5, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mIdToPath:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 190
    .local v3, "root":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/io/File;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    .line 191
    .local v4, "rootPath":Ljava/lang/String;
    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    if-eqz v1, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-le v7, v5, :cond_0

    .line 193
    :cond_1
    move-object v1, v3

    goto :goto_0

    .line 196
    .end local v3    # "root":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/io/File;>;"
    .end local v4    # "rootPath":Ljava/lang/String;
    :cond_2
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 198
    if-nez v1, :cond_3

    .line 199
    new-instance v5, Ljava/io/FileNotFoundException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to find root that contains "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 196
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .line 203
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    .line 204
    .restart local v4    # "rootPath":Ljava/lang/String;
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 205
    const-string v2, ""

    .line 212
    :goto_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x3a

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 206
    :cond_4
    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 207
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 209
    :cond_5
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method private getFileForDocId(Ljava/lang/String;)Ljava/io/File;
    .locals 8
    .param p1, "docId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 216
    const/16 v5, 0x3a

    const/4 v6, 0x1

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 217
    .local v1, "splitIndex":I
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 218
    .local v2, "tag":Ljava/lang/String;
    add-int/lit8 v5, v1, 0x1

    invoke-virtual {p1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 221
    .local v0, "path":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mRootsLock:Ljava/lang/Object;

    monitor-enter v6

    .line 222
    :try_start_0
    iget-object v5, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mIdToPath:Ljava/util/HashMap;

    invoke-virtual {v5, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    .line 223
    .local v3, "target":Ljava/io/File;
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    if-nez v3, :cond_0

    .line 225
    new-instance v5, Ljava/io/FileNotFoundException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "No root for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 223
    .end local v3    # "target":Ljava/io/File;
    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .line 227
    .restart local v3    # "target":Ljava/io/File;
    :cond_0
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_1

    .line 228
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    .line 230
    :cond_1
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 231
    .end local v3    # "target":Ljava/io/File;
    .local v4, "target":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_2

    .line 232
    new-instance v5, Ljava/io/FileNotFoundException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Missing file for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " at "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 234
    :cond_2
    return-object v4
.end method

.method private static getTypeForFile(Ljava/io/File;)Ljava/lang/String;
    .locals 1
    .param p0, "file"    # Ljava/io/File;

    .prologue
    .line 521
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 522
    const-string v0, "vnd.android.document/directory"

    .line 524
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/externalstorage/ExternalStorageProvider;->getTypeForName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getTypeForName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 529
    const/16 v3, 0x2e

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 530
    .local v1, "lastDot":I
    if-ltz v1, :cond_0

    .line 531
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 532
    .local v0, "extension":Ljava/lang/String;
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 533
    .local v2, "mime":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 538
    .end local v0    # "extension":Ljava/lang/String;
    .end local v2    # "mime":Ljava/lang/String;
    :goto_0
    return-object v2

    :cond_0
    const-string v2, "application/octet-stream"

    goto :goto_0
.end method

.method private includeFile(Landroid/database/MatrixCursor;Ljava/lang/String;Ljava/io/File;)V
    .locals 10
    .param p1, "result"    # Landroid/database/MatrixCursor;
    .param p2, "docId"    # Ljava/lang/String;
    .param p3, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 239
    if-nez p2, :cond_3

    .line 240
    invoke-direct {p0, p3}, Lcom/android/externalstorage/ExternalStorageProvider;->getDocIdForFile(Ljava/io/File;)Ljava/lang/String;

    move-result-object p2

    .line 245
    :goto_0
    const/4 v1, 0x0

    .line 247
    .local v1, "flags":I
    invoke-virtual {p3}, Ljava/io/File;->canWrite()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 248
    invoke-virtual {p3}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 249
    or-int/lit8 v1, v1, 0x8

    .line 250
    or-int/lit8 v1, v1, 0x4

    .line 251
    or-int/lit8 v1, v1, 0x40

    .line 259
    :cond_0
    :goto_1
    invoke-virtual {p3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 260
    .local v0, "displayName":Ljava/lang/String;
    invoke-static {p3}, Lcom/android/externalstorage/ExternalStorageProvider;->getTypeForFile(Ljava/io/File;)Ljava/lang/String;

    move-result-object v4

    .line 261
    .local v4, "mimeType":Ljava/lang/String;
    const-string v6, "image/"

    invoke-virtual {v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 262
    or-int/lit8 v1, v1, 0x1

    .line 265
    :cond_1
    invoke-virtual {p1}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    .line 266
    .local v5, "row":Landroid/database/MatrixCursor$RowBuilder;
    const-string v6, "document_id"

    invoke-virtual {v5, v6, p2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 267
    const-string v6, "_display_name"

    invoke-virtual {v5, v6, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 268
    const-string v6, "_size"

    invoke-virtual {p3}, Ljava/io/File;->length()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 269
    const-string v6, "mime_type"

    invoke-virtual {v5, v6, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 270
    const-string v6, "flags"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 273
    invoke-virtual {p3}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    .line 274
    .local v2, "lastModified":J
    const-wide v6, 0x757b12c00L

    cmp-long v6, v2, v6

    if-lez v6, :cond_2

    .line 275
    const-string v6, "last_modified"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 277
    :cond_2
    return-void

    .line 242
    .end local v0    # "displayName":Ljava/lang/String;
    .end local v1    # "flags":I
    .end local v2    # "lastModified":J
    .end local v4    # "mimeType":Ljava/lang/String;
    .end local v5    # "row":Landroid/database/MatrixCursor$RowBuilder;
    :cond_3
    invoke-direct {p0, p2}, Lcom/android/externalstorage/ExternalStorageProvider;->getFileForDocId(Ljava/lang/String;)Ljava/io/File;

    move-result-object p3

    goto :goto_0

    .line 253
    .restart local v1    # "flags":I
    :cond_4
    or-int/lit8 v1, v1, 0x2

    .line 254
    or-int/lit8 v1, v1, 0x4

    .line 255
    or-int/lit8 v1, v1, 0x40

    goto :goto_1
.end method

.method private static resolveDocumentProjection([Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0, "projection"    # [Ljava/lang/String;

    .prologue
    .line 180
    if-eqz p0, :cond_0

    .end local p0    # "projection":[Ljava/lang/String;
    :goto_0
    return-object p0

    .restart local p0    # "projection":[Ljava/lang/String;
    :cond_0
    sget-object p0, Lcom/android/externalstorage/ExternalStorageProvider;->DEFAULT_DOCUMENT_PROJECTION:[Ljava/lang/String;

    goto :goto_0
.end method

.method private static resolveRootProjection([Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0, "projection"    # [Ljava/lang/String;

    .prologue
    .line 176
    if-eqz p0, :cond_0

    .end local p0    # "projection":[Ljava/lang/String;
    :goto_0
    return-object p0

    .restart local p0    # "projection":[Ljava/lang/String;
    :cond_0
    sget-object p0, Lcom/android/externalstorage/ExternalStorageProvider;->DEFAULT_ROOT_PROJECTION:[Ljava/lang/String;

    goto :goto_0
.end method

.method private startObserving(Ljava/io/File;Landroid/net/Uri;)V
    .locals 3
    .param p1, "file"    # Ljava/io/File;
    .param p2, "notifyUri"    # Landroid/net/Uri;

    .prologue
    .line 542
    iget-object v2, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mObservers:Ljava/util/Map;

    monitor-enter v2

    .line 543
    :try_start_0
    iget-object v1, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mObservers:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/externalstorage/ExternalStorageProvider$DirectoryObserver;

    .line 544
    .local v0, "observer":Lcom/android/externalstorage/ExternalStorageProvider$DirectoryObserver;
    if-nez v0, :cond_0

    .line 545
    new-instance v0, Lcom/android/externalstorage/ExternalStorageProvider$DirectoryObserver;

    .end local v0    # "observer":Lcom/android/externalstorage/ExternalStorageProvider$DirectoryObserver;
    invoke-virtual {p0}, Lcom/android/externalstorage/ExternalStorageProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p1, v1, p2}, Lcom/android/externalstorage/ExternalStorageProvider$DirectoryObserver;-><init>(Ljava/io/File;Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 547
    .restart local v0    # "observer":Lcom/android/externalstorage/ExternalStorageProvider$DirectoryObserver;
    invoke-virtual {v0}, Lcom/android/externalstorage/ExternalStorageProvider$DirectoryObserver;->startWatching()V

    .line 548
    iget-object v1, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mObservers:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 550
    :cond_0
    # operator++ for: Lcom/android/externalstorage/ExternalStorageProvider$DirectoryObserver;->mRefCount:I
    invoke-static {v0}, Lcom/android/externalstorage/ExternalStorageProvider$DirectoryObserver;->access$108(Lcom/android/externalstorage/ExternalStorageProvider$DirectoryObserver;)I

    .line 553
    monitor-exit v2

    .line 554
    return-void

    .line 553
    .end local v0    # "observer":Lcom/android/externalstorage/ExternalStorageProvider$DirectoryObserver;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private stopObserving(Ljava/io/File;)V
    .locals 3
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 557
    iget-object v2, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mObservers:Ljava/util/Map;

    monitor-enter v2

    .line 558
    :try_start_0
    iget-object v1, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mObservers:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/externalstorage/ExternalStorageProvider$DirectoryObserver;

    .line 559
    .local v0, "observer":Lcom/android/externalstorage/ExternalStorageProvider$DirectoryObserver;
    if-nez v0, :cond_0

    monitor-exit v2

    .line 569
    :goto_0
    return-void

    .line 561
    :cond_0
    # operator-- for: Lcom/android/externalstorage/ExternalStorageProvider$DirectoryObserver;->mRefCount:I
    invoke-static {v0}, Lcom/android/externalstorage/ExternalStorageProvider$DirectoryObserver;->access$110(Lcom/android/externalstorage/ExternalStorageProvider$DirectoryObserver;)I

    .line 562
    # getter for: Lcom/android/externalstorage/ExternalStorageProvider$DirectoryObserver;->mRefCount:I
    invoke-static {v0}, Lcom/android/externalstorage/ExternalStorageProvider$DirectoryObserver;->access$100(Lcom/android/externalstorage/ExternalStorageProvider$DirectoryObserver;)I

    move-result v1

    if-nez v1, :cond_1

    .line 563
    iget-object v1, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mObservers:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 564
    invoke-virtual {v0}, Lcom/android/externalstorage/ExternalStorageProvider$DirectoryObserver;->stopWatching()V

    .line 568
    :cond_1
    monitor-exit v2

    goto :goto_0

    .end local v0    # "observer":Lcom/android/externalstorage/ExternalStorageProvider$DirectoryObserver;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private updateVolumesLocked()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    const/4 v10, 0x0

    .line 123
    iget-object v11, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mRoots:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->clear()V

    .line 124
    iget-object v11, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mIdToPath:Ljava/util/HashMap;

    invoke-virtual {v11}, Ljava/util/HashMap;->clear()V

    .line 125
    iget-object v11, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mIdToRoot:Ljava/util/HashMap;

    invoke-virtual {v11}, Ljava/util/HashMap;->clear()V

    .line 127
    iget-object v11, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v11}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v9

    .line 128
    .local v9, "volumes":[Landroid/os/storage/StorageVolume;
    move-object v0, v9

    .local v0, "arr$":[Landroid/os/storage/StorageVolume;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_7

    aget-object v8, v0, v2

    .line 129
    .local v8, "volume":Landroid/os/storage/StorageVolume;
    const-string v11, "mounted"

    invoke-virtual {v8}, Landroid/os/storage/StorageVolume;->getState()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    const-string v11, "mounted_ro"

    invoke-virtual {v8}, Landroid/os/storage/StorageVolume;->getState()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    :cond_0
    const/4 v4, 0x1

    .line 131
    .local v4, "mounted":Z
    :goto_1
    if-nez v4, :cond_2

    .line 128
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .end local v4    # "mounted":Z
    :cond_1
    move v4, v10

    .line 129
    goto :goto_1

    .line 134
    .restart local v4    # "mounted":Z
    :cond_2
    invoke-virtual {v8}, Landroid/os/storage/StorageVolume;->isPrimary()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-virtual {v8}, Landroid/os/storage/StorageVolume;->isEmulated()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 135
    const-string v7, "primary"

    .line 143
    .local v7, "rootId":Ljava/lang/String;
    :goto_3
    iget-object v11, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mIdToPath:Ljava/util/HashMap;

    invoke-virtual {v11, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 144
    const-string v11, "ExternalStorage"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Duplicate UUID "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "; skipping"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 136
    .end local v7    # "rootId":Ljava/lang/String;
    :cond_3
    invoke-virtual {v8}, Landroid/os/storage/StorageVolume;->getUuid()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_4

    .line 137
    invoke-virtual {v8}, Landroid/os/storage/StorageVolume;->getUuid()Ljava/lang/String;

    move-result-object v7

    .restart local v7    # "rootId":Ljava/lang/String;
    goto :goto_3

    .line 139
    .end local v7    # "rootId":Ljava/lang/String;
    :cond_4
    const-string v11, "ExternalStorage"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Missing UUID for "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v8}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "; skipping"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 149
    .restart local v7    # "rootId":Ljava/lang/String;
    :cond_5
    :try_start_0
    invoke-virtual {v8}, Landroid/os/storage/StorageVolume;->getPathFile()Ljava/io/File;

    move-result-object v5

    .line 150
    .local v5, "path":Ljava/io/File;
    iget-object v11, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mIdToPath:Ljava/util/HashMap;

    invoke-virtual {v11, v7, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    new-instance v6, Lcom/android/externalstorage/ExternalStorageProvider$RootInfo;

    const/4 v11, 0x0

    invoke-direct {v6, v11}, Lcom/android/externalstorage/ExternalStorageProvider$RootInfo;-><init>(Lcom/android/externalstorage/ExternalStorageProvider$1;)V

    .line 153
    .local v6, "root":Lcom/android/externalstorage/ExternalStorageProvider$RootInfo;
    iput-object v7, v6, Lcom/android/externalstorage/ExternalStorageProvider$RootInfo;->rootId:Ljava/lang/String;

    .line 154
    const v11, 0x2001b

    iput v11, v6, Lcom/android/externalstorage/ExternalStorageProvider$RootInfo;->flags:I

    .line 156
    const-string v11, "primary"

    invoke-virtual {v11, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 157
    invoke-virtual {p0}, Lcom/android/externalstorage/ExternalStorageProvider;->getContext()Landroid/content/Context;

    move-result-object v11

    const v12, 0x7f020001

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v6, Lcom/android/externalstorage/ExternalStorageProvider$RootInfo;->title:Ljava/lang/String;

    .line 161
    :goto_4
    invoke-direct {p0, v5}, Lcom/android/externalstorage/ExternalStorageProvider;->getDocIdForFile(Ljava/io/File;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v6, Lcom/android/externalstorage/ExternalStorageProvider$RootInfo;->docId:Ljava/lang/String;

    .line 162
    iget-object v11, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mRoots:Ljava/util/ArrayList;

    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    iget-object v11, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mIdToRoot:Ljava/util/HashMap;

    invoke-virtual {v11, v7, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    .line 164
    .end local v5    # "path":Ljava/io/File;
    .end local v6    # "root":Lcom/android/externalstorage/ExternalStorageProvider$RootInfo;
    :catch_0
    move-exception v1

    .line 165
    .local v1, "e":Ljava/io/FileNotFoundException;
    new-instance v10, Ljava/lang/IllegalStateException;

    invoke-direct {v10, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v10

    .line 159
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    .restart local v5    # "path":Ljava/io/File;
    .restart local v6    # "root":Lcom/android/externalstorage/ExternalStorageProvider$RootInfo;
    :cond_6
    :try_start_1
    invoke-virtual {v8}, Landroid/os/storage/StorageVolume;->getUserLabel()Ljava/lang/String;

    move-result-object v11

    iput-object v11, v6, Lcom/android/externalstorage/ExternalStorageProvider$RootInfo;->title:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    .line 169
    .end local v4    # "mounted":Z
    .end local v5    # "path":Ljava/io/File;
    .end local v6    # "root":Lcom/android/externalstorage/ExternalStorageProvider$RootInfo;
    .end local v7    # "rootId":Ljava/lang/String;
    .end local v8    # "volume":Landroid/os/storage/StorageVolume;
    :cond_7
    const-string v11, "ExternalStorage"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "After updating volumes, found "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mRoots:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " active roots"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    invoke-virtual {p0}, Lcom/android/externalstorage/ExternalStorageProvider;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string v12, "com.android.externalstorage.documents"

    invoke-static {v12}, Landroid/provider/DocumentsContract;->buildRootsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v11, v12, v14, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 173
    return-void
.end method


# virtual methods
.method public createDocument(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "displayName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 313
    invoke-static {p3}, Landroid/os/FileUtils;->buildValidFatFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 315
    invoke-direct {p0, p1}, Lcom/android/externalstorage/ExternalStorageProvider;->getFileForDocId(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 316
    .local v2, "parent":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_0

    .line 317
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Parent document isn\'t a directory"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 320
    :cond_0
    invoke-static {v2, p2, p3}, Lcom/android/externalstorage/ExternalStorageProvider;->buildUniqueFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 321
    .local v1, "file":Ljava/io/File;
    const-string v3, "vnd.android.document/directory"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 322
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    move-result v3

    if-nez v3, :cond_2

    .line 323
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to mkdir "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 327
    :cond_1
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    move-result v3

    if-nez v3, :cond_2

    .line 328
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to touch "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 330
    :catch_0
    move-exception v0

    .line 331
    .local v0, "e":Ljava/io/IOException;
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to touch "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 335
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    invoke-direct {p0, v1}, Lcom/android/externalstorage/ExternalStorageProvider;->getDocIdForFile(Ljava/io/File;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public deleteDocument(Ljava/lang/String;)V
    .locals 4
    .param p1, "docId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 424
    invoke-direct {p0, p1}, Lcom/android/externalstorage/ExternalStorageProvider;->getFileForDocId(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 425
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 426
    invoke-static {v0}, Landroid/os/FileUtils;->deleteContents(Ljava/io/File;)Z

    .line 428
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_1

    .line 429
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to delete "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 431
    :cond_1
    return-void
.end method

.method public getDocumentType(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "documentId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 482
    invoke-direct {p0, p1}, Lcom/android/externalstorage/ExternalStorageProvider;->getFileForDocId(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 483
    .local v0, "file":Ljava/io/File;
    invoke-static {v0}, Lcom/android/externalstorage/ExternalStorageProvider;->getTypeForFile(Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public isChildDocument(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1, "parentDocId"    # Ljava/lang/String;
    .param p2, "docId"    # Ljava/lang/String;

    .prologue
    .line 301
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/externalstorage/ExternalStorageProvider;->getFileForDocId(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v2

    .line 302
    .local v2, "parent":Ljava/io/File;
    invoke-direct {p0, p2}, Lcom/android/externalstorage/ExternalStorageProvider;->getFileForDocId(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v0

    .line 303
    .local v0, "doc":Ljava/io/File;
    invoke-static {v2, v0}, Landroid/os/FileUtils;->contains(Ljava/io/File;Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    return v3

    .line 304
    .end local v0    # "doc":Ljava/io/File;
    .end local v2    # "parent":Ljava/io/File;
    :catch_0
    move-exception v1

    .line 305
    .local v1, "e":Ljava/io/IOException;
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to determine if "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is child of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/android/externalstorage/ExternalStorageProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "storage"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    iput-object v0, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mStorageManager:Landroid/os/storage/StorageManager;

    .line 105
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mHandler:Landroid/os/Handler;

    .line 107
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mRoots:Ljava/util/ArrayList;

    .line 108
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mIdToRoot:Ljava/util/HashMap;

    .line 109
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mIdToPath:Ljava/util/HashMap;

    .line 111
    invoke-virtual {p0}, Lcom/android/externalstorage/ExternalStorageProvider;->updateVolumes()V

    .line 113
    const/4 v0, 0x1

    return v0
.end method

.method public openDocument(Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/os/ParcelFileDescriptor;
    .locals 6
    .param p1, "documentId"    # Ljava/lang/String;
    .param p2, "mode"    # Ljava/lang/String;
    .param p3, "signal"    # Landroid/os/CancellationSignal;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 490
    invoke-direct {p0, p1}, Lcom/android/externalstorage/ExternalStorageProvider;->getFileForDocId(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 491
    .local v1, "file":Ljava/io/File;
    invoke-static {p2}, Landroid/os/ParcelFileDescriptor;->parseMode(Ljava/lang/String;)I

    move-result v2

    .line 492
    .local v2, "pfdMode":I
    const/high16 v3, 0x10000000

    if-ne v2, v3, :cond_0

    .line 493
    invoke-static {v1, v2}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    .line 497
    :goto_0
    return-object v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mHandler:Landroid/os/Handler;

    new-instance v4, Lcom/android/externalstorage/ExternalStorageProvider$1;

    invoke-direct {v4, p0, v1}, Lcom/android/externalstorage/ExternalStorageProvider$1;-><init>(Lcom/android/externalstorage/ExternalStorageProvider;Ljava/io/File;)V

    invoke-static {v1, v2, v3, v4}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;ILandroid/os/Handler;Landroid/os/ParcelFileDescriptor$OnCloseListener;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    .line 506
    :catch_0
    move-exception v0

    .line 507
    .local v0, "e":Ljava/io/IOException;
    new-instance v3, Ljava/io/FileNotFoundException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to open for writing: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public openDocumentThumbnail(Ljava/lang/String;Landroid/graphics/Point;Landroid/os/CancellationSignal;)Landroid/content/res/AssetFileDescriptor;
    .locals 2
    .param p1, "documentId"    # Ljava/lang/String;
    .param p2, "sizeHint"    # Landroid/graphics/Point;
    .param p3, "signal"    # Landroid/os/CancellationSignal;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 516
    invoke-direct {p0, p1}, Lcom/android/externalstorage/ExternalStorageProvider;->getFileForDocId(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 517
    .local v0, "file":Ljava/io/File;
    invoke-static {v0}, Landroid/provider/DocumentsContract;->openImageThumbnail(Ljava/io/File;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v1

    return-object v1
.end method

.method public queryChildDocuments(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1, "parentDocumentId"    # Ljava/lang/String;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 445
    invoke-direct {p0, p1}, Lcom/android/externalstorage/ExternalStorageProvider;->getFileForDocId(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    .line 446
    .local v4, "parent":Ljava/io/File;
    new-instance v5, Lcom/android/externalstorage/ExternalStorageProvider$DirectoryCursor;

    invoke-static {p2}, Lcom/android/externalstorage/ExternalStorageProvider;->resolveDocumentProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, p0, v6, p1, v4}, Lcom/android/externalstorage/ExternalStorageProvider$DirectoryCursor;-><init>(Lcom/android/externalstorage/ExternalStorageProvider;[Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    .line 448
    .local v5, "result":Landroid/database/MatrixCursor;
    invoke-virtual {v4}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, "arr$":[Ljava/io/File;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 449
    .local v1, "file":Ljava/io/File;
    const/4 v6, 0x0

    invoke-direct {p0, v5, v6, v1}, Lcom/android/externalstorage/ExternalStorageProvider;->includeFile(Landroid/database/MatrixCursor;Ljava/lang/String;Ljava/io/File;)V

    .line 448
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 451
    .end local v1    # "file":Ljava/io/File;
    :cond_0
    return-object v5
.end method

.method public queryDocument(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 2
    .param p1, "documentId"    # Ljava/lang/String;
    .param p2, "projection"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 436
    new-instance v0, Landroid/database/MatrixCursor;

    invoke-static {p2}, Lcom/android/externalstorage/ExternalStorageProvider;->resolveDocumentProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 437
    .local v0, "result":Landroid/database/MatrixCursor;
    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/android/externalstorage/ExternalStorageProvider;->includeFile(Landroid/database/MatrixCursor;Ljava/lang/String;Ljava/io/File;)V

    .line 438
    return-object v0
.end method

.method public queryRoots([Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1, "projection"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 281
    new-instance v2, Landroid/database/MatrixCursor;

    invoke-static {p1}, Lcom/android/externalstorage/ExternalStorageProvider;->resolveRootProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 282
    .local v2, "result":Landroid/database/MatrixCursor;
    iget-object v7, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mRootsLock:Ljava/lang/Object;

    monitor-enter v7

    .line 283
    :try_start_0
    iget-object v6, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mIdToPath:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 284
    .local v4, "rootId":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mIdToRoot:Ljava/util/HashMap;

    invoke-virtual {v6, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/externalstorage/ExternalStorageProvider$RootInfo;

    .line 285
    .local v3, "root":Lcom/android/externalstorage/ExternalStorageProvider$RootInfo;
    iget-object v6, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mIdToPath:Ljava/util/HashMap;

    invoke-virtual {v6, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    .line 287
    .local v1, "path":Ljava/io/File;
    invoke-virtual {v2}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    .line 288
    .local v5, "row":Landroid/database/MatrixCursor$RowBuilder;
    const-string v6, "root_id"

    iget-object v8, v3, Lcom/android/externalstorage/ExternalStorageProvider$RootInfo;->rootId:Ljava/lang/String;

    invoke-virtual {v5, v6, v8}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 289
    const-string v6, "flags"

    iget v8, v3, Lcom/android/externalstorage/ExternalStorageProvider$RootInfo;->flags:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v6, v8}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 290
    const-string v6, "title"

    iget-object v8, v3, Lcom/android/externalstorage/ExternalStorageProvider$RootInfo;->title:Ljava/lang/String;

    invoke-virtual {v5, v6, v8}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 291
    const-string v6, "document_id"

    iget-object v8, v3, Lcom/android/externalstorage/ExternalStorageProvider$RootInfo;->docId:Ljava/lang/String;

    invoke-virtual {v5, v6, v8}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 292
    const-string v6, "available_bytes"

    invoke-virtual {v1}, Ljava/io/File;->getFreeSpace()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v5, v6, v8}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_0

    .line 294
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "path":Ljava/io/File;
    .end local v3    # "root":Lcom/android/externalstorage/ExternalStorageProvider$RootInfo;
    .end local v4    # "rootId":Ljava/lang/String;
    .end local v5    # "row":Landroid/database/MatrixCursor$RowBuilder;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 295
    return-object v2
.end method

.method public querySearchDocuments(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1, "rootId"    # Ljava/lang/String;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "projection"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 457
    new-instance v7, Landroid/database/MatrixCursor;

    invoke-static {p3}, Lcom/android/externalstorage/ExternalStorageProvider;->resolveDocumentProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 460
    .local v7, "result":Landroid/database/MatrixCursor;
    iget-object v9, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mRootsLock:Ljava/lang/Object;

    monitor-enter v9

    .line 461
    :try_start_0
    iget-object v8, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mIdToPath:Ljava/util/HashMap;

    invoke-virtual {v8, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    .line 462
    .local v5, "parent":Ljava/io/File;
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 464
    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    .line 465
    .local v6, "pending":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/io/File;>;"
    invoke-virtual {v6, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 466
    :cond_0
    :goto_0
    invoke-virtual {v6}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_2

    invoke-virtual {v7}, Landroid/database/MatrixCursor;->getCount()I

    move-result v8

    const/16 v9, 0x18

    if-ge v8, v9, :cond_2

    .line 467
    invoke-virtual {v6}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 468
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 469
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 470
    .local v1, "child":Ljava/io/File;
    invoke-virtual {v6, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 469
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 462
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "child":Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "parent":Ljava/io/File;
    .end local v6    # "pending":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/io/File;>;"
    :catchall_0
    move-exception v8

    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v8

    .line 473
    .restart local v2    # "file":Ljava/io/File;
    .restart local v5    # "parent":Ljava/io/File;
    .restart local v6    # "pending":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/io/File;>;"
    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 474
    const/4 v8, 0x0

    invoke-direct {p0, v7, v8, v2}, Lcom/android/externalstorage/ExternalStorageProvider;->includeFile(Landroid/database/MatrixCursor;Ljava/lang/String;Ljava/io/File;)V

    goto :goto_0

    .line 477
    .end local v2    # "file":Ljava/io/File;
    :cond_2
    return-object v7
.end method

.method public renameDocument(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "displayName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 404
    invoke-static {p2}, Landroid/os/FileUtils;->buildValidFatFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 406
    invoke-direct {p0, p1}, Lcom/android/externalstorage/ExternalStorageProvider;->getFileForDocId(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 407
    .local v2, "before":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    invoke-direct {v0, v3, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 408
    .local v0, "after":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 409
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Already exists "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 411
    :cond_0
    invoke-virtual {v2, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 412
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to rename to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 414
    :cond_1
    invoke-direct {p0, v0}, Lcom/android/externalstorage/ExternalStorageProvider;->getDocIdForFile(Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    .line 415
    .local v1, "afterDocId":Ljava/lang/String;
    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 418
    .end local v1    # "afterDocId":Ljava/lang/String;
    :goto_0
    return-object v1

    .restart local v1    # "afterDocId":Ljava/lang/String;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public updateVolumes()V
    .locals 2

    .prologue
    .line 117
    iget-object v1, p0, Lcom/android/externalstorage/ExternalStorageProvider;->mRootsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 118
    :try_start_0
    invoke-direct {p0}, Lcom/android/externalstorage/ExternalStorageProvider;->updateVolumesLocked()V

    .line 119
    monitor-exit v1

    .line 120
    return-void

    .line 119
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
