.class final Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;
.super Landroid/os/Handler;
.source "PhoneErrService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/app/RilErrorNotifier/PhoneErrService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;


# direct methods
.method public constructor <init>(Lcom/sec/app/RilErrorNotifier/PhoneErrService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 423
    iput-object p1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    .line 424
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 425
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v8, 0x1

    const/16 v7, 0xa

    .line 430
    const-string v4, "PhoneErrService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Handling incoming message: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    iget v3, p1, Landroid/os/Message;->arg1:I

    .line 433
    .local v3, "serviceId":I
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/content/Intent;

    .line 434
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 436
    .local v0, "action":Ljava/lang/String;
    const-string v4, "android.intent.action.SMS_FDN_RESTRICTED"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 437
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    # invokes: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->handleSmsError(I)V
    invoke-static {v4, v8}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$400(Lcom/sec/app/RilErrorNotifier/PhoneErrService;I)V

    .line 522
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    invoke-static {v4, v3}, Lcom/sec/app/RilErrorNotifier/PhoneErrorReceiver;->finishStartingService(Landroid/app/Service;I)V

    .line 523
    :cond_1
    :goto_1
    return-void

    .line 438
    :cond_2
    const-string v4, "android.intent.action.SIM_NOT_INSERTED"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 439
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const/4 v5, 0x2

    # invokes: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->handleSmsError(I)V
    invoke-static {v4, v5}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$400(Lcom/sec/app/RilErrorNotifier/PhoneErrService;I)V

    goto :goto_0

    .line 440
    :cond_3
    const-string v4, "android.intent.action.SDCARD_COPY_ERROR"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 441
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const/4 v5, 0x3

    # invokes: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->handleSmsError(I)V
    invoke-static {v4, v5}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$400(Lcom/sec/app/RilErrorNotifier/PhoneErrService;I)V

    goto :goto_0

    .line 442
    :cond_4
    const-string v4, "android.intent.action.DATA_COPY_ERROR"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 443
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const/4 v5, 0x4

    # invokes: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->handleSmsError(I)V
    invoke-static {v4, v5}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$400(Lcom/sec/app/RilErrorNotifier/PhoneErrService;I)V

    goto :goto_0

    .line 444
    :cond_5
    const-string v4, "android.intent.action.SIM_INIT_CRASH"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 445
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const/4 v5, 0x5

    # invokes: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->handleSmsError(I)V
    invoke-static {v4, v5}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$400(Lcom/sec/app/RilErrorNotifier/PhoneErrService;I)V

    .line 446
    const-string v4, "4"

    const-string v5, "ril.pin_mode"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_1

    .line 449
    :cond_6
    const-string v4, "android.intent.action.RILD_RESET_ACTION"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 450
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "reason"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "!!!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    # setter for: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mTitle:Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$002(Lcom/sec/app/RilErrorNotifier/PhoneErrService;Ljava/lang/String;)Ljava/lang/String;

    .line 451
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "reason"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "Please get ril log /data/log/dumpstate_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "reason"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_date.log"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    # setter for: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mString:Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$102(Lcom/sec/app/RilErrorNotifier/PhoneErrService;Ljava/lang/String;)Ljava/lang/String;

    .line 452
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const/4 v5, 0x6

    # invokes: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->handleSmsError(I)V
    invoke-static {v4, v5}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$400(Lcom/sec/app/RilErrorNotifier/PhoneErrService;I)V

    goto/16 :goto_0

    .line 455
    :cond_7
    const-string v4, "android.intent.action.PHONEAPP_RESET_ACTION"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 456
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const/4 v5, 0x7

    # invokes: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->handleSmsError(I)V
    invoke-static {v4, v5}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$400(Lcom/sec/app/RilErrorNotifier/PhoneErrService;I)V

    goto/16 :goto_0

    .line 457
    :cond_8
    const-string v4, "android.intent.action.RIL_TIMEOUT_ACTION"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 458
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const/16 v5, 0x8

    # invokes: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->handleSmsError(I)V
    invoke-static {v4, v5}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$400(Lcom/sec/app/RilErrorNotifier/PhoneErrService;I)V

    goto/16 :goto_0

    .line 459
    :cond_9
    const-string v4, "android.intent.action.NO_SIM_NOTY"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 460
    const-string v4, "sys.deviceOffReq"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 462
    .local v2, "isSuttingDown":Ljava/lang/String;
    const-string v4, "4"

    const-string v5, "ril.pin_mode"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 467
    const-string v4, "1"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 468
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const/16 v5, 0x9

    # invokes: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->handleSmsError(I)V
    invoke-static {v4, v5}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$400(Lcom/sec/app/RilErrorNotifier/PhoneErrService;I)V

    goto/16 :goto_0

    .line 469
    .end local v2    # "isSuttingDown":Ljava/lang/String;
    :cond_a
    const-string v4, "android.intent.action.DATA_ROUTER_DISPLAY"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 470
    const-string v4, "PhoneErrService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DATA_ROUTER_DISPLAY_ACTION 1: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "msg1"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    const-string v4, "PhoneErrService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DATA_ROUTER_DISPLAY_ACTION 2: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "msg2"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    const-string v4, "PhoneErrService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DATA_ROUTER_DISPLAY_ACTION 3: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "index"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const-string v5, "index"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    # setter for: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mTitle:Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$002(Lcom/sec/app/RilErrorNotifier/PhoneErrService;Ljava/lang/String;)Ljava/lang/String;

    .line 475
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "msg1"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "msg2"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    # setter for: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mString:Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$102(Lcom/sec/app/RilErrorNotifier/PhoneErrService;Ljava/lang/String;)Ljava/lang/String;

    .line 477
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    # invokes: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->handleSmsError(I)V
    invoke-static {v4, v7}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$400(Lcom/sec/app/RilErrorNotifier/PhoneErrService;I)V

    goto/16 :goto_0

    .line 478
    :cond_b
    const-string v4, "android.intent.action.RIL_PERM_BLOCKED"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 479
    # getter for: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->SIM_BLOCKED_INTENT_RECEIVED:Z
    invoke-static {}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$500()Z

    move-result v4

    if-nez v4, :cond_0

    .line 480
    # setter for: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->SIM_BLOCKED_INTENT_RECEIVED:Z
    invoke-static {v8}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$502(Z)Z

    .line 481
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    iget-object v4, v4, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mToastHandler:Landroid/os/Handler;

    const/16 v5, 0x63

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 483
    :cond_c
    const-string v4, "android.intent.action.RIL_PERSO_BLOCKED"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 484
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    iget-object v4, v4, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mToastHandler:Landroid/os/Handler;

    const/16 v5, 0x62

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 485
    :cond_d
    const-string v4, "android.intent.action.MODEM_RFS_ERROR"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 486
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const/16 v5, 0xb

    # invokes: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->handleSmsError(I)V
    invoke-static {v4, v5}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$400(Lcom/sec/app/RilErrorNotifier/PhoneErrService;I)V

    goto/16 :goto_0

    .line 489
    :cond_e
    const-string v4, "android.intent.action.REFRESH_SIM_RESET"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 490
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const/16 v5, 0x6e

    # invokes: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->handleSmsError(I)V
    invoke-static {v4, v5}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$400(Lcom/sec/app/RilErrorNotifier/PhoneErrService;I)V

    goto/16 :goto_0

    .line 492
    :cond_f
    const-string v4, "android.intent.action.REFRESHRESET"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_10

    const-string v4, "android.intent.action.REFRESH_RESET_FAIL"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 494
    :cond_10
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const/16 v5, 0x6f

    # invokes: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->handleSmsError(I)V
    invoke-static {v4, v5}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$400(Lcom/sec/app/RilErrorNotifier/PhoneErrService;I)V

    goto/16 :goto_0

    .line 497
    :cond_11
    const-string v4, "android.intent.action.BOOT_NOT_KDDI_CARD_NOTY"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 498
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const/16 v5, 0x78

    # invokes: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->handleSmsError(I)V
    invoke-static {v4, v5}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$400(Lcom/sec/app/RilErrorNotifier/PhoneErrService;I)V

    goto/16 :goto_0

    .line 501
    :cond_12
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.TIME_SET"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 509
    const-string v4, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 510
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    # invokes: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->shutdownMDMModem(Landroid/os/Message;)V
    invoke-static {v4, p1}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$600(Lcom/sec/app/RilErrorNotifier/PhoneErrService;Landroid/os/Message;)V

    goto/16 :goto_1

    .line 512
    :cond_13
    const-string v4, "android.intent.action.IMEI_CERT_FAIL"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 513
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const-string v5, "needrestart"

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    # setter for: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mNeedRestartAfterWarning:Z
    invoke-static {v4, v5}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$702(Lcom/sec/app/RilErrorNotifier/PhoneErrService;Z)Z

    .line 514
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    iget-object v4, v4, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mToastHandler:Landroid/os/Handler;

    const/16 v5, 0x72

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 515
    :cond_14
    const-string v4, "android.intent.action.SECURE_LOCK_REBOOT"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 516
    iget-object v4, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    iget-object v4, v4, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mToastHandler:Landroid/os/Handler;

    const/16 v5, 0xc9

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_1
.end method
