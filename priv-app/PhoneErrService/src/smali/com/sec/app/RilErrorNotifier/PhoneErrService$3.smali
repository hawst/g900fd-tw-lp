.class Lcom/sec/app/RilErrorNotifier/PhoneErrService$3;
.super Ljava/lang/Object;
.source "PhoneErrService.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/app/RilErrorNotifier/PhoneErrService;->showAlertImeiCertPanel(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;


# direct methods
.method constructor <init>(Lcom/sec/app/RilErrorNotifier/PhoneErrService;)V
    .locals 0

    .prologue
    .line 651
    iput-object p1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$3;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 654
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 655
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$3;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    # getter for: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mNeedRestartAfterWarning:Z
    invoke-static {v0}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$700(Lcom/sec/app/RilErrorNotifier/PhoneErrService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 656
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$3;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const-wide/16 v2, 0x3e8

    # invokes: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->systemRebootOnNoSimNoty(J)V
    invoke-static {v0, v2, v3}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$300(Lcom/sec/app/RilErrorNotifier/PhoneErrService;J)V

    .line 659
    :cond_0
    return-void
.end method
