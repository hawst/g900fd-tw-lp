.class public Lcom/sec/app/RilErrorNotifier/PhoneErrService;
.super Landroid/app/Service;
.source "PhoneErrService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/app/RilErrorNotifier/PhoneErrService$RebootThread;,
        Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;,
        Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;
    }
.end annotation


# static fields
.field private static SIM_BLOCKED_INTENT_RECEIVED:Z


# instance fields
.field private alertOnceDialog:Landroid/app/AlertDialog;

.field private mContext:Landroid/content/Context;

.field private mKeyguardManager:Landroid/app/KeyguardManager;

.field private mNeedRestartAfterWarning:Z

.field private mResultCode:I

.field private mSecPhoneServiceConnection:Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;

.field private mServiceHandler:Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;

.field private mServiceLooper:Landroid/os/Looper;

.field private mString:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;

.field public mToastHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->SIM_BLOCKED_INTENT_RECEIVED:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 68
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 70
    iput-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mContext:Landroid/content/Context;

    .line 79
    iput-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->alertOnceDialog:Landroid/app/AlertDialog;

    .line 156
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mNeedRestartAfterWarning:Z

    .line 170
    new-instance v0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;

    invoke-direct {v0, p0}, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;-><init>(Lcom/sec/app/RilErrorNotifier/PhoneErrService;)V

    iput-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mToastHandler:Landroid/os/Handler;

    .line 712
    return-void
.end method

.method static synthetic access$000(Lcom/sec/app/RilErrorNotifier/PhoneErrService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/app/RilErrorNotifier/PhoneErrService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/app/RilErrorNotifier/PhoneErrService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mTitle:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/app/RilErrorNotifier/PhoneErrService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/app/RilErrorNotifier/PhoneErrService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/app/RilErrorNotifier/PhoneErrService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mString:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/app/RilErrorNotifier/PhoneErrService;J)V
    .locals 1
    .param p0, "x0"    # Lcom/sec/app/RilErrorNotifier/PhoneErrService;
    .param p1, "x1"    # J

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->systemRebootOnNoSimNoty(J)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/app/RilErrorNotifier/PhoneErrService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/app/RilErrorNotifier/PhoneErrService;
    .param p1, "x1"    # I

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->handleSmsError(I)V

    return-void
.end method

.method static synthetic access$500()Z
    .locals 1

    .prologue
    .line 68
    sget-boolean v0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->SIM_BLOCKED_INTENT_RECEIVED:Z

    return v0
.end method

.method static synthetic access$502(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 68
    sput-boolean p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->SIM_BLOCKED_INTENT_RECEIVED:Z

    return p0
.end method

.method static synthetic access$600(Lcom/sec/app/RilErrorNotifier/PhoneErrService;Landroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/app/RilErrorNotifier/PhoneErrService;
    .param p1, "x1"    # Landroid/os/Message;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->shutdownMDMModem(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/app/RilErrorNotifier/PhoneErrService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mNeedRestartAfterWarning:Z

    return v0
.end method

.method static synthetic access$702(Lcom/sec/app/RilErrorNotifier/PhoneErrService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/app/RilErrorNotifier/PhoneErrService;
    .param p1, "x1"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mNeedRestartAfterWarning:Z

    return p1
.end method

.method private handleShutdown(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 784
    const/4 v4, 0x0

    .line 785
    .local v4, "shouldExit":Z
    const/4 v3, 0x0

    .line 787
    .local v3, "rspMsg":Landroid/os/Message;
    iget v5, p1, Landroid/os/Message;->what:I

    sparse-switch v5, :sswitch_data_0

    .line 837
    :cond_0
    :goto_0
    if-eqz v4, :cond_1

    .line 838
    iget-object v5, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mSecPhoneServiceConnection:Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;

    invoke-virtual {v5}, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->disconnect()V

    .line 842
    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-static {p0, v5}, Lcom/sec/app/RilErrorNotifier/PhoneErrorReceiver;->finishStartingService(Landroid/app/Service;I)V

    .line 844
    :cond_1
    return-void

    .line 791
    :sswitch_0
    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v3

    .line 792
    const/16 v5, 0x70

    iput v5, v3, Landroid/os/Message;->what:I

    .line 794
    iget-object v5, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mSecPhoneServiceConnection:Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;

    invoke-virtual {v5, v3}, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->connect(Landroid/os/Message;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 795
    const/4 v4, 0x1

    goto :goto_0

    .line 802
    :sswitch_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 803
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 806
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/16 v5, 0x11

    :try_start_0
    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 807
    const/16 v5, 0x60

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 808
    const/4 v5, 0x4

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 810
    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v3

    .line 811
    const/16 v5, 0x71

    iput v5, v3, Landroid/os/Message;->what:I

    .line 813
    iget-object v5, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mSecPhoneServiceConnection:Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-virtual {v5, v6, v3}, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->invokeOemRilRequestRaw([BLandroid/os/Message;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 818
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 819
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 821
    :catch_0
    move-exception v2

    .line 822
    .local v2, "e":Ljava/io/IOException;
    const-string v5, "PhoneErrService"

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 814
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 815
    .restart local v2    # "e":Ljava/io/IOException;
    const/4 v4, 0x1

    .line 818
    :try_start_2
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 819
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 821
    :catch_2
    move-exception v2

    .line 822
    const-string v5, "PhoneErrService"

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 817
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 818
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 819
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 823
    :goto_1
    throw v5

    .line 821
    :catch_3
    move-exception v2

    .line 822
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v6, "PhoneErrService"

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 830
    .end local v0    # "bos":Ljava/io/ByteArrayOutputStream;
    .end local v1    # "dos":Ljava/io/DataOutputStream;
    .end local v2    # "e":Ljava/io/IOException;
    :sswitch_2
    const/4 v4, 0x1

    .line 831
    goto/16 :goto_0

    .line 787
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x70 -> :sswitch_1
        0x71 -> :sswitch_2
    .end sparse-switch
.end method

.method private handleSmsError(I)V
    .locals 2
    .param p1, "errType"    # I

    .prologue
    .line 528
    sparse-switch p1, :sswitch_data_0

    .line 580
    :goto_0
    return-void

    .line 530
    :sswitch_0
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mToastHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 533
    :sswitch_1
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mToastHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 536
    :sswitch_2
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mToastHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 539
    :sswitch_3
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mToastHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 542
    :sswitch_4
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mToastHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 545
    :sswitch_5
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mToastHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 548
    :sswitch_6
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mToastHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 551
    :sswitch_7
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mToastHandler:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 554
    :sswitch_8
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mToastHandler:Landroid/os/Handler;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 557
    :sswitch_9
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mToastHandler:Landroid/os/Handler;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 560
    :sswitch_a
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mToastHandler:Landroid/os/Handler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 564
    :sswitch_b
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mToastHandler:Landroid/os/Handler;

    const/16 v1, 0x6e

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 568
    :sswitch_c
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mToastHandler:Landroid/os/Handler;

    const/16 v1, 0x6f

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 573
    :sswitch_d
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mToastHandler:Landroid/os/Handler;

    const/16 v1, 0x78

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 528
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_5
        0x7 -> :sswitch_6
        0x8 -> :sswitch_7
        0x9 -> :sswitch_8
        0xa -> :sswitch_9
        0xb -> :sswitch_a
        0x6e -> :sswitch_b
        0x6f -> :sswitch_c
        0x78 -> :sswitch_d
    .end sparse-switch
.end method

.method static showAlertPanel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 583
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 590
    .local v0, "alertDialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x7d9

    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    .line 592
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 595
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/high16 v2, 0x200000

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 598
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 599
    return-void
.end method

.method private shutdownMDMModem(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v0, 0x0

    .line 694
    invoke-virtual {p0}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "airplane_mode_on"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 697
    .local v0, "isAirplaneMode":Z
    :cond_0
    if-eqz v0, :cond_2

    .line 698
    const-string v1, "mdm"

    const-string v2, "ro.baseband"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "mdm2"

    const-string v2, "ro.baseband"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "apq"

    const-string v2, "ro.baseband"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 702
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->handleShutdown(Landroid/os/Message;)V

    .line 705
    :cond_2
    return-void
.end method

.method private systemRebootOnNoSimNoty(J)V
    .locals 3
    .param p1, "delayMillis"    # J

    .prologue
    .line 709
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mToastHandler:Landroid/os/Handler;

    const/16 v1, 0xc8

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 710
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 419
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 288
    const-string v1, "PhoneErrService"

    const-string v2, "Creating SmsReceiverService"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "PhoneErrService"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 294
    .local v0, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 296
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mServiceLooper:Landroid/os/Looper;

    .line 297
    new-instance v1, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;

    iget-object v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mServiceLooper:Landroid/os/Looper;

    invoke-direct {v1, p0, v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;-><init>(Lcom/sec/app/RilErrorNotifier/PhoneErrService;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mServiceHandler:Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;

    .line 298
    new-instance v1, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;

    iget-object v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mServiceHandler:Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;

    invoke-direct {v1, p0, v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;-><init>(Lcom/sec/app/RilErrorNotifier/PhoneErrService;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mSecPhoneServiceConnection:Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;

    .line 300
    const-string v1, "keyguard"

    invoke-virtual {p0, v1}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/KeyguardManager;

    iput-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mKeyguardManager:Landroid/app/KeyguardManager;

    .line 301
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 413
    const-string v0, "PhoneErrService"

    const-string v1, "Destroying PhoneErrorReceiverService"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mServiceLooper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 415
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 396
    if-eqz p1, :cond_0

    .line 397
    const-string v1, "PhoneErrService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Starting #"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    const-string v1, "result"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mResultCode:I

    .line 401
    const-string v1, "PhoneErrService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mResultCode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mResultCode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mServiceHandler:Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;

    invoke-virtual {v1}, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 404
    .local v0, "msg":Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 405
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 406
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mServiceHandler:Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;

    invoke-virtual {v1, v0}, Lcom/sec/app/RilErrorNotifier/PhoneErrService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    .line 408
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method showAlertImeiCertPanel(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 650
    const/4 v1, 0x0

    .line 651
    .local v1, "listener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v1, Lcom/sec/app/RilErrorNotifier/PhoneErrService$3;

    .end local v1    # "listener":Landroid/content/DialogInterface$OnClickListener;
    invoke-direct {v1, p0}, Lcom/sec/app/RilErrorNotifier/PhoneErrService$3;-><init>(Lcom/sec/app/RilErrorNotifier/PhoneErrService;)V

    .line 662
    .restart local v1    # "listener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v3, "Security Error"

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string v3, "This phone has been flashed with unauthorized software & is locked.\nCall your mobile operator for additional support.\nPlease note that repair/return for this issue may have additional cost."

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string v3, "Reboot"

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 669
    .local v0, "alertDialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x7d9

    invoke-virtual {v2, v3}, Landroid/view/Window;->setType(I)V

    .line 671
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 673
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/high16 v3, 0x200000

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 676
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 677
    return-void
.end method

.method showAlertRebootPanel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "text"    # Ljava/lang/String;

    .prologue
    .line 626
    const/4 v0, 0x0

    .line 627
    .local v0, "listener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$2;

    .end local v0    # "listener":Landroid/content/DialogInterface$OnClickListener;
    invoke-direct {v0, p0}, Lcom/sec/app/RilErrorNotifier/PhoneErrService$2;-><init>(Lcom/sec/app/RilErrorNotifier/PhoneErrService;)V

    .line 636
    .restart local v0    # "listener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, p2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, p3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x104000a

    invoke-virtual {v2, v3, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 643
    .local v1, "mIccSwapDialog":Landroid/app/AlertDialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x7d8

    invoke-virtual {v2, v3}, Landroid/view/Window;->setType(I)V

    .line 644
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 645
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/high16 v3, 0x200000

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 646
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 647
    return-void
.end method
