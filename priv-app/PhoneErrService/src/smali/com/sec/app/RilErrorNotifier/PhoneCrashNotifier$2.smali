.class Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$2;
.super Landroid/os/Handler;
.source "PhoneCrashNotifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;


# direct methods
.method constructor <init>(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$2;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 119
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 145
    :goto_0
    return-void

    .line 122
    :sswitch_0
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$2;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    const/16 v2, 0x3e9

    invoke-virtual {v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->displayCrashInfo(I)V

    goto :goto_0

    .line 126
    :sswitch_1
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$2;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    const/16 v2, 0x3ea

    invoke-virtual {v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->displayCrashInfo(I)V

    goto :goto_0

    .line 130
    :sswitch_2
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$2;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    const/16 v2, 0x3eb

    invoke-virtual {v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->displayCrashInfo(I)V

    goto :goto_0

    .line 134
    :sswitch_3
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$2;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    const/16 v2, 0x3ec

    invoke-virtual {v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->displayCrashInfo(I)V

    goto :goto_0

    .line 139
    :sswitch_4
    :try_start_0
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$2;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    # getter for: Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->access$000(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 140
    :catch_0
    move-exception v0

    .line 141
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 119
    nop

    :sswitch_data_0
    .sparse-switch
        0x3e9 -> :sswitch_0
        0x3ea -> :sswitch_1
        0x3eb -> :sswitch_2
        0x3ec -> :sswitch_3
        0x44c -> :sswitch_4
    .end sparse-switch
.end method
