.class Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;
.super Landroid/os/Handler;
.source "PhoneErrService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/app/RilErrorNotifier/PhoneErrService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;


# direct methods
.method constructor <init>(Lcom/sec/app/RilErrorNotifier/PhoneErrService;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v6, 0xbb8

    const v4, 0x7f050020

    const/4 v3, 0x1

    .line 174
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 279
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const v2, 0x7f05000d

    invoke-virtual {v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 283
    :goto_0
    :sswitch_0
    return-void

    .line 191
    :sswitch_1
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const v2, 0x7f050005

    invoke-virtual {v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 196
    :sswitch_2
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const v2, 0x7f050006

    invoke-virtual {v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 201
    :sswitch_3
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const v2, 0x7f050007

    invoke-virtual {v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 206
    :sswitch_4
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const v2, 0x7f050008

    invoke-virtual {v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 210
    :sswitch_5
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    invoke-virtual {v0}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const v2, 0x7f050009

    invoke-virtual {v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const v3, 0x7f05000a

    invoke-virtual {v2, v3}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->showAlertPanel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 213
    :sswitch_6
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    invoke-virtual {v0}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    # getter for: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mTitle:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$000(Lcom/sec/app/RilErrorNotifier/PhoneErrService;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    # getter for: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mString:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$100(Lcom/sec/app/RilErrorNotifier/PhoneErrService;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->showAlertPanel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 217
    :sswitch_7
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    invoke-virtual {v0}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Phone app reset"

    const-string v2, "PhoneApp is reset.please get platform log /data/log/dumpstate_phoneappreset_date.log"

    invoke-static {v0, v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->showAlertPanel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 221
    :sswitch_8
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    invoke-virtual {v0}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "IPC communication error"

    const-string v2, "IPC communication error. copy the dumpstate logs to sdcard with keystring *#9900#."

    invoke-static {v0, v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->showAlertPanel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 225
    :sswitch_9
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    invoke-virtual {v0}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const v2, 0x7f05000f

    invoke-virtual {v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const v3, 0x7f05000e

    invoke-virtual {v2, v3}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->showAlertPanel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 232
    :sswitch_a
    new-instance v0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$RebootThread;

    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService$RebootThread;-><init>(Lcom/sec/app/RilErrorNotifier/PhoneErrService;Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;)V

    invoke-virtual {v0}, Lcom/sec/app/RilErrorNotifier/PhoneErrService$RebootThread;->start()V

    goto/16 :goto_0

    .line 236
    :sswitch_b
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    invoke-virtual {v0}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    # getter for: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mTitle:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$000(Lcom/sec/app/RilErrorNotifier/PhoneErrService;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    # getter for: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->mString:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$100(Lcom/sec/app/RilErrorNotifier/PhoneErrService;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->showAlertPanel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 240
    :sswitch_c
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    invoke-virtual {v0}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "MODEM RFS error"

    const-string v2, "RFS communication error. copy the CP logs to sdcard with keystring *#9900#."

    invoke-static {v0, v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->showAlertPanel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 245
    :sswitch_d
    const-string v0, "PhoneErrService"

    const-string v1, "System reboot..."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    # invokes: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->systemRebootOnNoSimNoty(J)V
    invoke-static {v0, v6, v7}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$300(Lcom/sec/app/RilErrorNotifier/PhoneErrService;J)V

    goto/16 :goto_0

    .line 251
    :sswitch_e
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    invoke-virtual {v0}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const v2, 0x7f05001a

    invoke-virtual {v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const v3, 0x7f050019

    invoke-virtual {v2, v3}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->showAlertPanel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    # invokes: Lcom/sec/app/RilErrorNotifier/PhoneErrService;->systemRebootOnNoSimNoty(J)V
    invoke-static {v0, v6, v7}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->access$300(Lcom/sec/app/RilErrorNotifier/PhoneErrService;J)V

    goto/16 :goto_0

    .line 258
    :sswitch_f
    const-string v0, "PhoneErrService"

    const-string v1, "This Card is not kddi card (Network Locked)..."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    invoke-virtual {v0}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const v2, 0x7f050017

    invoke-virtual {v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const v3, 0x7f050018

    invoke-virtual {v2, v3}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->showAlertPanel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 265
    :sswitch_10
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    invoke-virtual {v1}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->showAlertImeiCertPanel(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 271
    :sswitch_11
    const-string v0, "310260"

    const-string v1, "gsm.sim.operator.numeric"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    invoke-virtual {v0}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const v2, 0x7f05001f

    invoke-virtual {v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    invoke-virtual {v2, v4}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->showAlertPanel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 274
    :cond_0
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    invoke-virtual {v1}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    invoke-virtual {v2, v4}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    const v4, 0x7f050021

    invoke-virtual {v3, v4}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->showAlertRebootPanel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 174
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_b
        0xb -> :sswitch_c
        0x62 -> :sswitch_0
        0x63 -> :sswitch_0
        0x6e -> :sswitch_d
        0x6f -> :sswitch_e
        0x72 -> :sswitch_10
        0x78 -> :sswitch_f
        0xc8 -> :sswitch_a
        0xc9 -> :sswitch_11
    .end sparse-switch
.end method
