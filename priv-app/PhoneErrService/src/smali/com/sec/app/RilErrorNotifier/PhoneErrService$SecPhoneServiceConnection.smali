.class Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;
.super Ljava/lang/Object;
.source "PhoneErrService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/app/RilErrorNotifier/PhoneErrService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SecPhoneServiceConnection"
.end annotation


# instance fields
.field private canBind:Z

.field private mConnectionCnt:I

.field private mSecPhoneReqMessenger:Landroid/os/Messenger;

.field private mSecPhoneRspMessenger:Landroid/os/Messenger;

.field private msgToNotifyConnect:Landroid/os/Message;

.field final synthetic this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;


# direct methods
.method public constructor <init>(Lcom/sec/app/RilErrorNotifier/PhoneErrService;Landroid/os/Handler;)V
    .locals 1
    .param p2, "responseHandler"    # Landroid/os/Handler;

    .prologue
    const/4 v0, 0x0

    .line 314
    iput-object p1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 306
    iput-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->msgToNotifyConnect:Landroid/os/Message;

    .line 307
    iput-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->mSecPhoneRspMessenger:Landroid/os/Messenger;

    .line 308
    iput-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->mSecPhoneReqMessenger:Landroid/os/Messenger;

    .line 310
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->canBind:Z

    .line 312
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->mConnectionCnt:I

    .line 315
    new-instance v0, Landroid/os/Messenger;

    invoke-direct {v0, p2}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->mSecPhoneRspMessenger:Landroid/os/Messenger;

    .line 316
    return-void
.end method


# virtual methods
.method public connect(Landroid/os/Message;)Z
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 335
    const-string v3, "PhoneErrService_Conn"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Connecting To SecPhone service / connCnt "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->mConnectionCnt:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " / msg "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " / "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    iget-boolean v3, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->canBind:Z

    if-nez v3, :cond_0

    .line 338
    const-string v2, "PhoneErrService_Conn"

    const-string v3, "Cannot bind"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    :goto_0
    return v1

    .line 342
    :cond_0
    iget v3, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->mConnectionCnt:I

    if-gez v3, :cond_1

    .line 343
    iput v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->mConnectionCnt:I

    .line 345
    :cond_1
    iget v3, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->mConnectionCnt:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->mConnectionCnt:I

    .line 347
    iget v3, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->mConnectionCnt:I

    if-le v3, v2, :cond_3

    iget-object v3, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->mSecPhoneReqMessenger:Landroid/os/Messenger;

    if-eqz v3, :cond_3

    .line 348
    const-string v1, "PhoneErrService_Conn"

    const-string v3, "Already connected"

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    if-eqz p1, :cond_2

    .line 350
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    :cond_2
    move v1, v2

    .line 352
    goto :goto_0

    .line 355
    :cond_3
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 356
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "com.sec.phone"

    const-string v4, "com.sec.phone.SecPhoneService"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 358
    iput-object p1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->msgToNotifyConnect:Landroid/os/Message;

    .line 360
    iget-object v3, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    invoke-virtual {v3, v0, p0, v2}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->canBind:Z

    .line 361
    iget-boolean v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->canBind:Z

    if-nez v2, :cond_4

    .line 362
    iput v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->mConnectionCnt:I

    .line 363
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    invoke-virtual {v1, p0}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->unbindService(Landroid/content/ServiceConnection;)V

    .line 366
    :cond_4
    iget-boolean v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->canBind:Z

    goto :goto_0
.end method

.method public disconnect()V
    .locals 1

    .prologue
    .line 370
    iget v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->mConnectionCnt:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->mConnectionCnt:I

    .line 372
    iget v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->mConnectionCnt:I

    if-nez v0, :cond_0

    .line 373
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneErrService;

    invoke-virtual {v0, p0}, Lcom/sec/app/RilErrorNotifier/PhoneErrService;->unbindService(Landroid/content/ServiceConnection;)V

    .line 375
    :cond_0
    return-void
.end method

.method public invokeOemRilRequestRaw([BLandroid/os/Message;)V
    .locals 3
    .param p1, "data"    # [B
    .param p2, "response"    # Landroid/os/Message;

    .prologue
    .line 378
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 379
    .local v0, "req":Landroid/os/Bundle;
    const-string v1, "request"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 381
    invoke-virtual {p2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 382
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->mSecPhoneRspMessenger:Landroid/os/Messenger;

    iput-object v1, p2, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 385
    :try_start_0
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->mSecPhoneReqMessenger:Landroid/os/Messenger;

    if-eqz v1, :cond_0

    .line 386
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->mSecPhoneReqMessenger:Landroid/os/Messenger;

    invoke-virtual {v1, p2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V

    .line 391
    :goto_0
    return-void

    .line 388
    :cond_0
    const-string v1, "PhoneErrService_Conn"

    const-string v2, "mSecPhoneReqMessenger is null. Do nothing."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 389
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 319
    const-string v0, "PhoneErrService_Conn"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onServiceConnected() - msg "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->msgToNotifyConnect:Landroid/os/Message;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    new-instance v0, Landroid/os/Messenger;

    invoke-direct {v0, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    iput-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->mSecPhoneReqMessenger:Landroid/os/Messenger;

    .line 323
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->msgToNotifyConnect:Landroid/os/Message;

    if-eqz v0, :cond_0

    .line 324
    const-string v0, "PhoneErrService_Conn"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifying "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->msgToNotifyConnect:Landroid/os/Message;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->msgToNotifyConnect:Landroid/os/Message;

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 327
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 330
    const-string v0, "PhoneErrService_Conn"

    const-string v1, "onServiceDisconnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneErrService$SecPhoneServiceConnection;->mSecPhoneReqMessenger:Landroid/os/Messenger;

    .line 332
    return-void
.end method
