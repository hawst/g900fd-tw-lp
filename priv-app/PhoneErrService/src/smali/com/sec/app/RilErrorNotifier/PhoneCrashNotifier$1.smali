.class Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;
.super Landroid/content/BroadcastReceiver;
.source "PhoneCrashNotifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;


# direct methods
.method constructor <init>(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 73
    :try_start_0
    const-string v1, "CrashNotifier"

    const-string v2, "receive CP CRASH Information"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    iget-object v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    invoke-virtual {v2}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "title"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->title:Ljava/lang/String;

    .line 76
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    iget-object v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    invoke-virtual {v2}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "message"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->errorInfo:Ljava/lang/String;

    .line 78
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    # getter for: Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->access$000(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 80
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    # getter for: Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->access$000(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 81
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    # getter for: Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->access$100(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x44c

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 84
    :cond_0
    const-string v1, "cpcrash"

    iget-object v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    iget-object v2, v2, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 86
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    iget-object v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    # getter for: Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->access$100(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x3e9

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    # setter for: Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->msg:Landroid/os/Message;
    invoke-static {v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->access$202(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;Landroid/os/Message;)Landroid/os/Message;

    .line 87
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    # getter for: Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->msg:Landroid/os/Message;
    invoke-static {v1}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->access$200(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 111
    :cond_1
    :goto_0
    return-void

    .line 89
    :cond_2
    const-string v1, "hsiccrash"

    iget-object v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    iget-object v2, v2, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 91
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    iget-object v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    # getter for: Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->access$100(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x3ea

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    # setter for: Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->msg:Landroid/os/Message;
    invoke-static {v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->access$202(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;Landroid/os/Message;)Landroid/os/Message;

    .line 92
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    # getter for: Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->msg:Landroid/os/Message;
    invoke-static {v1}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->access$200(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 108
    :catch_0
    move-exception v0

    .line 109
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 94
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_1
    const-string v1, "cpcrashwatchdog"

    iget-object v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    iget-object v2, v2, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 96
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    iget-object v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    # getter for: Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->access$100(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x3eb

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    # setter for: Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->msg:Landroid/os/Message;
    invoke-static {v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->access$202(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;Landroid/os/Message;)Landroid/os/Message;

    .line 97
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    # getter for: Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->msg:Landroid/os/Message;
    invoke-static {v1}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->access$200(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 99
    :cond_4
    const-string v1, "trtbcpcrash"

    iget-object v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    iget-object v2, v2, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 101
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    iget-object v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    # getter for: Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->access$100(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x3ec

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    # setter for: Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->msg:Landroid/os/Message;
    invoke-static {v1, v2}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->access$202(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;Landroid/os/Message;)Landroid/os/Message;

    .line 102
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    # getter for: Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->msg:Landroid/os/Message;
    invoke-static {v1}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->access$200(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 104
    :cond_5
    const-string v1, "DeviceReset"

    iget-object v2, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    iget-object v2, v2, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->errorInfo:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 106
    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;->this$0:Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    invoke-virtual {v1}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->DeviceReset()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
