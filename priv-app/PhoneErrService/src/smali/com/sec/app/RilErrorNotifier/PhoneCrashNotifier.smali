.class public Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;
.super Landroid/app/Activity;
.source "PhoneCrashNotifier.java"


# static fields
.field private static mPowerManager:Landroid/os/PowerManager;


# instance fields
.field errorInfo:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private msg:Landroid/os/Message;

.field private progressDialog:Landroid/app/ProgressDialog;

.field title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->progressDialog:Landroid/app/ProgressDialog;

    .line 69
    new-instance v0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;

    invoke-direct {v0, p0}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$1;-><init>(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;)V

    iput-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 114
    new-instance v0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$2;

    invoke-direct {v0, p0}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$2;-><init>(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;)V

    iput-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->progressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;)Landroid/os/Message;
    .locals 1
    .param p0, "x0"    # Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->msg:Landroid/os/Message;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;Landroid/os/Message;)Landroid/os/Message;
    .locals 0
    .param p0, "x0"    # Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;
    .param p1, "x1"    # Landroid/os/Message;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->msg:Landroid/os/Message;

    return-object p1
.end method

.method static synthetic access$300()Landroid/os/PowerManager;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->mPowerManager:Landroid/os/PowerManager;

    return-object v0
.end method


# virtual methods
.method public DeviceReset()V
    .locals 3

    .prologue
    .line 256
    const-string v1, "eng"

    sget-object v2, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 257
    const-string v1, "persist.sys.shutdown"

    const-string v2, "PCNF"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$3;

    invoke-direct {v1, p0}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier$3;-><init>(Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 265
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 266
    return-void
.end method

.method public displayCrashInfo(I)V
    .locals 10
    .param p1, "crashType"    # I

    .prologue
    const/4 v8, 0x1

    .line 150
    const-string v2, ""

    .line 151
    .local v2, "displayTitle":Ljava/lang/String;
    const-string v1, "Doing Modem RAM Dump! \n Do not touch anything for 10 minutes. \n"

    .line 152
    .local v1, "displayMessage":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->msg:Landroid/os/Message;

    iget v5, v5, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    .line 172
    :goto_0
    new-instance v5, Landroid/app/Notification$Builder;

    invoke-virtual {p0}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const/high16 v6, 0x7f020000

    invoke-virtual {v5, v6}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->errorInfo:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 179
    .local v0, "builder":Landroid/app/Notification$Builder;
    new-instance v4, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v4}, Landroid/app/Notification$BigTextStyle;-><init>()V

    .line 180
    .local v4, "style":Landroid/app/Notification$BigTextStyle;
    invoke-virtual {v4, v2}, Landroid/app/Notification$BigTextStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    .line 181
    invoke-virtual {v4, v2}, Landroid/app/Notification$BigTextStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    .line 182
    iget-object v5, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->errorInfo:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    .line 184
    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    .line 186
    const-string v5, "notification"

    invoke-virtual {p0, v5}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    .line 187
    .local v3, "notificationManager":Landroid/app/NotificationManager;
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v5

    invoke-virtual {v3, v8, v5}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 189
    iget-object v5, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v5, v2}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 190
    iget-object v5, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->progressDialog:Landroid/app/ProgressDialog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->errorInfo:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v5, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v5, v8}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 192
    iget-object v5, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v5, v8}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 193
    iget-object v5, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/ProgressDialog;->show()V

    .line 195
    iget-object v5, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->mHandler:Landroid/os/Handler;

    const/16 v6, 0x44c

    const-wide/32 v8, 0x927c0

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 197
    iget-object v5, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v5

    if-nez v5, :cond_0

    .line 199
    iget-object v5, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 201
    :cond_0
    return-void

    .line 155
    .end local v0    # "builder":Landroid/app/Notification$Builder;
    .end local v3    # "notificationManager":Landroid/app/NotificationManager;
    .end local v4    # "style":Landroid/app/Notification$BigTextStyle;
    :pswitch_0
    const-string v2, "MODEM CRASH!!"

    .line 156
    goto/16 :goto_0

    .line 159
    :pswitch_1
    const-string v2, "HSIC CRASH!!"

    .line 160
    goto/16 :goto_0

    .line 163
    :pswitch_2
    const-string v2, "MODEM CRASH WATCHDOG!!"

    .line 164
    goto/16 :goto_0

    .line 167
    :pswitch_3
    const-string v2, "MODEM CRASH!!"

    .line 168
    const-string v1, "Modem RAM Dump Done! \n Crash reason :"

    goto/16 :goto_0

    .line 152
    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 207
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 209
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    sput-object v0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->mPowerManager:Landroid/os/PowerManager;

    .line 211
    sget-object v0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->mPowerManager:Landroid/os/PowerManager;

    const v1, 0x3000001a

    const-string v2, "Phone_crash"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 215
    invoke-virtual {p0}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->title:Ljava/lang/String;

    .line 216
    invoke-virtual {p0}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "message"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->errorInfo:Ljava/lang/String;

    .line 218
    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->setContentView(I)V

    .line 220
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->mIntentFilter:Landroid/content/IntentFilter;

    .line 221
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.CP_CRASH"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 222
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 224
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->progressDialog:Landroid/app/ProgressDialog;

    .line 226
    const-string v0, "cpcrash"

    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 228
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->msg:Landroid/os/Message;

    .line 229
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->msg:Landroid/os/Message;

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 251
    :cond_0
    :goto_0
    const-string v0, "CrashNotifier"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    return-void

    .line 231
    :cond_1
    const-string v0, "hsiccrash"

    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 233
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3ea

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->msg:Landroid/os/Message;

    .line 234
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->msg:Landroid/os/Message;

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 236
    :cond_2
    const-string v0, "cpcrashwatchdog"

    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 238
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3eb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->msg:Landroid/os/Message;

    .line 239
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->msg:Landroid/os/Message;

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 241
    :cond_3
    const-string v0, "trtbcpcrash"

    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 243
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3ec

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->msg:Landroid/os/Message;

    .line 244
    iget-object v0, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->msg:Landroid/os/Message;

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 246
    :cond_4
    const-string v0, "DeviceReset"

    iget-object v1, p0, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->errorInfo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    invoke-virtual {p0}, Lcom/sec/app/RilErrorNotifier/PhoneCrashNotifier;->DeviceReset()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 270
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 271
    return-void
.end method
