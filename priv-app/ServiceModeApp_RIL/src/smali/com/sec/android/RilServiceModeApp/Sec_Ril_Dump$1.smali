.class Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;
.super Landroid/os/Handler;
.source "Sec_Ril_Dump.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;


# direct methods
.method constructor <init>(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V
    .locals 0

    .prologue
    .line 255
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 260
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "error"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 263
    .local v0, "error":I
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 409
    :goto_0
    :pswitch_0
    return-void

    .line 265
    :pswitch_1
    const-string v1, "RilDump"

    const-string v2, "QUERY_DONE"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 270
    :pswitch_2
    if-nez v0, :cond_0

    .line 273
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->infoIPCDumpLog()V
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$000(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V

    .line 274
    const-string v1, "RilDump"

    const-string v2, "IPC_DUMP_DONE Success"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 276
    :cond_0
    const-string v1, "RilDump"

    const-string v2, "IPC_DUMP_DONE fail"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 283
    :pswitch_3
    if-nez v0, :cond_1

    .line 286
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->infoIPCDumpBin()V
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$100(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V

    .line 287
    const-string v1, "RilDump"

    const-string v2, "IPC_DUMP_DONE Success"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 289
    :cond_1
    const-string v1, "RilDump"

    const-string v2, "IPC_DUMP_DONE fail"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 296
    :pswitch_4
    if-nez v0, :cond_2

    .line 299
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->infoRilLog()V
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$200(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V

    .line 300
    const-string v1, "RilDump"

    const-string v2, "Ril dump Success"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 302
    :cond_2
    const-string v1, "RilDump"

    const-string v2, "Ril dump fail"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 307
    :pswitch_5
    const-string v1, "RilDump"

    const-string v2, "EVENT_TICK!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 313
    :pswitch_6
    if-nez v0, :cond_3

    .line 316
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const-string v2, "DPram_dump Success path :: /data/log/dpram_dump"

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->DisplayMessageDialog(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)V

    .line 317
    const-string v1, "RilDump"

    const-string v2, "DPram_dump Success"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 319
    :cond_3
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const-string v2, "DPram_dump fail"

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->DisplayMessageDialog(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)V

    .line 320
    const-string v1, "RilDump"

    const-string v2, "DPram_dump fail"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 327
    :pswitch_7
    if-nez v0, :cond_4

    .line 330
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const-string v2, "NV BACKUP Success"

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->DisplayMessageDialog(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)V

    .line 331
    const-string v1, "RilDump"

    const-string v2, "NV BACKUP Success"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 333
    :cond_4
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const-string v2, "NV BACKUP fail"

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->DisplayMessageDialog(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)V

    .line 334
    const-string v1, "RilDump"

    const-string v2, "NV BACKUP fail"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 341
    :pswitch_8
    if-nez v0, :cond_5

    .line 344
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const-string v2, "NV DELETE Success"

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->DisplayMessageDialog(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)V

    .line 345
    const-string v1, "RilDump"

    const-string v2, "NV DELETE Success"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 347
    :cond_5
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const-string v2, "NV DELETE fail"

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->DisplayMessageDialog(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)V

    .line 348
    const-string v1, "RilDump"

    const-string v2, "NV DELETE fail"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 354
    :pswitch_9
    const-string v1, "RilDump"

    const-string v2, "[RIL::FD] QUERY_FD_STATE_DONE"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fdEnable:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;
    invoke-static {}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$400()Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    move-result-object v1

    sget-object v2, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;->SET_TRUE:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    if-ne v1, v2, :cond_6

    .line 357
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleFd:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$500(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v1

    const-string v2, "Disable Fast Dormancy (Current State : Enabled)"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 358
    :cond_6
    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fdEnable:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;
    invoke-static {}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$400()Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    move-result-object v1

    sget-object v2, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;->SET_FALSE:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    if-ne v1, v2, :cond_7

    .line 359
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleFd:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$500(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v1

    const-string v2, "Enable Fast Dormancy (Current State : Disabled)"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 361
    :cond_7
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleFd:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$500(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v1

    const-string v2, "Fast Dormancy None (by CSC)"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 368
    :pswitch_a
    const-string v1, "RilDump"

    const-string v2, "REFRESH_DATARECOVERY_STATE"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleDataRecovery:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$700(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DATA_STALL_ALARM_AGGRESSIVE_DELAY_IN_MS\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->dataRecoveryTime:I
    invoke-static {}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$600()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "min (default 1min)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 376
    :pswitch_b
    const-string v1, "RilDump"

    const-string v2, "Apn_edit_state change"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mApnEditable:Z
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$800(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 378
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleApnEdit:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$900(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v1

    const-string v2, "APN IP Version Edit (Current State : Enabled)"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 380
    :cond_8
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleApnEdit:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$900(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v1

    const-string v2, "APN IP Version Edit (Current State : Disabled)"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 385
    :pswitch_c
    const-string v1, "RilDump"

    const-string v2, "Download Booster log state change"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mDBLogState:Z
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1000(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 389
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleDB:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1100(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v1

    const-string v2, "Download booster log : on"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 391
    :cond_9
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleDB:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1100(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v1

    const-string v2, "Download booster log : off"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 396
    :pswitch_d
    const-string v1, "RilDump"

    const-string v2, "Download Booster Warning Toast state change"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mDBWarningState:Z
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1200(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 400
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleDBWaring:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v1

    const-string v2, "Download booster Warning Toast : on"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 402
    :cond_a
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleDBWaring:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v1

    const-string v2, "Download booster Warning Toast : off"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 263
    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method
