.class public Lcom/sec/android/RilServiceModeApp/TerminalMode;
.super Landroid/app/Activity;
.source "TerminalMode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;
    }
.end annotation


# instance fields
.field MODEM_CDMA:C

.field public active_code:I

.field private alt_key_pressed:Z

.field private currentModeTypeForEnd:C

.field private currentSVMode:C

.field private key_enable:Z

.field private list:Landroid/widget/ListView;

.field mCallType:C

.field public mHandler:Landroid/os/Handler;

.field private mKeyValue:Landroid/widget/EditText;

.field private mLayout1:Landroid/widget/LinearLayout;

.field private mNeedQuery:Z

.field private mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

.field private mQueryTimes:I

.field private mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

.field private mServiceMessenger:Landroid/os/Messenger;

.field private mStrings:[Ljava/lang/String;

.field private mSvcModeMessenger:Landroid/os/Messenger;

.field private mWatcher:Landroid/text/TextWatcher;

.field private mkeyStringType:Ljava/lang/String;

.field private phone:Lcom/android/internal/telephony/Phone;

.field private selectedString:I

.field private total_line:I


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 53
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 65
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v3

    const-string v1, ""

    aput-object v1, v0, v4

    const-string v1, ""

    aput-object v1, v0, v6

    const/4 v1, 0x3

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, ""

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mStrings:[Ljava/lang/String;

    .line 68
    iput v3, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->active_code:I

    .line 69
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->selectedString:I

    .line 70
    iput v3, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->total_line:I

    .line 71
    iput-boolean v4, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->key_enable:Z

    .line 72
    iput-boolean v4, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mNeedQuery:Z

    .line 73
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mQueryTimes:I

    .line 74
    iput-boolean v3, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->alt_key_pressed:Z

    .line 76
    iput-object v5, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->phone:Lcom/android/internal/telephony/Phone;

    .line 78
    iput-object v5, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    .line 100
    iput-char v6, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->MODEM_CDMA:C

    .line 102
    iput-object v5, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mServiceMessenger:Landroid/os/Messenger;

    .line 104
    new-instance v0, Lcom/sec/android/RilServiceModeApp/TerminalMode$1;

    invoke-direct {v0, p0}, Lcom/sec/android/RilServiceModeApp/TerminalMode$1;-><init>(Lcom/sec/android/RilServiceModeApp/TerminalMode;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mHandler:Landroid/os/Handler;

    .line 200
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mSvcModeMessenger:Landroid/os/Messenger;

    .line 603
    new-instance v0, Lcom/sec/android/RilServiceModeApp/TerminalMode$2;

    invoke-direct {v0, p0}, Lcom/sec/android/RilServiceModeApp/TerminalMode$2;-><init>(Lcom/sec/android/RilServiceModeApp/TerminalMode;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    .line 876
    new-instance v0, Lcom/sec/android/RilServiceModeApp/TerminalMode$4;

    invoke-direct {v0, p0}, Lcom/sec/android/RilServiceModeApp/TerminalMode$4;-><init>(Lcom/sec/android/RilServiceModeApp/TerminalMode;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mWatcher:Landroid/text/TextWatcher;

    return-void
.end method

.method private AnalisysString()V
    .locals 6

    .prologue
    const-wide/16 v4, 0xc8

    const/4 v3, 0x0

    const/16 v2, 0x3e9

    .line 470
    const-string v0, "TerminalMode"

    const-string v1, "AnalisysString()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->Update()V

    .line 473
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mStrings:[Ljava/lang/String;

    aget-object v0, v0, v3

    const-string v1, "Service Menu"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 474
    const-string v0, "TerminalMode"

    const-string v1, "Service Menu !"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    iget v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mQueryTimes:I

    if-lez v0, :cond_0

    .line 478
    iput-boolean v3, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mNeedQuery:Z

    .line 479
    iget v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mQueryTimes:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mQueryTimes:I

    .line 482
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 502
    :goto_0
    return-void

    .line 484
    :cond_0
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 485
    const-string v0, "TerminalMode"

    const-string v1, "Service Menu read DONE!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 487
    :cond_1
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mStrings:[Ljava/lang/String;

    aget-object v0, v0, v3

    const-string v1, "End service mode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 488
    const-string v0, "TerminalMode"

    const-string v1, "End Service Mode !"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 490
    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->finish()V

    goto :goto_0

    .line 493
    :cond_2
    const-string v0, "TESTMODE"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mkeyStringType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mStrings:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    const-string v1, "002"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 494
    const-string v0, "TerminalMode"

    const-string v1, "End Service Mode !"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 497
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->systemReboot()V

    goto :goto_0

    .line 500
    :cond_3
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private KeyValueInputDisplay(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 449
    const-string v0, "TerminalMode"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KeyValueInputDisplay:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    if-eqz p1, :cond_0

    .line 452
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mLayout1:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 456
    :goto_0
    return-void

    .line 454
    :cond_0
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mLayout1:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private ParseKeyCodeToIPCDefineValue(I)C
    .locals 3
    .param p1, "key"    # I

    .prologue
    const/16 v1, 0x64

    const/16 v0, 0x63

    const/16 v2, 0x53

    .line 505
    sparse-switch p1, :sswitch_data_0

    .line 550
    const/4 v0, 0x0

    :goto_0
    :sswitch_0
    return v0

    .line 507
    :sswitch_1
    const/16 v0, 0x30

    goto :goto_0

    .line 509
    :sswitch_2
    const/16 v0, 0x31

    goto :goto_0

    .line 511
    :sswitch_3
    const/16 v0, 0x32

    goto :goto_0

    .line 513
    :sswitch_4
    const/16 v0, 0x33

    goto :goto_0

    .line 515
    :sswitch_5
    const/16 v0, 0x34

    goto :goto_0

    .line 517
    :sswitch_6
    const/16 v0, 0x35

    goto :goto_0

    .line 519
    :sswitch_7
    const/16 v0, 0x36

    goto :goto_0

    .line 521
    :sswitch_8
    const/16 v0, 0x37

    goto :goto_0

    .line 523
    :sswitch_9
    const/16 v0, 0x38

    goto :goto_0

    .line 525
    :sswitch_a
    const/16 v0, 0x39

    goto :goto_0

    .line 527
    :sswitch_b
    const/16 v0, 0x23

    goto :goto_0

    .line 529
    :sswitch_c
    const/16 v0, 0x2a

    goto :goto_0

    :sswitch_d
    move v0, v1

    .line 533
    goto :goto_0

    .line 535
    :sswitch_e
    const/16 v0, 0x65

    goto :goto_0

    .line 537
    :sswitch_f
    const/16 v0, 0x66

    goto :goto_0

    :sswitch_10
    move v0, v1

    .line 541
    goto :goto_0

    :sswitch_11
    move v0, v2

    .line 543
    goto :goto_0

    :sswitch_12
    move v0, v2

    .line 545
    goto :goto_0

    .line 505
    nop

    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_1
        0x8 -> :sswitch_2
        0x9 -> :sswitch_3
        0xa -> :sswitch_4
        0xb -> :sswitch_5
        0xc -> :sswitch_6
        0xd -> :sswitch_7
        0xe -> :sswitch_8
        0xf -> :sswitch_9
        0x10 -> :sswitch_a
        0x11 -> :sswitch_c
        0x12 -> :sswitch_b
        0x13 -> :sswitch_0
        0x14 -> :sswitch_d
        0x15 -> :sswitch_e
        0x16 -> :sswitch_f
        0x17 -> :sswitch_11
        0x18 -> :sswitch_0
        0x19 -> :sswitch_10
        0x42 -> :sswitch_12
    .end sparse-switch
.end method

.method private SendData(CCCIC)V
    .locals 4
    .param p1, "svcMode"    # C
    .param p2, "modetype"    # C
    .param p3, "subtype"    # C
    .param p4, "keycode"    # I
    .param p5, "query"    # C

    .prologue
    .line 390
    const/4 v0, 0x0

    .line 392
    .local v0, "data":[B
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->selectedString:I

    .line 393
    const-string v1, "TerminalMode"

    const-string v2, "inside SendData()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    packed-switch p1, :pswitch_data_0

    .line 431
    :pswitch_0
    const-string v1, "TerminalMode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Switch err - default : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    :goto_0
    iput-char p1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->currentSVMode:C

    .line 437
    if-nez v0, :cond_0

    .line 438
    const-string v1, "TerminalMode"

    const-string v2, " err - data is NULL"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    :goto_1
    return-void

    .line 398
    :pswitch_1
    const-string v1, "TerminalMode"

    const-string v2, "sending OEM_SM_ENTER_MODE_MESSAGE"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v1, p1, p2, p3, p5}, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->getServMEnterData(CCCC)[B

    move-result-object v0

    .line 400
    iput-char p2, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->currentModeTypeForEnd:C

    goto :goto_0

    .line 405
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->setEndModeData(CC)[B

    move-result-object v0

    .line 407
    goto :goto_0

    .line 411
    :pswitch_3
    const-string v1, "TerminalMode"

    const-string v2, "SendData -setPressKeyData "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v1, p1, p4, p5}, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->setPressKeyData(CIC)[B

    move-result-object v0

    .line 415
    goto :goto_0

    .line 419
    :pswitch_4
    iput-char p2, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->currentModeTypeForEnd:C

    .line 420
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v1, p1, p2, p3}, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->setTestCallMode(CCC)[B

    move-result-object v0

    .line 422
    goto :goto_0

    .line 426
    :pswitch_5
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v1, p1, p2, p3}, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->setEndTestCallMode(CCC)[B

    move-result-object v0

    .line 428
    goto :goto_0

    .line 445
    :cond_0
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3f0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_1

    .line 395
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method private StartTerminalMode()V
    .locals 10

    .prologue
    const/4 v8, 0x5

    const/4 v2, 0x4

    const/4 v6, 0x3

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 632
    const-string v0, "TerminalMode"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "StartTerminalMode keyString : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mkeyStringType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    const-string v0, "DEBUG_SCR"

    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mkeyStringType:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 636
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-object v0, p0

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->SendData(CCCIC)V

    .line 638
    invoke-direct {p0, v3}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->KeyValueInputDisplay(Z)V

    .line 676
    :goto_0
    return-void

    .line 639
    :cond_0
    const-string v0, "EI_DEBUG_SCR"

    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mkeyStringType:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 640
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x6

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-object v0, p0

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->SendData(CCCIC)V

    .line 642
    invoke-direct {p0, v3}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->KeyValueInputDisplay(Z)V

    goto :goto_0

    .line 643
    :cond_1
    const-string v0, "DATA_ADV"

    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mkeyStringType:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 644
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-object v4, p0

    move v5, v1

    move v7, v6

    move v8, v3

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->SendData(CCCIC)V

    .line 646
    invoke-direct {p0, v1}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->KeyValueInputDisplay(Z)V

    goto :goto_0

    .line 647
    :cond_2
    const-string v0, "NAMBASIC"

    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mkeyStringType:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 648
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-object v4, p0

    move v5, v1

    move v7, v1

    move v8, v3

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->SendData(CCCIC)V

    .line 650
    invoke-direct {p0, v1}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->KeyValueInputDisplay(Z)V

    goto/16 :goto_0

    .line 651
    :cond_3
    const-string v0, "TESTMODE"

    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mkeyStringType:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 652
    const-string v0, "TerminalMode"

    const-string v2, "sending data for TEST MODE ENTRY"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 653
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-object v0, p0

    move v2, v1

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->SendData(CCCIC)V

    .line 655
    invoke-direct {p0, v1}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->KeyValueInputDisplay(Z)V

    goto/16 :goto_0

    .line 656
    :cond_4
    const-string v0, "NAMSIMPLE"

    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mkeyStringType:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 657
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v7, 0x2

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-object v4, p0

    move v5, v1

    move v8, v3

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->SendData(CCCIC)V

    .line 659
    invoke-direct {p0, v1}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->KeyValueInputDisplay(Z)V

    goto/16 :goto_0

    .line 660
    :cond_5
    const-string v0, "TEST_CALL"

    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mkeyStringType:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 661
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v6, 0x7

    iget-char v7, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mCallType:C

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-object v4, p0

    move v5, v2

    move v8, v3

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->SendData(CCCIC)V

    .line 663
    invoke-direct {p0, v1}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->KeyValueInputDisplay(Z)V

    goto/16 :goto_0

    .line 664
    :cond_6
    const-string v0, "1111"

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mkeyStringType:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 665
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0x1002

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-object v4, p0

    move v5, v1

    move v6, v8

    move v8, v3

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->SendData(CCCIC)V

    .line 668
    invoke-direct {p0, v3}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->KeyValueInputDisplay(Z)V

    goto/16 :goto_0

    .line 669
    :cond_7
    const-string v0, "2222"

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mkeyStringType:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 670
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0x1003

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-object v4, p0

    move v5, v1

    move v6, v8

    move v8, v3

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->SendData(CCCIC)V

    .line 673
    invoke-direct {p0, v3}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->KeyValueInputDisplay(Z)V

    goto/16 :goto_0

    .line 675
    :cond_8
    const-string v0, "TerminalMode"

    const-string v1, "err -strange value"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private Update()V
    .locals 5

    .prologue
    .line 381
    const-string v0, "TerminalMode"

    const-string v1, "Update!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->list:Landroid/widget/ListView;

    new-instance v1, Landroid/widget/ArrayAdapter;

    const v2, 0x7f03000d

    const v3, 0x7f08003d

    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mStrings:[Ljava/lang/String;

    invoke-direct {v1, p0, v2, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 384
    const-string v0, "TerminalMode"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Selected String Index : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->selectedString:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/RilServiceModeApp/TerminalMode;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/TerminalMode;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mStrings:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/RilServiceModeApp/TerminalMode;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/TerminalMode;

    .prologue
    .line 53
    iget v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->selectedString:I

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/RilServiceModeApp/TerminalMode;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/TerminalMode;
    .param p1, "x1"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->selectedString:I

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/RilServiceModeApp/TerminalMode;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/TerminalMode;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->AnalisysString()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/RilServiceModeApp/TerminalMode;)Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/TerminalMode;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/RilServiceModeApp/TerminalMode;)C
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/TerminalMode;

    .prologue
    .line 53
    iget-char v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->currentModeTypeForEnd:C

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/RilServiceModeApp/TerminalMode;CCCIC)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/TerminalMode;
    .param p1, "x1"    # C
    .param p2, "x2"    # C
    .param p3, "x3"    # C
    .param p4, "x4"    # I
    .param p5, "x5"    # C

    .prologue
    .line 53
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->SendData(CCCIC)V

    return-void
.end method

.method static synthetic access$602(Lcom/sec/android/RilServiceModeApp/TerminalMode;Landroid/os/Messenger;)Landroid/os/Messenger;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/TerminalMode;
    .param p1, "x1"    # Landroid/os/Messenger;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mServiceMessenger:Landroid/os/Messenger;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/RilServiceModeApp/TerminalMode;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/TerminalMode;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->StartTerminalMode()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/RilServiceModeApp/TerminalMode;C)C
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/TerminalMode;
    .param p1, "x1"    # C

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->parseCharToKeyCode(C)C

    move-result v0

    return v0
.end method

.method private connectToRilService()V
    .locals 5

    .prologue
    .line 588
    const-string v2, "TerminalMode"

    const-string v3, "connect To Ril service"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 589
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 591
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "init.svc.ril-daemon2"

    const-string v3, "stopped"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 593
    .local v1, "strRilDaemon2Status":Ljava/lang/String;
    const-string v2, "TerminalMode"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "init.svc.ril-daemon2: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    const-string v2, "com.sec.phone"

    const-string v3, "com.sec.phone.SecPhoneService"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 600
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v2, v3}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 601
    return-void
.end method

.method private invokeOemRilRequestRaw([BLandroid/os/Message;)V
    .locals 3
    .param p1, "data"    # [B
    .param p2, "response"    # Landroid/os/Message;

    .prologue
    .line 616
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 617
    .local v0, "req":Landroid/os/Bundle;
    const-string v1, "request"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 619
    invoke-virtual {p2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 620
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mSvcModeMessenger:Landroid/os/Messenger;

    iput-object v1, p2, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 623
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mServiceMessenger:Landroid/os/Messenger;

    if-eqz v1, :cond_0

    .line 624
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mServiceMessenger:Landroid/os/Messenger;

    invoke-virtual {v1, p2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V

    .line 629
    :goto_0
    return-void

    .line 626
    :cond_0
    const-string v1, "TerminalMode"

    const-string v2, "mServiceMessenger is null. Do nothing."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 627
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private parseCharToKeyCode(C)C
    .locals 1
    .param p1, "key"    # C

    .prologue
    .line 554
    packed-switch p1, :pswitch_data_0

    .line 584
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 556
    :pswitch_1
    const/16 v0, 0x12

    goto :goto_0

    .line 558
    :pswitch_2
    const/16 v0, 0x11

    goto :goto_0

    .line 560
    :pswitch_3
    const/4 v0, 0x7

    goto :goto_0

    .line 562
    :pswitch_4
    const/16 v0, 0x8

    goto :goto_0

    .line 564
    :pswitch_5
    const/16 v0, 0x9

    goto :goto_0

    .line 566
    :pswitch_6
    const/16 v0, 0xa

    goto :goto_0

    .line 568
    :pswitch_7
    const/16 v0, 0xb

    goto :goto_0

    .line 570
    :pswitch_8
    const/16 v0, 0xc

    goto :goto_0

    .line 572
    :pswitch_9
    const/16 v0, 0xd

    goto :goto_0

    .line 574
    :pswitch_a
    const/16 v0, 0xe

    goto :goto_0

    .line 576
    :pswitch_b
    const/16 v0, 0xf

    goto :goto_0

    .line 578
    :pswitch_c
    const/16 v0, 0x10

    goto :goto_0

    .line 554
    :pswitch_data_0
    .packed-switch 0x23
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method private sendSecondaryTestOffCmd()V
    .locals 7

    .prologue
    const/16 v6, 0x22

    .line 906
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 907
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 910
    .local v1, "dos":Ljava/io/DataOutputStream;
    :try_start_0
    const-string v4, "TerminalMode"

    const-string v5, " writing byte data in seondaryoff"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 911
    const/4 v3, 0x5

    .line 912
    .local v3, "fileSize":I
    const/16 v4, 0xc

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 913
    const/16 v4, 0x22

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 914
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 915
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 916
    const-string v4, "TerminalMode"

    const-string v5, " completed writing  byte data"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 922
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 928
    :goto_0
    const-string v4, "TerminalMode"

    const-string v5, " sending the RIL command"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 935
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mHandler:Landroid/os/Handler;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    .line 936
    .end local v3    # "fileSize":I
    :goto_1
    return-void

    .line 923
    .restart local v3    # "fileSize":I
    :catch_0
    move-exception v2

    .line 924
    .local v2, "e":Ljava/io/IOException;
    const-string v4, "TerminalMode"

    const-string v5, "IOException in sendSecondaryTestOffCmd!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 917
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fileSize":I
    :catch_1
    move-exception v2

    .line 918
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_2
    const-string v4, "TerminalMode"

    const-string v5, " failed to write"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 922
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 923
    :catch_2
    move-exception v2

    .line 924
    const-string v4, "TerminalMode"

    const-string v5, "IOException in sendSecondaryTestOffCmd!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 921
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 922
    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 925
    :goto_2
    throw v4

    .line 923
    :catch_3
    move-exception v2

    .line 924
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v5, "TerminalMode"

    const-string v6, "IOException in sendSecondaryTestOffCmd!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private systemReboot()V
    .locals 2

    .prologue
    .line 939
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 941
    .local v0, "mPowerManager":Landroid/os/PowerManager;
    const-string v1, "Device Reboot."

    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    .line 942
    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 14
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v13, 0x7

    const/4 v2, 0x3

    const/4 v1, 0x2

    const/4 v12, 0x1

    const/4 v3, 0x0

    .line 784
    const-string v0, "TerminalMode"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "dispatchKeyEvent("

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ")..."

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 786
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v7

    .line 787
    .local v7, "android_keyCode":I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v10

    .line 788
    .local v10, "keyAction":I
    const/4 v11, 0x0

    .line 790
    .local v11, "modem_keyCode":I
    if-nez v10, :cond_7

    .line 791
    sparse-switch v7, :sswitch_data_0

    .line 836
    iget-char v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->currentModeTypeForEnd:C

    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-ne v0, v13, :cond_3

    move v5, v11

    .line 870
    .end local v11    # "modem_keyCode":I
    .local v5, "modem_keyCode":I
    :cond_0
    :goto_0
    if-lez v5, :cond_6

    move v0, v12

    .line 873
    :goto_1
    return v0

    .line 793
    .end local v5    # "modem_keyCode":I
    .restart local v11    # "modem_keyCode":I
    :sswitch_0
    const-string v0, "TerminalMode"

    const-string v1, "Currently alt key pressed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 794
    iput-boolean v12, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->alt_key_pressed:Z

    move v5, v11

    .line 795
    .end local v11    # "modem_keyCode":I
    .restart local v5    # "modem_keyCode":I
    goto :goto_0

    .line 798
    .end local v5    # "modem_keyCode":I
    .restart local v11    # "modem_keyCode":I
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->getIntent()Landroid/content/Intent;

    move-result-object v9

    .line 799
    .local v9, "intent":Landroid/content/Intent;
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3e9

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 800
    const-string v0, "secondary"

    invoke-virtual {v9, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 801
    .local v8, "i":I
    const/16 v0, 0x3e8

    if-ne v8, v0, :cond_1

    .line 802
    const-string v0, "TerminalMode"

    const-string v2, "coming from secondary"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->sendSecondaryTestOffCmd()V

    .line 804
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->setResult(I)V

    .line 805
    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->finish()V

    .line 807
    :cond_1
    iget-char v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->currentModeTypeForEnd:C

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-ne v0, v13, :cond_2

    .line 808
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v1, 0x9

    iget-char v2, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->currentModeTypeForEnd:C

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-object v0, p0

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->SendData(CCCIC)V

    .line 813
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->finish()V

    move v5, v11

    .line 814
    .end local v11    # "modem_keyCode":I
    .restart local v5    # "modem_keyCode":I
    goto :goto_0

    .line 811
    .end local v5    # "modem_keyCode":I
    .restart local v11    # "modem_keyCode":I
    :cond_2
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-char v2, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->currentModeTypeForEnd:C

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-object v0, p0

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->SendData(CCCIC)V

    goto :goto_2

    .line 817
    .end local v8    # "i":I
    .end local v9    # "intent":Landroid/content/Intent;
    :sswitch_2
    invoke-direct {p0, v7}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->ParseKeyCodeToIPCDefineValue(I)C

    move-result v5

    .line 818
    .end local v11    # "modem_keyCode":I
    .restart local v5    # "modem_keyCode":I
    const-string v0, "TerminalMode"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "keyCode input ! : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 819
    if-eqz v5, :cond_0

    .line 822
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-object v1, p0

    move v4, v3

    move v6, v3

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->SendData(CCCIC)V

    goto/16 :goto_0

    .end local v5    # "modem_keyCode":I
    .restart local v11    # "modem_keyCode":I
    :sswitch_3
    move v5, v11

    .line 832
    .end local v11    # "modem_keyCode":I
    .restart local v5    # "modem_keyCode":I
    goto/16 :goto_0

    .line 839
    .end local v5    # "modem_keyCode":I
    .restart local v11    # "modem_keyCode":I
    :cond_3
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x3e9

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 840
    iput-boolean v12, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mNeedQuery:Z

    .line 841
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mQueryTimes:I

    .line 843
    iget-boolean v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->alt_key_pressed:Z

    if-eqz v0, :cond_4

    .line 844
    const-string v0, "TerminalMode"

    const-string v4, "Previously alt key pressed"

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 845
    iput-boolean v3, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->alt_key_pressed:Z

    .line 847
    const/16 v0, 0xa

    if-ne v7, v0, :cond_5

    .line 848
    const/16 v7, 0x12

    .line 855
    :cond_4
    :goto_3
    invoke-direct {p0, v7}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->ParseKeyCodeToIPCDefineValue(I)C

    move-result v5

    .line 856
    .end local v11    # "modem_keyCode":I
    .restart local v5    # "modem_keyCode":I
    const-string v0, "TerminalMode"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "keyCode input ! : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 858
    if-eqz v5, :cond_0

    .line 861
    iget-char v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->currentModeTypeForEnd:C

    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-eq v0, v1, :cond_0

    .line 862
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-object v1, p0

    move v4, v3

    move v6, v3

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->SendData(CCCIC)V

    goto/16 :goto_0

    .line 849
    .end local v5    # "modem_keyCode":I
    .restart local v11    # "modem_keyCode":I
    :cond_5
    const/16 v0, 0xf

    if-ne v7, v0, :cond_7

    .line 850
    const/16 v7, 0x11

    goto :goto_3

    .line 873
    .end local v11    # "modem_keyCode":I
    .restart local v5    # "modem_keyCode":I
    :cond_6
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_1

    .end local v5    # "modem_keyCode":I
    .restart local v11    # "modem_keyCode":I
    :cond_7
    move v5, v11

    .end local v11    # "modem_keyCode":I
    .restart local v5    # "modem_keyCode":I
    goto/16 :goto_0

    .line 791
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0x4 -> :sswitch_1
        0x6 -> :sswitch_3
        0x39 -> :sswitch_0
        0x4c -> :sswitch_2
    .end sparse-switch
.end method

.method public displayError()V
    .locals 2

    .prologue
    .line 192
    const-string v0, "Nothing updated!"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 193
    return-void
.end method

.method public displaySecondaryTestResult()V
    .locals 2

    .prologue
    .line 197
    const-string v0, "Secondary test completed"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 198
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 680
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 682
    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 683
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "keyString"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mkeyStringType:Ljava/lang/String;

    .line 684
    const-string v1, "callType"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    int-to-char v1, v1

    iput-char v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mCallType:C

    .line 686
    const-string v1, "TerminalMode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getString is asdfasdf "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mkeyStringType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 688
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->connectToRilService()V

    .line 690
    const v1, 0x7f03000c

    invoke-virtual {p0, v1}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->setContentView(I)V

    .line 695
    new-instance v1, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;-><init>(Lcom/sec/android/RilServiceModeApp/TerminalMode;Lcom/sec/android/RilServiceModeApp/TerminalMode$1;)V

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    .line 696
    const v1, 0x7f08000f

    invoke-virtual {p0, v1}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->list:Landroid/widget/ListView;

    .line 697
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->list:Landroid/widget/ListView;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 699
    const v1, 0x7f08003b

    invoke-virtual {p0, v1}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mLayout1:Landroid/widget/LinearLayout;

    .line 701
    const v1, 0x7f08003c

    invoke-virtual {p0, v1}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mKeyValue:Landroid/widget/EditText;

    .line 702
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mKeyValue:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 703
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mKeyValue:Landroid/widget/EditText;

    const-string v2, "Touch Here"

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 705
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3e9

    const-wide/16 v4, 0xc8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 707
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->Update()V

    .line 709
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->list:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/android/RilServiceModeApp/TerminalMode$3;

    invoke-direct {v2, p0}, Lcom/sec/android/RilServiceModeApp/TerminalMode$3;-><init>(Lcom/sec/android/RilServiceModeApp/TerminalMode;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 739
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 743
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->unbindService(Landroid/content/ServiceConnection;)V

    .line 744
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    .line 746
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 748
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 774
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 775
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 777
    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->onStop()V

    .line 778
    return-void
.end method

.method protected onStop()V
    .locals 4

    .prologue
    .line 753
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 755
    const-string v2, "TerminalMode"

    const-string v3, "stop called"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 757
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x3e9

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 759
    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 761
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "secondary"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 762
    .local v0, "i":I
    const/16 v2, 0x3e8

    if-ne v0, v2, :cond_0

    .line 763
    const-string v2, "TerminalMode"

    const-string v3, "coming from secondary"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 764
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->sendSecondaryTestOffCmd()V

    .line 765
    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->setResult(I)V

    .line 766
    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->finish()V

    .line 770
    :cond_0
    return-void
.end method
