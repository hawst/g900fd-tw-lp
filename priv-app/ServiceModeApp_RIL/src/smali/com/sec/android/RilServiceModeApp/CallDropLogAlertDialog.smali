.class public Lcom/sec/android/RilServiceModeApp/CallDropLogAlertDialog;
.super Landroid/app/Activity;
.source "CallDropLogAlertDialog.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mOkPopup:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 13
    const-string v0, "CallDropLogAlertDialog"

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogAlertDialog;->TAG:Ljava/lang/String;

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogAlertDialog;->mOkPopup:Landroid/app/AlertDialog;

    return-void
.end method

.method private dialogOkPopup(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    .line 27
    const-string v1, "CallDropLogAlertDialog"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dialogOkPopup : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogAlertDialog;->mOkPopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogAlertDialog;->mOkPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 29
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogAlertDialog;->mOkPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 30
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 31
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 32
    invoke-virtual {v0, p3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 33
    const-string v1, "OK"

    new-instance v2, Lcom/sec/android/RilServiceModeApp/CallDropLogAlertDialog$1;

    invoke-direct {v2, p0}, Lcom/sec/android/RilServiceModeApp/CallDropLogAlertDialog$1;-><init>(Lcom/sec/android/RilServiceModeApp/CallDropLogAlertDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 38
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 39
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogAlertDialog;->mOkPopup:Landroid/app/AlertDialog;

    .line 40
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogAlertDialog;->mOkPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 41
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 19
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 20
    const v1, 0x7f030005

    invoke-virtual {p0, v1}, Lcom/sec/android/RilServiceModeApp/CallDropLogAlertDialog;->setContentView(I)V

    .line 22
    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/CallDropLogAlertDialog;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "popup_event"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 23
    .local v0, "strEvent":Ljava/lang/String;
    const-string v1, "Call Failure (N/W Error)"

    invoke-direct {p0, p0, v1, v0}, Lcom/sec/android/RilServiceModeApp/CallDropLogAlertDialog;->dialogOkPopup(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    return-void
.end method
