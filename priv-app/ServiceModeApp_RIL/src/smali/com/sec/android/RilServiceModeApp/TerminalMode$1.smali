.class Lcom/sec/android/RilServiceModeApp/TerminalMode$1;
.super Landroid/os/Handler;
.source "TerminalMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/RilServiceModeApp/TerminalMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;


# direct methods
.method constructor <init>(Lcom/sec/android/RilServiceModeApp/TerminalMode;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$1;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 107
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 185
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$1;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    invoke-virtual {v0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->displayError()V

    .line 188
    :goto_0
    return-void

    .line 110
    :sswitch_0
    const-string v0, "TerminalMode"

    const-string v2, "in QUERT_SERVM_DONE"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "error"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 112
    .local v7, "error":I
    if-eqz v7, :cond_0

    .line 113
    const-string v0, "TerminalMode"

    const-string v2, "Exception Occur : %d"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 117
    :cond_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "response"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v6

    check-cast v6, [B

    .line 119
    .local v6, "buf":[B
    array-length v0, v6

    if-nez v0, :cond_1

    .line 121
    const-string v0, "TerminalMode"

    const-string v1, "response is null"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 126
    :cond_1
    array-length v0, v6

    div-int/lit8 v10, v0, 0x22

    .line 127
    .local v10, "numOfLine":I
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$1;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    # getter for: Lcom/sec/android/RilServiceModeApp/TerminalMode;->mStrings:[Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->access$000(Lcom/sec/android/RilServiceModeApp/TerminalMode;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-le v10, v0, :cond_2

    .line 128
    const-string v0, "TerminalMode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Too many lines. "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$1;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    # getter for: Lcom/sec/android/RilServiceModeApp/TerminalMode;->mStrings:[Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->access$000(Lcom/sec/android/RilServiceModeApp/TerminalMode;)[Ljava/lang/String;

    move-result-object v0

    array-length v10, v0

    .line 131
    :cond_2
    const/4 v0, 0x3

    array-length v2, v6

    add-int/lit8 v2, v2, -0x3

    invoke-static {v6, v0, v6, v3, v2}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 132
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    if-ge v8, v10, :cond_9

    .line 135
    new-instance v11, Ljava/lang/StringBuffer;

    const-string v0, ""

    invoke-direct {v11, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 136
    .local v11, "rowString":Ljava/lang/StringBuffer;
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$1;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    # getter for: Lcom/sec/android/RilServiceModeApp/TerminalMode;->mStrings:[Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->access$000(Lcom/sec/android/RilServiceModeApp/TerminalMode;)[Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    aput-object v2, v0, v8

    .line 138
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_2
    const/16 v0, 0x22

    if-ge v9, v0, :cond_6

    .line 139
    if-eqz v9, :cond_3

    if-ne v9, v1, :cond_5

    .line 140
    :cond_3
    if-ne v9, v1, :cond_4

    .line 141
    mul-int/lit8 v0, v8, 0x22

    add-int/2addr v0, v9

    aget-byte v0, v6, v0

    if-ne v0, v1, :cond_4

    .line 142
    const-string v0, "TerminalMode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Item focus : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$1;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    # setter for: Lcom/sec/android/RilServiceModeApp/TerminalMode;->selectedString:I
    invoke-static {v0, v8}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->access$102(Lcom/sec/android/RilServiceModeApp/TerminalMode;I)I

    .line 138
    :cond_4
    :goto_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 149
    :cond_5
    mul-int/lit8 v0, v8, 0x22

    add-int/2addr v0, v9

    aget-byte v0, v6, v0

    if-nez v0, :cond_7

    .line 150
    const-string v0, "TerminalMode"

    const-string v2, "break"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    :cond_6
    const-string v0, "TerminalMode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mStrings[i] i="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$1;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    # getter for: Lcom/sec/android/RilServiceModeApp/TerminalMode;->mStrings:[Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->access$000(Lcom/sec/android/RilServiceModeApp/TerminalMode;)[Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$1;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    # getter for: Lcom/sec/android/RilServiceModeApp/TerminalMode;->selectedString:I
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->access$100(Lcom/sec/android/RilServiceModeApp/TerminalMode;)I

    move-result v0

    if-ne v0, v8, :cond_8

    const-string v0, " *"

    :goto_4
    invoke-virtual {v11, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v8

    .line 158
    const-string v0, "TerminalMode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mStrings[i]:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$1;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    # getter for: Lcom/sec/android/RilServiceModeApp/TerminalMode;->mStrings:[Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->access$000(Lcom/sec/android/RilServiceModeApp/TerminalMode;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 153
    :cond_7
    mul-int/lit8 v0, v8, 0x22

    add-int/2addr v0, v9

    aget-byte v0, v6, v0

    int-to-char v0, v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 156
    :cond_8
    const-string v0, ""

    goto :goto_4

    .line 161
    .end local v9    # "j":I
    .end local v11    # "rowString":Ljava/lang/StringBuffer;
    :cond_9
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$1;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    # invokes: Lcom/sec/android/RilServiceModeApp/TerminalMode;->AnalisysString()V
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->access$200(Lcom/sec/android/RilServiceModeApp/TerminalMode;)V

    goto/16 :goto_0

    .line 167
    .end local v6    # "buf":[B
    .end local v7    # "error":I
    .end local v8    # "i":I
    .end local v10    # "numOfLine":I
    :sswitch_1
    const-string v0, "TerminalMode"

    const-string v2, "EVENT_TICK!"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$1;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$1;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    # getter for: Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->access$300(Lcom/sec/android/RilServiceModeApp/TerminalMode;)Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$1;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    # getter for: Lcom/sec/android/RilServiceModeApp/TerminalMode;->currentModeTypeForEnd:C
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->access$400(Lcom/sec/android/RilServiceModeApp/TerminalMode;)C

    move-result v2

    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$1;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    # getter for: Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;
    invoke-static {v4}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->access$300(Lcom/sec/android/RilServiceModeApp/TerminalMode;)Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$1;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    # getter for: Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;
    invoke-static {v4}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->access$300(Lcom/sec/android/RilServiceModeApp/TerminalMode;)Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$1;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    # getter for: Lcom/sec/android/RilServiceModeApp/TerminalMode;->mOem:Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;
    invoke-static {v4}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->access$300(Lcom/sec/android/RilServiceModeApp/TerminalMode;)Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move v4, v3

    move v5, v1

    # invokes: Lcom/sec/android/RilServiceModeApp/TerminalMode;->SendData(CCCIC)V
    invoke-static/range {v0 .. v5}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->access$500(Lcom/sec/android/RilServiceModeApp/TerminalMode;CCCIC)V

    goto/16 :goto_0

    .line 174
    :sswitch_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "error"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 175
    .restart local v7    # "error":I
    if-eqz v7, :cond_a

    .line 176
    const-string v0, "TerminalMode"

    const-string v1, "Exception Occur!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 178
    :cond_a
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$1;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    invoke-virtual {v0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->displaySecondaryTestResult()V

    goto/16 :goto_0

    .line 107
    nop

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_2
        0x3e9 -> :sswitch_1
        0x3f0 -> :sswitch_0
    .end sparse-switch
.end method
