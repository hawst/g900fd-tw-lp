.class Lcom/sec/android/RilServiceModeApp/ViewRilLog$1;
.super Landroid/os/Handler;
.source "ViewRilLog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/RilServiceModeApp/ViewRilLog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/RilServiceModeApp/ViewRilLog;


# direct methods
.method constructor <init>(Lcom/sec/android/RilServiceModeApp/ViewRilLog;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog$1;->this$0:Lcom/sec/android/RilServiceModeApp/ViewRilLog;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x0

    .line 138
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 162
    :goto_0
    return-void

    .line 140
    :sswitch_0
    const-string v0, "ViewRilLog"

    const-string v1, "PARSING_DONE ... "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/StringBuilder;

    # setter for: Lcom/sec/android/RilServiceModeApp/ViewRilLog;->LogMessage:Ljava/lang/StringBuilder;
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->access$002(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 144
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog$1;->this$0:Lcom/sec/android/RilServiceModeApp/ViewRilLog;

    # getter for: Lcom/sec/android/RilServiceModeApp/ViewRilLog;->mTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->access$100(Lcom/sec/android/RilServiceModeApp/ViewRilLog;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 145
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog$1;->this$0:Lcom/sec/android/RilServiceModeApp/ViewRilLog;

    # getter for: Lcom/sec/android/RilServiceModeApp/ViewRilLog;->mTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->access$100(Lcom/sec/android/RilServiceModeApp/ViewRilLog;)Landroid/widget/TextView;

    move-result-object v0

    # getter for: Lcom/sec/android/RilServiceModeApp/ViewRilLog;->LogMessage:Ljava/lang/StringBuilder;
    invoke-static {}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->access$000()Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog$1;->this$0:Lcom/sec/android/RilServiceModeApp/ViewRilLog;

    # getter for: Lcom/sec/android/RilServiceModeApp/ViewRilLog;->mTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->access$100(Lcom/sec/android/RilServiceModeApp/ViewRilLog;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 151
    :sswitch_1
    const-string v0, "ViewRilLog"

    const-string v1, "Ril dump Success"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 155
    :sswitch_2
    const-string v0, "ViewRilLog"

    const-string v1, "EVENT_TICK!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 138
    nop

    :sswitch_data_0
    .sparse-switch
        0x3e9 -> :sswitch_2
        0x3f1 -> :sswitch_1
        0x3f2 -> :sswitch_0
    .end sparse-switch
.end method
