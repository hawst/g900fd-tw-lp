.class public Lcom/sec/android/RilServiceModeApp/GcfModeSettings;
.super Landroid/app/Activity;
.source "GcfModeSettings.java"


# instance fields
.field private GcfMode:Ljava/lang/String;

.field private bGcfMode:Z

.field private mClicked:Landroid/view/View$OnClickListener;

.field private mGcfMode:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 82
    new-instance v0, Lcom/sec/android/RilServiceModeApp/GcfModeSettings$1;

    invoke-direct {v0, p0}, Lcom/sec/android/RilServiceModeApp/GcfModeSettings$1;-><init>(Lcom/sec/android/RilServiceModeApp/GcfModeSettings;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->mClicked:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/RilServiceModeApp/GcfModeSettings;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/GcfModeSettings;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->mGcfMode:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/RilServiceModeApp/GcfModeSettings;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/GcfModeSettings;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->bGcfMode:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/RilServiceModeApp/GcfModeSettings;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/GcfModeSettings;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->bGcfMode:Z

    return p1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 49
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    const v2, 0x7f030004

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->setContentView(I)V

    .line 52
    const-string v2, "Off"

    iput-object v2, p0, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->GcfMode:Ljava/lang/String;

    .line 53
    const v2, 0x7f08000d

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->mGcfMode:Landroid/widget/Button;

    .line 56
    const-string v2, "On"

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->GcfMode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 57
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->mGcfMode:Landroid/widget/Button;

    const-string v3, "Gcf Mode Enabled"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 58
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->bGcfMode:Z

    .line 64
    :goto_0
    iget-boolean v2, p0, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->bGcfMode:Z

    if-eqz v2, :cond_1

    const-string v1, "enabled"

    .line 66
    .local v1, "mode":Ljava/lang/String;
    :goto_1
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.app.GCF_MODE_ACTION"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 67
    .local v0, "i":Landroid/content/Intent;
    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 68
    const-string v2, "key"

    const-string v3, "gcf_mode"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 69
    const-string v2, "mode"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 70
    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->sendBroadcast(Landroid/content/Intent;)V

    .line 72
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->mGcfMode:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    return-void

    .line 60
    .end local v0    # "i":Landroid/content/Intent;
    .end local v1    # "mode":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->mGcfMode:Landroid/widget/Button;

    const-string v3, "Gcf Mode Disabled"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 61
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->bGcfMode:Z

    goto :goto_0

    .line 64
    :cond_1
    const-string v1, "disabled"

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 78
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 79
    return-void
.end method
