.class public Lcom/sec/android/RilServiceModeApp/ServiceModeApp;
.super Landroid/app/Activity;
.source "ServiceModeApp.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;
    }
.end annotation


# static fields
.field private static final mProductDevice:Ljava/lang/String;


# instance fields
.field private arrayAdapterString:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private calledByKeyPress:Z

.field private chipname:Ljava/lang/String;

.field private currentModeTypeForEnd:C

.field private currentSVMode:C

.field private isRoamingFreq:Z

.field level:I

.field private list:Landroid/widget/ListView;

.field public mHandler:Landroid/os/Handler;

.field private mIsActive:Z

.field private mKeyString:Ljava/lang/String;

.field private mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

.field private final mProductShip:Ljava/lang/String;

.field mSalescode:Ljava/lang/String;

.field private mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

.field private mServiceMessenger:Landroid/os/Messenger;

.field private mStrings:[Ljava/lang/String;

.field private mSvcModeMessenger:Landroid/os/Messenger;

.field mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private model:Ljava/lang/String;

.field private modemBoard2:Ljava/lang/String;

.field private nwkTypeName:[Ljava/lang/String;

.field private oem_modem_type:C

.field roamingfreqchange:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 96
    const-string v0, "ro.product.device"

    const-string v1, "NONE"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mProductDevice:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 70
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 79
    const/16 v0, 0x32

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;

    .line 81
    iput-char v2, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    .line 83
    iput-object v3, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    .line 93
    const-string v0, "ro.product.model"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->model:Ljava/lang/String;

    .line 94
    const-string v0, "ro.product_ship"

    const-string v1, "FALSE"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mProductShip:Ljava/lang/String;

    .line 98
    const-string v0, "ril.modem.board2"

    const-string v1, "NONE"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->modemBoard2:Ljava/lang/String;

    .line 99
    const-string v0, "ril.modem.board"

    const-string v1, "NONE"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->chipname:Ljava/lang/String;

    .line 104
    iput-object v3, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mServiceMessenger:Landroid/os/Messenger;

    .line 105
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    .line 106
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "MOBILE"

    aput-object v1, v0, v2

    const-string v1, "MOBILE_IMS"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->nwkTypeName:[Ljava/lang/String;

    .line 108
    iput-boolean v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mIsActive:Z

    .line 109
    iput-boolean v2, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->calledByKeyPress:Z

    .line 110
    iput-boolean v2, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->isRoamingFreq:Z

    .line 111
    const-string v0, "ro.csc.sales_code"

    const-string v1, "NONE"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mSalescode:Ljava/lang/String;

    .line 112
    iput-object v3, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->roamingfreqchange:Landroid/widget/Button;

    .line 116
    iput v2, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->level:I

    .line 121
    new-instance v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;

    invoke-direct {v0, p0}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;-><init>(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mHandler:Landroid/os/Handler;

    .line 252
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mSvcModeMessenger:Landroid/os/Messenger;

    .line 611
    new-instance v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$3;

    invoke-direct {v0, p0}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$3;-><init>(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method private AnalisysString()V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const-wide/16 v6, 0xc8

    const/16 v4, 0x3e9

    const/4 v2, 0x0

    .line 544
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->Update()V

    .line 546
    iget-boolean v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->calledByKeyPress:Z

    if-eqz v0, :cond_0

    .line 547
    const-string v0, "ServiceModeApp_RIL"

    const-string v1, "Refresh Screen due to previous Key press. "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    iput-boolean v2, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->calledByKeyPress:Z

    .line 549
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 569
    :goto_0
    return-void

    .line 553
    :cond_0
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;

    aget-object v0, v0, v2

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;

    aget-object v0, v0, v2

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;

    aget-object v0, v0, v3

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;

    aget-object v0, v0, v3

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 555
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->TmpFixFunc()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 556
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 558
    :cond_3
    const-string v0, "ServiceModeApp_RIL"

    const-string v1, " Do not need refresh "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 559
    :cond_4
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;

    aget-object v0, v0, v2

    const-string v1, "End service mode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 560
    const-string v0, "ServiceModeApp_RIL"

    const-string v1, "End Service Mode !"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->finish()V

    goto :goto_0

    .line 563
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->IsOnlyKeyNumberDisplay()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 564
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 566
    :cond_6
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private EndServiceMode()V
    .locals 8

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x2

    const/4 v3, 0x0

    .line 865
    const-string v0, "ServiceModeApp_RIL"

    const-string v4, "End Service Mode !"

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 867
    iput-boolean v3, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mIsActive:Z

    .line 869
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x3e9

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 871
    iget-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->currentModeTypeForEnd:C

    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-ne v0, v2, :cond_2

    .line 872
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    .line 878
    :goto_0
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 879
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 881
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->isRoamingFreq:Z

    if-eqz v0, :cond_1

    .line 882
    const-string v0, "ServiceModeApp_RIL"

    const-string v1, "End Service Mode (isRoamingFreq true)- unbindService"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 883
    iput-boolean v3, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->isRoamingFreq:Z

    .line 885
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_1

    .line 886
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 891
    :cond_1
    :goto_1
    return-void

    .line 875
    :cond_2
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto :goto_0

    .line 887
    :catch_0
    move-exception v7

    .line 888
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private IsOnlyKeyNumberDisplay()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 515
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v0, :cond_0

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_0

    .line 516
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x30

    if-le v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x3a

    if-ge v2, v3, :cond_0

    .line 521
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private ParseKeyValueForModem(C)C
    .locals 4
    .param p1, "key"    # C

    .prologue
    .line 572
    move v0, p1

    .line 574
    .local v0, "rtn_key":C
    sparse-switch p1, :sswitch_data_0

    .line 607
    :goto_0
    const-string v1, "ServiceModeApp_RIL"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "rtn_key is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    return v0

    .line 577
    :sswitch_0
    const/16 v0, 0x41

    .line 578
    goto :goto_0

    .line 581
    :sswitch_1
    const/16 v0, 0x42

    .line 582
    goto :goto_0

    .line 585
    :sswitch_2
    const/16 v0, 0x43

    .line 586
    goto :goto_0

    .line 589
    :sswitch_3
    const/16 v0, 0x44

    .line 590
    goto :goto_0

    .line 593
    :sswitch_4
    const/16 v0, 0x45

    .line 594
    goto :goto_0

    .line 597
    :sswitch_5
    const/16 v0, 0x46

    .line 598
    goto :goto_0

    .line 601
    :sswitch_6
    const/16 v0, 0x2a

    .line 602
    goto :goto_0

    .line 574
    :sswitch_data_0
    .sparse-switch
        0x2a -> :sswitch_6
        0x2d -> :sswitch_6
        0x41 -> :sswitch_0
        0x42 -> :sswitch_1
        0x43 -> :sswitch_2
        0x44 -> :sswitch_3
        0x45 -> :sswitch_4
        0x46 -> :sswitch_5
        0x61 -> :sswitch_0
        0x62 -> :sswitch_1
        0x63 -> :sswitch_2
        0x64 -> :sswitch_3
        0x65 -> :sswitch_4
        0x66 -> :sswitch_5
    .end sparse-switch
.end method

.method private SendData(CCCCCC)V
    .locals 7
    .param p1, "svcMode"    # C
    .param p2, "modetype"    # C
    .param p3, "subtype"    # C
    .param p4, "keycode"    # C
    .param p5, "query"    # C
    .param p6, "modem_type"    # C

    .prologue
    .line 443
    const/4 v6, 0x0

    .line 445
    .local v6, "data":[B
    packed-switch p1, :pswitch_data_0

    .line 473
    const-string v0, "ServiceModeApp_RIL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Switch err - default : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    :cond_0
    :goto_0
    :pswitch_0
    iput-char p1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->currentSVMode:C

    .line 479
    if-nez v6, :cond_2

    .line 480
    const-string v0, "ServiceModeApp_RIL"

    const-string v1, " err - data is NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    :goto_1
    return-void

    .line 447
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p5

    move v5, p6

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->getServMEnterData(CCCCC)[B

    move-result-object v6

    .line 448
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-eqz p2, :cond_0

    .line 449
    iput-char p2, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->currentModeTypeForEnd:C

    goto :goto_0

    .line 453
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0, p1, p2, p6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->setEndModeData(CCC)[B

    move-result-object v6

    .line 454
    goto :goto_0

    .line 457
    :pswitch_3
    const-string v0, "ServiceModeApp_RIL"

    const-string v1, "SendData -setPressKeyData "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mProductShip:Ljava/lang/String;

    const-string v1, "TRUE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->isKeyStringBlocked()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x35

    if-ne p4, v0, :cond_1

    .line 459
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    const-string v1, "[5] AUDIO"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 460
    const/4 v6, 0x0

    .line 461
    const-string v0, "ServiceModeApp_RIL"

    const-string v1, "SendData -[5] AUDIO ITEM Skip!! Because of the usermode condition!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 466
    :cond_1
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0, p1, p4, p5, p6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->setPressKeyData(CCCC)[B

    move-result-object v6

    .line 467
    goto :goto_0

    .line 488
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    .line 489
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3f1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-direct {p0, v6, v0}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_1

    .line 491
    :cond_3
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3f0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-direct {p0, v6, v0}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_1

    .line 445
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method private StartServiceMode()V
    .locals 13

    .prologue
    .line 685
    const-string v0, "ServiceModeApp_RIL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "StartServiceMode] keyString : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 687
    const-string v0, "197328640"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 688
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    .line 862
    :goto_0
    return-void

    .line 690
    :cond_0
    const-string v0, "TESTMODE"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 691
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto :goto_0

    .line 693
    :cond_1
    const-string v0, "27663368378"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 694
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x1023

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto :goto_0

    .line 696
    :cond_2
    const-string v0, "276633683782"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 697
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x1023

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 699
    :cond_3
    const-string v0, "0011"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "sm-g350"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->model:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "sm-g313"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->model:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "gt-s5312"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->model:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "sm-g110"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->model:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "sm-g130"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->model:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "gt-s5282"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->model:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "gt-s7262"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->model:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_4
    const-string v0, "00112"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 701
    :cond_5
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x4

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 703
    :cond_6
    const-string v0, "00112"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 704
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x7

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 706
    :cond_7
    const-string v0, "6201"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 707
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x40

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 709
    :cond_8
    const-string v0, "123456"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 710
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x4

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 712
    :cond_9
    const-string v0, "0228"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 713
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v3, 0x5

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 715
    :cond_a
    const-string v0, "32489"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 716
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v3, 0x6

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 719
    :cond_b
    const-string v0, "2580"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 720
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v3, 0x7

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 723
    :cond_c
    const-string v0, "9090"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 724
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x13

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 727
    :cond_d
    const-string v0, "0599"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 728
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x14

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 730
    :cond_e
    const-string v0, "7284"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 731
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x13

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 734
    :cond_f
    const-string v0, "4238378"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 735
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x17

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 739
    :cond_10
    const-string v0, "1575"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 740
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x15

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 743
    :cond_11
    const-string v0, "73876766"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 744
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x1b

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 747
    :cond_12
    const-string v0, "738767633"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 748
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x1c

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 751
    :cond_13
    const-string v0, "7387678378"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 752
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x1d

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 755
    :cond_14
    const-string v0, "7387677763"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 756
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x1e

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 759
    :cond_15
    const-string v0, "4387264636"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 760
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x1f

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 763
    :cond_16
    const-string v0, "6984125*"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 764
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x20

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 767
    :cond_17
    const-string v0, "2886"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 768
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x20

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 771
    :cond_18
    const-string v0, "2767*2878"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 772
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x21

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 774
    :cond_19
    const-string v0, "1111"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 775
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x1002

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 778
    :cond_1a
    const-string v0, "2222"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 780
    const-string v0, "ro.product.model"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    .line 782
    .local v9, "model":Ljava/lang/String;
    const-string v0, "ServiceModeApp_RIL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "2222 => model="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 783
    const-string v0, "samsung-sgh-i747"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1b

    const-string v0, "sgh-t999"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 784
    :cond_1b
    const-string v0, "ro.revision"

    const-string v1, "1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    .line 785
    .local v11, "revision":Ljava/lang/String;
    const-string v0, "ServiceModeApp_RIL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ro.revision => raw="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 786
    const/4 v10, 0x0

    .line 788
    .local v10, "nRev":I
    :try_start_0
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v10

    .line 793
    :goto_1
    const/16 v0, 0x10

    if-lt v10, v0, :cond_1c

    .line 794
    const-string v0, "ServiceModeApp_RIL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "2G devices => Rev="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 795
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "READ FTA HW VERSION"

    aput-object v2, v0, v1

    .line 796
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;

    const/4 v1, 0x1

    const-string v2, "FTA HW VERSION : REV0.4"

    aput-object v2, v0, v1

    .line 797
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->Update()V

    goto/16 :goto_0

    .line 789
    :catch_0
    move-exception v8

    .line 790
    .local v8, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v8}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 791
    const/4 v10, 0x0

    goto :goto_1

    .line 802
    .end local v8    # "e":Ljava/lang/NumberFormatException;
    .end local v10    # "nRev":I
    .end local v11    # "revision":Ljava/lang/String;
    :cond_1c
    const-string v0, "ServiceModeApp_RIL"

    const-string v1, "1G devices!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x1003

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 807
    .end local v9    # "model":Ljava/lang/String;
    :cond_1d
    const-string v0, "8888"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 808
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x1003

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 811
    :cond_1e
    const-string v0, "301279"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1f

    const-string v0, "279301"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 812
    :cond_1f
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x14

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 814
    :cond_20
    const-string v0, "2263"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_22

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->model:Ljava/lang/String;

    const-string v1, "sm-g350"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_21

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->model:Ljava/lang/String;

    const-string v1, "sm-g313"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_21

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->model:Ljava/lang/String;

    const-string v1, "gt-s5312"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_21

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->model:Ljava/lang/String;

    const-string v1, "sm-g110"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_21

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->model:Ljava/lang/String;

    const-string v1, "sm-g130"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_21

    const-string v0, "gt-s5282"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->model:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_21

    const-string v0, "gt-s7262"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->model:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    :cond_21
    const-string v0, "22632"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 816
    :cond_22
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x16

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 818
    :cond_23
    const-string v0, "66336"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 819
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x1022

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 821
    :cond_24
    const-string v0, "LTE_ANT_PATH_NORMAL"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 822
    const-string v0, "ServiceModeApp_RIL"

    const-string v1, "LTE_ANT_PATH_NORMAL test"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 823
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x22

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 826
    :cond_25
    const-string v0, "37375625"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 827
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x1025

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 829
    :cond_26
    const-string v0, "58366"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 830
    const-string v0, "ServiceModeApp_RIL"

    const-string v1, "[LTEON 14] Key input, Store LTE State as LTE ON!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 831
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x24

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 833
    :cond_27
    const-string v0, "CP_RAMDUMP"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 834
    new-instance v12, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;

    invoke-direct {v12}, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;-><init>()V

    .line 836
    .local v12, "temp":Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;
    invoke-virtual {v12}, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->DoRamdumpScreenUsingDebugScreen()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 837
    const-string v0, "ServiceModeApp_RIL"

    const-string v1, "DUMP_SVCIPC"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 838
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x1024

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 841
    :cond_28
    const-string v0, "ServiceModeApp_RIL"

    const-string v1, "DUMP_OEMHOOK"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 842
    const/4 v7, 0x0

    .line 843
    .local v7, "data":[B
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->DoCpCrash()[B

    move-result-object v7

    .line 844
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3f2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-direct {p0, v7, v0}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto/16 :goto_0

    .line 846
    .end local v7    # "data":[B
    .end local v12    # "temp":Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;
    :cond_29
    const-string v0, "758353266223"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 847
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x3a

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 849
    :cond_2a
    const-string v0, "1234567890"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 850
    const-string v0, "ServiceModeApp_RIL"

    const-string v1, "1234567890"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 851
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x3b

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 853
    :cond_2b
    const-string v0, "119"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 854
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x3c

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 856
    :cond_2c
    const-string v0, "33725327"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 857
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x1027

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto/16 :goto_0

    .line 860
    :cond_2d
    const-string v0, "ServiceModeApp_RIL"

    const-string v1, "err -strange value"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private TmpFixFunc()Z
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 525
    const-string v1, "Automatic["

    .line 526
    .local v1, "CpBug":Ljava/lang/String;
    const-string v2, "[1] WCDMA 850"

    .line 527
    .local v2, "CpBug1":Ljava/lang/String;
    const-string v3, "[1] GSM 850"

    .line 528
    .local v3, "CpBug2":Ljava/lang/String;
    const-string v0, "[2]"

    .line 530
    .local v0, "CheckSecondLine":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;

    aget-object v6, v6, v4

    invoke-virtual {v6, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;

    aget-object v6, v6, v4

    invoke-virtual {v6, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;

    aget-object v6, v6, v4

    invoke-virtual {v6, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 532
    :cond_0
    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;

    aget-object v6, v6, v5

    invoke-virtual {v6, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 533
    const-string v4, "ServiceModeApp_RIL"

    const-string v6, "tmp code : need to refresh"

    invoke-static {v4, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v5

    .line 539
    :cond_1
    return v4
.end method

.method private Update()V
    .locals 3

    .prologue
    .line 416
    const-string v0, "ServiceModeApp_RIL"

    const-string v1, "Update!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->arrayAdapterString:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 418
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->arrayAdapterString:Landroid/widget/ArrayAdapter;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 419
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->arrayAdapterString:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 421
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->roamingfreqchange:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 422
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/ServiceModeApp;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->calledByKeyPress:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;C)C
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/ServiceModeApp;
    .param p1, "x1"    # C

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->ParseKeyValueForModem(C)C

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->AnalisysString()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)C
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    .prologue
    .line 70
    iget-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->currentSVMode:C

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)C
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    .prologue
    .line 70
    iget-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;CCCCCC)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/ServiceModeApp;
    .param p1, "x1"    # C
    .param p2, "x2"    # C
    .param p3, "x3"    # C
    .param p4, "x4"    # C
    .param p5, "x5"    # C
    .param p6, "x6"    # C

    .prologue
    .line 70
    invoke-direct/range {p0 .. p6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    return-void
.end method

.method static synthetic access$802(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;Landroid/os/Messenger;)Landroid/os/Messenger;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/ServiceModeApp;
    .param p1, "x1"    # Landroid/os/Messenger;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mServiceMessenger:Landroid/os/Messenger;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->StartServiceMode()V

    return-void
.end method

.method private connectToRilService()V
    .locals 6

    .prologue
    .line 625
    const-string v3, "ServiceModeApp_RIL"

    const-string v4, "connect To Ril service"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 626
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 628
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "init.svc.ril-daemon2"

    const-string v4, "stopped"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 631
    .local v2, "strRilDaemon2Status":Ljava/lang/String;
    const-string v3, "ServiceModeApp_RIL"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "connectToRilService model: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->model:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 633
    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v3, 0x4

    iput-char v3, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    .line 634
    const-string v3, "TESTMODE"

    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 635
    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v3, 0x2

    iput-char v3, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    .line 636
    const-string v3, "com.sec.phone"

    const-string v4, "com.sec.phone.SecPhoneService"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 665
    :goto_0
    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {p0, v1, v3, v4}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 666
    return-void

    .line 638
    :cond_0
    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->model:Ljava/lang/String;

    const-string v4, "sm-g350"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->model:Ljava/lang/String;

    const-string v4, "sm-g313"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->model:Ljava/lang/String;

    const-string v4, "gt-s5312"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->model:Ljava/lang/String;

    const-string v4, "sm-g110"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->model:Ljava/lang/String;

    const-string v4, "sm-g130"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "gt-s5282"

    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->model:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "gt-s7262"

    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->model:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 640
    :cond_1
    const-string v3, "running"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "00112"

    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "22632"

    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 641
    :cond_2
    const-string v3, "ServiceModeApp_RIL"

    const-string v4, "connect to SecPhoneService2"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    const-string v3, "com.sec.phone"

    const-string v4, "com.sec.phone.SecPhoneService2"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 644
    :cond_3
    const-string v3, "com.sec.phone"

    const-string v4, "com.sec.phone.SecPhoneService"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 647
    :cond_4
    const-string v3, "running"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 648
    const-string v3, "NONE"

    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->modemBoard2:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->chipname:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->modemBoard2:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 649
    :cond_5
    const-string v3, "com.sec.phone"

    const-string v4, "com.sec.phone.SecPhoneService"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 651
    :cond_6
    const-string v3, "/sys/class/sec/switch/uart_sel"

    invoke-direct {p0, v3}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 652
    .local v0, "currentUart":Ljava/lang/String;
    const-string v3, "ESC"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string v3, "MODEM2"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string v3, "CP2"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 654
    :cond_7
    const-string v3, "com.sec.phone"

    const-string v4, "com.sec.phone.SecPhoneService2"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 655
    const-string v3, "ServiceModeApp_RIL"

    const-string v4, "ZK com.sec.phone.SecPhoneService2"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 657
    :cond_8
    const-string v3, "com.sec.phone"

    const-string v4, "com.sec.phone.SecPhoneService"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 658
    const-string v3, "ServiceModeApp_RIL"

    const-string v4, "ZK com.sec.phone.SecPhoneService"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 662
    .end local v0    # "currentUart":Ljava/lang/String;
    :cond_9
    const-string v3, "com.sec.phone"

    const-string v4, "com.sec.phone.SecPhoneService"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0
.end method

.method private invokeOemRilRequestRaw([BLandroid/os/Message;)V
    .locals 3
    .param p1, "data"    # [B
    .param p2, "response"    # Landroid/os/Message;

    .prologue
    .line 669
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 670
    .local v0, "req":Landroid/os/Bundle;
    const-string v1, "request"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 672
    invoke-virtual {p2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 673
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mSvcModeMessenger:Landroid/os/Messenger;

    iput-object v1, p2, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 676
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mServiceMessenger:Landroid/os/Messenger;

    if-eqz v1, :cond_0

    .line 677
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mServiceMessenger:Landroid/os/Messenger;

    invoke-virtual {v1, p2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V

    .line 682
    :goto_0
    return-void

    .line 679
    :cond_0
    const-string v1, "ServiceModeApp_RIL"

    const-string v2, "mServiceMessenger is null. Do nothing."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 680
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static isKeyStringBlocked()Z
    .locals 5

    .prologue
    .line 497
    const/4 v1, 0x0

    .line 499
    .local v1, "imeiBlocked":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/io/File;

    const-string v3, "/efs/FactoryApp/keystr"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x20

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 500
    const-string v2, "isKeyStringBlocked"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 505
    :goto_0
    const-string v2, "ON"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 506
    const-string v2, "isKeyStringBlocked"

    const-string v3, "return true"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    const/4 v2, 0x1

    .line 510
    :goto_1
    return v2

    .line 501
    :catch_0
    move-exception v0

    .line 502
    .local v0, "e1":Ljava/io/IOException;
    const-string v1, "OFF"

    .line 503
    const-string v2, "FactorySupport"

    const-string v3, "cannot open file : /efs/FactoryApp/keystr "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 509
    .end local v0    # "e1":Ljava/io/IOException;
    :cond_0
    const-string v2, "isKeyStringBlocked"

    const-string v3, "return false"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private readOneLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 1219
    const-string v6, ""

    .line 1220
    .local v6, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1221
    .local v0, "buf":Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 1223
    .local v4, "fr":Ljava/io/FileReader;
    :try_start_0
    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1224
    .end local v4    # "fr":Ljava/io/FileReader;
    .local v5, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    const/16 v7, 0x1fa0

    invoke-direct {v1, v5, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1225
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    if-eqz v1, :cond_2

    .line 1226
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .line 1227
    if-nez v6, :cond_2

    .line 1228
    const-string v7, ""
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1239
    if-eqz v5, :cond_0

    .line 1240
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 1241
    :cond_0
    if-eqz v1, :cond_1

    .line 1242
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_1
    :goto_0
    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 1251
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :goto_1
    return-object v7

    .line 1243
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_0
    move-exception v2

    .line 1244
    .local v2, "e":Ljava/io/IOException;
    const-string v8, "ServiceModeApp_RIL"

    const-string v9, "IOException close()"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1245
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1239
    .end local v2    # "e":Ljava/io/IOException;
    :cond_2
    if-eqz v5, :cond_3

    .line 1240
    :try_start_4
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 1241
    :cond_3
    if-eqz v1, :cond_4

    .line 1242
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_4
    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 1248
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_5
    :goto_2
    if-nez v6, :cond_a

    .line 1249
    const-string v7, ""

    goto :goto_1

    .line 1243
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_1
    move-exception v2

    .line 1244
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v7, "ServiceModeApp_RIL"

    const-string v8, "IOException close()"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1245
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 1247
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_2

    .line 1231
    .end local v2    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v3

    .line 1232
    .local v3, "ex":Ljava/io/FileNotFoundException;
    :goto_3
    :try_start_5
    const-string v7, "ServiceModeApp_RIL"

    const-string v8, "FileNotFoundException"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1233
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1239
    if-eqz v4, :cond_6

    .line 1240
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 1241
    :cond_6
    if-eqz v0, :cond_5

    .line 1242
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_2

    .line 1243
    :catch_3
    move-exception v2

    .line 1244
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v7, "ServiceModeApp_RIL"

    const-string v8, "IOException close()"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1245
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 1234
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "ex":Ljava/io/FileNotFoundException;
    :catch_4
    move-exception v2

    .line 1235
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_4
    :try_start_7
    const-string v7, "ServiceModeApp_RIL"

    const-string v8, "IOException"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1236
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1239
    if-eqz v4, :cond_7

    .line 1240
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 1241
    :cond_7
    if-eqz v0, :cond_5

    .line 1242
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    goto :goto_2

    .line 1243
    :catch_5
    move-exception v2

    .line 1244
    const-string v7, "ServiceModeApp_RIL"

    const-string v8, "IOException close()"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1245
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 1238
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 1239
    :goto_5
    if-eqz v4, :cond_8

    .line 1240
    :try_start_9
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 1241
    :cond_8
    if-eqz v0, :cond_9

    .line 1242
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 1246
    :cond_9
    :goto_6
    throw v7

    .line 1243
    :catch_6
    move-exception v2

    .line 1244
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v8, "ServiceModeApp_RIL"

    const-string v9, "IOException close()"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1245
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 1251
    .end local v2    # "e":Ljava/io/IOException;
    :cond_a
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_1

    .line 1238
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v7

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_5

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v7

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_5

    .line 1234
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_7
    move-exception v2

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_4

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_8
    move-exception v2

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_4

    .line 1231
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_9
    move-exception v3

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_a
    move-exception v3

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 895
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 897
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v4, 0x32

    if-ge v0, v4, :cond_0

    .line 898
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;

    const-string v5, ""

    aput-object v5, v4, v0

    .line 897
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 901
    :cond_0
    new-instance v4, Landroid/widget/ArrayAdapter;

    const v5, 0x7f03000d

    const v6, 0x7f08003d

    new-instance v7, Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {v4, p0, v5, v6, v7}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->arrayAdapterString:Landroid/widget/ArrayAdapter;

    .line 905
    const/4 v4, 0x0

    iput v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->level:I

    .line 907
    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 908
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "keyString"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    .line 909
    const-string v4, "ServiceModeApp_RIL"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "keyString is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 910
    const-string v4, "ServiceModeApp_RIL"

    const-string v5, "onCreate"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 912
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    if-nez v4, :cond_1

    .line 913
    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->finish()V

    .line 916
    :cond_1
    const v4, 0x7f030007

    invoke-virtual {p0, v4}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->setContentView(I)V

    .line 917
    const v4, 0x7f080010

    invoke-virtual {p0, v4}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->roamingfreqchange:Landroid/widget/Button;

    .line 918
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->roamingfreqchange:Landroid/widget/Button;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 920
    new-instance v4, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;-><init>(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;)V

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    .line 921
    const v4, 0x7f08000f

    invoke-virtual {p0, v4}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ListView;

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->list:Landroid/widget/ListView;

    .line 922
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->list:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->arrayAdapterString:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 925
    const-string v4, "power"

    invoke-virtual {p0, v4}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    .line 926
    .local v3, "pm":Landroid/os/PowerManager;
    const v4, 0x20000006

    const-string v5, "svcModeApp"

    invoke-virtual {v3, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 930
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->model:Ljava/lang/String;

    const-string v5, "e210l"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 931
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    iget-char v5, v4, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SERVM_FUNCTAG:C

    add-int/lit8 v5, v5, 0x64

    int-to-char v5, v5

    iput-char v5, v4, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SERVM_FUNCTAG:C

    .line 932
    const-string v4, "ServiceModeApp_RIL"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onCreate e210l(mOem.OEM_SERVM_FUNCTAG): "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    iget-char v6, v6, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SERVM_FUNCTAG:C

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 938
    :cond_2
    const-string v4, "ServiceModeApp_RIL"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onCreate commnon mOem.OEM_SERVM_FUNCTAG: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    iget-char v6, v6, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SERVM_FUNCTAG:C

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 942
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v4

    if-nez v4, :cond_3

    .line 943
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 946
    :cond_3
    const-string v2, "Right-hardkey :Back.   Please use the Menukey."

    .line 948
    .local v2, "msg":Ljava/lang/String;
    const/4 v4, 0x1

    invoke-static {p0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 950
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->list:Landroid/widget/ListView;

    new-instance v5, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$4;

    invoke-direct {v5, p0}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$4;-><init>(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)V

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 998
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1025
    const-string v0, "ServiceModeApp_RIL"

    const-string v1, "onCreateOptionsMenu occur"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1027
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 1029
    const-string v0, "276633683782"

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1030
    const-string v0, "END"

    invoke-interface {p1, v2, v3, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1031
    const-string v0, "Back"

    invoke-interface {p1, v2, v4, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1032
    const-string v0, "Help"

    invoke-interface {p1, v2, v5, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1033
    const-string v0, "WiFi"

    invoke-interface {p1, v2, v6, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1043
    :goto_0
    return v3

    .line 1035
    :cond_0
    const-string v0, "END"

    invoke-interface {p1, v2, v3, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1036
    const-string v0, "Back"

    invoke-interface {p1, v2, v4, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1037
    const-string v0, "Key Input"

    invoke-interface {p1, v2, v5, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1038
    const-string v0, "Select"

    invoke-interface {p1, v2, v6, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1039
    const/4 v0, 0x5

    const-string v1, "Help"

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1040
    const/4 v0, 0x6

    const-string v1, "WiFi"

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 1257
    const-string v1, "ServiceModeApp_RIL"

    const-string v2, "onDestroy start"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1258
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3e9

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1260
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    if-eqz v1, :cond_0

    .line 1261
    const-string v1, "ServiceModeApp_RIL"

    const-string v2, "unbind connection"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1262
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v1}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->unbindService(Landroid/content/ServiceConnection;)V

    .line 1263
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mServiceMessenger:Landroid/os/Messenger;

    .line 1264
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1270
    :cond_0
    :goto_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 1271
    const-string v1, "ServiceModeApp_RIL"

    const-string v2, "onDestroy finish"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1272
    return-void

    .line 1266
    :catch_0
    move-exception v0

    .line 1267
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x0

    .line 1193
    sparse-switch p1, :sswitch_data_0

    .line 1213
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 1196
    :sswitch_0
    const-string v0, "ServiceModeApp_RIL"

    const-string v1, "onKeyDown : "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1197
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->EndServiceMode()V

    goto :goto_0

    .line 1202
    :sswitch_1
    const-string v0, "ServiceModeApp_RIL"

    const-string v1, "KEYCODE_RIGHT"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1204
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1205
    const/16 v4, 0x5c

    .line 1206
    .local v4, "key":C
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x3

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    move v3, v2

    move v5, v2

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    .line 1208
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->calledByKeyPress:Z

    goto :goto_0

    .line 1193
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x4 -> :sswitch_0
        0x16 -> :sswitch_1
    .end sparse-switch
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 13
    .param p1, "featureId"    # I
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    const v12, 0x7f060009

    const/4 v1, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 1048
    invoke-interface {p2}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v3, "END"

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1049
    const-string v0, "ServiceModeApp_RIL"

    const-string v1, "End Button clicked"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1050
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->EndServiceMode()V

    .line 1188
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v2

    :cond_1
    return v2

    .line 1053
    :cond_2
    invoke-interface {p2}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v3, "Back"

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1054
    const-string v0, "ServiceModeApp_RIL"

    const-string v3, "Back Button clicked"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1055
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x3e9

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1056
    const/16 v4, 0x5c

    .line 1059
    .local v4, "key":C
    const-string v0, "276633683782"

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1060
    iget v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->level:I

    if-lez v0, :cond_0

    .line 1061
    iget v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->level:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->level:I

    .line 1062
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    move v3, v2

    move v5, v2

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto :goto_0

    .line 1066
    :cond_3
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-char v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C

    move-object v0, p0

    move v3, v2

    move v5, v2

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V

    goto :goto_0

    .line 1069
    .end local v4    # "key":C
    :cond_4
    invoke-interface {p2}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "Key Input"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1070
    const/4 v11, 0x0

    .line 1072
    .local v11, "textEntryView":Landroid/view/View;
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030002

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    .line 1073
    if-eqz v11, :cond_1

    .line 1077
    const v0, 0x7f08000b

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/EditText;

    .line 1078
    .local v9, "input":Landroid/widget/EditText;
    if-eqz v9, :cond_1

    .line 1083
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f060001

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f060002

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f060003

    invoke-virtual {p0, v1}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$5;

    invoke-direct {v2, p0, v9}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$5;-><init>(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;Landroid/widget/EditText;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f060004

    invoke-virtual {p0, v1}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    .line 1117
    .local v7, "a":Landroid/app/AlertDialog;
    invoke-virtual {v7, v11}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    .line 1118
    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 1119
    .end local v7    # "a":Landroid/app/AlertDialog;
    .end local v9    # "input":Landroid/widget/EditText;
    .end local v11    # "textEntryView":Landroid/view/View;
    :cond_5
    invoke-interface {p2}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "Select"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1120
    const/4 v11, 0x0

    .line 1122
    .restart local v11    # "textEntryView":Landroid/view/View;
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030003

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    .line 1123
    if-eqz v11, :cond_1

    .line 1127
    const v0, 0x7f08000c

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/EditText;

    .line 1128
    .local v10, "select":Landroid/widget/EditText;
    if-eqz v10, :cond_1

    .line 1132
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f060005

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f060006

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f060007

    invoke-virtual {p0, v1}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$6;

    invoke-direct {v2, p0, v10}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$6;-><init>(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;Landroid/widget/EditText;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f060008

    invoke-virtual {p0, v1}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    .line 1164
    .restart local v7    # "a":Landroid/app/AlertDialog;
    invoke-virtual {v7, v11}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    .line 1165
    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 1166
    .end local v7    # "a":Landroid/app/AlertDialog;
    .end local v10    # "select":Landroid/widget/EditText;
    .end local v11    # "textEntryView":Landroid/view/View;
    :cond_6
    invoke-interface {p2}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "Help"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1167
    const/4 v11, 0x0

    .line 1169
    .restart local v11    # "textEntryView":Landroid/view/View;
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030005

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    .line 1171
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v12}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v12}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f06000b

    invoke-virtual {p0, v1}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    .line 1177
    .restart local v7    # "a":Landroid/app/AlertDialog;
    if-eqz v11, :cond_7

    .line 1179
    invoke-virtual {v7, v11}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    .line 1181
    :cond_7
    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 1182
    .end local v7    # "a":Landroid/app/AlertDialog;
    .end local v11    # "textEntryView":Landroid/view/View;
    :cond_8
    invoke-interface {p2}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "WiFi"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1183
    new-instance v8, Landroid/content/Intent;

    const-string v0, "com.android.sec.WIFIINFO"

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1184
    .local v8, "i":Landroid/content/Intent;
    invoke-virtual {p0, v8}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 1010
    const-string v0, "ServiceModeApp_RIL"

    const-string v1, "onPause occur"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1012
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 1014
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1015
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1017
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mIsActive:Z

    if-eqz v0, :cond_1

    .line 1018
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->EndServiceMode()V

    .line 1020
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->isRoamingFreq:Z

    .line 1021
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 1002
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 1003
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mIsActive:Z

    .line 1004
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->connectToRilService()V

    .line 1005
    return-void
.end method
