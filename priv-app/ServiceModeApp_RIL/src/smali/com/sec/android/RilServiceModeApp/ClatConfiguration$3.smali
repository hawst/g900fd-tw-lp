.class Lcom/sec/android/RilServiceModeApp/ClatConfiguration$3;
.super Landroid/os/Handler;
.source "ClatConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/RilServiceModeApp/ClatConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;


# direct methods
.method constructor <init>(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$3;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x0

    .line 163
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 193
    # getter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$700()Ljava/lang/String;

    move-result-object v1

    const-string v2, "unknown event"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    :goto_0
    return-void

    .line 165
    :pswitch_0
    # getter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$700()Ljava/lang/String;

    move-result-object v1

    const-string v2, "EVENT_DATA_RECONNECT"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$3;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # getter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mWaitDiag:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$800(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-nez v1, :cond_0

    .line 167
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$3;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    new-instance v2, Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$3;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # getter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$300(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mWaitDiag:Landroid/app/ProgressDialog;
    invoke-static {v1, v2}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$802(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 171
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$3;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # getter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mWaitDiag:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$800(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Landroid/app/ProgressDialog;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$3;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # getter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$300(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0600b4

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 172
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$3;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # getter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mWaitDiag:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$800(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Landroid/app/ProgressDialog;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 173
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$3;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # getter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mWaitDiag:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$800(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Landroid/app/ProgressDialog;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 174
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$3;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # getter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mWaitDiag:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$800(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 175
    :catch_0
    move-exception v0

    .line 176
    .local v0, "e":Landroid/view/WindowManager$BadTokenException;
    invoke-virtual {v0}, Landroid/view/WindowManager$BadTokenException;->printStackTrace()V

    goto :goto_0

    .line 180
    .end local v0    # "e":Landroid/view/WindowManager$BadTokenException;
    :pswitch_1
    # getter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$700()Ljava/lang/String;

    move-result-object v1

    const-string v2, "EVENT_DATA_RECONNECT_DONE"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$3;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # getter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mWaitDiag:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$800(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$3;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # getter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mWaitDiag:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$800(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$3;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # getter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mWaitDiag:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$800(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 183
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$3;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # getter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mWaitDiag:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$800(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 185
    :cond_1
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$3;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # getter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$300(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "Configuration is applied. Plz enable data connection."

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 189
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$3;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # setter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mWaitDiag:Landroid/app/ProgressDialog;
    invoke-static {v1, v4}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$802(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    goto/16 :goto_0

    .line 186
    :catch_1
    move-exception v0

    .line 187
    .restart local v0    # "e":Landroid/view/WindowManager$BadTokenException;
    :try_start_2
    invoke-virtual {v0}, Landroid/view/WindowManager$BadTokenException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 189
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$3;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # setter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mWaitDiag:Landroid/app/ProgressDialog;
    invoke-static {v1, v4}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$802(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    goto/16 :goto_0

    .end local v0    # "e":Landroid/view/WindowManager$BadTokenException;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$3;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # setter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mWaitDiag:Landroid/app/ProgressDialog;
    invoke-static {v2, v4}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$802(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    throw v1

    .line 163
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
