.class Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$12;
.super Ljava/lang/Object;
.source "Sec_Ril_Dump.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->changeDataRecoveryTime()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

.field final synthetic val$msg:Landroid/os/Message;


# direct methods
.method constructor <init>(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 1428
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$12;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    iput-object p2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$12;->val$msg:Landroid/os/Message;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 1430
    const/4 v0, 0x1

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->dataRecoveryTime:I
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$602(I)I

    .line 1431
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$12;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    invoke-virtual {v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "data_stall_alarm_aggressive_delay_in_ms"

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->dataRecoveryTime:I
    invoke-static {}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$600()I

    move-result v2

    const v3, 0xea60

    mul-int/2addr v2, v3

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1434
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$12;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Rebbot Device\n\nDATA_STALL_ALARM_AGGRESSIVE_DELAY_IN_MS will set to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->dataRecoveryTime:I
    invoke-static {}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$600()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "min"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->changeDataRecoveryTimeResultMessage(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$5800(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)V

    .line 1437
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$12;->val$msg:Landroid/os/Message;

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1438
    return-void
.end method
