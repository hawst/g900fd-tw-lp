.class Lcom/sec/android/RilServiceModeApp/ClatConfiguration$2;
.super Ljava/lang/Object;
.source "ClatConfiguration.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/RilServiceModeApp/ClatConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;


# direct methods
.method constructor <init>(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$2;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 134
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$2;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # getter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mUpdateBtn:Landroid/widget/Button;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$000(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_2

    .line 135
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$2;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # invokes: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->replaceConfigures()Z
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$100(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Z

    move-result v1

    .line 136
    .local v1, "isChanged":Z
    if-eqz v1, :cond_1

    .line 137
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$2;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    iget-object v2, v2, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mCm:Landroid/net/ConnectivityManager;

    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 138
    .local v0, "dataNetwork":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 139
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$2;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # getter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mProgressHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$200(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 140
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$2;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    iget-object v2, v2, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2, v4}, Landroid/telephony/TelephonyManager;->setDataEnabled(Z)V

    .line 141
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$2;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    iget-object v2, v2, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2, v3}, Landroid/telephony/TelephonyManager;->setDataEnabled(Z)V

    .line 142
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$2;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # getter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mProgressHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$200(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Landroid/os/Handler;

    move-result-object v2

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 149
    .end local v0    # "dataNetwork":Landroid/net/NetworkInfo;
    :goto_0
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$2;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # invokes: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->readConfigures()V
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$400(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)V

    .line 157
    .end local v1    # "isChanged":Z
    :goto_1
    return-void

    .line 144
    .restart local v0    # "dataNetwork":Landroid/net/NetworkInfo;
    .restart local v1    # "isChanged":Z
    :cond_0
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$2;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # getter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$300(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "Configuration is applied. Plz enable data connection."

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 147
    .end local v0    # "dataNetwork":Landroid/net/NetworkInfo;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$2;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # getter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$300(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "Non of configuration is changed."

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 150
    .end local v1    # "isChanged":Z
    :cond_2
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$2;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # getter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mRestoreBtn:Landroid/widget/Button;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$500(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_3

    .line 151
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$2;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # getter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$300(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "Restore clat configuration."

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 152
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$2;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # invokes: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->restoreConfigures()V
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$600(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)V

    .line 153
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$2;->this$0:Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    # invokes: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->readConfigures()V
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$400(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)V

    goto :goto_1

    .line 155
    :cond_3
    # getter for: Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->access$700()Ljava/lang/String;

    move-result-object v2

    const-string v3, "OnClickListener(): default handler"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
