.class Lcom/sec/android/RilServiceModeApp/ViewRilLog$2;
.super Ljava/lang/Object;
.source "ViewRilLog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/RilServiceModeApp/ViewRilLog;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/RilServiceModeApp/ViewRilLog;


# direct methods
.method constructor <init>(Lcom/sec/android/RilServiceModeApp/ViewRilLog;)V
    .locals 0

    .prologue
    .line 201
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog$2;->this$0:Lcom/sec/android/RilServiceModeApp/ViewRilLog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 203
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 204
    .local v3, "rilLogMessage":Ljava/lang/StringBuilder;
    const-string v2, ""

    .line 205
    .local v2, "rilLogLine":Ljava/lang/String;
    const/4 v1, 0x0

    .line 208
    .local v1, "i":I
    :cond_0
    :goto_0
    :try_start_0
    # getter for: Lcom/sec/android/RilServiceModeApp/ViewRilLog;->in:Ljava/io/RandomAccessFile;
    invoke-static {}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->access$200()Ljava/io/RandomAccessFile;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 209
    add-int/lit8 v1, v1, 0x1

    .line 211
    # getter for: Lcom/sec/android/RilServiceModeApp/ViewRilLog;->in:Ljava/io/RandomAccessFile;
    invoke-static {}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->access$200()Ljava/io/RandomAccessFile;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v4

    const-wide/16 v6, 0x1b58

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    .line 212
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 225
    :catch_0
    move-exception v0

    .line 226
    .local v0, "e":Ljava/io/FileNotFoundException;
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog$2;->this$0:Lcom/sec/android/RilServiceModeApp/ViewRilLog;

    const-string v5, "File open fail!"

    # invokes: Lcom/sec/android/RilServiceModeApp/ViewRilLog;->DisplayMessageDialog(Ljava/lang/String;)V
    invoke-static {v4, v5}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->access$400(Lcom/sec/android/RilServiceModeApp/ViewRilLog;Ljava/lang/String;)V

    .line 227
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog$2;->this$0:Lcom/sec/android/RilServiceModeApp/ViewRilLog;

    # getter for: Lcom/sec/android/RilServiceModeApp/ViewRilLog;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {v4}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->access$500(Lcom/sec/android/RilServiceModeApp/ViewRilLog;)Landroid/app/ProgressDialog;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/ProgressDialog;->dismiss()V

    .line 228
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 236
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog$2;->this$0:Lcom/sec/android/RilServiceModeApp/ViewRilLog;

    # getter for: Lcom/sec/android/RilServiceModeApp/ViewRilLog;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {v4}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->access$500(Lcom/sec/android/RilServiceModeApp/ViewRilLog;)Landroid/app/ProgressDialog;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/ProgressDialog;->dismiss()V

    .line 237
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog$2;->this$0:Lcom/sec/android/RilServiceModeApp/ViewRilLog;

    # invokes: Lcom/sec/android/RilServiceModeApp/ViewRilLog;->sendParsedString(Ljava/lang/StringBuilder;)V
    invoke-static {v4, v3}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->access$600(Lcom/sec/android/RilServiceModeApp/ViewRilLog;Ljava/lang/StringBuilder;)V

    .line 241
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :goto_1
    return-void

    .line 218
    :cond_1
    :try_start_2
    const-string v4, "ViewRilLog"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Last FilePointer: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    # getter for: Lcom/sec/android/RilServiceModeApp/ViewRilLog;->in:Ljava/io/RandomAccessFile;
    invoke-static {}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->access$200()Ljava/io/RandomAccessFile;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    # getter for: Lcom/sec/android/RilServiceModeApp/ViewRilLog;->in:Ljava/io/RandomAccessFile;
    invoke-static {}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->access$200()Ljava/io/RandomAccessFile;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v4

    long-to-int v4, v4

    div-int/lit16 v4, v4, 0x1b58

    add-int/lit8 v4, v4, 0x1

    # setter for: Lcom/sec/android/RilServiceModeApp/ViewRilLog;->totalPages:I
    invoke-static {v4}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->access$302(I)I

    .line 222
    const-string v4, "ViewRilLog"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "totalPages = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    # getter for: Lcom/sec/android/RilServiceModeApp/ViewRilLog;->totalPages:I
    invoke-static {}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->access$300()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    # getter for: Lcom/sec/android/RilServiceModeApp/ViewRilLog;->in:Ljava/io/RandomAccessFile;
    invoke-static {}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->access$200()Ljava/io/RandomAccessFile;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 236
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog$2;->this$0:Lcom/sec/android/RilServiceModeApp/ViewRilLog;

    # getter for: Lcom/sec/android/RilServiceModeApp/ViewRilLog;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {v4}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->access$500(Lcom/sec/android/RilServiceModeApp/ViewRilLog;)Landroid/app/ProgressDialog;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/ProgressDialog;->dismiss()V

    .line 237
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog$2;->this$0:Lcom/sec/android/RilServiceModeApp/ViewRilLog;

    # invokes: Lcom/sec/android/RilServiceModeApp/ViewRilLog;->sendParsedString(Ljava/lang/StringBuilder;)V
    invoke-static {v4, v3}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->access$600(Lcom/sec/android/RilServiceModeApp/ViewRilLog;Ljava/lang/StringBuilder;)V

    goto :goto_1

    .line 229
    :catch_1
    move-exception v0

    .line 230
    .local v0, "e":Ljava/io/IOException;
    :try_start_3
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog$2;->this$0:Lcom/sec/android/RilServiceModeApp/ViewRilLog;

    const-string v5, "File open fail!"

    # invokes: Lcom/sec/android/RilServiceModeApp/ViewRilLog;->DisplayMessageDialog(Ljava/lang/String;)V
    invoke-static {v4, v5}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->access$400(Lcom/sec/android/RilServiceModeApp/ViewRilLog;Ljava/lang/String;)V

    .line 231
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog$2;->this$0:Lcom/sec/android/RilServiceModeApp/ViewRilLog;

    # getter for: Lcom/sec/android/RilServiceModeApp/ViewRilLog;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {v4}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->access$500(Lcom/sec/android/RilServiceModeApp/ViewRilLog;)Landroid/app/ProgressDialog;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/ProgressDialog;->dismiss()V

    .line 232
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 236
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog$2;->this$0:Lcom/sec/android/RilServiceModeApp/ViewRilLog;

    # getter for: Lcom/sec/android/RilServiceModeApp/ViewRilLog;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {v4}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->access$500(Lcom/sec/android/RilServiceModeApp/ViewRilLog;)Landroid/app/ProgressDialog;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/ProgressDialog;->dismiss()V

    .line 237
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog$2;->this$0:Lcom/sec/android/RilServiceModeApp/ViewRilLog;

    # invokes: Lcom/sec/android/RilServiceModeApp/ViewRilLog;->sendParsedString(Ljava/lang/StringBuilder;)V
    invoke-static {v4, v3}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->access$600(Lcom/sec/android/RilServiceModeApp/ViewRilLog;Ljava/lang/StringBuilder;)V

    goto/16 :goto_1

    .line 236
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog$2;->this$0:Lcom/sec/android/RilServiceModeApp/ViewRilLog;

    # getter for: Lcom/sec/android/RilServiceModeApp/ViewRilLog;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->access$500(Lcom/sec/android/RilServiceModeApp/ViewRilLog;)Landroid/app/ProgressDialog;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/ProgressDialog;->dismiss()V

    .line 237
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog$2;->this$0:Lcom/sec/android/RilServiceModeApp/ViewRilLog;

    # invokes: Lcom/sec/android/RilServiceModeApp/ViewRilLog;->sendParsedString(Ljava/lang/StringBuilder;)V
    invoke-static {v5, v3}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->access$600(Lcom/sec/android/RilServiceModeApp/ViewRilLog;Ljava/lang/StringBuilder;)V

    throw v4
.end method
