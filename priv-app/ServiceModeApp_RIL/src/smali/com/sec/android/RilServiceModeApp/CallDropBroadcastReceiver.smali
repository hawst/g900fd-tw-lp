.class public Lcom/sec/android/RilServiceModeApp/CallDropBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "CallDropBroadcastReceiver.java"


# instance fields
.field private final CALLDROP_FILEPATH:Ljava/lang/String;

.field private final CALLDROP_NOSVC_FILEPATH:Ljava/lang/String;

.field private final CALL_DROP:Ljava/lang/String;

.field private final MAX_SAVE_COUNT:I

.field private final TAG:Ljava/lang/String;

.field private allCallDrop_info:[Ljava/lang/String;

.field private isFileExist:Z

.field private newCallDrop_EventInfo:Ljava/lang/String;

.field private newCallDrop_SaveInfo:Ljava/lang/String;

.field private processChModString:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x28

    .line 21
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 23
    const-string v0, "CallDropBroadcastReceiver"

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropBroadcastReceiver;->TAG:Ljava/lang/String;

    .line 24
    const-string v0, "com.android.action.CALL_DROP"

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropBroadcastReceiver;->CALL_DROP:Ljava/lang/String;

    .line 25
    const-string v0, "data/log/CallDropInfoLog.txt"

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropBroadcastReceiver;->CALLDROP_FILEPATH:Ljava/lang/String;

    .line 26
    const-string v0, "data/log/CallNosvcInfoLog.txt"

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropBroadcastReceiver;->CALLDROP_NOSVC_FILEPATH:Ljava/lang/String;

    .line 28
    iput v1, p0, Lcom/sec/android/RilServiceModeApp/CallDropBroadcastReceiver;->MAX_SAVE_COUNT:I

    .line 34
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropBroadcastReceiver;->allCallDrop_info:[Ljava/lang/String;

    return-void
.end method

.method private getPopupEvent(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "code"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 154
    const/4 v1, 0x0

    .line 156
    .local v1, "eventCode":I
    :try_start_0
    const-string v3, ","

    invoke-virtual {p2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 161
    :goto_0
    const-string v3, "CallDropBroadcastReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getPopupEvent: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    const/4 v2, 0x0

    .line 164
    .local v2, "reasonText":Ljava/lang/String;
    sparse-switch v1, :sswitch_data_0

    .line 188
    const-string v2, "NoPopup"

    .line 191
    :cond_0
    :goto_1
    return-object v2

    .line 157
    .end local v2    # "reasonText":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 158
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "CallDropBroadcastReceiver"

    const-string v4, "getPopupEvent-split error"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 166
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "reasonText":Ljava/lang/String;
    :sswitch_0
    const v3, 0x7f060091

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 167
    goto :goto_1

    .line 170
    :sswitch_1
    const v3, 0x7f060092

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 171
    goto :goto_1

    .line 174
    :sswitch_2
    const v3, 0x7f060093

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 175
    goto :goto_1

    .line 178
    :sswitch_3
    const v3, 0x7f060094

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 179
    const-string v3, ","

    invoke-virtual {p2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v6

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/16 v4, 0x67

    if-ne v3, v4, :cond_1

    .line 180
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f060095

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 182
    :cond_1
    const-string v3, ","

    invoke-virtual {p2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v6

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/16 v4, 0x68

    if-ne v3, v4, :cond_0

    .line 183
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f060096

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 164
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x5 -> :sswitch_2
        0xa -> :sswitch_3
    .end sparse-switch
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 26
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 42
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v22

    const-string v23, "com.android.action.CALL_DROP"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_e

    .line 43
    const/4 v7, 0x0

    .line 44
    .local v7, "callDrop_LogFile":Ljava/io/File;
    const/4 v10, 0x0

    .line 45
    .local v10, "fileReader":Ljava/io/FileReader;
    const/4 v4, 0x0

    .line 47
    .local v4, "bufReader":Ljava/io/BufferedReader;
    const/16 v18, 0x0

    .line 48
    .local v18, "selectedFilePath":Ljava/lang/String;
    const/16 v21, 0x0

    .line 50
    .local v21, "totalLine":I
    new-instance v12, Ljava/text/SimpleDateFormat;

    const-string v22, "yyyy.MM.dd HH:mm:ss"

    sget-object v23, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-direct {v12, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 51
    .local v12, "formatter":Ljava/text/SimpleDateFormat;
    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    .line 52
    .local v8, "currentTime":Ljava/util/Date;
    invoke-virtual {v12, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    .line 54
    .local v6, "cTime":Ljava/lang/String;
    const-string v22, "chmod 644 FILEPATH"

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/RilServiceModeApp/CallDropBroadcastReceiver;->processChModString:Ljava/lang/String;

    .line 56
    const-string v22, "calldrop_log"

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/RilServiceModeApp/CallDropBroadcastReceiver;->newCallDrop_EventInfo:Ljava/lang/String;

    .line 57
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/RilServiceModeApp/CallDropBroadcastReceiver;->newCallDrop_EventInfo:Ljava/lang/String;

    move-object/from16 v22, v0

    const-string v23, "23,"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/RilServiceModeApp/CallDropBroadcastReceiver;->newCallDrop_EventInfo:Ljava/lang/String;

    move-object/from16 v22, v0

    const-string v23, "24,"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_1

    .line 58
    :cond_0
    new-instance v7, Ljava/io/File;

    .end local v7    # "callDrop_LogFile":Ljava/io/File;
    const-string v22, "data/log/CallNosvcInfoLog.txt"

    move-object/from16 v0, v22

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 59
    .restart local v7    # "callDrop_LogFile":Ljava/io/File;
    const-string v18, "data/log/CallNosvcInfoLog.txt"

    .line 65
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/RilServiceModeApp/CallDropBroadcastReceiver;->processChModString:Ljava/lang/String;

    move-object/from16 v22, v0

    const-string v23, "FILEPATH"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/RilServiceModeApp/CallDropBroadcastReceiver;->processChModString:Ljava/lang/String;

    .line 66
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ","

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/RilServiceModeApp/CallDropBroadcastReceiver;->newCallDrop_EventInfo:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/RilServiceModeApp/CallDropBroadcastReceiver;->newCallDrop_SaveInfo:Ljava/lang/String;

    .line 67
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/RilServiceModeApp/CallDropBroadcastReceiver;->isFileExist:Z

    .line 69
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/RilServiceModeApp/CallDropBroadcastReceiver;->isFileExist:Z

    move/from16 v22, v0

    if-eqz v22, :cond_5

    .line 71
    const/16 v20, 0x0

    .line 73
    .local v20, "strLine":Ljava/lang/String;
    :try_start_0
    new-instance v11, Ljava/io/FileReader;

    move-object/from16 v0, v18

    invoke-direct {v11, v0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    .end local v10    # "fileReader":Ljava/io/FileReader;
    .local v11, "fileReader":Ljava/io/FileReader;
    :try_start_1
    new-instance v5, Ljava/io/BufferedReader;

    invoke-direct {v5, v11}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_14
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_12
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 76
    .end local v4    # "bufReader":Ljava/io/BufferedReader;
    .local v5, "bufReader":Ljava/io/BufferedReader;
    :goto_1
    :try_start_2
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v20

    if-eqz v20, :cond_2

    .line 77
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/RilServiceModeApp/CallDropBroadcastReceiver;->allCallDrop_info:[Ljava/lang/String;

    move-object/from16 v22, v0

    aput-object v20, v22, v21
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_15
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_13
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    .line 78
    add-int/lit8 v21, v21, 0x1

    goto :goto_1

    .line 61
    .end local v5    # "bufReader":Ljava/io/BufferedReader;
    .end local v11    # "fileReader":Ljava/io/FileReader;
    .end local v20    # "strLine":Ljava/lang/String;
    .restart local v4    # "bufReader":Ljava/io/BufferedReader;
    .restart local v10    # "fileReader":Ljava/io/FileReader;
    :cond_1
    new-instance v7, Ljava/io/File;

    .end local v7    # "callDrop_LogFile":Ljava/io/File;
    const-string v22, "data/log/CallDropInfoLog.txt"

    move-object/from16 v0, v22

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 62
    .restart local v7    # "callDrop_LogFile":Ljava/io/File;
    const-string v18, "data/log/CallDropInfoLog.txt"

    goto :goto_0

    .line 80
    .end local v4    # "bufReader":Ljava/io/BufferedReader;
    .end local v10    # "fileReader":Ljava/io/FileReader;
    .restart local v5    # "bufReader":Ljava/io/BufferedReader;
    .restart local v11    # "fileReader":Ljava/io/FileReader;
    .restart local v20    # "strLine":Ljava/lang/String;
    :cond_2
    :try_start_3
    invoke-virtual {v7}, Ljava/io/File;->delete()Z
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_15
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_13
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 87
    if-eqz v11, :cond_3

    .line 88
    :try_start_4
    invoke-virtual {v11}, Ljava/io/FileReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 95
    :cond_3
    :goto_2
    if-eqz v5, :cond_4

    .line 96
    :try_start_5
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :cond_4
    move-object v4, v5

    .end local v5    # "bufReader":Ljava/io/BufferedReader;
    .restart local v4    # "bufReader":Ljava/io/BufferedReader;
    move-object v10, v11

    .line 104
    .end local v11    # "fileReader":Ljava/io/FileReader;
    .end local v20    # "strLine":Ljava/lang/String;
    .restart local v10    # "fileReader":Ljava/io/FileReader;
    :cond_5
    :goto_3
    const/16 v16, 0x0

    .line 106
    .local v16, "outputStream":Ljava/io/FileOutputStream;
    const/4 v3, 0x0

    .line 108
    .local v3, "addLine":I
    :try_start_6
    new-instance v17, Ljava/io/FileOutputStream;

    const/16 v22, 0x1

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_b
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_d
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 109
    .end local v16    # "outputStream":Ljava/io/FileOutputStream;
    .local v17, "outputStream":Ljava/io/FileOutputStream;
    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/RilServiceModeApp/CallDropBroadcastReceiver;->newCallDrop_SaveInfo:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->getBytes()[B

    move-result-object v22

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 111
    const/16 v22, 0x28

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_a

    .line 112
    const/16 v3, 0x27

    .line 117
    :goto_4
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_5
    if-ge v13, v3, :cond_b

    .line 118
    const-string v22, "\n"

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->getBytes()[B

    move-result-object v22

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 119
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/RilServiceModeApp/CallDropBroadcastReceiver;->allCallDrop_info:[Ljava/lang/String;

    move-object/from16 v22, v0

    aget-object v22, v22, v13

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->getBytes()[B

    move-result-object v22

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_11
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_10
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 117
    add-int/lit8 v13, v13, 0x1

    goto :goto_5

    .line 90
    .end local v3    # "addLine":I
    .end local v4    # "bufReader":Ljava/io/BufferedReader;
    .end local v10    # "fileReader":Ljava/io/FileReader;
    .end local v13    # "i":I
    .end local v17    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v5    # "bufReader":Ljava/io/BufferedReader;
    .restart local v11    # "fileReader":Ljava/io/FileReader;
    .restart local v20    # "strLine":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 91
    .local v9, "e":Ljava/io/IOException;
    const-string v22, "CallDropBroadcastReceiver"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Read fileReader Close IOException: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 98
    .end local v9    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v9

    .line 99
    .restart local v9    # "e":Ljava/io/IOException;
    const-string v22, "CallDropBroadcastReceiver"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Read bufReader Close IOException: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v5

    .end local v5    # "bufReader":Ljava/io/BufferedReader;
    .restart local v4    # "bufReader":Ljava/io/BufferedReader;
    move-object v10, v11

    .line 101
    .end local v11    # "fileReader":Ljava/io/FileReader;
    .restart local v10    # "fileReader":Ljava/io/FileReader;
    goto/16 :goto_3

    .line 81
    .end local v9    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v9

    .line 82
    .local v9, "e":Ljava/io/FileNotFoundException;
    :goto_6
    :try_start_8
    const-string v22, "CallDropBroadcastReceiver"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Read FileNotFoundException: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v9}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 87
    if-eqz v10, :cond_6

    .line 88
    :try_start_9
    invoke-virtual {v10}, Ljava/io/FileReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    .line 95
    .end local v9    # "e":Ljava/io/FileNotFoundException;
    :cond_6
    :goto_7
    if-eqz v4, :cond_5

    .line 96
    :try_start_a
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3

    goto/16 :goto_3

    .line 98
    :catch_3
    move-exception v9

    .line 99
    .local v9, "e":Ljava/io/IOException;
    const-string v22, "CallDropBroadcastReceiver"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Read bufReader Close IOException: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 90
    .local v9, "e":Ljava/io/FileNotFoundException;
    :catch_4
    move-exception v9

    .line 91
    .local v9, "e":Ljava/io/IOException;
    const-string v22, "CallDropBroadcastReceiver"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Read fileReader Close IOException: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 83
    .end local v9    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v9

    .line 84
    .restart local v9    # "e":Ljava/io/IOException;
    :goto_8
    :try_start_b
    const-string v22, "CallDropBroadcastReceiver"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Read IOException: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 87
    if-eqz v10, :cond_7

    .line 88
    :try_start_c
    invoke-virtual {v10}, Ljava/io/FileReader;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_7

    .line 95
    :cond_7
    :goto_9
    if-eqz v4, :cond_5

    .line 96
    :try_start_d
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_6

    goto/16 :goto_3

    .line 98
    :catch_6
    move-exception v9

    .line 99
    const-string v22, "CallDropBroadcastReceiver"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Read bufReader Close IOException: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 90
    :catch_7
    move-exception v9

    .line 91
    const-string v22, "CallDropBroadcastReceiver"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Read fileReader Close IOException: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 86
    .end local v9    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v22

    .line 87
    :goto_a
    if-eqz v10, :cond_8

    .line 88
    :try_start_e
    invoke-virtual {v10}, Ljava/io/FileReader;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_8

    .line 95
    :cond_8
    :goto_b
    if-eqz v4, :cond_9

    .line 96
    :try_start_f
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_9

    .line 100
    :cond_9
    :goto_c
    throw v22

    .line 90
    :catch_8
    move-exception v9

    .line 91
    .restart local v9    # "e":Ljava/io/IOException;
    const-string v23, "CallDropBroadcastReceiver"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Read fileReader Close IOException: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b

    .line 98
    .end local v9    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v9

    .line 99
    .restart local v9    # "e":Ljava/io/IOException;
    const-string v23, "CallDropBroadcastReceiver"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Read bufReader Close IOException: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_c

    .line 114
    .end local v9    # "e":Ljava/io/IOException;
    .end local v20    # "strLine":Ljava/lang/String;
    .restart local v3    # "addLine":I
    .restart local v17    # "outputStream":Ljava/io/FileOutputStream;
    :cond_a
    move/from16 v3, v21

    goto/16 :goto_4

    .line 123
    .restart local v13    # "i":I
    :cond_b
    :try_start_10
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/RilServiceModeApp/CallDropBroadcastReceiver;->processChModString:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_10
    .catch Ljava/io/FileNotFoundException; {:try_start_10 .. :try_end_10} :catch_11
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    .line 130
    if-eqz v17, :cond_c

    .line 131
    :try_start_11
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileOutputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_a

    :cond_c
    move-object/from16 v16, v17

    .line 138
    .end local v13    # "i":I
    .end local v17    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v16    # "outputStream":Ljava/io/FileOutputStream;
    :cond_d
    :goto_d
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v22

    const-string v23, "calldrop_popup"

    const/16 v24, 0x0

    invoke-static/range {v22 .. v24}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v15

    .line 139
    .local v15, "isCalldropPopup":I
    const/16 v22, 0x1

    move/from16 v0, v22

    if-ne v15, v0, :cond_e

    .line 140
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/RilServiceModeApp/CallDropBroadcastReceiver;->newCallDrop_EventInfo:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/sec/android/RilServiceModeApp/CallDropBroadcastReceiver;->getPopupEvent(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 142
    .local v19, "strEventType":Ljava/lang/String;
    const-string v22, "NoPopup"

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_e

    .line 143
    new-instance v14, Landroid/content/Intent;

    invoke-direct {v14}, Landroid/content/Intent;-><init>()V

    .line 144
    .local v14, "iAlert":Landroid/content/Intent;
    const-string v22, "popup_event"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "["

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "] "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v14, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 145
    const-class v22, Lcom/sec/android/RilServiceModeApp/CallDropLogAlertDialog;

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v14, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 146
    const/high16 v22, 0x10000000

    move/from16 v0, v22

    invoke-virtual {v14, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 147
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 151
    .end local v3    # "addLine":I
    .end local v4    # "bufReader":Ljava/io/BufferedReader;
    .end local v6    # "cTime":Ljava/lang/String;
    .end local v7    # "callDrop_LogFile":Ljava/io/File;
    .end local v8    # "currentTime":Ljava/util/Date;
    .end local v10    # "fileReader":Ljava/io/FileReader;
    .end local v12    # "formatter":Ljava/text/SimpleDateFormat;
    .end local v14    # "iAlert":Landroid/content/Intent;
    .end local v15    # "isCalldropPopup":I
    .end local v16    # "outputStream":Ljava/io/FileOutputStream;
    .end local v18    # "selectedFilePath":Ljava/lang/String;
    .end local v19    # "strEventType":Ljava/lang/String;
    .end local v21    # "totalLine":I
    :cond_e
    return-void

    .line 133
    .restart local v3    # "addLine":I
    .restart local v4    # "bufReader":Ljava/io/BufferedReader;
    .restart local v6    # "cTime":Ljava/lang/String;
    .restart local v7    # "callDrop_LogFile":Ljava/io/File;
    .restart local v8    # "currentTime":Ljava/util/Date;
    .restart local v10    # "fileReader":Ljava/io/FileReader;
    .restart local v12    # "formatter":Ljava/text/SimpleDateFormat;
    .restart local v13    # "i":I
    .restart local v17    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v18    # "selectedFilePath":Ljava/lang/String;
    .restart local v21    # "totalLine":I
    :catch_a
    move-exception v9

    .line 134
    .restart local v9    # "e":Ljava/io/IOException;
    const-string v22, "CallDropBroadcastReceiver"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "close IOException: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v16, v17

    .line 136
    .end local v17    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v16    # "outputStream":Ljava/io/FileOutputStream;
    goto/16 :goto_d

    .line 124
    .end local v9    # "e":Ljava/io/IOException;
    .end local v13    # "i":I
    :catch_b
    move-exception v9

    .line 125
    .local v9, "e":Ljava/io/FileNotFoundException;
    :goto_e
    :try_start_12
    const-string v22, "CallDropBroadcastReceiver"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Write FileNotFoundException: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v9}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    .line 130
    if-eqz v16, :cond_d

    .line 131
    :try_start_13
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_c

    goto/16 :goto_d

    .line 133
    :catch_c
    move-exception v9

    .line 134
    .local v9, "e":Ljava/io/IOException;
    const-string v22, "CallDropBroadcastReceiver"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "close IOException: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_d

    .line 126
    .end local v9    # "e":Ljava/io/IOException;
    :catch_d
    move-exception v9

    .line 127
    .restart local v9    # "e":Ljava/io/IOException;
    :goto_f
    :try_start_14
    const-string v22, "CallDropBroadcastReceiver"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Write IOException: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    .line 130
    if-eqz v16, :cond_d

    .line 131
    :try_start_15
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_e

    goto/16 :goto_d

    .line 133
    :catch_e
    move-exception v9

    .line 134
    const-string v22, "CallDropBroadcastReceiver"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "close IOException: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_d

    .line 129
    .end local v9    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v22

    .line 130
    :goto_10
    if-eqz v16, :cond_f

    .line 131
    :try_start_16
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_f

    .line 135
    :cond_f
    :goto_11
    throw v22

    .line 133
    :catch_f
    move-exception v9

    .line 134
    .restart local v9    # "e":Ljava/io/IOException;
    const-string v23, "CallDropBroadcastReceiver"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "close IOException: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_11

    .line 129
    .end local v9    # "e":Ljava/io/IOException;
    .end local v16    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v17    # "outputStream":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v22

    move-object/from16 v16, v17

    .end local v17    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v16    # "outputStream":Ljava/io/FileOutputStream;
    goto :goto_10

    .line 126
    .end local v16    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v17    # "outputStream":Ljava/io/FileOutputStream;
    :catch_10
    move-exception v9

    move-object/from16 v16, v17

    .end local v17    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v16    # "outputStream":Ljava/io/FileOutputStream;
    goto :goto_f

    .line 124
    .end local v16    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v17    # "outputStream":Ljava/io/FileOutputStream;
    :catch_11
    move-exception v9

    move-object/from16 v16, v17

    .end local v17    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v16    # "outputStream":Ljava/io/FileOutputStream;
    goto/16 :goto_e

    .line 86
    .end local v3    # "addLine":I
    .end local v10    # "fileReader":Ljava/io/FileReader;
    .end local v16    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v11    # "fileReader":Ljava/io/FileReader;
    .restart local v20    # "strLine":Ljava/lang/String;
    :catchall_3
    move-exception v22

    move-object v10, v11

    .end local v11    # "fileReader":Ljava/io/FileReader;
    .restart local v10    # "fileReader":Ljava/io/FileReader;
    goto/16 :goto_a

    .end local v4    # "bufReader":Ljava/io/BufferedReader;
    .end local v10    # "fileReader":Ljava/io/FileReader;
    .restart local v5    # "bufReader":Ljava/io/BufferedReader;
    .restart local v11    # "fileReader":Ljava/io/FileReader;
    :catchall_4
    move-exception v22

    move-object v4, v5

    .end local v5    # "bufReader":Ljava/io/BufferedReader;
    .restart local v4    # "bufReader":Ljava/io/BufferedReader;
    move-object v10, v11

    .end local v11    # "fileReader":Ljava/io/FileReader;
    .restart local v10    # "fileReader":Ljava/io/FileReader;
    goto/16 :goto_a

    .line 83
    .end local v10    # "fileReader":Ljava/io/FileReader;
    .restart local v11    # "fileReader":Ljava/io/FileReader;
    :catch_12
    move-exception v9

    move-object v10, v11

    .end local v11    # "fileReader":Ljava/io/FileReader;
    .restart local v10    # "fileReader":Ljava/io/FileReader;
    goto/16 :goto_8

    .end local v4    # "bufReader":Ljava/io/BufferedReader;
    .end local v10    # "fileReader":Ljava/io/FileReader;
    .restart local v5    # "bufReader":Ljava/io/BufferedReader;
    .restart local v11    # "fileReader":Ljava/io/FileReader;
    :catch_13
    move-exception v9

    move-object v4, v5

    .end local v5    # "bufReader":Ljava/io/BufferedReader;
    .restart local v4    # "bufReader":Ljava/io/BufferedReader;
    move-object v10, v11

    .end local v11    # "fileReader":Ljava/io/FileReader;
    .restart local v10    # "fileReader":Ljava/io/FileReader;
    goto/16 :goto_8

    .line 81
    .end local v10    # "fileReader":Ljava/io/FileReader;
    .restart local v11    # "fileReader":Ljava/io/FileReader;
    :catch_14
    move-exception v9

    move-object v10, v11

    .end local v11    # "fileReader":Ljava/io/FileReader;
    .restart local v10    # "fileReader":Ljava/io/FileReader;
    goto/16 :goto_6

    .end local v4    # "bufReader":Ljava/io/BufferedReader;
    .end local v10    # "fileReader":Ljava/io/FileReader;
    .restart local v5    # "bufReader":Ljava/io/BufferedReader;
    .restart local v11    # "fileReader":Ljava/io/FileReader;
    :catch_15
    move-exception v9

    move-object v4, v5

    .end local v5    # "bufReader":Ljava/io/BufferedReader;
    .restart local v4    # "bufReader":Ljava/io/BufferedReader;
    move-object v10, v11

    .end local v11    # "fileReader":Ljava/io/FileReader;
    .restart local v10    # "fileReader":Ljava/io/FileReader;
    goto/16 :goto_6
.end method
