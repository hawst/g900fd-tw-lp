.class Lcom/sec/android/RilServiceModeApp/SetupWizardSkip$1;
.super Ljava/lang/Object;
.source "SetupWizardSkip.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;


# direct methods
.method constructor <init>(Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip$1;->this$0:Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 93
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip$1;->this$0:Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;

    # getter for: Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->mSkipMode:Landroid/widget/Button;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->access$000(Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 94
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip$1;->this$0:Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;

    # getter for: Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->phone:Lcom/android/internal/telephony/Phone;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->access$100(Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;)Lcom/android/internal/telephony/Phone;

    move-result-object v2

    invoke-interface {v2}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 96
    .local v1, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 98
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip$1;->this$0:Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;

    # getter for: Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->bSkipMode:Z
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->access$200(Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;)Z

    move-result v2

    if-ne v2, v4, :cond_1

    .line 99
    const-string v2, "SetupWizardSkipSettings"

    const-string v3, "changed Enable ----> Disable "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    const-string v2, "setup_wizard_skip"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 101
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 103
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip$1;->this$0:Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;

    # getter for: Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->mSkipMode:Landroid/widget/Button;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->access$000(Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;)Landroid/widget/Button;

    move-result-object v2

    const-string v3, "Setup Wizard Skip : Disabled"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 104
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip$1;->this$0:Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;

    # setter for: Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->bSkipMode:Z
    invoke-static {v2, v5}, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->access$202(Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;Z)Z

    .line 114
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "sp":Landroid/content/SharedPreferences;
    :cond_0
    :goto_0
    return-void

    .line 106
    .restart local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .restart local v1    # "sp":Landroid/content/SharedPreferences;
    :cond_1
    const-string v2, "SetupWizardSkipSettings"

    const-string v3, "changed Disable ----> Enable "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    const-string v2, "setup_wizard_skip"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 108
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 110
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip$1;->this$0:Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;

    # getter for: Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->mSkipMode:Landroid/widget/Button;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->access$000(Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;)Landroid/widget/Button;

    move-result-object v2

    const-string v3, "Setup Wizard Skip : Enabled"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip$1;->this$0:Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;

    # setter for: Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->bSkipMode:Z
    invoke-static {v2, v4}, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->access$202(Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;Z)Z

    goto :goto_0
.end method
