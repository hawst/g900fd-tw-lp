.class Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;
.super Ljava/lang/Object;
.source "Sec_Ril_Dump.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;


# direct methods
.method constructor <init>(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V
    .locals 0

    .prologue
    .line 815
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const-wide/16 v10, 0x3e8

    const/4 v13, 0x2

    const/high16 v12, 0x10000000

    .line 819
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mMmsProvisioning:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1600(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_0

    .line 820
    new-instance v2, Landroid/content/Intent;

    const-string v5, "com.sec.android.app.mmsprovision"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 821
    .local v2, "i":Landroid/content/Intent;
    const/16 v5, 0x20

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 822
    const-string v5, "key"

    const-string v6, "MmsProvisioning"

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 823
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    invoke-virtual {v5, v2}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->sendBroadcast(Landroid/content/Intent;)V

    .line 826
    .end local v2    # "i":Landroid/content/Intent;
    :cond_0
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mDpramDump:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1700(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_1

    .line 827
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;
    invoke-static {v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1800(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v6, 0xe

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->SendData(I)V
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1900(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;I)V

    .line 830
    :cond_1
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mLogcatRadio:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2000(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_2

    .line 831
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;
    invoke-static {v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1800(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->SendData(I)V
    invoke-static {v5, v13}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1900(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;I)V

    .line 835
    :cond_2
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mViewLog:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2100(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_3

    .line 837
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.MAIN"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 838
    .restart local v2    # "i":Landroid/content/Intent;
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->context:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2200(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/content/Context;

    move-result-object v5

    const-class v6, Lcom/sec/android/RilServiceModeApp/ViewRilLog;

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 839
    invoke-virtual {v2, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 840
    const-string v5, "key"

    const-string v6, "ViewRilLog"

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 841
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->context:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2200(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 844
    .end local v2    # "i":Landroid/content/Intent;
    :cond_3
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClearLog:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_4

    .line 845
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;
    invoke-static {v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1800(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v6, 0x5

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->SendData(I)V
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1900(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;I)V

    .line 846
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const-string v6, "Clear Ril Log"

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->DisplayMessageDialog(Ljava/lang/String;)V
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)V

    .line 849
    :cond_4
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mStorageInfo:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2400(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_5

    .line 850
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    new-instance v6, Landroid/os/StatFs;

    const-string v7, "/data"

    invoke-direct {v6, v7}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mDataFileStats:Landroid/os/StatFs;
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2502(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Landroid/os/StatFs;)Landroid/os/StatFs;

    .line 851
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mDataFileStats:Landroid/os/StatFs;
    invoke-static {v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2500(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/os/StatFs;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/StatFs;->getBlockCount()I

    move-result v6

    int-to-long v6, v6

    iget-object v8, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mDataFileStats:Landroid/os/StatFs;
    invoke-static {v8}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2500(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/os/StatFs;

    move-result-object v8

    invoke-virtual {v8}, Landroid/os/StatFs;->getBlockSize()I

    move-result v8

    int-to-long v8, v8

    mul-long/2addr v6, v8

    div-long/2addr v6, v10

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mTotalMemory:J
    invoke-static {v5, v6, v7}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2602(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;J)J

    .line 854
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mDataFileStats:Landroid/os/StatFs;
    invoke-static {v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2500(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/os/StatFs;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mDataFileStats:Landroid/os/StatFs;
    invoke-static {v7}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2500(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/os/StatFs;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/StatFs;->getBlockSize()I

    move-result v7

    mul-int/2addr v6, v7

    int-to-long v6, v6

    div-long/2addr v6, v10

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mFreeMem:J
    invoke-static {v5, v6, v7}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2702(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;J)J

    .line 857
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mFreeMem:J
    invoke-static {v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2700(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)J

    move-result-wide v6

    long-to-double v6, v6

    iget-object v8, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mTotalMemory:J
    invoke-static {v8}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2600(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)J

    move-result-wide v8

    long-to-double v8, v8

    const-wide v10, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v8, v10

    sub-double/2addr v6, v8

    double-to-long v6, v6

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mRemainMemory:J
    invoke-static {v5, v6, v7}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2802(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;J)J

    .line 859
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "/data memory information\n\nTotal Memory : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mTotalMemory:J
    invoke-static {v7}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2600(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "K\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "Free Memory: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mFreeMem:J
    invoke-static {v7}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2700(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "K\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "remain delta: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mRemainMemory:J
    invoke-static {v7}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2800(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "K\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->memory_info_msg:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2902(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)Ljava/lang/String;

    .line 863
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->memory_info_msg:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2900(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Ljava/lang/String;

    move-result-object v6

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->DisplayMessageDialog(Ljava/lang/String;)V
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)V

    .line 865
    :cond_5
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mIPCLog:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3000(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_6

    .line 866
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;
    invoke-static {v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1800(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v6, 0x8

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->SendData(I)V
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1900(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;I)V

    .line 869
    :cond_6
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mViewIPCLog:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3100(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_7

    .line 870
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.MAIN"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 871
    .restart local v2    # "i":Landroid/content/Intent;
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->context:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2200(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/content/Context;

    move-result-object v5

    const-class v6, Lcom/sec/android/RilServiceModeApp/ViewRilLog;

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 872
    invoke-virtual {v2, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 873
    const-string v5, "key"

    const-string v6, "ViewIPCDumpLog"

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 874
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->context:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2200(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 876
    .end local v2    # "i":Landroid/content/Intent;
    :cond_7
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mIPCBin:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3200(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_8

    .line 877
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;
    invoke-static {v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1800(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v6, 0x9

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->SendData(I)V
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1900(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;I)V

    .line 881
    :cond_8
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mCopyToSdcard:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_b

    .line 882
    invoke-static {}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->ExternalSDcardMounted()Z

    move-result v5

    if-nez v5, :cond_a

    .line 883
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const-string v6, "External SD Card UnMounted!!"

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->DisplayMessageDialog(Ljava/lang/String;)V
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)V

    .line 1029
    :cond_9
    :goto_0
    return-void

    .line 887
    :cond_a
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "/data/log/ril_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->rildump_time:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".log"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->inFile:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3402(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)Ljava/lang/String;

    .line 888
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "/sdcard/ril_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->rildump_time:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".log"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->outFile:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3502(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)Ljava/lang/String;

    .line 889
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->inFile:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3400(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->outFile:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3500(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "Ril log "

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->WriteToSDcard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    invoke-static {v5, v6, v7, v8}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3600(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    .line 891
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const-string v6, ""

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->inFile:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3402(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)Ljava/lang/String;

    .line 892
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const-string v6, ""

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->outFile:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3502(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)Ljava/lang/String;

    .line 894
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "/data/log/ipcdump_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->rildump_time:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".log"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->inFile:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3402(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)Ljava/lang/String;

    .line 895
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "/sdcard/ipcdump_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->rildump_time:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".log"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->outFile:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3502(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)Ljava/lang/String;

    .line 896
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->inFile:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3400(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->outFile:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3500(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "IPC Dump log "

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->WriteToSDcard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    invoke-static {v5, v6, v7, v8}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3600(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    .line 898
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const-string v6, ""

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->inFile:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3402(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)Ljava/lang/String;

    .line 899
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const-string v6, ""

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->outFile:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3502(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)Ljava/lang/String;

    .line 901
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "/data/log/ipcdump_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->rildump_time:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".bin"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->inFile:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3402(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)Ljava/lang/String;

    .line 902
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "/sdcard/ipcdump_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->rildump_time:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".bin"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->outFile:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3502(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)Ljava/lang/String;

    .line 903
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->inFile:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3400(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->outFile:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3500(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "IPC Dump Bin "

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->WriteToSDcard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    invoke-static {v5, v6, v7, v8}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3600(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    .line 905
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const-string v6, "Copy Success!!"

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->DisplayMessageDialog(Ljava/lang/String;)V
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)V

    .line 909
    :cond_b
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mCpRamdump:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3700(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_d

    .line 910
    invoke-static {}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->ExternalSDcardMounted()Z

    move-result v5

    if-nez v5, :cond_c

    .line 911
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const-string v6, "External SD Card UnMounted!!"

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->DisplayMessageDialog(Ljava/lang/String;)V
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 915
    :cond_c
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const-string v6, ""

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->inFile:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3402(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)Ljava/lang/String;

    .line 916
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const-string v6, ""

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->outFile:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3502(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)Ljava/lang/String;

    .line 918
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const-string v6, "/data/dump_for_cp"

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->inFile:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3402(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)Ljava/lang/String;

    .line 919
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const-string v6, "/sdcard/dump_for_cp"

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->outFile:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3502(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)Ljava/lang/String;

    .line 921
    new-instance v4, Ljava/io/File;

    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->inFile:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3400(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 923
    .local v4, "oFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1c

    .line 924
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->inFile:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3400(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->outFile:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3500(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "CP Ramdump "

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->WriteToSDcard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    invoke-static {v5, v6, v7, v8}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3600(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 926
    .local v1, "err":I
    if-nez v1, :cond_1b

    .line 927
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const-string v6, "copy success!!"

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->DisplayMessageDialog(Ljava/lang/String;)V
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)V

    .line 936
    .end local v1    # "err":I
    .end local v4    # "oFile":Ljava/io/File;
    :cond_d
    :goto_1
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mNvBackup:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3800(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_e

    .line 939
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;
    invoke-static {v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1800(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v6, 0x1

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mActionType:I
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3902(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;I)I

    .line 940
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->showPassDialog()V
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$4000(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V

    .line 944
    :cond_e
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mNvDelete:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$4100(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_f

    .line 947
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;
    invoke-static {v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1800(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v6, 0x0

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mActionType:I
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3902(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;I)I

    .line 948
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->showPassDialog()V
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$4000(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V

    .line 951
    :cond_f
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mEfsData:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$4200(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_10

    .line 952
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;
    invoke-static {v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1800(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mActionType:I
    invoke-static {v5, v13}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$3902(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;I)I

    .line 953
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->showPassDialog()V
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$4000(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V

    .line 957
    :cond_10
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleFd:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$500(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_11

    .line 958
    const-string v5, "RilDump"

    const-string v6, "[RIL::FD] toggle btn clicked"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 959
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->changeFDEnableState()V
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$4300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V

    .line 960
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->EnableFastDormancy()V
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$4400(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V

    .line 964
    :cond_11
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mCscCompare:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$4500(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_12

    .line 965
    const-string v5, "RilDump"

    const-string v6, "csc compare btn clicked"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 966
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->showCscCompareCfrmDialog()V
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$4600(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V

    .line 970
    :cond_12
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleDataRecovery:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$700(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_13

    .line 971
    const-string v5, "RilDump"

    const-string v6, "DataRecovery toggle btn clicked"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 972
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->changeDataRecoveryTime()V
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$4700(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V

    .line 975
    :cond_13
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleApnEdit:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$900(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_14

    .line 976
    const-string v5, "RilDump"

    const-string v6, "ApnEidt btn clicked"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 977
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->changeApnEditState()V
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$4800(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V

    .line 980
    :cond_14
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mExit:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$4900(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_15

    .line 981
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    invoke-virtual {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->finish()V

    .line 984
    :cond_15
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleDB:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1100(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_16

    .line 985
    const-string v5, "RilDump"

    const-string v6, "Download Booster clicked"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 986
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->changeDownloadBoosterState()V
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$5000(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V

    .line 989
    :cond_16
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleDBWaring:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_17

    .line 990
    const-string v5, "RilDump"

    const-string v6, "Download Booster Warning clicked"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 991
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->changeDBWarningToastState()V
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$5100(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V

    .line 994
    :cond_17
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mViewAPN:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$5200(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_18

    .line 995
    const-string v5, "RilDump"

    const-string v6, "[RIL::ViewAPN] btn clicked"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 996
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.MAIN"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 997
    .restart local v2    # "i":Landroid/content/Intent;
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->context:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2200(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/content/Context;

    move-result-object v5

    const-class v6, Lcom/sec/android/RilServiceModeApp/ViewApnInfo;

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 998
    invoke-virtual {v2, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 999
    const-string v5, "key"

    const-string v6, "ViewApnInfo"

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1000
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->context:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2200(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1003
    .end local v2    # "i":Landroid/content/Intent;
    :cond_18
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mViewSetupWizardSkip:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$5300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_19

    .line 1004
    const-string v5, "RilDump"

    const-string v6, "[RIL::ViewSetupWizardSkip] btn clicked"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1005
    const-string v5, "ro.build.product"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1006
    .local v0, "build_product":Ljava/lang/String;
    const-string v5, "(?i).*vzw.*"

    invoke-virtual {v0, v5}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1d

    .line 1007
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.MAIN"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1008
    .restart local v2    # "i":Landroid/content/Intent;
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->context:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2200(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/content/Context;

    move-result-object v5

    const-class v6, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1009
    invoke-virtual {v2, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1010
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->context:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2200(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1016
    .end local v0    # "build_product":Ljava/lang/String;
    .end local v2    # "i":Landroid/content/Intent;
    :cond_19
    :goto_2
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mSmokeTests:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$5400(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_1a

    .line 1017
    const-string v5, "RilDump"

    const-string v6, "[RIL] Run smoketests"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1018
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.MAIN"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1019
    .restart local v2    # "i":Landroid/content/Intent;
    const-string v5, "com.samsung.smoketests"

    const-string v6, "com.samsung.smoketests.activities.MainActivity"

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1020
    invoke-virtual {v2, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1021
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->context:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2200(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1024
    .end local v2    # "i":Landroid/content/Intent;
    :cond_1a
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mImsLogger:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$5500(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;

    move-result-object v5

    if-ne p1, v5, :cond_9

    .line 1025
    new-instance v3, Landroid/content/Intent;

    const-string v5, "com.sec.imslogger.action_bootup_start"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1026
    .local v3, "intent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->context:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$2200(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 929
    .end local v3    # "intent":Landroid/content/Intent;
    .restart local v1    # "err":I
    .restart local v4    # "oFile":Ljava/io/File;
    :cond_1b
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const-string v6, "exception error!!"

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->DisplayMessageDialog(Ljava/lang/String;)V
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 931
    .end local v1    # "err":I
    :cond_1c
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const-string v6, "dump_for_cp file is not exist"

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->DisplayMessageDialog(Ljava/lang/String;)V
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1012
    .end local v4    # "oFile":Ljava/io/File;
    .restart local v0    # "build_product":Ljava/lang/String;
    :cond_1d
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const-string v6, "Do not support SetupWizardSkip "

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->DisplayMessageDialog(Ljava/lang/String;)V
    invoke-static {v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)V

    goto :goto_2
.end method
