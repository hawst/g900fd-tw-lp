.class Lcom/sec/android/RilServiceModeApp/ServiceModeApp$6;
.super Ljava/lang/Object;
.source "ServiceModeApp.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->onMenuItemSelected(ILandroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

.field final synthetic val$select:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 1138
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$6;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    iput-object p2, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$6;->val$select:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 12
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v11, 0x1

    .line 1140
    const/4 v7, 0x0

    .line 1142
    .local v7, "DataLen":I
    const-string v0, "ServiceModeApp_RIL"

    const-string v1, "OK Button clicked"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1144
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$6;->val$select:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1145
    .local v10, "num":Ljava/lang/String;
    const/4 v0, 0x0

    invoke-virtual {v10, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 1147
    .local v4, "line_num":C
    const-string v0, "ServiceModeApp_RIL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "line is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1148
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$6;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$6;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$400(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$6;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$400(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$6;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;
    invoke-static {v3}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$400(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v3, 0x0

    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$6;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$400(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$6;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C
    invoke-static {v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$500(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)C

    move-result v6

    # invokes: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V
    invoke-static/range {v0 .. v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$600(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;CCCCCC)V

    .line 1152
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$6;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->calledByKeyPress:Z
    invoke-static {v0, v1}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$1102(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;Z)Z

    .line 1154
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$6;->val$select:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1161
    .end local v4    # "line_num":C
    .end local v10    # "num":Ljava/lang/String;
    :goto_0
    return-void

    .line 1155
    :catch_0
    move-exception v8

    .line 1156
    .local v8, "e":Ljava/lang/Exception;
    const-string v0, "ServiceModeApp_RIL"

    const-string v1, "Exception in Select Option Menu!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1157
    const-string v9, "Wrong value. Enter the row"

    .line 1158
    .local v9, "msg":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$6;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    invoke-static {v0, v9, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
