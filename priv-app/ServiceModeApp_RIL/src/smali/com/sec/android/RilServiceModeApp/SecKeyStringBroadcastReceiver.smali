.class public Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SecKeyStringBroadcastReceiver.java"


# static fields
.field static final APNTEST_KEYSTRING:[Ljava/lang/String;

.field static final APN_NAME:[Ljava/lang/String;

.field static final SECRIL_DUMP_KEYSTRING:[Ljava/lang/String;

.field static final SERVICEMODE_KEYSTRING:[Ljava/lang/String;

.field private static final mProductDevice:Ljava/lang/String;

.field private static final mSalesCode:Ljava/lang/String;


# instance fields
.field private bStartActivity:Z

.field private mTestApnList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private offloadDebug:I

.field product_model:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 23
    const-string v0, "ro.csc.sales_code"

    const-string v1, "NONE"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->mSalesCode:Ljava/lang/String;

    .line 25
    const-string v0, "ro.product.device"

    const-string v1, "NONE"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->mProductDevice:Ljava/lang/String;

    .line 33
    const/16 v0, 0x22

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "197328640"

    aput-object v1, v0, v3

    const-string v1, "27663368378"

    aput-object v1, v0, v4

    const-string v1, "276633683782"

    aput-object v1, v0, v5

    const-string v1, "2684"

    aput-object v1, v0, v6

    const-string v1, "0011"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "00112"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "123456"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "32489"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "2580"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "232337"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "232331"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "232332"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "9090"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "73876766"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "738767633"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "7387678378"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "7387677763"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "4387264636"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "4238378"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "1575"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "6984125*"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "2886"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "2767*2878"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "1111"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "2222"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "8888"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "301279"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "279301"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "2263"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "22632"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "58366"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "CP_RAMDUMP"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "37375625"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "33725327"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->SERVICEMODE_KEYSTRING:[Ljava/lang/String;

    .line 40
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "147852"

    aput-object v1, v0, v3

    const-string v1, "3698741"

    aput-object v1, v0, v4

    const-string v1, "369852"

    aput-object v1, v0, v5

    const-string v1, "1478963"

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->APNTEST_KEYSTRING:[Ljava/lang/String;

    .line 44
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "Suwon"

    aput-object v1, v0, v3

    const-string v1, "Delete_DB"

    aput-object v1, v0, v4

    const-string v1, "Gumi"

    aput-object v1, v0, v5

    const-string v1, "Open_market"

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->APN_NAME:[Ljava/lang/String;

    .line 48
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "745"

    aput-object v1, v0, v3

    const-string v1, "746"

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->SECRIL_DUMP_KEYSTRING:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 28
    const-string v0, "ro.product.model"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->product_model:Ljava/lang/String;

    .line 29
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->bStartActivity:Z

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->offloadDebug:I

    .line 55
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->InitApnList()V

    .line 56
    return-void
.end method

.method private CheckHostKeyString([Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "keyStringSet"    # [Ljava/lang/String;
    .param p2, "host"    # Ljava/lang/String;

    .prologue
    .line 67
    const/4 v0, 0x0

    .local v0, "idx":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 68
    aget-object v1, p1, v0

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    const/4 v1, 0x1

    .line 72
    :goto_1
    return v1

    .line 67
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 72
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private InitApnList()V
    .locals 4

    .prologue
    .line 59
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->mTestApnList:Ljava/util/HashMap;

    .line 60
    const/4 v0, 0x0

    .local v0, "idx":I
    :goto_0
    sget-object v1, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->APNTEST_KEYSTRING:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 61
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->mTestApnList:Ljava/util/HashMap;

    sget-object v2, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->APNTEST_KEYSTRING:[Ljava/lang/String;

    aget-object v2, v2, v0

    sget-object v3, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->APN_NAME:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 63
    :cond_0
    return-void
.end method


# virtual methods
.method public DoRamdumpScreenUsingDebugScreen()Z
    .locals 6

    .prologue
    .line 117
    const-string v1, "/data/misc/radio/ramdumpmode.txt"

    .line 118
    .local v1, "filePath":Ljava/lang/String;
    const-string v2, ""

    .line 119
    .local v2, "mode":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 122
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 124
    invoke-virtual {p0, v1}, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 126
    const-string v3, "ServiceMode_RIL"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ramdump mode is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 129
    const/4 v3, 0x1

    .line 131
    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    .line 136
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.provider.Telephony.SECRET_CODE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 137
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 138
    .local v3, "i":Landroid/content/Intent;
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    .line 140
    .local v2, "host":Ljava/lang/String;
    const-string v4, "ServiceMode_RIL"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "host is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    sget-object v4, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->SERVICEMODE_KEYSTRING:[Ljava/lang/String;

    invoke-direct {p0, v4, v2}, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->CheckHostKeyString([Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 143
    const-class v4, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    invoke-virtual {v3, p1, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 144
    const-string v4, "keyString"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 179
    :cond_0
    :goto_0
    const-string v4, "CTC"

    sget-object v5, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    sget-object v4, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->mProductDevice:Ljava/lang/String;

    const-string v5, "CTC"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 180
    :cond_1
    const-string v4, "TESTMODE"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 181
    const-class v4, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    invoke-virtual {v3, p1, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 182
    const-string v4, "keyString"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 191
    :cond_2
    :goto_1
    sget-object v4, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->mSalesCode:Ljava/lang/String;

    const-string v5, "CHM"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 193
    const-string v4, "827828868378"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 194
    const-class v4, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    invoke-virtual {v3, p1, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 195
    const-string v4, "keyString"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199
    :cond_3
    const-string v4, "ro.factory.factory_binary"

    const-string v5, ""

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 200
    .local v1, "factoryMode":Ljava/lang/String;
    const-string v4, "33725327"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, "factory"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 201
    iput-boolean v7, p0, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->bStartActivity:Z

    .line 202
    const-string v4, "ServiceMode_RIL"

    const-string v5, "Not Factory Binary"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    :cond_4
    iget-boolean v4, p0, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->bStartActivity:Z

    if-eqz v4, :cond_5

    .line 206
    const/high16 v4, 0x10000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 207
    invoke-virtual {p1, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 210
    .end local v1    # "factoryMode":Ljava/lang/String;
    .end local v2    # "host":Ljava/lang/String;
    .end local v3    # "i":Landroid/content/Intent;
    :cond_5
    return-void

    .line 145
    .restart local v2    # "host":Ljava/lang/String;
    .restart local v3    # "i":Landroid/content/Intent;
    :cond_6
    sget-object v4, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->APNTEST_KEYSTRING:[Ljava/lang/String;

    invoke-direct {p0, v4, v2}, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->CheckHostKeyString([Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 146
    const-string v0, ""

    .line 147
    .local v0, "apnName":Ljava/lang/String;
    const-class v4, Lcom/sec/android/RilServiceModeApp/TestApnSettings;

    invoke-virtual {v3, p1, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 148
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->mTestApnList:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "apnName":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 150
    .restart local v0    # "apnName":Ljava/lang/String;
    const-string v4, "testBed"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 151
    .end local v0    # "apnName":Ljava/lang/String;
    :cond_7
    sget-object v4, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->SECRIL_DUMP_KEYSTRING:[Ljava/lang/String;

    invoke-direct {p0, v4, v2}, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->CheckHostKeyString([Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 152
    const-class v4, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    invoke-virtual {v3, p1, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 153
    :cond_8
    const-string v4, "66336"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 154
    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->DoRamdumpScreenUsingDebugScreen()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 155
    const-string v4, "ServiceMode_RIL"

    const-string v5, "Show CP dump menu using debug screen"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    const-class v4, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    invoke-virtual {v3, p1, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 157
    const-string v4, "keyString"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 159
    :cond_9
    iput-boolean v7, p0, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->bStartActivity:Z

    goto/16 :goto_0

    .line 161
    :cond_a
    const-string v4, "03"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 162
    const-class v4, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;

    invoke-virtual {v3, p1, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 163
    :cond_b
    const-string v4, "3214789"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 164
    const-class v4, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;

    invoke-virtual {v3, p1, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 165
    :cond_c
    const-string v4, "22553767"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 166
    const-class v4, Lcom/sec/android/RilServiceModeApp/CallDropLogView;

    invoke-virtual {v3, p1, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 167
    :cond_d
    const-string v4, "36764"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 168
    const-class v4, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    invoke-virtual {v3, p1, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 169
    :cond_e
    const-string v4, "758353266223"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 170
    const-class v4, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    invoke-virtual {v3, p1, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 171
    const-string v4, "keyString"

    const-string v5, "758353266223"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 172
    :cond_f
    const-string v4, "1234567890"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 173
    const-class v4, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    invoke-virtual {v3, p1, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 174
    const-string v4, "keyString"

    const-string v5, "1234567890"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 175
    :cond_10
    const-string v4, "119"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 176
    const-class v4, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    invoke-virtual {v3, p1, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 177
    const-string v4, "keyString"

    const-string v5, "119"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 185
    :cond_11
    const-string v4, "TESTMODE"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 186
    iput-boolean v7, p0, Lcom/sec/android/RilServiceModeApp/SecKeyStringBroadcastReceiver;->bStartActivity:Z

    goto/16 :goto_1
.end method

.method public readOneLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 76
    const-string v5, ""

    .line 77
    .local v5, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 79
    .local v0, "buf":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/FileReader;

    invoke-direct {v6, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    const/16 v7, 0x1fa0

    invoke-direct {v1, v6, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    if-eqz v1, :cond_3

    .line 82
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 85
    .local v4, "line":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 86
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 87
    if-nez v5, :cond_3

    .line 88
    const-string v6, ""
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_b
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 102
    if-eqz v1, :cond_0

    .line 104
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    :goto_0
    move-object v0, v1

    .line 113
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "line":Ljava/lang/String;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :goto_1
    return-object v6

    .line 105
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v4    # "line":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 106
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 91
    .end local v2    # "e":Ljava/io/IOException;
    :cond_1
    :try_start_3
    const-string v6, ""
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_c
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_b
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_a
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 102
    if-eqz v1, :cond_2

    .line 104
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_2
    :goto_2
    move-object v0, v1

    .line 107
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_1

    .line 105
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_1
    move-exception v2

    .line 106
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 102
    .end local v2    # "e":Ljava/io/IOException;
    .end local v4    # "line":Ljava/lang/String;
    :cond_3
    if-eqz v1, :cond_7

    .line 104
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    move-object v0, v1

    .line 110
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_4
    :goto_3
    if-nez v5, :cond_6

    .line 111
    const-string v6, ""

    goto :goto_1

    .line 105
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_2
    move-exception v2

    .line 106
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 107
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3

    .line 95
    .end local v2    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v3

    .line 96
    .local v3, "ex":Ljava/io/FileNotFoundException;
    :goto_4
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 102
    if-eqz v0, :cond_4

    .line 104
    :try_start_7
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_3

    .line 105
    :catch_4
    move-exception v2

    .line 106
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 97
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "ex":Ljava/io/FileNotFoundException;
    :catch_5
    move-exception v2

    .line 98
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_5
    :try_start_8
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 102
    if-eqz v0, :cond_4

    .line 104
    :try_start_9
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    goto :goto_3

    .line 105
    :catch_6
    move-exception v2

    .line 106
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 99
    .end local v2    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v2

    .line 100
    .local v2, "e":Ljava/lang/NullPointerException;
    :goto_6
    :try_start_a
    invoke-virtual {v2}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 102
    if-eqz v0, :cond_4

    .line 104
    :try_start_b
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    goto :goto_3

    .line 105
    :catch_8
    move-exception v2

    .line 106
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 102
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_7
    if-eqz v0, :cond_5

    .line 104
    :try_start_c
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 107
    :cond_5
    :goto_8
    throw v6

    .line 105
    :catch_9
    move-exception v2

    .line 106
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .end local v2    # "e":Ljava/io/IOException;
    :cond_6
    move-object v6, v5

    .line 113
    goto :goto_1

    .line 102
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v6

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_7

    .line 99
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_a
    move-exception v2

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_6

    .line 97
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_b
    move-exception v2

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_5

    .line 95
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_c
    move-exception v3

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_4

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :cond_7
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3
.end method
