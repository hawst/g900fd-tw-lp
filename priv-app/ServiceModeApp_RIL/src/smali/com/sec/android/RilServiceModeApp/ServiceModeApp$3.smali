.class Lcom/sec/android/RilServiceModeApp/ServiceModeApp$3;
.super Ljava/lang/Object;
.source "ServiceModeApp.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/RilServiceModeApp/ServiceModeApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;


# direct methods
.method constructor <init>(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)V
    .locals 0

    .prologue
    .line 611
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$3;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 613
    const-string v0, "ServiceModeApp_RIL"

    const-string v1, "onServiceConnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 614
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$3;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    # setter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$802(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 615
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$3;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # invokes: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->StartServiceMode()V
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$900(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)V

    .line 616
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 619
    const-string v0, "ServiceModeApp_RIL"

    const-string v1, "onServiceDisconnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 620
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$3;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$802(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 621
    return-void
.end method
