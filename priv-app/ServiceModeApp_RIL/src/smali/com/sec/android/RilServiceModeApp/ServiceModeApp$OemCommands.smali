.class Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;
.super Ljava/lang/Object;
.source "ServiceModeApp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/RilServiceModeApp/ServiceModeApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OemCommands"
.end annotation


# instance fields
.field final OEM_MODEM_CDMA:C

.field final OEM_MODEM_FORCE_CRASH_EXIT:I

.field final OEM_MODEM_GSM:C

.field OEM_SERVM_FUNCTAG:C

.field final OEM_SM_ACTION:C

.field final OEM_SM_DUMMY:C

.field final OEM_SM_END_MODE_MESSAGE:C

.field final OEM_SM_ENTER_MODE_MESSAGE:C

.field final OEM_SM_GET_DISPLAY_DATA_MESSAGE:C

.field final OEM_SM_PROCESS_KEY_MESSAGE:C

.field final OEM_SM_QUERY:C

.field final OEM_SM_TYPE_MONITOR:C

.field final OEM_SM_TYPE_MONITOR_S:C

.field final OEM_SM_TYPE_MONITOR_SKT:C

.field final OEM_SM_TYPE_NAM_EDIT:C

.field final OEM_SM_TYPE_PHONE_TEST:C

.field final OEM_SM_TYPE_SUB_ALERT_ERASE_ENTER:C

.field final OEM_SM_TYPE_SUB_ALL_VERSION_ENTER:C

.field final OEM_SM_TYPE_SUB_BAND_SEL_ENTER:C

.field final OEM_SM_TYPE_SUB_BATTERY_INFO_ENTER:C

.field final OEM_SM_TYPE_SUB_BLUETOOTH_TEST_ENTER:C

.field final OEM_SM_TYPE_SUB_CIPHERING_PROTECTION_ENTER:C

.field final OEM_SM_TYPE_SUB_EFS_CLEAR:C

.field final OEM_SM_TYPE_SUB_ENTER:C

.field final OEM_SM_TYPE_SUB_FACTORY_PRECONFIG_ENTER:C

.field final OEM_SM_TYPE_SUB_FACTORY_RESET_ENTER:C

.field final OEM_SM_TYPE_SUB_FACTORY_VF_TEST_ENTER:C

.field final OEM_SM_TYPE_SUB_FTA_HW_VERSION_ENTER:C

.field final OEM_SM_TYPE_SUB_FTA_SW_VERSION_ENTER:C

.field final OEM_SM_TYPE_SUB_GCF_TESTMODE_ENTER:C

.field final OEM_SM_TYPE_SUB_GET_SELLOUT_SMS_INFO_ENTER:C

.field final OEM_SM_TYPE_SUB_GPSONE_SS_TEST_ENTER:C

.field final OEM_SM_TYPE_SUB_GSM_FACTORY_AUDIO_LB_ENTER:C

.field final OEM_SM_TYPE_SUB_HOSTLINK_ENTER:C

.field final OEM_SM_TYPE_SUB_IMEI_READ_ENTER:C

.field final OEM_SM_TYPE_SUB_INTEGRITY_PROTECTION_ENTER:C

.field final OEM_SM_TYPE_SUB_LOGGING_SETTING:C

.field final OEM_SM_TYPE_SUB_MELODY_TEST_ENTER:C

.field final OEM_SM_TYPE_SUB_MP3_TEST_ENTER:C

.field final OEM_SM_TYPE_SUB_RRC_VERSION_ENTER:C

.field final OEM_SM_TYPE_SUB_RSC_FILE_VERSION_ENTER:C

.field final OEM_SM_TYPE_SUB_SELLOUT_SMS_DISABLE_ENTER:C

.field final OEM_SM_TYPE_SUB_SELLOUT_SMS_ENABLE_ENTER:C

.field final OEM_SM_TYPE_SUB_SELLOUT_SMS_PRODUCT_MODE_ON:C

.field final OEM_SM_TYPE_SUB_SELLOUT_SMS_TEST_MODE_ON:C

.field final OEM_SM_TYPE_SUB_SET_LTE_ANT_PATH_NORMAL:C

.field final OEM_SM_TYPE_SUB_SET_LTE_ON:C

.field final OEM_SM_TYPE_SUB_SUIMPROBE_ENTER:C

.field final OEM_SM_TYPE_SUB_SW_VERSION_ENTER:C

.field final OEM_SM_TYPE_SUB_TFS4_EXPLORE_ENTER:C

.field final OEM_SM_TYPE_SUB_TOTAL_CALL_TIME_INFO_ENTER:C

.field final OEM_SM_TYPE_SUB_TSP_CP_DEBUGSCREEN_MODE_ENTER:C

.field final OEM_SM_TYPE_SUB_TSP_CP_RAMDUMP_MODE_ENTER:C

.field final OEM_SM_TYPE_SUB_TSP_CP_RAMDUMP_MODE_EXCUTE:C

.field final OEM_SM_TYPE_SUB_TST_AUTO_ANSWER_ENTER:C

.field final OEM_SM_TYPE_SUB_TST_FREQ_LOCK_MODE_ENTER:C

.field final OEM_SM_TYPE_SUB_TST_FTA_HW_VERSION_ENTER:C

.field final OEM_SM_TYPE_SUB_TST_FTA_SW_VERSION_ENTER:C

.field final OEM_SM_TYPE_SUB_TST_NV_RESET_ENTER:C

.field final OEM_SM_TYPE_SUB_USB_DRIVER_ENTER:C

.field final OEM_SM_TYPE_SUB_USB_UART_DIAG_CONTROL_ENTER:C

.field final OEM_SM_TYPE_SUB_VIBRATOR_TEST_ENTER:C

.field final OEM_SM_TYPE_TEST_AUTO:C

.field final OEM_SM_TYPE_TEST_MANUAL:C

.field final OEM_SYSDUMP_FUNCTAG:I

.field final synthetic this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;


# direct methods
.method private constructor <init>(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 254
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256
    iput-char v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SERVM_FUNCTAG:C

    .line 259
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SYSDUMP_FUNCTAG:I

    .line 260
    const/16 v0, 0x17

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_MODEM_FORCE_CRASH_EXIT:I

    .line 263
    iput-char v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_DUMMY:C

    .line 264
    iput-char v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_ENTER_MODE_MESSAGE:C

    .line 265
    iput-char v2, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_END_MODE_MESSAGE:C

    .line 266
    iput-char v5, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_PROCESS_KEY_MESSAGE:C

    .line 267
    iput-char v3, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_GET_DISPLAY_DATA_MESSAGE:C

    .line 270
    iput-char v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_TEST_MANUAL:C

    .line 271
    iput-char v2, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_TEST_AUTO:C

    .line 272
    iput-char v5, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_NAM_EDIT:C

    .line 273
    iput-char v3, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_MONITOR:C

    .line 274
    const/4 v0, 0x5

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_PHONE_TEST:C

    .line 276
    const/4 v0, 0x7

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_MONITOR_S:C

    .line 280
    iput-char v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_ENTER:C

    .line 281
    iput-char v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_SW_VERSION_ENTER:C

    .line 282
    iput-char v2, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_FTA_SW_VERSION_ENTER:C

    .line 283
    iput-char v5, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_FTA_HW_VERSION_ENTER:C

    .line 284
    iput-char v3, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_ALL_VERSION_ENTER:C

    .line 285
    const/4 v0, 0x5

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_BATTERY_INFO_ENTER:C

    .line 286
    const/4 v0, 0x6

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_CIPHERING_PROTECTION_ENTER:C

    .line 287
    const/4 v0, 0x7

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_INTEGRITY_PROTECTION_ENTER:C

    .line 288
    const/16 v0, 0x8

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_IMEI_READ_ENTER:C

    .line 289
    const/16 v0, 0x9

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_BLUETOOTH_TEST_ENTER:C

    .line 290
    const/16 v0, 0xa

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_VIBRATOR_TEST_ENTER:C

    .line 291
    const/16 v0, 0xb

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_MELODY_TEST_ENTER:C

    .line 292
    const/16 v0, 0xc

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_MP3_TEST_ENTER:C

    .line 293
    const/16 v0, 0xd

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_FACTORY_RESET_ENTER:C

    .line 294
    const/16 v0, 0xe

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_FACTORY_PRECONFIG_ENTER:C

    .line 295
    const/16 v0, 0xf

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_TFS4_EXPLORE_ENTER:C

    .line 296
    const/16 v0, 0x11

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_RSC_FILE_VERSION_ENTER:C

    .line 297
    const/16 v0, 0x12

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_USB_DRIVER_ENTER:C

    .line 298
    const/16 v0, 0x13

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_USB_UART_DIAG_CONTROL_ENTER:C

    .line 299
    const/16 v0, 0x14

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_RRC_VERSION_ENTER:C

    .line 300
    const/16 v0, 0x15

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_GPSONE_SS_TEST_ENTER:C

    .line 301
    const/16 v0, 0x16

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_BAND_SEL_ENTER:C

    .line 302
    const/16 v0, 0x17

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_GCF_TESTMODE_ENTER:C

    .line 303
    const/16 v0, 0x18

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_GSM_FACTORY_AUDIO_LB_ENTER:C

    .line 304
    const/16 v0, 0x19

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_FACTORY_VF_TEST_ENTER:C

    .line 305
    const/16 v0, 0x1a

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_TOTAL_CALL_TIME_INFO_ENTER:C

    .line 306
    const/16 v0, 0x1b

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_SELLOUT_SMS_ENABLE_ENTER:C

    .line 307
    const/16 v0, 0x1c

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_SELLOUT_SMS_DISABLE_ENTER:C

    .line 308
    const/16 v0, 0x1d

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_SELLOUT_SMS_TEST_MODE_ON:C

    .line 309
    const/16 v0, 0x1e

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_SELLOUT_SMS_PRODUCT_MODE_ON:C

    .line 310
    const/16 v0, 0x1f

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_GET_SELLOUT_SMS_INFO_ENTER:C

    .line 311
    const/16 v0, 0x20

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_TST_AUTO_ANSWER_ENTER:C

    .line 312
    const/16 v0, 0x21

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_TST_NV_RESET_ENTER:C

    .line 313
    const/16 v0, 0x22

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_SET_LTE_ANT_PATH_NORMAL:C

    .line 314
    const/16 v0, 0x24

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_SET_LTE_ON:C

    .line 315
    const/16 v0, 0x40

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_LOGGING_SETTING:C

    .line 316
    const/16 v0, 0x1002

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_TST_FTA_SW_VERSION_ENTER:C

    .line 317
    const/16 v0, 0x1003

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_TST_FTA_HW_VERSION_ENTER:C

    .line 318
    const/16 v0, 0x1022

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_TSP_CP_RAMDUMP_MODE_ENTER:C

    .line 319
    const/16 v0, 0x1023

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_TSP_CP_DEBUGSCREEN_MODE_ENTER:C

    .line 320
    const/16 v0, 0x1024

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_TSP_CP_RAMDUMP_MODE_EXCUTE:C

    .line 321
    const/16 v0, 0x1025

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_TST_FREQ_LOCK_MODE_ENTER:C

    .line 322
    const/16 v0, 0x1027

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_EFS_CLEAR:C

    .line 324
    const/16 v0, 0x3a

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_SUIMPROBE_ENTER:C

    .line 325
    const/16 v0, 0x3b

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_HOSTLINK_ENTER:C

    .line 326
    const/16 v0, 0x3c

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_SUB_ALERT_ERASE_ENTER:C

    .line 329
    iput-char v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_ACTION:C

    .line 330
    iput-char v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_QUERY:C

    .line 331
    iput-char v1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SM_TYPE_MONITOR_SKT:C

    .line 333
    iput-char v2, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_MODEM_CDMA:C

    .line 334
    iput-char v3, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_MODEM_GSM:C

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/RilServiceModeApp/ServiceModeApp;
    .param p2, "x1"    # Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;

    .prologue
    .line 254
    invoke-direct {p0, p1}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;-><init>(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)V

    return-void
.end method


# virtual methods
.method DoCpCrash()[B
    .locals 5

    .prologue
    .line 398
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 399
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 402
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v3, 0x7

    :try_start_0
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 403
    const/16 v3, 0x17

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 404
    const/4 v3, 0x5

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 405
    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 410
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    :goto_0
    return-object v3

    .line 406
    :catch_0
    move-exception v2

    .line 407
    .local v2, "e":Ljava/io/IOException;
    const-string v3, "ServiceModeApp_RIL"

    const-string v4, "IOException in DoCpCrash!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    const/4 v3, 0x0

    goto :goto_0
.end method

.method getServMEnterData(CCCCC)[B
    .locals 6
    .param p1, "svcMode"    # C
    .param p2, "modeType"    # C
    .param p3, "subType"    # C
    .param p4, "query"    # C
    .param p5, "modem_type"    # C

    .prologue
    .line 337
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 338
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 339
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/16 v3, 0x8

    .line 341
    .local v3, "fileSize":C
    :try_start_0
    iget-char v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SERVM_FUNCTAG:C

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 342
    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 343
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 345
    invoke-virtual {v1, p5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 346
    invoke-virtual {v1, p2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 347
    invoke-virtual {v1, p3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 348
    invoke-virtual {v1, p4}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 353
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    :goto_0
    return-object v4

    .line 349
    :catch_0
    move-exception v2

    .line 350
    .local v2, "e":Ljava/io/IOException;
    const-string v4, "ServiceModeApp_RIL"

    const-string v5, "IOException in getServMQueryData!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    const/4 v4, 0x0

    goto :goto_0
.end method

.method setEndModeData(CCC)[B
    .locals 6
    .param p1, "svcMode"    # C
    .param p2, "modeType"    # C
    .param p3, "modem_type"    # C

    .prologue
    .line 379
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 380
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 381
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v3, 0x6

    .line 384
    .local v3, "fileSize":I
    :try_start_0
    iget-char v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SERVM_FUNCTAG:C

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 385
    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 386
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 388
    invoke-virtual {v1, p3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 389
    invoke-virtual {v1, p2}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 394
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    :goto_0
    return-object v4

    .line 390
    :catch_0
    move-exception v2

    .line 391
    .local v2, "e":Ljava/io/IOException;
    const-string v4, "ServiceModeApp_RIL"

    const-string v5, "IOException in getServMQueryData!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    const/4 v4, 0x0

    goto :goto_0
.end method

.method setPressKeyData(CCCC)[B
    .locals 6
    .param p1, "svcMode"    # C
    .param p2, "keycode"    # C
    .param p3, "query"    # C
    .param p4, "modem_type"    # C

    .prologue
    .line 358
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 359
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 360
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v3, 0x7

    .line 363
    .local v3, "fileSize":I
    :try_start_0
    iget-char v4, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;->OEM_SERVM_FUNCTAG:C

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 364
    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 365
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 367
    invoke-virtual {v1, p4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 368
    invoke-virtual {v1, p2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 369
    invoke-virtual {v1, p3}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 375
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    :goto_0
    return-object v4

    .line 370
    :catch_0
    move-exception v2

    .line 371
    .local v2, "e":Ljava/io/IOException;
    const-string v4, "ServiceModeApp_RIL"

    const-string v5, "IOException in getServMQueryData!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    const/4 v4, 0x0

    goto :goto_0
.end method
