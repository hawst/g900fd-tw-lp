.class Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;
.super Ljava/lang/Object;
.source "Sec_Ril_Dump.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OemCommands"
.end annotation


# instance fields
.field final NV_BACKUP:I

.field final NV_DELETE:I

.field final OEM_DBG_STATE_GET:I

.field final OEM_DPRAMDUMP_ON:I

.field final OEM_DUMPSTATE:I

.field final OEM_ENABLE_LOG:I

.field final OEM_IPC_DUMP_BIN:I

.field final OEM_IPC_DUMP_LOG:I

.field final OEM_KERNEL_LOG:I

.field final OEM_LOGCAT_CLEAR:I

.field final OEM_LOGCAT_MAIN:I

.field final OEM_LOGCAT_RADIO:I

.field final OEM_NV_BACKUP:I

.field final OEM_RAMDUMP_MODE:I

.field final OEM_RAMDUMP_STATE_GET:I

.field final OEM_SYSDUMP_FUNCTAG:I

.field final STORE_EFSDATA:I

.field final synthetic this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;


# direct methods
.method private constructor <init>(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V
    .locals 4

    .prologue
    const/4 v3, 0x7

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 421
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 423
    iput v3, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;->OEM_SYSDUMP_FUNCTAG:I

    .line 426
    iput v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;->OEM_LOGCAT_MAIN:I

    .line 427
    iput v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;->OEM_LOGCAT_RADIO:I

    .line 428
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;->OEM_DUMPSTATE:I

    .line 429
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;->OEM_KERNEL_LOG:I

    .line 430
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;->OEM_LOGCAT_CLEAR:I

    .line 431
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;->OEM_DBG_STATE_GET:I

    .line 432
    iput v3, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;->OEM_ENABLE_LOG:I

    .line 433
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;->OEM_IPC_DUMP_LOG:I

    .line 434
    const/16 v0, 0x9

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;->OEM_IPC_DUMP_BIN:I

    .line 435
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;->OEM_RAMDUMP_MODE:I

    .line 436
    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;->OEM_RAMDUMP_STATE_GET:I

    .line 437
    const/16 v0, 0xe

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;->OEM_DPRAMDUMP_ON:I

    .line 438
    const/16 v0, 0x11

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;->OEM_NV_BACKUP:I

    .line 440
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;->NV_DELETE:I

    .line 441
    iput v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;->NV_BACKUP:I

    .line 442
    iput v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;->STORE_EFSDATA:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;
    .param p2, "x1"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;

    .prologue
    .line 421
    invoke-direct {p0, p1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;-><init>(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V

    return-void
.end method


# virtual methods
.method StartNVBackupData(II)[B
    .locals 6
    .param p1, "cmd"    # I
    .param p2, "sub"    # I

    .prologue
    .line 470
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 471
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 472
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v3, 0x5

    .line 475
    .local v3, "fileSize":I
    const/4 v4, 0x7

    :try_start_0
    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 476
    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 477
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 478
    invoke-virtual {v1, p2}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 484
    if-eqz v1, :cond_0

    .line 486
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 491
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    :cond_1
    :goto_1
    return-object v4

    .line 480
    :catch_0
    move-exception v2

    .line 481
    .local v2, "e":Ljava/io/IOException;
    :try_start_2
    const-string v4, "RilDump"

    const-string v5, "IOException in getServMQueryData!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 482
    const/4 v4, 0x0

    .line 484
    if-eqz v1, :cond_1

    .line 486
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 487
    :catch_1
    move-exception v5

    goto :goto_1

    .line 484
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    if-eqz v1, :cond_2

    .line 486
    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 488
    :cond_2
    :goto_2
    throw v4

    .line 487
    :catch_2
    move-exception v4

    goto :goto_0

    :catch_3
    move-exception v5

    goto :goto_2
.end method

.method StartSysDumpData(I)[B
    .locals 6
    .param p1, "cmd"    # I

    .prologue
    .line 446
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 447
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 448
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/16 v3, 0x8

    .line 451
    .local v3, "fileSize":I
    const/4 v4, 0x7

    :try_start_0
    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 452
    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 453
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 454
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    iget-object v4, v4, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->month:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 455
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    iget-object v4, v4, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->day:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 456
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    iget-object v4, v4, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->hour:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 457
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    iget-object v4, v4, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->min:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 465
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    :goto_0
    return-object v4

    .line 460
    :catch_0
    move-exception v2

    .line 461
    .local v2, "e":Ljava/io/IOException;
    const-string v4, "RilDump"

    const-string v5, "IOException in getServMQueryData!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    const/4 v4, 0x0

    goto :goto_0
.end method
