.class public Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;
.super Landroid/app/Activity;
.source "NandFlashHeaderRead.java"


# static fields
.field private static mParsedUniqueNumber:Ljava/lang/StringBuilder;

.field private static mUniqueNumberBuilder:Ljava/lang/StringBuilder;


# instance fields
.field private final CHAR_TO_NUMBER:I

.field private final NUMBERCHAR_TO_NUMBER:I

.field private final STATUS_START:I

.field private mBackButton:Landroid/widget/Button;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mClicked:Landroid/view/View$OnClickListener;

.field private mUniqueNumber:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 35
    const/16 v0, 0x30

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->NUMBERCHAR_TO_NUMBER:I

    .line 36
    const/16 v0, 0x37

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->CHAR_TO_NUMBER:I

    .line 37
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->STATUS_START:I

    .line 149
    new-instance v0, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead$1;

    invoke-direct {v0, p0}, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead$1;-><init>(Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mClicked:Landroid/view/View$OnClickListener;

    .line 188
    new-instance v0, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead$2;

    invoke-direct {v0, p0}, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead$2;-><init>(Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public static GetTextFromFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "filepath"    # Ljava/lang/String;

    .prologue
    .line 63
    const-string v5, ""

    .line 64
    .local v5, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 66
    .local v0, "buf":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/FileReader;

    invoke-direct {v6, p0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    const/16 v7, 0x1fa0

    invoke-direct {v1, v6, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    if-eqz v1, :cond_3

    .line 69
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 72
    .local v4, "line":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 73
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 74
    if-nez v5, :cond_3

    .line 75
    const-string v6, ""
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_b
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 89
    if-eqz v1, :cond_0

    .line 91
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    :goto_0
    move-object v0, v1

    .line 100
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "line":Ljava/lang/String;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :goto_1
    return-object v6

    .line 92
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v4    # "line":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 93
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 78
    .end local v2    # "e":Ljava/io/IOException;
    :cond_1
    :try_start_3
    const-string v6, ""
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_c
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_b
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_a
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 89
    if-eqz v1, :cond_2

    .line 91
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_2
    :goto_2
    move-object v0, v1

    .line 94
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_1

    .line 92
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_1
    move-exception v2

    .line 93
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 89
    .end local v2    # "e":Ljava/io/IOException;
    .end local v4    # "line":Ljava/lang/String;
    :cond_3
    if-eqz v1, :cond_7

    .line 91
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    move-object v0, v1

    .line 97
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_4
    :goto_3
    if-nez v5, :cond_6

    .line 98
    const-string v6, ""

    goto :goto_1

    .line 92
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_2
    move-exception v2

    .line 93
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 94
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3

    .line 82
    .end local v2    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v3

    .line 83
    .local v3, "ex":Ljava/io/FileNotFoundException;
    :goto_4
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 89
    if-eqz v0, :cond_4

    .line 91
    :try_start_7
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_3

    .line 92
    :catch_4
    move-exception v2

    .line 93
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 84
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "ex":Ljava/io/FileNotFoundException;
    :catch_5
    move-exception v2

    .line 85
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_5
    :try_start_8
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 89
    if-eqz v0, :cond_4

    .line 91
    :try_start_9
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    goto :goto_3

    .line 92
    :catch_6
    move-exception v2

    .line 93
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 86
    .end local v2    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v2

    .line 87
    .local v2, "e":Ljava/lang/NullPointerException;
    :goto_6
    :try_start_a
    invoke-virtual {v2}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 89
    if-eqz v0, :cond_4

    .line 91
    :try_start_b
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    goto :goto_3

    .line 92
    :catch_8
    move-exception v2

    .line 93
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 89
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_7
    if-eqz v0, :cond_5

    .line 91
    :try_start_c
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 94
    :cond_5
    :goto_8
    throw v6

    .line 92
    :catch_9
    move-exception v2

    .line 93
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .end local v2    # "e":Ljava/io/IOException;
    :cond_6
    move-object v6, v5

    .line 100
    goto :goto_1

    .line 89
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v6

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_7

    .line 86
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_a
    move-exception v2

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_6

    .line 84
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_b
    move-exception v2

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_5

    .line 82
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_c
    move-exception v3

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_4

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :cond_7
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3
.end method

.method private HexToDecimal([C)I
    .locals 8
    .param p1, "ascii"    # [C

    .prologue
    const/16 v7, 0x41

    const/16 v6, 0x39

    const/16 v5, 0x30

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 173
    const/4 v0, 0x0

    .line 174
    .local v0, "result":I
    aget-char v1, p1, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gt v5, v1, :cond_2

    aget-char v1, p1, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gt v1, v6, :cond_2

    .line 175
    aget-char v1, p1, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x30

    mul-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    .line 180
    :cond_0
    :goto_0
    aget-char v1, p1, v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gt v5, v1, :cond_3

    aget-char v1, p1, v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gt v1, v6, :cond_3

    .line 181
    aget-char v1, p1, v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x30

    add-int/2addr v0, v1

    .line 185
    :cond_1
    :goto_1
    return v0

    .line 176
    :cond_2
    aget-char v1, p1, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gt v7, v1, :cond_0

    aget-char v1, p1, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x45

    if-gt v1, v2, :cond_0

    .line 177
    aget-char v1, p1, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x37

    mul-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    goto :goto_0

    .line 182
    :cond_3
    aget-char v1, p1, v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gt v7, v1, :cond_1

    aget-char v1, p1, v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x45

    if-gt v1, v2, :cond_1

    .line 183
    aget-char v1, p1, v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x37

    add-int/2addr v0, v1

    goto :goto_1
.end method

.method private ReadUniqueNumber()V
    .locals 14

    .prologue
    const/16 v13, 0x1c

    const/16 v12, 0x14

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    .line 104
    const-string v6, "NandFlashHeaderRead"

    const-string v7, "ReadUniqueNumber"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    new-instance v3, Ljava/io/File;

    const-string v6, "/sys/block/mmcblk0/device/cid"

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 108
    .local v3, "cid_file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 110
    const-string v6, "/sys/block/mmcblk0/device/cid"

    invoke-static {v6}, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->GetTextFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 111
    .local v2, "cid":Ljava/lang/String;
    const-string v6, "/sys/block/mmcblk0/device/name"

    invoke-static {v6}, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->GetTextFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 112
    .local v5, "memory_name":Ljava/lang/String;
    const-string v1, ""

    .line 113
    .local v1, "Unique_Number":Ljava/lang/String;
    const-string v4, ""

    .line 114
    .local v4, "eMMC":Ljava/lang/String;
    const-string v0, ""

    .line 116
    .local v0, "PNM":Ljava/lang/String;
    const-string v6, "NandFlashHeaderRead"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "cid : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", memory_name : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "c"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 119
    invoke-virtual {v2, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 121
    const-string v6, "15"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 122
    invoke-virtual {v5, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 131
    :cond_0
    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 134
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x12

    invoke-virtual {v2, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 137
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 140
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x1e

    invoke-virtual {v2, v13, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 142
    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 144
    const-string v6, "NandFlashHeaderRead"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unique Number : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mUniqueNumber:Landroid/widget/TextView;

    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    .end local v0    # "PNM":Ljava/lang/String;
    .end local v1    # "Unique_Number":Ljava/lang/String;
    .end local v2    # "cid":Ljava/lang/String;
    .end local v4    # "eMMC":Ljava/lang/String;
    .end local v5    # "memory_name":Ljava/lang/String;
    :cond_1
    return-void

    .line 123
    .restart local v0    # "PNM":Ljava/lang/String;
    .restart local v1    # "Unique_Number":Ljava/lang/String;
    .restart local v2    # "cid":Ljava/lang/String;
    .restart local v4    # "eMMC":Ljava/lang/String;
    .restart local v5    # "memory_name":Ljava/lang/String;
    :cond_2
    const-string v6, "02"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, "45"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 124
    :cond_3
    const/4 v6, 0x5

    invoke-virtual {v5, v11, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 125
    :cond_4
    const-string v6, "11"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_5

    const-string v6, "90"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 126
    :cond_5
    const/4 v6, 0x1

    invoke-virtual {v5, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 127
    :cond_6
    const-string v6, "FE"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 128
    const/4 v6, 0x4

    const/4 v7, 0x6

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mBackButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->stringReset()V

    return-void
.end method

.method static synthetic access$200()Ljava/lang/StringBuilder;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mUniqueNumberBuilder:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->parseUniqueNumber()V

    return-void
.end method

.method static synthetic access$400()Ljava/lang/StringBuilder;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mParsedUniqueNumber:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->printUniqueNumber()V

    return-void
.end method

.method private init()V
    .locals 3

    .prologue
    .line 48
    const v1, 0x7f080013

    invoke-virtual {p0, v1}, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mUniqueNumber:Landroid/widget/TextView;

    .line 50
    const v1, 0x7f080014

    invoke-virtual {p0, v1}, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mBackButton:Landroid/widget/Button;

    .line 51
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mBackButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sput-object v1, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mUniqueNumberBuilder:Ljava/lang/StringBuilder;

    .line 54
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 55
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "com.android.samsungtest.DGS_UniqueNumber"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 56
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 58
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sput-object v1, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mParsedUniqueNumber:Ljava/lang/StringBuilder;

    .line 60
    return-void
.end method

.method private parseUniqueNumber()V
    .locals 5

    .prologue
    .line 162
    const/4 v2, 0x2

    new-array v0, v2, [C

    .line 163
    .local v0, "asciiByte":[C
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mUniqueNumberBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    if-ge v1, v2, :cond_0

    .line 165
    const/4 v2, 0x0

    sget-object v3, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mUniqueNumberBuilder:Ljava/lang/StringBuilder;

    mul-int/lit8 v4, v1, 0x2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v3

    aput-char v3, v0, v2

    .line 166
    const/4 v2, 0x1

    sget-object v3, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mUniqueNumberBuilder:Ljava/lang/StringBuilder;

    mul-int/lit8 v4, v1, 0x2

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v3

    aput-char v3, v0, v2

    .line 168
    sget-object v2, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mParsedUniqueNumber:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->HexToDecimal([C)I

    move-result v3

    int-to-char v3, v3

    invoke-static {v3}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 170
    :cond_0
    return-void
.end method

.method private printUniqueNumber()V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mUniqueNumber:Landroid/widget/TextView;

    sget-object v1, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mParsedUniqueNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    return-void
.end method

.method private stringReset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 207
    sget-object v0, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mUniqueNumberBuilder:Ljava/lang/StringBuilder;

    sget-object v1, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mUniqueNumberBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 208
    sget-object v0, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mParsedUniqueNumber:Ljava/lang/StringBuilder;

    sget-object v1, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mParsedUniqueNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 209
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 41
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    const v0, 0x7f030008

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->setContentView(I)V

    .line 43
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->init()V

    .line 44
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->ReadUniqueNumber()V

    .line 45
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 213
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 214
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 215
    return-void
.end method
