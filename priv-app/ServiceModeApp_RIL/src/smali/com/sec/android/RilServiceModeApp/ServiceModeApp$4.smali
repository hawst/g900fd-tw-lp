.class Lcom/sec/android/RilServiceModeApp/ServiceModeApp$4;
.super Ljava/lang/Object;
.source "ServiceModeApp.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;


# direct methods
.method constructor <init>(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)V
    .locals 0

    .prologue
    .line 950
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$4;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 16
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 953
    const-string v2, "ServiceModeApp_RIL"

    const-string v3, "In setOnItemClickListener!!"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 954
    const-string v2, "ServiceModeApp_RIL"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "position is :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 955
    const-string v2, "ServiceModeApp_RIL"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "id is : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p4

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 958
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$4;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$000(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)[Ljava/lang/String;

    move-result-object v2

    aget-object v13, v2, p3

    .line 962
    .local v13, "row":Ljava/lang/String;
    const-string v2, "["

    invoke-virtual {v13, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "]"

    invoke-virtual {v13, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 963
    const/4 v11, 0x0

    .line 964
    .local v11, "line":I
    const-string v2, "]"

    invoke-virtual {v13, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v10

    .line 965
    .local v10, "index2":I
    const-string v2, "["

    invoke-virtual {v13, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    .line 966
    .local v9, "index1":I
    const-string v2, "ServiceModeApp_RIL"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "index2 is : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 967
    const-string v2, "ServiceModeApp_RIL"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "index1 is : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 970
    const-string v14, "  "

    .line 971
    .local v14, "space":Ljava/lang/String;
    const-string v15, "*"

    .line 973
    .local v15, "star":Ljava/lang/String;
    add-int/lit8 v2, v9, 0x1

    invoke-virtual {v13, v2, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 974
    .local v12, "num":Ljava/lang/String;
    const-string v2, "ServiceModeApp_RIL"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "num is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 975
    const/4 v2, 0x0

    invoke-virtual {v12, v2}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 977
    .local v6, "line_num":C
    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v12, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 978
    :cond_0
    const-string v2, "ServiceModeApp_RIL"

    const-string v3, "Empty number : [  ] or *"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 995
    .end local v6    # "line_num":C
    .end local v9    # "index1":I
    .end local v10    # "index2":I
    .end local v11    # "line":I
    .end local v12    # "num":Ljava/lang/String;
    .end local v14    # "space":Ljava/lang/String;
    .end local v15    # "star":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 982
    .restart local v6    # "line_num":C
    .restart local v9    # "index1":I
    .restart local v10    # "index2":I
    .restart local v11    # "line":I
    .restart local v12    # "num":Ljava/lang/String;
    .restart local v14    # "space":Ljava/lang/String;
    .restart local v15    # "star":Ljava/lang/String;
    :cond_2
    const-string v2, "276633683782"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$4;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$100(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 984
    const/16 v2, 0x30

    if-le v6, v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$4;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$000(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, p3

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 985
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$4;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    iget v3, v2, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->level:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->level:I

    .line 988
    :cond_3
    const-string v2, "ServiceModeApp_RIL"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "line is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 989
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$4;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$4;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;
    invoke-static {v3}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$400(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v3, 0x3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$4;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;
    invoke-static {v4}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$400(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$4;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$400(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$4;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;
    invoke-static {v7}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$400(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$4;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C
    invoke-static {v8}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$500(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)C

    move-result v8

    # invokes: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V
    invoke-static/range {v2 .. v8}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$600(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;CCCCCC)V

    .line 992
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$4;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    const/4 v3, 0x1

    # setter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->calledByKeyPress:Z
    invoke-static {v2, v3}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$1102(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;Z)Z

    goto/16 :goto_0
.end method
