.class Lcom/sec/android/RilServiceModeApp/GcfModeSettings$1;
.super Ljava/lang/Object;
.source "GcfModeSettings.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/RilServiceModeApp/GcfModeSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/RilServiceModeApp/GcfModeSettings;


# direct methods
.method constructor <init>(Lcom/sec/android/RilServiceModeApp/GcfModeSettings;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/GcfModeSettings$1;->this$0:Lcom/sec/android/RilServiceModeApp/GcfModeSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    .line 84
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/GcfModeSettings$1;->this$0:Lcom/sec/android/RilServiceModeApp/GcfModeSettings;

    # getter for: Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->mGcfMode:Landroid/widget/Button;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->access$000(Lcom/sec/android/RilServiceModeApp/GcfModeSettings;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 85
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/GcfModeSettings$1;->this$0:Lcom/sec/android/RilServiceModeApp/GcfModeSettings;

    # getter for: Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->bGcfMode:Z
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->access$100(Lcom/sec/android/RilServiceModeApp/GcfModeSettings;)Z

    move-result v2

    if-ne v2, v4, :cond_1

    .line 86
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/GcfModeSettings$1;->this$0:Lcom/sec/android/RilServiceModeApp/GcfModeSettings;

    # getter for: Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->mGcfMode:Landroid/widget/Button;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->access$000(Lcom/sec/android/RilServiceModeApp/GcfModeSettings;)Landroid/widget/Button;

    move-result-object v2

    const-string v3, "Gcf Mode Disabled"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/GcfModeSettings$1;->this$0:Lcom/sec/android/RilServiceModeApp/GcfModeSettings;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->bGcfMode:Z
    invoke-static {v2, v3}, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->access$102(Lcom/sec/android/RilServiceModeApp/GcfModeSettings;Z)Z

    .line 93
    :goto_0
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/GcfModeSettings$1;->this$0:Lcom/sec/android/RilServiceModeApp/GcfModeSettings;

    # getter for: Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->bGcfMode:Z
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->access$100(Lcom/sec/android/RilServiceModeApp/GcfModeSettings;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v1, "enabled"

    .line 95
    .local v1, "mode":Ljava/lang/String;
    :goto_1
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.app.GCF_MODE_ACTION"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 96
    .local v0, "i":Landroid/content/Intent;
    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 97
    const-string v2, "key"

    const-string v3, "gcf_mode"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 98
    const-string v2, "mode"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 99
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/GcfModeSettings$1;->this$0:Lcom/sec/android/RilServiceModeApp/GcfModeSettings;

    invoke-virtual {v2, v0}, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->sendBroadcast(Landroid/content/Intent;)V

    .line 101
    .end local v0    # "i":Landroid/content/Intent;
    .end local v1    # "mode":Ljava/lang/String;
    :cond_0
    return-void

    .line 89
    :cond_1
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/GcfModeSettings$1;->this$0:Lcom/sec/android/RilServiceModeApp/GcfModeSettings;

    # getter for: Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->mGcfMode:Landroid/widget/Button;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->access$000(Lcom/sec/android/RilServiceModeApp/GcfModeSettings;)Landroid/widget/Button;

    move-result-object v2

    const-string v3, "Gcf Mode Enabled"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/GcfModeSettings$1;->this$0:Lcom/sec/android/RilServiceModeApp/GcfModeSettings;

    # setter for: Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->bGcfMode:Z
    invoke-static {v2, v4}, Lcom/sec/android/RilServiceModeApp/GcfModeSettings;->access$102(Lcom/sec/android/RilServiceModeApp/GcfModeSettings;Z)Z

    goto :goto_0

    .line 93
    :cond_2
    const-string v1, "disabled"

    goto :goto_1
.end method
