.class public Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;
.super Landroid/app/Activity;
.source "SetupWizardSkip.java"


# instance fields
.field private SkipMode:Z

.field private bSkipMode:Z

.field private mClicked:Landroid/view/View$OnClickListener;

.field private mSkipMode:Landroid/widget/Button;

.field private phone:Lcom/android/internal/telephony/Phone;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->phone:Lcom/android/internal/telephony/Phone;

    .line 91
    new-instance v0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip$1;

    invoke-direct {v0, p0}, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip$1;-><init>(Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->mClicked:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->mSkipMode:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;)Lcom/android/internal/telephony/Phone;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->phone:Lcom/android/internal/telephony/Phone;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->bSkipMode:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->bSkipMode:Z

    return p1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 56
    const-string v2, "SetupWizardSkipSettings"

    const-string v3, "onCreate "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->phone:Lcom/android/internal/telephony/Phone;

    .line 59
    const v2, 0x7f030004

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->setContentView(I)V

    .line 61
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->phone:Lcom/android/internal/telephony/Phone;

    invoke-interface {v2}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 62
    .local v1, "prefs":Landroid/content/SharedPreferences;
    const-string v2, "setup_wizard_skip"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->SkipMode:Z

    .line 64
    const v2, 0x7f08000d

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->mSkipMode:Landroid/widget/Button;

    .line 66
    iget-boolean v2, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->SkipMode:Z

    if-ne v2, v5, :cond_0

    .line 67
    const-string v2, "SetupWizardSkipSettings"

    const-string v3, "Default = Enable "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->mSkipMode:Landroid/widget/Button;

    const-string v3, "Setup Wizard Skip : Enabled"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 69
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 70
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "setup_wizard_skip"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 71
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 73
    iput-boolean v5, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->bSkipMode:Z

    .line 83
    :goto_0
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->mSkipMode:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    return-void

    .line 75
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    const-string v2, "SetupWizardSkipSettings"

    const-string v3, "Default = Disable "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->mSkipMode:Landroid/widget/Button;

    const-string v3, "Setup Wizard Skip : Disabled"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 77
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 78
    .restart local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "setup_wizard_skip"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 79
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 81
    iput-boolean v4, p0, Lcom/sec/android/RilServiceModeApp/SetupWizardSkip;->bSkipMode:Z

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 88
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 89
    return-void
.end method
