.class Lcom/sec/android/RilServiceModeApp/ServiceModeApp$5;
.super Ljava/lang/Object;
.source "ServiceModeApp.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->onMenuItemSelected(ILandroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

.field final synthetic val$input:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 1089
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$5;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    iput-object p2, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$5;->val$input:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 17
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 1091
    const/4 v14, 0x0

    .line 1093
    .local v14, "DataLen":I
    const-string v1, "ServiceModeApp_RIL"

    const-string v2, "OK Button clicked"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1095
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$5;->val$input:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v16

    .line 1096
    .local v16, "keyData":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v14

    .line 1098
    if-eqz v14, :cond_1

    .line 1099
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_0
    if-ge v15, v14, :cond_0

    .line 1100
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/lang/String;->charAt(I)C

    move-result v13

    .line 1101
    .local v13, "CharAtOfString":C
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$5;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # invokes: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->ParseKeyValueForModem(C)C
    invoke-static {v1, v13}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$1200(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;C)C

    move-result v5

    .line 1103
    .local v5, "key":C
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$5;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$5;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$400(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$5;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;
    invoke-static {v3}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$400(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$5;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;
    invoke-static {v4}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$400(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$5;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;
    invoke-static {v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$400(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$5;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C
    invoke-static {v7}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$500(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)C

    move-result v7

    # invokes: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V
    invoke-static/range {v1 .. v7}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$600(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;CCCCCC)V

    .line 1099
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 1108
    .end local v5    # "key":C
    .end local v13    # "CharAtOfString":C
    :cond_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$5;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$5;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$400(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v7, 0x3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$5;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$400(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$5;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$400(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v9, 0x0

    const/16 v10, 0x53

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$5;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$400(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$5;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$500(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)C

    move-result v12

    # invokes: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V
    invoke-static/range {v6 .. v12}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$600(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;CCCCCC)V

    .line 1111
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$5;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->calledByKeyPress:Z
    invoke-static {v1, v2}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$1102(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;Z)Z

    .line 1113
    .end local v15    # "i":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$5;->val$input:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1114
    return-void
.end method
