.class Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;
.super Ljava/lang/Object;
.source "TerminalMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/RilServiceModeApp/TerminalMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OemCommands"
.end annotation


# instance fields
.field final OEM_HIDDEN_FUNCTAG:C

.field final OEM_HM_END_TEST_CALL_MESSAGE:C

.field final OEM_HM_TEST_CALL_MESSAGE:C

.field final OEM_HM_TYPE_TEST_CALL:C

.field final OEM_SERVM_FUNCTAG:C

.field final OEM_SM_ACTION:C

.field final OEM_SM_DUMMY:C

.field final OEM_SM_END_MODE_MESSAGE:C

.field final OEM_SM_ENTER_MODE_MESSAGE:C

.field final OEM_SM_PROCESS_KEY_MESSAGE:C

.field final OEM_SM_QUERY:C

.field final OEM_SM_TYPE_MONITOR:C

.field final OEM_SM_TYPE_NAM:C

.field final OEM_SM_TYPE_OP_SP_MODE:C

.field final OEM_SM_TYPE_PHONE_TEST:C

.field final OEM_SM_TYPE_SUB_CDMA_TESTMODE_ENTER:C

.field final OEM_SM_TYPE_SUB_ENTER:C

.field final OEM_SM_TYPE_SUB_FTA_HW_VERSION_ENTER:C

.field final OEM_SM_TYPE_SUB_FTA_SW_VERSION_ENTER:C

.field final OEM_SM_TYPE_SUB_NAM_ADVANCED_EDIT:C

.field final OEM_SM_TYPE_SUB_NAM_BASIC_EDIT:C

.field final OEM_SM_TYPE_SUB_NAM_EDIT:C

.field final OEM_SM_TYPE_SUB_SW_VERSION_ENTER:C

.field final OEM_SM_TYPE_SUB_TST_FTA_HW_VERSION_ENTER:C

.field final OEM_SM_TYPE_SUB_TST_FTA_SW_VERSION_ENTER:C

.field final OEM_SM_TYPE_TEST_AUTO:C

.field final OEM_SM_TYPE_TEST_MANUAL:C

.field final synthetic this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;


# direct methods
.method private constructor <init>(Lcom/sec/android/RilServiceModeApp/TerminalMode;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 202
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 204
    iput-char v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_SERVM_FUNCTAG:C

    .line 205
    const/16 v0, 0x51

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_HIDDEN_FUNCTAG:C

    .line 208
    iput-char v2, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_SM_DUMMY:C

    .line 209
    iput-char v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_SM_ENTER_MODE_MESSAGE:C

    .line 210
    iput-char v3, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_SM_END_MODE_MESSAGE:C

    .line 211
    iput-char v4, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_SM_PROCESS_KEY_MESSAGE:C

    .line 214
    iput-char v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_SM_TYPE_TEST_MANUAL:C

    .line 215
    iput-char v3, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_SM_TYPE_TEST_AUTO:C

    .line 216
    iput-char v4, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_SM_TYPE_NAM:C

    .line 217
    iput-char v5, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_SM_TYPE_MONITOR:C

    .line 218
    const/4 v0, 0x5

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_SM_TYPE_PHONE_TEST:C

    .line 219
    const/4 v0, 0x6

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_SM_TYPE_OP_SP_MODE:C

    .line 222
    iput-char v5, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_HM_TEST_CALL_MESSAGE:C

    .line 223
    const/16 v0, 0x9

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_HM_END_TEST_CALL_MESSAGE:C

    .line 226
    const/4 v0, 0x7

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_HM_TYPE_TEST_CALL:C

    .line 229
    iput-char v2, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_SM_TYPE_SUB_ENTER:C

    .line 230
    iput-char v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_SM_TYPE_SUB_SW_VERSION_ENTER:C

    .line 231
    iput-char v3, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_SM_TYPE_SUB_FTA_SW_VERSION_ENTER:C

    .line 232
    iput-char v4, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_SM_TYPE_SUB_FTA_HW_VERSION_ENTER:C

    .line 233
    const/16 v0, 0x1002

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_SM_TYPE_SUB_TST_FTA_SW_VERSION_ENTER:C

    .line 234
    const/16 v0, 0x1003

    iput-char v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_SM_TYPE_SUB_TST_FTA_HW_VERSION_ENTER:C

    .line 237
    iput-char v2, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_SM_TYPE_SUB_CDMA_TESTMODE_ENTER:C

    .line 241
    iput-char v3, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_SM_TYPE_SUB_NAM_EDIT:C

    .line 243
    iput-char v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_SM_TYPE_SUB_NAM_BASIC_EDIT:C

    .line 245
    iput-char v4, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_SM_TYPE_SUB_NAM_ADVANCED_EDIT:C

    .line 251
    iput-char v2, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_SM_ACTION:C

    .line 252
    iput-char v1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->OEM_SM_QUERY:C

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/RilServiceModeApp/TerminalMode;Lcom/sec/android/RilServiceModeApp/TerminalMode$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/RilServiceModeApp/TerminalMode;
    .param p2, "x1"    # Lcom/sec/android/RilServiceModeApp/TerminalMode$1;

    .prologue
    .line 202
    invoke-direct {p0, p1}, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;-><init>(Lcom/sec/android/RilServiceModeApp/TerminalMode;)V

    return-void
.end method


# virtual methods
.method getServMEnterData(CCCC)[B
    .locals 7
    .param p1, "svcMode"    # C
    .param p2, "modeType"    # C
    .param p3, "subType"    # C
    .param p4, "query"    # C

    .prologue
    .line 255
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 256
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 257
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/16 v3, 0x8

    .line 259
    .local v3, "fileSize":C
    const/4 v4, 0x1

    :try_start_0
    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 260
    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 261
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 263
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    iget-char v4, v4, Lcom/sec/android/RilServiceModeApp/TerminalMode;->MODEM_CDMA:C

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 264
    invoke-virtual {v1, p2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 265
    invoke-virtual {v1, p3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 266
    invoke-virtual {v1, p4}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 272
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 278
    :goto_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    :goto_1
    return-object v4

    .line 273
    :catch_0
    move-exception v2

    .line 274
    .local v2, "e":Ljava/io/IOException;
    const-string v4, "TerminalMode"

    const-string v5, "IOException in getServMEnterData!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 267
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 268
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_2
    const-string v4, "TerminalMode"

    const-string v5, "IOException in getServMQueryData!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 269
    const/4 v4, 0x0

    .line 272
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 273
    :catch_2
    move-exception v2

    .line 274
    const-string v5, "TerminalMode"

    const-string v6, "IOException in getServMEnterData!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 271
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 272
    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 275
    :goto_2
    throw v4

    .line 273
    :catch_3
    move-exception v2

    .line 274
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v5, "TerminalMode"

    const-string v6, "IOException in getServMEnterData!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method setEndModeData(CC)[B
    .locals 7
    .param p1, "svcMode"    # C
    .param p2, "modeType"    # C

    .prologue
    .line 309
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 310
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 311
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v3, 0x6

    .line 313
    .local v3, "fileSize":I
    const/4 v4, 0x1

    :try_start_0
    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 314
    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 315
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 317
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    iget-char v4, v4, Lcom/sec/android/RilServiceModeApp/TerminalMode;->MODEM_CDMA:C

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 318
    invoke-virtual {v1, p2}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 324
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 330
    :goto_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    :goto_1
    return-object v4

    .line 325
    :catch_0
    move-exception v2

    .line 326
    .local v2, "e":Ljava/io/IOException;
    const-string v4, "TerminalMode"

    const-string v5, "IOException in setEndModeData!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 319
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 320
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_2
    const-string v4, "TerminalMode"

    const-string v5, "IOException in getServMQueryData!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 321
    const/4 v4, 0x0

    .line 324
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 325
    :catch_2
    move-exception v2

    .line 326
    const-string v5, "TerminalMode"

    const-string v6, "IOException in setEndModeData!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 323
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 324
    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 327
    :goto_2
    throw v4

    .line 325
    :catch_3
    move-exception v2

    .line 326
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v5, "TerminalMode"

    const-string v6, "IOException in setEndModeData!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method setEndTestCallMode(CCC)[B
    .locals 7
    .param p1, "svcMode"    # C
    .param p2, "modeType"    # C
    .param p3, "subType"    # C

    .prologue
    .line 357
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 358
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 359
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v3, 0x5

    .line 361
    .local v3, "fileSize":C
    const/16 v4, 0x51

    :try_start_0
    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 362
    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 363
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 364
    invoke-virtual {v1, p3}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 370
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 376
    :goto_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    :goto_1
    return-object v4

    .line 371
    :catch_0
    move-exception v2

    .line 372
    .local v2, "e":Ljava/io/IOException;
    const-string v4, "TerminalMode"

    const-string v5, "IOException in setEndTestCallMode!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 365
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 366
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_2
    const-string v4, "TerminalMode"

    const-string v5, "IOException in getServMQueryData!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 367
    const/4 v4, 0x0

    .line 370
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 371
    :catch_2
    move-exception v2

    .line 372
    const-string v5, "TerminalMode"

    const-string v6, "IOException in setEndTestCallMode!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 369
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 370
    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 373
    :goto_2
    throw v4

    .line 371
    :catch_3
    move-exception v2

    .line 372
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v5, "TerminalMode"

    const-string v6, "IOException in setEndTestCallMode!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method setPressKeyData(CIC)[B
    .locals 7
    .param p1, "svcMode"    # C
    .param p2, "keycode"    # I
    .param p3, "query"    # C

    .prologue
    .line 282
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 283
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 284
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v3, 0x7

    .line 285
    .local v3, "fileSize":I
    const-string v4, "TerminalMode"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "keycode : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    const/4 v4, 0x1

    :try_start_0
    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 288
    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 289
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 291
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$OemCommands;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    iget-char v4, v4, Lcom/sec/android/RilServiceModeApp/TerminalMode;->MODEM_CDMA:C

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 292
    int-to-char v4, p2

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 293
    invoke-virtual {v1, p3}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 305
    :goto_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    :goto_1
    return-object v4

    .line 300
    :catch_0
    move-exception v2

    .line 301
    .local v2, "e":Ljava/io/IOException;
    const-string v4, "TerminalMode"

    const-string v5, "IOException in setPressKeyData!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 294
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 295
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_2
    const-string v4, "TerminalMode"

    const-string v5, "IOException in getServMQueryData!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 296
    const/4 v4, 0x0

    .line 299
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 300
    :catch_2
    move-exception v2

    .line 301
    const-string v5, "TerminalMode"

    const-string v6, "IOException in setPressKeyData!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 298
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 299
    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 302
    :goto_2
    throw v4

    .line 300
    :catch_3
    move-exception v2

    .line 301
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v5, "TerminalMode"

    const-string v6, "IOException in setPressKeyData!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method setTestCallMode(CCC)[B
    .locals 7
    .param p1, "svcMode"    # C
    .param p2, "modeType"    # C
    .param p3, "subType"    # C

    .prologue
    .line 334
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 335
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 336
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v3, 0x5

    .line 338
    .local v3, "fileSize":C
    const/16 v4, 0x51

    :try_start_0
    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 339
    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 340
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 341
    invoke-virtual {v1, p3}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 347
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 353
    :goto_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    :goto_1
    return-object v4

    .line 348
    :catch_0
    move-exception v2

    .line 349
    .local v2, "e":Ljava/io/IOException;
    const-string v4, "TerminalMode"

    const-string v5, "IOException in setTestCallMode!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 342
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 343
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_2
    const-string v4, "TerminalMode"

    const-string v5, "IOException in getServMQueryData!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 344
    const/4 v4, 0x0

    .line 347
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 348
    :catch_2
    move-exception v2

    .line 349
    const-string v5, "TerminalMode"

    const-string v6, "IOException in setTestCallMode!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 346
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 347
    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 350
    :goto_2
    throw v4

    .line 348
    :catch_3
    move-exception v2

    .line 349
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v5, "TerminalMode"

    const-string v6, "IOException in setTestCallMode!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
