.class Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$11;
.super Ljava/lang/Object;
.source "Sec_Ril_Dump.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->changeDataRecoveryTime()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

.field final synthetic val$msg:Landroid/os/Message;

.field final synthetic val$recoveryTime:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Landroid/widget/EditText;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 1406
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$11;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    iput-object p2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$11;->val$recoveryTime:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$11;->val$msg:Landroid/os/Message;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 1408
    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$11;->val$recoveryTime:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1410
    .local v2, "sRecoveryTime":Ljava/lang/String;
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1411
    .local v1, "newDataRecoveryTime":I
    const-string v3, "RilDump"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " newDataRecoveryTime = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1412
    if-lez v1, :cond_0

    .line 1413
    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->dataRecoveryTime:I
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$602(I)I

    .line 1414
    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$11;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    invoke-virtual {v3}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "data_stall_alarm_aggressive_delay_in_ms"

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->dataRecoveryTime:I
    invoke-static {}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$600()I

    move-result v5

    const v6, 0xea60

    mul-int/2addr v5, v6

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1417
    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$11;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Rebbot Device\n\nDATA_STALL_ALARM_AGGRESSIVE_DELAY_IN_MS will set to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->dataRecoveryTime:I
    invoke-static {}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$600()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "min"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->changeDataRecoveryTimeResultMessage(Ljava/lang/String;)V
    invoke-static {v3, v4}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$5800(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)V

    .line 1419
    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$11;->val$msg:Landroid/os/Message;

    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    .line 1426
    .end local v1    # "newDataRecoveryTime":I
    :goto_0
    return-void

    .line 1421
    .restart local v1    # "newDataRecoveryTime":I
    :cond_0
    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$11;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const-string v4, "Input a number 1~9999"

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->changeDataRecoveryTimeResultMessage(Ljava/lang/String;)V
    invoke-static {v3, v4}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$5800(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1423
    .end local v1    # "newDataRecoveryTime":I
    :catch_0
    move-exception v0

    .line 1424
    .local v0, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$11;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const-string v4, "Input a number 1~9999"

    # invokes: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->changeDataRecoveryTimeResultMessage(Ljava/lang/String;)V
    invoke-static {v3, v4}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$5800(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)V

    goto :goto_0
.end method
