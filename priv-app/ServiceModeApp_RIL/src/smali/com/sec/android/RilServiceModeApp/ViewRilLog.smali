.class public Lcom/sec/android/RilServiceModeApp/ViewRilLog;
.super Landroid/app/Activity;
.source "ViewRilLog.java"


# static fields
.field private static LogMessage:Ljava/lang/StringBuilder;

.field private static current_page:I

.field private static in:Ljava/io/RandomAccessFile;

.field private static mFis:Ljava/io/FileInputStream;

.field private static mIsr:Ljava/io/InputStreamReader;

.field private static page:I

.field private static totalPages:I


# instance fields
.field private buf:[B

.field private builder:Landroid/app/AlertDialog$Builder;

.field private fis:Ljava/io/FileInputStream;

.field private fos:Ljava/io/FileOutputStream;

.field private inFile:Ljava/lang/String;

.field public mHandler:Landroid/os/Handler;

.field private mScroller:Landroid/widget/Scroller;

.field private mTextView:Landroid/widget/TextView;

.field private phone:Lcom/android/internal/telephony/Phone;

.field private progressDialog:Landroid/app/ProgressDialog;

.field private rilLog:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 119
    sput-object v1, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->mFis:Ljava/io/FileInputStream;

    .line 120
    sput-object v1, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->mIsr:Ljava/io/InputStreamReader;

    .line 122
    sput-object v1, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->in:Ljava/io/RandomAccessFile;

    .line 124
    sput v0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->page:I

    .line 125
    sput v0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->current_page:I

    .line 126
    sput v0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->totalPages:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 92
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 96
    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->phone:Lcom/android/internal/telephony/Phone;

    .line 100
    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->fis:Ljava/io/FileInputStream;

    .line 101
    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->fos:Ljava/io/FileOutputStream;

    .line 102
    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->rilLog:Ljava/io/File;

    .line 104
    const/16 v0, 0x400

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->buf:[B

    .line 109
    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->mScroller:Landroid/widget/Scroller;

    .line 134
    new-instance v0, Lcom/sec/android/RilServiceModeApp/ViewRilLog$1;

    invoke-direct {v0, p0}, Lcom/sec/android/RilServiceModeApp/ViewRilLog$1;-><init>(Lcom/sec/android/RilServiceModeApp/ViewRilLog;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private DisplayMessageDialog(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 403
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 404
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->builder:Landroid/app/AlertDialog$Builder;

    const v1, 0x104000a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 405
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->builder:Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 406
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 407
    return-void
.end method

.method static synthetic access$000()Ljava/lang/StringBuilder;
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->LogMessage:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method static synthetic access$002(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 0
    .param p0, "x0"    # Ljava/lang/StringBuilder;

    .prologue
    .line 92
    sput-object p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->LogMessage:Ljava/lang/StringBuilder;

    return-object p0
.end method

.method static synthetic access$100(Lcom/sec/android/RilServiceModeApp/ViewRilLog;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/ViewRilLog;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->mTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200()Ljava/io/RandomAccessFile;
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->in:Ljava/io/RandomAccessFile;

    return-object v0
.end method

.method static synthetic access$300()I
    .locals 1

    .prologue
    .line 92
    sget v0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->totalPages:I

    return v0
.end method

.method static synthetic access$302(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 92
    sput p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->totalPages:I

    return p0
.end method

.method static synthetic access$400(Lcom/sec/android/RilServiceModeApp/ViewRilLog;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/ViewRilLog;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->DisplayMessageDialog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/RilServiceModeApp/ViewRilLog;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/ViewRilLog;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->progressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/RilServiceModeApp/ViewRilLog;Ljava/lang/StringBuilder;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/ViewRilLog;
    .param p1, "x1"    # Ljava/lang/StringBuilder;

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->sendParsedString(Ljava/lang/StringBuilder;)V

    return-void
.end method

.method static synthetic access$700()I
    .locals 1

    .prologue
    .line 92
    sget v0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->page:I

    return v0
.end method

.method private moveToPage()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 326
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->builder:Landroid/app/AlertDialog$Builder;

    .line 328
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->progressDialog:Landroid/app/ProgressDialog;

    .line 329
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->progressDialog:Landroid/app/ProgressDialog;

    const-string v3, "Loading Ril Log..."

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 330
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v8}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 331
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 332
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->show()V

    .line 336
    :try_start_0
    new-instance v2, Ljava/io/RandomAccessFile;

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->inFile:Ljava/lang/String;

    const-string v4, "r"

    invoke-direct {v2, v3, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v2, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->in:Ljava/io/RandomAccessFile;

    .line 337
    const-string v2, "ViewRilLog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->inFile:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->in:Ljava/io/RandomAccessFile;

    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v4

    const-wide/16 v6, 0x400

    div-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Kb"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/sec/android/RilServiceModeApp/ViewRilLog$3;

    invoke-direct {v3, p0}, Lcom/sec/android/RilServiceModeApp/ViewRilLog$3;-><init>(Lcom/sec/android/RilServiceModeApp/ViewRilLog;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 395
    :goto_0
    sget v2, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->page:I

    add-int/lit8 v2, v2, 0x1

    sput v2, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->current_page:I

    .line 396
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget v3, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->current_page:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->totalPages:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "pages"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 398
    .local v1, "message":Ljava/lang/String;
    invoke-static {p0, v1, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 400
    return-void

    .line 385
    .end local v1    # "message":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 386
    .local v0, "e":Ljava/io/FileNotFoundException;
    const-string v2, "File open fail!"

    invoke-direct {p0, v2}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->DisplayMessageDialog(Ljava/lang/String;)V

    .line 387
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 388
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0

    .line 389
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 390
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "File open fail!"

    invoke-direct {p0, v2}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->DisplayMessageDialog(Ljava/lang/String;)V

    .line 391
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 392
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0
.end method

.method private sendParsedString(Ljava/lang/StringBuilder;)V
    .locals 3
    .param p1, "rilLogMessage"    # Ljava/lang/StringBuilder;

    .prologue
    .line 259
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3f2

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 260
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 262
    const-string v1, "ViewRilLog"

    const-string v2, "sendParsedString..."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x1

    const/4 v5, 0x0

    .line 167
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 169
    const v3, 0x7f030010

    invoke-virtual {p0, v3}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->setContentView(I)V

    .line 171
    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 172
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "key"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 174
    .local v2, "key":Ljava/lang/String;
    const v3, 0x7f08003e

    invoke-virtual {p0, v3}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->mTextView:Landroid/widget/TextView;

    .line 175
    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 177
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->builder:Landroid/app/AlertDialog$Builder;

    .line 179
    new-instance v3, Landroid/app/ProgressDialog;

    invoke-direct {v3, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->progressDialog:Landroid/app/ProgressDialog;

    .line 180
    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->progressDialog:Landroid/app/ProgressDialog;

    const-string v4, "Loading Ril Log..."

    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 181
    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3, v10}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 182
    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 183
    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->show()V

    .line 185
    sput v5, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->page:I

    .line 186
    sput v5, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->totalPages:I

    .line 189
    :try_start_0
    const-string v3, "ViewRilLog"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 190
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/data/log/ril_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->getRilDumpTime()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".log"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->inFile:Ljava/lang/String;

    .line 198
    :cond_0
    :goto_0
    new-instance v3, Ljava/io/RandomAccessFile;

    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->inFile:Ljava/lang/String;

    const-string v5, "r"

    invoke-direct {v3, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->in:Ljava/io/RandomAccessFile;

    .line 199
    const-string v3, "ViewRilLog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->inFile:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->in:Ljava/io/RandomAccessFile;

    invoke-virtual {v5}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v6

    const-wide/16 v8, 0x400

    div-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Kb"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/sec/android/RilServiceModeApp/ViewRilLog$2;

    invoke-direct {v4, p0}, Lcom/sec/android/RilServiceModeApp/ViewRilLog$2;-><init>(Lcom/sec/android/RilServiceModeApp/ViewRilLog;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 253
    :goto_1
    const-string v3, "You can move to next page by menu key."

    invoke-static {p0, v3, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 256
    return-void

    .line 191
    :cond_1
    :try_start_1
    const-string v3, "ViewIPCDumpLog"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 192
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/data/log/ipcdump_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->getRilDumpTime()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".log"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->inFile:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 243
    :catch_0
    move-exception v0

    .line 244
    .local v0, "e":Ljava/io/FileNotFoundException;
    const-string v3, "File open fail!"

    invoke-direct {p0, v3}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->DisplayMessageDialog(Ljava/lang/String;)V

    .line 245
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 246
    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_1

    .line 247
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 248
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "File open fail!"

    invoke-direct {p0, v3}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->DisplayMessageDialog(Ljava/lang/String;)V

    .line 249
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 250
    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x0

    .line 272
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 274
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 278
    const/4 v0, 0x3

    const v1, 0x7f060026

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 279
    const/4 v0, 0x4

    const v1, 0x7f060027

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 280
    const/4 v0, 0x6

    const v1, 0x7f060028

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 281
    const/4 v0, 0x5

    const v1, 0x7f060029

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 283
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 267
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 268
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 412
    packed-switch p1, :pswitch_data_0

    .line 418
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 414
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->finish()V

    goto :goto_0

    .line 412
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 288
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 322
    :goto_0
    return v0

    .line 291
    :pswitch_0
    sget v1, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->page:I

    if-nez v1, :cond_0

    .line 292
    const-string v1, "This page is First"

    invoke-direct {p0, v1}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->DisplayMessageDialog(Ljava/lang/String;)V

    goto :goto_0

    .line 294
    :cond_0
    sget v1, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->page:I

    add-int/lit8 v1, v1, -0x1

    sput v1, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->page:I

    .line 295
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->moveToPage()V

    goto :goto_0

    .line 302
    :pswitch_1
    sget v1, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->page:I

    sget v2, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->totalPages:I

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_1

    .line 303
    const-string v1, "This page is Last"

    invoke-direct {p0, v1}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->DisplayMessageDialog(Ljava/lang/String;)V

    goto :goto_0

    .line 305
    :cond_1
    sget v1, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->page:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->page:I

    .line 306
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->moveToPage()V

    goto :goto_0

    .line 312
    :pswitch_2
    sput v1, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->page:I

    .line 313
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->moveToPage()V

    goto :goto_0

    .line 317
    :pswitch_3
    sget v1, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->totalPages:I

    add-int/lit8 v1, v1, -0x1

    sput v1, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->page:I

    .line 318
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/ViewRilLog;->moveToPage()V

    goto :goto_0

    .line 288
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
