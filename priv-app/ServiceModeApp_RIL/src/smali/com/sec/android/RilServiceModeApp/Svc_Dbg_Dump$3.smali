.class Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;
.super Ljava/lang/Object;
.source "Svc_Dbg_Dump.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;


# direct methods
.method constructor <init>(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)V
    .locals 0

    .prologue
    .line 378
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x7

    const/4 v2, 0x6

    .line 380
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPALL:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$100(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)Landroid/widget/Button;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 381
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    # invokes: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->SendData(II)V
    invoke-static {v0, v2, v4}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$200(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;II)V

    .line 382
    const-string v0, "SvcDbgDump"

    const-string v1, "all clicked"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    const-string v1, "ALL"

    # invokes: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->infoLog(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;Ljava/lang/String;)V

    .line 385
    :cond_0
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPMsg:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$400(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)Landroid/widget/Button;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 386
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    # invokes: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->SendData(II)V
    invoke-static {v0, v2, v5}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$200(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;II)V

    .line 387
    const-string v0, "SvcDbgDump"

    const-string v1, "msg clicked"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    const-string v1, "MSG"

    # invokes: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->infoLog(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;Ljava/lang/String;)V

    .line 390
    :cond_1
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPCPLog:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$500(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)Landroid/widget/Button;

    move-result-object v0

    if-ne p1, v0, :cond_2

    .line 391
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    # invokes: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->SendData(II)V
    invoke-static {v0, v2, v6}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$200(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;II)V

    .line 392
    const-string v0, "SvcDbgDump"

    const-string v1, "cp_log clicked"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    const-string v1, "CP_LOG"

    # invokes: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->infoLog(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;Ljava/lang/String;)V

    .line 395
    :cond_2
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPCPLog2:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$600(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)Landroid/widget/Button;

    move-result-object v0

    if-ne p1, v0, :cond_3

    .line 396
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    const/4 v1, 0x3

    # invokes: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->SendData(II)V
    invoke-static {v0, v2, v1}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$200(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;II)V

    .line 397
    const-string v0, "SvcDbgDump"

    const-string v1, "cp_log2 clicked"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    const-string v1, "CP_LOG2"

    # invokes: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->infoLog(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;Ljava/lang/String;)V

    .line 400
    :cond_3
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPRamDump:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$700(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)Landroid/widget/Button;

    move-result-object v0

    if-ne p1, v0, :cond_4

    .line 401
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    const/4 v1, 0x4

    # invokes: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->SendData(II)V
    invoke-static {v0, v2, v1}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$200(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;II)V

    .line 402
    const-string v0, "SvcDbgDump"

    const-string v1, "ramdump clicked"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    const-string v1, "RAMDUMP"

    # invokes: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->infoLog(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;Ljava/lang/String;)V

    .line 405
    :cond_4
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringLoopBack:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$800(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)Landroid/widget/Button;

    move-result-object v0

    if-ne p1, v0, :cond_5

    .line 406
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    # invokes: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->SendData(II)V
    invoke-static {v0, v3, v5}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$200(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;II)V

    .line 407
    const-string v0, "SvcDbgDump"

    const-string v1, "mCPDBGStringLoopBack clicked"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    const-string v1, "mCPDBGStringLoopBack"

    # invokes: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->infoLog(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;Ljava/lang/String;)V

    .line 410
    :cond_5
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringMSG:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$900(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)Landroid/widget/Button;

    move-result-object v0

    if-ne p1, v0, :cond_6

    .line 411
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    # invokes: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->SendData(II)V
    invoke-static {v0, v3, v6}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$200(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;II)V

    .line 412
    const-string v0, "SvcDbgDump"

    const-string v1, "mCPDBGStringMSG clicked"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    const-string v1, "mCPDBGStringMSG"

    # invokes: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->infoLog(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;Ljava/lang/String;)V

    .line 415
    :cond_6
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringLOG1:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$1000(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)Landroid/widget/Button;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 416
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    const/4 v1, 0x3

    # invokes: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->SendData(II)V
    invoke-static {v0, v3, v1}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$200(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;II)V

    .line 417
    const-string v0, "SvcDbgDump"

    const-string v1, "mCPDBGStringLOG1 clicked"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    const-string v1, "mCPDBGStringLOG1"

    # invokes: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->infoLog(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;Ljava/lang/String;)V

    .line 420
    :cond_7
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringLOG2:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$1100(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)Landroid/widget/Button;

    move-result-object v0

    if-ne p1, v0, :cond_8

    .line 421
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    const/4 v1, 0x4

    # invokes: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->SendData(II)V
    invoke-static {v0, v3, v1}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$200(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;II)V

    .line 422
    const-string v0, "SvcDbgDump"

    const-string v1, "mCPDBGStringLOG2 clicked"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    const-string v1, "mCPDBGStringLOG2"

    # invokes: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->infoLog(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;Ljava/lang/String;)V

    .line 425
    :cond_8
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringDisable:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$1200(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)Landroid/widget/Button;

    move-result-object v0

    if-ne p1, v0, :cond_9

    .line 426
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    # invokes: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->SendData(II)V
    invoke-static {v0, v3, v4}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$200(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;II)V

    .line 427
    const-string v0, "SvcDbgDump"

    const-string v1, "mCPDBGStringDisable clicked"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    const-string v1, "mCPDBGStringDisable"

    # invokes: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->infoLog(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$300(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;Ljava/lang/String;)V

    .line 430
    :cond_9
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    # getter for: Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mExit:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->access$1300(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)Landroid/widget/Button;

    move-result-object v0

    if-ne p1, v0, :cond_a

    .line 431
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;->this$0:Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    invoke-virtual {v0}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->finish()V

    .line 433
    :cond_a
    return-void
.end method
