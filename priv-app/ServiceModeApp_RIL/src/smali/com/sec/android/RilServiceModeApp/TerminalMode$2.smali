.class Lcom/sec/android/RilServiceModeApp/TerminalMode$2;
.super Ljava/lang/Object;
.source "TerminalMode.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/RilServiceModeApp/TerminalMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;


# direct methods
.method constructor <init>(Lcom/sec/android/RilServiceModeApp/TerminalMode;)V
    .locals 0

    .prologue
    .line 603
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$2;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 605
    const-string v0, "TerminalMode"

    const-string v1, "onServiceConnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 606
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$2;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    # setter for: Lcom/sec/android/RilServiceModeApp/TerminalMode;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->access$602(Lcom/sec/android/RilServiceModeApp/TerminalMode;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 607
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$2;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    # invokes: Lcom/sec/android/RilServiceModeApp/TerminalMode;->StartTerminalMode()V
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->access$700(Lcom/sec/android/RilServiceModeApp/TerminalMode;)V

    .line 608
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 611
    const-string v0, "TerminalMode"

    const-string v1, "onServiceDisconnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/TerminalMode$2;->this$0:Lcom/sec/android/RilServiceModeApp/TerminalMode;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/RilServiceModeApp/TerminalMode;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/android/RilServiceModeApp/TerminalMode;->access$602(Lcom/sec/android/RilServiceModeApp/TerminalMode;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 613
    return-void
.end method
