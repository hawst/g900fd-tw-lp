.class public Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;
.super Landroid/app/Activity;
.source "Sec_Ril_Dump.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;,
        Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;
    }
.end annotation


# static fields
.field private static bDbgEnable:Z

.field private static dataRecoveryTime:I

.field private static fdEnable:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

.field public static rildump_time:Ljava/lang/String;


# instance fields
.field private RilLogMessage:Ljava/lang/String;

.field private bLeaveHint:Z

.field private buf:[B

.field private builder:Landroid/app/AlertDialog$Builder;

.field private context:Landroid/content/Context;

.field day:Ljava/lang/String;

.field private dialog_message:Ljava/lang/String;

.field private file_len:J

.field private fis:Ljava/io/FileInputStream;

.field private fos:Ljava/io/FileOutputStream;

.field hour:Ljava/lang/String;

.field private inFile:Ljava/lang/String;

.field private mActionType:I

.field private mApnEditable:Z

.field private mClearLog:Landroid/widget/Button;

.field private mClicked:Landroid/view/View$OnClickListener;

.field private mCopyToSdcard:Landroid/widget/Button;

.field private mCpRamdump:Landroid/widget/Button;

.field private mCscCompare:Landroid/widget/Button;

.field private mDBLogState:Z

.field private mDBWarningState:Z

.field private mDataFileStats:Landroid/os/StatFs;

.field private mDpramDump:Landroid/widget/Button;

.field private mEfsData:Landroid/widget/Button;

.field private mExit:Landroid/widget/Button;

.field private mFreeMem:J

.field public mHandler:Landroid/os/Handler;

.field private mIPCBin:Landroid/widget/Button;

.field private mIPCLog:Landroid/widget/Button;

.field private mImsLogger:Landroid/widget/Button;

.field private mLogcatRadio:Landroid/widget/Button;

.field private mMmsProvisioning:Landroid/widget/Button;

.field private mNvBackup:Landroid/widget/Button;

.field private mNvDelete:Landroid/widget/Button;

.field private mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

.field private final mOkListener:Landroid/content/DialogInterface$OnClickListener;

.field private mRemainMemory:J

.field private mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

.field private mServiceMessenger:Landroid/os/Messenger;

.field private mSmokeTests:Landroid/widget/Button;

.field private mStorageInfo:Landroid/widget/Button;

.field private mSvcModeMessenger:Landroid/os/Messenger;

.field private mToggleApnEdit:Landroid/widget/Button;

.field private mToggleDB:Landroid/widget/Button;

.field private mToggleDBWaring:Landroid/widget/Button;

.field private mToggleDataRecovery:Landroid/widget/Button;

.field private mToggleFd:Landroid/widget/Button;

.field private mTotalMemory:J

.field private mViewAPN:Landroid/widget/Button;

.field private mViewIPCLog:Landroid/widget/Button;

.field private mViewLog:Landroid/widget/Button;

.field private mViewSetupWizardSkip:Landroid/widget/Button;

.field private memory_info_msg:Ljava/lang/String;

.field min:Ljava/lang/String;

.field month:Ljava/lang/String;

.field private outFile:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 221
    sput-boolean v1, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->bDbgEnable:Z

    .line 227
    sget-object v0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;->SET_NOT:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    sput-object v0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fdEnable:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    .line 240
    sput v1, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->dataRecoveryTime:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 133
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 139
    iput-object v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mServiceMessenger:Landroid/os/Messenger;

    .line 149
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->RilLogMessage:Ljava/lang/String;

    .line 151
    iput-object v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fis:Ljava/io/FileInputStream;

    .line 152
    iput-object v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fos:Ljava/io/FileOutputStream;

    .line 154
    const/16 v0, 0x400

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->buf:[B

    .line 155
    iput-object v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    .line 229
    iput-boolean v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mApnEditable:Z

    .line 230
    iput-boolean v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mDBLogState:Z

    .line 231
    iput-boolean v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mDBWarningState:Z

    .line 253
    iput-boolean v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->bLeaveHint:Z

    .line 255
    new-instance v0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;

    invoke-direct {v0, p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;-><init>(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mHandler:Landroid/os/Handler;

    .line 413
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mSvcModeMessenger:Landroid/os/Messenger;

    .line 547
    new-instance v0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$2;

    invoke-direct {v0, p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$2;-><init>(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    .line 815
    new-instance v0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;

    invoke-direct {v0, p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$3;-><init>(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClicked:Landroid/view/View$OnClickListener;

    .line 1031
    new-instance v0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$4;

    invoke-direct {v0, p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$4;-><init>(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOkListener:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method private DisplayMessageDialog(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 806
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->builder:Landroid/app/AlertDialog$Builder;

    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 807
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->builder:Landroid/app/AlertDialog$Builder;

    const-string v1, "Ril Dump"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 808
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 809
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->builder:Landroid/app/AlertDialog$Builder;

    const v1, 0x104000a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 810
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->builder:Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 811
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 813
    return-void
.end method

.method private EnableFastDormancy()V
    .locals 3

    .prologue
    .line 1362
    const-string v1, "gsm.operator.numeric"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1365
    .local v0, "numeric":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 1366
    const-string v1, "RilDump"

    const-string v2, "[RIL::FD] Operator numberic is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1367
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->queryFastDormancyState()V

    .line 1378
    :goto_0
    return-void

    .line 1369
    :cond_0
    const-string v1, "45001"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1370
    const-string v1, "RilDump"

    const-string v2, "[RIL::FD] Samsung Testbed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1371
    sget-object v1, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;->SET_FALSE:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    sput-object v1, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fdEnable:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    .line 1372
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->queryFastDormancyState()V

    goto :goto_0

    .line 1376
    :cond_1
    sget-object v1, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fdEnable:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    invoke-direct {p0, v1, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->setFastDormancyState(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;Ljava/lang/String;)V

    .line 1377
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->queryFastDormancyState()V

    goto :goto_0
.end method

.method public static ExternalSDcardMounted()Z
    .locals 4

    .prologue
    .line 1039
    const-string v2, "EXTERNAL_STORAGE_STATE"

    const-string v3, "removed"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1040
    .local v1, "statusExternalSDcard":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1042
    .local v0, "mounted":Z
    const-string v2, "mounted"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1043
    const/4 v0, 0x1

    .line 1048
    :goto_0
    return v0

    .line 1045
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private SendData(I)V
    .locals 3
    .param p1, "cmd"    # I

    .prologue
    .line 497
    const/4 v0, 0x0

    .line 499
    .local v0, "data":[B
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    invoke-virtual {v1, p1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;->StartSysDumpData(I)[B

    move-result-object v0

    .line 501
    if-nez v0, :cond_0

    .line 502
    const-string v1, "RilDump"

    const-string v2, " err - data is NULL"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 522
    :goto_0
    return-void

    .line 506
    :cond_0
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    .line 507
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3f0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_0

    .line 508
    :cond_1
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v1, 0x8

    if-ne p1, v1, :cond_2

    .line 509
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3ef

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_0

    .line 510
    :cond_2
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v1, 0x9

    if-ne p1, v1, :cond_3

    .line 511
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3ee

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_0

    .line 512
    :cond_3
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v1, 0xe

    if-ne p1, v1, :cond_4

    .line 513
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3ed

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_0

    .line 515
    :cond_4
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3f1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_0
.end method

.method private SendNVBackupCmd(II)V
    .locals 3
    .param p1, "cmd"    # I
    .param p2, "subCmd"    # I

    .prologue
    .line 525
    const/4 v0, 0x0

    .line 527
    .local v0, "data":[B
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;->StartNVBackupData(II)[B

    move-result-object v0

    .line 529
    if-nez v0, :cond_1

    .line 530
    const-string v1, "RilDump"

    const-string v2, " err - data is NULL"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    :cond_0
    :goto_0
    return-void

    .line 534
    :cond_1
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    if-ne p2, v1, :cond_2

    .line 535
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3f2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_0

    .line 536
    :cond_2
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-nez p2, :cond_0

    .line 537
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3f3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_0
.end method

.method private WriteToSDcard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 6
    .param p1, "in"    # Ljava/lang/String;
    .param p2, "out"    # Ljava/lang/String;
    .param p3, "DumpType"    # Ljava/lang/String;

    .prologue
    .line 703
    const/4 v1, 0x0

    .line 705
    .local v1, "err":I
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fis:Ljava/io/FileInputStream;

    .line 706
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fos:Ljava/io/FileOutputStream;

    .line 708
    :goto_0
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fis:Ljava/io/FileInputStream;

    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->buf:[B

    invoke-virtual {v4, v5}, Ljava/io/FileInputStream;->read([B)I

    move-result v4

    const/4 v5, -0x1

    if-le v4, v5, :cond_2

    .line 709
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fos:Ljava/io/FileOutputStream;

    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->buf:[B

    invoke-virtual {v4, v5}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 713
    :catch_0
    move-exception v3

    .line 714
    .local v3, "fnfe":Ljava/io/FileNotFoundException;
    :try_start_1
    sget-object v4, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v5, "// Exception from"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 715
    const/4 v1, -0x1

    .line 720
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fos:Ljava/io/FileOutputStream;

    if-eqz v4, :cond_0

    .line 722
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fos:Ljava/io/FileOutputStream;

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_7

    .line 726
    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fis:Ljava/io/FileInputStream;

    if-eqz v4, :cond_1

    .line 728
    :try_start_3
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fis:Ljava/io/FileInputStream;

    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6

    :cond_1
    :goto_2
    move v2, v1

    .line 733
    .end local v1    # "err":I
    .end local v3    # "fnfe":Ljava/io/FileNotFoundException;
    .local v2, "err":I
    :goto_3
    return v2

    .line 712
    .end local v2    # "err":I
    .restart local v1    # "err":I
    :cond_2
    const/4 v1, 0x0

    .line 720
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fos:Ljava/io/FileOutputStream;

    if-eqz v4, :cond_3

    .line 722
    :try_start_4
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fos:Ljava/io/FileOutputStream;

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_9

    .line 726
    :cond_3
    :goto_4
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fis:Ljava/io/FileInputStream;

    if-eqz v4, :cond_4

    .line 728
    :try_start_5
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fis:Ljava/io/FileInputStream;

    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_8

    :cond_4
    :goto_5
    move v2, v1

    .line 733
    .end local v1    # "err":I
    .restart local v2    # "err":I
    goto :goto_3

    .line 716
    .end local v2    # "err":I
    .restart local v1    # "err":I
    :catch_1
    move-exception v0

    .line 717
    .local v0, "e":Ljava/lang/Exception;
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 718
    const/4 v1, -0x1

    .line 720
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fos:Ljava/io/FileOutputStream;

    if-eqz v4, :cond_5

    .line 722
    :try_start_7
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fos:Ljava/io/FileOutputStream;

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    .line 726
    :cond_5
    :goto_6
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fis:Ljava/io/FileInputStream;

    if-eqz v4, :cond_6

    .line 728
    :try_start_8
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fis:Ljava/io/FileInputStream;

    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    :cond_6
    :goto_7
    move v2, v1

    .line 733
    .end local v1    # "err":I
    .restart local v2    # "err":I
    goto :goto_3

    .line 720
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "err":I
    .restart local v1    # "err":I
    :catchall_0
    move-exception v4

    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fos:Ljava/io/FileOutputStream;

    if-eqz v4, :cond_7

    .line 722
    :try_start_9
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fos:Ljava/io/FileOutputStream;

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    .line 726
    :cond_7
    :goto_8
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fis:Ljava/io/FileInputStream;

    if-eqz v4, :cond_8

    .line 728
    :try_start_a
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fis:Ljava/io/FileInputStream;

    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2

    :cond_8
    :goto_9
    move v2, v1

    .line 733
    .end local v1    # "err":I
    .restart local v2    # "err":I
    goto :goto_3

    .line 729
    .end local v2    # "err":I
    .restart local v1    # "err":I
    :catch_2
    move-exception v4

    goto :goto_9

    .line 723
    :catch_3
    move-exception v4

    goto :goto_8

    .line 729
    .restart local v0    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v4

    goto :goto_7

    .line 723
    :catch_5
    move-exception v4

    goto :goto_6

    .line 729
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v3    # "fnfe":Ljava/io/FileNotFoundException;
    :catch_6
    move-exception v4

    goto :goto_2

    .line 723
    :catch_7
    move-exception v4

    goto :goto_1

    .line 729
    .end local v3    # "fnfe":Ljava/io/FileNotFoundException;
    :catch_8
    move-exception v4

    goto :goto_5

    .line 723
    :catch_9
    move-exception v4

    goto :goto_4
.end method

.method static synthetic access$000(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->infoIPCDumpLog()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->infoIPCDumpBin()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mDBLogState:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleDB:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mDBWarningState:Z

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleDBWaring:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Landroid/os/Messenger;)Landroid/os/Messenger;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;
    .param p1, "x1"    # Landroid/os/Messenger;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mServiceMessenger:Landroid/os/Messenger;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mMmsProvisioning:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mDpramDump:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;
    .param p1, "x1"    # I

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->SendData(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->infoRilLog()V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mLogcatRadio:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mViewLog:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClearLog:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mStorageInfo:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/os/StatFs;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mDataFileStats:Landroid/os/StatFs;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Landroid/os/StatFs;)Landroid/os/StatFs;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;
    .param p1, "x1"    # Landroid/os/StatFs;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mDataFileStats:Landroid/os/StatFs;

    return-object p1
.end method

.method static synthetic access$2600(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-wide v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mTotalMemory:J

    return-wide v0
.end method

.method static synthetic access$2602(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;
    .param p1, "x1"    # J

    .prologue
    .line 133
    iput-wide p1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mTotalMemory:J

    return-wide p1
.end method

.method static synthetic access$2700(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-wide v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mFreeMem:J

    return-wide v0
.end method

.method static synthetic access$2702(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;
    .param p1, "x1"    # J

    .prologue
    .line 133
    iput-wide p1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mFreeMem:J

    return-wide p1
.end method

.method static synthetic access$2800(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-wide v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mRemainMemory:J

    return-wide v0
.end method

.method static synthetic access$2802(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;
    .param p1, "x1"    # J

    .prologue
    .line 133
    iput-wide p1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mRemainMemory:J

    return-wide p1
.end method

.method static synthetic access$2900(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->memory_info_msg:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2902(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->memory_info_msg:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->DisplayMessageDialog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mIPCLog:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mViewIPCLog:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mIPCBin:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mCopyToSdcard:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->inFile:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3402(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->inFile:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3500(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->outFile:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3502(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->outFile:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3600(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 133
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->WriteToSDcard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$3700(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mCpRamdump:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mNvBackup:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$3902(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;
    .param p1, "x1"    # I

    .prologue
    .line 133
    iput p1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mActionType:I

    return p1
.end method

.method static synthetic access$400()Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;
    .locals 1

    .prologue
    .line 133
    sget-object v0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fdEnable:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->showPassDialog()V

    return-void
.end method

.method static synthetic access$4100(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mNvDelete:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mEfsData:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->changeFDEnableState()V

    return-void
.end method

.method static synthetic access$4400(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->EnableFastDormancy()V

    return-void
.end method

.method static synthetic access$4500(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mCscCompare:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->showCscCompareCfrmDialog()V

    return-void
.end method

.method static synthetic access$4700(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->changeDataRecoveryTime()V

    return-void
.end method

.method static synthetic access$4800(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->changeApnEditState()V

    return-void
.end method

.method static synthetic access$4900(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mExit:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleFd:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->changeDownloadBoosterState()V

    return-void
.end method

.method static synthetic access$5100(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->changeDBWarningToastState()V

    return-void
.end method

.method static synthetic access$5200(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mViewAPN:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$5300(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mViewSetupWizardSkip:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$5400(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mSmokeTests:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$5500(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mImsLogger:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$5600(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->showFactoryResetCfrmDialog()V

    return-void
.end method

.method static synthetic access$5700(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->checkpassword(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$5800(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->changeDataRecoveryTimeResultMessage(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600()I
    .locals 1

    .prologue
    .line 133
    sget v0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->dataRecoveryTime:I

    return v0
.end method

.method static synthetic access$602(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 133
    sput p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->dataRecoveryTime:I

    return p0
.end method

.method static synthetic access$700(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleDataRecovery:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mApnEditable:Z

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleApnEdit:Landroid/widget/Button;

    return-object v0
.end method

.method private changeApnEditState()V
    .locals 4

    .prologue
    .line 1462
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3f7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1464
    .local v0, "msg":Landroid/os/Message;
    iget-boolean v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mApnEditable:Z

    if-eqz v1, :cond_0

    .line 1465
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mApnEditable:Z

    .line 1469
    :goto_0
    const-string v1, "RilDump"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "apneditable state:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mApnEditable:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1470
    const-string v1, "ril.ipversion.editable"

    iget-boolean v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mApnEditable:Z

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1471
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1472
    return-void

    .line 1467
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mApnEditable:Z

    goto :goto_0
.end method

.method private changeDBWarningToastState()V
    .locals 3

    .prologue
    .line 1519
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3f9

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1537
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1538
    return-void
.end method

.method private changeDataRecoveryTime()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 1389
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mHandler:Landroid/os/Handler;

    const/16 v5, 0x3f6

    invoke-virtual {v4, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 1391
    .local v2, "msg":Landroid/os/Message;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1392
    .local v1, "alt_bld":Landroid/app/AlertDialog$Builder;
    new-instance v3, Landroid/widget/EditText;

    invoke-direct {v3, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 1396
    .local v3, "recoveryTime":Landroid/widget/EditText;
    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 1397
    invoke-static {}, Landroid/text/method/DigitsKeyListener;->getInstance()Landroid/text/method/DigitsKeyListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 1398
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setInputType(I)V

    .line 1400
    new-array v4, v6, [Landroid/text/InputFilter;

    const/4 v5, 0x0

    new-instance v6, Landroid/text/InputFilter$LengthFilter;

    const/4 v7, 0x4

    invoke-direct {v6, v7}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 1404
    const-string v4, "recovery time val(1~9999 min)"

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1405
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 1406
    const-string v4, "OK"

    new-instance v5, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$11;

    invoke-direct {v5, p0, v3, v2}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$11;-><init>(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Landroid/widget/EditText;Landroid/os/Message;)V

    invoke-virtual {v1, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1428
    const-string v4, "reset"

    new-instance v5, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$12;

    invoke-direct {v5, p0, v2}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$12;-><init>(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Landroid/os/Message;)V

    invoke-virtual {v1, v4, v5}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1440
    const-string v4, "Cancel"

    new-instance v5, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$13;

    invoke-direct {v5, p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$13;-><init>(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V

    invoke-virtual {v1, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1445
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1446
    .local v0, "alert":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 1447
    return-void
.end method

.method private changeDataRecoveryTimeResultMessage(Ljava/lang/String;)V
    .locals 4
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 1450
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1452
    .local v0, "alt_bld":Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->builder:Landroid/app/AlertDialog$Builder;

    const v2, 0x1080027

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 1453
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->builder:Landroid/app/AlertDialog$Builder;

    const-string v2, "result"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1454
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1455
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->builder:Landroid/app/AlertDialog$Builder;

    const v2, 0x104000a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1456
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->builder:Landroid/app/AlertDialog$Builder;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1457
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 1458
    return-void
.end method

.method private changeDownloadBoosterState()V
    .locals 3

    .prologue
    .line 1481
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3f8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1499
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1500
    return-void
.end method

.method private changeFDEnableState()V
    .locals 3

    .prologue
    .line 1256
    const-string v0, "RilDump"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[RIL::FD] Before fdEnable = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fdEnable:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1257
    sget-object v0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fdEnable:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    sget-object v1, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;->SET_NOT:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    if-ne v0, v1, :cond_1

    .line 1258
    sget-object v0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;->SET_TRUE:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    sput-object v0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fdEnable:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    .line 1264
    :cond_0
    :goto_0
    const-string v0, "RilDump"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[RIL::FD] After fdEnable = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fdEnable:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1265
    return-void

    .line 1259
    :cond_1
    sget-object v0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fdEnable:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    sget-object v1, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;->SET_TRUE:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    if-ne v0, v1, :cond_2

    .line 1260
    sget-object v0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;->SET_FALSE:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    sput-object v0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fdEnable:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    goto :goto_0

    .line 1261
    :cond_2
    sget-object v0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fdEnable:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    sget-object v1, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;->SET_FALSE:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    if-ne v0, v1, :cond_0

    .line 1262
    sget-object v0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;->SET_NOT:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    sput-object v0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fdEnable:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    goto :goto_0
.end method

.method private checkpassword(Ljava/lang/String;)V
    .locals 5
    .param p1, "inputPasswd"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 1232
    const-string v0, ""

    .line 1233
    .local v0, "cmpString":Ljava/lang/String;
    iget v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mActionType:I

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-ne v1, v3, :cond_1

    .line 1234
    const-string v0, "873283"

    .line 1240
    :cond_0
    :goto_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1241
    iget v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mActionType:I

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-ne v1, v4, :cond_3

    .line 1242
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->handleStoreEFSData()V

    .line 1251
    :goto_1
    return-void

    .line 1235
    :cond_1
    iget v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mActionType:I

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-nez v1, :cond_2

    .line 1236
    const-string v0, "3352225"

    goto :goto_0

    .line 1237
    :cond_2
    iget v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mActionType:I

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-ne v1, v4, :cond_0

    .line 1238
    const-string v0, "3372679"

    goto :goto_0

    .line 1243
    :cond_3
    iget v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mActionType:I

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-eq v1, v3, :cond_4

    iget v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mActionType:I

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-nez v1, :cond_5

    .line 1245
    :cond_4
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v1, 0x11

    iget v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mActionType:I

    invoke-direct {p0, v1, v2}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->SendNVBackupCmd(II)V

    goto :goto_1

    .line 1247
    :cond_5
    const-string v1, "mActionType is wrong"

    invoke-direct {p0, v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->DisplayMessageDialog(Ljava/lang/String;)V

    goto :goto_1

    .line 1249
    :cond_6
    const-string v1, "Invalid password!!"

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method private connectToRilService()V
    .locals 3

    .prologue
    .line 560
    const-string v1, "RilDump"

    const-string v2, "connect To Ril service"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 562
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.phone"

    const-string v2, "com.sec.phone.SecPhoneService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 563
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 564
    return-void
.end method

.method private getCurrentTime()V
    .locals 4

    .prologue
    .line 688
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 692
    .local v0, "cal":Ljava/util/Calendar;
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v2, "00"

    invoke-direct {v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->month:Ljava/lang/String;

    .line 693
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v2, "00"

    invoke-direct {v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->day:Ljava/lang/String;

    .line 694
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v2, "00"

    invoke-direct {v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->hour:Ljava/lang/String;

    .line 695
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v2, "00"

    invoke-direct {v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->min:Ljava/lang/String;

    .line 697
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->month:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->day:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->hour:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->min:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->rildump_time:Ljava/lang/String;

    .line 698
    const-string v1, "RilDump"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Ril Dump Time : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->rildump_time:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 700
    return-void
.end method

.method private getFastdormancyState(Ljava/lang/String;)Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;
    .locals 9
    .param p1, "numeric"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x3

    .line 1268
    const-string v6, "RilDump"

    const-string v7, "[RIL::FD] getFastDormancyState"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1270
    sget-object v2, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;->SET_NOT:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    .line 1271
    .local v2, "isFD":Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;
    const/4 v0, 0x0

    .line 1273
    .local v0, "FDString":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1275
    .local v4, "phoneContext":Landroid/content/Context;
    :try_start_0
    const-string v6, "com.android.phone"

    const/4 v7, 0x3

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 1281
    :goto_0
    if-eqz v4, :cond_3

    .line 1282
    const-string v6, "fdormancy.preferences_name"

    invoke-virtual {v4, v6, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 1284
    .local v5, "preferences":Landroid/content/SharedPreferences;
    if-eqz v5, :cond_2

    .line 1285
    const-string v6, "fdormancy.key.mccmnc"

    const-string v7, ""

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1287
    .local v3, "mccmnc":Ljava/lang/String;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_1

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1288
    const-string v6, "RilDump"

    const-string v7, "[RIL::FD] FD state from key string."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1289
    const-string v6, "fdormancy.key.state"

    const-string v7, ""

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1290
    if-eqz v0, :cond_0

    .line 1291
    invoke-static {v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;->valueOf(Ljava/lang/String;)Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    move-result-object v2

    .line 1303
    .end local v3    # "mccmnc":Ljava/lang/String;
    .end local v5    # "preferences":Landroid/content/SharedPreferences;
    :cond_0
    :goto_1
    return-object v2

    .line 1277
    :catch_0
    move-exception v1

    .line 1278
    .local v1, "ex":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v4, 0x0

    goto :goto_0

    .line 1294
    .end local v1    # "ex":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v3    # "mccmnc":Ljava/lang/String;
    .restart local v5    # "preferences":Landroid/content/SharedPreferences;
    :cond_1
    const-string v6, "RilDump"

    const-string v7, "[RIL::FD] Preference is exist but MCCMNC dosen\'t match."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1297
    .end local v3    # "mccmnc":Ljava/lang/String;
    :cond_2
    const-string v6, "RilDump"

    const-string v7, "[RIL::FD] Preference has no FD setting value."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1300
    .end local v5    # "preferences":Landroid/content/SharedPreferences;
    :cond_3
    const-string v6, "RilDump"

    const-string v7, "[RIL::FD] Can\'t get phone context"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static getRilDumpTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 418
    sget-object v0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->rildump_time:Ljava/lang/String;

    return-object v0
.end method

.method private handleStoreEFSData()V
    .locals 20

    .prologue
    .line 1112
    const-string v17, "RilDump"

    const-string v18, "+handleStoreEFSData()"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1116
    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [Ljava/io/File;

    move-object/from16 v16, v0

    .line 1117
    .local v16, "src_log":[Ljava/io/File;
    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v5, v0, [Ljava/io/File;

    .line 1118
    .local v5, "dest_log":[Ljava/io/File;
    new-instance v4, Ljava/io/File;

    const-string v17, "/mnt/sdcard/log"

    move-object/from16 v0, v17

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1120
    .local v4, "dest_folder":Ljava/io/File;
    const/4 v8, 0x3

    .line 1121
    .local v8, "fcount":I
    const-string v3, "/mnt/sdcard/log/"

    .line 1122
    .local v3, "dest":Ljava/lang/String;
    const-string v7, ""

    .line 1124
    .local v7, "existfile":Ljava/lang/String;
    const/16 v17, 0x0

    new-instance v18, Ljava/io/File;

    const-string v19, "/efs/FactoryApp/test_nv"

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    aput-object v18, v16, v17

    .line 1125
    const/16 v17, 0x1

    new-instance v18, Ljava/io/File;

    const-string v19, "/efs/FactoryApp/hist_nv"

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    aput-object v18, v16, v17

    .line 1126
    const/16 v17, 0x2

    new-instance v18, Ljava/io/File;

    const-string v19, "/efs/nv.log"

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    aput-object v18, v16, v17

    .line 1128
    const/16 v17, 0x0

    new-instance v18, Ljava/io/File;

    const-string v19, "/mnt/sdcard/log/test_nv"

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    aput-object v18, v5, v17

    .line 1129
    const/16 v17, 0x1

    new-instance v18, Ljava/io/File;

    const-string v19, "/mnt/sdcard/log/hist_nv"

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    aput-object v18, v5, v17

    .line 1130
    const/16 v17, 0x2

    new-instance v18, Ljava/io/File;

    const-string v19, "/mnt/sdcard/log/nv.log"

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    aput-object v18, v5, v17

    .line 1132
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_0
    const/16 v17, 0x3

    move/from16 v0, v17

    if-ge v14, v0, :cond_3

    .line 1133
    aget-object v17, v16, v14

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_0

    .line 1134
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    aget-object v18, v16, v14

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "\r\n"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1135
    const-string v17, "RilDump"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "src_log exist!!"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1138
    :cond_0
    const/4 v9, 0x0

    .line 1139
    .local v9, "finstream":Ljava/io/FileInputStream;
    const/4 v12, 0x0

    .line 1142
    .local v12, "foutstream":Ljava/io/FileOutputStream;
    :try_start_0
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v17

    if-nez v17, :cond_1

    .line 1143
    invoke-virtual {v4}, Ljava/io/File;->mkdir()Z

    .line 1146
    :cond_1
    new-instance v10, Ljava/io/FileInputStream;

    aget-object v17, v16, v14

    move-object/from16 v0, v17

    invoke-direct {v10, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1147
    .end local v9    # "finstream":Ljava/io/FileInputStream;
    .local v10, "finstream":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v13, Ljava/io/FileOutputStream;

    aget-object v17, v5, v14

    move-object/from16 v0, v17

    invoke-direct {v13, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1150
    .end local v12    # "foutstream":Ljava/io/FileOutputStream;
    .local v13, "foutstream":Ljava/io/FileOutputStream;
    const/16 v17, 0x400

    :try_start_2
    move/from16 v0, v17

    new-array v2, v0, [B

    .line 1153
    .local v2, "buffer":[B
    :goto_1
    invoke-virtual {v10, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v15

    .local v15, "len":I
    if-lez v15, :cond_2

    .line 1154
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v13, v2, v0, v15}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_1

    .line 1159
    .end local v2    # "buffer":[B
    .end local v15    # "len":I
    :catch_0
    move-exception v11

    move-object v12, v13

    .end local v13    # "foutstream":Ljava/io/FileOutputStream;
    .restart local v12    # "foutstream":Ljava/io/FileOutputStream;
    move-object v9, v10

    .line 1160
    .end local v10    # "finstream":Ljava/io/FileInputStream;
    .restart local v9    # "finstream":Ljava/io/FileInputStream;
    .local v11, "fnfe":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_3
    const-string v17, "RilDump"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "fnfe : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v11}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1161
    sget-object v17, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v18, "// Exception from"

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1166
    :try_start_4
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V

    .line 1167
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 1132
    .end local v11    # "fnfe":Ljava/io/FileNotFoundException;
    :goto_3
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_0

    .line 1157
    .end local v9    # "finstream":Ljava/io/FileInputStream;
    .end local v12    # "foutstream":Ljava/io/FileOutputStream;
    .restart local v2    # "buffer":[B
    .restart local v10    # "finstream":Ljava/io/FileInputStream;
    .restart local v13    # "foutstream":Ljava/io/FileOutputStream;
    .restart local v15    # "len":I
    :cond_2
    :try_start_5
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "COPY TO SDCARD(/sdcard/log/..) DONE!!"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->DisplayMessageDialog(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1166
    :try_start_6
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V

    .line 1167
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    move-object v12, v13

    .end local v13    # "foutstream":Ljava/io/FileOutputStream;
    .restart local v12    # "foutstream":Ljava/io/FileOutputStream;
    move-object v9, v10

    .line 1170
    .end local v10    # "finstream":Ljava/io/FileInputStream;
    .restart local v9    # "finstream":Ljava/io/FileInputStream;
    goto :goto_3

    .line 1168
    .end local v9    # "finstream":Ljava/io/FileInputStream;
    .end local v12    # "foutstream":Ljava/io/FileOutputStream;
    .restart local v10    # "finstream":Ljava/io/FileInputStream;
    .restart local v13    # "foutstream":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v6

    .line 1169
    .local v6, "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    move-object v12, v13

    .end local v13    # "foutstream":Ljava/io/FileOutputStream;
    .restart local v12    # "foutstream":Ljava/io/FileOutputStream;
    move-object v9, v10

    .line 1171
    .end local v10    # "finstream":Ljava/io/FileInputStream;
    .restart local v9    # "finstream":Ljava/io/FileInputStream;
    goto :goto_3

    .line 1168
    .end local v2    # "buffer":[B
    .end local v6    # "e":Ljava/io/IOException;
    .end local v15    # "len":I
    .restart local v11    # "fnfe":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v6

    .line 1169
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 1162
    .end local v6    # "e":Ljava/io/IOException;
    .end local v11    # "fnfe":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v6

    .line 1163
    .local v6, "e":Ljava/lang/Exception;
    :goto_4
    :try_start_7
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1166
    :try_start_8
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V

    .line 1167
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_3

    .line 1168
    :catch_4
    move-exception v6

    .line 1169
    .local v6, "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 1165
    .end local v6    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v17

    .line 1166
    :goto_5
    :try_start_9
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V

    .line 1167
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    .line 1170
    :goto_6
    throw v17

    .line 1168
    :catch_5
    move-exception v6

    .line 1169
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 1173
    .end local v6    # "e":Ljava/io/IOException;
    .end local v9    # "finstream":Ljava/io/FileInputStream;
    .end local v12    # "foutstream":Ljava/io/FileOutputStream;
    :cond_3
    const-string v17, "RilDump"

    const-string v18, "-handleStoreEFSData()"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1174
    return-void

    .line 1165
    .restart local v10    # "finstream":Ljava/io/FileInputStream;
    .restart local v12    # "foutstream":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v17

    move-object v9, v10

    .end local v10    # "finstream":Ljava/io/FileInputStream;
    .restart local v9    # "finstream":Ljava/io/FileInputStream;
    goto :goto_5

    .end local v9    # "finstream":Ljava/io/FileInputStream;
    .end local v12    # "foutstream":Ljava/io/FileOutputStream;
    .restart local v10    # "finstream":Ljava/io/FileInputStream;
    .restart local v13    # "foutstream":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v17

    move-object v12, v13

    .end local v13    # "foutstream":Ljava/io/FileOutputStream;
    .restart local v12    # "foutstream":Ljava/io/FileOutputStream;
    move-object v9, v10

    .end local v10    # "finstream":Ljava/io/FileInputStream;
    .restart local v9    # "finstream":Ljava/io/FileInputStream;
    goto :goto_5

    .line 1162
    .end local v9    # "finstream":Ljava/io/FileInputStream;
    .restart local v10    # "finstream":Ljava/io/FileInputStream;
    :catch_6
    move-exception v6

    move-object v9, v10

    .end local v10    # "finstream":Ljava/io/FileInputStream;
    .restart local v9    # "finstream":Ljava/io/FileInputStream;
    goto :goto_4

    .end local v9    # "finstream":Ljava/io/FileInputStream;
    .end local v12    # "foutstream":Ljava/io/FileOutputStream;
    .restart local v10    # "finstream":Ljava/io/FileInputStream;
    .restart local v13    # "foutstream":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v6

    move-object v12, v13

    .end local v13    # "foutstream":Ljava/io/FileOutputStream;
    .restart local v12    # "foutstream":Ljava/io/FileOutputStream;
    move-object v9, v10

    .end local v10    # "finstream":Ljava/io/FileInputStream;
    .restart local v9    # "finstream":Ljava/io/FileInputStream;
    goto :goto_4

    .line 1159
    :catch_8
    move-exception v11

    goto/16 :goto_2

    .end local v9    # "finstream":Ljava/io/FileInputStream;
    .restart local v10    # "finstream":Ljava/io/FileInputStream;
    :catch_9
    move-exception v11

    move-object v9, v10

    .end local v10    # "finstream":Ljava/io/FileInputStream;
    .restart local v9    # "finstream":Ljava/io/FileInputStream;
    goto/16 :goto_2
.end method

.method private infoIPCDumpBin()V
    .locals 4

    .prologue
    .line 779
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/data/log/ipcdump_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->rildump_time:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".bin"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->inFile:Ljava/lang/String;

    .line 781
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->inFile:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 782
    .local v0, "oFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 784
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->file_len:J

    .line 785
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "File size : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->file_len:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Bytes\nSaved Location :\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->dialog_message:Ljava/lang/String;

    .line 788
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->dialog_message:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->DisplayMessageDialog(Ljava/lang/String;)V

    .line 793
    :goto_0
    return-void

    .line 791
    :cond_0
    const-string v1, "IPC Dump Bin Fail!"

    invoke-direct {p0, v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->DisplayMessageDialog(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private infoIPCDumpLog()V
    .locals 6

    .prologue
    .line 759
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/data/log/ipcdump_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->rildump_time:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".log"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->inFile:Ljava/lang/String;

    .line 761
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->inFile:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 763
    .local v0, "oFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 765
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->file_len:J

    .line 766
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "File size : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->file_len:J

    const-wide/16 v4, 0x400

    div-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Kb\nSaved Location :\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->dialog_message:Ljava/lang/String;

    .line 769
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->dialog_message:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->DisplayMessageDialog(Ljava/lang/String;)V

    .line 771
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mViewIPCLog:Landroid/widget/Button;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 775
    :goto_0
    return-void

    .line 773
    :cond_0
    const-string v1, "IPC Dump Log Fail!"

    invoke-direct {p0, v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->DisplayMessageDialog(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private infoRilLog()V
    .locals 6

    .prologue
    .line 739
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/data/log/ril_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->rildump_time:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".log"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->inFile:Ljava/lang/String;

    .line 741
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->inFile:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 743
    .local v0, "oFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 745
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->file_len:J

    .line 746
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "File size : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->file_len:J

    const-wide/16 v4, 0x400

    div-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Kb\nSaved Location :\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->dialog_message:Ljava/lang/String;

    .line 749
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->dialog_message:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->DisplayMessageDialog(Ljava/lang/String;)V

    .line 751
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mViewLog:Landroid/widget/Button;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 755
    :goto_0
    return-void

    .line 753
    :cond_0
    const-string v1, "Ril Dump Fail!"

    invoke-direct {p0, v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->DisplayMessageDialog(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private invokeOemRilRequestRaw([BLandroid/os/Message;)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "response"    # Landroid/os/Message;

    .prologue
    .line 567
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 568
    .local v0, "req":Landroid/os/Bundle;
    const-string v1, "request"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 570
    invoke-virtual {p2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 571
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mSvcModeMessenger:Landroid/os/Messenger;

    iput-object v1, p2, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 574
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mServiceMessenger:Landroid/os/Messenger;

    invoke-virtual {v1, p2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 577
    :goto_0
    return-void

    .line 575
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private queryApnEditState()V
    .locals 3

    .prologue
    .line 1475
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3f7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1476
    .local v0, "msg":Landroid/os/Message;
    const-string v1, "ril.ipversion.editable"

    const-string v2, "false"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mApnEditable:Z

    .line 1477
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1478
    return-void
.end method

.method private queryDBWarningToastState()V
    .locals 3

    .prologue
    .line 1541
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3f9

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1553
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1554
    return-void
.end method

.method private queryDataRecoveryTime()V
    .locals 4

    .prologue
    const v3, 0xea60

    .line 1382
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3f6

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1383
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "data_stall_alarm_aggressive_delay_in_ms"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    div-int/2addr v1, v3

    sput v1, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->dataRecoveryTime:I

    .line 1385
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1386
    return-void
.end method

.method private queryDownloadBoosterState()V
    .locals 3

    .prologue
    .line 1503
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3f8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1515
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1516
    return-void
.end method

.method private queryFastDormancyState()V
    .locals 4

    .prologue
    .line 1339
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x3f5

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1342
    .local v0, "msg":Landroid/os/Message;
    const-string v2, "gsm.operator.numeric"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1345
    .local v1, "numeric":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;->SET_FALSE:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    sput-object v2, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fdEnable:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    .line 1347
    if-nez v1, :cond_0

    .line 1348
    const-string v2, "RilDump"

    const-string v3, "[RIL::FD] Operator numberic is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1349
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1359
    :goto_0
    return-void

    .line 1351
    :cond_0
    const-string v2, "45001"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1352
    const-string v2, "RilDump"

    const-string v3, "[RIL::FD] Samsung Testbed"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1353
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 1357
    :cond_1
    invoke-direct {p0, v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->getFastdormancyState(Ljava/lang/String;)Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    move-result-object v2

    sput-object v2, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->fdEnable:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;

    .line 1358
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method private setFastDormancyState(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;Ljava/lang/String;)V
    .locals 9
    .param p1, "fd"    # Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;
    .param p2, "numeric"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x3

    .line 1307
    const-string v5, "RilDump"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[RIL::FD] setFastDormancyState fdEnable = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1309
    const/4 v3, 0x0

    .line 1311
    .local v3, "phoneContext":Landroid/content/Context;
    :try_start_0
    const-string v5, "com.android.phone"

    const/4 v6, 0x3

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 1317
    :goto_0
    if-eqz v3, :cond_1

    .line 1318
    const-string v5, "fdormancy.preferences_name"

    invoke-virtual {v3, v5, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 1320
    .local v4, "preferences":Landroid/content/SharedPreferences;
    if-eqz v4, :cond_0

    .line 1321
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1322
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v5, "fdormancy.key.mccmnc"

    invoke-interface {v0, v5, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1323
    const-string v5, "fdormancy.key.state"

    invoke-virtual {p1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$PreferenceFD;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1324
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1328
    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1329
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.FD_SETTING_CHANGED"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1330
    .local v2, "i":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->sendBroadcast(Landroid/content/Intent;)V

    .line 1336
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v2    # "i":Landroid/content/Intent;
    .end local v4    # "preferences":Landroid/content/SharedPreferences;
    :cond_0
    :goto_1
    return-void

    .line 1313
    :catch_0
    move-exception v1

    .line 1314
    .local v1, "ex":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v3, 0x0

    goto :goto_0

    .line 1333
    .end local v1    # "ex":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    const-string v5, "RilDump"

    const-string v6, "[RIL::FD] Can\'t get phone context"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private showCscCompareCfrmDialog()V
    .locals 4

    .prologue
    .line 1089
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1091
    .local v1, "cfrm_bld":Landroid/app/AlertDialog$Builder;
    const-string v2, "Inform"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1092
    const-string v2, "This action must be tested after doing FullReset"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1093
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1094
    const-string v2, "OK"

    new-instance v3, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$5;

    invoke-direct {v3, p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$5;-><init>(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1100
    const-string v2, "Cancel"

    new-instance v3, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$6;

    invoke-direct {v3, p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$6;-><init>(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1106
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1107
    .local v0, "alert":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 1108
    return-void
.end method

.method private showFactoryResetCfrmDialog()V
    .locals 4

    .prologue
    .line 1177
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1179
    .local v1, "cfrm_bld":Landroid/app/AlertDialog$Builder;
    const-string v2, "Inform"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1180
    const-string v2, "Want to execute Full Reset?"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1181
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1182
    const-string v2, "OK"

    new-instance v3, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$7;

    invoke-direct {v3, p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$7;-><init>(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1189
    const-string v2, "Cancel"

    new-instance v3, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$8;

    invoke-direct {v3, p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$8;-><init>(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1194
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1195
    .local v0, "alert":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 1196
    return-void
.end method

.method private showPassDialog()V
    .locals 6

    .prologue
    .line 1202
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1203
    .local v1, "alt_bld":Landroid/app/AlertDialog$Builder;
    new-instance v2, Landroid/widget/EditText;

    invoke-direct {v2, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 1207
    .local v2, "passwdText":Landroid/widget/EditText;
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 1208
    invoke-static {}, Landroid/text/method/DigitsKeyListener;->getInstance()Landroid/text/method/DigitsKeyListener;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 1209
    invoke-virtual {v2}, Landroid/widget/EditText;->getInputType()I

    move-result v4

    or-int/lit16 v4, v4, 0x80

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setInputType(I)V

    .line 1211
    new-instance v3, Landroid/text/method/PasswordTransformationMethod;

    invoke-direct {v3}, Landroid/text/method/PasswordTransformationMethod;-><init>()V

    .line 1212
    .local v3, "ptm":Landroid/text/method/PasswordTransformationMethod;
    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 1214
    const-string v4, "Input password"

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1215
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 1216
    const-string v4, "OK"

    new-instance v5, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$9;

    invoke-direct {v5, p0, v2}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$9;-><init>(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Landroid/widget/EditText;)V

    invoke-virtual {v1, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1222
    const-string v4, "Cancel"

    new-instance v5, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$10;

    invoke-direct {v5, p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$10;-><init>(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V

    invoke-virtual {v1, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1227
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1228
    .local v0, "alert":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 1229
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 583
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 585
    const v0, 0x7f030009

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->setContentView(I)V

    .line 590
    new-instance v0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;-><init>(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$1;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mOem:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$OemCommands;

    .line 593
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->connectToRilService()V

    .line 596
    const v0, 0x7f080015

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mDpramDump:Landroid/widget/Button;

    .line 597
    const v0, 0x7f080016

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mLogcatRadio:Landroid/widget/Button;

    .line 598
    const v0, 0x7f080017

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mViewLog:Landroid/widget/Button;

    .line 599
    const v0, 0x7f080018

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClearLog:Landroid/widget/Button;

    .line 601
    const v0, 0x7f080019

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mIPCLog:Landroid/widget/Button;

    .line 602
    const v0, 0x7f08001a

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mViewIPCLog:Landroid/widget/Button;

    .line 603
    const v0, 0x7f08001b

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mIPCBin:Landroid/widget/Button;

    .line 605
    const v0, 0x7f08001c

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mCopyToSdcard:Landroid/widget/Button;

    .line 607
    const v0, 0x7f08001d

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mMmsProvisioning:Landroid/widget/Button;

    .line 609
    const v0, 0x7f08001e

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mNvBackup:Landroid/widget/Button;

    .line 610
    const v0, 0x7f08001f

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mNvDelete:Landroid/widget/Button;

    .line 611
    const v0, 0x7f080020

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleFd:Landroid/widget/Button;

    .line 612
    const v0, 0x7f080021

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleDataRecovery:Landroid/widget/Button;

    .line 615
    const v0, 0x7f080025

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleApnEdit:Landroid/widget/Button;

    .line 616
    const v0, 0x7f080026

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleDB:Landroid/widget/Button;

    .line 617
    const v0, 0x7f080027

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleDBWaring:Landroid/widget/Button;

    .line 619
    const v0, 0x7f080024

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mViewAPN:Landroid/widget/Button;

    .line 621
    const v0, 0x7f080028

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mViewSetupWizardSkip:Landroid/widget/Button;

    .line 623
    const v0, 0x7f080023

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mCscCompare:Landroid/widget/Button;

    .line 625
    const v0, 0x7f08002b

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mExit:Landroid/widget/Button;

    .line 627
    const v0, 0x7f080022

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mEfsData:Landroid/widget/Button;

    .line 630
    const v0, 0x7f080029

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mSmokeTests:Landroid/widget/Button;

    .line 633
    const v0, 0x7f08002a

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mImsLogger:Landroid/widget/Button;

    .line 635
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->queryFastDormancyState()V

    .line 636
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->queryDataRecoveryTime()V

    .line 637
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->queryApnEditState()V

    .line 638
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->queryDownloadBoosterState()V

    .line 639
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->queryDBWarningToastState()V

    .line 641
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mViewLog:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 643
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mDpramDump:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 644
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mLogcatRadio:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 645
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mViewLog:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 646
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClearLog:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 648
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mIPCLog:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 649
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mViewIPCLog:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 650
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mIPCBin:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 652
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mCopyToSdcard:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 653
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mMmsProvisioning:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 655
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mNvBackup:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 656
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mNvDelete:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 657
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleFd:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 658
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleDataRecovery:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 659
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleApnEdit:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 661
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleDB:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 662
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mToggleDBWaring:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 663
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mViewAPN:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 664
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mViewSetupWizardSkip:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 666
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mCscCompare:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 669
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mExit:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 671
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mEfsData:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 673
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mSmokeTests:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 675
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mImsLogger:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 677
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->builder:Landroid/app/AlertDialog$Builder;

    .line 679
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mViewLog:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 680
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mViewIPCLog:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 681
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->getCurrentTime()V

    .line 683
    iput-object p0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->context:Landroid/content/Context;

    .line 685
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 797
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->unbindService(Landroid/content/ServiceConnection;)V

    .line 798
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    .line 801
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 802
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1069
    .line 1075
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 1059
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 1061
    iget-boolean v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->bLeaveHint:Z

    if-eqz v0, :cond_0

    .line 1062
    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->finish()V

    .line 1064
    :cond_0
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 1

    .prologue
    .line 1053
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 1054
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->bLeaveHint:Z

    .line 1055
    return-void
.end method
