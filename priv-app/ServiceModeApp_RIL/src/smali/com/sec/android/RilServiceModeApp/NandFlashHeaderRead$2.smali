.class Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead$2;
.super Landroid/content/BroadcastReceiver;
.source "NandFlashHeaderRead.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;


# direct methods
.method constructor <init>(Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead$2;->this$0:Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 191
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 192
    .local v0, "action":Ljava/lang/String;
    const-string v1, "NandFlashHeaderRead"

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    const-string v1, "com.android.samsungtest.DGS_UniqueNumber"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 195
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead$2;->this$0:Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;

    # invokes: Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->stringReset()V
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->access$100(Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;)V

    .line 196
    # getter for: Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mUniqueNumberBuilder:Ljava/lang/StringBuilder;
    invoke-static {}, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->access$200()Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "UniqueNumberKey"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead$2;->this$0:Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;

    # invokes: Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->parseUniqueNumber()V
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->access$300(Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;)V

    .line 198
    const-string v1, "NandFlashHeaderRead"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "print_originalString:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mUniqueNumberBuilder:Ljava/lang/StringBuilder;
    invoke-static {}, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->access$200()Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    const-string v1, "NandFlashHeaderRead"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "print_parsedString:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->mParsedUniqueNumber:Ljava/lang/StringBuilder;
    invoke-static {}, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->access$400()Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead$2;->this$0:Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;

    # invokes: Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->printUniqueNumber()V
    invoke-static {v1}, Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;->access$500(Lcom/sec/android/RilServiceModeApp/NandFlashHeaderRead;)V

    .line 203
    :cond_0
    return-void
.end method
