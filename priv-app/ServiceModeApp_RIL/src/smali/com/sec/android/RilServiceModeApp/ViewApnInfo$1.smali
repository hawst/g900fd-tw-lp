.class Lcom/sec/android/RilServiceModeApp/ViewApnInfo$1;
.super Ljava/lang/Object;
.source "ViewApnInfo.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/RilServiceModeApp/ViewApnInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/RilServiceModeApp/ViewApnInfo;


# direct methods
.method constructor <init>(Lcom/sec/android/RilServiceModeApp/ViewApnInfo;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/ViewApnInfo$1;->this$0:Lcom/sec/android/RilServiceModeApp/ViewApnInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 137
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const-string v5, "ViewApnInfo"

    const-string v6, "onItemSelected() entered!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/ViewApnInfo$1;->this$0:Lcom/sec/android/RilServiceModeApp/ViewApnInfo;

    # getter for: Lcom/sec/android/RilServiceModeApp/ViewApnInfo;->mMccMncSpinner:Landroid/widget/Spinner;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/ViewApnInfo;->access$000(Lcom/sec/android/RilServiceModeApp/ViewApnInfo;)Landroid/widget/Spinner;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 140
    .local v4, "selItem":Ljava/lang/String;
    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 141
    .local v3, "info":[Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/ViewApnInfo$1;->this$0:Lcom/sec/android/RilServiceModeApp/ViewApnInfo;

    const/4 v6, 0x0

    aget-object v6, v3, v6

    const/4 v7, 0x1

    aget-object v7, v3, v7

    # invokes: Lcom/sec/android/RilServiceModeApp/ViewApnInfo;->getApnInfo(Ljava/lang/String;Ljava/lang/String;)Lcom/android/internal/telephony/dataconnection/ApnSetting;
    invoke-static {v5, v6, v7}, Lcom/sec/android/RilServiceModeApp/ViewApnInfo;->access$100(Lcom/sec/android/RilServiceModeApp/ViewApnInfo;Ljava/lang/String;Ljava/lang/String;)Lcom/android/internal/telephony/dataconnection/ApnSetting;

    move-result-object v1

    .line 142
    .local v1, "apn":Lcom/android/internal/telephony/dataconnection/ApnSetting;
    if-eqz v1, :cond_0

    .line 143
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/ViewApnInfo$1;->this$0:Lcom/sec/android/RilServiceModeApp/ViewApnInfo;

    # invokes: Lcom/sec/android/RilServiceModeApp/ViewApnInfo;->makeArrayList(Lcom/android/internal/telephony/dataconnection/ApnSetting;)Ljava/util/ArrayList;
    invoke-static {v5, v1}, Lcom/sec/android/RilServiceModeApp/ViewApnInfo;->access$200(Lcom/sec/android/RilServiceModeApp/ViewApnInfo;Lcom/android/internal/telephony/dataconnection/ApnSetting;)Ljava/util/ArrayList;

    move-result-object v2

    .line 144
    .local v2, "apninfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/ViewApnInfo$1;->this$0:Lcom/sec/android/RilServiceModeApp/ViewApnInfo;

    # getter for: Lcom/sec/android/RilServiceModeApp/ViewApnInfo;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/ViewApnInfo;->access$300(Lcom/sec/android/RilServiceModeApp/ViewApnInfo;)Landroid/content/Context;

    move-result-object v5

    const v6, 0x1090003

    invoke-direct {v0, v5, v6, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 147
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/sec/android/RilServiceModeApp/ViewApnInfo$1;->this$0:Lcom/sec/android/RilServiceModeApp/ViewApnInfo;

    # getter for: Lcom/sec/android/RilServiceModeApp/ViewApnInfo;->mApnListView:Landroid/widget/ListView;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/ViewApnInfo;->access$400(Lcom/sec/android/RilServiceModeApp/ViewApnInfo;)Landroid/widget/ListView;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 149
    .end local v0    # "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    .end local v2    # "apninfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    const-string v5, "ViewApnInfo"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Spinner selected item = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 153
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const-string v0, "ViewApnInfo"

    const-string v1, "onNothingSelected() entered!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    return-void
.end method
