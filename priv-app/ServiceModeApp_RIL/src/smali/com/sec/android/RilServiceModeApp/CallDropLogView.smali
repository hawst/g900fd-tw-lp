.class public Lcom/sec/android/RilServiceModeApp/CallDropLogView;
.super Landroid/app/Activity;
.source "CallDropLogView.java"


# static fields
.field private static final mCountryCode:Ljava/lang/String;


# instance fields
.field private final CALLDROP_FILEPATH:Ljava/lang/String;

.field private final CALLDROP_LOG_CAUSECODE:I

.field private final CALLDROP_LOG_DATE:I

.field private final CALLDROP_LOG_DLCH:I

.field private final CALLDROP_LOG_ECIO:I

.field private final CALLDROP_LOG_MAINCAUSE:I

.field private final CALLDROP_LOG_MULTILAB:I

.field private final CALLDROP_LOG_PSC:I

.field private final CALLDROP_LOG_RSCP:I

.field private final CALLDROP_LOG_SIR:I

.field private final CALLDROP_LOG_TX:I

.field private final CALLDROP_LOG_ULCH:I

.field private final DEVICE_FILEPATH:Ljava/lang/String;

.field private final MAX_SAVE_COUNT:I

.field private final TAG:Ljava/lang/String;

.field private callDrop_Log:Landroid/widget/TextView;

.field private eachCallDrop_Info:[Ljava/lang/String;

.field private log:Ljava/lang/String;

.field private logCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43
    const-string v0, "ro.csc.country_code"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->mCountryCode:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x28

    .line 18
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 20
    const-string v0, "CallDropLogView"

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->TAG:Ljava/lang/String;

    .line 21
    const-string v0, "data/log/CallDropInfoLog.txt"

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->CALLDROP_FILEPATH:Ljava/lang/String;

    .line 22
    const-string v0, "/sys/class/sec/switch/adc"

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->DEVICE_FILEPATH:Ljava/lang/String;

    .line 24
    iput v1, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->MAX_SAVE_COUNT:I

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->CALLDROP_LOG_DATE:I

    .line 27
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->CALLDROP_LOG_MAINCAUSE:I

    .line 28
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->CALLDROP_LOG_CAUSECODE:I

    .line 29
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->CALLDROP_LOG_PSC:I

    .line 30
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->CALLDROP_LOG_RSCP:I

    .line 31
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->CALLDROP_LOG_ECIO:I

    .line 32
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->CALLDROP_LOG_TX:I

    .line 33
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->CALLDROP_LOG_SIR:I

    .line 34
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->CALLDROP_LOG_MULTILAB:I

    .line 35
    const/16 v0, 0x9

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->CALLDROP_LOG_ULCH:I

    .line 36
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->CALLDROP_LOG_DLCH:I

    .line 39
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->eachCallDrop_Info:[Ljava/lang/String;

    return-void
.end method

.method private getDropReason(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "code"    # Ljava/lang/String;

    .prologue
    .line 244
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 245
    .local v0, "reasonCode":I
    const-string v2, "CallDropLogView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Parse integer: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    const/4 v1, 0x0

    .line 248
    .local v1, "reasonText":Ljava/lang/String;
    packed-switch v0, :pswitch_data_0

    .line 286
    :pswitch_0
    const v2, 0x7f060083

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 290
    :goto_0
    return-object v1

    .line 250
    :pswitch_1
    const v2, 0x7f06008c

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 251
    goto :goto_0

    .line 254
    :pswitch_2
    const v2, 0x7f06007b

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 255
    goto :goto_0

    .line 258
    :pswitch_3
    const v2, 0x7f06007c

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 259
    goto :goto_0

    .line 262
    :pswitch_4
    const v2, 0x7f06007d

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 263
    goto :goto_0

    .line 266
    :pswitch_5
    const v2, 0x7f06007f

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 267
    goto :goto_0

    .line 270
    :pswitch_6
    const v2, 0x7f06007e

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 271
    goto :goto_0

    .line 274
    :pswitch_7
    const v2, 0x7f060080

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 275
    goto :goto_0

    .line 278
    :pswitch_8
    const v2, 0x7f060081

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 279
    goto :goto_0

    .line 282
    :pswitch_9
    const v2, 0x7f060082

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 283
    goto :goto_0

    .line 248
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_9
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private getMultilabState(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "code"    # Ljava/lang/String;

    .prologue
    .line 342
    const v1, 0x7f060079

    invoke-virtual {p0, v1}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 344
    .local v0, "multilabState":Ljava/lang/String;
    const-string v1, "1"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 345
    const-string v1, "X"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 352
    :goto_0
    return-object v0

    .line 346
    :cond_0
    const-string v1, "2"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 347
    const-string v1, "O"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 349
    :cond_1
    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getRrcReleaseReason(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "cause"    # Ljava/lang/String;

    .prologue
    .line 294
    const-string v2, "-"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 338
    .end local p1    # "cause":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 297
    .restart local p1    # "cause":Ljava/lang/String;
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 298
    .local v0, "causeCode":I
    const-string v1, "\n"

    .line 300
    .local v1, "causeText":Ljava/lang/String;
    packed-switch v0, :pswitch_data_0

    .line 334
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    move-object p1, v1

    .line 338
    goto :goto_0

    .line 302
    :pswitch_0
    const v2, 0x7f060084

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 303
    goto :goto_1

    .line 306
    :pswitch_1
    const v2, 0x7f060085

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 307
    goto :goto_1

    .line 310
    :pswitch_2
    const v2, 0x7f060086

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 311
    goto :goto_1

    .line 314
    :pswitch_3
    const v2, 0x7f060087

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 315
    goto :goto_1

    .line 318
    :pswitch_4
    const v2, 0x7f060088

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 319
    goto :goto_1

    .line 322
    :pswitch_5
    const v2, 0x7f060089

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 323
    goto :goto_1

    .line 326
    :pswitch_6
    const v2, 0x7f06008a

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 327
    goto :goto_1

    .line 330
    :pswitch_7
    const v2, 0x7f06008b

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 331
    goto :goto_1

    .line 300
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private isJIGConnected()Z
    .locals 10

    .prologue
    .line 71
    const/4 v4, 0x0

    .line 72
    .local v4, "fileReader":Ljava/io/FileReader;
    const/4 v0, 0x0

    .line 73
    .local v0, "bufReader":Ljava/io/BufferedReader;
    const/4 v2, 0x0

    .line 76
    .local v2, "connectedType":Ljava/lang/String;
    :try_start_0
    new-instance v5, Ljava/io/FileReader;

    const-string v6, "/sys/class/sec/switch/adc"

    invoke-direct {v5, v6}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    .end local v4    # "fileReader":Ljava/io/FileReader;
    .local v5, "fileReader":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 79
    .end local v0    # "bufReader":Ljava/io/BufferedReader;
    .local v1, "bufReader":Ljava/io/BufferedReader;
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_d
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v2

    .line 88
    if-eqz v5, :cond_0

    .line 89
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 96
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 97
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_1
    move-object v0, v1

    .end local v1    # "bufReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufReader":Ljava/io/BufferedReader;
    move-object v4, v5

    .line 104
    .end local v5    # "fileReader":Ljava/io/FileReader;
    .restart local v4    # "fileReader":Ljava/io/FileReader;
    :cond_2
    :goto_1
    if-eqz v2, :cond_7

    const-string v6, "1c"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 105
    const/4 v6, 0x1

    .line 107
    :goto_2
    return v6

    .line 91
    .end local v0    # "bufReader":Ljava/io/BufferedReader;
    .end local v4    # "fileReader":Ljava/io/FileReader;
    .restart local v1    # "bufReader":Ljava/io/BufferedReader;
    .restart local v5    # "fileReader":Ljava/io/FileReader;
    :catch_0
    move-exception v3

    .line 92
    .local v3, "e":Ljava/io/IOException;
    const-string v6, "CallDropLogView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Read fileReader Close IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 99
    .end local v3    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v3

    .line 100
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v6, "CallDropLogView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Read bufReader Close IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .end local v1    # "bufReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufReader":Ljava/io/BufferedReader;
    move-object v4, v5

    .line 102
    .end local v5    # "fileReader":Ljava/io/FileReader;
    .restart local v4    # "fileReader":Ljava/io/FileReader;
    goto :goto_1

    .line 80
    .end local v3    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v3

    .line 82
    .local v3, "e":Ljava/io/FileNotFoundException;
    :goto_3
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 88
    if-eqz v4, :cond_3

    .line 89
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 96
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    :cond_3
    :goto_4
    if-eqz v0, :cond_2

    .line 97
    :try_start_7
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_1

    .line 99
    :catch_3
    move-exception v3

    .line 100
    .local v3, "e":Ljava/io/IOException;
    const-string v6, "CallDropLogView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Read bufReader Close IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 91
    .local v3, "e":Ljava/io/FileNotFoundException;
    :catch_4
    move-exception v3

    .line 92
    .local v3, "e":Ljava/io/IOException;
    const-string v6, "CallDropLogView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Read fileReader Close IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 83
    .end local v3    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v3

    .line 85
    .restart local v3    # "e":Ljava/io/IOException;
    :goto_5
    :try_start_8
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 88
    if-eqz v4, :cond_4

    .line 89
    :try_start_9
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    .line 96
    :cond_4
    :goto_6
    if-eqz v0, :cond_2

    .line 97
    :try_start_a
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    goto/16 :goto_1

    .line 99
    :catch_6
    move-exception v3

    .line 100
    const-string v6, "CallDropLogView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Read bufReader Close IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 91
    :catch_7
    move-exception v3

    .line 92
    const-string v6, "CallDropLogView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Read fileReader Close IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 87
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 88
    :goto_7
    if-eqz v4, :cond_5

    .line 89
    :try_start_b
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    .line 96
    :cond_5
    :goto_8
    if-eqz v0, :cond_6

    .line 97
    :try_start_c
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 101
    :cond_6
    :goto_9
    throw v6

    .line 91
    :catch_8
    move-exception v3

    .line 92
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v7, "CallDropLogView"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Read fileReader Close IOException: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 99
    .end local v3    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v3

    .line 100
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v7, "CallDropLogView"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Read bufReader Close IOException: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 107
    .end local v3    # "e":Ljava/io/IOException;
    :cond_7
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 87
    .end local v4    # "fileReader":Ljava/io/FileReader;
    .restart local v5    # "fileReader":Ljava/io/FileReader;
    :catchall_1
    move-exception v6

    move-object v4, v5

    .end local v5    # "fileReader":Ljava/io/FileReader;
    .restart local v4    # "fileReader":Ljava/io/FileReader;
    goto :goto_7

    .end local v0    # "bufReader":Ljava/io/BufferedReader;
    .end local v4    # "fileReader":Ljava/io/FileReader;
    .restart local v1    # "bufReader":Ljava/io/BufferedReader;
    .restart local v5    # "fileReader":Ljava/io/FileReader;
    :catchall_2
    move-exception v6

    move-object v0, v1

    .end local v1    # "bufReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufReader":Ljava/io/BufferedReader;
    move-object v4, v5

    .end local v5    # "fileReader":Ljava/io/FileReader;
    .restart local v4    # "fileReader":Ljava/io/FileReader;
    goto :goto_7

    .line 83
    .end local v4    # "fileReader":Ljava/io/FileReader;
    .restart local v5    # "fileReader":Ljava/io/FileReader;
    :catch_a
    move-exception v3

    move-object v4, v5

    .end local v5    # "fileReader":Ljava/io/FileReader;
    .restart local v4    # "fileReader":Ljava/io/FileReader;
    goto/16 :goto_5

    .end local v0    # "bufReader":Ljava/io/BufferedReader;
    .end local v4    # "fileReader":Ljava/io/FileReader;
    .restart local v1    # "bufReader":Ljava/io/BufferedReader;
    .restart local v5    # "fileReader":Ljava/io/FileReader;
    :catch_b
    move-exception v3

    move-object v0, v1

    .end local v1    # "bufReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufReader":Ljava/io/BufferedReader;
    move-object v4, v5

    .end local v5    # "fileReader":Ljava/io/FileReader;
    .restart local v4    # "fileReader":Ljava/io/FileReader;
    goto/16 :goto_5

    .line 80
    .end local v4    # "fileReader":Ljava/io/FileReader;
    .restart local v5    # "fileReader":Ljava/io/FileReader;
    :catch_c
    move-exception v3

    move-object v4, v5

    .end local v5    # "fileReader":Ljava/io/FileReader;
    .restart local v4    # "fileReader":Ljava/io/FileReader;
    goto/16 :goto_3

    .end local v0    # "bufReader":Ljava/io/BufferedReader;
    .end local v4    # "fileReader":Ljava/io/FileReader;
    .restart local v1    # "bufReader":Ljava/io/BufferedReader;
    .restart local v5    # "fileReader":Ljava/io/FileReader;
    :catch_d
    move-exception v3

    move-object v0, v1

    .end local v1    # "bufReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufReader":Ljava/io/BufferedReader;
    move-object v4, v5

    .end local v5    # "fileReader":Ljava/io/FileReader;
    .restart local v4    # "fileReader":Ljava/io/FileReader;
    goto/16 :goto_3
.end method

.method private makeCallDropLogText(Ljava/lang/String;)V
    .locals 12
    .param p1, "logView"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x1

    const/4 v9, 0x6

    const/4 v8, 0x5

    const/4 v7, 0x2

    .line 152
    const/4 v3, 0x0

    .line 154
    .local v3, "logInfoSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->logCount:I

    if-ge v0, v4, :cond_a

    .line 155
    const/4 v2, 0x0

    .line 156
    .local v2, "logInfo":[Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->eachCallDrop_Info:[Ljava/lang/String;

    aget-object v4, v4, v0

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 157
    array-length v3, v2

    .line 159
    const/4 v1, 0x2

    .local v1, "j":I
    :goto_1
    if-ge v1, v3, :cond_2

    .line 160
    aget-object v4, v2, v1

    const-string v5, "-1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 161
    if-eq v1, v9, :cond_0

    const/4 v4, 0x7

    if-eq v1, v4, :cond_0

    if-ne v1, v8, :cond_1

    .line 159
    :cond_0
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 164
    :cond_1
    const-string v4, "-"

    aput-object v4, v2, v1

    goto :goto_2

    .line 169
    :cond_2
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    const-string v5, "****************************************\n"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    .line 177
    if-ge v7, v3, :cond_4

    .line 178
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    aget-object v5, v2, v10

    invoke-direct {p0, v5}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getDropReason(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    .line 181
    aget-object v4, v2, v10

    const-string v5, "5"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 187
    aget-object v4, v2, v7

    invoke-direct {p0, v4}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getRrcReleaseReason(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v7

    .line 189
    :cond_3
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    const-string v5, "cause"

    aget-object v6, v2, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    .line 191
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    const-string v5, "\n----------------------------------------\n"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    .line 202
    :cond_4
    if-ge v11, v3, :cond_5

    .line 203
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    const v5, 0x7f060075

    invoke-virtual {p0, v5}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    .line 204
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    aget-object v5, v2, v11

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    .line 205
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    const-string v5, "    "

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    .line 208
    :cond_5
    if-ge v8, v3, :cond_6

    .line 209
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    const v5, 0x7f060076

    invoke-virtual {p0, v5}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    .line 210
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    aget-object v5, v2, v8

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    .line 211
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    .line 214
    :cond_6
    if-ge v9, v3, :cond_7

    .line 215
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    const v5, 0x7f060077

    invoke-virtual {p0, v5}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    .line 216
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    aget-object v5, v2, v9

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    .line 217
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    const-string v5, "    "

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    .line 226
    :cond_7
    const/16 v4, 0x8

    if-ge v4, v3, :cond_8

    .line 227
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    const/16 v5, 0x8

    aget-object v5, v2, v5

    invoke-direct {p0, v5}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getMultilabState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    .line 228
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    .line 231
    :cond_8
    const/16 v4, 0xa

    if-ge v4, v3, :cond_9

    .line 232
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    const v5, 0x7f06007a

    invoke-virtual {p0, v5}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    .line 233
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    const/16 v5, 0xa

    aget-object v5, v2, v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    .line 234
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    .line 235
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    const/16 v5, 0x9

    aget-object v5, v2, v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    .line 238
    :cond_9
    iget-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    const-string v5, "\n****************************************\n\n"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    .line 154
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 241
    .end local v1    # "j":I
    .end local v2    # "logInfo":[Ljava/lang/String;
    :cond_a
    return-void
.end method

.method private readCallDropLogFile()V
    .locals 10

    .prologue
    .line 111
    const/4 v3, 0x0

    .line 112
    .local v3, "fileReader":Ljava/io/FileReader;
    const/4 v0, 0x0

    .line 115
    .local v0, "bufReader":Ljava/io/BufferedReader;
    const/4 v5, 0x0

    .line 117
    .local v5, "strLine":Ljava/lang/String;
    :try_start_0
    new-instance v4, Ljava/io/FileReader;

    const-string v6, "data/log/CallDropInfoLog.txt"

    invoke-direct {v4, v6}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_c
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    .end local v3    # "fileReader":Ljava/io/FileReader;
    .local v4, "fileReader":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 120
    .end local v0    # "bufReader":Ljava/io/BufferedReader;
    .local v1, "bufReader":Ljava/io/BufferedReader;
    const/4 v6, 0x0

    :try_start_2
    iput v6, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->logCount:I

    .line 122
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 123
    iget-object v6, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->eachCallDrop_Info:[Ljava/lang/String;

    iget v7, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->logCount:I

    aput-object v5, v6, v7

    .line 124
    iget v6, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->logCount:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->logCount:I
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    .line 127
    :catch_0
    move-exception v2

    move-object v0, v1

    .end local v1    # "bufReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufReader":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 128
    .end local v4    # "fileReader":Ljava/io/FileReader;
    .local v2, "e":Ljava/io/FileNotFoundException;
    .restart local v3    # "fileReader":Ljava/io/FileReader;
    :goto_1
    :try_start_3
    const-string v6, "CallDropLogView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Read FileNotFoundException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 133
    if-eqz v3, :cond_0

    .line 134
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 141
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    :cond_0
    :goto_2
    if-eqz v0, :cond_1

    .line 142
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 148
    :cond_1
    :goto_3
    return-void

    .line 133
    .end local v0    # "bufReader":Ljava/io/BufferedReader;
    .end local v3    # "fileReader":Ljava/io/FileReader;
    .restart local v1    # "bufReader":Ljava/io/BufferedReader;
    .restart local v4    # "fileReader":Ljava/io/FileReader;
    :cond_2
    if-eqz v4, :cond_3

    .line 134
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    .line 141
    :cond_3
    :goto_4
    if-eqz v1, :cond_4

    .line 142
    :try_start_7
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    :cond_4
    move-object v0, v1

    .end local v1    # "bufReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufReader":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 146
    .end local v4    # "fileReader":Ljava/io/FileReader;
    .restart local v3    # "fileReader":Ljava/io/FileReader;
    goto :goto_3

    .line 136
    .end local v0    # "bufReader":Ljava/io/BufferedReader;
    .end local v3    # "fileReader":Ljava/io/FileReader;
    .restart local v1    # "bufReader":Ljava/io/BufferedReader;
    .restart local v4    # "fileReader":Ljava/io/FileReader;
    :catch_1
    move-exception v2

    .line 137
    .local v2, "e":Ljava/io/IOException;
    const-string v6, "CallDropLogView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Read fileReader Close IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 144
    .end local v2    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v2

    .line 145
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v6, "CallDropLogView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Read bufReader Close IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .end local v1    # "bufReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufReader":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 147
    .end local v4    # "fileReader":Ljava/io/FileReader;
    .restart local v3    # "fileReader":Ljava/io/FileReader;
    goto :goto_3

    .line 136
    .local v2, "e":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v2

    .line 137
    .local v2, "e":Ljava/io/IOException;
    const-string v6, "CallDropLogView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Read fileReader Close IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 144
    .end local v2    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v2

    .line 145
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v6, "CallDropLogView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Read bufReader Close IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 129
    .end local v2    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v2

    .line 130
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_5
    :try_start_8
    const-string v6, "CallDropLogView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Read IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 133
    if-eqz v3, :cond_5

    .line 134
    :try_start_9
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    .line 141
    :cond_5
    :goto_6
    if-eqz v0, :cond_1

    .line 142
    :try_start_a
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    goto/16 :goto_3

    .line 144
    :catch_6
    move-exception v2

    .line 145
    const-string v6, "CallDropLogView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Read bufReader Close IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 136
    :catch_7
    move-exception v2

    .line 137
    const-string v6, "CallDropLogView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Read fileReader Close IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 132
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 133
    :goto_7
    if-eqz v3, :cond_6

    .line 134
    :try_start_b
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    .line 141
    :cond_6
    :goto_8
    if-eqz v0, :cond_7

    .line 142
    :try_start_c
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 146
    :cond_7
    :goto_9
    throw v6

    .line 136
    :catch_8
    move-exception v2

    .line 137
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v7, "CallDropLogView"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Read fileReader Close IOException: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 144
    .end local v2    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v2

    .line 145
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v7, "CallDropLogView"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Read bufReader Close IOException: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 132
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fileReader":Ljava/io/FileReader;
    .restart local v4    # "fileReader":Ljava/io/FileReader;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "fileReader":Ljava/io/FileReader;
    .restart local v3    # "fileReader":Ljava/io/FileReader;
    goto :goto_7

    .end local v0    # "bufReader":Ljava/io/BufferedReader;
    .end local v3    # "fileReader":Ljava/io/FileReader;
    .restart local v1    # "bufReader":Ljava/io/BufferedReader;
    .restart local v4    # "fileReader":Ljava/io/FileReader;
    :catchall_2
    move-exception v6

    move-object v0, v1

    .end local v1    # "bufReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufReader":Ljava/io/BufferedReader;
    move-object v3, v4

    .end local v4    # "fileReader":Ljava/io/FileReader;
    .restart local v3    # "fileReader":Ljava/io/FileReader;
    goto :goto_7

    .line 129
    .end local v3    # "fileReader":Ljava/io/FileReader;
    .restart local v4    # "fileReader":Ljava/io/FileReader;
    :catch_a
    move-exception v2

    move-object v3, v4

    .end local v4    # "fileReader":Ljava/io/FileReader;
    .restart local v3    # "fileReader":Ljava/io/FileReader;
    goto/16 :goto_5

    .end local v0    # "bufReader":Ljava/io/BufferedReader;
    .end local v3    # "fileReader":Ljava/io/FileReader;
    .restart local v1    # "bufReader":Ljava/io/BufferedReader;
    .restart local v4    # "fileReader":Ljava/io/FileReader;
    :catch_b
    move-exception v2

    move-object v0, v1

    .end local v1    # "bufReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufReader":Ljava/io/BufferedReader;
    move-object v3, v4

    .end local v4    # "fileReader":Ljava/io/FileReader;
    .restart local v3    # "fileReader":Ljava/io/FileReader;
    goto/16 :goto_5

    .line 127
    :catch_c
    move-exception v2

    goto/16 :goto_1

    .end local v3    # "fileReader":Ljava/io/FileReader;
    .restart local v4    # "fileReader":Ljava/io/FileReader;
    :catch_d
    move-exception v2

    move-object v3, v4

    .end local v4    # "fileReader":Ljava/io/FileReader;
    .restart local v3    # "fileReader":Ljava/io/FileReader;
    goto/16 :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 48
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->setContentView(I)V

    .line 52
    const/high16 v0, 0x7f080000

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->callDrop_Log:Landroid/widget/TextView;

    .line 53
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 6
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 357
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 359
    const-string v1, "KOREA"

    sget-object v2, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->mCountryCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "eng"

    sget-object v2, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 360
    const-string v1, "CallDropLogView"

    const-string v2, "onCreateOptionsMenu CallDropPopUp Menu addl"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "calldrop_popup"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 363
    .local v0, "isCalldropPopup":I
    if-ne v0, v4, :cond_1

    .line 364
    const v1, 0x7f06008f

    invoke-interface {p1, v3, v4, v3, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 365
    const v1, 0x7f06008e

    invoke-interface {p1, v3, v5, v3, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 371
    .end local v0    # "isCalldropPopup":I
    :cond_0
    :goto_0
    return v4

    .line 367
    .restart local v0    # "isCalldropPopup":I
    :cond_1
    const v1, 0x7f06008d

    invoke-interface {p1, v3, v4, v3, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 368
    const v1, 0x7f060090

    invoke-interface {p1, v3, v5, v3, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 5
    .param p1, "featureId"    # I
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v4, 0x0

    .line 399
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 411
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 401
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "calldrop_popup"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 402
    const-string v0, "CallDropLogView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "call drop popup On - SettingDB: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "calldrop_popup"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 405
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "calldrop_popup"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 406
    const-string v0, "CallDropLogView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "call drop popup Off - SettingDB: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "calldrop_popup"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 399
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 376
    const-string v2, "KOREA"

    sget-object v3, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->mCountryCode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "eng"

    sget-object v3, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 377
    const-string v2, "CallDropLogView"

    const-string v3, "onPrepareOptionsMenu KOREA model"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "calldrop_popup"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 380
    .local v1, "isCalldropPopup":I
    if-ne v1, v5, :cond_1

    .line 381
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 382
    .local v0, "calldropItem":Landroid/view/MenuItem;
    const v2, 0x7f06008f

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 384
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 385
    const v2, 0x7f06008e

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 394
    .end local v0    # "calldropItem":Landroid/view/MenuItem;
    .end local v1    # "isCalldropPopup":I
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v2

    return v2

    .line 387
    .restart local v1    # "isCalldropPopup":I
    :cond_1
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 388
    .restart local v0    # "calldropItem":Landroid/view/MenuItem;
    const v2, 0x7f06008d

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 390
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 391
    const v2, 0x7f060090

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 57
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 59
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->isJIGConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    :cond_0
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    .line 62
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->readCallDropLogFile()V

    .line 63
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->makeCallDropLogText(Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->callDrop_Log:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->log:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    :goto_0
    return-void

    .line 66
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/CallDropLogView;->finish()V

    goto :goto_0
.end method
