.class public Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;
.super Landroid/app/Activity;
.source "Svc_Dbg_Dump.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$OemCommands;
    }
.end annotation


# static fields
.field public static svcdbgdump_time:Ljava/lang/String;


# instance fields
.field private SvcDebugMessage:Ljava/lang/String;

.field private buf:[B

.field private builder:Landroid/app/AlertDialog$Builder;

.field private context:Landroid/content/Context;

.field day:Ljava/lang/String;

.field private dialog_message:Ljava/lang/String;

.field private file_len:J

.field private fis:Ljava/io/FileInputStream;

.field private fos:Ljava/io/FileOutputStream;

.field hour:Ljava/lang/String;

.field private inFile:Ljava/lang/String;

.field private mCPALL:Landroid/widget/Button;

.field private mCPCPLog:Landroid/widget/Button;

.field private mCPCPLog2:Landroid/widget/Button;

.field private mCPDBGStringDisable:Landroid/widget/Button;

.field private mCPDBGStringLOG1:Landroid/widget/Button;

.field private mCPDBGStringLOG2:Landroid/widget/Button;

.field private mCPDBGStringLoopBack:Landroid/widget/Button;

.field private mCPDBGStringMSG:Landroid/widget/Button;

.field private mCPMsg:Landroid/widget/Button;

.field private mCPRamDump:Landroid/widget/Button;

.field private mClicked:Landroid/view/View$OnClickListener;

.field private mExit:Landroid/widget/Button;

.field public mHandler:Landroid/os/Handler;

.field private final mOkListener:Landroid/content/DialogInterface$OnClickListener;

.field private mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

.field private mServiceMessenger:Landroid/os/Messenger;

.field private mSvcModeMessenger:Landroid/os/Messenger;

.field private mTextView:Landroid/widget/TextView;

.field min:Ljava/lang/String;

.field month:Ljava/lang/String;

.field progressDialog:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 67
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 72
    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mServiceMessenger:Landroid/os/Messenger;

    .line 81
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->SvcDebugMessage:Ljava/lang/String;

    .line 82
    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->fis:Ljava/io/FileInputStream;

    .line 83
    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->fos:Ljava/io/FileOutputStream;

    .line 84
    const/16 v0, 0x400

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->buf:[B

    .line 111
    new-instance v0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$1;

    invoke-direct {v0, p0}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$1;-><init>(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mHandler:Landroid/os/Handler;

    .line 130
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mSvcModeMessenger:Landroid/os/Messenger;

    .line 219
    new-instance v0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$2;

    invoke-direct {v0, p0}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$2;-><init>(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    .line 378
    new-instance v0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;

    invoke-direct {v0, p0}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$3;-><init>(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mClicked:Landroid/view/View$OnClickListener;

    .line 436
    new-instance v0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$4;

    invoke-direct {v0, p0}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$4;-><init>(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mOkListener:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method private DisplayMessageDialog(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 370
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->builder:Landroid/app/AlertDialog$Builder;

    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 371
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->builder:Landroid/app/AlertDialog$Builder;

    const-string v1, "SVC Debug Dump"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 372
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 373
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->builder:Landroid/app/AlertDialog$Builder;

    const v1, 0x104000a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 374
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->builder:Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 375
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 376
    return-void
.end method

.method private RILDumpInfo()V
    .locals 6

    .prologue
    .line 341
    const-string v1, "SvcDbgDump"

    const-string v2, "RILDumpInfo"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "logcat -b radio -d -f /data/dump_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->svcdbgdump_time:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".log"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->commandLineReport(Ljava/lang/String;)V

    .line 343
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/data/dump_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->svcdbgdump_time:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".log"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->inFile:Ljava/lang/String;

    .line 344
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->inFile:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 345
    .local v0, "oFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->file_len:J

    .line 346
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Make success!\n\nFile size : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->file_len:J

    const-wide/16 v4, 0x400

    div-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Kb\nSaved Location :\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->dialog_message:Ljava/lang/String;

    .line 348
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->dialog_message:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->DisplayMessageDialog(Ljava/lang/String;)V

    .line 349
    return-void
.end method

.method private SendData(II)V
    .locals 3
    .param p1, "maincmd"    # I
    .param p2, "subcmd"    # I

    .prologue
    .line 197
    const/4 v0, 0x0

    .line 198
    .local v0, "data":[B
    const/4 v1, 0x6

    if-ne p1, v1, :cond_0

    .line 199
    invoke-static {p2}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$OemCommands;->StartDumpData(I)[B

    move-result-object v0

    .line 206
    :goto_0
    if-nez v0, :cond_2

    .line 207
    const-string v1, "SvcDbgDump"

    const-string v2, " err - data is NULL"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    :goto_1
    return-void

    .line 200
    :cond_0
    const/4 v1, 0x7

    if-ne p1, v1, :cond_1

    .line 201
    invoke-static {p2}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump$OemCommands;->StartStringData(I)[B

    move-result-object v0

    goto :goto_0

    .line 203
    :cond_1
    const-string v1, "SvcDbgDump"

    const-string v2, " Unknown Main Command"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 212
    :cond_2
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3f1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_1
.end method

.method private SystemDumpInfo()V
    .locals 6

    .prologue
    .line 352
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "logcat -b main -d -f /data/main_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->svcdbgdump_time:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".log"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->commandLineReport(Ljava/lang/String;)V

    .line 353
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/data/main"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->svcdbgdump_time:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".log"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->inFile:Ljava/lang/String;

    .line 354
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->inFile:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 355
    .local v0, "oFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->file_len:J

    .line 356
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Make success!\n\nFile size : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->file_len:J

    const-wide/16 v4, 0x400

    div-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Kb\nSaved Location :\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->dialog_message:Ljava/lang/String;

    .line 358
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->dialog_message:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->DisplayMessageDialog(Ljava/lang/String;)V

    .line 359
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;Landroid/os/Messenger;)Landroid/os/Messenger;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;
    .param p1, "x1"    # Landroid/os/Messenger;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mServiceMessenger:Landroid/os/Messenger;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPALL:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringLOG1:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringLOG2:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringDisable:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mExit:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->SendData(II)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->infoLog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPMsg:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPCPLog:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPCPLog2:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPRamDump:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringLoopBack:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringMSG:Landroid/widget/Button;

    return-object v0
.end method

.method private commandLineReport(Ljava/lang/String;)V
    .locals 5
    .param p1, "command"    # Ljava/lang/String;

    .prologue
    .line 443
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    .line 447
    .local v2, "rt":Ljava/lang/Runtime;
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v1

    .line 449
    .local v1, "p":Ljava/lang/Process;
    invoke-virtual {v1}, Ljava/lang/Process;->waitFor()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 454
    .end local v1    # "p":Ljava/lang/Process;
    :goto_0
    return-void

    .line 450
    :catch_0
    move-exception v0

    .line 451
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v4, "// Exception from"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 452
    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private connectToRilService()V
    .locals 3

    .prologue
    .line 232
    const-string v1, "SvcDbgDump"

    const-string v2, "connect To Ril service"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 234
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.phone"

    const-string v2, "com.sec.phone.SecPhoneService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 235
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 236
    return-void
.end method

.method private getCurrentTime()V
    .locals 4

    .prologue
    .line 325
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 327
    .local v0, "cal":Ljava/util/Calendar;
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v2, "00"

    invoke-direct {v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->month:Ljava/lang/String;

    .line 328
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v2, "00"

    invoke-direct {v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->day:Ljava/lang/String;

    .line 329
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v2, "00"

    invoke-direct {v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->hour:Ljava/lang/String;

    .line 330
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v2, "00"

    invoke-direct {v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->min:Ljava/lang/String;

    .line 331
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->month:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->day:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->hour:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->min:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->svcdbgdump_time:Ljava/lang/String;

    .line 332
    const-string v1, "SvcDbgDump"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SVC Debug Dump Time : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->svcdbgdump_time:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    return-void
.end method

.method private infoLog(Ljava/lang/String;)V
    .locals 2
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    .line 336
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "send EVENT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " success!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->dialog_message:Ljava/lang/String;

    .line 337
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->dialog_message:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->DisplayMessageDialog(Ljava/lang/String;)V

    .line 338
    return-void
.end method

.method private invokeOemRilRequestRaw([BLandroid/os/Message;)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "response"    # Landroid/os/Message;

    .prologue
    .line 239
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 240
    .local v0, "req":Landroid/os/Bundle;
    const-string v1, "request"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 242
    invoke-virtual {p2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 243
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mSvcModeMessenger:Landroid/os/Messenger;

    iput-object v1, p2, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 246
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mServiceMessenger:Landroid/os/Messenger;

    invoke-virtual {v1, p2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 249
    :goto_0
    return-void

    .line 247
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 255
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 256
    const v2, 0x7f03000b

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->setContentView(I)V

    .line 263
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->connectToRilService()V

    .line 266
    const v2, 0x7f080030

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mTextView:Landroid/widget/TextView;

    .line 267
    const v2, 0x7f080031

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPALL:Landroid/widget/Button;

    .line 268
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPALL:Landroid/widget/Button;

    const-string v3, "Dump]   ALL"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 269
    const v2, 0x7f080032

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPMsg:Landroid/widget/Button;

    .line 270
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPMsg:Landroid/widget/Button;

    const-string v3, "Dump]   MSG"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 271
    const v2, 0x7f080033

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPCPLog:Landroid/widget/Button;

    .line 272
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPCPLog:Landroid/widget/Button;

    const-string v3, "Dump]   CP LOG"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 273
    const v2, 0x7f080034

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPCPLog2:Landroid/widget/Button;

    .line 274
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPCPLog2:Landroid/widget/Button;

    const-string v3, "Dump]   CP LOG2"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 275
    const v2, 0x7f080035

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPRamDump:Landroid/widget/Button;

    .line 276
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPRamDump:Landroid/widget/Button;

    const-string v3, "Dump]   ramdump"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 277
    const v2, 0x7f080036

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringLoopBack:Landroid/widget/Button;

    .line 278
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringLoopBack:Landroid/widget/Button;

    const-string v3, "String]   LoopBack"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 279
    const v2, 0x7f080037

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringMSG:Landroid/widget/Button;

    .line 280
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringMSG:Landroid/widget/Button;

    const-string v3, "String]   MSG"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 281
    const v2, 0x7f080038

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringLOG1:Landroid/widget/Button;

    .line 282
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringLOG1:Landroid/widget/Button;

    const-string v3, "String]   LOG1"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 283
    const v2, 0x7f080039

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringLOG2:Landroid/widget/Button;

    .line 284
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringLOG2:Landroid/widget/Button;

    const-string v3, "String]   LOG2"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 285
    const v2, 0x7f08003a

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringDisable:Landroid/widget/Button;

    .line 286
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringDisable:Landroid/widget/Button;

    const-string v3, "String]   Disable"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 287
    const v2, 0x7f08002b

    invoke-virtual {p0, v2}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mExit:Landroid/widget/Button;

    .line 288
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPALL:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 289
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPMsg:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 290
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPCPLog:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 291
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPCPLog2:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 292
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPRamDump:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 293
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringLoopBack:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 294
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringMSG:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 295
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringLOG1:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 296
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringLOG2:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 297
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mCPDBGStringDisable:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 298
    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mExit:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 300
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->progressDialog:Landroid/app/ProgressDialog;

    .line 301
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->builder:Landroid/app/AlertDialog$Builder;

    .line 303
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->getCurrentTime()V

    .line 305
    iput-object p0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->context:Landroid/content/Context;

    .line 308
    invoke-virtual {p0}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 309
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "TYPE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 310
    const-string v2, "TYPE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 311
    .local v1, "type":Ljava/lang/String;
    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 313
    const-string v2, "SvcDbgDump"

    const-string v3, "RIL log"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->RILDumpInfo()V

    .line 316
    :cond_0
    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 318
    const-string v2, "SvcDbgDump"

    const-string v3, "System log"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->SystemDumpInfo()V

    .line 322
    .end local v1    # "type":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->unbindService(Landroid/content/ServiceConnection;)V

    .line 364
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/Svc_Dbg_Dump;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    .line 366
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 367
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 458
    .line 463
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method
