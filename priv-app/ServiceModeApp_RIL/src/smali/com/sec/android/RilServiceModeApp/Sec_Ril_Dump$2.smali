.class Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$2;
.super Ljava/lang/Object;
.source "Sec_Ril_Dump.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;


# direct methods
.method constructor <init>(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;)V
    .locals 0

    .prologue
    .line 547
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$2;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 549
    const-string v0, "RilDump"

    const-string v1, "onServiceConnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$2;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1402(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 551
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 554
    const-string v0, "RilDump"

    const-string v1, "onServiceDisconnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump$2;->this$0:Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;->access$1402(Lcom/sec/android/RilServiceModeApp/Sec_Ril_Dump;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 556
    return-void
.end method
