.class public Lcom/sec/android/RilServiceModeApp/ClatConfiguration;
.super Landroid/app/Activity;
.source "ClatConfiguration.java"


# static fields
.field private static IPV4_LOCAL_SUBNET:Ljava/lang/String;

.field private static IPV6_HOST_ID:Ljava/lang/String;

.field private static LOG_TAG:Ljava/lang/String;

.field private static PATH_CLATD_CONF:Ljava/lang/String;

.field private static PATH_MODIFIED_CONF:Ljava/lang/String;

.field private static PLAT_FROM_DNS64:Ljava/lang/String;

.field private static PLAT_FROM_DNS64_HOSTNAME:Ljava/lang/String;

.field private static mIpv4LocalSubnetValue:Ljava/lang/String;

.field private static mIpv6HostIdValue:Ljava/lang/String;

.field private static mPlatFromDns64HostnameValue:Ljava/lang/String;

.field private static mPlatFromDns64Value:Ljava/lang/String;


# instance fields
.field private mClicked:Landroid/view/View$OnClickListener;

.field mCm:Landroid/net/ConnectivityManager;

.field private mConfPath:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mIpv4LocalSubnet:Landroid/widget/EditText;

.field private mIpv6HostId:Landroid/widget/EditText;

.field private mPlatFromDns64:Landroid/widget/Spinner;

.field private mPlatFromDns64Adap:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private mPlatFromDns64Hostname:Landroid/widget/EditText;

.field private mProgressHandler:Landroid/os/Handler;

.field private mRestoreBtn:Landroid/widget/Button;

.field mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mUpdateBtn:Landroid/widget/Button;

.field private mWaitDiag:Landroid/app/ProgressDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-string v0, "ClatConfiguration"

    sput-object v0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    .line 62
    const-string v0, "ipv6_host_id"

    sput-object v0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->IPV6_HOST_ID:Ljava/lang/String;

    .line 63
    const-string v0, "ipv4_local_subnet"

    sput-object v0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->IPV4_LOCAL_SUBNET:Ljava/lang/String;

    .line 64
    const-string v0, "plat_from_dns64"

    sput-object v0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->PLAT_FROM_DNS64:Ljava/lang/String;

    .line 65
    const-string v0, "plat_from_dns64_hostname"

    sput-object v0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->PLAT_FROM_DNS64_HOSTNAME:Ljava/lang/String;

    .line 68
    const-string v0, "/system/etc/clatd.conf"

    sput-object v0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->PATH_CLATD_CONF:Ljava/lang/String;

    .line 69
    const-string v0, "/data/misc/radio/.clatd.mod.conf"

    sput-object v0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->PATH_MODIFIED_CONF:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mContext:Landroid/content/Context;

    .line 132
    new-instance v0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$2;

    invoke-direct {v0, p0}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$2;-><init>(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mClicked:Landroid/view/View$OnClickListener;

    .line 160
    new-instance v0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$3;

    invoke-direct {v0, p0}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$3;-><init>(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)V

    iput-object v0, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mProgressHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mUpdateBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->replaceConfigures()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mProgressHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->readConfigures()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mRestoreBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->restoreConfigures()V

    return-void
.end method

.method static synthetic access$700()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/ClatConfiguration;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mWaitDiag:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/RilServiceModeApp/ClatConfiguration;
    .param p1, "x1"    # Landroid/app/ProgressDialog;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mWaitDiag:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method private backupConf()V
    .locals 12

    .prologue
    .line 200
    const/4 v4, 0x0

    .line 201
    .local v4, "fis":Ljava/io/FileInputStream;
    const/4 v6, 0x0

    .line 204
    .local v6, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/File;

    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->PATH_MODIFIED_CONF:Ljava/lang/String;

    invoke-direct {v2, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 205
    .local v2, "fBak":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->PATH_CLATD_CONF:Ljava/lang/String;

    invoke-direct {v3, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 207
    .local v3, "fOrg":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_2

    .line 208
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    const-string v9, "original configuration is missing."

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mConfPath:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249
    if-eqz v4, :cond_0

    .line 250
    :try_start_1
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 252
    :cond_0
    if-eqz v6, :cond_1

    .line 253
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 259
    .end local v2    # "fBak":Ljava/io/File;
    .end local v3    # "fOrg":Ljava/io/File;
    :cond_1
    :goto_0
    return-void

    .line 255
    .restart local v2    # "fBak":Ljava/io/File;
    .restart local v3    # "fOrg":Ljava/io/File;
    :catch_0
    move-exception v1

    .line 256
    .local v1, "e":Ljava/io/IOException;
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "IOException : cannot close fis or fos "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 213
    .end local v1    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_2
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 214
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    const-string v9, "backup configuration is already created."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->PATH_MODIFIED_CONF:Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mConfPath:Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 249
    if-eqz v4, :cond_3

    .line 250
    :try_start_3
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 252
    :cond_3
    if-eqz v6, :cond_1

    .line 253
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 255
    :catch_1
    move-exception v1

    .line 256
    .restart local v1    # "e":Ljava/io/IOException;
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "IOException : cannot close fis or fos "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 218
    .end local v1    # "e":Ljava/io/IOException;
    :cond_4
    :try_start_4
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    const-string v9, "backup configuration is missing. Create it."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 220
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_5

    .line 221
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    const-string v9, "original configuration is missing."

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :cond_5
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    const-string v9, "Do backup."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    const/4 v0, 0x0

    .line 229
    .local v0, "data":I
    new-instance v5, Ljava/io/FileInputStream;

    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->PATH_CLATD_CONF:Ljava/lang/String;

    invoke-direct {v5, v8}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 230
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .local v5, "fis":Ljava/io/FileInputStream;
    :try_start_5
    new-instance v7, Ljava/io/FileOutputStream;

    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->PATH_MODIFIED_CONF:Ljava/lang/String;

    invoke-direct {v7, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 232
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .local v7, "fos":Ljava/io/FileOutputStream;
    :goto_1
    :try_start_6
    invoke-virtual {v5}, Ljava/io/FileInputStream;->read()I

    move-result v0

    const/4 v8, -0x1

    if-eq v0, v8, :cond_7

    .line 233
    invoke-virtual {v7, v0}, Ljava/io/FileOutputStream;->write(I)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    goto :goto_1

    .line 243
    :catch_2
    move-exception v1

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    move-object v4, v5

    .line 244
    .end local v0    # "data":I
    .end local v2    # "fBak":Ljava/io/File;
    .end local v3    # "fOrg":Ljava/io/File;
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "e":Ljava/io/IOException;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :goto_2
    :try_start_7
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "IOException : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 246
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->PATH_CLATD_CONF:Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mConfPath:Ljava/lang/String;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 249
    if-eqz v4, :cond_6

    .line 250
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 252
    :cond_6
    if-eqz v6, :cond_1

    .line 253
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    goto/16 :goto_0

    .line 255
    :catch_3
    move-exception v1

    .line 256
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "IOException : cannot close fis or fos "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 236
    .end local v1    # "e":Ljava/io/IOException;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v0    # "data":I
    .restart local v2    # "fBak":Ljava/io/File;
    .restart local v3    # "fOrg":Ljava/io/File;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :cond_7
    :try_start_9
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    .line 237
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    .line 239
    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Ljava/io/File;->setReadable(ZZ)Z

    .line 240
    const/4 v8, 0x1

    invoke-virtual {v2, v8}, Ljava/io/File;->setWritable(Z)Z

    .line 242
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->PATH_MODIFIED_CONF:Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mConfPath:Ljava/lang/String;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 249
    if-eqz v5, :cond_8

    .line 250
    :try_start_a
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    .line 252
    :cond_8
    if-eqz v7, :cond_9

    .line 253
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    :cond_9
    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    move-object v4, v5

    .line 257
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_0

    .line 255
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v1

    .line 256
    .restart local v1    # "e":Ljava/io/IOException;
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "IOException : cannot close fis or fos "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    move-object v4, v5

    .line 258
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_0

    .line 248
    .end local v0    # "data":I
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "fBak":Ljava/io/File;
    .end local v3    # "fOrg":Ljava/io/File;
    :catchall_0
    move-exception v8

    .line 249
    :goto_3
    if-eqz v4, :cond_a

    .line 250
    :try_start_b
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 252
    :cond_a
    if-eqz v6, :cond_b

    .line 253
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5

    .line 257
    :cond_b
    :goto_4
    throw v8

    .line 255
    :catch_5
    move-exception v1

    .line 256
    .restart local v1    # "e":Ljava/io/IOException;
    sget-object v9, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "IOException : cannot close fis or fos "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 248
    .end local v1    # "e":Ljava/io/IOException;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v0    # "data":I
    .restart local v2    # "fBak":Ljava/io/File;
    .restart local v3    # "fOrg":Ljava/io/File;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v8

    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_3

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v8

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_3

    .line 243
    .end local v0    # "data":I
    .end local v2    # "fBak":Ljava/io/File;
    .end local v3    # "fOrg":Ljava/io/File;
    :catch_6
    move-exception v1

    goto/16 :goto_2

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v0    # "data":I
    .restart local v2    # "fBak":Ljava/io/File;
    .restart local v3    # "fOrg":Ljava/io/File;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catch_7
    move-exception v1

    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_2
.end method

.method private readConfigures()V
    .locals 4

    .prologue
    .line 262
    sget-object v1, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Update UI from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mConfPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mConfPath:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->IPV6_HOST_ID:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->readValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mIpv6HostIdValue:Ljava/lang/String;

    .line 264
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mConfPath:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->IPV4_LOCAL_SUBNET:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->readValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mIpv4LocalSubnetValue:Ljava/lang/String;

    .line 265
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mConfPath:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->PLAT_FROM_DNS64:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->readValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mPlatFromDns64Value:Ljava/lang/String;

    .line 266
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mConfPath:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->PLAT_FROM_DNS64_HOSTNAME:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->readValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mPlatFromDns64HostnameValue:Ljava/lang/String;

    .line 269
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mIpv6HostId:Landroid/widget/EditText;

    sget-object v2, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mIpv6HostIdValue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 270
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mIpv4LocalSubnet:Landroid/widget/EditText;

    sget-object v2, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mIpv4LocalSubnetValue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 271
    const-string v1, "yes"

    sget-object v2, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mPlatFromDns64Value:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    .line 272
    .local v0, "id":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mPlatFromDns64:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 273
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mPlatFromDns64Hostname:Landroid/widget/EditText;

    sget-object v2, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mPlatFromDns64HostnameValue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 275
    return-void

    .line 271
    .end local v0    # "id":I
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private readValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 321
    const/4 v0, 0x0

    .line 322
    .local v0, "br":Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 325
    .local v4, "retStr":Ljava/lang/String;
    if-nez p1, :cond_1

    .line 326
    :try_start_0
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    const-string v9, "path is null"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 356
    if-eqz v0, :cond_0

    .line 357
    :try_start_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 364
    :cond_0
    :goto_0
    return-object v7

    .line 359
    :catch_0
    move-exception v2

    .line 360
    .local v2, "e":Ljava/io/IOException;
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "IOException : cannot close fw or br "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 331
    .end local v2    # "e":Ljava/io/IOException;
    :cond_1
    :try_start_2
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 333
    .local v3, "fi":Ljava/io/File;
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/FileReader;

    invoke-direct {v8, v3}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 335
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    :cond_2
    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .local v5, "str":Ljava/lang/String;
    if-eqz v5, :cond_3

    .line 336
    invoke-virtual {v5, p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 337
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 338
    new-instance v6, Ljava/util/StringTokenizer;

    const-string v8, " \n"

    invoke-direct {v6, v5, v8}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    .local v6, "tok":Ljava/util/StringTokenizer;
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    .line 340
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 341
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    .line 349
    .end local v6    # "tok":Ljava/util/StringTokenizer;
    :cond_3
    :goto_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 356
    if-eqz v1, :cond_4

    .line 357
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_4
    :goto_2
    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v7, v4

    .line 364
    goto :goto_0

    .line 343
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v6    # "tok":Ljava/util/StringTokenizer;
    :cond_5
    const/4 v4, 0x0

    .line 345
    goto :goto_1

    .line 359
    .end local v6    # "tok":Ljava/util/StringTokenizer;
    :catch_1
    move-exception v2

    .line 360
    .restart local v2    # "e":Ljava/io/IOException;
    sget-object v7, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "IOException : cannot close fw or br "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 350
    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fi":Ljava/io/File;
    .end local v5    # "str":Ljava/lang/String;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    :catch_2
    move-exception v2

    .line 351
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_5
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Couldn\'t find "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " in "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 356
    if-eqz v0, :cond_0

    .line 357
    :try_start_6
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_0

    .line 359
    :catch_3
    move-exception v2

    .line 360
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "IOException : cannot close fw or br "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 355
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 356
    :goto_4
    if-eqz v0, :cond_6

    .line 357
    :try_start_7
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 361
    :cond_6
    :goto_5
    throw v7

    .line 359
    :catch_4
    move-exception v2

    .line 360
    .restart local v2    # "e":Ljava/io/IOException;
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "IOException : cannot close fw or br "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 355
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "fi":Ljava/io/File;
    :catchall_1
    move-exception v7

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_4

    .line 350
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catch_5
    move-exception v2

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_3
.end method

.method private replaceConfigures()Z
    .locals 4

    .prologue
    .line 278
    const/4 v0, 0x0

    .line 280
    .local v0, "isChanged":Z
    sget-object v1, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mIpv6HostIdValue:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mIpv6HostId:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 281
    sget-object v1, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ipv6_host_id changed to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mIpv6HostId:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " update it."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mConfPath:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->IPV6_HOST_ID:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mIpv6HostId:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->replaceValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    const/4 v0, 0x1

    .line 286
    :cond_0
    sget-object v1, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mIpv4LocalSubnetValue:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mIpv4LocalSubnet:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 287
    sget-object v1, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ipv4_local_subnet changed to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mIpv4LocalSubnet:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " update it."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mConfPath:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->IPV4_LOCAL_SUBNET:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mIpv4LocalSubnet:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->replaceValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    const/4 v0, 0x1

    .line 292
    :cond_1
    sget-object v1, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mPlatFromDns64Value:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mPlatFromDns64:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 293
    sget-object v1, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "plat_from_dns64 changed to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mPlatFromDns64:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " update it."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mConfPath:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->PLAT_FROM_DNS64:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mPlatFromDns64:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->replaceValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    const/4 v0, 0x1

    .line 298
    :cond_2
    sget-object v1, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mPlatFromDns64HostnameValue:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mPlatFromDns64Hostname:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 299
    sget-object v1, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "plat_from_dns64_hostname changed to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mPlatFromDns64Hostname:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " update it."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mConfPath:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->PLAT_FROM_DNS64_HOSTNAME:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mPlatFromDns64Hostname:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->replaceValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    const/4 v0, 0x1

    .line 304
    :cond_3
    return v0
.end method

.method private replaceValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 368
    const/4 v4, 0x0

    .line 369
    .local v4, "fw":Ljava/io/FileWriter;
    const/4 v0, 0x0

    .line 372
    .local v0, "br":Ljava/io/BufferedReader;
    if-nez p1, :cond_2

    .line 373
    :try_start_0
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    const-string v9, "path is null"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_9
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 400
    if-eqz v4, :cond_0

    .line 401
    :try_start_1
    invoke-virtual {v4}, Ljava/io/FileWriter;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 407
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 408
    :try_start_2
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 414
    :cond_1
    :goto_1
    return-void

    .line 403
    :catch_0
    move-exception v2

    .line 404
    .local v2, "e":Ljava/io/IOException;
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "IOException : cannot close fw"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 410
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 411
    .restart local v2    # "e":Ljava/io/IOException;
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "IOException : cannot close br"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 378
    .end local v2    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_3
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 380
    .local v3, "fi":Ljava/io/File;
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/FileReader;

    invoke-direct {v8, v3}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 381
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    :try_start_4
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    .line 383
    .local v7, "writeStr":Ljava/lang/StringBuffer;
    :goto_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .local v6, "str":Ljava/lang/String;
    if-eqz v6, :cond_7

    .line 384
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 385
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 395
    .end local v6    # "str":Ljava/lang/String;
    .end local v7    # "writeStr":Ljava/lang/StringBuffer;
    :catch_2
    move-exception v2

    move-object v0, v1

    .line 396
    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v3    # "fi":Ljava/io/File;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_5
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Couldn\'t replaced "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ": "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " in "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 400
    if-eqz v4, :cond_3

    .line 401
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileWriter;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    .line 407
    :cond_3
    :goto_4
    if-eqz v0, :cond_1

    .line 408
    :try_start_7
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto/16 :goto_1

    .line 410
    :catch_3
    move-exception v2

    .line 411
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "IOException : cannot close br"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 387
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "fi":Ljava/io/File;
    .restart local v6    # "str":Ljava/lang/String;
    .restart local v7    # "writeStr":Ljava/lang/StringBuffer;
    :cond_4
    :try_start_8
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_2

    .line 399
    .end local v6    # "str":Ljava/lang/String;
    .end local v7    # "writeStr":Ljava/lang/StringBuffer;
    :catchall_0
    move-exception v8

    move-object v0, v1

    .line 400
    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v3    # "fi":Ljava/io/File;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    :goto_5
    if-eqz v4, :cond_5

    .line 401
    :try_start_9
    invoke-virtual {v4}, Ljava/io/FileWriter;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    .line 407
    :cond_5
    :goto_6
    if-eqz v0, :cond_6

    .line 408
    :try_start_a
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_8

    .line 412
    :cond_6
    :goto_7
    throw v8

    .line 390
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "fi":Ljava/io/File;
    .restart local v6    # "str":Ljava/lang/String;
    .restart local v7    # "writeStr":Ljava/lang/StringBuffer;
    :cond_7
    :try_start_b
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 392
    new-instance v5, Ljava/io/FileWriter;

    invoke-direct {v5, p1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 393
    .end local v4    # "fw":Ljava/io/FileWriter;
    .local v5, "fw":Ljava/io/FileWriter;
    :try_start_c
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 394
    invoke-virtual {v5}, Ljava/io/FileWriter;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_a
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 400
    if-eqz v5, :cond_8

    .line 401
    :try_start_d
    invoke-virtual {v5}, Ljava/io/FileWriter;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_4

    .line 407
    :cond_8
    :goto_8
    if-eqz v1, :cond_9

    .line 408
    :try_start_e
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_5

    :cond_9
    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v4, v5

    .line 412
    .end local v5    # "fw":Ljava/io/FileWriter;
    .restart local v4    # "fw":Ljava/io/FileWriter;
    goto/16 :goto_1

    .line 403
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v4    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v5    # "fw":Ljava/io/FileWriter;
    :catch_4
    move-exception v2

    .line 404
    .restart local v2    # "e":Ljava/io/IOException;
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "IOException : cannot close fw"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 410
    .end local v2    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v2

    .line 411
    .restart local v2    # "e":Ljava/io/IOException;
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "IOException : cannot close br"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v4, v5

    .line 413
    .end local v5    # "fw":Ljava/io/FileWriter;
    .restart local v4    # "fw":Ljava/io/FileWriter;
    goto/16 :goto_1

    .line 403
    .end local v3    # "fi":Ljava/io/File;
    .end local v6    # "str":Ljava/lang/String;
    .end local v7    # "writeStr":Ljava/lang/StringBuffer;
    :catch_6
    move-exception v2

    .line 404
    sget-object v8, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "IOException : cannot close fw"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 403
    .end local v2    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v2

    .line 404
    .restart local v2    # "e":Ljava/io/IOException;
    sget-object v9, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "IOException : cannot close fw"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 410
    .end local v2    # "e":Ljava/io/IOException;
    :catch_8
    move-exception v2

    .line 411
    .restart local v2    # "e":Ljava/io/IOException;
    sget-object v9, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "IOException : cannot close br"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 399
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v8

    goto/16 :goto_5

    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v4    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "fi":Ljava/io/File;
    .restart local v5    # "fw":Ljava/io/FileWriter;
    .restart local v6    # "str":Ljava/lang/String;
    .restart local v7    # "writeStr":Ljava/lang/StringBuffer;
    :catchall_2
    move-exception v8

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v4, v5

    .end local v5    # "fw":Ljava/io/FileWriter;
    .restart local v4    # "fw":Ljava/io/FileWriter;
    goto/16 :goto_5

    .line 395
    .end local v3    # "fi":Ljava/io/File;
    .end local v6    # "str":Ljava/lang/String;
    .end local v7    # "writeStr":Ljava/lang/StringBuffer;
    :catch_9
    move-exception v2

    goto/16 :goto_3

    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v4    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "fi":Ljava/io/File;
    .restart local v5    # "fw":Ljava/io/FileWriter;
    .restart local v6    # "str":Ljava/lang/String;
    .restart local v7    # "writeStr":Ljava/lang/StringBuffer;
    :catch_a
    move-exception v2

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v4, v5

    .end local v5    # "fw":Ljava/io/FileWriter;
    .restart local v4    # "fw":Ljava/io/FileWriter;
    goto/16 :goto_3
.end method

.method private restoreConfigures()V
    .locals 3

    .prologue
    .line 308
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->PATH_MODIFIED_CONF:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 309
    .local v0, "fBak":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 310
    sget-object v1, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    const-string v2, "backup configuration was already deleted."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->backupConf()V

    .line 318
    return-void

    .line 312
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 313
    sget-object v1, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->LOG_TAG:Ljava/lang/String;

    const-string v2, "fail to delete backup configuration"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 81
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 83
    iput-object p0, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mContext:Landroid/content/Context;

    .line 84
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 86
    .local v0, "res":Landroid/content/res/Resources;
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mContext:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mCm:Landroid/net/ConnectivityManager;

    .line 87
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 90
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->backupConf()V

    .line 92
    const v1, 0x7f0600aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->setTitle(Ljava/lang/CharSequence;)V

    .line 93
    const v1, 0x7f030001

    invoke-virtual {p0, v1}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->setContentView(I)V

    .line 95
    const v1, 0x7f080002

    invoke-virtual {p0, v1}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mIpv6HostId:Landroid/widget/EditText;

    .line 98
    const v1, 0x7f080004

    invoke-virtual {p0, v1}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mIpv4LocalSubnet:Landroid/widget/EditText;

    .line 101
    const v1, 0x7f080006

    invoke-virtual {p0, v1}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mPlatFromDns64:Landroid/widget/Spinner;

    .line 103
    const v1, 0x7f050003

    const v2, 0x1090008

    invoke-static {p0, v1, v2}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mPlatFromDns64Adap:Landroid/widget/ArrayAdapter;

    .line 105
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mPlatFromDns64Adap:Landroid/widget/ArrayAdapter;

    const v2, 0x1090009

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 106
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mPlatFromDns64:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mPlatFromDns64Adap:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 107
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mPlatFromDns64:Landroid/widget/Spinner;

    new-instance v2, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$1;

    invoke-direct {v2, p0}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration$1;-><init>(Lcom/sec/android/RilServiceModeApp/ClatConfiguration;)V

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 117
    const v1, 0x7f080008

    invoke-virtual {p0, v1}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mPlatFromDns64Hostname:Landroid/widget/EditText;

    .line 124
    const v1, 0x7f080009

    invoke-virtual {p0, v1}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mUpdateBtn:Landroid/widget/Button;

    .line 125
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mUpdateBtn:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    const v1, 0x7f08000a

    invoke-virtual {p0, v1}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mRestoreBtn:Landroid/widget/Button;

    .line 127
    iget-object v1, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mRestoreBtn:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    invoke-direct {p0}, Lcom/sec/android/RilServiceModeApp/ClatConfiguration;->readConfigures()V

    .line 130
    return-void
.end method
