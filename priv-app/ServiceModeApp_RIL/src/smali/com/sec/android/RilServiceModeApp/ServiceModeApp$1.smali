.class Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;
.super Landroid/os/Handler;
.source "ServiceModeApp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/RilServiceModeApp/ServiceModeApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;


# direct methods
.method constructor <init>(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 17
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 123
    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->what:I

    sparse-switch v2, :sswitch_data_0

    .line 216
    :goto_0
    return-void

    .line 125
    :sswitch_0
    const-string v2, "ServiceModeApp_RIL"

    const-string v3, "Service mode End"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    invoke-virtual {v2}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->finish()V

    goto :goto_0

    .line 130
    :sswitch_1
    const-string v2, "ServiceModeApp_RIL"

    const-string v3, "in QUERT_SERVM_DONE"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "error"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    .line 132
    .local v10, "error":I
    if-eqz v10, :cond_0

    .line 133
    const-string v2, "ServiceModeApp_RIL"

    const-string v3, "Exception Occur : %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 135
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "response"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v9

    check-cast v9, [B

    .line 137
    .local v9, "buf":[B
    const-string v2, "ServiceModeApp_RIL"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "size of result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, v9

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    array-length v2, v9

    if-nez v2, :cond_1

    .line 140
    const-string v2, "ServiceModeApp_RIL"

    const-string v3, "response is null"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    iget-object v2, v2, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x3e9

    const-wide/16 v4, 0x5dc

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 146
    :cond_1
    array-length v2, v9

    div-int/lit8 v15, v2, 0x22

    .line 147
    .local v15, "numOfLine":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$000(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    if-le v15, v2, :cond_2

    .line 148
    const-string v2, "ServiceModeApp_RIL"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Too many lines. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$000(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)[Ljava/lang/String;

    move-result-object v2

    array-length v15, v2

    .line 151
    :cond_2
    const/4 v12, 0x0

    .line 153
    .local v12, "index":I
    const-string v2, "276633683782"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$100(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 154
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    const/16 v2, 0x32

    if-ge v11, v2, :cond_3

    .line 155
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$000(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)[Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    aput-object v3, v2, v11

    .line 154
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 158
    .end local v11    # "i":I
    :cond_3
    const/4 v14, 0x0

    .line 159
    .local v14, "k":I
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_2
    if-ge v11, v15, :cond_a

    .line 160
    const-string v16, ""

    .line 161
    .local v16, "rowString":Ljava/lang/String;
    const/4 v13, 0x2

    .local v13, "j":I
    :goto_3
    const/16 v2, 0x22

    if-ge v13, v2, :cond_4

    .line 162
    mul-int/lit8 v2, v11, 0x22

    add-int/2addr v2, v13

    array-length v3, v9

    if-lt v2, v3, :cond_7

    .line 163
    const-string v2, "ServiceModeApp_RIL"

    const-string v3, "unexpected end of line"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    :cond_4
    const-string v2, "276633683782"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mKeyString:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$100(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 179
    if-eqz v11, :cond_5

    const/4 v2, 0x5

    if-ne v11, v2, :cond_8

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    iget v2, v2, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->level:I

    if-nez v2, :cond_8

    .line 180
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$000(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)[Ljava/lang/String;

    move-result-object v2

    aput-object v16, v2, v14

    .line 181
    add-int/lit8 v14, v14, 0x1

    .line 188
    :cond_6
    :goto_4
    const-string v2, "ServiceModeApp_RIL"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Line "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$000(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v11

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 166
    :cond_7
    mul-int/lit8 v2, v11, 0x22

    add-int/2addr v2, v13

    aget-byte v2, v9, v2

    if-eqz v2, :cond_4

    .line 169
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    mul-int/lit8 v3, v11, 0x22

    add-int/2addr v3, v13

    aget-byte v3, v9, v3

    int-to-char v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 161
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_3

    .line 182
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    iget v2, v2, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->level:I

    if-lez v2, :cond_6

    .line 183
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$000(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)[Ljava/lang/String;

    move-result-object v2

    aput-object v16, v2, v11

    goto :goto_4

    .line 186
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mStrings:[Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$000(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)[Ljava/lang/String;

    move-result-object v2

    aput-object v16, v2, v11

    goto :goto_4

    .line 202
    .end local v13    # "j":I
    .end local v16    # "rowString":Ljava/lang/String;
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # invokes: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->AnalisysString()V
    invoke-static {v2}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$200(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)V

    goto/16 :goto_0

    .line 207
    .end local v9    # "buf":[B
    .end local v10    # "error":I
    .end local v11    # "i":I
    .end local v12    # "index":I
    .end local v14    # "k":I
    .end local v15    # "numOfLine":I
    :sswitch_2
    const-string v2, "ServiceModeApp_RIL"

    const-string v3, "EVENT_TICK!"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->currentSVMode:C
    invoke-static {v3}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$300(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)C

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;
    invoke-static {v4}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$400(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;
    invoke-static {v5}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$400(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;
    invoke-static {v6}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$400(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->mOem:Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;
    invoke-static {v7}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$400(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)Lcom/sec/android/RilServiceModeApp/ServiceModeApp$OemCommands;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v7, 0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/RilServiceModeApp/ServiceModeApp$1;->this$0:Lcom/sec/android/RilServiceModeApp/ServiceModeApp;

    # getter for: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->oem_modem_type:C
    invoke-static {v8}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$500(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;)C

    move-result v8

    # invokes: Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->SendData(CCCCCC)V
    invoke-static/range {v2 .. v8}, Lcom/sec/android/RilServiceModeApp/ServiceModeApp;->access$600(Lcom/sec/android/RilServiceModeApp/ServiceModeApp;CCCCCC)V

    goto/16 :goto_0

    .line 123
    :sswitch_data_0
    .sparse-switch
        0x3e9 -> :sswitch_2
        0x3f0 -> :sswitch_1
        0x3f1 -> :sswitch_0
    .end sparse-switch
.end method
