.class public final Lcom/sec/android/app/myfiles/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final EmptyText:I = 0x7f0f00b0

.field public static final LinearLayout01:I = 0x7f0f00af

.field public static final VideoView:I = 0x7f0f0119

.field public static final VideoViewLayout:I = 0x7f0f0118

.field public static final VideoViewNoVideo:I = 0x7f0f011a

.field public static final action_bar_common:I = 0x7f0f000a

.field public static final action_bar_home_as_up:I = 0x7f0f000b

.field public static final action_bar_search_view:I = 0x7f0f000d

.field public static final action_bar_title:I = 0x7f0f0007

.field public static final action_bar_up_button:I = 0x7f0f0006

.field public static final action_menu_items:I = 0x7f0f014b

.field public static final actionbar_selectall_check:I = 0x7f0f0002

.field public static final actionmode_menu_lock:I = 0x7f0f012e

.field public static final actionmode_menu_unlock:I = 0x7f0f012f

.field public static final actionmode_search_layout:I = 0x7f0f0010

.field public static final add_ftp_anonymous:I = 0x7f0f0028

.field public static final add_ftp_anonymous_layout:I = 0x7f0f0027

.field public static final add_ftp_anonymous_textview:I = 0x7f0f0029

.field public static final add_ftp_encoding:I = 0x7f0f002c

.field public static final add_ftp_encoding_container:I = 0x7f0f002a

.field public static final add_ftp_encoding_textview:I = 0x7f0f002b

.field public static final add_ftp_encryption:I = 0x7f0f002d

.field public static final add_ftp_mode:I = 0x7f0f0020

.field public static final add_ftp_mode_container:I = 0x7f0f001e

.field public static final add_ftp_mode_textview:I = 0x7f0f001f

.field public static final add_ftp_password:I = 0x7f0f0026

.field public static final add_ftp_password_container:I = 0x7f0f0024

.field public static final add_ftp_password_textview:I = 0x7f0f0025

.field public static final add_ftp_port:I = 0x7f0f001d

.field public static final add_ftp_port_container:I = 0x7f0f001b

.field public static final add_ftp_port_textview:I = 0x7f0f001c

.field public static final add_ftp_server:I = 0x7f0f001a

.field public static final add_ftp_server_container:I = 0x7f0f0018

.field public static final add_ftp_server_textview:I = 0x7f0f0019

.field public static final add_ftp_sessionname:I = 0x7f0f0017

.field public static final add_ftp_sessionname_container:I = 0x7f0f0015

.field public static final add_ftp_sessionname_textview:I = 0x7f0f0016

.field public static final add_ftp_username:I = 0x7f0f0023

.field public static final add_ftp_username_container:I = 0x7f0f0021

.field public static final add_ftp_username_textview:I = 0x7f0f0022

.field public static final advance_date:I = 0x7f0f00e9

.field public static final advance_extention:I = 0x7f0f00ef

.field public static final advance_extention_search_view:I = 0x7f0f00f1

.field public static final advance_filetype:I = 0x7f0f00e5

.field public static final advance_location:I = 0x7f0f00e1

.field public static final advance_name:I = 0x7f0f00df

.field public static final advance_name_search_view:I = 0x7f0f00e0

.field public static final advanced_search_result:I = 0x7f0f000e

.field public static final app_size:I = 0x7f0f00ac

.field public static final application_name:I = 0x7f0f00aa

.field public static final boundary:I = 0x7f0f0059

.field public static final cache_size:I = 0x7f0f00ae

.field public static final cancel_add_ftp:I = 0x7f0f011e

.field public static final cancel_button:I = 0x7f0f009b

.field public static final category_item_container:I = 0x7f0f0113

.field public static final category_titlebar_container:I = 0x7f0f0045

.field public static final checked:I = 0x7f0f011c

.field public static final cloud_header:I = 0x7f0f0084

.field public static final cloud_header_container:I = 0x7f0f0083

.field public static final confirm_oma_field_description:I = 0x7f0f00a2

.field public static final confirm_oma_field_name:I = 0x7f0f009d

.field public static final confirm_oma_field_size:I = 0x7f0f009f

.field public static final confirm_oma_field_type:I = 0x7f0f00a1

.field public static final confirm_oma_field_vendor:I = 0x7f0f009e

.field public static final confirm_oma_field_version:I = 0x7f0f00a0

.field public static final container_icon:I = 0x7f0f00c0

.field public static final container_label:I = 0x7f0f00c1

.field public static final content_item:I = 0x7f0f002e

.field public static final contents_container:I = 0x7f0f0115

.field public static final countText:I = 0x7f0f00d5

.field public static final create_folder_button:I = 0x7f0f004b

.field public static final create_folder_container:I = 0x7f0f004a

.field public static final create_folder_image:I = 0x7f0f004c

.field public static final create_folder_text:I = 0x7f0f004d

.field public static final custom_dialog:I = 0x7f0f00a4

.field public static final custom_dialog_all:I = 0x7f0f00a6

.field public static final custom_done:I = 0x7f0f0012

.field public static final custom_layout:I = 0x7f0f0011

.field public static final data_size:I = 0x7f0f00ad

.field public static final date_delete:I = 0x7f0f00ee

.field public static final date_edittext:I = 0x7f0f00ec

.field public static final date_main:I = 0x7f0f00eb

.field public static final date_summary:I = 0x7f0f00ed

.field public static final descriptionLayout:I = 0x7f0f00d1

.field public static final deselect_all_item:I = 0x7f0f0101

.field public static final detail_container:I = 0x7f0f0067

.field public static final device_select_button:I = 0x7f0f000c

.field public static final direct_drag_background:I = 0x7f0f002f

.field public static final divider:I = 0x7f0f006a

.field public static final do_not_show_container:I = 0x7f0f00c6

.field public static final do_not_show_textview:I = 0x7f0f00a8

.field public static final doc_ext_overlay:I = 0x7f0f0036

.field public static final done_add_ftp:I = 0x7f0f011f

.field public static final done_button_container:I = 0x7f0f000f

.field public static final download_app_hover_popup_layout:I = 0x7f0f00a9

.field public static final download_container:I = 0x7f0f0075

.field public static final download_header:I = 0x7f0f0074

.field public static final download_header_container:I = 0x7f0f0073

.field public static final download_image:I = 0x7f0f0076

.field public static final download_view:I = 0x7f0f0077

.field public static final downloaded_flag:I = 0x7f0f003b

.field public static final drag_file_preview:I = 0x7f0f005b

.field public static final edit_text:I = 0x7f0f00a3

.field public static final empty:I = 0x7f0f0057

.field public static final empty_image:I = 0x7f0f00b3

.field public static final empty_text:I = 0x7f0f00b4

.field public static final expand_indicator:I = 0x7f0f0116

.field public static final extension_main:I = 0x7f0f00f0

.field public static final fileName:I = 0x7f0f00d3

.field public static final fileicon:I = 0x7f0f00b6

.field public static final filenametext:I = 0x7f0f00b7

.field public static final filetype_main:I = 0x7f0f00e7

.field public static final filetype_summary:I = 0x7f0f00e8

.field public static final folder_hover_popup:I = 0x7f0f00b2

.field public static final folder_hover_popup_layout:I = 0x7f0f00b1

.field public static final grid_item_layout:I = 0x7f0f006d

.field public static final grid_item_layout_base:I = 0x7f0f006c

.field public static final gridview:I = 0x7f0f0056

.field public static final headerText:I = 0x7f0f0097

.field public static final home:I = 0x7f0f0149

.field public static final horizontalScrollView:I = 0x7f0f0088

.field public static final hover_content_item:I = 0x7f0f00b5

.field public static final hover_item_icon_overlay:I = 0x7f0f00b9

.field public static final hover_item_thumbnail:I = 0x7f0f00b8

.field public static final hover_operations_include_layout:I = 0x7f0f00bf

.field public static final hover_operations_popup_linear_layout:I = 0x7f0f00ba

.field public static final hover_popup_delete_btn:I = 0x7f0f00bc

.field public static final hover_popup_share_via_btn:I = 0x7f0f00bb

.field public static final icon:I = 0x7f0f0031

.field public static final icon_container:I = 0x7f0f0030

.field public static final icon_overlay:I = 0x7f0f0035

.field public static final icon_personal_contents:I = 0x7f0f0039

.field public static final image_hover_popup:I = 0x7f0f00be

.field public static final image_hover_popup_layout:I = 0x7f0f00bd

.field public static final in_order_menu:I = 0x7f0f0108

.field public static final information_container:I = 0x7f0f010c

.field public static final items_count:I = 0x7f0f005f

.field public static final items_count_layout:I = 0x7f0f005e

.field public static final keyboard:I = 0x7f0f009a

.field public static final knox_gridview:I = 0x7f0f00c2

.field public static final layout:I = 0x7f0f003c

.field public static final left_button:I = 0x7f0f0008

.field public static final left_page_arrow:I = 0x7f0f008e

.field public static final linear1:I = 0x7f0f00e2

.field public static final linear2:I = 0x7f0f00e6

.field public static final linear3:I = 0x7f0f00ea

.field public static final list_container:I = 0x7f0f00c3

.field public static final list_detail_description:I = 0x7f0f0065

.field public static final list_item_description:I = 0x7f0f0066

.field public static final list_view_progress_bar:I = 0x7f0f0058

.field public static final listback:I = 0x7f0f00f2

.field public static final listitem_progressbar:I = 0x7f0f006b

.field public static final listview:I = 0x7f0f0055

.field public static final local_storage_header:I = 0x7f0f007d

.field public static final location_main:I = 0x7f0f00e3

.field public static final location_summary:I = 0x7f0f00e4

.field public static final lock:I = 0x7f0f0034

.field public static final main_category_cell_text_container:I = 0x7f0f006f

.field public static final main_category_cloudlist:I = 0x7f0f0085

.field public static final main_category_gridview:I = 0x7f0f0072

.field public static final main_category_gridview_container:I = 0x7f0f0071

.field public static final main_category_panel:I = 0x7f0f0089

.field public static final main_category_remote_share:I = 0x7f0f0082

.field public static final main_category_remotesharelist_container:I = 0x7f0f0081

.field public static final main_category_shortcutlist:I = 0x7f0f007b

.field public static final main_category_shortcutlist_container:I = 0x7f0f007a

.field public static final main_category_storagelist:I = 0x7f0f007e

.field public static final main_list_item_container:I = 0x7f0f00c4

.field public static final menu_add_ftp:I = 0x7f0f0140

.field public static final menu_add_shortcut:I = 0x7f0f0129

.field public static final menu_add_shortcut_home:I = 0x7f0f012a

.field public static final menu_advance_cancel:I = 0x7f0f0133

.field public static final menu_advance_search:I = 0x7f0f0134

.field public static final menu_advanced_search:I = 0x7f0f014c

.field public static final menu_clear_history:I = 0x7f0f0145

.field public static final menu_copy:I = 0x7f0f0123

.field public static final menu_create_folder:I = 0x7f0f013a

.field public static final menu_delete:I = 0x7f0f0121

.field public static final menu_delete_shortcut:I = 0x7f0f013f

.field public static final menu_detail:I = 0x7f0f0130

.field public static final menu_download:I = 0x7f0f0131

.field public static final menu_download_all:I = 0x7f0f014a

.field public static final menu_download_settings:I = 0x7f0f0146

.field public static final menu_edit_ftp:I = 0x7f0f0132

.field public static final menu_extract:I = 0x7f0f012c

.field public static final menu_extract_here:I = 0x7f0f012d

.field public static final menu_id_home:I = 0x7f0f0136

.field public static final menu_move:I = 0x7f0f0122

.field public static final menu_move_to_KNOX:I = 0x7f0f0126

.field public static final menu_move_to_private:I = 0x7f0f0124

.field public static final menu_normal_delete:I = 0x7f0f0138

.field public static final menu_remove_from_KNOX:I = 0x7f0f0127

.field public static final menu_remove_from_private:I = 0x7f0f0125

.field public static final menu_rename:I = 0x7f0f0128

.field public static final menu_scan_for_nearby_devices:I = 0x7f0f0141

.field public static final menu_search:I = 0x7f0f0135

.field public static final menu_select_item:I = 0x7f0f0137

.field public static final menu_settings:I = 0x7f0f013d

.field public static final menu_share:I = 0x7f0f0120

.field public static final menu_shortcut_delete_cancel:I = 0x7f0f0143

.field public static final menu_shortcut_delete_done:I = 0x7f0f0144

.field public static final menu_sign_in:I = 0x7f0f013e

.field public static final menu_sort_by:I = 0x7f0f013c

.field public static final menu_storage_usage:I = 0x7f0f0142

.field public static final menu_view_by:I = 0x7f0f013b

.field public static final menu_vzcloud:I = 0x7f0f0139

.field public static final menu_zip:I = 0x7f0f012b

.field public static final moved_file_name:I = 0x7f0f005d

.field public static final moved_file_size:I = 0x7f0f005c

.field public static final music_hover_popup:I = 0x7f0f00cd

.field public static final music_hover_popup_layout:I = 0x7f0f00cc

.field public static final navigation_bar:I = 0x7f0f0000

.field public static final navigation_bar_up:I = 0x7f0f0005

.field public static final need_wifi_warning_textview:I = 0x7f0f00ce

.field public static final network_icon:I = 0x7f0f003a

.field public static final next_button:I = 0x7f0f009c

.field public static final no_albumart:I = 0x7f0f00ca

.field public static final no_albumart_text:I = 0x7f0f00cb

.field public static final notification:I = 0x7f0f00a5

.field public static final open_file_contents:I = 0x7f0f00cf

.field public static final overwrite_dialog_checkbox_container:I = 0x7f0f00d7

.field public static final overwriteall_checkbox:I = 0x7f0f00d8

.field public static final overwriteall_text:I = 0x7f0f00d9

.field public static final page_indicator:I = 0x7f0f0110

.field public static final pager:I = 0x7f0f0102

.field public static final password_entry:I = 0x7f0f0099

.field public static final path_indicator:I = 0x7f0f0044

.field public static final path_indicator_arrow:I = 0x7f0f00dd

.field public static final path_indicator_background:I = 0x7f0f00da

.field public static final path_indicator_button:I = 0x7f0f00dc

.field public static final path_indicator_container:I = 0x7f0f00db

.field public static final percentText:I = 0x7f0f00d6

.field public static final position_indicator:I = 0x7f0f0117

.field public static final prefixFile:I = 0x7f0f00d2

.field public static final processing:I = 0x7f0f00d0

.field public static final progressBar:I = 0x7f0f00d4

.field public static final remote_share_header:I = 0x7f0f0080

.field public static final remote_share_header_container:I = 0x7f0f007f

.field public static final right_button:I = 0x7f0f0009

.field public static final right_page_arrow:I = 0x7f0f008f

.field public static final scrollView:I = 0x7f0f0070

.field public static final search_dailog_popup_btn_layout:I = 0x7f0f00de

.field public static final search_dropdown:I = 0x7f0f0013

.field public static final search_edit_text:I = 0x7f0f00f8

.field public static final search_edit_text1:I = 0x7f0f0014

.field public static final search_file_type_checkbox:I = 0x7f0f00f4

.field public static final search_file_type_title:I = 0x7f0f00f3

.field public static final search_for_button1:I = 0x7f0f0050

.field public static final search_for_button2:I = 0x7f0f0051

.field public static final search_for_button3:I = 0x7f0f0052

.field public static final search_for_button4:I = 0x7f0f0053

.field public static final search_for_button5:I = 0x7f0f0054

.field public static final search_for_container:I = 0x7f0f004e

.field public static final search_for_text:I = 0x7f0f004f

.field public static final search_fragment_frame_layout:I = 0x7f0f00f9

.field public static final search_from_date_button:I = 0x7f0f00fd

.field public static final search_header_layout:I = 0x7f0f00f6

.field public static final search_item_advance:I = 0x7f0f0152

.field public static final search_item_all:I = 0x7f0f014d

.field public static final search_item_date:I = 0x7f0f0150

.field public static final search_item_extention:I = 0x7f0f0151

.field public static final search_item_file_type:I = 0x7f0f014f

.field public static final search_item_location:I = 0x7f0f014e

.field public static final search_location_checkbox:I = 0x7f0f00fc

.field public static final search_location_container:I = 0x7f0f00fa

.field public static final search_location_title:I = 0x7f0f00fb

.field public static final search_main_layout:I = 0x7f0f00f5

.field public static final search_to_date_button:I = 0x7f0f00fe

.field public static final select_all:I = 0x7f0f0153

.field public static final select_all_checkbox:I = 0x7f0f0048

.field public static final select_all_container:I = 0x7f0f0047

.field public static final select_all_custom_layout:I = 0x7f0f0100

.field public static final select_all_drop_down_button:I = 0x7f0f00f7

.field public static final select_all_item:I = 0x7f0f00ff

.field public static final select_all_menu:I = 0x7f0f0003

.field public static final select_all_text:I = 0x7f0f0049

.field public static final select_checkbox:I = 0x7f0f0037

.field public static final select_container:I = 0x7f0f0060

.field public static final select_radiobutton:I = 0x7f0f0038

.field public static final selectallcontainer:I = 0x7f0f0001

.field public static final selector_menu_cancel:I = 0x7f0f0147

.field public static final selector_menu_ok:I = 0x7f0f0148

.field public static final shortcut_1:I = 0x7f0f008a

.field public static final shortcut_2:I = 0x7f0f008b

.field public static final shortcut_3:I = 0x7f0f008c

.field public static final shortcut_4:I = 0x7f0f008d

.field public static final shortcut_5:I = 0x7f0f0090

.field public static final shortcut_6:I = 0x7f0f0091

.field public static final shortcut_add_icon:I = 0x7f0f0096

.field public static final shortcut_header_container:I = 0x7f0f0078

.field public static final shortcut_icon:I = 0x7f0f0093

.field public static final shortcut_icon_loading:I = 0x7f0f0061

.field public static final shortcut_item_add_container:I = 0x7f0f0095

.field public static final shortcut_item_container:I = 0x7f0f0092

.field public static final shortcut_name:I = 0x7f0f0094

.field public static final shortcut_view_pager:I = 0x7f0f0087

.field public static final shortcut_view_pager_container:I = 0x7f0f0086

.field public static final shortcuts_header:I = 0x7f0f0079

.field public static final show_checkBox:I = 0x7f0f00c7

.field public static final show_checkBox1:I = 0x7f0f00c9

.field public static final show_checkbox:I = 0x7f0f00a7

.field public static final size:I = 0x7f0f006e

.field public static final sort_by_ascending:I = 0x7f0f0109

.field public static final sort_by_descending:I = 0x7f0f010a

.field public static final sort_by_menu:I = 0x7f0f0103

.field public static final sort_by_name:I = 0x7f0f0106

.field public static final sort_by_size:I = 0x7f0f0107

.field public static final sort_by_time:I = 0x7f0f0104

.field public static final sort_by_type:I = 0x7f0f0105

.field public static final space_info:I = 0x7f0f010f

.field public static final space_information_container:I = 0x7f0f010d

.field public static final spacerBottom:I = 0x7f0f0098

.field public static final split_bar:I = 0x7f0f0040

.field public static final split_bar_hover:I = 0x7f0f0041

.field public static final split_view_container:I = 0x7f0f003d

.field public static final split_view_left_panel:I = 0x7f0f003e

.field public static final split_view_right_panel:I = 0x7f0f0042

.field public static final storage_header_container:I = 0x7f0f007c

.field public static final storage_type:I = 0x7f0f010e

.field public static final storage_usage_bar:I = 0x7f0f0111

.field public static final storage_usage_category:I = 0x7f0f0112

.field public static final storage_usage_underline:I = 0x7f0f0114

.field public static final storage_usage_view:I = 0x7f0f010b

.field public static final text1:I = 0x7f0f0032

.field public static final text2:I = 0x7f0f005a

.field public static final text3:I = 0x7f0f0069

.field public static final text4:I = 0x7f0f0068

.field public static final text_container:I = 0x7f0f0064

.field public static final thumbnail:I = 0x7f0f0033

.field public static final title:I = 0x7f0f0004

.field public static final titleTv:I = 0x7f0f0063

.field public static final title_bar:I = 0x7f0f0043

.field public static final title_bar_text:I = 0x7f0f0046

.field public static final title_content_text_container:I = 0x7f0f0062

.field public static final total_size:I = 0x7f0f00ab

.field public static final treeview:I = 0x7f0f003f

.field public static final unchecked:I = 0x7f0f011d

.field public static final unselect_all:I = 0x7f0f0154

.field public static final warning_textview:I = 0x7f0f00c5

.field public static final warning_textview1:I = 0x7f0f00c8

.field public static final wifi_only_checkbox:I = 0x7f0f011b


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1518
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
