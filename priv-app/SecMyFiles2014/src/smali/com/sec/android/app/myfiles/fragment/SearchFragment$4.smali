.class Lcom/sec/android/app/myfiles/fragment/SearchFragment$4;
.super Ljava/lang/Object;
.source "SearchFragment.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/SearchFragment;->createScrollListener()Landroid/widget/AbsListView$OnScrollListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)V
    .locals 0

    .prologue
    .line 344
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0
    .param p1, "arg0"    # Landroid/widget/AbsListView;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 374
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2
    .param p1, "scrollState"    # Landroid/widget/AbsListView;
    .param p2, "state"    # I

    .prologue
    .line 348
    packed-switch p2, :pswitch_data_0

    .line 370
    :cond_0
    :goto_0
    return-void

    .line 350
    :pswitch_0
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;->SCROLL_DONE:Z

    goto :goto_0

    .line 355
    :pswitch_1
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;->SCROLL_DONE:Z

    .line 357
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 359
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v1, :cond_0

    .line 360
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 361
    .local v0, "currentFocus":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 362
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    goto :goto_0

    .line 348
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
