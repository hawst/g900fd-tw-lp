.class public Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;
.source "NearByDevicesBrowserFragment.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;


# static fields
.field private static final MODULE:Ljava/lang/String; = "NearByDevicesBrowserFragment"


# instance fields
.field private bBack:Z

.field private mCurTitle:Ljava/lang/String;

.field private mDevicesCollectionUpdatedListener:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;

.field private mDevicesListShowing:Z

.field private mFromLeftPanel:Z

.field private mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

.field private mPathList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mProvider:Lcom/samsung/android/allshare/media/Provider;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;-><init>()V

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mCurTitle:Ljava/lang/String;

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathList:Ljava/util/ArrayList;

    .line 76
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->bBack:Z

    .line 78
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mFromLeftPanel:Z

    .line 80
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mDevicesListShowing:Z

    .line 786
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$5;-><init>(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->bBack:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mDevicesListShowing:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mCurTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mCurTitle:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathList:Ljava/util/ArrayList;

    return-object v0
.end method

.method private downloadItems()Z
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v6, 0x0

    .line 842
    const/4 v3, 0x0

    .line 844
    .local v3, "retValue":Z
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->isSelectMode()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 846
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v5, :cond_3

    .line 848
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v5, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v2

    .line 849
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 851
    .local v4, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 853
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/myfiles/element/BrowserItem;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getSelectedRemoteItem()Lcom/samsung/android/allshare/Item;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 851
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 856
    :cond_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_2

    .line 858
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    if-nez v5, :cond_1

    .line 859
    const-string v5, "downloadItems()"

    const-string v7, "mProvider == null."

    invoke-static {v6, v5, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    move v5, v6

    .line 890
    .end local v1    # "i":I
    .end local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v4    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    :goto_1
    return v5

    .line 863
    .restart local v1    # "i":I
    .restart local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .restart local v4    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/media/Provider;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/myfiles/nearbydevices/DownloadMenuOperation;->downloadItems(Ljava/util/ArrayList;Ljava/lang/String;)Z

    move-result v3

    .line 866
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 868
    .local v0, "count":I
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0c0003

    new-array v9, v11, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v6

    invoke-virtual {v7, v8, v0, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 873
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->finishSelectMode()V

    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v4    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    :goto_2
    move v5, v3

    .line 890
    goto :goto_1

    .line 877
    .restart local v1    # "i":I
    .restart local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .restart local v4    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v7, 0x7f0b0084

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_2

    .line 883
    .end local v1    # "i":I
    .end local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v4    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    :cond_3
    const-string v5, "downloadItems()"

    const-string v7, "mAdapter == null."

    invoke-static {v6, v5, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 887
    :cond_4
    const-string v5, "downloadItems()"

    const-string v7, "isSelectMode() is false."

    invoke-static {v6, v5, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private isOnlyFolders()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 464
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mDevicesListShowing:Z

    if-eqz v3, :cond_1

    .line 487
    :cond_0
    :goto_0
    return v2

    .line 467
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v0

    .line 468
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 469
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 475
    :cond_2
    const-string v3, "MediaType"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 476
    .local v1, "mediaType":I
    if-eq v1, v2, :cond_3

    .line 478
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 479
    const/4 v2, 0x0

    goto :goto_0

    .line 482
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 484
    .end local v1    # "mediaType":I
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private onCreateDevicesCollectionUpdatedListener()Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;
    .locals 1

    .prologue
    .line 137
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;)V

    return-object v0
.end method

.method private setListViewScrollListener(Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 2
    .param p1, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    .line 780
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 781
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getOnScrollListener()Landroid/widget/AbsListView$OnScrollListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 782
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getOnScrollListener()Landroid/widget/AbsListView$OnScrollListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 784
    :cond_0
    return-void
.end method


# virtual methods
.method public AsyncOpenCompleted()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 372
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v1, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getProvider()Lcom/samsung/android/allshare/media/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    .line 373
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mFromLeftPanel:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    if-nez v1, :cond_3

    .line 374
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mDevicesListShowing:Z

    .line 375
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 380
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v0

    .line 381
    .local v0, "c":Landroid/database/Cursor;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 383
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->bBack:Z

    if-eqz v1, :cond_4

    .line 385
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/view/PathIndicator;->goUp()V

    .line 387
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 389
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 392
    :cond_1
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->bBack:Z

    .line 400
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->invalidateOptionsMenu()V

    .line 401
    return-void

    .line 377
    .end local v0    # "c":Landroid/database/Cursor;
    :cond_3
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mDevicesListShowing:Z

    goto :goto_0

    .line 395
    .restart local v0    # "c":Landroid/database/Cursor;
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    if-eqz v1, :cond_2

    .line 396
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public changeProvider(Ljava/lang/String;)V
    .locals 3
    .param p1, "providerId"    # Ljava/lang/String;

    .prologue
    .line 170
    if-eqz p1, :cond_0

    .line 172
    invoke-static {}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->getInstance()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->resolveProvider(Ljava/lang/String;)Lcom/samsung/android/allshare/media/Provider;

    move-result-object v0

    .line 173
    .local v0, "provider":Lcom/samsung/android/allshare/media/Provider;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v1, :cond_0

    .line 175
    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    .line 176
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v1, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->setProvider(Lcom/samsung/android/allshare/media/Provider;)V

    .line 177
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 178
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/media/Provider;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 179
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 182
    .end local v0    # "provider":Lcom/samsung/android/allshare/media/Provider;
    :cond_0
    return-void
.end method

.method public deviceAdded(Lcom/samsung/android/allshare/Device;)V
    .locals 0
    .param p1, "device"    # Lcom/samsung/android/allshare/Device;

    .prologue
    .line 271
    return-void
.end method

.method public deviceRemoved(Lcom/samsung/android/allshare/Device;)V
    .locals 6
    .param p1, "device"    # Lcom/samsung/android/allshare/Device;

    .prologue
    .line 248
    instance-of v3, p1, Lcom/samsung/android/allshare/media/Provider;

    if-eqz v3, :cond_2

    move-object v2, p1

    .line 250
    check-cast v2, Lcom/samsung/android/allshare/media/Provider;

    .line 251
    .local v2, "provider":Lcom/samsung/android/allshare/media/Provider;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/media/Provider;->getID()Ljava/lang/String;

    move-result-object v0

    .line 253
    .local v0, "id":Ljava/lang/String;
    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/media/Provider;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 255
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/media/Provider;->getName()Ljava/lang/String;

    move-result-object v1

    .line 256
    .local v1, "name":Ljava/lang/String;
    if-nez v1, :cond_0

    const-string v1, ""

    .line 257
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 259
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f0b0083

    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 262
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    .line 265
    .end local v0    # "id":Ljava/lang/String;
    .end local v1    # "name":Ljava/lang/String;
    .end local v2    # "provider":Lcom/samsung/android/allshare/media/Provider;
    :cond_2
    return-void
.end method

.method public getOptionsMenuResId(Z)I
    .locals 7
    .param p1, "isSelectMode"    # Z

    .prologue
    const v3, 0x7f0e0012

    const/4 v4, -0x1

    const/4 v6, 0x0

    .line 406
    const-string v2, "NearByDevicesBrowserFragment"

    const-string v5, "NearByDevicesFragment - getOptionsMenuResId"

    invoke-static {v6, v2, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 408
    const/4 v1, 0x0

    .line 410
    .local v1, "type":Lcom/samsung/android/allshare/Device$DeviceType;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v2, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getProvider()Lcom/samsung/android/allshare/media/Provider;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 411
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v2, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getProvider()Lcom/samsung/android/allshare/media/Provider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/allshare/media/Provider;->getDeviceType()Lcom/samsung/android/allshare/Device$DeviceType;

    move-result-object v1

    .line 442
    :goto_0
    sget-object v2, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    if-ne v1, v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->isCurrentFolderRoot()Z

    move-result v2

    if-nez v2, :cond_6

    .line 444
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->isSelectMode()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 446
    const v2, 0x7f0e0013

    .line 459
    :goto_1
    return v2

    .line 412
    :cond_1
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mDevicesListShowing:Z

    if-eqz v2, :cond_2

    move v2, v3

    .line 414
    goto :goto_1

    .line 418
    :cond_2
    const-string v2, "NearByDevicesBrowserFragment"

    const-string v5, "NearByDevicesFragment - getOptionsMenuResId : getProvider() == null"

    invoke-static {v6, v2, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 420
    const/4 v0, 0x0

    .line 422
    .local v0, "deviceKey":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->getInstance()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 424
    invoke-static {}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->getInstance()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->getSelectedDeviceKey()Ljava/lang/String;

    move-result-object v0

    .line 425
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v2, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->setProvider(Ljava/lang/String;)V

    .line 428
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v2, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getProvider()Lcom/samsung/android/allshare/media/Provider;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 429
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v2, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getProvider()Lcom/samsung/android/allshare/media/Provider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/allshare/media/Provider;->getDeviceType()Lcom/samsung/android/allshare/Device$DeviceType;

    move-result-object v1

    goto :goto_0

    .line 432
    :cond_4
    const-string v2, "NearByDevicesBrowserFragment"

    const-string v3, "NearByDevicesFragment - getOptionsMenuResId : disconnected~!!!"

    invoke-static {v6, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 433
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v3, 0x7f0b0083

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x1

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 434
    invoke-static {}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->getInstance()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->unRegisterDevice()V

    .line 435
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    const-string v5, "type=9"

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 436
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    move v2, v4

    .line 438
    goto :goto_1

    .end local v0    # "deviceKey":Ljava/lang/String;
    :cond_5
    move v2, v3

    .line 450
    goto :goto_1

    .line 454
    :cond_6
    sget-object v2, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    if-ne v1, v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->isCurrentFolderRoot()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 456
    const v2, 0x7f0e0011

    goto/16 :goto_1

    :cond_7
    move v2, v4

    .line 459
    goto/16 :goto_1
.end method

.method protected getPath()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x2f

    .line 826
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 828
    .local v1, "path":Ljava/lang/StringBuilder;
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mFromLeftPanel:Z

    if-eqz v2, :cond_0

    .line 829
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 830
    const-string v2, "Nearby devices"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 832
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 833
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 834
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 832
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 837
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 576
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 578
    return-void
.end method

.method public onBackPressed()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 342
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 344
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->finishSelectMode()V

    .line 366
    :cond_0
    :goto_0
    return v2

    .line 348
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goUp()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 350
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->cancelThumbnailsLoading(Z)V

    .line 351
    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->bBack:Z

    .line 352
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mDevicesListShowing:Z

    if-nez v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 359
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 360
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 86
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreate(Landroid/os/Bundle;)V

    .line 89
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .line 91
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3, p0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setSupportAsync(Lcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    .line 94
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 96
    .local v0, "argument":Landroid/os/Bundle;
    const/4 v2, 0x0

    .line 98
    .local v2, "startFolder":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 100
    const-string v3, "FOLDERPATH"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 103
    :cond_0
    if-eqz v2, :cond_1

    const-string v3, "devices_list"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 105
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mDevicesListShowing:Z

    .line 107
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mFromLeftPanel:Z

    .line 109
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v3, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->removeProvider()V

    .line 111
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v3, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->setFromLeftPanel(Z)V

    .line 113
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->onCreateDevicesCollectionUpdatedListener()Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mDevicesCollectionUpdatedListener:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;

    .line 133
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goToRoot(Ljava/lang/Boolean;)Z

    .line 134
    return-void

    .line 117
    :cond_1
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mDevicesListShowing:Z

    .line 119
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mFromLeftPanel:Z

    .line 121
    invoke-static {}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->getInstance()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->resolveProvider(Ljava/lang/String;)Lcom/samsung/android/allshare/media/Provider;

    move-result-object v1

    .line 123
    .local v1, "device":Lcom/samsung/android/allshare/Device;
    check-cast v1, Lcom/samsung/android/allshare/media/Provider;

    .end local v1    # "device":Lcom/samsung/android/allshare/Device;
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    .line 125
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    if-eqz v3, :cond_2

    .line 127
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v3, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->setProvider(Lcom/samsung/android/allshare/media/Provider;)V

    .line 130
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v3, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    invoke-virtual {v3, v5}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->setFromLeftPanel(Z)V

    goto :goto_0
.end method

.method protected onCreateDetailFragmentListener()Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 297
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/NearbyDeviceDetailFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/NearbyDeviceDetailFragment;-><init>()V

    return-object v0
.end method

.method protected onCreateFileOperationListener()Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;
    .locals 1

    .prologue
    .line 276
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;)V

    return-object v0
.end method

.method protected onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 584
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;)V

    return-object v0
.end method

.method protected onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1

    .prologue
    .line 740
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$4;-><init>(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;)V

    return-object v0
.end method

.method protected onCreateMultipleDetailFragmentListener()Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 303
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/MultipleNearByDevicesDetailFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/MultipleNearByDevicesDetailFragment;-><init>()V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 187
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 189
    .local v0, "view":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setOnPathChangeListener(Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;)V

    .line 191
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 192
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    if-eqz v1, :cond_3

    .line 193
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/media/Provider;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 198
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    if-eqz v1, :cond_0

    .line 199
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 203
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 205
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->setListViewScrollListener(Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 207
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    if-eqz v1, :cond_2

    .line 208
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    if-eqz v1, :cond_2

    .line 209
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v1, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->setEnclosingViews(Ljava/lang/String;Landroid/widget/AdapterView;)V

    .line 210
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v1, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->setEnclosingViews(Ljava/lang/String;Landroid/widget/AdapterView;)V

    .line 211
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v1, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->setEnclosingViews(Ljava/lang/String;Landroid/widget/AdapterView;)V

    .line 217
    :cond_2
    return-object v0

    .line 195
    :cond_3
    const-string v1, "onCreateView"

    const-string v2, "mProvider cannot be null."

    invoke-static {v3, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 895
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onDestroy()V

    .line 896
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 897
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    if-eqz v0, :cond_0

    .line 898
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->clearCache()V

    .line 899
    :cond_0
    return-void
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 232
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onDestroyView()V

    .line 236
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 532
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 570
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :cond_0
    :goto_0
    return v1

    .line 535
    :sswitch_0
    const/4 v3, -0x1

    invoke-virtual {p0, v1, v3, v2}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->startSelectMode(III)V

    goto :goto_0

    .line 546
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->downloadItems()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 547
    const/4 v2, 0x2

    const-string v3, "NearByDevicesBrowserFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onOptionsItemSelected =  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/media/Provider;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 555
    :sswitch_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setmAdapter(Landroid/widget/BaseAdapter;)V

    .line 556
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->showDetail(Ljava/lang/String;)V

    goto :goto_0

    .line 560
    :sswitch_3
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isKNOXMode(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 561
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v3, "knox_feature_disabled_toast"

    const-string v4, "string"

    const-string v5, "android"

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 562
    .local v0, "stringResId":I
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    move v1, v2

    .line 563
    goto :goto_0

    .line 565
    .end local v0    # "stringResId":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v3, "NEAR"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/Utils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mDevicesCollectionUpdatedListener:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;

    invoke-static {v2, v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->scanForNearbyDevices(Landroid/app/Activity;Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;)V

    goto :goto_0

    .line 532
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0f0130 -> :sswitch_2
        0x7f0f0131 -> :sswitch_1
        0x7f0f0137 -> :sswitch_0
        0x7f0f0141 -> :sswitch_3
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 492
    if-nez p1, :cond_1

    .line 527
    :cond_0
    :goto_0
    return-void

    .line 494
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 496
    const v2, 0x7f0f0137

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 498
    .local v1, "menuItem":Landroid/view/MenuItem;
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mDevicesListShowing:Z

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->isSelectMode()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->isOnlyFolders()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 500
    :cond_2
    if-eqz v1, :cond_3

    .line 502
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 519
    :cond_3
    :goto_1
    const v2, 0x7f0f0141

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 520
    if-eqz v1, :cond_0

    .line 521
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mFromLeftPanel:Z

    if-eqz v2, :cond_6

    .line 522
    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 505
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->isSelectMode()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 506
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v2

    if-lt v2, v3, :cond_3

    .line 507
    const v2, 0x7f0f0130

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 508
    .local v0, "detailMenuItem":Landroid/view/MenuItem;
    if-eqz v0, :cond_3

    .line 509
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 514
    .end local v0    # "detailMenuItem":Landroid/view/MenuItem;
    :cond_5
    if-eqz v1, :cond_3

    .line 515
    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 524
    :cond_6
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onRefresh()V
    .locals 3

    .prologue
    .line 333
    const/4 v0, 0x0

    const-string v1, "NearByDevicesBrowserFragment"

    const-string v2, "refresh"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 335
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->refreshNavigation()V

    .line 336
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 337
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 240
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onResume()V

    .line 241
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->showPathIndicator(Z)V

    .line 242
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    const-string v1, "9"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setCategoryItemListSelection(Ljava/lang/String;)I

    .line 243
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 222
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 227
    return-void
.end method

.method protected resetPathIndicator(Z)V
    .locals 2
    .param p1, "refreshPathIndicator"    # Z

    .prologue
    .line 812
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 814
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 815
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/media/Provider;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 816
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 823
    :cond_0
    :goto_0
    return-void

    .line 818
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    if-eqz v0, :cond_0

    .line 820
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/media/Provider;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 821
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected selectAllItem()V
    .locals 1

    .prologue
    .line 309
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->selectAllItem()V

    .line 311
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->selectAllItem()V

    .line 316
    :cond_0
    return-void
.end method

.method protected unselectAllItem()V
    .locals 1

    .prologue
    .line 321
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->unselectAllItem()V

    .line 323
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 328
    :cond_0
    return-void
.end method
