.class Lcom/sec/android/app/myfiles/SelectorActivity$7;
.super Ljava/lang/Object;
.source "SelectorActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/SelectorActivity;->onCreateWarningDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

.field final synthetic val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/SelectorActivity;Lcom/sec/android/app/myfiles/utils/SharedDataStore;)V
    .locals 0

    .prologue
    .line 1818
    iput-object p1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$7;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iput-object p2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$7;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 1823
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 1825
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$7;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$7;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mIsCheckedShowDialogWLANOn:Z
    invoke-static {v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$1800(Lcom/sec/android/app/myfiles/SelectorActivity;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setDoNotShowCheckWLANOn(Z)V

    .line 1826
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$7;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->commitShowDataWarning()Z

    .line 1828
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$7;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mTabHost:Landroid/widget/TabHost;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$1700(Lcom/sec/android/app/myfiles/SelectorActivity;)Landroid/widget/TabHost;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$7;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mForWarningPosition:I
    invoke-static {v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$600(Lcom/sec/android/app/myfiles/SelectorActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->setCurrentTab(I)V

    .line 1830
    return-void
.end method
