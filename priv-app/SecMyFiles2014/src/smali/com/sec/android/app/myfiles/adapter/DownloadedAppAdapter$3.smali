.class Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$3;
.super Ljava/lang/Object;
.source "DownloadedAppAdapter.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

.field final synthetic val$item:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;)V
    .locals 0

    .prologue
    .line 431
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    iput-object p2, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$3;->val$item:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 436
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->isSelectMode()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, p2}, Lcom/sec/android/app/myfiles/utils/Utils;->isHoverSettingDisabled(Landroid/content/Context;Landroid/view/MotionEvent;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mDownloadAppHoverPopup:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->access$200(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 439
    const/4 v0, 0x0

    .line 441
    .local v0, "bSupportHover":Z
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 442
    .local v2, "hoverRect":Landroid/graphics/Rect;
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    check-cast v3, Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 444
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mDownloadAppHoverPopup:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->access$200(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$3;->val$item:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    invoke-virtual {v3, v2, v4}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(Landroid/graphics/Rect;Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;)V

    .line 445
    const/4 v0, 0x1

    .line 447
    if-eqz v0, :cond_0

    .line 449
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 495
    .end local v0    # "bSupportHover":Z
    .end local v2    # "hoverRect":Landroid/graphics/Rect;
    :cond_0
    :goto_0
    :pswitch_0
    return v5

    .line 454
    .restart local v0    # "bSupportHover":Z
    .restart local v2    # "hoverRect":Landroid/graphics/Rect;
    :pswitch_1
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v3

    if-ne v3, v6, :cond_1

    .line 456
    const/16 v3, 0xa

    const/4 v4, -0x1

    invoke-static {v3, v4}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 464
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mDownloadAppHoverPopup:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->access$200(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v3

    const/16 v4, 0x1f4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->sendDelayedMessage(I)V

    goto :goto_0

    .line 459
    :catch_0
    move-exception v1

    .line 461
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 470
    .end local v1    # "e":Landroid/os/RemoteException;
    :pswitch_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mDownloadAppHoverPopup:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->access$200(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->removeDelayedMessage()V

    .line 473
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v3

    if-ne v3, v6, :cond_0

    .line 475
    const/4 v3, 0x1

    const/4 v4, -0x1

    invoke-static {v3, v4}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 478
    :catch_1
    move-exception v1

    .line 480
    .restart local v1    # "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 449
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
