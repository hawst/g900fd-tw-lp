.class Lcom/sec/android/app/myfiles/fragment/HistoryFragment$5;
.super Ljava/lang/Object;
.source "HistoryFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->showDeleteOfRetryDownloadDialog(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

.field final synthetic val$externalId:J


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;J)V
    .locals 0

    .prologue
    .line 710
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    iput-wide p2, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$5;->val$externalId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 715
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    iget-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$5;->val$externalId:J

    # invokes: Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->deleteDownload(J)I
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->access$700(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;J)I

    move-result v0

    if-lez v0, :cond_0

    .line 718
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->AsyncOpen()V

    .line 722
    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 724
    return-void
.end method
