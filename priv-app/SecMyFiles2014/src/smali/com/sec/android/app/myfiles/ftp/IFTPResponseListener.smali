.class public interface abstract Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
.super Ljava/lang/Object;
.source "IFTPResponseListener.java"


# virtual methods
.method public abstract onBrowseOperationFailed(ZZI)V
.end method

.method public abstract onBrowseResponseReceived(Ljava/util/List;Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onCloseConnection(Z)V
.end method

.method public abstract onDiscoveryCompleted(Landroid/os/Bundle;)V
.end method

.method public abstract onDiscoveryCompleted(Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;)V
.end method

.method public abstract onDownloadCompleted(Z[Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/FTPParams;Z)V
.end method

.method public abstract onFileDeleted()V
.end method

.method public abstract onFileRenamed()V
.end method

.method public abstract onFolderCreated()V
.end method

.method public abstract onMoveFilesSelfCompleted()V
.end method

.method public abstract onOpenConnection(Z)V
.end method

.method public abstract onSelectCompleted(ZLjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onUploadCompleted(ZLjava/lang/String;)V
.end method
