.class public Lcom/sec/android/app/myfiles/view/MyFilesListView;
.super Landroid/widget/ListView;
.source "MyFilesListView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lcom/sec/android/app/myfiles/view/IDirectDragable;


# static fields
.field private static final DIRECT_DRAG_FIRE_TIME:I = 0x3e8


# instance fields
.field private mArgument:Landroid/os/Bundle;

.field private mCollapseAnimation:Landroid/view/animation/Animation;

.field private mCollapsedHeight:I

.field protected mContext:Landroid/content/Context;

.field private mDirectDragBackground:I

.field private mDirectDragEnabled:Z

.field private mDirectDragFireTimer:Landroid/os/CountDownTimer;

.field private mDirectDragListener:Lcom/sec/android/app/myfiles/view/IDirectDragable$OnDirectDragListener;

.field private mExpandAnimation:Landroid/view/animation/Animation;

.field private mExpanded:Z

.field private mLastHeight:I

.field private mLongClickTriggered:Z

.field private mMaximumHeight:I

.field public mScrollState:I

.field private mTouchedRowPosition:I

.field private mTouchedRowView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 43
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mTouchedRowPosition:I

    .line 55
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mScrollState:I

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mExpanded:Z

    .line 303
    new-instance v0, Lcom/sec/android/app/myfiles/view/MyFilesListView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/view/MyFilesListView$2;-><init>(Lcom/sec/android/app/myfiles/view/MyFilesListView;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mExpandAnimation:Landroid/view/animation/Animation;

    .line 322
    new-instance v0, Lcom/sec/android/app/myfiles/view/MyFilesListView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/view/MyFilesListView$3;-><init>(Lcom/sec/android/app/myfiles/view/MyFilesListView;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mCollapseAnimation:Landroid/view/animation/Animation;

    .line 77
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mContext:Landroid/content/Context;

    .line 79
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->init()V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mTouchedRowPosition:I

    .line 55
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mScrollState:I

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mExpanded:Z

    .line 303
    new-instance v0, Lcom/sec/android/app/myfiles/view/MyFilesListView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/view/MyFilesListView$2;-><init>(Lcom/sec/android/app/myfiles/view/MyFilesListView;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mExpandAnimation:Landroid/view/animation/Animation;

    .line 322
    new-instance v0, Lcom/sec/android/app/myfiles/view/MyFilesListView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/view/MyFilesListView$3;-><init>(Lcom/sec/android/app/myfiles/view/MyFilesListView;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mCollapseAnimation:Landroid/view/animation/Animation;

    .line 86
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mContext:Landroid/content/Context;

    .line 88
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->init()V

    .line 89
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/view/MyFilesListView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/view/MyFilesListView;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mTouchedRowView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/view/MyFilesListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/view/MyFilesListView;

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mDirectDragBackground:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/view/MyFilesListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/view/MyFilesListView;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->performDirectDrag()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/view/MyFilesListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/view/MyFilesListView;

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mMaximumHeight:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/view/MyFilesListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/view/MyFilesListView;

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mCollapsedHeight:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/view/MyFilesListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/view/MyFilesListView;

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mLastHeight:I

    return v0
.end method

.method private collapse()V
    .locals 4

    .prologue
    .line 279
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mLastHeight:I

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mCollapseAnimation:Landroid/view/animation/Animation;

    iget v1, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mLastHeight:I

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v1, v2

    float-to-int v1, v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mCollapseAnimation:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 285
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mExpanded:Z

    .line 286
    return-void
.end method

.method private endDirectDrag()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 217
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mDirectDragEnabled:Z

    .line 223
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mLongClickTriggered:Z

    .line 224
    return-void
.end method

.method private expand()V
    .locals 4

    .prologue
    .line 291
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mCollapsedHeight:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 293
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setVisibility(I)V

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mExpandAnimation:Landroid/view/animation/Animation;

    iget v1, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mLastHeight:I

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v1, v2

    float-to-int v1, v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 297
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mExpandAnimation:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 299
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mExpanded:Z

    .line 300
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08003e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mDirectDragBackground:I

    .line 96
    invoke-virtual {p0, p0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 98
    invoke-virtual {p0, p0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090141

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mCollapsedHeight:I

    .line 104
    :cond_0
    return-void
.end method

.method private performDirectDrag()V
    .locals 3

    .prologue
    .line 199
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mDirectDragListener:Lcom/sec/android/app/myfiles/view/IDirectDragable$OnDirectDragListener;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mDirectDragListener:Lcom/sec/android/app/myfiles/view/IDirectDragable$OnDirectDragListener;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mTouchedRowView:Landroid/view/View;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mArgument:Landroid/os/Bundle;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/myfiles/view/IDirectDragable$OnDirectDragListener;->onDirectDragStarted(Landroid/view/View;Landroid/os/Bundle;)V

    .line 203
    :cond_0
    return-void
.end method


# virtual methods
.method public awakeScrollBar()V
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->awakenScrollBars(I)Z

    .line 150
    return-void
.end method

.method public isExpanded()Z
    .locals 1

    .prologue
    .line 363
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mExpanded:Z

    return v0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 241
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 247
    iget v1, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mScrollState:I

    if-eq v1, p2, :cond_0

    .line 249
    iput p2, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mScrollState:I

    .line 254
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 274
    :cond_1
    :goto_0
    return-void

    .line 256
    :pswitch_0
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;->SCROLL_DONE:Z

    goto :goto_0

    .line 260
    :pswitch_1
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;->SCROLL_DONE:Z

    .line 262
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    if-eqz v1, :cond_1

    .line 263
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 264
    .local v0, "currentFocus":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 265
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    goto :goto_0

    .line 254
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 110
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mDirectDragEnabled:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mLongClickTriggered:Z

    if-eqz v2, :cond_0

    .line 112
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 114
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 136
    .end local v0    # "action":I
    :cond_0
    :goto_0
    const/4 v2, 0x0

    return v2

    .line 118
    .restart local v0    # "action":I
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->pointToPosition(II)I

    move-result v1

    .line 120
    .local v1, "newRowPosition":I
    iget v2, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mTouchedRowPosition:I

    if-eq v2, v1, :cond_0

    .line 122
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->endDirectDrag()V

    goto :goto_0

    .line 130
    .end local v1    # "newRowPosition":I
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->endDirectDrag()V

    goto :goto_0

    .line 114
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setDirectDragMode(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 143
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mDirectDragEnabled:Z

    .line 144
    return-void
.end method

.method public setHeight(I)V
    .locals 1
    .param p1, "height"    # I

    .prologue
    .line 369
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 370
    return-void
.end method

.method public setMaximumHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 375
    iput p1, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mMaximumHeight:I

    .line 376
    return-void
.end method

.method public setOnDirectDragListener(Lcom/sec/android/app/myfiles/view/IDirectDragable$OnDirectDragListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/myfiles/view/IDirectDragable$OnDirectDragListener;

    .prologue
    .line 209
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mDirectDragListener:Lcom/sec/android/app/myfiles/view/IDirectDragable$OnDirectDragListener;

    .line 210
    return-void
.end method

.method public toggleExpandCollapse()V
    .locals 1

    .prologue
    .line 350
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mExpanded:Z

    if-eqz v0, :cond_0

    .line 352
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->collapse()V

    .line 358
    :goto_0
    return-void

    .line 356
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->expand()V

    goto :goto_0
.end method

.method public triggerLongClick(ILandroid/os/Bundle;)V
    .locals 6
    .param p1, "position"    # I
    .param p2, "argument"    # Landroid/os/Bundle;

    .prologue
    .line 156
    iput-object p2, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mArgument:Landroid/os/Bundle;

    .line 158
    iput p1, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mTouchedRowPosition:I

    .line 160
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getFirstVisiblePosition()I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mTouchedRowView:Landroid/view/View;

    .line 163
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mLongClickTriggered:Z

    .line 167
    new-instance v0, Lcom/sec/android/app/myfiles/view/MyFilesListView$1;

    const-wide/16 v2, 0x3e8

    const-wide/16 v4, 0xa

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/view/MyFilesListView$1;-><init>(Lcom/sec/android/app/myfiles/view/MyFilesListView;JJ)V

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/view/MyFilesListView$1;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mDirectDragFireTimer:Landroid/os/CountDownTimer;

    .line 194
    return-void
.end method
