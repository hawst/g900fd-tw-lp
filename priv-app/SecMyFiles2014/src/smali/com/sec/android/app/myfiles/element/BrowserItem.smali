.class public Lcom/sec/android/app/myfiles/element/BrowserItem;
.super Ljava/lang/Object;
.source "BrowserItem.java"


# instance fields
.field public mContentType:I

.field public mFormat:I

.field public mId:J

.field public mItemType:I

.field public mPath:Ljava/lang/String;

.field public mSearchCategory:I

.field public mSelectedRemoteItem:Lcom/samsung/android/allshare/Item;

.field public mSize:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput v2, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mItemType:I

    .line 34
    iput v2, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mFormat:I

    .line 38
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mSize:J

    .line 40
    iput v2, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mSearchCategory:I

    .line 42
    return-void
.end method

.method public constructor <init>(IJLjava/lang/String;)V
    .locals 4
    .param p1, "contentType"    # I
    .param p2, "id"    # J
    .param p4, "path"    # Ljava/lang/String;

    .prologue
    const/4 v2, -0x1

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput v2, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mItemType:I

    .line 34
    iput v2, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mFormat:I

    .line 38
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mSize:J

    .line 40
    iput v2, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mSearchCategory:I

    .line 47
    iput p1, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mContentType:I

    .line 49
    iput-wide p2, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mId:J

    .line 51
    iput-object p4, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public constructor <init>(IJLjava/lang/String;I)V
    .locals 4
    .param p1, "contentType"    # I
    .param p2, "id"    # J
    .param p4, "path"    # Ljava/lang/String;
    .param p5, "itemType"    # I

    .prologue
    const/4 v2, -0x1

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput v2, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mItemType:I

    .line 34
    iput v2, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mFormat:I

    .line 38
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mSize:J

    .line 40
    iput v2, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mSearchCategory:I

    .line 68
    iput p1, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mContentType:I

    .line 70
    iput-wide p2, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mId:J

    .line 72
    iput-object p4, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    .line 74
    iput p5, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mItemType:I

    .line 76
    return-void
.end method

.method public constructor <init>(IJLjava/lang/String;IJ)V
    .locals 4
    .param p1, "contentType"    # I
    .param p2, "id"    # J
    .param p4, "path"    # Ljava/lang/String;
    .param p5, "itemType"    # I
    .param p6, "itemSize"    # J

    .prologue
    const/4 v2, -0x1

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput v2, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mItemType:I

    .line 34
    iput v2, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mFormat:I

    .line 38
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mSize:J

    .line 40
    iput v2, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mSearchCategory:I

    .line 80
    iput p1, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mContentType:I

    .line 82
    iput-wide p2, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mId:J

    .line 84
    iput-object p4, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    .line 86
    iput p5, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mItemType:I

    .line 88
    iput-wide p6, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mSize:J

    .line 90
    return-void
.end method

.method public constructor <init>(IJLjava/lang/String;Lcom/samsung/android/allshare/Item;)V
    .locals 4
    .param p1, "contentType"    # I
    .param p2, "id"    # J
    .param p4, "path"    # Ljava/lang/String;
    .param p5, "selectedItem"    # Lcom/samsung/android/allshare/Item;

    .prologue
    const/4 v2, -0x1

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput v2, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mItemType:I

    .line 34
    iput v2, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mFormat:I

    .line 38
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mSize:J

    .line 40
    iput v2, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mSearchCategory:I

    .line 56
    iput p1, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mContentType:I

    .line 58
    iput-wide p2, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mId:J

    .line 60
    iput-object p4, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    .line 62
    iput-object p5, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mSelectedRemoteItem:Lcom/samsung/android/allshare/Item;

    .line 64
    return-void
.end method


# virtual methods
.method public getFile()Ljava/io/File;
    .locals 2

    .prologue
    .line 127
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public getFileTitle()Ljava/lang/String;
    .locals 3

    .prologue
    .line 141
    iget-object v1, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    const/16 v2, 0x2f

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    .line 142
    .local v0, "ind":I
    if-ltz v0, :cond_0

    .line 143
    iget-object v1, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 145
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    goto :goto_0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedRemoteItem()Lcom/samsung/android/allshare/Item;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mSelectedRemoteItem:Lcom/samsung/android/allshare/Item;

    return-object v0
.end method

.method public isFile()Ljava/lang/Boolean;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 94
    iget v3, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mFormat:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 96
    iget v3, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mFormat:I

    const/16 v4, 0x3001

    if-ne v3, v4, :cond_0

    .line 98
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 122
    :goto_0
    return-object v1

    .line 102
    :cond_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    .line 106
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mSelectedRemoteItem:Lcom/samsung/android/allshare/Item;

    if-nez v3, :cond_3

    .line 108
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 110
    .local v0, "file":Ljava/io/File;
    if-nez v0, :cond_2

    .line 112
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    .line 115
    :cond_2
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    .line 119
    .end local v0    # "file":Ljava/io/File;
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mSelectedRemoteItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/Item;->getExtension()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 120
    iget-object v3, p0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mSelectedRemoteItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/Item;->getExtension()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_4

    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 122
    :cond_5
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0
.end method
