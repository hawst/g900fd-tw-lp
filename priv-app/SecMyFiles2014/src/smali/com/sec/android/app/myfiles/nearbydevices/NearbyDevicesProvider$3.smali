.class Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$3;
.super Ljava/lang/Object;
.source "NearbyDevicesProvider.java"

# interfaces
.implements Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)V
    .locals 0

    .prologue
    .line 298
    iput-object p1, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$3;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeviceAdded(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/Device;Lcom/samsung/android/allshare/ERROR;)V
    .locals 2
    .param p1, "deviceType"    # Lcom/samsung/android/allshare/Device$DeviceType;
    .param p2, "device"    # Lcom/samsung/android/allshare/Device;
    .param p3, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 312
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$3;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    # getter for: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mDevicesCollection:Ljava/util/Collection;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$300(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 314
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$3;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    # getter for: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mSaveCurDevices:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$400(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$3;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    # getter for: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mDevicesCollection:Ljava/util/Collection;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$300(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$3;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    # invokes: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->checkDevice(Lcom/samsung/android/allshare/Device;)Lcom/samsung/android/allshare/Device;
    invoke-static {v1, p2}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$800(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;Lcom/samsung/android/allshare/Device;)Lcom/samsung/android/allshare/Device;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 316
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$3;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    # invokes: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->notifyDeviceAdded(Lcom/samsung/android/allshare/Device;)V
    invoke-static {v0, p2}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$900(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;Lcom/samsung/android/allshare/Device;)V

    .line 324
    :cond_0
    :goto_0
    return-void

    .line 318
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$3;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    # invokes: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->isRegisteredDevice(Lcom/samsung/android/allshare/Device;)Z
    invoke-static {v0, p2}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$1000(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;Lcom/samsung/android/allshare/Device;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$3;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    # getter for: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mSaveCurDevices:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$400(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$3;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    # getter for: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mDevicesCollection:Ljava/util/Collection;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$300(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$3;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    # invokes: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->checkDevice(Lcom/samsung/android/allshare/Device;)Lcom/samsung/android/allshare/Device;
    invoke-static {v1, p2}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$800(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;Lcom/samsung/android/allshare/Device;)Lcom/samsung/android/allshare/Device;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 322
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$3;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    # invokes: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->notifyDeviceAdded(Lcom/samsung/android/allshare/Device;)V
    invoke-static {v0, p2}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$900(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;Lcom/samsung/android/allshare/Device;)V

    goto :goto_0
.end method

.method public onDeviceRemoved(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/Device;Lcom/samsung/android/allshare/ERROR;)V
    .locals 1
    .param p1, "deviceType"    # Lcom/samsung/android/allshare/Device$DeviceType;
    .param p2, "device"    # Lcom/samsung/android/allshare/Device;
    .param p3, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$3;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    # getter for: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mDevicesCollection:Ljava/util/Collection;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$300(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$3;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    # invokes: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->notifyDeviceRemoved(Lcom/samsung/android/allshare/Device;)V
    invoke-static {v0, p2}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$700(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;Lcom/samsung/android/allshare/Device;)V

    .line 305
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$3;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    # getter for: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mSaveCurDevices:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$400(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$3;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    # getter for: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mSaveCurDevices:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$400(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$3;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    # getter for: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mSaveCurDevices:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$400(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 307
    :cond_0
    return-void
.end method
