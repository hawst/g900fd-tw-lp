.class Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;
.super Ljava/lang/Object;
.source "BaiduDownLoadOperationTask.java"

# interfaces
.implements Lcom/samsung/scloud/response/ProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ConcreteProgressListener"
.end annotation


# instance fields
.field private mFile:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;

.field private mTotalSize:J

.field private mUpFile:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;

.field final synthetic this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 423
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    .line 425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 403
    iput-wide v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->mTotalSize:J

    .line 405
    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->mFile:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;

    .line 407
    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->mUpFile:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;

    .line 426
    iput-wide v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->mTotalSize:J

    .line 427
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;JLcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;)V
    .locals 4
    .param p2, "totalSize"    # J
    .param p4, "file"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;

    .prologue
    const/4 v2, 0x0

    .line 409
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    .line 411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 403
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->mTotalSize:J

    .line 405
    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->mFile:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;

    .line 407
    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->mUpFile:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;

    .line 412
    iput-wide p2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->mTotalSize:J

    .line 413
    iput-object p4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->mFile:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;

    .line 414
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;JLcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;)V
    .locals 4
    .param p2, "totalSize"    # J
    .param p4, "file"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;

    .prologue
    const/4 v2, 0x0

    .line 416
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    .line 418
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 403
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->mTotalSize:J

    .line 405
    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->mFile:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;

    .line 407
    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->mUpFile:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;

    .line 419
    iput-wide p2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->mTotalSize:J

    .line 420
    iput-object p4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->mUpFile:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;

    .line 421
    return-void
.end method


# virtual methods
.method public onException(Lcom/samsung/scloud/exception/SCloudException;)V
    .locals 0
    .param p1, "e"    # Lcom/samsung/scloud/exception/SCloudException;

    .prologue
    .line 432
    return-void
.end method

.method public onUpdate(Ljava/lang/String;IJ)V
    .locals 5
    .param p1, "file"    # Ljava/lang/String;
    .param p2, "statusTransferOngoing"    # I
    .param p3, "progress"    # J

    .prologue
    const/4 v4, 0x0

    .line 437
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->access$200(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;)Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->setToContinue(Z)V

    .line 455
    :goto_0
    return-void

    .line 441
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->mTotalSize:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    .line 442
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->mFile:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;

    if-eqz v0, :cond_1

    .line 443
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->mFile:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;

    iput-wide p3, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;->mDownloadPercent:J

    .line 445
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->mUpFile:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;

    if-eqz v0, :cond_2

    .line 446
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->mUpFile:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;

    iput-wide p3, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;->mUploadPercent:J

    .line 448
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    # invokes: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->updateProgressBar()V
    invoke-static {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->access$300(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;)V

    goto :goto_0

    .line 451
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->access$400(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;)Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    move-result-object v0

    long-to-int v1, p3

    iput v1, v0, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mCurrentTargetPercentage:I

    .line 453
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->access$500(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;)Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    move-result-object v2

    aput-object v2, v1, v4

    # invokes: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->publishProgress([Ljava/lang/Object;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->access$600(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;[Ljava/lang/Object;)V

    goto :goto_0
.end method
