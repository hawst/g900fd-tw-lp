.class Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;
.super Landroid/os/AsyncTask;
.source "DropboxAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DropBoxThumbnailWorkerTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$IdPath;",
        "Ljava/lang/Integer;",
        "Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;",
        ">;"
    }
.end annotation


# instance fields
.field private mContentsType:I

.field private mFilePath:Ljava/lang/String;

.field private mImageView:Landroid/widget/ImageView;

.field public mStarted:Z

.field private mThumbnailCacheFile:Ljava/io/File;

.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;Landroid/widget/ImageView;I)V
    .locals 1
    .param p2, "imageView"    # Landroid/widget/ImageView;
    .param p3, "type"    # I

    .prologue
    .line 607
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 603
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mThumbnailCacheFile:Ljava/io/File;

    .line 604
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mFilePath:Ljava/lang/String;

    .line 605
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mStarted:Z

    .line 608
    iput-object p2, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mImageView:Landroid/widget/ImageView;

    .line 609
    iput p3, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mContentsType:I

    .line 610
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;Landroid/widget/ImageView;ILjava/lang/String;)V
    .locals 0
    .param p2, "imageView"    # Landroid/widget/ImageView;
    .param p3, "type"    # I
    .param p4, "filePath"    # Ljava/lang/String;

    .prologue
    .line 613
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;-><init>(Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;Landroid/widget/ImageView;I)V

    .line 614
    iput-object p4, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mFilePath:Ljava/lang/String;

    .line 615
    return-void
.end method

.method private postExecuteActions(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;)V
    .locals 4
    .param p1, "result"    # Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;

    .prologue
    .line 675
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mThumbnailWorkerTasks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 676
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mThumbnailWorkerTasks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p0, :cond_2

    .line 677
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mThumbnailWorkerTasks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 681
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mThumbnailWorkerTasks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 682
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mThumbnailWorkerTasks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;

    iget-boolean v1, v1, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mStarted:Z

    if-nez v1, :cond_3

    .line 683
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mThumbnailWorkerTasks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mStarted:Z

    .line 684
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mThumbnailWorkerTasks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;

    sget-object v2, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$IdPath;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 690
    :cond_1
    return-void

    .line 675
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 681
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$IdPath;)Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;
    .locals 14
    .param p1, "params"    # [Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$IdPath;

    .prologue
    const/4 v3, 0x5

    const/4 v4, 0x0

    const/4 v13, 0x0

    .line 619
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mFilePath:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mFilePath:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    move-object v1, v13

    .line 671
    :goto_0
    return-object v1

    .line 621
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v1, v13

    .line 623
    goto :goto_0

    .line 625
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    move-result-object v0

    .line 627
    .local v0, "DBAPIHelper":Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;
    iget v1, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mContentsType:I

    packed-switch v1, :pswitch_data_0

    move-object v1, v13

    .line 671
    goto :goto_0

    .line 629
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mFilePath:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getFileThumbnailFolderName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 630
    .local v9, "serverFolderName":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 631
    .local v7, "ThumbnailCacheFolder":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_3

    .line 632
    invoke-virtual {v7}, Ljava/io/File;->mkdirs()Z

    .line 634
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "large.jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 636
    .local v6, "ThumbnailCacheFilePath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mThumbnailCacheFile:Ljava/io/File;

    .line 638
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mThumbnailCacheFile:Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mFilePath:Ljava/lang/String;

    new-instance v5, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$ConcreteProgressListener;

    invoke-direct {v5}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$ConcreteProgressListener;-><init>()V

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->getFileThumbnail(Ljava/io/File;Ljava/lang/String;IILcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;

    .line 640
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mThumbnailCacheFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 643
    invoke-static {v6}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 644
    .local v8, "bitmapImage":Landroid/graphics/Bitmap;
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mFilePath:Ljava/lang/String;

    invoke-direct {v1, v2, v8}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;-><init>(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .end local v8    # "bitmapImage":Landroid/graphics/Bitmap;
    :cond_4
    move-object v1, v13

    .line 649
    goto :goto_0

    .line 653
    .end local v6    # "ThumbnailCacheFilePath":Ljava/lang/String;
    .end local v7    # "ThumbnailCacheFolder":Ljava/io/File;
    .end local v9    # "serverFolderName":Ljava/lang/String;
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mFilePath:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getFileThumbnailFolderName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 654
    .local v10, "videoServerFolderName":Ljava/lang/String;
    new-instance v12, Ljava/io/File;

    invoke-direct {v12, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 655
    .local v12, "videoThumbnailCacheFolder":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_5

    .line 656
    invoke-virtual {v12}, Ljava/io/File;->mkdirs()Z

    .line 658
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "large.jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 659
    .local v11, "videoThumbnailCacheFilePath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mThumbnailCacheFile:Ljava/io/File;

    .line 660
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mThumbnailCacheFile:Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mFilePath:Ljava/lang/String;

    new-instance v5, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$ConcreteProgressListener;

    invoke-direct {v5}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$ConcreteProgressListener;-><init>()V

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->getFileThumbnail(Ljava/io/File;Ljava/lang/String;IILcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;

    .line 661
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mThumbnailCacheFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 662
    invoke-static {v11}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 663
    .restart local v8    # "bitmapImage":Landroid/graphics/Bitmap;
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mFilePath:Ljava/lang/String;

    invoke-direct {v1, v2, v8}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;-><init>(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .end local v8    # "bitmapImage":Landroid/graphics/Bitmap;
    :cond_6
    move-object v1, v13

    .line 665
    goto/16 :goto_0

    .line 627
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 600
    check-cast p1, [Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$IdPath;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->doInBackground([Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$IdPath;)Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;)V
    .locals 0
    .param p1, "result"    # Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;

    .prologue
    .line 694
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->postExecuteActions(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;)V

    .line 695
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onCancelled(Ljava/lang/Object;)V

    .line 696
    return-void
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 600
    check-cast p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->onCancelled(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;)V

    return-void
.end method

.method protected onPostExecute(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;)V
    .locals 3
    .param p1, "result"    # Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;

    .prologue
    .line 700
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 701
    iget-object v0, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;->mPath:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 702
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mThumbnailCache:Landroid/util/LruCache;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->access$200(Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;)Landroid/util/LruCache;

    move-result-object v0

    iget-object v1, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;->mPath:Ljava/lang/String;

    iget-object v2, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 703
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mImageView:Landroid/widget/ImageView;

    iget-object v1, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 706
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->postExecuteActions(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;)V

    .line 707
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 708
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 600
    check-cast p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->onPostExecute(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;)V

    return-void
.end method
