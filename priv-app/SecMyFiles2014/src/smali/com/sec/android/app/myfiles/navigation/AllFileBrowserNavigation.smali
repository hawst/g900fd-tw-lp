.class public Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;
.super Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
.source "AllFileBrowserNavigation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$GetRootFolderTask;
    }
.end annotation


# static fields
.field private static final FOLDER_DATA_INDEX:I = 0x1

.field private static final MODULE:Ljava/lang/String; = "AllFileBrowserNavigation"

.field private static final mFilesApiProjection:[Ljava/lang/String;

.field private static final mFilesProjection:[Ljava/lang/String;

.field private static final mFolderProjection:[Ljava/lang/String;


# instance fields
.field private isStorageChooserScreenVisible:Z

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mFileUri:Landroid/net/Uri;

.field private mHideStorageSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 69
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "_data"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mFolderProjection:[Ljava/lang/String;

    .line 74
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "_data"

    aput-object v1, v0, v3

    const-string v1, "format"

    aput-object v1, v0, v4

    const-string v1, "date_modified"

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mFilesProjection:[Ljava/lang/String;

    .line 80
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "_data"

    aput-object v1, v0, v3

    const-string v1, "format"

    aput-object v1, v0, v4

    const-string v1, "_size"

    aput-object v1, v0, v5

    const-string v1, "date_modified"

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mFilesApiProjection:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I

    .prologue
    .line 90
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;-><init>(Landroid/content/Context;I)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mContentResolver:Landroid/content/ContentResolver;

    .line 96
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mFileUri:Landroid/net/Uri;

    .line 98
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mHideStorageSet:Ljava/util/HashSet;

    .line 99
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mFileUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$100()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mFolderProjection:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;)Landroid/content/ContentResolver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mContentResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method private createFilteringWhere()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1234
    const-string v0, ""

    .line 1236
    .local v0, "filteringWhere":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1239
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_data LIKE ? OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1236
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1242
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1244
    const/4 v2, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1252
    :goto_1
    return-object v0

    .line 1249
    :cond_1
    const-string v0, "_data LIKE ?"

    goto :goto_1
.end method

.method private createFilteringWhereArgs()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1258
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1260
    .local v1, "extensionResultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1262
    .local v0, "extension":Ljava/lang/String;
    const-string v3, "MP4_A"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "3GP_A"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "3G2_A"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "ASF_A"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "3GPP_A"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1265
    :cond_0
    const-string v3, "_A"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1269
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1272
    .end local v0    # "extension":Ljava/lang/String;
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_3

    .line 1275
    const-string v3, "%."

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1278
    :cond_3
    return-object v1
.end method

.method private getFilesInPath(Ljava/lang/String;III)Landroid/database/Cursor;
    .locals 30
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "sortBy"    # I
    .param p3, "inOrder"    # I
    .param p4, "target"    # I

    .prologue
    .line 642
    const/4 v9, 0x0

    .line 644
    .local v9, "where":Ljava/lang/String;
    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    .line 646
    .local v26, "whereArgsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v10, 0x0

    .line 648
    .local v10, "whereArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    move/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-static {v0, v1, v2, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesDB;->getOrderBy(IIIZ)Ljava/lang/String;

    move-result-object v23

    .line 650
    .local v23, "orderBy":Ljava/lang/String;
    const-string v22, "format=12289"

    .line 652
    .local v22, "onlyFolder":Ljava/lang/String;
    const-string v21, "NOT format=12289"

    .line 654
    .local v21, "onlyFile":Ljava/lang/String;
    const-wide/16 v24, 0x0

    .line 657
    .local v24, "pathId":J
    const/4 v6, 0x2

    const-string v7, "AllFileBrowserNavigation"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getFilesInPath() : path = "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 660
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isAbsoluteRootFolder(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 662
    new-instance v18, Landroid/database/MatrixCursor;

    sget-object v6, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mFilesProjection:[Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-direct {v0, v6}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 664
    .local v18, "mc":Landroid/database/MatrixCursor;
    if-eqz p2, :cond_0

    if-eqz p2, :cond_5

    if-nez p4, :cond_5

    .line 667
    :cond_0
    const/4 v14, 0x0

    .line 669
    .local v14, "file":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mHideStorageSet:Ljava/util/HashSet;

    const/16 v7, 0x101

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 671
    new-instance v14, Ljava/io/File;

    .end local v14    # "file":Ljava/io/File;
    sget-object v6, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-direct {v14, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 673
    .restart local v14    # "file":Ljava/io/File;
    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const/16 v8, 0x101

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    sget-object v8, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const/16 v8, 0x3001

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    invoke-virtual {v14}, Ljava/io/File;->lastModified()J

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 680
    :cond_1
    sget-boolean v6, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v6, :cond_2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mHideStorageSet:Ljava/util/HashSet;

    const/16 v7, 0x202

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 682
    new-instance v14, Ljava/io/File;

    .end local v14    # "file":Ljava/io/File;
    sget-object v6, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardStoragePath:Ljava/lang/String;

    invoke-direct {v14, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 684
    .restart local v14    # "file":Ljava/io/File;
    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const/16 v8, 0x202

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "/storage/extSdCard"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const/16 v8, 0x3001

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    invoke-virtual {v14}, Ljava/io/File;->lastModified()J

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 691
    :cond_2
    sget-boolean v6, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-eqz v6, :cond_4

    .line 693
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getMountedUsbStorage()Ljava/util/ArrayList;

    move-result-object v20

    .line 697
    .local v20, "mountedUsbList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;>;"
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_0
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v6

    move/from16 v0, v17

    if-ge v0, v6, :cond_4

    .line 699
    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;

    .line 701
    .local v19, "mountedUsb":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mHideStorageSet:Ljava/util/HashSet;

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->getStorageType()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 703
    new-instance v14, Ljava/io/File;

    .end local v14    # "file":Ljava/io/File;
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->getStoragePath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v14, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 705
    .restart local v14    # "file":Ljava/io/File;
    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->getStorageType()I

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->getStoragePath()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const/16 v8, 0x3001

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    invoke-virtual {v14}, Ljava/io/File;->lastModified()J

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 697
    :cond_3
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    .line 714
    .end local v17    # "i":I
    .end local v19    # "mountedUsb":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    .end local v20    # "mountedUsbList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;>;"
    :cond_4
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretMode(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mHideStorageSet:Ljava/util/HashSet;

    const/16 v7, 0x9

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 716
    new-instance v14, Ljava/io/File;

    .end local v14    # "file":Ljava/io/File;
    sget-object v6, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-direct {v14, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 718
    .restart local v14    # "file":Ljava/io/File;
    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const/16 v8, 0x9

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/android/app/myfiles/utils/Utils;->getSecretBoxRootFolder(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const/16 v8, 0x3001

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    invoke-virtual {v14}, Ljava/io/File;->lastModified()J

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 993
    .end local v14    # "file":Ljava/io/File;
    .end local v18    # "mc":Landroid/database/MatrixCursor;
    :cond_5
    :goto_1
    return-object v18

    .line 729
    :cond_6
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isUsbStorageFolder(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 731
    new-instance v18, Landroid/database/MatrixCursor;

    sget-object v6, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mFilesApiProjection:[Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-direct {v0, v6}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 733
    .restart local v18    # "mc":Landroid/database/MatrixCursor;
    new-instance v14, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v14, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 735
    .restart local v14    # "file":Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_f

    .line 737
    const/4 v15, 0x0

    .line 739
    .local v15, "files":[Ljava/io/File;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->isEnableFiltering()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 740
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->isDrmFiltering()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 742
    new-instance v6, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$1;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$1;-><init>(Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;)V

    invoke-virtual {v14, v6}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v15

    .line 851
    :goto_2
    if-eqz v15, :cond_f

    .line 857
    const/4 v6, 0x2

    const-string v7, "AllFileBrowserNavigation"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Usb storage path : "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 861
    const/16 v17, 0x0

    .restart local v17    # "i":I
    :goto_3
    array-length v6, v15

    move/from16 v0, v17

    if-ge v0, v6, :cond_f

    .line 863
    aget-object v6, v15, v17

    invoke-static {v6}, Lcom/sec/android/app/myfiles/utils/Utils;->checkNotVewingItem(Ljava/io/File;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 861
    :cond_7
    :goto_4
    add-int/lit8 v17, v17, 0x1

    goto :goto_3

    .line 762
    .end local v17    # "i":I
    :cond_8
    new-instance v6, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$2;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$2;-><init>(Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;)V

    invoke-virtual {v14, v6}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v15

    goto :goto_2

    .line 848
    :cond_9
    invoke-virtual {v14}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v15

    goto :goto_2

    .line 869
    .restart local v17    # "i":I
    :cond_a
    const/4 v6, 0x2

    move/from16 v0, p4

    if-eq v0, v6, :cond_c

    if-nez p4, :cond_b

    aget-object v6, v15, v17

    invoke-virtual {v6}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_c

    :cond_b
    const/4 v6, 0x1

    move/from16 v0, p4

    if-ne v0, v6, :cond_7

    aget-object v6, v15, v17

    invoke-virtual {v6}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_7

    .line 872
    :cond_c
    const/4 v6, 0x5

    new-array v7, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    aget-object v8, v15, v17

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v6

    const/4 v6, 0x1

    aget-object v8, v15, v17

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v6

    const/4 v8, 0x2

    aget-object v6, v15, v17

    invoke-virtual {v6}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_d

    const/16 v6, 0x3001

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    :goto_5
    aput-object v6, v7, v8

    const/4 v8, 0x3

    aget-object v6, v15, v17

    invoke-virtual {v6}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_e

    const-string v6, "0"

    :goto_6
    aput-object v6, v7, v8

    const/4 v6, 0x4

    aget-object v8, v15, v17

    invoke-virtual {v8}, Ljava/io/File;->lastModified()J

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_4

    :cond_d
    const-string v6, "0"

    goto :goto_5

    :cond_e
    aget-object v6, v15, v17

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    goto :goto_6

    .line 890
    .end local v15    # "files":[Ljava/io/File;
    .end local v17    # "i":I
    :cond_f
    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->setStorageChooserScreenVisible(Z)V

    .line 892
    new-instance v16, Lcom/sec/android/app/myfiles/SortCursor;

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move/from16 v2, p2

    move/from16 v3, p3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/myfiles/SortCursor;-><init>(Landroid/database/Cursor;II)V

    .local v16, "fsc":Lcom/sec/android/app/myfiles/SortCursor;
    move-object/from16 v18, v16

    .line 893
    goto/16 :goto_1

    .line 895
    .end local v14    # "file":Ljava/io/File;
    .end local v16    # "fsc":Lcom/sec/android/app/myfiles/SortCursor;
    .end local v18    # "mc":Landroid/database/MatrixCursor;
    :cond_10
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->UsingFileSystem()Z

    move-result v6

    if-eqz v6, :cond_11

    .line 897
    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->setStorageChooserScreenVisible(Z)V

    .line 899
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p4

    move/from16 v3, p2

    move/from16 v4, p3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->searchAndroidFolder(Ljava/lang/String;III)Lcom/sec/android/app/myfiles/SortCursor;

    move-result-object v18

    goto/16 :goto_1

    .line 901
    :cond_11
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isInternalRootFolder(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 903
    const-string v9, "_data LIKE ? AND parent=0"

    .line 905
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "%"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 940
    .end local v10    # "whereArgs":[Ljava/lang/String;
    :goto_7
    packed-switch p4, :pswitch_data_0

    .line 975
    :cond_12
    :goto_8
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowHiddenFileStatus()Z

    move-result v6

    if-nez v6, :cond_13

    .line 977
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND NOT (SUBSTR(LOWER("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_data"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "),LENGTH(RTRIM(LOWER("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_data"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "),"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\"abcdefghijklmnopqrstuvwxyz01234567890;.!@#$%^&()-_=+\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "))) LIKE \'/.%\')"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 980
    :cond_13
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Ljava/lang/String;

    .line 982
    .restart local v10    # "whereArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->setStorageChooserScreenVisible(Z)V

    .line 984
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "external"

    invoke-static {v7}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    const/4 v8, 0x0

    move-object/from16 v11, v23

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 990
    .local v12, "c":Landroid/database/Cursor;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isAndroidFolder(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_19

    .line 991
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p4

    move/from16 v3, p2

    move/from16 v4, p3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->searchAndroidFolder(Ljava/lang/String;III)Lcom/sec/android/app/myfiles/SortCursor;

    move-result-object v18

    goto/16 :goto_1

    .line 907
    .end local v12    # "c":Landroid/database/Cursor;
    :cond_14
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isExternalRootFolder(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 909
    const-string v9, "_data LIKE ? AND parent=0"

    .line 911
    const-string v6, "/storage/extSdCard%"

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_7

    .line 913
    :cond_15
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_16

    .line 915
    const-string v9, "_data LIKE ? AND parent=0"

    .line 917
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "%"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_7

    .line 921
    :cond_16
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mContentResolver:Landroid/content/ContentResolver;

    const-string v7, "external"

    invoke-static {v7}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    const/4 v8, 0x0

    const-string v9, "_data LIKE ?"

    .end local v9    # "where":Ljava/lang/String;
    const/4 v11, 0x1

    new-array v10, v11, [Ljava/lang/String;

    .end local v10    # "whereArgs":[Ljava/lang/String;
    const/4 v11, 0x0

    aput-object p1, v10, v11

    const/4 v11, 0x0

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 927
    .local v13, "cursor":Landroid/database/Cursor;
    if-eqz v13, :cond_18

    .line 929
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_17

    .line 931
    const-string v6, "_id"

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    .line 934
    :cond_17
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 937
    :cond_18
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "parent="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v24

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .restart local v9    # "where":Ljava/lang/String;
    goto/16 :goto_7

    .line 944
    .end local v13    # "cursor":Landroid/database/Cursor;
    :pswitch_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 946
    goto/16 :goto_8

    .line 951
    :pswitch_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 953
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->isEnableFiltering()Z

    move-result v6

    if-eqz v6, :cond_12

    .line 955
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->createFilteringWhere()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 957
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->createFilteringWhereArgs()Ljava/util/ArrayList;

    move-result-object v6

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_8

    .line 965
    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->isEnableFiltering()Z

    move-result v6

    if-eqz v6, :cond_12

    .line 967
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " OR ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->createFilteringWhere()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "))"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 969
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->createFilteringWhereArgs()Ljava/util/ArrayList;

    move-result-object v6

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_8

    .restart local v10    # "whereArgs":[Ljava/lang/String;
    .restart local v12    # "c":Landroid/database/Cursor;
    :cond_19
    move-object/from16 v18, v12

    .line 993
    goto/16 :goto_1

    .line 940
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private searchAndroidFolder(Ljava/lang/String;III)Lcom/sec/android/app/myfiles/SortCursor;
    .locals 8
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "target"    # I
    .param p3, "sortBy"    # I
    .param p4, "inOrder"    # I

    .prologue
    .line 998
    if-eqz p1, :cond_8

    .line 1000
    new-instance v3, Landroid/database/MatrixCursor;

    sget-object v4, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mFilesApiProjection:[Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 1002
    .local v3, "mc":Landroid/database/MatrixCursor;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1004
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1006
    const/4 v2, 0x0

    .line 1008
    .local v2, "files":[Ljava/io/File;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->isEnableFiltering()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1010
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->isDrmFiltering()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1012
    new-instance v4, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$3;-><init>(Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;)V

    invoke-virtual {v1, v4}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v2

    .line 1126
    :goto_0
    if-eqz v2, :cond_7

    .line 1128
    const/4 v0, 0x0

    .local v0, "ctr":I
    :goto_1
    array-length v4, v2

    if-ge v0, v4, :cond_7

    .line 1130
    aget-object v4, v2, v0

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->checkNotVewingItem(Ljava/io/File;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1128
    :cond_0
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1033
    .end local v0    # "ctr":I
    :cond_1
    new-instance v4, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$4;

    invoke-direct {v4, p0}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$4;-><init>(Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;)V

    invoke-virtual {v1, v4}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v2

    goto :goto_0

    .line 1123
    :cond_2
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    goto :goto_0

    .line 1135
    .restart local v0    # "ctr":I
    :cond_3
    aget-object v4, v2, v0

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1136
    if-eqz p2, :cond_4

    const/4 v4, 0x2

    if-ne p2, v4, :cond_0

    .line 1138
    :cond_4
    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v6, v2, v0

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aget-object v6, v2, v0

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const/16 v6, 0x3001

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "0"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    aget-object v6, v2, v0

    invoke-virtual {v6}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_2

    .line 1146
    :cond_5
    const/4 v4, 0x1

    if-eq p2, v4, :cond_6

    const/4 v4, 0x2

    if-ne p2, v4, :cond_0

    .line 1147
    :cond_6
    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v6, v2, v0

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aget-object v6, v2, v0

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "0"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    aget-object v6, v2, v0

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    aget-object v6, v2, v0

    invoke-virtual {v6}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 1156
    .end local v0    # "ctr":I
    .end local v2    # "files":[Ljava/io/File;
    :cond_7
    new-instance v4, Lcom/sec/android/app/myfiles/SortCursor;

    iget v5, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentSortBy:I

    iget v6, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentInOrder:I

    invoke-direct {v4, v3, v5, v6}, Lcom/sec/android/app/myfiles/SortCursor;-><init>(Landroid/database/Cursor;II)V

    .line 1158
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "mc":Landroid/database/MatrixCursor;
    :goto_3
    return-object v4

    :cond_8
    const/4 v4, 0x0

    goto :goto_3
.end method


# virtual methods
.method public AsyncOpen()V
    .locals 3

    .prologue
    .line 1165
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$5;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$5;-><init>(Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;Lcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    .line 1175
    .local v0, "fg":Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "OpenOperation"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1176
    return-void
.end method

.method public add(ILjava/lang/String;)Z
    .locals 1
    .param p1, "id"    # I
    .param p2, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 254
    const/4 v0, 0x1

    return v0
.end method

.method public addAll(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 215
    .local p1, "idList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local p2, "folderList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    return-void
.end method

.method public addHideStorage(I)V
    .locals 2
    .param p1, "storage"    # I

    .prologue
    .line 1283
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mHideStorageSet:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1284
    return-void
.end method

.method public clear()V
    .locals 0

    .prologue
    .line 278
    return-void
.end method

.method public clearHideStorage()V
    .locals 1

    .prologue
    .line 1295
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mHideStorageSet:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 1296
    return-void
.end method

.method public clearHistoricalPaths()V
    .locals 1

    .prologue
    .line 103
    invoke-super {p0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->clearHistoricalPaths()V

    .line 104
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getRootFolder()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 105
    return-void
.end method

.method public getCurrentFolderID()I
    .locals 1

    .prologue
    .line 574
    const/4 v0, 0x0

    return v0
.end method

.method public getExistParentPath(Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "historicalPaths"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;
    .param p2, "currentPath"    # Ljava/lang/String;

    .prologue
    .line 1301
    :goto_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->getList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 1302
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->get()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1303
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1304
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->pop()Ljava/lang/String;

    .line 1305
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 1310
    .end local v0    # "file":Ljava/io/File;
    :goto_1
    return-object v1

    .line 1307
    .restart local v0    # "file":Ljava/io/File;
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->pop()Ljava/lang/String;

    goto :goto_0

    .line 1310
    .end local v0    # "file":Ljava/io/File;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getFilesInCurrentFolder()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 636
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->getFilesInFolder(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getFilesInFolder(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 581
    iget v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mShowFolderFileType:I

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->getFilesInFolder(Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getFilesInFolder(Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 8
    .param p1, "folderPath"    # Ljava/lang/String;
    .param p2, "showFolderFileType"    # I

    .prologue
    const/4 v7, 0x1

    .line 594
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 596
    .local v1, "cursors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/database/Cursor;>;"
    iget v4, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentSortBy:I

    packed-switch v4, :pswitch_data_0

    .line 618
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v0, v4, [Landroid/database/Cursor;

    .line 622
    .local v0, "cursorArray":[Landroid/database/Cursor;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_3

    .line 624
    iget v4, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentInOrder:I

    if-nez v4, :cond_2

    move v3, v2

    .line 626
    .local v3, "index":I
    :goto_2
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/database/Cursor;

    aput-object v4, v0, v2

    .line 622
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 599
    .end local v0    # "cursorArray":[Landroid/database/Cursor;
    .end local v2    # "i":I
    .end local v3    # "index":I
    :pswitch_0
    iget v4, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentSortBy:I

    iget v5, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentInOrder:I

    invoke-direct {p0, p1, v4, v5, p2}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->getFilesInPath(Ljava/lang/String;III)Landroid/database/Cursor;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 606
    :pswitch_1
    if-eq p2, v7, :cond_1

    .line 608
    iget v4, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentSortBy:I

    iget v5, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentInOrder:I

    const/4 v6, 0x0

    invoke-direct {p0, p1, v4, v5, v6}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->getFilesInPath(Ljava/lang/String;III)Landroid/database/Cursor;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 611
    :cond_1
    if-eqz p2, :cond_0

    .line 613
    iget v4, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentSortBy:I

    iget v5, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentInOrder:I

    invoke-direct {p0, p1, v4, v5, v7}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->getFilesInPath(Ljava/lang/String;III)Landroid/database/Cursor;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 624
    .restart local v0    # "cursorArray":[Landroid/database/Cursor;
    .restart local v2    # "i":I
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    sub-int/2addr v4, v2

    add-int/lit8 v3, v4, -0x1

    goto :goto_2

    .line 629
    :cond_3
    new-instance v4, Landroid/database/MergeCursor;

    invoke-direct {v4, v0}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    return-object v4

    .line 596
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public getPositionFromPath(Ljava/lang/String;)I
    .locals 5
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1180
    const/4 v3, 0x0

    .line 1182
    .local v3, "position":I
    const/4 v1, 0x0

    .line 1183
    .local v1, "existPosition":Z
    if-eqz p1, :cond_1

    .line 1185
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v0

    .line 1187
    .local v0, "cursorForFinDo":Landroid/database/Cursor;
    if-eqz v0, :cond_5

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1191
    :cond_0
    const-string v4, "_data"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1193
    .local v2, "filePath":Ljava/lang/String;
    if-nez v2, :cond_3

    .line 1194
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1204
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1206
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1214
    .end local v0    # "cursorForFinDo":Landroid/database/Cursor;
    .end local v2    # "filePath":Ljava/lang/String;
    :cond_1
    :goto_2
    if-nez v1, :cond_2

    .line 1215
    const/4 v3, 0x0

    .line 1217
    :cond_2
    return v3

    .line 1197
    .restart local v0    # "cursorForFinDo":Landroid/database/Cursor;
    .restart local v2    # "filePath":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1198
    const/4 v1, 0x1

    .line 1199
    goto :goto_1

    .line 1201
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1210
    .end local v2    # "filePath":Ljava/lang/String;
    :cond_5
    if-eqz v0, :cond_1

    .line 1211
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_2
.end method

.method public goBack()Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 509
    const/4 v1, 0x0

    .line 511
    .local v1, "result":Z
    iget-object v5, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mHistoricalPaths:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->getExistParentPath(Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 512
    .local v2, "upPath":Ljava/lang/String;
    iput-object v2, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mWrongUpPath:Ljava/lang/String;

    .line 515
    if-nez v2, :cond_0

    .line 567
    :goto_0
    return v3

    .line 519
    :cond_0
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->UsingFileSystem()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 521
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isUsbStorageFolder(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isAndroidFolder(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isInExternalSdStorage(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxFolder(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 523
    :cond_1
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isInExternalSdStorage(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    sget-boolean v5, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-nez v5, :cond_2

    .line 525
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->goUp()Z

    move-result v3

    goto :goto_0

    .line 528
    :cond_2
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isUsbStorageFolder(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    sget-boolean v5, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-nez v5, :cond_3

    .line 530
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->goUp()Z

    move-result v3

    goto :goto_0

    .line 533
    :cond_3
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxFolder(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretMode(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 535
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->goUp()Z

    move-result v3

    goto :goto_0

    .line 539
    :cond_4
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 540
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 542
    iput-object v2, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    move v3, v4

    .line 543
    goto :goto_0

    .line 547
    .end local v0    # "file":Ljava/io/File;
    :cond_5
    iput-object v2, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 553
    :cond_6
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isInternalRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 555
    iput-object v2, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    move v3, v4

    .line 556
    goto :goto_0

    .line 559
    :cond_7
    if-eqz v2, :cond_8

    .line 560
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 561
    .restart local v0    # "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 562
    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->notifyUpdate(Z)V

    .line 563
    const/4 v1, 0x1

    .end local v0    # "file":Ljava/io/File;
    :cond_8
    move v3, v1

    .line 567
    goto :goto_0
.end method

.method public goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z
    .locals 11
    .param p1, "folderPath"    # Ljava/lang/String;
    .param p2, "isBack"    # Ljava/lang/Boolean;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 291
    const-string v0, "AllFileBrowserNavigation"

    const-string v1, "goTo()"

    invoke-static {v9, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 292
    const/4 v0, 0x2

    const-string v1, "AllFileBrowserNavigation"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "=======> target folder : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 294
    const/4 v8, 0x0

    .line 296
    .local v8, "result":Z
    if-nez p1, :cond_0

    move v0, v8

    .line 386
    :goto_0
    return v0

    .line 301
    :cond_0
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->UsingFileSystem()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isRootFolder(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isInternalRootFolder(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isExternalRootFolder(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isUsbStorageFolder(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isAndroidFolder(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 309
    :cond_1
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 311
    .local v7, "file":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 313
    const-string v0, "/storage/extSdCard"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v0, :cond_3

    :cond_2
    const-string v0, "/storage/UsbDrive"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-nez v0, :cond_4

    .line 315
    :cond_3
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getRootFolder()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    move v0, v9

    .line 316
    goto :goto_0

    .line 319
    :cond_4
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->RemoveAllPaddingThumbnailMessage()V

    .line 324
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_5

    .line 325
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mHistoricalPaths:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->push(Ljava/lang/String;)V

    .line 328
    :cond_5
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 334
    invoke-virtual {p0, v9}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->notifyUpdate(Z)V

    move v0, v10

    .line 336
    goto :goto_0

    .line 340
    :cond_6
    const-string v0, "AllFileBrowserNavigation"

    const-string v1, "goTo() : file is not exist"

    invoke-static {v9, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    move v0, v9

    .line 342
    goto :goto_0

    .line 346
    .end local v7    # "file":Ljava/io/File;
    :cond_7
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isEmulatedEntryFolder(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 348
    const-string v0, "AllFileBrowserNavigation"

    const-string v1, "goTo() : isEmulatedEntryFolder"

    invoke-static {v9, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    move v0, v9

    .line 350
    goto/16 :goto_0

    .line 353
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mFileUri:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mFolderProjection:[Ljava/lang/String;

    const-string v3, "_data = ?"

    new-array v4, v10, [Ljava/lang/String;

    aput-object p1, v4, v9

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 359
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_b

    .line 361
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v10, :cond_a

    .line 363
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 367
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_9

    .line 368
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mHistoricalPaths:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->push(Ljava/lang/String;)V

    .line 371
    :cond_9
    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 373
    invoke-virtual {p0, v9}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->notifyUpdate(Z)V

    .line 375
    const/4 v8, 0x1

    .line 383
    :cond_a
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_b
    move v0, v8

    .line 386
    goto/16 :goto_0
.end method

.method public goToRoot(Ljava/lang/Boolean;)Z
    .locals 2
    .param p1, "isBack"    # Ljava/lang/Boolean;

    .prologue
    .line 395
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mHistoricalPaths:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->push(Ljava/lang/String;)V

    .line 399
    :cond_0
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getRootFolder()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 401
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->notifyUpdate(Z)V

    .line 403
    const/4 v0, 0x1

    return v0
.end method

.method public goUp()Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 410
    const-string v3, "AllFileBrowserNavigation"

    const-string v4, "goUp()"

    invoke-static {v1, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 411
    const/4 v3, 0x2

    const-string v4, "AllFileBrowserNavigation"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "======> current path : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 413
    const-string v3, "/storage/extSdCard"

    iget-object v4, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "/storage/UsbDrive"

    iget-object v4, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 450
    :cond_0
    :goto_0
    return v1

    .line 421
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isRootFolder(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 422
    const-string v2, "AllFileBrowserNavigation"

    const-string v3, "goUp() : isRootFolder"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 425
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isInternalRootFolder(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isExternalRootFolder(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isUsbStorageRootFolder(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 432
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mHistoricalPaths:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->push(Ljava/lang/String;)V

    .line 434
    const-string v3, "/storage"

    iput-object v3, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 436
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->notifyUpdate(Z)V

    move v1, v2

    .line 438
    goto :goto_0

    .line 442
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getParentFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 444
    .local v0, "parentPath":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mHistoricalPaths:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->push(Ljava/lang/String;)V

    .line 446
    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 448
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->notifyUpdate(Z)V

    move v1, v2

    .line 450
    goto :goto_0
.end method

.method public isCurrentFolderRoot()Z
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isRootFolder(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isStorageChooserScreenVisible()Z
    .locals 1

    .prologue
    .line 1222
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->isStorageChooserScreenVisible:Z

    return v0
.end method

.method public refreshNavigation()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 115
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isAbsoluteRootFolder(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isCheckMountedStorage()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isInUsbHostStorage(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-eqz v2, :cond_3

    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isInExternalSdStorage(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v2, :cond_3

    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxFolder(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretMode(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 120
    :cond_3
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getRootFolder()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 122
    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->notifyUpdate(Z)V

    .line 124
    const-string v2, "AllFileBrowserNavigation"

    const-string v3, "refreshNavigatione() mCurrentFolderPath is setted by Rootfolder"

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 170
    :cond_4
    :goto_0
    return-void

    .line 130
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isUsbStorageFolder(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 132
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->getUsbStorage(Ljava/lang/String;)Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;

    move-result-object v1

    .line 134
    .local v1, "usbStorage":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    if-eqz v1, :cond_7

    .line 136
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->isMounted()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 138
    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->notifyUpdate(Z)V

    goto :goto_0

    .line 143
    :cond_6
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getRootFolder()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 145
    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->notifyUpdate(Z)V

    .line 151
    .end local v1    # "usbStorage":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    :cond_7
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->UsingFileSystem()Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isAbsoluteRootFolder(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isInternalRootFolder(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isExternalRootFolder(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 157
    :cond_8
    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->notifyUpdate(Z)V

    goto :goto_0

    .line 162
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 167
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$GetRootFolderTask;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$GetRootFolderTask;-><init>(Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;)V

    .line 169
    .local v0, "getRootFolder":Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$GetRootFolderTask;
    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$GetRootFolderTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public removeHideStorage(I)V
    .locals 2
    .param p1, "storage"    # I

    .prologue
    .line 1289
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mHideStorageSet:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1290
    return-void
.end method

.method public setCurrentPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 587
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 589
    return-void
.end method

.method public setStorageChooserScreenVisible(Z)V
    .locals 0
    .param p1, "isStorageChooserScreenVisible"    # Z

    .prologue
    .line 1228
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->isStorageChooserScreenVisible:Z

    .line 1229
    return-void
.end method
