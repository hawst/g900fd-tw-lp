.class Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;
.super Ljava/lang/Object;
.source "FTPNavigation.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/navigation/FTPNavigation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AsyncOpenRunnable"
.end annotation


# instance fields
.field private final mAsyncFragment:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

.field private final mCurrentItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

.field private final mDoNotCheckConnection:Z

.field private final mInitial:Z

.field final synthetic this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/navigation/FTPNavigation;Lcom/sec/android/app/myfiles/ftp/IFTPItem;Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;ZZ)V
    .locals 0
    .param p2, "currentItem"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .param p3, "asyncUi"    # Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;
    .param p4, "initial"    # Z
    .param p5, "doNotCheckConnection"    # Z

    .prologue
    .line 1094
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1096
    iput-object p2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mCurrentItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 1098
    iput-boolean p4, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mInitial:Z

    .line 1100
    iput-boolean p5, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mDoNotCheckConnection:Z

    .line 1102
    iput-object p3, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mAsyncFragment:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    .line 1104
    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;

    .prologue
    .line 1091
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mInitial:Z

    return v0
.end method


# virtual methods
.method public run()V
    .locals 19

    .prologue
    .line 1109
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1111
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    new-instance v3, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable$1;-><init>(Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1280
    :goto_0
    return-void

    .line 1126
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mCurrentItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    if-eqz v2, :cond_b

    .line 1128
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mCurrentItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    invoke-interface {v2}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mCurrentItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    invoke-interface {v2}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isSymbolicLink()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1129
    :cond_1
    const/4 v2, 0x2

    const-string v3, "ftp"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mCurrentItem : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mCurrentItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    invoke-interface {v6}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1130
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    # invokes: Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getPreviousPath()Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->access$000(Lcom/sec/android/app/myfiles/navigation/FTPNavigation;)Ljava/lang/String;

    move-result-object v18

    .line 1149
    .local v18, "previousPath":Ljava/lang/String;
    const/4 v5, 0x0

    .line 1153
    .local v5, "timestamp":Ljava/lang/String;
    if-eqz v5, :cond_4

    .line 1155
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCache:Lcom/sec/android/app/myfiles/ftp/FTPCache;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mCurrentItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    invoke-interface {v4}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v5, v4}, Lcom/sec/android/app/myfiles/ftp/FTPCache;->isPathCached(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1157
    const/4 v2, 0x2

    const-string v3, "ftp"

    const-string v4, "mAbortBrowsing ??  mCache.isPathCached !!!"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1159
    sget-boolean v2, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-eqz v2, :cond_2

    .line 1161
    const/4 v2, 0x2

    const-string v3, "ftp"

    const-string v4, "mAbortBrowsing ture mCache.isPathCached !!!"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1170
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mAsyncFragment:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setCompletedCondition(Z)V

    goto :goto_0

    .line 1165
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCache:Lcom/sec/android/app/myfiles/ftp/FTPCache;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mCurrentItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    invoke-interface {v4}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget v6, v6, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentSortBy:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget v7, v7, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentInOrder:I

    invoke-virtual {v2, v3, v4, v6, v7}, Lcom/sec/android/app/myfiles/ftp/FTPCache;->loadBuffer(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;II)V

    .line 1167
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCache:Lcom/sec/android/app/myfiles/ftp/FTPCache;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget v4, v4, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentSortBy:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget v6, v6, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentInOrder:I

    invoke-virtual {v3, v4, v6}, Lcom/sec/android/app/myfiles/ftp/FTPCache;->getBufferItems(II)Ljava/util/List;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentItems:Ljava/util/List;

    goto :goto_1

    .line 1174
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mAsyncFragment:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setCompletedCondition(Z)V

    .line 1176
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mCurrentItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCache:Lcom/sec/android/app/myfiles/ftp/FTPCache;

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mInitial:Z

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget v8, v8, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentSortBy:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget v9, v9, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentInOrder:I

    invoke-virtual/range {v2 .. v9}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->browse(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Lcom/sec/android/app/myfiles/ftp/FTPCache;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;II)V

    goto/16 :goto_0

    .line 1182
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mDoNotCheckConnection:Z

    if-nez v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->syncIsConnectionUp()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1184
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCache:Lcom/sec/android/app/myfiles/ftp/FTPCache;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mCurrentItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    invoke-interface {v4}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/myfiles/ftp/FTPCache;->isPathCached(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1186
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCache:Lcom/sec/android/app/myfiles/ftp/FTPCache;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mCurrentItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    invoke-interface {v4}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget v6, v6, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentSortBy:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget v7, v7, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentInOrder:I

    invoke-virtual {v2, v3, v4, v6, v7}, Lcom/sec/android/app/myfiles/ftp/FTPCache;->loadBuffer(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;II)V

    .line 1194
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCache:Lcom/sec/android/app/myfiles/ftp/FTPCache;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget v4, v4, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentSortBy:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget v6, v6, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentInOrder:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mCurrentItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    invoke-interface {v8}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v4, v6, v7, v8}, Lcom/sec/android/app/myfiles/ftp/FTPCache;->getBufferItems(IILcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentItems:Ljava/util/List;

    .line 1198
    invoke-static {}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getCurrentTime()J

    move-result-wide v16

    .line 1200
    .local v16, "currentTime":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCache:Lcom/sec/android/app/myfiles/ftp/FTPCache;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mCurrentItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    invoke-interface {v4}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/myfiles/ftp/FTPCache;->getLastUpdateTime(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)J

    move-result-wide v14

    .line 1202
    .local v14, "cachedTime":J
    const-wide/16 v2, -0x1

    cmp-long v2, v14, v2

    if-eqz v2, :cond_6

    sub-long v2, v16, v14

    const-wide/16 v6, 0x2710

    cmp-long v2, v2, v6

    if-lez v2, :cond_7

    .line 1205
    :cond_6
    const/4 v2, 0x1

    # getter for: Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->MODULE:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->access$100()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "path: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mCurrentItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    invoke-interface {v6}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", currentTime: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v16

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", cachedTime: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", diff: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sub-long v6, v16, v14

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 1207
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mBrowseHandler:Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mCurrentItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    invoke-virtual {v2, v3, v4, v6}, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->pushBrowseRequest(Landroid/content/Context;Lcom/sec/android/app/myfiles/ftp/FTPParams;Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V

    .line 1215
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mAsyncFragment:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setCompletedCondition(Z)V

    goto/16 :goto_0

    .line 1211
    :cond_7
    const/4 v2, 0x1

    # getter for: Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->MODULE:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->access$100()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "browse request has not been posted - time diff too small: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sub-long v6, v16, v14

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1219
    .end local v14    # "cachedTime":J
    .end local v16    # "currentTime":J
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mAsyncFragment:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setCompletedCondition(Z)V

    .line 1221
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v6, v2, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mCurrentItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v8, v2, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCache:Lcom/sec/android/app/myfiles/ftp/FTPCache;

    invoke-static {}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getCurrentTimeStr()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mInitial:Z

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget v12, v2, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentSortBy:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget v13, v2, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentInOrder:I

    invoke-virtual/range {v6 .. v13}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->browse(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Lcom/sec/android/app/myfiles/ftp/FTPCache;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;II)V

    goto/16 :goto_0

    .line 1228
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    new-instance v3, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable$2;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable$2;-><init>(Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 1266
    .end local v5    # "timestamp":Ljava/lang/String;
    .end local v18    # "previousPath":Ljava/lang/String;
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->popItem()V

    .line 1268
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mCurrentItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->openItem(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V

    .line 1270
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mAsyncFragment:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setCompletedCondition(Z)V

    goto/16 :goto_0

    .line 1276
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mAsyncFragment:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setCompletedCondition(Z)V

    goto/16 :goto_0
.end method
