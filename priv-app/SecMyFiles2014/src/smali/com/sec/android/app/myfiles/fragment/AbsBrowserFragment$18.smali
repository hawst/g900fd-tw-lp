.class Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$18;
.super Ljava/lang/Object;
.source "AbsBrowserFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onPrepareDialog(ILandroid/app/Dialog;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

.field final synthetic val$checkDoNotShow:Landroid/widget/CheckBox;

.field final synthetic val$dialogId:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;ILandroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 3009
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$18;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iput p2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$18;->val$dialogId:I

    iput-object p3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$18;->val$checkDoNotShow:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 3014
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$18;->val$dialogId:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$18;->val$dialogId:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 3016
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$18;->val$checkDoNotShow:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3017
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$18;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->bDoNotCheckDropBox:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$302(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;Z)Z

    .line 3022
    :cond_1
    :goto_0
    return-void

    .line 3019
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$18;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->bDoNotCheckDropBox:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$302(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;Z)Z

    goto :goto_0
.end method
