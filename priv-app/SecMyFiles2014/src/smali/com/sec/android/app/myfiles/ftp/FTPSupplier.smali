.class public Lcom/sec/android/app/myfiles/ftp/FTPSupplier;
.super Ljava/lang/Object;
.source "FTPSupplier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/ftp/FTPSupplier$FoldersFilesTuple;
    }
.end annotation


# instance fields
.field protected final mContext:Landroid/content/Context;

.field private mCurrentBrowseOperation:Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;

.field private mCurrentDownloadFolderOperation:Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;

.field private mCurrentSelectOperation:Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;

.field private final mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

.field private final mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

.field protected mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

.field protected final mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

.field private mSync:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/myfiles/ftp/FTPParams;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p3, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .param p4, "clientOperationListener"    # Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;

    .prologue
    const/4 v2, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 719
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mSync:Ljava/lang/Object;

    .line 55
    if-eqz p1, :cond_2

    if-eqz p3, :cond_2

    if-eqz p2, :cond_2

    .line 57
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mContext:Landroid/content/Context;

    .line 59
    iput-object p3, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .line 61
    iput-object p2, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .line 63
    invoke-static {}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->getInstance()Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;

    move-result-object v0

    .line 65
    .local v0, "factory":Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;
    if-eqz v0, :cond_1

    .line 67
    invoke-virtual {v0, p2}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->getFTPConnectionHandler(Lcom/sec/android/app/myfiles/ftp/FTPParams;)Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    .line 69
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    if-eqz v1, :cond_0

    .line 71
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v1, p4}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->setFtpClientOperationListener(Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;)V

    .line 75
    :cond_0
    new-instance v1, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mSync:Ljava/lang/Object;

    invoke-direct {v1, p1, v2, v3}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;-><init>(Landroid/content/Context;Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    .line 85
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    if-nez v1, :cond_3

    .line 87
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "IFTPConnectionHandler cannot be null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 79
    :cond_1
    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    .line 81
    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    goto :goto_0

    .line 93
    .end local v0    # "factory":Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    .line 97
    .restart local v0    # "factory":Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;
    :cond_3
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/ftp/FTPSupplier;)Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    return-object v0
.end method

.method protected static discoverLocalStructure([Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPSupplier$FoldersFilesTuple;
    .locals 14
    .param p0, "srcFiles"    # [Ljava/lang/String;
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    .line 669
    if-eqz p0, :cond_6

    array-length v11, p0

    if-lez v11, :cond_6

    .line 670
    new-instance v6, Ljava/util/Stack;

    invoke-direct {v6}, Ljava/util/Stack;-><init>()V

    .line 671
    .local v6, "folder":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/String;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 672
    .local v5, "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/ArrayDeque;

    invoke-direct {v3}, Ljava/util/ArrayDeque;-><init>()V

    .line 673
    .local v3, "discoverFolders":Ljava/util/ArrayDeque;, "Ljava/util/ArrayDeque<Ljava/lang/String;>;"
    const/4 v11, 0x0

    aget-object v11, p0, v11

    invoke-static {v11}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->getPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 674
    .local v10, "prefix":Ljava/lang/String;
    move-object v0, p0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v8, v0

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_2

    aget-object v9, v0, v7

    .line 675
    .local v9, "path":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 676
    .local v4, "file":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 677
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11, v10, p1}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->getPathWithoutPrefix(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 678
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v11}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 674
    :cond_0
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 679
    :cond_1
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 680
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 683
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v4    # "file":Ljava/io/File;
    .end local v9    # "path":Ljava/lang/String;
    :cond_2
    invoke-virtual {v3}, Ljava/util/ArrayDeque;->size()I

    move-result v11

    if-lez v11, :cond_5

    .line 684
    invoke-virtual {v3}, Ljava/util/ArrayDeque;->pop()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 685
    .restart local v9    # "path":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 686
    .restart local v4    # "file":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 687
    .local v2, "childs":[Ljava/io/File;
    if-eqz v2, :cond_2

    array-length v11, v2

    if-lez v11, :cond_2

    .line 688
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v8, v0

    const/4 v7, 0x0

    :goto_2
    if-ge v7, v8, :cond_2

    aget-object v1, v0, v7

    .line 689
    .local v1, "child":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-eqz v11, :cond_4

    .line 690
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v11}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    .line 691
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11, v10, p1}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->getPathWithoutPrefix(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 688
    :cond_3
    :goto_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 692
    :cond_4
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 693
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 698
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "child":Ljava/io/File;
    .end local v2    # "childs":[Ljava/io/File;
    .end local v4    # "file":Ljava/io/File;
    .end local v9    # "path":Ljava/lang/String;
    :cond_5
    new-instance v11, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$FoldersFilesTuple;

    invoke-static {v6}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->getArrayFromPaths(Ljava/util/Collection;)[Ljava/lang/String;

    move-result-object v12

    invoke-static {v5}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->getArrayFromPaths(Ljava/util/Collection;)[Ljava/lang/String;

    move-result-object v13

    invoke-direct {v11, v12, v13}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$FoldersFilesTuple;-><init>([Ljava/lang/String;[Ljava/lang/String;)V

    .line 702
    .end local v3    # "discoverFolders":Ljava/util/ArrayDeque;, "Ljava/util/ArrayDeque<Ljava/lang/String;>;"
    .end local v5    # "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v6    # "folder":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/String;>;"
    .end local v7    # "i$":I
    .end local v8    # "len$":I
    .end local v10    # "prefix":Ljava/lang/String;
    :goto_4
    return-object v11

    :cond_6
    const/4 v11, 0x0

    goto :goto_4
.end method

.method protected static getArrayFromItems(Ljava/util/Collection;)[Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 603
    .local p0, "items":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 605
    .local v2, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 606
    .local v1, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    invoke-interface {v1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 609
    .end local v1    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    :cond_0
    invoke-static {v2}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->getArrayFromPaths(Ljava/util/Collection;)[Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method protected static getArrayFromPaths(Ljava/util/Collection;)[Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 595
    .local p0, "paths":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    if-eqz p0, :cond_0

    .line 596
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {p0, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 598
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method protected static getPathWithoutPrefix(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;

    .prologue
    .line 626
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 627
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 628
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 629
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 631
    :cond_0
    const-string v1, ""

    invoke-virtual {p0, p1, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 632
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static getPrefix(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 613
    const-string v3, "/"

    invoke-virtual {p0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 614
    .local v2, "split":[Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 615
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_1

    .line 616
    if-lez v0, :cond_0

    .line 617
    const-string v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 619
    :cond_0
    aget-object v3, v2, v0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 615
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 621
    :cond_1
    const-string v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 622
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method protected static getSourceDir([Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "srcFiles"    # [Ljava/lang/String;

    .prologue
    .line 647
    if-eqz p0, :cond_3

    array-length v3, p0

    if-lez v3, :cond_3

    .line 648
    const/4 v3, 0x0

    aget-object v3, p0, v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 649
    .local v2, "split":[Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 650
    array-length v3, v2

    const/4 v4, 0x2

    if-le v3, v4, :cond_1

    .line 651
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 652
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_0

    .line 653
    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 654
    aget-object v3, v2, v0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 652
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 656
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 664
    .end local v0    # "i":I
    .end local v1    # "sb":Ljava/lang/StringBuilder;
    .end local v2    # "split":[Ljava/lang/String;
    :goto_1
    return-object v3

    .line 658
    .restart local v2    # "split":[Ljava/lang/String;
    :cond_1
    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    goto :goto_1

    .line 661
    :cond_2
    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    goto :goto_1

    .line 664
    .end local v2    # "split":[Ljava/lang/String;
    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method protected static getSourceDir([Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "srcFiles"    # [Ljava/lang/String;
    .param p1, "srcFolders"    # [Ljava/lang/String;

    .prologue
    .line 636
    invoke-static {p0}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->getSourceDir([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 637
    .local v0, "fromFiles":Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->getSourceDir([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 639
    .local v1, "fromFolders":Ljava/lang/String;
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 640
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-le v2, v3, :cond_0

    .line 642
    .end local v1    # "fromFolders":Ljava/lang/String;
    :goto_0
    return-object v1

    .restart local v1    # "fromFolders":Ljava/lang/String;
    :cond_0
    move-object v1, v0

    .line 640
    goto :goto_0

    .line 642
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abortCurrentOperation()V
    .locals 1

    .prologue
    .line 548
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mCurrentBrowseOperation:Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;

    if-eqz v0, :cond_0

    .line 549
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mCurrentBrowseOperation:Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->abortBrowsing()V

    .line 550
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->abortBrowse()V

    .line 552
    :cond_0
    return-void
.end method

.method public browse(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Lcom/sec/android/app/myfiles/ftp/FTPCache;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;II)V
    .locals 8
    .param p1, "item"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .param p2, "cache"    # Lcom/sec/android/app/myfiles/ftp/FTPCache;
    .param p3, "timestamp"    # Ljava/lang/String;
    .param p4, "initialBrowse"    # Z
    .param p5, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .param p6, "sortBy"    # I
    .param p7, "order"    # I

    .prologue
    .line 145
    if-eqz p5, :cond_0

    .line 146
    :goto_0
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move v5, p6

    move v6, p7

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/FTPCache;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;IIZ)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mCurrentBrowseOperation:Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mCurrentBrowseOperation:Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 148
    return-void

    .line 145
    :cond_0
    iget-object p5, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    goto :goto_0
.end method

.method public closeConnection(Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mCurrentBrowseOperation:Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mCurrentBrowseOperation:Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->cancel(Z)Z

    .line 118
    :cond_0
    if-eqz p1, :cond_1

    .line 119
    :goto_0
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/DisconnectOperation;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/myfiles/ftp/operation/DisconnectOperation;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/DisconnectOperation;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 120
    return-void

    .line 118
    :cond_1
    iget-object p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    goto :goto_0
.end method

.method public copyFromFtpToLocal(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 6
    .param p1, "file"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .param p2, "target"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .prologue
    .line 487
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 488
    if-eqz p3, :cond_1

    .line 489
    :goto_0
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mContext:Landroid/content/Context;

    const v3, 0x7f0b00ac

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v5

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mCurrentDownloadFolderOperation:Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;

    .line 491
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mCurrentDownloadFolderOperation:Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    const/4 v3, 0x0

    new-instance v4, Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    invoke-direct {v4, p1, p2}, Lcom/sec/android/app/myfiles/ftp/DownloadItem;-><init>(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/lang/String;)V

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 495
    :cond_0
    return-void

    .line 488
    :cond_1
    iget-object p3, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    goto :goto_0
.end method

.method public copyFromFtpToLocal(Ljava/util/Collection;Ljava/lang/String;)V
    .locals 1
    .param p2, "target"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 471
    .local p1, "items":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->copyFromFtpToLocal(Ljava/util/Collection;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 472
    return-void
.end method

.method public copyFromFtpToLocal(Ljava/util/Collection;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 1
    .param p2, "target"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 475
    .local p1, "items":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->downloadItems(Ljava/util/Collection;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 476
    return-void
.end method

.method public copyFromLocalToFtp(Ljava/util/Collection;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 2
    .param p2, "targetParent"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 415
    .local p1, "srcFiles":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->getArrayFromPaths(Ljava/util/Collection;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p2, v1, p3}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->uploadFromLocalToFtp([Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 416
    return-void
.end method

.method public copyFromLocalToFtp([Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 1
    .param p1, "srcFiles"    # [Ljava/lang/String;
    .param p2, "targetParent"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .prologue
    .line 431
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->uploadFromLocalToFtp([Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 432
    return-void
.end method

.method public copyItemsFromLocalToFtp(Ljava/util/Collection;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 2
    .param p2, "targetParent"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 423
    .local p1, "srcItems":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->getArrayFromItems(Ljava/util/Collection;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p2, v1, p3}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->uploadFromLocalToFtp([Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 424
    return-void
.end method

.method public copySelf(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 8
    .param p2, "target"    # Ljava/lang/String;
    .param p3, "src"    # Ljava/lang/String;
    .param p4, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/DownloadItem;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 524
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/DownloadItem;>;"
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 526
    if-eqz p4, :cond_1

    .line 527
    :goto_0
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mContext:Landroid/content/Context;

    const v3, 0x7f0b00ac

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    const-string v5, ""

    move-object v2, p4

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mCurrentDownloadFolderOperation:Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;

    .line 529
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mCurrentDownloadFolderOperation:Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    invoke-interface {p1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 533
    :cond_0
    return-void

    .line 526
    :cond_1
    iget-object p4, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    goto :goto_0
.end method

.method public copyToFtp(Ljava/util/List;Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 11
    .param p2, "targetParams"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p3, "target"    # Ljava/lang/String;
    .param p4, "src"    # Ljava/lang/String;
    .param p5, "move"    # Z
    .param p6, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/DownloadItem;",
            ">;",
            "Lcom/sec/android/app/myfiles/ftp/FTPParams;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 536
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/DownloadItem;>;"
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    if-eqz p3, :cond_0

    .line 537
    if-eqz p6, :cond_1

    .line 538
    :goto_0
    new-instance v1, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mContext:Landroid/content/Context;

    const v4, 0x7f0b00ac

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    const-string v6, ""

    move-object/from16 v3, p6

    move-object v7, p3

    move-object v8, p2

    move-object v9, p4

    move/from16 v10, p5

    invoke-direct/range {v1 .. v10}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Z)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mCurrentDownloadFolderOperation:Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;

    .line 541
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mCurrentDownloadFolderOperation:Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    invoke-interface {p1, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 545
    :cond_0
    return-void

    .line 537
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    move-object/from16 p6, v0

    goto :goto_0
.end method

.method public createDirectory(Ljava/lang/String;)V
    .locals 8
    .param p1, "newDirPath"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 158
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 159
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mContext:Landroid/content/Context;

    const v3, 0x7f0b0019

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mContext:Landroid/content/Context;

    const v5, 0x7f0b009e

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$1;-><init>(Lcom/sec/android/app/myfiles/ftp/FTPSupplier;)V

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/FTPAction;Z)V

    .line 186
    .local v0, "o":Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 187
    .local v7, "arg":Landroid/os/Bundle;
    const-string v1, "title"

    invoke-virtual {v7, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const-string v1, "create_dir"

    invoke-virtual {v7, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/os/Bundle;

    aput-object v7, v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 191
    .end local v0    # "o":Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;
    .end local v7    # "arg":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method public deleteFiles(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 256
    if-eqz p1, :cond_1

    .line 257
    const-string v2, "recur_disc_files"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "recur_disc_folders"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 259
    const-string v2, "recur_disc_files"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 260
    .local v0, "files":[Ljava/lang/String;
    const-string v2, "recur_disc_folders"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 261
    .local v1, "folders":[Ljava/lang/String;
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    array-length v2, v0

    if-gtz v2, :cond_0

    array-length v2, v1

    if-lez v2, :cond_1

    .line 262
    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->deleteFiles([Ljava/lang/String;[Ljava/lang/String;)V

    .line 266
    .end local v0    # "files":[Ljava/lang/String;
    .end local v1    # "folders":[Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public deleteFiles(Ljava/util/List;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 8
    .param p2, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;",
            "Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 226
    .local p1, "filesToDelete":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 227
    const-string v1, "MyFiles"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AbsFTPSupplier deleteFiles: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    if-eqz p2, :cond_1

    .line 229
    :goto_0
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0020

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mContext:Landroid/content/Context;

    const v5, 0x7f0b006b

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mContext:Landroid/content/Context;

    const v6, 0x7f0b00c7

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 232
    .local v0, "discover":Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    invoke-interface {p1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 235
    .end local v0    # "discover":Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;
    :cond_0
    return-void

    .line 228
    :cond_1
    iget-object p2, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    goto :goto_0
.end method

.method public deleteFiles([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 8
    .param p1, "folders"    # [Ljava/lang/String;
    .param p2, "files"    # [Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 275
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    array-length v1, p2

    array-length v2, p1

    add-int/2addr v1, v2

    if-lez v1, :cond_0

    .line 276
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mContext:Landroid/content/Context;

    const v3, 0x7f0b0020

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mContext:Landroid/content/Context;

    const v4, 0x7f0b006b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mContext:Landroid/content/Context;

    const v5, 0x7f0b009f

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$2;

    invoke-direct {v5, p0}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$2;-><init>(Lcom/sec/android/app/myfiles/ftp/FTPSupplier;)V

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/FTPAction;Z)V

    .line 309
    .local v0, "o":Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 310
    .local v7, "arg":Landroid/os/Bundle;
    const-string v1, "recur_disc_files"

    invoke-virtual {v7, v1, p2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 311
    const-string v1, "recur_disc_folders"

    invoke-virtual {v7, v1, p1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 312
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/os/Bundle;

    aput-object v7, v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 314
    .end local v0    # "o":Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;
    .end local v7    # "arg":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method protected downloadItems(Ljava/util/Collection;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 6
    .param p2, "targetDir"    # Ljava/lang/String;
    .param p3, "move"    # Z
    .param p4, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 498
    .local p1, "items":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 500
    .local v0, "downloadItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/ftp/DownloadItem;>;"
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 501
    .local v3, "sb":Ljava/lang/StringBuilder;
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 502
    .local v2, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 503
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 504
    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 505
    invoke-interface {v2}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 506
    new-instance v4, Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v2, v5}, Lcom/sec/android/app/myfiles/ftp/DownloadItem;-><init>(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 509
    .end local v2    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    :cond_0
    invoke-virtual {p0, v0, p3, p4}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->downloadItems(Ljava/util/List;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 510
    return-void
.end method

.method protected downloadItems(Ljava/util/List;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 7
    .param p2, "move"    # Z
    .param p3, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/DownloadItem;",
            ">;Z",
            "Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 513
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/DownloadItem;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 514
    if-eqz p3, :cond_1

    .line 515
    :goto_0
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mContext:Landroid/content/Context;

    const v3, 0x7f0b00ac

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    const-string v5, ""

    move-object v2, p3

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mCurrentDownloadFolderOperation:Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;

    .line 517
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mCurrentDownloadFolderOperation:Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    invoke-interface {p1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 521
    :cond_0
    return-void

    .line 514
    :cond_1
    iget-object p3, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    goto :goto_0
.end method

.method public getParams()Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    return-object v0
.end method

.method public moveFiles([Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 9
    .param p1, "from"    # [Ljava/lang/String;
    .param p2, "to"    # [Ljava/lang/String;
    .param p3, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .prologue
    const/4 v6, 0x0

    .line 323
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    array-length v1, p1

    if-lez v1, :cond_0

    array-length v1, p2

    if-lez v1, :cond_0

    array-length v1, p1

    array-length v2, p2

    if-ne v1, v2, :cond_0

    .line 325
    if-eqz p3, :cond_1

    move-object v8, p3

    .line 326
    .local v8, "properListener":Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    :goto_0
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mContext:Landroid/content/Context;

    const v3, 0x7f0b00ca

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mContext:Landroid/content/Context;

    const v5, 0x7f0b00cb

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$3;

    invoke-direct {v5, p0, v8}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$3;-><init>(Lcom/sec/android/app/myfiles/ftp/FTPSupplier;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/FTPAction;Z)V

    .line 359
    .local v0, "o":Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 360
    .local v7, "arg":Landroid/os/Bundle;
    const-string v1, "rename_from"

    invoke-virtual {v7, v1, p1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 361
    const-string v1, "rename_to"

    invoke-virtual {v7, v1, p2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 362
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/os/Bundle;

    aput-object v7, v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 364
    .end local v0    # "o":Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;
    .end local v7    # "arg":Landroid/os/Bundle;
    .end local v8    # "properListener":Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    :cond_0
    return-void

    .line 325
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    goto :goto_0
.end method

.method public moveFromFtpToLocal(Ljava/util/Collection;Ljava/lang/String;)V
    .locals 1
    .param p2, "target"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 479
    .local p1, "items":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->moveFromFtpToLocal(Ljava/util/Collection;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 480
    return-void
.end method

.method public moveFromFtpToLocal(Ljava/util/Collection;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 1
    .param p2, "target"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 483
    .local p1, "items":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->downloadItems(Ljava/util/Collection;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 484
    return-void
.end method

.method public moveFromLocalToFtp(Ljava/util/Collection;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 2
    .param p2, "targetParent"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 419
    .local p1, "srcFiles":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->getArrayFromPaths(Ljava/util/Collection;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, p2, v1, p3}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->uploadFromLocalToFtp([Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 420
    return-void
.end method

.method public moveFromLocalToFtp([Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 1
    .param p1, "srcFiles"    # [Ljava/lang/String;
    .param p2, "targetParent"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .prologue
    .line 435
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->uploadFromLocalToFtp([Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 436
    return-void
.end method

.method public moveItemsFromLocalToFtp(Ljava/util/Collection;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 2
    .param p2, "targetParent"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 427
    .local p1, "srcItems":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->getArrayFromItems(Ljava/util/Collection;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, p2, v1, p3}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->uploadFromLocalToFtp([Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 428
    return-void
.end method

.method public openConnection(Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .prologue
    .line 107
    if-eqz p1, :cond_0

    .line 108
    :goto_0
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/ConnectOperation;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/myfiles/ftp/operation/ConnectOperation;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/ConnectOperation;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 109
    return-void

    .line 107
    :cond_0
    iget-object p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    goto :goto_0
.end method

.method public openItem(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V
    .locals 1
    .param p1, "item"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .prologue
    .line 123
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public renameFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "from"    # Ljava/lang/String;
    .param p2, "to"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 373
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 374
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mContext:Landroid/content/Context;

    const v3, 0x7f0b0023

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mContext:Landroid/content/Context;

    const v5, 0x7f0b00a0

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$4;

    invoke-direct {v5, p0}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$4;-><init>(Lcom/sec/android/app/myfiles/ftp/FTPSupplier;)V

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/FTPAction;Z)V

    .line 400
    .local v0, "o":Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 401
    .local v7, "arg":Landroid/os/Bundle;
    const-string v1, "title"

    invoke-virtual {v7, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    const-string v1, "rename_from"

    invoke-virtual {v7, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    const-string v1, "rename_to"

    invoke-virtual {v7, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/os/Bundle;

    aput-object v7, v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 406
    .end local v0    # "o":Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;
    .end local v7    # "arg":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method public selectItem(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/lang/String;)V
    .locals 6
    .param p1, "file"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .param p2, "target"    # Ljava/lang/String;

    .prologue
    .line 555
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 556
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mContext:Landroid/content/Context;

    const v4, 0x7f0b00ac

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mCurrentSelectOperation:Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;

    .line 559
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mCurrentSelectOperation:Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    const/4 v3, 0x0

    new-instance v4, Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    invoke-direct {v4, p1, p2}, Lcom/sec/android/app/myfiles/ftp/DownloadItem;-><init>(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/lang/String;)V

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 563
    :cond_0
    return-void
.end method

.method public selectItems(Ljava/util/List;Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 567
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    .local p2, "targets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 569
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v9

    .line 571
    .local v9, "size":I
    if-lez v9, :cond_1

    .line 573
    new-array v7, v9, [Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    .line 575
    .local v7, "downloadItems":[Lcom/sec/android/app/myfiles/ftp/DownloadItem;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v9, :cond_0

    .line 577
    new-instance v2, Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    invoke-virtual {p2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v2, v0, v1}, Lcom/sec/android/app/myfiles/ftp/DownloadItem;-><init>(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/lang/String;)V

    aput-object v2, v7, v8

    .line 575
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 581
    :cond_0
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mContext:Landroid/content/Context;

    const v4, 0x7f0b00ac

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    const-string v5, ""

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mCurrentSelectOperation:Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;

    .line 584
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mCurrentSelectOperation:Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-virtual {v0, v1, v7}, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 591
    .end local v7    # "downloadItems":[Lcom/sec/android/app/myfiles/ftp/DownloadItem;
    .end local v8    # "i":I
    .end local v9    # "size":I
    :cond_1
    return-void
.end method

.method public supportsMdtm()Z
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncSupportsMdtm()Z

    move-result v0

    return v0
.end method

.method public syncGetTimestamp(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncPrepareConnection()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v0, p1}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->getTimestamp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 201
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public syncPrepareConnection()Z
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncPrepareConnection()Z

    move-result v0

    .line 215
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected uploadFromLocalToFtp([Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 8
    .param p1, "srcFiles"    # [Ljava/lang/String;
    .param p2, "targetParent"    # Ljava/lang/String;
    .param p3, "move"    # Z
    .param p4, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .prologue
    .line 440
    invoke-static {p1, p2}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->discoverLocalStructure([Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPSupplier$FoldersFilesTuple;

    move-result-object v7

    .line 442
    .local v7, "tpl":Lcom/sec/android/app/myfiles/ftp/FTPSupplier$FoldersFilesTuple;
    if-eqz v7, :cond_0

    if-eqz p1, :cond_0

    .line 444
    iget-object v1, v7, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$FoldersFilesTuple;->mFolders:[Ljava/lang/String;

    iget-object v2, v7, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$FoldersFilesTuple;->mFiles:[Ljava/lang/String;

    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->getSourceDir([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v3, p2

    move v5, p3

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->uploadFromLocalToFtp([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 448
    :cond_0
    return-void
.end method

.method protected uploadFromLocalToFtp([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 11
    .param p1, "folders"    # [Ljava/lang/String;
    .param p2, "files"    # [Ljava/lang/String;
    .param p3, "target"    # Ljava/lang/String;
    .param p4, "src"    # Ljava/lang/String;
    .param p5, "move"    # Z
    .param p6, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .prologue
    .line 451
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-eqz p4, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 452
    if-eqz p6, :cond_1

    .line 453
    :goto_0
    new-instance v1, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    if-eqz p5, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0021

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    :goto_1
    const-string v5, ""

    const/4 v9, 0x0

    move-object/from16 v3, p6

    move-object v6, p3

    move-object v7, p4

    move/from16 v8, p5

    invoke-direct/range {v1 .. v9}, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/FTPParams;)V

    .line 456
    .local v1, "uo":Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 457
    .local v10, "arg":Landroid/os/Bundle;
    const-string v2, "recur_disc_folders"

    invoke-virtual {v10, v2, p1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 458
    const-string v2, "recur_disc_files"

    invoke-virtual {v10, v2, p2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 459
    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/os/Bundle;

    const/4 v4, 0x0

    aput-object v10, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 461
    .end local v1    # "uo":Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;
    .end local v10    # "arg":Landroid/os/Bundle;
    :cond_0
    return-void

    .line 452
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    move-object/from16 p6, v0

    goto :goto_0

    .line 453
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0022

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method public visitItemsRecursively(Ljava/util/List;Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 8
    .param p2, "visitor"    # Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;
    .param p3, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;",
            "Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;",
            "Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 244
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 245
    const-string v1, "MyFiles"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AbsFTPSupplier computeDirSize: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    if-eqz p3, :cond_1

    .line 247
    :goto_0
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    const-string v4, "Computing size"

    const-string v5, "Processing"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mContext:Landroid/content/Context;

    const v3, 0x7f0b00c7

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object v2, p3

    move-object v3, p2

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 250
    .local v0, "discover":Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    invoke-interface {p1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 253
    .end local v0    # "discover":Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;
    :cond_0
    return-void

    .line 246
    :cond_1
    iget-object p3, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    goto :goto_0
.end method
