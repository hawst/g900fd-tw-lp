.class Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$34;
.super Ljava/lang/Object;
.source "AbsBrowserFragment.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V
    .locals 0

    .prologue
    .line 6312
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$34;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 6316
    const-string v2, "AbsBrowserFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mProgressHandler msg.what : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 6318
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 6344
    :cond_0
    :goto_0
    return v0

    .line 6322
    :pswitch_0
    const/4 v1, 0x2

    const-string v2, "AbsBrowserFragment"

    const-string v3, "Call mProgressHandler"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 6324
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$34;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mWaitProgressDlg:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$1000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    move-result-object v1

    if-nez v1, :cond_1

    .line 6326
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$34;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    new-instance v2, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$34;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mWaitProgressDlg:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    invoke-static {v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$1002(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;Lcom/sec/android/app/myfiles/utils/MyFilesDialog;)Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    .line 6329
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$34;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6330
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$34;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mWaitProgressDlg:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$1000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$34;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    const v3, 0x7f0b006b

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->showWaitProgressDialog(Ljava/lang/String;Z)V

    goto :goto_0

    .line 6336
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$34;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mProgressHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$1100(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 6337
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$34;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mWaitProgressDlg:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$1000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 6338
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$34;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mWaitProgressDlg:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$1000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->showWaitProgressDialog(Ljava/lang/String;Z)V

    goto :goto_0

    .line 6318
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
