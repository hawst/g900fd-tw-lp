.class public Lcom/sec/android/app/myfiles/view/ListDetailTextContainer;
.super Landroid/widget/RelativeLayout;
.source "ListDetailTextContainer.java"


# instance fields
.field private mDetailTextHorizontalSpace:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    const/4 v0, 0x1

    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/myfiles/view/ListDetailTextContainer;->mDetailTextHorizontalSpace:I

    .line 39
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 9
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/4 v7, 0x0

    .line 44
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 46
    const v5, 0x7f0f005a

    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/view/ListDetailTextContainer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 48
    .local v2, "sizeOrItemCountText":Landroid/view/View;
    const v5, 0x7f0f0069

    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/view/ListDetailTextContainer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 50
    .local v1, "lastModifiedText":Landroid/view/View;
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 52
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 54
    .local v3, "viewWidth":I
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    sub-int v5, v3, v5

    iget v6, p0, Lcom/sec/android/app/myfiles/view/ListDetailTextContainer;->mDetailTextHorizontalSpace:I

    sub-int v4, v5, v6

    .line 56
    .local v4, "w":I
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 58
    .local v0, "h":I
    or-int v5, v4, v8

    or-int v6, v0, v8

    invoke-virtual {v2, v5, v6}, Landroid/view/View;->measure(II)V

    .line 60
    invoke-virtual {v2, v7, v7, v4, v0}, Landroid/view/View;->layout(IIII)V

    .line 62
    .end local v0    # "h":I
    .end local v3    # "viewWidth":I
    .end local v4    # "w":I
    :cond_0
    return-void
.end method
