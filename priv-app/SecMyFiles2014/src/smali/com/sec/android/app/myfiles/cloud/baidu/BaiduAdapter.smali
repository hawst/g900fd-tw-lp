.class public Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
.source "BaiduAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$ConcreteProgressListener;,
        Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;
    }
.end annotation


# static fields
.field private static final MAX_THUMBNAIL_TASK_NUMBER:I = 0xa

.field private static final TAG:Ljava/lang/String; = "BaiduAdapter"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFragmentId:I

.field private mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

.field private mSavedSearchKeyWord:Ljava/lang/String;

.field private mSelectedItemBackground:I

.field private mThumbnailCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field mThumbnailWorkerTasks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;",
            ">;"
        }
    .end annotation
.end field

.field private navi:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I
    .param p5, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    .param p6, "fragmentId"    # I

    .prologue
    const/4 v5, 0x0

    .line 90
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 78
    iput-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mSavedSearchKeyWord:Ljava/lang/String;

    .line 79
    const/4 v2, 0x2

    const/16 v3, 0x55

    const/16 v4, 0x6a

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mSelectedItemBackground:I

    .line 82
    iput-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 548
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mThumbnailWorkerTasks:Ljava/util/ArrayList;

    .line 92
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mContext:Landroid/content/Context;

    .line 93
    iput p6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mFragmentId:I

    .line 95
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v2

    const-wide/16 v4, 0x400

    div-long/2addr v2, v4

    long-to-int v1, v2

    .line 97
    .local v1, "maxMemory":I
    div-int/lit8 v0, v1, 0x8

    .line 99
    .local v0, "cacheSize":I
    new-instance v2, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$1;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$1;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;I)V

    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mThumbnailCache:Landroid/util/LruCache;

    .line 109
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.feature.hovering_ui"

    invoke-static {v2, v3}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 111
    new-instance v2, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 113
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;)Landroid/util/LruCache;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mThumbnailCache:Landroid/util/LruCache;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mIsDragMode:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    .prologue
    .line 68
    iget v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mSelectedItemBackground:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mIsDragMode:Z

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mIsDragMode:Z

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;)Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mDropListener:Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;)Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mDropListener:Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mIsDragMode:Z

    return v0
.end method

.method private retrieveThumbnailFromFile(Ljava/lang/String;Landroid/widget/ImageView;)Z
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "displayImageView"    # Landroid/widget/ImageView;

    .prologue
    .line 555
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->getFileThumbnailFolderName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "large.jpg"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 556
    .local v0, "ThumbnailCacheFilePath":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 557
    .local v2, "thumbFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 558
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 559
    .local v1, "bitmapImage":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 560
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mThumbnailCache:Landroid/util/LruCache;

    invoke-virtual {v3, v0, v1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 561
    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 562
    const/4 v3, 0x1

    .line 565
    .end local v1    # "bitmapImage":Landroid/graphics/Bitmap;
    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 18
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 119
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;

    .line 121
    .local v11, "vh":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    invoke-super/range {p0 .. p3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 129
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    if-eqz v12, :cond_0

    .line 132
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 135
    :cond_0
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v12, :cond_1

    .line 137
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 138
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 141
    :cond_1
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 142
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 144
    const-string v12, "_data"

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 146
    .local v4, "filePath":Ljava/lang/String;
    const/16 v12, 0x2f

    invoke-virtual {v4, v12}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v12

    add-int/lit8 v12, v12, 0x1

    invoke-virtual {v4, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 148
    .local v3, "fileName":Ljava/lang/String;
    const-string v12, "format"

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 151
    .local v6, "format":I
    const/16 v12, 0x3001

    if-ne v6, v12, :cond_c

    .line 153
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v12}, Landroid/widget/TextView;->getVisibility()I

    move-result v12

    const/16 v13, 0x8

    if-ne v12, v13, :cond_2

    .line 154
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 155
    :cond_2
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mHasThumbnail:Z

    .line 158
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mSelectModeFrom:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_3

    .line 160
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mSelectionType:I

    if-nez v12, :cond_b

    .line 162
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectRadioButton:Landroid/widget/RadioButton;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 171
    :cond_3
    :goto_0
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_4

    .line 173
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v12, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const v13, 0x7f0200af

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 179
    :cond_4
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v12, :cond_5

    .line 180
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 237
    :cond_5
    :goto_1
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mViewMode:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_7

    .line 242
    const/16 v12, 0x3001

    if-ne v6, v12, :cond_15

    .line 244
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 246
    .local v10, "sb":Ljava/lang/StringBuilder;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->navi:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    .line 256
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0c0002

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->navi:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    invoke-virtual {v14, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->getItemsInFolder(Ljava/lang/String;)I

    move-result v14

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->navi:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->getItemsInFolder(Ljava/lang/String;)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v12, v13, v14, v15}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 265
    .end local v10    # "sb":Ljava/lang/StringBuilder;
    .local v2, "content":Ljava/lang/String;
    :goto_2
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    if-eqz v12, :cond_6

    .line 266
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 270
    :cond_6
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mContext:Landroid/content/Context;

    const-string v13, "date_modified"

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    invoke-static {v12, v14, v15}, Lcom/sec/android/app/myfiles/utils/Utils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    .line 272
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    if-eqz v12, :cond_7

    .line 273
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 277
    .end local v2    # "content":Ljava/lang/String;
    :cond_7
    const/16 v12, 0x3001

    if-eq v6, v12, :cond_9

    .line 279
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v5

    .line 282
    .local v5, "fileType":I
    new-instance v7, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$2;

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v5, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$2;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;ILjava/lang/String;)V

    .line 363
    .local v7, "hoverListener":Landroid/view/View$OnHoverListener;
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mSelectModeFrom:I

    const/4 v13, 0x1

    if-eq v12, v13, :cond_9

    .line 366
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mViewMode:I

    if-eqz v12, :cond_8

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mViewMode:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_16

    .line 368
    :cond_8
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v12, v7}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 379
    .end local v5    # "fileType":I
    .end local v7    # "hoverListener":Landroid/view/View$OnHoverListener;
    :cond_9
    :goto_3
    new-instance v12, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$3;

    move-object/from16 v0, p0

    invoke-direct {v12, v0, v6, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$3;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;ILjava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 512
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mFragmentId:I

    const/16 v13, 0x20

    if-ne v12, v13, :cond_a

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mSavedSearchKeyWord:Ljava/lang/String;

    if-eqz v12, :cond_a

    .line 513
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v12}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v12

    invoke-interface {v12}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    .line 514
    .local v9, "s":Ljava/lang/String;
    const-string v12, "BaiduAdapter"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "SearhKeyWord="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 515
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mSavedSearchKeyWord:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v13}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->getColoredSpannableString(Ljava/lang/CharSequence;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 517
    .end local v9    # "s":Ljava/lang/String;
    :cond_a
    return-void

    .line 164
    :cond_b
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mSelectionType:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_3

    .line 166
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto/16 :goto_0

    .line 187
    :cond_c
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mSelectModeFrom:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_d

    .line 189
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mSelectionType:I

    if-nez v12, :cond_10

    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v12}, Landroid/widget/RadioButton;->getVisibility()I

    move-result v12

    const/16 v13, 0x8

    if-ne v12, v13, :cond_10

    .line 192
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectRadioButton:Landroid/widget/RadioButton;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 205
    :cond_d
    :goto_4
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_e

    .line 208
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowFileExtensionStatus()Z

    move-result v12

    if-eqz v12, :cond_13

    .line 210
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v12, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    :cond_e
    :goto_5
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v12, :cond_f

    .line 230
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 233
    :cond_f
    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->loadThumbnail(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 194
    :cond_10
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mSelectionType:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_11

    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v12}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v12

    const/16 v13, 0x8

    if-ne v12, v13, :cond_11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mContext:Landroid/content/Context;

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v12

    if-nez v12, :cond_12

    :cond_11
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mSelectionType:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_d

    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v12}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v12

    const/16 v13, 0x8

    if-ne v12, v13, :cond_d

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mContext:Landroid/content/Context;

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentView()I

    move-result v12

    const/4 v13, 0x2

    if-ne v12, v13, :cond_d

    .line 201
    :cond_12
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_4

    .line 214
    :cond_13
    const/16 v12, 0x2e

    invoke-virtual {v3, v12}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v8

    .line 217
    .local v8, "lastDot":I
    if-lez v8, :cond_14

    .line 219
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const/4 v13, 0x0

    invoke-virtual {v3, v13, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 223
    :cond_14
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v12, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 262
    .end local v8    # "lastDot":I
    :cond_15
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mContext:Landroid/content/Context;

    const-string v13, "_size"

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    invoke-static {v12, v14, v15}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "content":Ljava/lang/String;
    goto/16 :goto_2

    .line 372
    .end local v2    # "content":Ljava/lang/String;
    .restart local v5    # "fileType":I
    .restart local v7    # "hoverListener":Landroid/view/View$OnHoverListener;
    :cond_16
    iget-object v12, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v12, v7}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto/16 :goto_3
.end method

.method protected getColoredSpannableString(Ljava/lang/CharSequence;Ljava/lang/String;)Landroid/text/SpannableString;
    .locals 7
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "queryText"    # Ljava/lang/String;

    .prologue
    .line 520
    if-nez p1, :cond_0

    .line 521
    const-string p1, ""

    .line 523
    :cond_0
    if-eqz p2, :cond_2

    .line 524
    const-string v4, "[  ]+"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 525
    .local v2, "p":Ljava/util/regex/Pattern;
    invoke-virtual {v2, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 526
    const-string v4, " "

    invoke-virtual {p2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 527
    const/4 v4, 0x1

    invoke-virtual {p2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    .line 529
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 531
    .local v0, "index":I
    const/4 v4, -0x1

    if-eq v0, v4, :cond_2

    .line 532
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080033

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 534
    .local v1, "matchedColor":I
    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 536
    .local v3, "span":Landroid/text/SpannableString;
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v4, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, v0

    const/16 v6, 0x21

    invoke-virtual {v3, v4, v0, v5, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 541
    .end local v0    # "index":I
    .end local v1    # "matchedColor":I
    .end local v2    # "p":Ljava/util/regex/Pattern;
    .end local v3    # "span":Landroid/text/SpannableString;
    :goto_0
    return-object v3

    :cond_2
    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected getContentType()I
    .locals 1

    .prologue
    .line 762
    const/16 v0, 0x1f

    return v0
.end method

.method public getThumbnailCache()Landroid/util/LruCache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 776
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mThumbnailCache:Landroid/util/LruCache;

    return-object v0
.end method

.method protected loadThumbnail(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Landroid/database/Cursor;)V
    .locals 8
    .param p1, "vh"    # Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v7, 0x1

    .line 573
    const-string v5, "_data"

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 574
    .local v2, "filePath":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-gtz v5, :cond_1

    .line 644
    :cond_0
    :goto_0
    return-void

    .line 577
    :cond_1
    invoke-static {v2}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v3

    .line 578
    .local v3, "fileType":I
    const/4 v1, 0x0

    .line 580
    .local v1, "displayImageView":Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->isVideoFileType(I)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 582
    :cond_2
    iget v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mViewMode:I

    if-eqz v5, :cond_3

    iget v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mViewMode:I

    if-ne v5, v7, :cond_4

    .line 583
    :cond_3
    iget-object v1, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    .line 588
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mThumbnailCache:Landroid/util/LruCache;

    invoke-virtual {v5, v2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 590
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_5

    .line 591
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 585
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_4
    iget-object v1, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    goto :goto_1

    .line 594
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_5
    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->retrieveThumbnailFromFile(Ljava/lang/String;Landroid/widget/ImageView;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 597
    const/4 v4, 0x0

    .line 599
    .local v4, "task":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 600
    new-instance v4, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;

    .end local v4    # "task":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;
    const/4 v5, 0x2

    invoke-direct {v4, p0, v1, v5, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;Landroid/widget/ImageView;ILjava/lang/String;)V

    .line 604
    .restart local v4    # "task":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;
    :goto_2
    iput-object v4, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnailWorker:Landroid/os/AsyncTask;

    .line 605
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mThumbnailWorkerTasks:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 607
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mThumbnailWorkerTasks:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/16 v6, 0xa

    if-ge v5, v6, :cond_0

    .line 608
    iput-boolean v7, v4, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mStarted:Z

    .line 609
    sget-object v5, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v6, 0x0

    new-array v6, v6, [Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$IdPath;

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 602
    :cond_6
    new-instance v4, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;

    .end local v4    # "task":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;
    const/4 v5, 0x3

    invoke-direct {v4, p0, v1, v5, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;Landroid/widget/ImageView;ILjava/lang/String;)V

    .restart local v4    # "task":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;
    goto :goto_2

    .line 642
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v4    # "task":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;
    :cond_7
    iget-object v5, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/MediaFile;->getSmallIcon(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public setSearchKeyword(Ljava/lang/String;)V
    .locals 0
    .param p1, "mSavedSearchKeyWord"    # Ljava/lang/String;

    .prologue
    .line 545
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mSavedSearchKeyWord:Ljava/lang/String;

    .line 546
    return-void
.end method

.method public stopThumbnailsDownload()V
    .locals 1

    .prologue
    .line 551
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mThumbnailWorkerTasks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 552
    return-void
.end method
