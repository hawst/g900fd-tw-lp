.class public Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;
.super Ljava/lang/Object;
.source "BaiduActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BaiduAPIHelper"
.end annotation


# static fields
.field public static final baiduCloudServerPath:Ljava/lang/String; = "/apps/samsung"

.field private static instance:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

.field public static mSignInRequested:Z


# instance fields
.field bAPI:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

.field context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->mSignInRequested:Z

    .line 57
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->instance:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->context:Landroid/content/Context;

    .line 53
    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->bAPI:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    .line 71
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->context:Landroid/content/Context;

    .line 72
    invoke-static {p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->bAPI:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    .line 73
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    sget-object v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->instance:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    if-nez v0, :cond_0

    .line 64
    new-instance v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->instance:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    .line 66
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->instance:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    return-object v0
.end method

.method public static requestGetToken(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 77
    const-string v1, "BaiduAPIHelper"

    const-string v2, "requestGetToken"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.cloudagent.ACTION_REQUEST_TOKEN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 79
    .local v0, "gettokenintent":Landroid/content/Intent;
    const-string v1, "packageName"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 80
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 81
    return-void
.end method

.method public static requestSignIn(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 85
    const-string v1, "BaiduAPIHelper"

    const-string v2, "requestSignIn"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->mSignInRequested:Z

    .line 88
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.cloudagent.ACTION_REQUEST_SIGNIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 89
    .local v0, "loginIntent":Landroid/content/Intent;
    const-string v1, "packageName"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 90
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 91
    return-void
.end method


# virtual methods
.method public createFolder(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 5
    .param p1, "folderName"    # Ljava/lang/String;
    .param p2, "serverPath"    # Ljava/lang/String;

    .prologue
    .line 211
    const/4 v1, 0x0

    .line 214
    .local v1, "folder":Lcom/samsung/scloud/data/SCloudFolder;
    :try_start_0
    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity;->DEBUG:Z
    invoke-static {}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity;->access$000()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 215
    const-string v2, "BaiduAPIHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createFolder serverPath="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", folderName ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->bAPI:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    invoke-virtual {v2, p1, p2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->makeFolder(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 221
    :goto_0
    return-object v1

    .line 218
    :catch_0
    move-exception v0

    .line 219
    .local v0, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    goto :goto_0
.end method

.method public deleteFile(Ljava/lang/String;)V
    .locals 3
    .param p1, "serverPath"    # Ljava/lang/String;

    .prologue
    .line 292
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->bAPI:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->delete(Ljava/lang/String;)Z

    .line 294
    new-instance v0, Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->context:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    .line 295
    .local v0, "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 296
    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/db/CacheDB;->deleteCloudData(Ljava/lang/String;)I

    .line 297
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    .line 301
    .end local v0    # "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    :goto_0
    return-void

    .line 298
    :catch_0
    move-exception v1

    .line 299
    .local v1, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v1}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    goto :goto_0
.end method

.method public download(Ljava/io/File;Ljava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)V
    .locals 4
    .param p1, "localFile"    # Ljava/io/File;
    .param p2, "serverPath"    # Ljava/lang/String;
    .param p3, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;

    .prologue
    .line 199
    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity;->DEBUG:Z
    invoke-static {}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity;->access$000()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 200
    const-string v1, "BaiduAPIHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "download:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->bAPI:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, p2, v2, p3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getFile(Ljava/io/File;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)V
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    .line 207
    :goto_0
    return-void

    .line 204
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    goto :goto_0
.end method

.method public fileCopyOrMove(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 6
    .param p1, "fromPath"    # Ljava/lang/String;
    .param p2, "toPath"    # Ljava/lang/String;
    .param p3, "operationType"    # I

    .prologue
    const/4 v2, 0x1

    .line 305
    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity;->DEBUG:Z
    invoke-static {}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity;->access$000()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 306
    const-string v3, "BaiduAPIHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "BaiduAPI rename fromPath:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " toPath:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    :cond_0
    const/4 v1, 0x0

    .line 310
    .local v1, "node":Lcom/samsung/scloud/data/SCloudNode;
    if-nez p3, :cond_1

    .line 311
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->bAPI:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    const/4 v4, 0x1

    invoke-virtual {v3, p1, p2, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->copy(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v1

    .line 319
    :goto_0
    return v2

    .line 313
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->bAPI:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    const/4 v4, 0x1

    invoke-virtual {v3, p1, p2, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->move(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/scloud/data/SCloudNode;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 314
    :catch_0
    move-exception v0

    .line 315
    .local v0, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    .line 316
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getAuthToken()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->bAPI:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getAuthToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDeltaList(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .locals 3
    .param p1, "Cursor"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 264
    const/4 v1, 0x0

    .line 267
    .local v1, "nodes":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudNode;>;"
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->bAPI:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getDeltaList(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 272
    :goto_0
    return-object v1

    .line 268
    :catch_0
    move-exception v0

    .line 269
    .local v0, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    goto :goto_0
.end method

.method public getErrorCode()I
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->bAPI:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getErrorCode()I

    move-result v0

    return v0
.end method

.method public getFileInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 5
    .param p1, "serverPath"    # Ljava/lang/String;

    .prologue
    .line 150
    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity;->DEBUG:Z
    invoke-static {}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity;->access$000()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 151
    const-string v2, "BaiduAPIHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getFileInfo:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    :cond_0
    const/4 v1, 0x0

    .line 156
    .local v1, "file":Lcom/samsung/scloud/data/SCloudFile;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->bAPI:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getFileInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFile;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 161
    :goto_0
    return-object v1

    .line 157
    :catch_0
    move-exception v0

    .line 158
    .local v0, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    goto :goto_0
.end method

.method public getFolderInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 3
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 252
    const/4 v1, 0x0

    .line 254
    .local v1, "folder":Lcom/samsung/scloud/data/SCloudFolder;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->bAPI:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getFolderInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 259
    :goto_0
    return-object v1

    .line 255
    :catch_0
    move-exception v0

    .line 256
    .local v0, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    goto :goto_0
.end method

.method public getList()Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    .line 226
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 230
    .local v3, "fileslist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Ljava/lang/String;>;"
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->bAPI:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    const-string v7, "/apps/samsung"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getList(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;

    move-result-object v4

    .line 231
    .local v4, "folder":Lcom/samsung/scloud/data/SCloudFolder;
    invoke-virtual {v4}, Lcom/samsung/scloud/data/SCloudFolder;->getFiles()Ljava/util/ArrayList;

    move-result-object v2

    .line 232
    .local v2, "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudFile;>;"
    if-eqz v2, :cond_2

    .line 233
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/scloud/data/SCloudFile;

    .line 234
    .local v0, "aFile":Lcom/samsung/scloud/data/SCloudFile;
    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity;->DEBUG:Z
    invoke-static {}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity;->access$000()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 235
    const-string v6, "BaiduAPIHelper"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getList: absolutePath is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/samsung/scloud/data/SCloudFile;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/scloud/data/SCloudFile;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string v7, "/apps/samsung"

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-ne v6, v9, :cond_0

    .line 237
    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v0}, Lcom/samsung/scloud/data/SCloudFile;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-virtual {v0}, Lcom/samsung/scloud/data/SCloudFile;->getName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 238
    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity;->DEBUG:Z
    invoke-static {}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity;->access$000()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 239
    const-string v6, "BaiduAPIHelper"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getList: fileName is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/samsung/scloud/data/SCloudFile;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 243
    .end local v0    # "aFile":Lcom/samsung/scloud/data/SCloudFile;
    .end local v2    # "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudFile;>;"
    .end local v4    # "folder":Lcom/samsung/scloud/data/SCloudFolder;
    .end local v5    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v1

    .line 244
    .local v1, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v1}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    .line 247
    .end local v1    # "e":Lcom/samsung/scloud/exception/SCloudException;
    :cond_2
    return-object v3
.end method

.method public getThumbnail(Ljava/io/File;Ljava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)V
    .locals 7
    .param p1, "thumbnailFile"    # Ljava/io/File;
    .param p2, "serverPath"    # Ljava/lang/String;
    .param p3, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;

    .prologue
    .line 325
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->bAPI:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    const/4 v3, 0x5

    const/4 v4, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getThumbnail(Ljava/io/File;Ljava/lang/String;IILcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    .line 329
    :goto_0
    return-void

    .line 326
    :catch_0
    move-exception v6

    .line 327
    .local v6, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v6}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    goto :goto_0
.end method

.method public getWholeFolderList(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .locals 4
    .param p1, "Cursor"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudFolder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 278
    const/4 v1, 0x0

    .line 281
    .local v1, "nodes":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudFolder;>;"
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->bAPI:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getWholeFolderList(ILjava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 286
    :goto_0
    return-object v1

    .line 282
    :catch_0
    move-exception v0

    .line 283
    .local v0, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    goto :goto_0
.end method

.method public isAuthKeySaved()Z
    .locals 6

    .prologue
    .line 129
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->context:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v2

    .line 131
    .local v2, "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDropboxAuthKey()Ljava/lang/String;

    move-result-object v0

    .line 133
    .local v0, "authKey":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDropboxAuthSecret()Ljava/lang/String;

    move-result-object v1

    .line 135
    .local v1, "authSecret":Ljava/lang/String;
    const-string v3, "BaiduAPIHelper"

    const-string v4, "isAuthKeySaved"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 138
    :cond_0
    const/4 v3, 0x0

    .line 144
    :goto_0
    return v3

    .line 140
    :cond_1
    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity;->DEBUG:Z
    invoke-static {}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity;->access$000()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 141
    const-string v3, "BaiduAPIHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "authKey: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", authSecret: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :cond_2
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->setAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public isBaiduCloudAccountIsSignin()Z
    .locals 5

    .prologue
    .line 113
    const/4 v0, 0x0

    .line 114
    .local v0, "bAvailable":Z
    const-string v2, "BaiduAPIHelper"

    const-string v3, "isBaiduCloudAccountIsSignin"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/cloudagent/CloudStore$API;->isCloudAvailable(Landroid/content/Context;)Z

    move-result v0

    .line 118
    const-string v2, "BaiduAPIHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isBaiduCloudAccountIsSignin bAvailable:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v2, v0

    .line 124
    :goto_0
    return v2

    .line 119
    :catch_0
    move-exception v1

    .line 120
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 121
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isSignin()Z
    .locals 3

    .prologue
    .line 95
    const-string v0, "BaiduAPIHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bAPI.isSignin():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->bAPI:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->isSignin()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->bAPI:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->isSignin()Z

    move-result v0

    return v0
.end method

.method public rename(Ljava/lang/String;Ljava/lang/String;ZZ)Z
    .locals 6
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .param p2, "newName"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .param p4, "isDir"    # Z

    .prologue
    const/4 v2, 0x0

    .line 333
    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity;->DEBUG:Z
    invoke-static {}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity;->access$000()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 334
    const-string v3, "BaiduAPIHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "BaiduAPI rename IdOrPath:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " newName:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " overwrite:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    :cond_0
    const/4 v1, 0x0

    .line 339
    .local v1, "node":Lcom/samsung/scloud/data/SCloudNode;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->bAPI:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    invoke-virtual {v3, p1, p2, p3, p4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->rename(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/samsung/scloud/data/SCloudNode;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 345
    if-eqz v1, :cond_1

    .line 346
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteThumbnail(Ljava/lang/String;)V

    .line 347
    const/4 v2, 0x1

    .line 350
    :cond_1
    :goto_0
    return v2

    .line 340
    :catch_0
    move-exception v0

    .line 341
    .local v0, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAuthToken(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "secret"    # Ljava/lang/String;

    .prologue
    .line 102
    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity;->DEBUG:Z
    invoke-static {}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    const-string v0, "BaiduAPIHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setAuthToken key:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " secret:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->bAPI:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->setAuthToken(Ljava/lang/String;)V

    .line 105
    return-void
.end method

.method public setToContinue(Z)V
    .locals 3
    .param p1, "isContinue"    # Z

    .prologue
    .line 173
    const-string v0, "BaiduAPIHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BaiduAPI setToContinue: checkCancel="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->bAPI:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->setToContinue(Z)V

    .line 175
    return-void
.end method

.method public upload(Ljava/io/File;Ljava/lang/String;ZLjava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 8
    .param p1, "localFile"    # Ljava/io/File;
    .param p2, "serverPath"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .param p4, "revision"    # Ljava/lang/String;
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;

    .prologue
    .line 179
    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity;->DEBUG:Z
    invoke-static {}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    const-string v0, "BaiduAPIHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BaiduAPI upload: localFile="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", overwrite="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    const-string v0, "BaiduAPIHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "serverPath:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    const-string v0, "BaiduAPIHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "revision:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    :cond_0
    const/4 v6, 0x0

    .line 189
    .local v6, "cloudFile":Lcom/samsung/scloud/data/SCloudFile;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->bAPI:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->putFile(Ljava/io/File;Ljava/lang/String;ZLjava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 194
    :goto_0
    return-object v6

    .line 190
    :catch_0
    move-exception v7

    .line 191
    .local v7, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v7}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    goto :goto_0
.end method
