.class public Lcom/sec/android/app/myfiles/view/SplitViewContainer;
.super Landroid/widget/LinearLayout;
.source "SplitViewContainer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/view/SplitViewContainer$SplitBarStateObserver;
    }
.end annotation


# static fields
.field private static final LEFT_PANEL_MIN_WIDTH_RATIO:F = 0.08f

.field private static final MODULE:Ljava/lang/String; = "SplitViewContainer"

.field private static final PRESS_RANGE:I = 0x1e

.field private static final RIGHT_PANEL_MIN_WIDTH_RATIO:F = 0.34f

.field private static final SPLIT_BAR_PRESS_STATE_CHANGED:I


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDragStarted:Z

.field private mLeftPanel:Landroid/view/ViewGroup;

.field private mLeftPanelDefaultWidth:I

.field private mLeftPanelMinWidth:I

.field private mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

.field private mRightPanel:Landroid/view/ViewGroup;

.field private mRightPanelMinWidth:I

.field private mSds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

.field private mSplitBar:Landroid/view/View;

.field private mSplitBarHoverListener:Landroid/view/View$OnHoverListener;

.field private mSplitBarHoverView:Landroid/view/View;

.field private mSplitBarRect:Landroid/graphics/Rect;

.field private mSplitBarStateObserverList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/view/SplitViewContainer$SplitBarStateObserver;",
            ">;"
        }
    .end annotation
.end field

.field private mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

.field private mViewWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    .line 491
    new-instance v0, Lcom/sec/android/app/myfiles/view/SplitViewContainer$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/view/SplitViewContainer$1;-><init>(Lcom/sec/android/app/myfiles/view/SplitViewContainer;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBarHoverListener:Landroid/view/View$OnHoverListener;

    .line 87
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mContext:Landroid/content/Context;

    .line 89
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->init()V

    .line 92
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/view/SplitViewContainer;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/view/SplitViewContainer;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private findTreeView(Landroid/view/View;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 257
    instance-of v4, p1, Landroid/view/ViewGroup;

    if-eqz v4, :cond_3

    move-object v2, p1

    .line 259
    check-cast v2, Landroid/view/ViewGroup;

    .line 263
    .local v2, "vg":Landroid/view/ViewGroup;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-ge v1, v4, :cond_3

    .line 265
    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 267
    .local v0, "child":Landroid/view/View;
    instance-of v4, v0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    if-eqz v4, :cond_1

    .line 269
    check-cast v0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    .end local v0    # "child":Landroid/view/View;
    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    .line 283
    .end local v1    # "i":I
    .end local v2    # "vg":Landroid/view/ViewGroup;
    :cond_0
    :goto_1
    return v3

    .line 273
    .restart local v0    # "child":Landroid/view/View;
    .restart local v1    # "i":I
    .restart local v2    # "vg":Landroid/view/ViewGroup;
    :cond_1
    instance-of v4, v0, Landroid/view/ViewGroup;

    if-eqz v4, :cond_2

    .line 275
    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->findTreeView(Landroid/view/View;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 263
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 283
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "i":I
    .end local v2    # "vg":Landroid/view/ViewGroup;
    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private init()V
    .locals 2

    .prologue
    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBarStateObserverList:Ljava/util/ArrayList;

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mDragStarted:Z

    .line 101
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBarRect:Landroid/graphics/Rect;

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09014a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mLeftPanelDefaultWidth:I

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09014b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mLeftPanelMinWidth:I

    .line 107
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09014c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mRightPanelMinWidth:I

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isEnableMultiWindow(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 113
    new-instance v1, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-direct {v1, v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 119
    :goto_0
    return-void

    .line 117
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    goto :goto_0
.end method

.method private notifyToObservers(I)V
    .locals 4
    .param p1, "state"    # I

    .prologue
    .line 470
    iget-object v3, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBarStateObserverList:Ljava/util/ArrayList;

    monitor-enter v3

    .line 472
    packed-switch p1, :pswitch_data_0

    .line 483
    :cond_0
    :try_start_0
    monitor-exit v3

    .line 484
    return-void

    .line 476
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBarStateObserverList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/view/SplitViewContainer$SplitBarStateObserver;

    .line 478
    .local v1, "observer":Lcom/sec/android/app/myfiles/view/SplitViewContainer$SplitBarStateObserver;
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mDragStarted:Z

    invoke-interface {v1, v2}, Lcom/sec/android/app/myfiles/view/SplitViewContainer$SplitBarStateObserver;->onSplitBarPressed(Z)V

    goto :goto_0

    .line 483
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "observer":Lcom/sec/android/app/myfiles/view/SplitViewContainer$SplitBarStateObserver;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 472
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 291
    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 299
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v7

    if-nez v7, :cond_1

    .line 301
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v8}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 311
    :cond_1
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    if-ne v7, v5, :cond_3

    .line 313
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v4, v7

    .line 315
    .local v4, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 317
    .local v0, "action":I
    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mLeftPanel:Landroid/view/ViewGroup;

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mLeftPanel:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v7

    if-nez v7, :cond_3

    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBar:Landroid/view/View;

    if-eqz v7, :cond_3

    .line 319
    packed-switch v0, :pswitch_data_0

    .line 420
    :cond_2
    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mDragStarted:Z

    .line 429
    .end local v0    # "action":I
    .end local v4    # "x":I
    :cond_3
    :goto_1
    :try_start_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_1
    .catch Ljava/lang/ArithmeticException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v5

    .line 435
    :cond_4
    :goto_2
    return v5

    .line 304
    :catch_0
    move-exception v1

    .line 306
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 323
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "action":I
    .restart local v4    # "x":I
    :pswitch_0
    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBarRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    add-int/lit8 v7, v7, -0x1e

    if-gt v7, v4, :cond_3

    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBarRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    add-int/lit8 v7, v7, 0x1e

    if-gt v4, v7, :cond_3

    .line 326
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mDragStarted:Z

    .line 330
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    if-eqz v7, :cond_5

    .line 332
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    invoke-interface {v7, v5}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 335
    :cond_5
    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->notifyToObservers(I)V

    .line 337
    iget-object v6, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBar:Landroid/view/View;

    invoke-virtual {v6, v5}, Landroid/view/View;->setPressed(Z)V

    .line 339
    iget-object v6, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    if-eqz v6, :cond_4

    .line 341
    iget-object v6, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    invoke-virtual {v6, v5}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->setPositionIndicatorPressed(Z)V

    goto :goto_2

    .line 352
    :pswitch_1
    iget-boolean v7, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mDragStarted:Z

    if-eqz v7, :cond_3

    .line 354
    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mLeftPanel:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 356
    .local v3, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mLeftPanelMinWidth:I

    if-lt v4, v7, :cond_6

    iget v7, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mViewWidth:I

    iget v8, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mRightPanelMinWidth:I

    sub-int/2addr v7, v8

    if-gt v4, v7, :cond_6

    .line 358
    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSplitLocation(I)V

    .line 360
    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 362
    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mLeftPanel:Landroid/view/ViewGroup;

    invoke-virtual {v7, v3}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 364
    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    if-eqz v7, :cond_3

    .line 366
    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    invoke-virtual {v7, v5}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->showPositionIndicator(Z)V

    goto :goto_1

    .line 371
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 373
    iget v5, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mLeftPanelMinWidth:I

    if-ge v4, v5, :cond_3

    .line 375
    iget-object v5, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBarRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    iput v5, v3, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 377
    iget-object v5, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mLeftPanel:Landroid/view/ViewGroup;

    invoke-virtual {v5, v3}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 379
    iget-object v5, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    if-eqz v5, :cond_3

    .line 381
    iget-object v5, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->showPositionIndicator(Z)V

    goto/16 :goto_1

    .line 394
    .end local v3    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :pswitch_2
    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mDragStarted:Z

    if-eqz v5, :cond_2

    .line 396
    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mDragStarted:Z

    .line 400
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-eqz v5, :cond_7

    .line 402
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    invoke-interface {v5, v6}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 405
    :cond_7
    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->notifyToObservers(I)V

    .line 407
    iget-object v5, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBar:Landroid/view/View;

    invoke-virtual {v5, v6}, Landroid/view/View;->setPressed(Z)V

    .line 409
    iget-object v5, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    if-eqz v5, :cond_3

    .line 411
    iget-object v5, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->setPositionIndicatorPressed(Z)V

    goto/16 :goto_1

    .line 431
    .end local v0    # "action":I
    .end local v4    # "x":I
    :catch_1
    move-exception v2

    .line 433
    .local v2, "ex":Ljava/lang/ArithmeticException;
    invoke-virtual {v2}, Ljava/lang/ArithmeticException;->printStackTrace()V

    move v5, v6

    .line 435
    goto/16 :goto_2

    .line 319
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 124
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 126
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09014a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mLeftPanelDefaultWidth:I

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09014b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mLeftPanelMinWidth:I

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09014c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mRightPanelMinWidth:I

    .line 149
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 154
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 156
    if-eqz p1, :cond_7

    .line 158
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBar:Landroid/view/View;

    if-nez v2, :cond_0

    .line 160
    const v2, 0x7f0f0040

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBar:Landroid/view/View;

    .line 163
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBarHoverView:Landroid/view/View;

    if-nez v2, :cond_1

    .line 165
    const v2, 0x7f0f0041

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBarHoverView:Landroid/view/View;

    .line 169
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBarHoverView:Landroid/view/View;

    if-eqz v2, :cond_9

    .line 171
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBarHoverView:Landroid/view/View;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBarHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 183
    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mLeftPanel:Landroid/view/ViewGroup;

    if-nez v2, :cond_3

    .line 185
    const v2, 0x7f0f003e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mLeftPanel:Landroid/view/ViewGroup;

    .line 188
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mRightPanel:Landroid/view/ViewGroup;

    if-nez v2, :cond_4

    .line 190
    const v2, 0x7f0f0042

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mRightPanel:Landroid/view/ViewGroup;

    .line 193
    :cond_4
    invoke-direct {p0, p0}, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->findTreeView(Landroid/view/View;)Z

    .line 195
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->getMeasuredWidth()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mViewWidth:I

    .line 197
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 199
    iget v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mViewWidth:I

    int-to-float v2, v2

    const v3, 0x3da3d70a    # 0.08f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mLeftPanelMinWidth:I

    .line 201
    iget v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mViewWidth:I

    int-to-float v2, v2

    const v3, 0x3eae147b    # 0.34f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mRightPanelMinWidth:I

    .line 204
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSplitLocation()I

    move-result v0

    .line 206
    .local v0, "leftPanelWidth":I
    if-nez v0, :cond_b

    .line 208
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 210
    iget v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mViewWidth:I

    iget v3, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mRightPanelMinWidth:I

    sub-int v0, v2, v3

    .line 217
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSplitLocation(I)V

    .line 230
    :cond_6
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mLeftPanel:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 232
    .local v1, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 234
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mLeftPanel:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 236
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    if-eqz v2, :cond_7

    .line 238
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->showPositionIndicator(Z)V

    .line 242
    .end local v0    # "leftPanelWidth":I
    .end local v1    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBar:Landroid/view/View;

    if-eqz v2, :cond_8

    .line 244
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBar:Landroid/view/View;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBarRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 246
    :cond_8
    return-void

    .line 177
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBar:Landroid/view/View;

    if-eqz v2, :cond_2

    .line 179
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBar:Landroid/view/View;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBarHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto/16 :goto_0

    .line 214
    .restart local v0    # "leftPanelWidth":I
    :cond_a
    iget v0, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mLeftPanelDefaultWidth:I

    goto :goto_1

    .line 222
    :cond_b
    iget v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mViewWidth:I

    iget v3, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mRightPanelMinWidth:I

    sub-int/2addr v2, v3

    if-le v0, v2, :cond_6

    .line 224
    iget v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mViewWidth:I

    iget v3, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mRightPanelMinWidth:I

    sub-int v0, v2, v3

    .line 226
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSplitLocation(I)V

    goto :goto_2
.end method

.method public refresh()V
    .locals 0

    .prologue
    .line 251
    invoke-direct {p0, p0}, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->findTreeView(Landroid/view/View;)Z

    .line 252
    return-void
.end method

.method public registerSplitBarStateObserver(Lcom/sec/android/app/myfiles/view/SplitViewContainer$SplitBarStateObserver;)Z
    .locals 6
    .param p1, "observer"    # Lcom/sec/android/app/myfiles/view/SplitViewContainer$SplitBarStateObserver;

    .prologue
    const/4 v0, 0x0

    .line 442
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBarStateObserverList:Ljava/util/ArrayList;

    monitor-enter v1

    .line 444
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBarStateObserverList:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 446
    const/4 v2, 0x0

    const-string v3, "SplitViewContainer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Split bar state observer is already registerd : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 449
    monitor-exit v1

    .line 453
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBarStateObserverList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 455
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public unregisterSplitBarStateObserver(Lcom/sec/android/app/myfiles/view/SplitViewContainer$SplitBarStateObserver;)Z
    .locals 2
    .param p1, "observer"    # Lcom/sec/android/app/myfiles/view/SplitViewContainer$SplitBarStateObserver;

    .prologue
    .line 461
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBarStateObserverList:Ljava/util/ArrayList;

    monitor-enter v1

    .line 463
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/SplitViewContainer;->mSplitBarStateObserverList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 464
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
