.class public Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;
.super Ljava/lang/Object;
.source "MyFilesAirButtonImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;
    }
.end annotation


# static fields
.field private static final MODULE:Ljava/lang/String; = "MyFilesAirButton"


# instance fields
.field private final AIRBUTTON_COPY:I

.field private final AIRBUTTON_DELETE:I

.field private final AIRBUTTON_MOVE:I

.field private final AIRBUTTON_RENAME:I

.field private final AIRBUTTON_SHARE:I

.field private mContext:Landroid/content/Context;

.field private mPosition:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->AIRBUTTON_COPY:I

    .line 40
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->AIRBUTTON_MOVE:I

    .line 41
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->AIRBUTTON_DELETE:I

    .line 42
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->AIRBUTTON_RENAME:I

    .line 43
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->AIRBUTTON_SHARE:I

    .line 47
    iput-object p1, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->mContext:Landroid/content/Context;

    .line 48
    return-void
.end method


# virtual methods
.method public createAirButtonMenu(Landroid/view/View;IIZ)Lcom/samsung/android/airbutton/AirButtonImpl;
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "menuType"    # I
    .param p3, "position"    # I
    .param p4, "auto"    # Z

    .prologue
    .line 52
    const/4 v0, 0x0

    .line 54
    .local v0, "airButtonAdapter":Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;
    sparse-switch p2, :sswitch_data_0

    .line 65
    invoke-virtual {p0, p3}, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->getFilesMenuList(I)Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;

    move-result-object v0

    .line 69
    :goto_0
    const/4 v1, 0x0

    .line 71
    .local v1, "airButtonWidget":Lcom/samsung/android/airbutton/AirButtonImpl;
    if-eqz v0, :cond_0

    .line 73
    new-instance v1, Lcom/samsung/android/airbutton/AirButtonImpl;

    .end local v1    # "airButtonWidget":Lcom/samsung/android/airbutton/AirButtonImpl;
    const/4 v2, 0x1

    invoke-direct {v1, p1, v0, v2, p4}, Lcom/samsung/android/airbutton/AirButtonImpl;-><init>(Landroid/view/View;Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;IZ)V

    .line 74
    .restart local v1    # "airButtonWidget":Lcom/samsung/android/airbutton/AirButtonImpl;
    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/samsung/android/airbutton/AirButtonImpl;->setGravity(I)V

    .line 75
    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/airbutton/AirButtonImpl;->setDirection(I)V

    .line 82
    :goto_1
    return-object v1

    .line 57
    .end local v1    # "airButtonWidget":Lcom/samsung/android/airbutton/AirButtonImpl;
    :sswitch_0
    invoke-virtual {p0, p3}, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->getShortcutMenuList(I)Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;

    move-result-object v0

    .line 58
    goto :goto_0

    .line 61
    :sswitch_1
    invoke-virtual {p0, p3}, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->getFolderMenuList(I)Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;

    move-result-object v0

    .line 62
    goto :goto_0

    .line 79
    .restart local v1    # "airButtonWidget":Lcom/samsung/android/airbutton/AirButtonImpl;
    :cond_0
    const/4 v2, 0x0

    const-string v3, "MyFilesAirButton"

    const-string v4, "createAirButtonMenu failed"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 54
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x3001 -> :sswitch_1
    .end sparse-switch
.end method

.method public createAirButtonMenuForListView(Landroid/view/View;)Lcom/samsung/android/airbutton/AirButtonImpl;
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 88
    new-instance v0, Lcom/samsung/android/airbutton/AirButtonImpl;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->getFolderMenuList(I)Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/samsung/android/airbutton/AirButtonImpl;-><init>(Landroid/view/View;Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;IZ)V

    .line 89
    .local v0, "airButtonWidget":Lcom/samsung/android/airbutton/AirButtonImpl;
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/samsung/android/airbutton/AirButtonImpl;->setGravity(I)V

    .line 90
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/airbutton/AirButtonImpl;->setDirection(I)V

    .line 93
    return-object v0
.end method

.method public getDownloadedAppMenuList(I)Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 155
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 157
    .local v1, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;>;"
    new-instance v0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;

    const/4 v2, 0x2

    invoke-direct {v0, p1, v2}, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;-><init>(II)V

    .line 158
    .local v0, "curItemDeleteInfo":Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;
    new-instance v2, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020039

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0020

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    new-instance v2, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;

    invoke-direct {v2, v1}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;-><init>(Ljava/util/ArrayList;)V

    return-object v2
.end method

.method public getFilesMenuList(I)Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;
    .locals 9
    .param p1, "position"    # I

    .prologue
    .line 132
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 134
    .local v4, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;>;"
    new-instance v3, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;

    const/4 v5, 0x4

    invoke-direct {v3, p1, v5}, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;-><init>(II)V

    .line 135
    .local v3, "curItemShareInfo":Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;
    new-instance v5, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020046

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b001f

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7, v3}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    new-instance v0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;

    const/4 v5, 0x0

    invoke-direct {v0, p1, v5}, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;-><init>(II)V

    .line 139
    .local v0, "curItemCopyInfo":Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;
    new-instance v5, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020037

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0022

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7, v0}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    new-instance v2, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;

    const/4 v5, 0x3

    invoke-direct {v2, p1, v5}, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;-><init>(II)V

    .line 143
    .local v2, "curItemRenameInfo":Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;
    new-instance v5, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020042

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0023

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7, v2}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    new-instance v1, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;

    const/4 v5, 0x2

    invoke-direct {v1, p1, v5}, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;-><init>(II)V

    .line 147
    .local v1, "curItemDeleteInfo":Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;
    new-instance v5, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020039

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0020

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7, v1}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 150
    new-instance v5, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;

    invoke-direct {v5, v4}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;-><init>(Ljava/util/ArrayList;)V

    return-object v5
.end method

.method public getFolderMenuList(I)Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;
    .locals 9
    .param p1, "position"    # I

    .prologue
    .line 109
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 111
    .local v4, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;>;"
    new-instance v0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;

    const/4 v5, 0x0

    invoke-direct {v0, p1, v5}, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;-><init>(II)V

    .line 112
    .local v0, "curItemCopyInfo":Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;
    new-instance v5, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020037

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0022

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7, v0}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    new-instance v2, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;

    const/4 v5, 0x1

    invoke-direct {v2, p1, v5}, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;-><init>(II)V

    .line 116
    .local v2, "curItemMoveInfo":Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;
    new-instance v5, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f02003f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0021

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7, v2}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    new-instance v3, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;

    const/4 v5, 0x3

    invoke-direct {v3, p1, v5}, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;-><init>(II)V

    .line 120
    .local v3, "curItemRenameInfo":Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;
    new-instance v5, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020042

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0023

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7, v3}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    new-instance v1, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;

    const/4 v5, 0x2

    invoke-direct {v1, p1, v5}, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;-><init>(II)V

    .line 124
    .local v1, "curItemDeleteInfo":Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;
    new-instance v5, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020039

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0020

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7, v1}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    new-instance v5, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;

    invoke-direct {v5, v4}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;-><init>(Ljava/util/ArrayList;)V

    return-object v5
.end method

.method public getShortcutMenuList(I)Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 98
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 99
    .local v1, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;>;"
    new-instance v0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;

    const/4 v2, 0x2

    invoke-direct {v0, p1, v2}, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;-><init>(II)V

    .line 101
    .local v0, "curItemInfo":Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl$CurItemFromAirButton;
    new-instance v2, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020039

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b00e6

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    new-instance v2, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;

    invoke-direct {v2, v1}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;-><init>(Ljava/util/ArrayList;)V

    return-object v2
.end method
