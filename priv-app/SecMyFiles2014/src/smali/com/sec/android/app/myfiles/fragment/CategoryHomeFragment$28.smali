.class Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$28;
.super Ljava/lang/Object;
.source "CategoryHomeFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mLocalItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V
    .locals 0

    .prologue
    .line 3108
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$28;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 3112
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p2}, Landroid/view/View;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_1

    .line 3133
    :cond_0
    :goto_0
    return-void

    .line 3115
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$28;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mStorageListAdapter:Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$500(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;

    move-result-object v3

    invoke-virtual {v3, p3}, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 3117
    .local v2, "item":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$28;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentDirectory(Ljava/lang/String;)V

    .line 3119
    long-to-int v1, p4

    .line 3121
    .local v1, "categoryType":I
    const/16 v1, 0x201

    .line 3123
    const/4 v0, 0x0

    .line 3125
    .local v0, "argument":Landroid/os/Bundle;
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "argument":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 3127
    .restart local v0    # "argument":Landroid/os/Bundle;
    const-string v3, "FOLDERPATH"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3129
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$28;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v3, v3, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v3, :cond_0

    .line 3130
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$28;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setStartBrowserState(Z)V

    .line 3131
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$28;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v3, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v3, v1, v0}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto :goto_0
.end method
