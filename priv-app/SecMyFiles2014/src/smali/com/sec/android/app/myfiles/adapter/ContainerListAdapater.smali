.class public Lcom/sec/android/app/myfiles/adapter/ContainerListAdapater;
.super Landroid/widget/ArrayAdapter;
.source "ContainerListAdapater.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/myfiles/element/PersonaItem;",
        ">;"
    }
.end annotation


# instance fields
.field mArrList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/element/PersonaItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/element/PersonaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p2, "objects":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/element/PersonaItem;>;"
    const v0, 0x7f04002e

    const v1, 0x7f0f00c1

    invoke-direct {p0, p1, v0, v1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ContainerListAdapater;->mArrList:Ljava/util/List;

    .line 40
    iput-object p2, p0, Lcom/sec/android/app/myfiles/adapter/ContainerListAdapater;->mArrList:Ljava/util/List;

    .line 41
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 46
    if-nez p2, :cond_0

    .line 47
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ContainerListAdapater;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 48
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f04002e

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 51
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/ContainerListAdapater;->mArrList:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/PersonaItem;

    .line 53
    .local v1, "item":Lcom/sec/android/app/myfiles/element/PersonaItem;
    const v4, 0x7f0f00c0

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 54
    .local v2, "iv":Landroid/widget/ImageView;
    const v4, 0x7f0f00c1

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 55
    .local v3, "tv":Landroid/widget/TextView;
    if-eqz v2, :cond_1

    .line 56
    if-nez p1, :cond_3

    .line 57
    const v4, 0x7f0200e0

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 62
    :cond_1
    :goto_0
    if-eqz v3, :cond_2

    .line 63
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/PersonaItem;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    :cond_2
    return-object p2

    .line 59
    :cond_3
    const v4, 0x7f0200e1

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method
