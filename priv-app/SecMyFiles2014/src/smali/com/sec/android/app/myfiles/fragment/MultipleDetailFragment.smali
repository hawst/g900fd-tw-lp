.class public Lcom/sec/android/app/myfiles/fragment/MultipleDetailFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment;
.source "MultipleDetailFragment.java"


# instance fields
.field private paths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment;-><init>()V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/MultipleDetailFragment;->paths:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public getFileSize(Ljava/util/ArrayList;)J
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 29
    .local p1, "pathes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/MultipleDetailFragment;->paths:Ljava/util/ArrayList;

    .line 30
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/MultipleDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/android/app/myfiles/utils/Utils;->getPathFilesSize(Landroid/content/Context;Ljava/util/ArrayList;)J

    move-result-wide v0

    return-wide v0
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 34
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/MultipleDetailFragment;->paths:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 35
    .local v2, "str":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 36
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 37
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/MultipleDetailFragment;->dismiss()V

    .line 41
    .end local v0    # "file":Ljava/io/File;
    .end local v2    # "str":Ljava/lang/String;
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment;->onResume()V

    .line 42
    return-void
.end method
