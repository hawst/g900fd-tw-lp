.class public Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;
.super Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
.source "MusicHoverPopup.java"


# static fields
.field private static final EXTERNAL_MEDIA_AUDIO_DB_URI:Landroid/net/Uri;

.field private static final HOVER_ALBUM_ART_HEIGHT:I = 0x2bc

.field private static final HOVER_ALBUM_ART_WIDTH:I = 0x2bc

.field private static final MODULE:Ljava/lang/String; = "MusicHoverPlay"


# instance fields
.field private layout:Landroid/widget/RelativeLayout;

.field private mAlbumArt:Landroid/graphics/Bitmap;

.field private mHoverOperationContainer:Landroid/view/View;

.field private mbAlbumArtExsist:Z

.field private mediaPlayer:Landroid/media/MediaPlayer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    sget-object v0, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->EXTERNAL_MEDIA_AUDIO_DB_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 78
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;-><init>(Landroid/content/Context;)V

    .line 56
    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mAlbumArt:Landroid/graphics/Bitmap;

    .line 57
    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 59
    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->layout:Landroid/widget/RelativeLayout;

    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mbAlbumArtExsist:Z

    .line 75
    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mHoverOperationContainer:Landroid/view/View;

    .line 79
    return-void
.end method

.method private getAlbumartBitmap(I)Landroid/graphics/Bitmap;
    .locals 14
    .param p1, "albumID"    # I

    .prologue
    const/4 v1, 0x0

    .line 90
    const/4 v11, -0x1

    if-ne p1, v11, :cond_1

    .line 182
    :cond_0
    :goto_0
    return-object v1

    .line 93
    :cond_1
    const/4 v1, 0x0

    .line 95
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    new-instance v6, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v6}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 97
    .local v6, "sBitmapOptions":Landroid/graphics/BitmapFactory$Options;
    const-string v11, "content://media/external/audio/albumart"

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 99
    .local v0, "artworkUri":Landroid/net/Uri;
    const/16 v10, 0x2bc

    .line 101
    .local v10, "width":I
    const/16 v5, 0x2bc

    .line 103
    .local v5, "height":I
    const/4 v4, 0x0

    .line 107
    .local v4, "fileDescriptor":Landroid/os/ParcelFileDescriptor;
    int-to-long v12, p1

    :try_start_0
    invoke-static {v0, v12, v13}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v9

    .line 109
    .local v9, "uri":Landroid/net/Uri;
    iget-object v11, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mResolver:Landroid/content/ContentResolver;

    const-string v12, "r"

    invoke-virtual {v11, v9, v12}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    .line 111
    const/4 v7, 0x1

    .line 113
    .local v7, "sampleSize":I
    const/4 v11, 0x1

    iput-boolean v11, v6, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 115
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v11

    const/4 v12, 0x0

    invoke-static {v11, v12, v6}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 117
    iget v11, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-gt v11, v5, :cond_2

    iget v11, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-le v11, v10, :cond_3

    .line 119
    :cond_2
    iget v11, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v12, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-le v11, v12, :cond_6

    .line 121
    iget v11, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v11, v11

    int-to-float v12, v5

    div-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 129
    :cond_3
    :goto_1
    iput v7, v6, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 131
    const/4 v11, 0x0

    iput-boolean v11, v6, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 133
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v11

    const/4 v12, 0x0

    invoke-static {v11, v12, v6}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 136
    .local v2, "bm":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_5

    .line 138
    iget v11, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-ne v11, v10, :cond_4

    iget v11, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-eq v11, v5, :cond_5

    .line 140
    :cond_4
    const/4 v11, 0x1

    invoke-static {v2, v10, v5, v11}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 142
    .local v8, "tmpBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    move-object v2, v8

    .line 148
    .end local v8    # "tmpBitmap":Landroid/graphics/Bitmap;
    :cond_5
    move-object v1, v2

    .line 162
    if-eqz v4, :cond_0

    .line 168
    :try_start_1
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 175
    :goto_2
    if-nez v1, :cond_0

    goto :goto_0

    .line 125
    .end local v2    # "bm":Landroid/graphics/Bitmap;
    :cond_6
    :try_start_2
    iget v11, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v11, v11

    int-to-float v12, v10

    div-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->round(F)I
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v7

    goto :goto_1

    .line 150
    .end local v7    # "sampleSize":I
    .end local v9    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v11

    .line 162
    if-eqz v4, :cond_0

    .line 168
    :try_start_3
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 175
    :goto_3
    if-nez v1, :cond_0

    goto :goto_0

    .line 154
    :catch_1
    move-exception v3

    .line 158
    .local v3, "e":Ljava/lang/OutOfMemoryError;
    :try_start_4
    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 162
    .end local v3    # "e":Ljava/lang/OutOfMemoryError;
    :catchall_0
    move-exception v11

    if-eqz v4, :cond_7

    .line 168
    :try_start_5
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 175
    :goto_4
    if-nez v1, :cond_7

    :cond_7
    throw v11

    .line 170
    .restart local v2    # "bm":Landroid/graphics/Bitmap;
    .restart local v7    # "sampleSize":I
    .restart local v9    # "uri":Landroid/net/Uri;
    :catch_2
    move-exception v11

    goto :goto_2

    .end local v2    # "bm":Landroid/graphics/Bitmap;
    .end local v7    # "sampleSize":I
    .end local v9    # "uri":Landroid/net/Uri;
    :catch_3
    move-exception v11

    goto :goto_3

    :catch_4
    move-exception v12

    goto :goto_4
.end method

.method private setAlbumAtrImage()V
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v9, 0x0

    .line 186
    const/4 v6, -0x1

    .line 188
    .local v6, "album_id":I
    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "album_id"

    aput-object v0, v2, v9

    .line 190
    .local v2, "cols":[Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 191
    .local v8, "where":Ljava/lang/StringBuilder;
    const-string v0, "_data =? "

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    const/4 v7, 0x0

    .line 194
    .local v7, "cursor":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mAlbumArt:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->EXTERNAL_MEDIA_AUDIO_DB_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mFilePath:Ljava/lang/String;

    aput-object v5, v4, v9

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 197
    if-eqz v7, :cond_1

    .line 198
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    const-string v0, "album_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 203
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 206
    :cond_1
    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->getAlbumartBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mAlbumArt:Landroid/graphics/Bitmap;

    .line 209
    :cond_2
    return-void
.end method


# virtual methods
.method protected changeList()V
    .locals 0

    .prologue
    .line 509
    return-void
.end method

.method protected changeListThreadStart()V
    .locals 0

    .prologue
    .line 515
    return-void
.end method

.method protected dissmissPopup()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 446
    const-string v1, "MusicHoverPlay"

    const-string v2, "stopMusic()"

    invoke-static {v3, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 448
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_1

    .line 449
    const-string v1, "MusicHoverPlay"

    const-string v2, "abandonAudioFocus pause()"

    invoke-static {v3, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 450
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->abandonAudioFocus()V

    .line 454
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 456
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->stop()V

    .line 457
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepare()V

    .line 458
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->seekTo(I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 467
    :cond_0
    :goto_0
    iput-object v4, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 470
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->isShow()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 474
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    .line 482
    :goto_1
    iput-object v4, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    .line 484
    :cond_2
    return-void

    .line 461
    :catch_0
    move-exception v0

    .line 462
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 463
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 464
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 476
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 478
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1
.end method

.method protected initViews()Z
    .locals 8

    .prologue
    const v7, 0x7f0f00bf

    const/16 v6, 0x8

    const/4 v3, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 213
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mFilePath:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    .line 380
    :goto_0
    return v1

    .line 216
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mAlbumArt:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 217
    iput-object v5, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mAlbumArt:Landroid/graphics/Bitmap;

    .line 219
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->setAlbumAtrImage()V

    .line 220
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 222
    new-instance v1, Landroid/app/Dialog;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v1, v4}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    .line 225
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mAlbumArt:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    .line 227
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v2, 0x7f040035

    invoke-static {v1, v2, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->layout:Landroid/widget/RelativeLayout;

    .line 228
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->layout:Landroid/widget/RelativeLayout;

    const v2, 0x7f0f00cd

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 230
    .local v0, "iv":Landroid/widget/ImageView;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mHoverOperationContainer:Landroid/view/View;

    .line 234
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mHoverOperationContainer:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 237
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mAlbumArt:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 238
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mbAlbumArtExsist:Z

    .line 257
    .end local v0    # "iv":Landroid/widget/ImageView;
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->setDialogProperties()V

    .line 258
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->setDialogPosition()V

    .line 260
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 262
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->initHoverOperationView(Landroid/view/View;)V

    .line 264
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->layout:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup$1;-><init>(Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 280
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->layout:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup$2;-><init>(Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    move v1, v3

    .line 380
    goto :goto_0

    .line 242
    :cond_2
    const-string v1, "MusicHoverPlay"

    const-string v4, "mAlbumArt is empty ~ !!! "

    invoke-static {v2, v1, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 244
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v4, 0x7f040034

    invoke-static {v1, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->layout:Landroid/widget/RelativeLayout;

    .line 246
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mHoverOperationContainer:Landroid/view/View;

    .line 250
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mHoverOperationContainer:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 253
    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mbAlbumArtExsist:Z

    goto :goto_1
.end method

.method protected setDialogPosition()V
    .locals 4

    .prologue
    .line 488
    const/4 v1, 0x0

    const-string v2, "MusicHoverPlay"

    const-string v3, "setPosition"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 490
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 497
    .local v0, "params":Landroid/view/WindowManager$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mHoverRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v1, v1, -0x32

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 498
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mHoverRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    add-int/lit8 v1, v1, -0x64

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 500
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 501
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x33

    invoke-virtual {v1, v2}, Landroid/view/Window;->setGravity(I)V

    .line 503
    return-void
.end method

.method protected showContent()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    .line 386
    const/4 v0, 0x0

    .line 388
    .local v0, "PRELISTEN_START_TIME":I
    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 389
    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->stop()V

    .line 391
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 401
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->removeDelayedMessage()V

    .line 404
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->requestAudioFocus()Z

    move-result v4

    if-nez v4, :cond_1

    .line 405
    const-string v4, "MusicHoverPlay"

    const-string v5, "start() : requestAudioFocus() is false"

    invoke-static {v7, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 406
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->getHandler()Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 442
    :goto_1
    return-void

    .line 392
    :catch_0
    move-exception v2

    .line 394
    .local v2, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 395
    const-string v4, "MusicHoverPlay"

    const-string v5, "mediaPlayer.isPlaying() - IllegalStateException"

    invoke-static {v7, v4, v5, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 396
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v2

    .line 398
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 399
    const-string v4, "MusicHoverPlay"

    const-string v5, "mediaPlayer.isPlaying() - IOException"

    invoke-static {v7, v4, v5, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 412
    .end local v2    # "e":Ljava/io/IOException;
    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mFilePath:Ljava/lang/String;

    if-nez v4, :cond_2

    .line 413
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->getHandler()Landroid/os/Handler;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_4

    goto :goto_1

    .line 435
    :catch_2
    move-exception v3

    .line 436
    .local v3, "ex":Ljava/io/IOException;
    const-string v4, "MusicHoverPlay"

    const-string v5, "IOException"

    invoke-static {v7, v4, v5, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 417
    .end local v3    # "ex":Ljava/io/IOException;
    :cond_2
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mFilePath:Ljava/lang/String;

    const-string v5, "content://"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 418
    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mFilePath:Ljava/lang/String;

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 422
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v5, "audio"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    .line 423
    .local v1, "audioManager":Landroid/media/AudioManager;
    invoke-virtual {v1}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v4

    if-ne v4, v8, :cond_4

    .line 424
    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 428
    :goto_3
    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->prepare()V

    .line 430
    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    .line 431
    div-int/lit8 v0, v0, 0x3

    .line 432
    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4, v0}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 433
    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->start()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_4

    goto :goto_1

    .line 437
    .end local v1    # "audioManager":Landroid/media/AudioManager;
    :catch_3
    move-exception v3

    .line 438
    .local v3, "ex":Ljava/lang/IllegalStateException;
    const-string v4, "MusicHoverPlay"

    const-string v5, "IllegalStateException"

    invoke-static {v7, v4, v5, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 420
    .end local v3    # "ex":Ljava/lang/IllegalStateException;
    :cond_3
    :try_start_3
    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mFilePath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_2

    .line 439
    :catch_4
    move-exception v3

    .line 440
    .local v3, "ex":Ljava/lang/IllegalArgumentException;
    const-string v4, "MusicHoverPlay"

    const-string v5, "IllegalArgumentException"

    invoke-static {v7, v4, v5, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    .line 426
    .end local v3    # "ex":Ljava/lang/IllegalArgumentException;
    .restart local v1    # "audioManager":Landroid/media/AudioManager;
    :cond_4
    :try_start_4
    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_3
.end method
