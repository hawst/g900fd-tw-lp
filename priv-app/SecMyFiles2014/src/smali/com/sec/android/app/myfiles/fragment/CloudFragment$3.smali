.class Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;
.super Ljava/lang/Object;
.source "CloudFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CloudFragment;->onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private volatile mCachingCancelled:Z

.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CloudFragment;)V
    .locals 1

    .prologue
    .line 1000
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1002
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->mCachingCancelled:Z

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;

    .prologue
    .line 1000
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->mCachingCancelled:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;
    .param p1, "x1"    # Z

    .prologue
    .line 1000
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->mCachingCancelled:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;
    .param p1, "x1"    # Landroid/net/Uri;
    .param p2, "x2"    # Landroid/net/Uri;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 1000
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->intentForImageOpen(Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;)V

    return-void
.end method

.method private intentForImageOpen(Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 5
    .param p1, "fileUri"    # Landroid/net/Uri;
    .param p2, "cloudUri"    # Landroid/net/Uri;
    .param p3, "mimetype"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 1006
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 1027
    :cond_0
    :goto_0
    return-void

    .line 1011
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1013
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "isCloudUri"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1015
    const-string v2, "cloudUri"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1017
    const-string v2, "from-myfiles"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1021
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1023
    :catch_0
    move-exception v0

    .line 1025
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 34
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1032
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v6, 0x0

    const-string v10, "DropboxFragment"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "onItemClick() : parent = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v6, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1033
    invoke-virtual/range {p1 .. p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v6

    move/from16 v0, p3

    invoke-interface {v6, v0}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Landroid/database/Cursor;

    .line 1034
    .local v23, "cursor":Landroid/database/Cursor;
    const-string v6, "_id"

    move-object/from16 v0, v23

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v23

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    .line 1035
    .local v24, "cursorId":J
    const-string v6, "_data"

    move-object/from16 v0, v23

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v23

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v32

    .line 1036
    .local v32, "path":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->isSelectMode()Z

    move-result v6

    if-eqz v6, :cond_0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    if-eqz v6, :cond_e

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v10, "run_from"

    invoke-virtual {v6, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    const/16 v10, 0x15

    if-ne v6, v10, :cond_e

    .line 1040
    :cond_0
    const-string v6, "format"

    move-object/from16 v0, v23

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v23

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const/16 v10, 0x3001

    if-ne v6, v10, :cond_3

    .line 1043
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->isSelectMode()Z

    move-result v6

    if-eqz v6, :cond_1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v6, :cond_1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v6

    if-lez v6, :cond_1

    .line 1045
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 1048
    :cond_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    move-object/from16 v0, v32

    invoke-virtual {v6, v0, v10}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    .line 1051
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxRoot:Ljava/lang/String;
    invoke-static {v11}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/CloudFragment;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v11, v11, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    # setter for: Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;
    invoke-static {v6, v10}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->access$002(Lcom/sec/android/app/myfiles/fragment/CloudFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1052
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;
    invoke-static {v10}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CloudFragment;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 1053
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v6}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;
    invoke-static {v10}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CloudFragment;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentDirectory(Ljava/lang/String;)V

    .line 1055
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->AsyncOpen()V

    .line 1057
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v18

    .line 1059
    .local v18, "c":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 1061
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getSelectModeFrom()I

    move-result v6

    if-nez v6, :cond_2

    .line 1063
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->isSelectMode()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1065
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->finishSelectMode()V

    .line 1230
    .end local v18    # "c":Landroid/database/Cursor;
    :cond_2
    :goto_0
    return-void

    .line 1070
    :cond_3
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getSelectionType()I

    move-result v6

    const/4 v10, 0x1

    if-ne v6, v10, :cond_4

    .line 1072
    const v6, 0x7f0f0037

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    .line 1074
    .local v20, "checkBoxView":Landroid/view/View;
    if-eqz v20, :cond_2

    .line 1076
    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 1078
    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->performClick()Z

    goto :goto_0

    .line 1083
    .end local v20    # "checkBoxView":Landroid/view/View;
    :cond_4
    invoke-static/range {v32 .. v32}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v29

    .line 1084
    .local v29, "fileType":I
    invoke-static/range {v29 .. v29}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v6

    if-nez v6, :cond_6

    .line 1086
    invoke-static/range {v29 .. v29}, Lcom/sec/android/app/myfiles/MediaFile;->isArchiveFileType(I)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1087
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v6

    const v10, 0x7f0b00ec

    const/4 v11, 0x0

    invoke-static {v6, v10, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1091
    :cond_5
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    const/16 v10, 0xb

    move-object/from16 v0, v32

    invoke-virtual {v6, v0, v10}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->startDownload(Ljava/lang/String;I)V

    goto :goto_0

    .line 1093
    :cond_6
    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Images;->getContentUri()Landroid/net/Uri;

    move-result-object v5

    .line 1094
    .local v5, "PhotoUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 1095
    .local v4, "cr":Landroid/content/ContentResolver;
    const-string v7, "cloud_server_id  =  ?   COLLATE NOCASE "

    .line 1096
    .local v7, "where":Ljava/lang/String;
    const/4 v6, 0x1

    new-array v8, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v32, v8, v6

    .line 1097
    .local v8, "whereArgs":[Ljava/lang/String;
    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "_id"

    aput-object v11, v6, v10

    const/4 v10, 0x1

    const-string v11, "cloud_cached_path"

    aput-object v11, v6, v10

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 1101
    .local v22, "cloudDBcursor":Landroid/database/Cursor;
    if-eqz v22, :cond_2

    .line 1102
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1103
    const-string v6, "cloudDBcursor"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "cloudDBcursor.getCount() : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->getCount()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v6, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1104
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->getCount()I

    move-result v6

    const/4 v10, 0x1

    if-ne v6, v10, :cond_d

    .line 1105
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1106
    const-string v6, "_id"

    move-object/from16 v0, v22

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v22

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    .line 1107
    .local v21, "cid":I
    const-string v6, "cloud_cached_path"

    move-object/from16 v0, v22

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v22

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 1108
    .local v19, "cachedPath":Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v33

    .line 1109
    .local v33, "viewUri":Landroid/net/Uri;
    const-string v6, "mime_type"

    move-object/from16 v0, v23

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v23

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v31

    .line 1110
    .local v31, "mimetype":Ljava/lang/String;
    if-eqz v19, :cond_7

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_a

    .line 1112
    :cond_7
    move-object/from16 v15, v33

    .line 1114
    .local v15, "uri":Landroid/net/Uri;
    move-object/from16 v16, v31

    .line 1116
    .local v16, "mime":Ljava/lang/String;
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->mCachingCancelled:Z

    .line 1118
    new-instance v9, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3$2;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    new-instance v12, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3$1;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3$1;-><init>(Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;)V

    const/4 v13, 0x1

    const/4 v14, 0x0

    move-object/from16 v10, p0

    invoke-direct/range {v9 .. v16}, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3$2;-><init>(Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;Lcom/sec/android/app/myfiles/fragment/AbsFragment;Ljava/lang/Runnable;ZZLandroid/net/Uri;Ljava/lang/String;)V

    .line 1169
    .local v9, "cacheFileProgress":Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;
    if-eqz v9, :cond_9

    .line 1171
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v27

    .line 1173
    .local v27, "fManager":Landroid/app/FragmentManager;
    if-nez v27, :cond_8

    .line 1175
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getActivity()Landroid/app/Activity;

    move-result-object v17

    .line 1177
    .local v17, "activity":Landroid/app/Activity;
    if-eqz v17, :cond_8

    .line 1179
    invoke-virtual/range {v17 .. v17}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v27

    .line 1185
    .end local v17    # "activity":Landroid/app/Activity;
    :cond_8
    if-eqz v27, :cond_9

    .line 1187
    :try_start_0
    const-string v6, "CacheOperation"

    move-object/from16 v0, v27

    invoke-virtual {v9, v0, v6}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1195
    .end local v27    # "fManager":Landroid/app/FragmentManager;
    :cond_9
    :goto_1
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1190
    .restart local v27    # "fManager":Landroid/app/FragmentManager;
    :catch_0
    move-exception v26

    .line 1192
    .local v26, "ex":Ljava/lang/Exception;
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 1199
    .end local v9    # "cacheFileProgress":Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;
    .end local v15    # "uri":Landroid/net/Uri;
    .end local v16    # "mime":Ljava/lang/String;
    .end local v26    # "ex":Ljava/lang/Exception;
    .end local v27    # "fManager":Landroid/app/FragmentManager;
    :cond_a
    move-object/from16 v30, v33

    .line 1201
    .local v30, "fileUri":Landroid/net/Uri;
    new-instance v28, Ljava/io/File;

    move-object/from16 v0, v28

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1203
    .local v28, "file":Ljava/io/File;
    invoke-virtual/range {v28 .. v28}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_b

    .line 1205
    invoke-static/range {v28 .. v28}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v30

    .line 1209
    :cond_b
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move-object/from16 v2, v33

    move-object/from16 v3, v31

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->intentForImageOpen(Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;)V

    .line 1220
    .end local v19    # "cachedPath":Ljava/lang/String;
    .end local v21    # "cid":I
    .end local v28    # "file":Ljava/io/File;
    .end local v30    # "fileUri":Landroid/net/Uri;
    .end local v31    # "mimetype":Ljava/lang/String;
    .end local v33    # "viewUri":Landroid/net/Uri;
    :cond_c
    :goto_2
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1212
    :cond_d
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-nez v6, :cond_c

    invoke-static/range {v29 .. v29}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 1213
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    const/16 v10, 0xb

    move-object/from16 v0, v32

    invoke-virtual {v6, v0, v10}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->startDownload(Ljava/lang/String;I)V

    goto :goto_2

    .line 1227
    .end local v4    # "cr":Landroid/content/ContentResolver;
    .end local v5    # "PhotoUri":Landroid/net/Uri;
    .end local v7    # "where":Ljava/lang/String;
    .end local v8    # "whereArgs":[Ljava/lang/String;
    .end local v22    # "cloudDBcursor":Landroid/database/Cursor;
    .end local v29    # "fileType":I
    :cond_e
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-wide/from16 v0, v24

    move-object/from16 v2, v32

    invoke-virtual {v6, v0, v1, v2}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->multipleSelect(JLjava/lang/String;)V

    goto/16 :goto_0
.end method
