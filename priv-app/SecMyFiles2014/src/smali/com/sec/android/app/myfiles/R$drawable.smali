.class public final Lcom/sec/android/app/myfiles/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final actionbar_check_box_animator_list:I = 0x7f020000

.field public static final actionbar_ic_near_by_device:I = 0x7f020001

.field public static final actionbar_title_ripple:I = 0x7f020002

.field public static final air_view_zoom_in_btn_icon_delete:I = 0x7f020003

.field public static final air_view_zoom_in_btn_icon_delete_focused:I = 0x7f020004

.field public static final air_view_zoom_in_btn_icon_delete_pressed:I = 0x7f020005

.field public static final air_view_zoom_in_btn_icon_delete_selected:I = 0x7f020006

.field public static final air_view_zoom_in_btn_icon_share:I = 0x7f020007

.field public static final air_view_zoom_in_btn_icon_share_focused:I = 0x7f020008

.field public static final air_view_zoom_in_btn_icon_share_pressed:I = 0x7f020009

.field public static final air_view_zoom_in_btn_icon_share_selected:I = 0x7f02000a

.field public static final black:I = 0x7f02013b

.field public static final drag_shadow_background:I = 0x7f02000b

.field public static final dropdown_item_text_color:I = 0x7f02000c

.field public static final engraved_bg:I = 0x7f02000d

.field public static final gray:I = 0x7f02013c

.field public static final grid_selected:I = 0x7f02000e

.field public static final home_shortcut_bg:I = 0x7f02000f

.field public static final hover_popup_bg:I = 0x7f020010

.field public static final ic_btn_cancel:I = 0x7f020011

.field public static final ic_btn_cancel_disable:I = 0x7f020012

.field public static final ic_btn_home:I = 0x7f020013

.field public static final ic_btn_home_disable:I = 0x7f020014

.field public static final ic_btn_home_press:I = 0x7f020015

.field public static final ic_btn_ok:I = 0x7f020016

.field public static final ic_btn_ok_disable:I = 0x7f020017

.field public static final ic_btn_round_more_disabled:I = 0x7f020018

.field public static final ic_btn_round_more_disabled_focused:I = 0x7f020019

.field public static final ic_btn_round_more_focused:I = 0x7f02001a

.field public static final ic_btn_round_more_normal:I = 0x7f02001b

.field public static final ic_btn_round_more_pressed:I = 0x7f02001c

.field public static final ic_btn_up_folder:I = 0x7f02001d

.field public static final ic_btn_up_folder_disable:I = 0x7f02001e

.field public static final ic_btn_up_folder_press:I = 0x7f02001f

.field public static final ic_category_all_files:I = 0x7f020020

.field public static final ic_category_baidu:I = 0x7f020021

.field public static final ic_category_device_storage:I = 0x7f020022

.field public static final ic_category_documents:I = 0x7f020023

.field public static final ic_category_download_history:I = 0x7f020024

.field public static final ic_category_downloaded_apps:I = 0x7f020025

.field public static final ic_category_dropbox:I = 0x7f020026

.field public static final ic_category_ftp:I = 0x7f020027

.field public static final ic_category_images:I = 0x7f020028

.field public static final ic_category_music:I = 0x7f020029

.field public static final ic_category_personalpage:I = 0x7f02002a

.field public static final ic_category_recent_files:I = 0x7f02002b

.field public static final ic_category_remote_share:I = 0x7f02002c

.field public static final ic_category_remote_share_download:I = 0x7f02002d

.field public static final ic_category_remote_share_upload:I = 0x7f02002e

.field public static final ic_category_sd_storage:I = 0x7f02002f

.field public static final ic_category_shortcut:I = 0x7f020030

.field public static final ic_category_videos:I = 0x7f020031

.field public static final ic_folder_closed:I = 0x7f020032

.field public static final ic_homescreen_shortcut_item_dropbox:I = 0x7f020033

.field public static final ic_homescreen_shortcut_item_ftp:I = 0x7f020034

.field public static final ic_homescreen_shortcut_item_local:I = 0x7f020035

.field public static final ic_menu_add_shortcut:I = 0x7f020036

.field public static final ic_menu_copy:I = 0x7f020037

.field public static final ic_menu_create_folder:I = 0x7f020038

.field public static final ic_menu_delete:I = 0x7f020039

.field public static final ic_menu_detail:I = 0x7f02003a

.field public static final ic_menu_download:I = 0x7f02003b

.field public static final ic_menu_download_all:I = 0x7f02003c

.field public static final ic_menu_download_all_disabled:I = 0x7f02003d

.field public static final ic_menu_lock:I = 0x7f02003e

.field public static final ic_menu_move:I = 0x7f02003f

.field public static final ic_menu_move_knox:I = 0x7f020040

.field public static final ic_menu_move_personal:I = 0x7f020041

.field public static final ic_menu_rename:I = 0x7f020042

.field public static final ic_menu_search:I = 0x7f020043

.field public static final ic_menu_select_all:I = 0x7f020044

.field public static final ic_menu_settings:I = 0x7f020045

.field public static final ic_menu_share:I = 0x7f020046

.field public static final ic_menu_sort_by:I = 0x7f020047

.field public static final ic_menu_storage_usage:I = 0x7f020048

.field public static final ic_menu_unlock:I = 0x7f020049

.field public static final ic_menu_view_as:I = 0x7f02004a

.field public static final ic_more_option_ftp:I = 0x7f02004b

.field public static final ic_more_option_zip:I = 0x7f02004c

.field public static final ic_personal_contents:I = 0x7f02004d

.field public static final ic_shortcut_item_add:I = 0x7f02004e

.field public static final ic_shortcut_item_baidu:I = 0x7f02004f

.field public static final ic_shortcut_item_dropbox:I = 0x7f020050

.field public static final ic_shortcut_item_ftp:I = 0x7f020051

.field public static final ic_shortcut_item_local:I = 0x7f020052

.field public static final ic_splitview_baidu:I = 0x7f020053

.field public static final ic_splitview_documents:I = 0x7f020054

.field public static final ic_splitview_download_history:I = 0x7f020055

.field public static final ic_splitview_downloaded_apps:I = 0x7f020056

.field public static final ic_splitview_dropbox:I = 0x7f020057

.field public static final ic_splitview_folder:I = 0x7f020058

.field public static final ic_splitview_ftp:I = 0x7f020059

.field public static final ic_splitview_images:I = 0x7f02005a

.field public static final ic_splitview_music:I = 0x7f02005b

.field public static final ic_splitview_personalpage:I = 0x7f02005c

.field public static final ic_splitview_phonestorage:I = 0x7f02005d

.field public static final ic_splitview_recent_files:I = 0x7f02005e

.field public static final ic_splitview_sdstorage:I = 0x7f02005f

.field public static final ic_splitview_usb:I = 0x7f020060

.field public static final ic_splitview_video:I = 0x7f020061

.field public static final ic_storage_usage_dot_documents:I = 0x7f020062

.field public static final ic_storage_usage_dot_images:I = 0x7f020063

.field public static final ic_storage_usage_dot_music:I = 0x7f020064

.field public static final ic_storage_usage_dot_others:I = 0x7f020065

.field public static final ic_storage_usage_dot_videos:I = 0x7f020066

.field public static final ic_storage_usage_page_indicator_off:I = 0x7f020067

.field public static final ic_storage_usage_page_indicator_on:I = 0x7f020068

.field public static final ic_usb_storage:I = 0x7f020069

.field public static final left_shadow:I = 0x7f02006a

.field public static final listplay_playbutton:I = 0x7f02006b

.field public static final listplay_stopbutton:I = 0x7f02006c

.field public static final main_screen_icon_bg:I = 0x7f02006d

.field public static final main_screen_icon_bg_focus:I = 0x7f02006e

.field public static final main_screen_icon_bg_press:I = 0x7f02006f

.field public static final mirror_myfiles_list_subtitle_tab_arrow:I = 0x7f020070

.field public static final music_play_grid_icon_play_focus:I = 0x7f020071

.field public static final music_play_grid_icon_play_normal:I = 0x7f020072

.field public static final music_play_grid_icon_play_press:I = 0x7f020073

.field public static final music_play_grid_icon_stop_focus:I = 0x7f020074

.field public static final music_play_grid_icon_stop_normal:I = 0x7f020075

.field public static final music_play_grid_icon_stop_press:I = 0x7f020076

.field public static final music_play_progressive_bar_list_bg:I = 0x7f020077

.field public static final myfiles_action_bar_up_icon:I = 0x7f020078

.field public static final myfiles_drag_and_drop_bg:I = 0x7f020079

.field public static final myfiles_file_amr:I = 0x7f02007a

.field public static final myfiles_file_amr_thumb:I = 0x7f02007b

.field public static final myfiles_file_apk:I = 0x7f02007c

.field public static final myfiles_file_apk_thumb:I = 0x7f02007d

.field public static final myfiles_file_doc:I = 0x7f02007e

.field public static final myfiles_file_doc_thumb:I = 0x7f02007f

.field public static final myfiles_file_eml:I = 0x7f020080

.field public static final myfiles_file_eml_att:I = 0x7f020081

.field public static final myfiles_file_eml_att_thumb:I = 0x7f020082

.field public static final myfiles_file_eml_thumb:I = 0x7f020083

.field public static final myfiles_file_etc_lock:I = 0x7f020084

.field public static final myfiles_file_etc_thumb_lock:I = 0x7f020085

.field public static final myfiles_file_html:I = 0x7f020086

.field public static final myfiles_file_html_thumb:I = 0x7f020087

.field public static final myfiles_file_hwp:I = 0x7f020088

.field public static final myfiles_file_hwp_thumb:I = 0x7f020089

.field public static final myfiles_file_mp3:I = 0x7f02008a

.field public static final myfiles_file_mp3_thumb:I = 0x7f02008b

.field public static final myfiles_file_pdf:I = 0x7f02008c

.field public static final myfiles_file_pdf_thumb:I = 0x7f02008d

.field public static final myfiles_file_ppt:I = 0x7f02008e

.field public static final myfiles_file_ppt_thumb:I = 0x7f02008f

.field public static final myfiles_file_scrapbook:I = 0x7f020090

.field public static final myfiles_file_scrapbook_thumb:I = 0x7f020091

.field public static final myfiles_file_snb:I = 0x7f020092

.field public static final myfiles_file_snb_thumb:I = 0x7f020093

.field public static final myfiles_file_spd:I = 0x7f020094

.field public static final myfiles_file_spd_thumb:I = 0x7f020095

.field public static final myfiles_file_txt:I = 0x7f020096

.field public static final myfiles_file_txt_thumb:I = 0x7f020097

.field public static final myfiles_file_type_3gp:I = 0x7f020098

.field public static final myfiles_file_type_3gp_thumb:I = 0x7f020099

.field public static final myfiles_file_type_doc:I = 0x7f02009a

.field public static final myfiles_file_type_doc_thumb:I = 0x7f02009b

.field public static final myfiles_file_type_drm:I = 0x7f02009c

.field public static final myfiles_file_type_history_etc:I = 0x7f02009d

.field public static final myfiles_file_type_history_etc_thumb:I = 0x7f02009e

.field public static final myfiles_file_type_mp4:I = 0x7f02009f

.field public static final myfiles_file_type_mp4_thumb:I = 0x7f0200a0

.field public static final myfiles_file_type_ppt:I = 0x7f0200a1

.field public static final myfiles_file_type_ppt_thumb:I = 0x7f0200a2

.field public static final myfiles_file_type_txt:I = 0x7f0200a3

.field public static final myfiles_file_type_txt_thumb:I = 0x7f0200a4

.field public static final myfiles_file_type_xls:I = 0x7f0200a5

.field public static final myfiles_file_type_xls_thumb:I = 0x7f0200a6

.field public static final myfiles_file_xls:I = 0x7f0200a7

.field public static final myfiles_file_xls_thumb:I = 0x7f0200a8

.field public static final myfiles_header_icon_home:I = 0x7f0200a9

.field public static final myfiles_icon_audio:I = 0x7f0200aa

.field public static final myfiles_icon_audio_thumb:I = 0x7f0200ab

.field public static final myfiles_icon_drm_lock:I = 0x7f0200ac

.field public static final myfiles_icon_etc:I = 0x7f0200ad

.field public static final myfiles_icon_etc_thumb:I = 0x7f0200ae

.field public static final myfiles_icon_folder_normal:I = 0x7f0200af

.field public static final myfiles_icon_folder_normal_open:I = 0x7f0200b0

.field public static final myfiles_icon_folder_normal_thumb:I = 0x7f0200b1

.field public static final myfiles_icon_folder_sdcard:I = 0x7f0200b2

.field public static final myfiles_icon_folder_sdcard_thumb:I = 0x7f0200b3

.field public static final myfiles_icon_image:I = 0x7f0200b4

.field public static final myfiles_icon_image_thumb:I = 0x7f0200b5

.field public static final myfiles_icon_list_download:I = 0x7f0200b6

.field public static final myfiles_icon_list_download_thumb:I = 0x7f0200b7

.field public static final myfiles_icon_play:I = 0x7f0200b8

.field public static final myfiles_icon_storyalbum:I = 0x7f0200b9

.field public static final myfiles_icon_storyalbum_thumb:I = 0x7f0200ba

.field public static final myfiles_icon_task:I = 0x7f0200bb

.field public static final myfiles_icon_task_thumb:I = 0x7f0200bc

.field public static final myfiles_icon_unsupport:I = 0x7f0200bd

.field public static final myfiles_icon_vcalendar:I = 0x7f0200be

.field public static final myfiles_icon_vcalendar_thumb:I = 0x7f0200bf

.field public static final myfiles_icon_vcard:I = 0x7f0200c0

.field public static final myfiles_icon_vcard_thumb:I = 0x7f0200c1

.field public static final myfiles_icon_video:I = 0x7f0200c2

.field public static final myfiles_icon_video_thumb:I = 0x7f0200c3

.field public static final myfiles_icon_vnote:I = 0x7f0200c4

.field public static final myfiles_icon_vnote_thumb:I = 0x7f0200c5

.field public static final myfiles_icon_zip:I = 0x7f0200c6

.field public static final myfiles_icon_zip_thumb:I = 0x7f0200c7

.field public static final myfiles_list_btn_up_folder:I = 0x7f0200c8

.field public static final myfiles_list_btn_up_folder_dim:I = 0x7f0200c9

.field public static final myfiles_list_btn_up_folder_focus:I = 0x7f0200ca

.field public static final myfiles_list_btn_up_folder_press:I = 0x7f0200cb

.field public static final myfiles_list_ftp:I = 0x7f0200cc

.field public static final myfiles_list_subtitle_tab_arrow:I = 0x7f0200cd

.field public static final myfiles_move_bg_text:I = 0x7f0200ce

.field public static final myfiles_noitems:I = 0x7f0200cf

.field public static final myfiles_splash:I = 0x7f0200d0

.field public static final myfiles_thumb_doc:I = 0x7f0200d1

.field public static final myfiles_thumb_ftp:I = 0x7f0200d2

.field public static final myfiles_thumb_hwp:I = 0x7f0200d3

.field public static final myfiles_thumb_pdf:I = 0x7f0200d4

.field public static final myfiles_thumb_play:I = 0x7f0200d5

.field public static final myfiles_thumb_ppt:I = 0x7f0200d6

.field public static final myfiles_thumb_txt:I = 0x7f0200d7

.field public static final myfiles_thumb_xls:I = 0x7f0200d8

.field public static final no_item_l:I = 0x7f0200d9

.field public static final no_item_v:I = 0x7f0200da

.field public static final page_left_arrow:I = 0x7f0200db

.field public static final page_right_arrow:I = 0x7f0200dc

.field public static final password_field_default:I = 0x7f0200dd

.field public static final path_indicator_button_background_normal:I = 0x7f0200de

.field public static final path_indicator_button_background_pressed:I = 0x7f0200df

.field public static final security_home_icon_security:I = 0x7f0200e0

.field public static final security_home_icon_security_02:I = 0x7f0200e1

.field public static final shortcut_item_text_background:I = 0x7f0200e2

.field public static final shortcut_panel_background:I = 0x7f0200e3

.field public static final split_bar:I = 0x7f0200e4

.field public static final split_bar_pressed:I = 0x7f0200e5

.field public static final split_view_left_arrow:I = 0x7f0200e6

.field public static final split_view_left_arrow_pressed:I = 0x7f0200e7

.field public static final splitbar:I = 0x7f0200e8

.field public static final storage_usage_bar_background:I = 0x7f0200e9

.field public static final storage_usage_bar_block_documents:I = 0x7f0200ea

.field public static final storage_usage_bar_block_images:I = 0x7f0200eb

.field public static final storage_usage_bar_block_music:I = 0x7f0200ec

.field public static final storage_usage_bar_block_others:I = 0x7f0200ed

.field public static final storage_usage_bar_block_videos:I = 0x7f0200ee

.field public static final storage_usage_bar_mask:I = 0x7f0200ef

.field public static final thumbnail_focus:I = 0x7f0200f0

.field public static final thumbnail_ic_personal:I = 0x7f0200f1

.field public static final title_bar_background:I = 0x7f0200f2

.field public static final translucent_background:I = 0x7f020138

.field public static final translucent_background50:I = 0x7f020139

.field public static final tree_view_expandable_indicator:I = 0x7f0200f3

.field public static final tree_view_expanded_indicator:I = 0x7f0200f4

.field public static final tw_ab_bottom_transparent:I = 0x7f0200f5

.field public static final tw_ab_transparent:I = 0x7f0200f6

.field public static final tw_ab_transparent_dark_holo:I = 0x7f0200f7

.field public static final tw_action_item_background_focused_holo:I = 0x7f0200f8

.field public static final tw_action_item_background_focused_holo_dark:I = 0x7f0200f9

.field public static final tw_action_item_background_pressed_holo:I = 0x7f0200fa

.field public static final tw_action_item_background_pressed_holo_dark:I = 0x7f0200fb

.field public static final tw_btn_borderless_holo_dark:I = 0x7f0200fc

.field public static final tw_btn_check_to_on_mtrl_000:I = 0x7f0200fd

.field public static final tw_btn_check_to_on_mtrl_001:I = 0x7f0200fe

.field public static final tw_btn_check_to_on_mtrl_002:I = 0x7f0200ff

.field public static final tw_btn_check_to_on_mtrl_003:I = 0x7f020100

.field public static final tw_btn_check_to_on_mtrl_004:I = 0x7f020101

.field public static final tw_btn_check_to_on_mtrl_005:I = 0x7f020102

.field public static final tw_btn_check_to_on_mtrl_006:I = 0x7f020103

.field public static final tw_btn_check_to_on_mtrl_007:I = 0x7f020104

.field public static final tw_btn_check_to_on_mtrl_008:I = 0x7f020105

.field public static final tw_btn_check_to_on_mtrl_009:I = 0x7f020106

.field public static final tw_btn_check_to_on_mtrl_010:I = 0x7f020107

.field public static final tw_btn_check_to_on_mtrl_011:I = 0x7f020108

.field public static final tw_btn_check_to_on_mtrl_012:I = 0x7f020109

.field public static final tw_btn_check_to_on_mtrl_013:I = 0x7f02010a

.field public static final tw_btn_check_to_on_mtrl_014:I = 0x7f02010b

.field public static final tw_btn_check_to_on_mtrl_015:I = 0x7f02010c

.field public static final tw_btn_default_disabled_focused_holo_dark:I = 0x7f02010d

.field public static final tw_btn_default_disabled_holo_dark:I = 0x7f02010e

.field public static final tw_btn_default_focused_holo_dark:I = 0x7f02010f

.field public static final tw_btn_default_pressed_holo_dark:I = 0x7f020110

.field public static final tw_buttonbarbutton_selector_default:I = 0x7f020111

.field public static final tw_buttonbarbutton_selector_default_holo:I = 0x7f020112

.field public static final tw_buttonbarbutton_selector_disabled:I = 0x7f020113

.field public static final tw_buttonbarbutton_selector_disabled_holo:I = 0x7f020114

.field public static final tw_buttonbarbutton_selector_focused:I = 0x7f020115

.field public static final tw_buttonbarbutton_selector_focused_holo:I = 0x7f020116

.field public static final tw_buttonbarbutton_selector_pressed:I = 0x7f020117

.field public static final tw_buttonbarbutton_selector_pressed_holo:I = 0x7f020118

.field public static final tw_cab_background_top:I = 0x7f020119

.field public static final tw_cab_background_top_holo:I = 0x7f02011a

.field public static final tw_dialog_list_section_divider:I = 0x7f02011b

.field public static final tw_dialog_list_section_divider_holo:I = 0x7f02011c

.field public static final tw_ic_clear_search_api:I = 0x7f02011d

.field public static final tw_list_disabled:I = 0x7f02011e

.field public static final tw_list_divider:I = 0x7f02011f

.field public static final tw_list_divider_holo:I = 0x7f020120

.field public static final tw_list_focused:I = 0x7f020121

.field public static final tw_list_focused_holo:I = 0x7f020122

.field public static final tw_list_icon_create:I = 0x7f020123

.field public static final tw_list_pressed:I = 0x7f020124

.field public static final tw_list_pressed_holo:I = 0x7f020125

.field public static final tw_list_selected:I = 0x7f020126

.field public static final tw_no_item_bg_l:I = 0x7f020127

.field public static final tw_no_item_bg_v:I = 0x7f020128

.field public static final tw_preference_header_list_arrow_focused_holo:I = 0x7f020129

.field public static final tw_preference_header_list_arrow_focused_holo_dark:I = 0x7f02012a

.field public static final tw_preference_header_list_arrow_pressed_holo:I = 0x7f02012b

.field public static final tw_preference_header_list_arrow_pressed_holo_dark:I = 0x7f02012c

.field public static final tw_searchfield_background_holo:I = 0x7f02012d

.field public static final tw_searchfiled_background:I = 0x7f02012e

.field public static final tw_select_all_bg:I = 0x7f02012f

.field public static final tw_select_all_bg_holo:I = 0x7f020130

.field public static final tw_spinner_ab_default_am:I = 0x7f020131

.field public static final tw_spinner_ab_disabled_am:I = 0x7f020132

.field public static final tw_spinner_ab_focused_am:I = 0x7f020133

.field public static final tw_spinner_ab_pressed_am:I = 0x7f020134

.field public static final tw_split_list_selected_holo:I = 0x7f020135

.field public static final tw_sub_action_bar_bg:I = 0x7f020136

.field public static final tw_sub_action_bar_bg_holo:I = 0x7f020137

.field public static final white:I = 0x7f02013a


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
