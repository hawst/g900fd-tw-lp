.class public final Lcom/sec/android/app/myfiles/R$plurals;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "plurals"
.end annotation


# static fields
.field public static final copy_file_complete:I = 0x7f0c0006

.field public static final copy_folder_complete:I = 0x7f0c0007

.field public static final copy_item_complete:I = 0x7f0c0005

.field public static final delete_file:I = 0x7f0c0010

.field public static final delete_folder:I = 0x7f0c000f

.field public static final delete_item_complete:I = 0x7f0c000b

.field public static final delete_item_num:I = 0x7f0c0004

.field public static final device_removed:I = 0x7f0c0014

.field public static final device_will_be_removed:I = 0x7f0c001a

.field public static final file_deleted:I = 0x7f0c000e

.field public static final file_will_be_deleted:I = 0x7f0c0011

.field public static final files:I = 0x7f0c0000

.field public static final folder_deleted:I = 0x7f0c000d

.field public static final folder_will_be_deleted:I = 0x7f0c0012

.field public static final folders:I = 0x7f0c0001

.field public static final ftp_server_removed:I = 0x7f0c0015

.field public static final ftp_server_will_be_removed:I = 0x7f0c0019

.field public static final item_num:I = 0x7f0c0002

.field public static final move_file_complete:I = 0x7f0c0009

.field public static final move_folder_complete:I = 0x7f0c000a

.field public static final move_item_complete:I = 0x7f0c0008

.field public static final moved_KNOX:I = 0x7f0c001c

.field public static final moved_personal:I = 0x7f0c001b

.field public static final nearby_devices_download_request:I = 0x7f0c0003

.field public static final remove_ftp_server:I = 0x7f0c0016

.field public static final remove_nearby_device:I = 0x7f0c0017

.field public static final shortcut_deleted:I = 0x7f0c0013

.field public static final shortcut_will_be_deleted:I = 0x7f0c0018

.field public static final uninstall_app_complete:I = 0x7f0c000c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1999
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
