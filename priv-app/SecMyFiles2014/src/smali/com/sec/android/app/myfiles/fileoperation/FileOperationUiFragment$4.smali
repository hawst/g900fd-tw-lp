.class Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$4;
.super Landroid/content/BroadcastReceiver;
.source "FileOperationUiFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setAbortReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;)V
    .locals 0

    .prologue
    .line 249
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$4;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 254
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 256
    .local v0, "action":Ljava/lang/String;
    const/4 v1, 0x2

    const-string v2, "FileOperationUiFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Broadcast receive : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 258
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 260
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$4;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFolder:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxFolder(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretMode(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 262
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$4;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->cancel()V

    .line 264
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$4;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$4;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    if-eqz v1, :cond_0

    .line 266
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$4;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$4;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 271
    :cond_0
    const-string v1, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 273
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$4;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$4;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    if-eqz v1, :cond_1

    .line 277
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$4;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$4;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 283
    :cond_1
    return-void
.end method
