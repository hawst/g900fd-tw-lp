.class Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;
.super Ljava/lang/Object;
.source "SearchFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/SearchFragment;->onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)V
    .locals 0

    .prologue
    .line 517
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 28
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 522
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual/range {p1 .. p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v6

    move/from16 v0, p3

    invoke-interface {v6, v0}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/database/Cursor;

    .line 524
    .local v13, "cursor":Landroid/database/Cursor;
    const-string v6, "_data"

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 526
    .local v22, "path":Ljava/lang/String;
    const-string v6, "/storage"

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "format"

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const v9, 0x7f0b0007

    if-ne v6, v9, :cond_6

    .line 529
    :cond_0
    const-string v6, "format"

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const/16 v9, 0x3001

    if-ne v6, v9, :cond_2

    .line 532
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    instance-of v6, v6, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v6, :cond_1

    .line 534
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 535
    .local v10, "argument":Landroid/os/Bundle;
    const-string v6, "FOLDERPATH"

    move-object/from16 v0, v22

    invoke-virtual {v10, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    const-string v6, "is_from_search_intent_key"

    const/4 v9, 0x1

    invoke-virtual {v10, v6, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 537
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/myfiles/MainActivity;

    const/16 v9, 0x201

    invoke-virtual {v6, v9, v10}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    .line 538
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v6}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v6

    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentDirectory(Ljava/lang/String;)V

    .line 661
    .end local v10    # "argument":Landroid/os/Bundle;
    :cond_1
    :goto_0
    return-void

    .line 541
    :cond_2
    const-string v6, "format"

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const v9, 0x7f0b0007

    if-ne v6, v9, :cond_3

    .line 543
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v23

    .line 545
    .local v23, "pm":Landroid/content/pm/PackageManager;
    const-string v6, "_data"

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v20

    .line 547
    .local v20, "launchIntent":Landroid/content/Intent;
    if-eqz v20, :cond_1

    .line 549
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 552
    .end local v20    # "launchIntent":Landroid/content/Intent;
    .end local v23    # "pm":Landroid/content/pm/PackageManager;
    :cond_3
    const-string v6, "content_search"

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    const/4 v9, -0x1

    if-eq v6, v9, :cond_4

    const-string v6, "content_search"

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v9, "content_search"

    invoke-virtual {v6, v9}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_4

    .line 555
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v6}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 556
    const/16 v4, 0x11

    new-instance v5, Ljava/io/File;

    move-object/from16 v0, v22

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getCurrentSortBy()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSavedSearchKeyWord:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)Ljava/lang/String;

    move-result-object v9

    invoke-static/range {v4 .. v9}, Lcom/sec/android/app/myfiles/utils/FileUtils;->openFile(ILjava/io/File;Landroid/app/Activity;ZILjava/lang/String;)V

    goto :goto_0

    .line 560
    :cond_4
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v6}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 562
    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v18

    .line 564
    .local v18, "fileTypeInt":I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/myfiles/MediaFile;->isArchiveFileType(I)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 565
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    move-object/from16 v0, v22

    # invokes: Lcom/sec/android/app/myfiles/fragment/SearchFragment;->createFolderForExtract(Ljava/lang/String;)Z
    invoke-static {v6, v0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/SearchFragment;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 567
    :cond_5
    const/16 v6, 0x11

    new-instance v9, Ljava/io/File;

    move-object/from16 v0, v22

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v25

    const/16 v26, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getCurrentSortBy()I

    move-result v27

    move-object/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-static {v6, v9, v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/FileUtils;->openFile(ILjava/io/File;Landroid/app/Activity;ZI)V

    goto/16 :goto_0

    .line 571
    .end local v18    # "fileTypeInt":I
    :cond_6
    const-string v6, "_id"

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 572
    .local v14, "cursorId":J
    const-string v6, "format"

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const/16 v9, 0x3001

    if-ne v6, v9, :cond_8

    .line 573
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    instance-of v6, v6, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v6, :cond_1

    .line 575
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 576
    .restart local v10    # "argument":Landroid/os/Bundle;
    const-string v6, "FOLDERPATH"

    move-object/from16 v0, v22

    invoke-virtual {v10, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    const-string v6, "search_option_type"

    const/4 v9, 0x6

    invoke-virtual {v10, v6, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 579
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 580
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/myfiles/MainActivity;

    const/16 v9, 0x1f

    invoke-virtual {v6, v9, v10}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    .line 584
    :goto_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v6}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v6

    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentDirectory(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 582
    :cond_7
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/myfiles/MainActivity;

    const/16 v9, 0x8

    invoke-virtual {v6, v9, v10}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto :goto_1

    .line 588
    .end local v10    # "argument":Landroid/os/Bundle;
    :cond_8
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getSelectionType()I

    move-result v6

    const/4 v9, 0x1

    if-ne v6, v9, :cond_9

    .line 590
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v0, v22

    invoke-virtual {v6, v14, v15, v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->multipleSelect(JLjava/lang/String;)V

    goto/16 :goto_0

    .line 594
    :cond_9
    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v17

    .line 595
    .local v17, "fileType":I
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v6

    if-nez v6, :cond_b

    .line 597
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/myfiles/MediaFile;->isArchiveFileType(I)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 598
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v6

    const v9, 0x7f0b00ec

    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-static {v6, v9, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 602
    :cond_a
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    const/16 v9, 0xb

    move-object/from16 v0, v22

    invoke-virtual {v6, v0, v9}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->startDownload(Ljava/lang/String;I)V

    .line 604
    :cond_b
    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Images;->getContentUri()Landroid/net/Uri;

    move-result-object v5

    .line 605
    .local v5, "PhotoUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 607
    .local v4, "cr":Landroid/content/ContentResolver;
    const-string v7, "cloud_server_id  =  ?   COLLATE NOCASE "

    .line 609
    .local v7, "where":Ljava/lang/String;
    const/4 v6, 0x1

    new-array v8, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v22, v8, v6

    .line 610
    .local v8, "whereArgs":[Ljava/lang/String;
    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v25, "_id"

    aput-object v25, v6, v9

    const/4 v9, 0x1

    const-string v25, "_data"

    aput-object v25, v6, v9

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 612
    .local v12, "cloudDBcursor":Landroid/database/Cursor;
    if-eqz v12, :cond_1

    .line 613
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 614
    const-string v6, "cloudDBcursor"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "cloudDBcursor.getCount() : "

    move-object/from16 v0, v25

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v25

    move/from16 v0, v25

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v6

    const/4 v9, 0x1

    if-ne v6, v9, :cond_d

    .line 618
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 619
    const/4 v6, 0x0

    invoke-interface {v12, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 620
    .local v11, "cid":I
    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v24

    .line 622
    .local v24, "view_uri":Landroid/net/Uri;
    const-string v6, "mime_type"

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 625
    .local v21, "mimetype":Ljava/lang/String;
    new-instance v6, Landroid/content/Intent;

    const-string v9, "android.intent.action.VIEW"

    invoke-direct {v6, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    move-object/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v19

    .line 629
    .local v19, "intent":Landroid/content/Intent;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 657
    .end local v11    # "cid":I
    .end local v19    # "intent":Landroid/content/Intent;
    .end local v21    # "mimetype":Ljava/lang/String;
    .end local v24    # "view_uri":Landroid/net/Uri;
    :cond_c
    :goto_2
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 631
    .restart local v11    # "cid":I
    .restart local v19    # "intent":Landroid/content/Intent;
    .restart local v21    # "mimetype":Ljava/lang/String;
    .restart local v24    # "view_uri":Landroid/net/Uri;
    :catch_0
    move-exception v16

    .line 635
    .local v16, "e":Ljava/lang/Exception;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 641
    .end local v11    # "cid":I
    .end local v16    # "e":Ljava/lang/Exception;
    .end local v19    # "intent":Landroid/content/Intent;
    .end local v21    # "mimetype":Ljava/lang/String;
    .end local v24    # "view_uri":Landroid/net/Uri;
    :cond_d
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-nez v6, :cond_c

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 649
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    const/16 v9, 0xb

    move-object/from16 v0, v22

    invoke-virtual {v6, v0, v9}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->startDownload(Ljava/lang/String;I)V

    goto :goto_2
.end method
