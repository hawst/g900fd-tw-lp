.class Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$1;
.super Ljava/lang/Object;
.source "AbsBrowserFragment.java"

# interfaces
.implements Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V
    .locals 0

    .prologue
    .line 444
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    .line 448
    const-string v8, "AbsBrowserFragment"

    const-string v9, "MainActivity : SLOOK - TYPE_SCONNECTOR"

    invoke-static {v10, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 449
    const/4 v5, 0x0

    .line 450
    .local v5, "mSelectedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 451
    .local v3, "intent":Landroid/content/Intent;
    const-string v8, "com.samsung.android.sconnect.START"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 452
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v8, v8, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    instance-of v8, v8, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    if-nez v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v8, v8, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v8, v8, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->isSelectMode()Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v8, v8, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v8, v8, Lcom/sec/android/app/myfiles/SelectorActivity;

    if-eqz v8, :cond_2

    .line 456
    :cond_0
    const-string v8, "AbsBrowserFragment"

    const-string v9, "MainActivity : SLOOK - no selected item or Activity is SelectorActivity or selection is on NearbyDevice"

    invoke-static {v10, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 479
    :goto_0
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->isAdded()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 481
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v8, v3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 486
    :cond_1
    :goto_1
    return-void

    .line 458
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v8, v8, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v5

    .line 459
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 461
    .local v7, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v4, 0x0

    .line 462
    .local v4, "isDirectory":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v2, v8, :cond_4

    .line 463
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .line 464
    .local v6, "selectedItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    new-instance v1, Ljava/io/File;

    iget-object v8, v6, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-direct {v1, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 465
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 466
    const/4 v4, 0x1

    .line 468
    :cond_3
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 462
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 470
    .end local v1    # "file":Ljava/io/File;
    .end local v6    # "selectedItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    :cond_4
    if-nez v4, :cond_5

    .line 471
    const-string v8, "android.intent.extra.STREAM"

    invoke-virtual {v3, v8, v7}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 473
    const-string v8, "AbsBrowserFragment"

    const-string v9, "MainActivity : SLOOK - select item is file"

    invoke-static {v10, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 475
    :cond_5
    const-string v8, "AbsBrowserFragment"

    const-string v9, "MainActivity : SLOOK - select item is contains dir"

    invoke-static {v10, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 482
    .end local v2    # "i":I
    .end local v4    # "isDirectory":Z
    .end local v7    # "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :catch_0
    move-exception v0

    .line 483
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_1
.end method
