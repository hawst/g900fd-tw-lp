.class Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$8;
.super Ljava/lang/Object;
.source "RemoteShareFragment.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->onCreateSelectedItemChangeListener()Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)V
    .locals 0

    .prologue
    .line 835
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public multipleSelectionChanged(II)V
    .locals 8
    .param p1, "totalItems"    # I
    .param p2, "selectedItems"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 839
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->invalidateOptionsMenu()V

    .line 841
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v3, v3, Lcom/sec/android/app/myfiles/MainActivity;

    if-nez v3, :cond_1

    .line 902
    :cond_0
    :goto_0
    return-void

    .line 844
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v0, v3, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    .line 847
    .local v0, "activity":Lcom/sec/android/app/myfiles/MainActivity;
    iget-object v1, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeZip:Landroid/view/MenuItem;

    .local v1, "menuItem":Landroid/view/MenuItem;
    if-eqz v1, :cond_2

    .line 848
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 850
    :cond_2
    iget-object v1, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeExtract:Landroid/view/MenuItem;

    if-eqz v1, :cond_3

    .line 851
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 853
    :cond_3
    iget-object v1, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeShare:Landroid/view/MenuItem;

    if-eqz v1, :cond_4

    .line 854
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 856
    :cond_4
    iget-object v1, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    if-eqz v1, :cond_5

    .line 857
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 859
    :cond_5
    iget-object v1, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeMove:Landroid/view/MenuItem;

    if-eqz v1, :cond_6

    .line 860
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 862
    :cond_6
    iget-object v1, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeCopy:Landroid/view/MenuItem;

    if-eqz v1, :cond_7

    .line 863
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 865
    :cond_7
    iget-object v1, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeRename:Landroid/view/MenuItem;

    if-eqz v1, :cond_8

    .line 866
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 868
    :cond_8
    iget-object v1, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeExtractHere:Landroid/view/MenuItem;

    if-eqz v1, :cond_9

    .line 869
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 871
    :cond_9
    iget-object v1, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeFtpEdit:Landroid/view/MenuItem;

    if-eqz v1, :cond_a

    .line 872
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 874
    :cond_a
    iget-object v1, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDetail:Landroid/view/MenuItem;

    if-eqz v1, :cond_c

    .line 875
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isSelectMode()Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isSelectMode()Z

    move-result v3

    if-eqz v3, :cond_12

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v3

    if-nez v3, :cond_12

    .line 877
    :cond_b
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 882
    :cond_c
    :goto_1
    iget-object v1, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeAddShortcut:Landroid/view/MenuItem;

    if-eqz v1, :cond_d

    .line 883
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 885
    :cond_d
    iget-object v1, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeAddShortcutHome:Landroid/view/MenuItem;

    if-eqz v1, :cond_e

    .line 886
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 888
    :cond_e
    iget-object v1, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDownload:Landroid/view/MenuItem;

    if-eqz v1, :cond_11

    .line 889
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_10

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isSelectMode()Z

    move-result v3

    if-eqz v3, :cond_f

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v3

    if-eqz v3, :cond_10

    :cond_f
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mSelectableItemsCount:I
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->access$900(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)I

    move-result v3

    if-nez v3, :cond_13

    .line 892
    :cond_10
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 897
    :cond_11
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0038

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 898
    .local v2, "msg":Ljava/lang/String;
    iget-object v3, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    if-eqz v3, :cond_0

    .line 899
    iget-object v3, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 900
    invoke-virtual {v0, v2}, Lcom/sec/android/app/myfiles/MainActivity;->setSpinnerWidth(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 878
    .end local v2    # "msg":Ljava/lang/String;
    :cond_12
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isSelectMode()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 879
    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 894
    :cond_13
    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_2
.end method

.method public singleSelectionChanged(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 906
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->invalidateOptionsMenu()V

    .line 907
    return-void
.end method
