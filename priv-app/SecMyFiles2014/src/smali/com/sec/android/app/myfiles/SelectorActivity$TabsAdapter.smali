.class public Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;
.super Landroid/support/v13/app/FragmentPagerAdapter;
.source "SelectorActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Landroid/widget/TabHost$OnTabChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/SelectorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TabsAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$DummyTabFactory;,
        Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;
    }
.end annotation


# instance fields
.field private firstOpen:Z

.field private final mContext:Landroid/content/Context;

.field private mCurrentFtpFragment:Landroid/app/Fragment;

.field private mCurrentShortcutFragment:Landroid/app/Fragment;

.field private final mFragmentInstanceMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field private mFtpBrowserTabInfo:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;

.field private mFtpPosition:I

.field private mFtpSelectedPath:Ljava/lang/String;

.field private mFtpShortcutTabInfo:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;

.field private mFtpTabMode:Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;

.field private mNeedToShowFtpBrowser:Z

.field private mShortcutBrowserTabInfo:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;

.field private mShortcutPosition:I

.field private mShortcutTabInfo:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;

.field private mShortcutTabMode:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

.field private final mTabHost:Landroid/widget/TabHost;

.field private final mTabs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mViewPager:Landroid/support/v4/view/ViewPager;

.field final synthetic this$0:Lcom/sec/android/app/myfiles/SelectorActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/SelectorActivity;Landroid/app/Activity;Landroid/widget/TabHost;Landroid/support/v4/view/ViewPager;)V
    .locals 2
    .param p2, "activity"    # Landroid/app/Activity;
    .param p3, "tabHost"    # Landroid/widget/TabHost;
    .param p4, "pager"    # Landroid/support/v4/view/ViewPager;

    .prologue
    const/4 v1, -0x1

    .line 862
    iput-object p1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    .line 863
    invoke-virtual {p2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v13/app/FragmentPagerAdapter;-><init>(Landroid/app/FragmentManager;)V

    .line 786
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->firstOpen:Z

    .line 794
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mTabs:Ljava/util/ArrayList;

    .line 796
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFragmentInstanceMap:Ljava/util/HashMap;

    .line 798
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mNeedToShowFtpBrowser:Z

    .line 800
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpSelectedPath:Ljava/lang/String;

    .line 808
    sget-object v0, Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;->SHORTCUT:Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpTabMode:Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;

    .line 810
    sget-object v0, Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;->SHORTCUT:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mShortcutTabMode:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    .line 812
    iput v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpPosition:I

    .line 814
    iput v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mShortcutPosition:I

    .line 865
    iput-object p2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mContext:Landroid/content/Context;

    .line 867
    iput-object p3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mTabHost:Landroid/widget/TabHost;

    .line 869
    iput-object p4, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 871
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v0, p0}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    .line 873
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 875
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 876
    return-void
.end method

.method private createSelectOptionClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 1191
    new-instance v0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$1;-><init>(Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;)V

    return-object v0
.end method


# virtual methods
.method public addTab(Landroid/widget/TabHost$TabSpec;Ljava/lang/Class;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "tabSpec"    # Landroid/widget/TabHost$TabSpec;
    .param p3, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TabHost$TabSpec;",
            "Ljava/lang/Class",
            "<*>;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 882
    .local p2, "clss":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :try_start_0
    new-instance v3, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$DummyTabFactory;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v3, p0, v4}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$DummyTabFactory;-><init>(Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;Landroid/content/Context;)V

    invoke-virtual {p1, v3}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/widget/TabHost$TabContentFactory;)Landroid/widget/TabHost$TabSpec;

    .line 884
    invoke-virtual {p1}, Landroid/widget/TabHost$TabSpec;->getTag()Ljava/lang/String;

    move-result-object v2

    .line 886
    .local v2, "tag":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;

    invoke-direct {v1, p0, v2, p2, p3}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;-><init>(Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 888
    .local v1, "info":Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;
    const-string v3, "ftp-shortcut"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 890
    iput-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpShortcutTabInfo:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;

    .line 895
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mTabs:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 897
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mTabMap:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$000(Lcom/sec/android/app/myfiles/SelectorActivity;)Ljava/util/HashMap;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mTabMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$000(Lcom/sec/android/app/myfiles/SelectorActivity;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 899
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v3, p1}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 901
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->notifyDataSetChanged()V

    .line 907
    .end local v1    # "info":Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;
    .end local v2    # "tag":Ljava/lang/String;
    :goto_1
    return-void

    .line 891
    .restart local v1    # "info":Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;
    .restart local v2    # "tag":Ljava/lang/String;
    :cond_1
    const-string v3, "user-shortcut"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 892
    iput-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mShortcutTabInfo:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 903
    .end local v1    # "info":Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;
    .end local v2    # "tag":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 905
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public finishUpdate(Landroid/view/ViewGroup;)V
    .locals 15
    .param p1, "container"    # Landroid/view/ViewGroup;

    .prologue
    .line 1210
    invoke-super/range {p0 .. p1}, Landroid/support/v13/app/FragmentPagerAdapter;->finishUpdate(Landroid/view/ViewGroup;)V

    .line 1212
    iget-boolean v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->firstOpen:Z

    const/4 v11, 0x1

    if-ne v10, v11, :cond_1

    .line 1214
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->getCount()I

    move-result v10

    const/4 v11, 0x2

    if-le v10, v11, :cond_0

    .line 1216
    iget-object v11, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    const/4 v10, 0x1

    invoke-virtual {p0, v10}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->getItem(I)Landroid/app/Fragment;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    iput-object v10, v11, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 1221
    :cond_0
    iget-object v11, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->getItem(I)Landroid/app/Fragment;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    iput-object v10, v11, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 1227
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->firstOpen:Z

    .line 1228
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->selectmodeActionbar()V

    .line 1231
    :cond_1
    const/4 v10, 0x1

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->MODULE:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$1100()Ljava/lang/String;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getActionBar: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/SelectorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1233
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectionType:I
    invoke-static {v10}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$400(Lcom/sec/android/app/myfiles/SelectorActivity;)I

    move-result v10

    const/4 v11, 0x1

    if-le v10, v11, :cond_e

    .line 1234
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v10, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCreateFolderContainer:Landroid/widget/LinearLayout;

    if-eqz v10, :cond_2

    .line 1235
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v10, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCreateFolderContainer:Landroid/widget/LinearLayout;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1236
    :cond_2
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v10, :cond_4

    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/SelectorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v10

    const v11, 0x7f0f0009

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    if-eqz v10, :cond_4

    .line 1237
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v10, v10, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;

    if-nez v10, :cond_3

    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v10, v10, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;

    if-eqz v10, :cond_8

    .line 1238
    :cond_3
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/SelectorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v10

    const v11, 0x7f0f0009

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/View;->setEnabled(Z)V

    .line 1239
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v10, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCreateFolderContainer:Landroid/widget/LinearLayout;

    if-eqz v10, :cond_4

    .line 1240
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v10, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCreateFolderContainer:Landroid/widget/LinearLayout;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1390
    :cond_4
    :goto_0
    iget-boolean v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mNeedToShowFtpBrowser:Z

    if-eqz v10, :cond_7

    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v10}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v10

    iget v11, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpPosition:I

    if-ne v10, v11, :cond_7

    .line 1392
    const/4 v9, 0x0

    .line 1394
    .local v9, "needToResetPath":Z
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFragmentInstanceMap:Ljava/util/HashMap;

    iget v11, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpPosition:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    if-eqz v10, :cond_5

    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFragmentInstanceMap:Ljava/util/HashMap;

    iget v11, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpPosition:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    instance-of v10, v10, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    if-eqz v10, :cond_5

    .line 1396
    const/4 v9, 0x1

    .line 1399
    :cond_5
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mNeedToShowFtpBrowser:Z

    .line 1401
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1403
    .local v1, "arg":Landroid/os/Bundle;
    const-string v10, "id"

    const/16 v11, 0xa

    invoke-virtual {v1, v10, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1405
    const-string v10, "run_from"

    const/16 v11, 0x15

    invoke-virtual {v1, v10, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1407
    const-string v10, "FOLDERPATH"

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Security;->getInstance()Lcom/sec/android/app/myfiles/utils/Security;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpSelectedPath:Ljava/lang/String;

    invoke-virtual {v11, v12}, Lcom/sec/android/app/myfiles/utils/Security;->decrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1409
    const-string v10, "SELECT_PATH_MODE"

    const/4 v11, 0x1

    invoke-virtual {v1, v10, v11}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1411
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mContext:Landroid/content/Context;

    if-eqz v10, :cond_6

    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mContext:Landroid/content/Context;

    instance-of v10, v10, Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v10, :cond_6

    .line 1412
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mContext:Landroid/content/Context;

    check-cast v10, Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v10, v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView(Landroid/os/Bundle;)V

    .line 1415
    :cond_6
    if-eqz v9, :cond_7

    .line 1417
    iget v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpPosition:I

    invoke-virtual {p0, v10}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->getItem(I)Landroid/app/Fragment;

    move-result-object v5

    .line 1419
    .local v5, "frag":Landroid/app/Fragment;
    if-eqz v5, :cond_7

    instance-of v10, v5, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    if-eqz v10, :cond_7

    .line 1421
    check-cast v5, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    .end local v5    # "frag":Landroid/app/Fragment;
    const/4 v10, 0x0

    invoke-virtual {v5, v10, v1}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->gotoFolder(Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 1427
    .end local v1    # "arg":Landroid/os/Bundle;
    .end local v9    # "needToResetPath":Z
    :cond_7
    return-void

    .line 1241
    :cond_8
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v10, v10, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    if-eqz v10, :cond_a

    sget-boolean v10, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-nez v10, :cond_9

    sget-boolean v10, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-nez v10, :cond_9

    sget-boolean v10, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-eqz v10, :cond_a

    :cond_9
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v10, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mCurrentFolder:Ljava/lang/String;

    const-string v11, "/storage"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 1243
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/SelectorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v10

    const v11, 0x7f0f0009

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/View;->setEnabled(Z)V

    .line 1244
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v10, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCreateFolderContainer:Landroid/widget/LinearLayout;

    if-eqz v10, :cond_4

    .line 1245
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v10, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCreateFolderContainer:Landroid/widget/LinearLayout;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 1246
    :cond_a
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v10, v10, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    if-eqz v10, :cond_c

    sget-boolean v10, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-nez v10, :cond_b

    sget-boolean v10, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-nez v10, :cond_b

    sget-boolean v10, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-eqz v10, :cond_c

    :cond_b
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v10, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mCurrentFolder:Ljava/lang/String;

    if-eqz v10, :cond_c

    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v10, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mCurrentFolder:Ljava/lang/String;

    const-string v11, "/storage"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 1249
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/SelectorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v10

    const v11, 0x7f0f0009

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/View;->setEnabled(Z)V

    .line 1250
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v10, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCreateFolderContainer:Landroid/widget/LinearLayout;

    if-eqz v10, :cond_4

    .line 1251
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v10, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCreateFolderContainer:Landroid/widget/LinearLayout;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 1252
    :cond_c
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v10, v10, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    if-eqz v10, :cond_d

    sget-boolean v10, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->success:Z

    if-nez v10, :cond_d

    .line 1253
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/SelectorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v10

    const v11, 0x7f0f0009

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/View;->setEnabled(Z)V

    .line 1254
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v10, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCreateFolderContainer:Landroid/widget/LinearLayout;

    if-eqz v10, :cond_4

    .line 1255
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v10, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCreateFolderContainer:Landroid/widget/LinearLayout;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 1257
    :cond_d
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/SelectorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v10

    const v11, 0x7f0f0009

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_0

    .line 1261
    :cond_e
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v10, :cond_4

    .line 1262
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getmAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v0

    .line 1263
    .local v0, "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
    if-eqz v0, :cond_4

    .line 1264
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v10

    if-nez v10, :cond_10

    .line 1265
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/SelectorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v10

    const v11, 0x7f0f0009

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/View;->setEnabled(Z)V

    .line 1270
    :goto_1
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v11, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/SelectorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v11

    const v12, 0x7f0f0001

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mActionbarSelectAllContainer:Landroid/view/View;

    .line 1271
    iget-object v11, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/SelectorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v10

    const v12, 0x7f0f0003

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, v11, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllButton:Landroid/widget/TextView;

    .line 1272
    iget-object v11, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/SelectorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v10

    const v12, 0x7f0f0002

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    iput-object v10, v11, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllCheckBox:Landroid/widget/ImageView;

    .line 1273
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllButton:Landroid/widget/TextView;

    if-eqz v10, :cond_7

    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllCheckBox:Landroid/widget/ImageView;

    if-eqz v10, :cond_7

    .line 1276
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v10, v10, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;

    if-nez v10, :cond_f

    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v10, v10, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;

    if-eqz v10, :cond_11

    .line 1277
    :cond_f
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/SelectorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v10

    const v11, 0x7f0f0009

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/View;->setEnabled(Z)V

    .line 1278
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectionType:I
    invoke-static {v10}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$400(Lcom/sec/android/app/myfiles/SelectorActivity;)I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_4

    .line 1279
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/SelectorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v10

    const v11, 0x7f0f0009

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/View;->setEnabled(Z)V

    .line 1280
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllButton:Landroid/widget/TextView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1281
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/SelectorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0b0038

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v10, v11, v12}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 1282
    .local v8, "msg":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllButton:Landroid/widget/TextView;

    invoke-virtual {v10, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1283
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v10, v8}, Lcom/sec/android/app/myfiles/SelectorActivity;->setSpinnerWidth(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1267
    .end local v8    # "msg":Ljava/lang/String;
    :cond_10
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/SelectorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v10

    const v11, 0x7f0f0009

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_1

    .line 1286
    :cond_11
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectionType:I
    invoke-static {v10}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$400(Lcom/sec/android/app/myfiles/SelectorActivity;)I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_4

    .line 1288
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v10

    if-nez v10, :cond_13

    .line 1289
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/SelectorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v10

    const v11, 0x7f0f0009

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/4 v11, 0x4

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    .line 1294
    :goto_2
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/SelectorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0b0038

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v10, v11, v12}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 1295
    .restart local v8    # "msg":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllButton:Landroid/widget/TextView;

    invoke-virtual {v10, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1296
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v10, v8}, Lcom/sec/android/app/myfiles/SelectorActivity;->setSpinnerWidth(Ljava/lang/String;)V

    .line 1297
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    .line 1298
    .local v2, "cursor":Landroid/database/Cursor;
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mActionbarSelectAllContainer:Landroid/view/View;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/View;->setEnabled(Z)V

    .line 1299
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllButton:Landroid/widget/TextView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1300
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllCheckBox:Landroid/widget/ImageView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1301
    const/4 v4, 0x0

    .line 1302
    .local v4, "fileCount":I
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mCategoryType:I
    invoke-static {v10}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$1200(Lcom/sec/android/app/myfiles/SelectorActivity;)I

    move-result v10

    const/4 v11, -0x1

    if-ne v10, v11, :cond_19

    .line 1304
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v10

    if-lez v10, :cond_1a

    .line 1306
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1307
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_3
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v10

    if-ge v6, v10, :cond_1a

    .line 1309
    new-instance v3, Ljava/io/File;

    const-string v10, "_data"

    invoke-interface {v2, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v3, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1310
    .local v3, "file":Ljava/io/File;
    const/4 v7, 0x0

    .line 1312
    .local v7, "isFile":Z
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v10, v10, Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    if-eqz v10, :cond_16

    .line 1314
    const-string v10, "format"

    invoke-interface {v2, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    const/4 v11, -0x1

    if-ne v10, v11, :cond_14

    .line 1316
    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v7

    .line 1335
    :goto_4
    if-eqz v7, :cond_12

    .line 1337
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mActionbarSelectAllContainer:Landroid/view/View;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/view/View;->setEnabled(Z)V

    .line 1338
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllButton:Landroid/widget/TextView;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1339
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllCheckBox:Landroid/widget/ImageView;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1340
    add-int/lit8 v4, v4, 0x1

    .line 1343
    :cond_12
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    .line 1307
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 1291
    .end local v2    # "cursor":Landroid/database/Cursor;
    .end local v3    # "file":Ljava/io/File;
    .end local v4    # "fileCount":I
    .end local v6    # "i":I
    .end local v7    # "isFile":Z
    .end local v8    # "msg":Ljava/lang/String;
    :cond_13
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/SelectorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v10

    const v11, 0x7f0f0009

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 1320
    .restart local v2    # "cursor":Landroid/database/Cursor;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v4    # "fileCount":I
    .restart local v6    # "i":I
    .restart local v7    # "isFile":Z
    .restart local v8    # "msg":Ljava/lang/String;
    :cond_14
    const-string v10, "format"

    invoke-interface {v2, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v2, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_15

    const/4 v7, 0x1

    :goto_5
    goto :goto_4

    :cond_15
    const/4 v7, 0x0

    goto :goto_5

    .line 1325
    :cond_16
    const-string v10, "format"

    invoke-interface {v2, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    const/4 v11, -0x1

    if-ne v10, v11, :cond_17

    .line 1327
    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v7

    goto :goto_4

    .line 1331
    :cond_17
    const-string v10, "format"

    invoke-interface {v2, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v2, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    if-nez v10, :cond_18

    const/4 v7, 0x1

    :goto_6
    goto :goto_4

    :cond_18
    const/4 v7, 0x0

    goto :goto_6

    .line 1347
    .end local v3    # "file":Ljava/io/File;
    .end local v6    # "i":I
    .end local v7    # "isFile":Z
    :cond_19
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v4

    .line 1348
    if-lez v4, :cond_1a

    .line 1349
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mActionbarSelectAllContainer:Landroid/view/View;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/view/View;->setEnabled(Z)V

    .line 1350
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllButton:Landroid/widget/TextView;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1351
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllCheckBox:Landroid/widget/ImageView;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1355
    :cond_1a
    iget-object v11, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    const v12, 0x7f0f0047

    invoke-virtual {v10, v12}, Lcom/sec/android/app/myfiles/SelectorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout;

    iput-object v10, v11, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllContainer:Landroid/widget/RelativeLayout;

    .line 1356
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllContainer:Landroid/widget/RelativeLayout;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1357
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mActionbarSelectAllContainer:Landroid/view/View;

    new-instance v11, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$2;

    invoke-direct {v11, p0}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$2;-><init>(Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;)V

    invoke-virtual {v10, v11}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1374
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v10

    if-nez v10, :cond_1b

    .line 1375
    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->setSelectOptionsVisibility(ZZ)V

    .line 1376
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllCheckBox:Landroid/widget/ImageView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setSelected(Z)V

    goto/16 :goto_0

    .line 1377
    :cond_1b
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v10

    if-ne v4, v10, :cond_1c

    .line 1378
    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->setSelectOptionsVisibility(ZZ)V

    .line 1379
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllCheckBox:Landroid/widget/ImageView;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setSelected(Z)V

    goto/16 :goto_0

    .line 1381
    :cond_1c
    const/4 v10, 0x1

    const/4 v11, 0x1

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->setSelectOptionsVisibility(ZZ)V

    .line 1382
    iget-object v10, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllCheckBox:Landroid/widget/ImageView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setSelected(Z)V

    goto/16 :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 913
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mTabs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/app/Fragment;
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 920
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mTabs:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;->tag:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;->access$100(Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;)Ljava/lang/String;

    move-result-object v2

    .line 922
    .local v2, "tag":Ljava/lang/String;
    const-string v3, "ftp-shortcut"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "ftp-browser"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 923
    :cond_0
    iput p1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpPosition:I

    .line 927
    :cond_1
    const-string v3, "ftp-shortcut"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpTabMode:Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;

    sget-object v4, Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;->BROWSING:Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;

    if-eq v3, v4, :cond_3

    :cond_2
    const-string v3, "ftp-browser"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpTabMode:Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;

    sget-object v4, Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;->SHORTCUT:Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;

    if-ne v3, v4, :cond_8

    .line 930
    :cond_3
    sget-object v3, Lcom/sec/android/app/myfiles/SelectorActivity$13;->$SwitchMap$com$sec$android$app$myfiles$SelectorActivity$FtpTabMode:[I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpTabMode:Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 941
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mTabs:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpShortcutTabInfo:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;

    invoke-virtual {v3, p1, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 946
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFragmentInstanceMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 948
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFragmentInstanceMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 978
    :cond_4
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFragmentInstanceMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Fragment;

    .line 980
    .local v0, "fragment":Landroid/app/Fragment;
    if-nez v0, :cond_7

    .line 982
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mTabs:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;

    .line 984
    .local v1, "info":Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mContext:Landroid/content/Context;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;->clss:Ljava/lang/Class;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;->access$200(Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;)Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;->args:Landroid/os/Bundle;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;->access$300(Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;)Landroid/os/Bundle;

    move-result-object v5

    invoke-static {v3, v4, v5}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    move-result-object v0

    .line 986
    const-string v3, "ftp-shortcut"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "ftp-browser"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 988
    :cond_5
    iput-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mCurrentFtpFragment:Landroid/app/Fragment;

    .line 994
    :cond_6
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFragmentInstanceMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 997
    .end local v1    # "info":Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;
    :cond_7
    return-object v0

    .line 934
    .end local v0    # "fragment":Landroid/app/Fragment;
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mTabs:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpBrowserTabInfo:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;

    invoke-virtual {v3, p1, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 950
    :cond_8
    const-string v3, "user-shortcut"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mShortcutTabMode:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    sget-object v4, Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;->BROWSING:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    if-eq v3, v4, :cond_a

    :cond_9
    const-string v3, "user-browser"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mShortcutTabMode:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    sget-object v4, Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;->SHORTCUT:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    if-ne v3, v4, :cond_4

    .line 954
    :cond_a
    iput p1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mShortcutPosition:I

    .line 956
    sget-object v3, Lcom/sec/android/app/myfiles/SelectorActivity$13;->$SwitchMap$com$sec$android$app$myfiles$SelectorActivity$ShortcutTabMode:[I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mShortcutTabMode:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    .line 967
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mTabs:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mShortcutTabInfo:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;

    invoke-virtual {v3, p1, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 972
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFragmentInstanceMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 974
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFragmentInstanceMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 960
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mTabs:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mShortcutBrowserTabInfo:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;

    invoke-virtual {v3, p1, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 989
    .restart local v0    # "fragment":Landroid/app/Fragment;
    .restart local v1    # "info":Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;
    :cond_b
    const-string v3, "user-shortcut"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string v3, "user-browser"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 991
    :cond_c
    iput-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mCurrentShortcutFragment:Landroid/app/Fragment;

    goto :goto_2

    .line 930
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 956
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 1004
    const/4 v0, 0x0

    const-string v1, "SelectorActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SelectorActivity.getitemPosition "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 1007
    instance-of v0, p1, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpTabMode:Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;

    sget-object v1, Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;->BROWSING:Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;

    if-eq v0, v1, :cond_3

    :cond_0
    instance-of v0, p1, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpTabMode:Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;

    sget-object v1, Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;->SHORTCUT:Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;

    if-eq v0, v1, :cond_3

    :cond_1
    instance-of v0, p1, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mShortcutTabMode:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    sget-object v1, Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;->BROWSING:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    if-eq v0, v1, :cond_3

    :cond_2
    instance-of v0, p1, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mShortcutTabMode:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    sget-object v1, Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;->SHORTCUT:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    if-ne v0, v1, :cond_4

    .line 1012
    :cond_3
    const/4 v0, -0x2

    .line 1016
    :goto_0
    return v0

    :cond_4
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public goToBaiduShortcut(ILjava/lang/String;)V
    .locals 4
    .param p1, "pos"    # I
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 1472
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFragmentInstanceMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 1473
    .local v1, "frag":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    if-eqz v1, :cond_0

    instance-of v2, v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    if-eqz v2, :cond_0

    .line 1474
    const-string v2, "Baidu/"

    const-string v3, ""

    invoke-virtual {p2, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 1475
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->getItem(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .line 1476
    .local v0, "c":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getmAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    .line 1477
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->onRefresh()V

    .line 1478
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->onPageSelected(I)V

    .line 1480
    .end local v0    # "c":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;
    :cond_0
    return-void
.end method

.method public goToCloudShortcut(ILjava/lang/String;)V
    .locals 4
    .param p1, "pos"    # I
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 1459
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFragmentInstanceMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 1460
    .local v1, "frag":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    if-eqz v1, :cond_0

    instance-of v2, v1, Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    if-eqz v2, :cond_0

    .line 1461
    const-string v2, "Dropbox/"

    const-string v3, ""

    invoke-virtual {p2, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 1462
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->getItem(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    .line 1463
    .local v0, "c":Lcom/sec/android/app/myfiles/fragment/CloudFragment;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getmAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    .line 1464
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/Dropbox"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getmAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->setPathIndicator(Ljava/lang/String;)V

    .line 1465
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->onRefresh()V

    .line 1466
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->onPageSelected(I)V

    .line 1468
    .end local v0    # "c":Lcom/sec/android/app/myfiles/fragment/CloudFragment;
    :cond_0
    return-void
.end method

.method public goToFtpShortcut(ILjava/lang/String;)V
    .locals 1
    .param p1, "pos"    # I
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 1484
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mNeedToShowFtpBrowser:Z

    .line 1486
    iput-object p2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpSelectedPath:Ljava/lang/String;

    .line 1488
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->onPageSelected(I)V

    .line 1490
    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 1455
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 1161
    return-void
.end method

.method public onPageSelected(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 1437
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v1

    .line 1439
    .local v1, "widget":Landroid/widget/TabWidget;
    invoke-virtual {v1}, Landroid/widget/TabWidget;->getDescendantFocusability()I

    move-result v0

    .line 1441
    .local v0, "oldFocusability":I
    const/high16 v2, 0x60000

    invoke-virtual {v1, v2}, Landroid/widget/TabWidget;->setDescendantFocusability(I)V

    .line 1443
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v2, p1}, Landroid/widget/TabHost;->setCurrentTab(I)V

    .line 1445
    invoke-virtual {v1, v0}, Landroid/widget/TabWidget;->setDescendantFocusability(I)V

    .line 1447
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->getItem(I)Landroid/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    iput-object v2, v3, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 1449
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectionType:I
    invoke-static {v3}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$400(Lcom/sec/android/app/myfiles/SelectorActivity;)I

    move-result v3

    const/4 v4, -0x1

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->startSelectMode(III)V

    .line 1450
    return-void
.end method

.method public onTabChanged(Ljava/lang/String;)V
    .locals 7
    .param p1, "tabId"    # Ljava/lang/String;

    .prologue
    const/4 v6, -0x1

    .line 1108
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v1

    .line 1110
    .local v1, "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v0

    .line 1112
    .local v0, "position":I
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mForWarningPosition:I
    invoke-static {v2}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$600(Lcom/sec/android/app/myfiles/SelectorActivity;)I

    move-result v2

    if-ne v2, v6, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mLatestPosition:I
    invoke-static {v2}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$700(Lcom/sec/android/app/myfiles/SelectorActivity;)I

    move-result v2

    if-eq v2, v0, :cond_1

    .line 1114
    const-string v2, "dropbox"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckDropBox()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1115
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/SelectorActivity;->showWarningDialog(I)V

    .line 1116
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mTabHost:Landroid/widget/TabHost;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mLatestPosition:I
    invoke-static {v3}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$700(Lcom/sec/android/app/myfiles/SelectorActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TabHost;->setCurrentTab(I)V

    .line 1117
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # setter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mForWarningPosition:I
    invoke-static {v2, v0}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$602(Lcom/sec/android/app/myfiles/SelectorActivity;I)I

    .line 1156
    :goto_0
    return-void

    .line 1119
    :cond_0
    const-string v2, "ftp-shortcut"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckFTP()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1120
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/SelectorActivity;->showWarningDialog(I)V

    .line 1121
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mTabHost:Landroid/widget/TabHost;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mLatestPosition:I
    invoke-static {v3}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$700(Lcom/sec/android/app/myfiles/SelectorActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TabHost;->setCurrentTab(I)V

    .line 1122
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # setter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mForWarningPosition:I
    invoke-static {v2, v0}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$602(Lcom/sec/android/app/myfiles/SelectorActivity;I)I

    goto :goto_0

    .line 1127
    :cond_1
    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->mIsActiveWarningUsingWLAN:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mForWarningPosition:I
    invoke-static {v2}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$600(Lcom/sec/android/app/myfiles/SelectorActivity;)I

    move-result v2

    if-ne v2, v6, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mLatestPosition:I
    invoke-static {v2}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$700(Lcom/sec/android/app/myfiles/SelectorActivity;)I

    move-result v2

    if-eq v2, v0, :cond_3

    .line 1129
    const-string v2, "baidu"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckWLANOn()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1130
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/SelectorActivity;->showWarningDialog(I)V

    .line 1131
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mTabHost:Landroid/widget/TabHost;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mLatestPosition:I
    invoke-static {v3}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$700(Lcom/sec/android/app/myfiles/SelectorActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TabHost;->setCurrentTab(I)V

    .line 1132
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # setter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mForWarningPosition:I
    invoke-static {v2, v0}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$602(Lcom/sec/android/app/myfiles/SelectorActivity;I)I

    goto :goto_0

    .line 1134
    :cond_2
    const-string v2, "ftp-shortcut"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckWLANOn()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1135
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/SelectorActivity;->showWarningDialog(I)V

    .line 1136
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mTabHost:Landroid/widget/TabHost;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mLatestPosition:I
    invoke-static {v3}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$700(Lcom/sec/android/app/myfiles/SelectorActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TabHost;->setCurrentTab(I)V

    .line 1137
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # setter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mForWarningPosition:I
    invoke-static {v2, v0}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$602(Lcom/sec/android/app/myfiles/SelectorActivity;I)I

    goto/16 :goto_0

    .line 1143
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # setter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mLatestPosition:I
    invoke-static {v2, v0}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$702(Lcom/sec/android/app/myfiles/SelectorActivity;I)I

    .line 1145
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1147
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFragmentInstanceMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    iput-object v2, v3, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 1149
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v2, :cond_4

    .line 1150
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->selectmodeActionbar()V

    .line 1155
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # setter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mForWarningPosition:I
    invoke-static {v2, v6}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$602(Lcom/sec/android/app/myfiles/SelectorActivity;I)I

    goto/16 :goto_0

    .line 1152
    :cond_4
    const/4 v2, 0x0

    const-string v3, "SelectorActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SelectorActivity.onTabChanged: position: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setSelectAllClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "clickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 1165
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iput-object p1, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllClickListener:Landroid/view/View$OnClickListener;

    .line 1166
    return-void
.end method

.method public setSelectOptionsVisibility(ZZ)V
    .locals 6
    .param p1, "selectAll"    # Z
    .param p2, "unselectAll"    # Z

    .prologue
    .line 1175
    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectionLock:Ljava/lang/Object;
    invoke-static {}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$800()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 1176
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 1177
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0035

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0037

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    # setter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectOptions:[Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$902(Lcom/sec/android/app/myfiles/SelectorActivity;[Ljava/lang/String;)[Ljava/lang/String;

    .line 1178
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    # setter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectOptionsIds:[I
    invoke-static {v0, v2}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$1002(Lcom/sec/android/app/myfiles/SelectorActivity;[I)[I

    .line 1186
    :cond_0
    :goto_0
    monitor-exit v1

    .line 1188
    return-void

    .line 1179
    :cond_1
    if-eqz p1, :cond_2

    .line 1180
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0035

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    # setter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectOptions:[Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$902(Lcom/sec/android/app/myfiles/SelectorActivity;[Ljava/lang/String;)[Ljava/lang/String;

    .line 1181
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, v2, v3

    # setter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectOptionsIds:[I
    invoke-static {v0, v2}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$1002(Lcom/sec/android/app/myfiles/SelectorActivity;[I)[I

    goto :goto_0

    .line 1186
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1182
    :cond_2
    if-eqz p2, :cond_0

    .line 1183
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0037

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    # setter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectOptions:[Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$902(Lcom/sec/android/app/myfiles/SelectorActivity;[Ljava/lang/String;)[Ljava/lang/String;

    .line 1184
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    aput v4, v2, v3

    # setter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectOptionsIds:[I
    invoke-static {v0, v2}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$1002(Lcom/sec/android/app/myfiles/SelectorActivity;[I)[I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1178
    nop

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data
.end method

.method public setUnselectAllClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "clickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 1170
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iput-object p1, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mUnselectAllClickListener:Landroid/view/View$OnClickListener;

    .line 1171
    return-void
.end method

.method public switchFtpMode(ILandroid/os/Bundle;)V
    .locals 4
    .param p1, "fragmentId"    # I
    .param p2, "arg"    # Landroid/os/Bundle;

    .prologue
    .line 1023
    const/4 v0, 0x0

    const-string v1, "SelectorActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SelectorActivity.switchFtpMode: fragmentId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 1026
    const/16 v0, 0xa

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpTabMode:Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;

    sget-object v1, Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;->SHORTCUT:Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;

    if-ne v0, v1, :cond_3

    .line 1028
    sget-object v0, Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;->BROWSING:Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpTabMode:Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;

    .line 1030
    const-string v0, "selection_type"

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectionType:I
    invoke-static {v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$400(Lcom/sec/android/app/myfiles/SelectorActivity;)I

    move-result v1

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1032
    const-string v0, "show_folder_file_type"

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mShowFolderFileType:I
    invoke-static {v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$500(Lcom/sec/android/app/myfiles/SelectorActivity;)I

    move-result v1

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1034
    new-instance v0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;

    const-string v1, "ftp-browser"

    const-class v2, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/SelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-static {p2, v3}, Lcom/sec/android/app/myfiles/SelectorActivity;->appendIntentArgs(Landroid/os/Bundle;Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v3

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;-><init>(Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpBrowserTabInfo:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;

    .line 1037
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mCurrentFtpFragment:Landroid/app/Fragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mCurrentFtpFragment:Landroid/app/Fragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1039
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/SelectorActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mCurrentFtpFragment:Landroid/app/Fragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 1043
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->notifyDataSetChanged()V

    .line 1058
    :cond_1
    :goto_0
    iget v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpPosition:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 1059
    iget v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpPosition:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->onPageSelected(I)V

    .line 1062
    :cond_2
    return-void

    .line 1045
    :cond_3
    const/16 v0, 0x1c

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpTabMode:Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;

    sget-object v1, Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;->BROWSING:Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;

    if-ne v0, v1, :cond_1

    .line 1047
    sget-object v0, Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;->SHORTCUT:Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mFtpTabMode:Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;

    .line 1049
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mCurrentFtpFragment:Landroid/app/Fragment;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mCurrentFtpFragment:Landroid/app/Fragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1051
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/SelectorActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mCurrentFtpFragment:Landroid/app/Fragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1055
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public switchUserShortcutMode(ILandroid/os/Bundle;)V
    .locals 5
    .param p1, "fragmentId"    # I
    .param p2, "arg"    # Landroid/os/Bundle;

    .prologue
    const/16 v4, 0x24

    .line 1066
    const/4 v0, 0x0

    const-string v1, "SelectorActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SelectorActivity.switchUserShortcutMode: fragmentId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 1069
    const/16 v0, 0x1a

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mShortcutTabMode:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    sget-object v1, Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;->SHORTCUT:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    if-ne v0, v1, :cond_2

    .line 1071
    sget-object v0, Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;->BROWSING:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mShortcutTabMode:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    .line 1073
    const-string v0, "selection_type"

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectionType:I
    invoke-static {v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$400(Lcom/sec/android/app/myfiles/SelectorActivity;)I

    move-result v1

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1075
    const-string v0, "show_folder_file_type"

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mShowFolderFileType:I
    invoke-static {v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$500(Lcom/sec/android/app/myfiles/SelectorActivity;)I

    move-result v1

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1077
    const-string v0, "shortcut_mode_enable_intent_key"

    const/4 v1, 0x1

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1079
    new-instance v0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;

    const-string v1, "user-browser"

    const-class v2, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    invoke-direct {v0, p0, v1, v2, p2}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;-><init>(Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mShortcutBrowserTabInfo:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;

    .line 1082
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mCurrentShortcutFragment:Landroid/app/Fragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mCurrentShortcutFragment:Landroid/app/Fragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1083
    const-string v0, "id"

    invoke-virtual {p2, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1084
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/SelectorActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mCurrentShortcutFragment:Landroid/app/Fragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 1086
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->notifyDataSetChanged()V

    .line 1088
    iget v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mShortcutPosition:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->onPageSelected(I)V

    .line 1103
    :cond_1
    :goto_0
    return-void

    .line 1090
    :cond_2
    if-ne p1, v4, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mShortcutTabMode:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    sget-object v1, Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;->BROWSING:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    if-ne v0, v1, :cond_1

    .line 1092
    sget-object v0, Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;->SHORTCUT:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mShortcutTabMode:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    .line 1094
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mCurrentShortcutFragment:Landroid/app/Fragment;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mCurrentShortcutFragment:Landroid/app/Fragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1096
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/SelectorActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mCurrentShortcutFragment:Landroid/app/Fragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1099
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->notifyDataSetChanged()V

    .line 1101
    iget v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->mShortcutPosition:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->onPageSelected(I)V

    goto :goto_0
.end method
