.class public Lcom/sec/android/app/myfiles/fragment/SearchFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;
.source "SearchFragment.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/fragment/ISearchable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;
    }
.end annotation


# static fields
.field public static final INDEX_BROADCAST_ACTION:Ljava/lang/String; = "action.index.SEARCH"

.field private static final MODULE:Ljava/lang/String; = "SearchFragment"

.field public static final PROJ_CONTENT:Ljava/lang/String; = "content_search"


# instance fields
.field private dialog:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

.field private isSearchRenameorDelete:Z

.field private isSearchRenameorDeleteRefresh:Z

.field private linearLayout:Landroid/widget/LinearLayout;

.field public mAdvancedSearchResult:Landroid/widget/TextView;

.field protected mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mClearFocusAfterSearch:Z

.field private mContextSelectedItemIsFile:Z

.field private mCurrentParam:Lcom/sec/android/app/myfiles/utils/SearchParameter;

.field private mExtractFile:Ljava/lang/String;

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mPathStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRenameReceiver:Landroid/content/BroadcastReceiver;

.field private mRenameReceiverFilter:Landroid/content/IntentFilter;

.field private mSavedSearchDate:Ljava/lang/String;

.field private mSavedSearchExtension:Ljava/lang/String;

.field private mSavedSearchFileType:Ljava/lang/String;

.field private mSavedSearchKeyWord:Ljava/lang/String;

.field private mScanFileReceiver:Landroid/content/BroadcastReceiver;

.field private mScanFileReceiverFilter:Landroid/content/IntentFilter;

.field mSearchNavigation:Lcom/sec/android/app/myfiles/navigation/SearchNavigation;

.field private mSearchTask:Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;

.field private mSearchView:Landroid/widget/SearchView;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 96
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;-><init>()V

    .line 108
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mPathStack:Ljava/util/Stack;

    .line 110
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->isSearchRenameorDeleteRefresh:Z

    .line 112
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->isSearchRenameorDelete:Z

    .line 145
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mContextSelectedItemIsFile:Z

    .line 147
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 149
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mIntentFilter:Landroid/content/IntentFilter;

    .line 151
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->dialog:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    .line 153
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mClearFocusAfterSearch:Z

    .line 1044
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->isSearchRenameorDelete:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/fragment/SearchFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 96
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->isSearchRenameorDelete:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->isSearchRenameorDeleteRefresh:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/myfiles/fragment/SearchFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 96
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->isSearchRenameorDeleteRefresh:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSavedSearchKeyWord:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/fragment/SearchFragment;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->createFolderForExtract(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->dialog:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/fragment/SearchFragment;Landroid/database/Cursor;Ljava/util/Set;)Landroid/database/Cursor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchFragment;
    .param p1, "x1"    # Landroid/database/Cursor;
    .param p2, "x2"    # Ljava/util/Set;

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->createThumbnailCursor(Landroid/database/Cursor;Ljava/util/Set;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mClearFocusAfterSearch:Z

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/myfiles/fragment/SearchFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 96
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mClearFocusAfterSearch:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/myfiles/fragment/SearchFragment;Landroid/database/Cursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchFragment;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->goSearch(Landroid/database/Cursor;)V

    return-void
.end method

.method private afterAdvanceSearch()V
    .locals 20

    .prologue
    .line 1293
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 1294
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchView:Landroid/widget/SearchView;

    move-object/from16 v16, v0

    const/16 v17, 0x8

    invoke-virtual/range {v16 .. v17}, Landroid/widget/SearchView;->setVisibility(I)V

    .line 1300
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v16, v0

    const v17, 0x7f0f004e

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/myfiles/AbsMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/RelativeLayout;

    move-object/from16 v10, v16

    check-cast v10, Landroid/widget/RelativeLayout;

    .line 1301
    .local v10, "searchFor":Landroid/widget/RelativeLayout;
    if-eqz v10, :cond_0

    .line 1302
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1304
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v7

    .line 1308
    .local v7, "nf":Ljava/text/NumberFormat;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v16

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v18, v0

    const v19, 0x7f0b0145

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " ("

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mResultCount()I

    move-result v18

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v7, v0, v1}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ")"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 1309
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v16

    const/16 v17, 0x1

    invoke-virtual/range {v16 .. v17}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 1310
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v16, v0

    check-cast v16, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/myfiles/MainActivity;->invalidateOptionsMenu()V

    .line 1311
    const/16 v16, 0x4

    move/from16 v0, v16

    new-array v5, v0, [Z

    fill-array-data v5, :array_0

    .line 1312
    .local v5, "changedItems":[Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v16, v0

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getmChangedItemsForAdvanceSearch()[Z

    move-result-object v5

    .line 1313
    const/16 v16, 0x4

    move/from16 v0, v16

    new-array v2, v0, [Z

    fill-array-data v2, :array_1

    .line 1314
    .local v2, "a":[Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v16, v0

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setmChangedItemsForAdvanceSearch([Z)V

    .line 1315
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1316
    .local v4, "builderLocation":Ljava/lang/StringBuilder;
    const-string v9, ""

    .line 1317
    .local v9, "preLocation":Ljava/lang/String;
    const/4 v6, 0x1

    .line 1318
    .local v6, "index":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v16, v0

    const v17, 0x7f0f0050

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/myfiles/AbsMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/Button;

    move-object/from16 v15, v16

    check-cast v15, Landroid/widget/Button;

    .line 1319
    .local v15, "searchForName":Landroid/widget/Button;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v16, v0

    const v17, 0x7f0f0051

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/myfiles/AbsMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/Button;

    move-object/from16 v14, v16

    check-cast v14, Landroid/widget/Button;

    .line 1320
    .local v14, "searchForLocation":Landroid/widget/Button;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v16, v0

    const v17, 0x7f0f0052

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/myfiles/AbsMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/Button;

    move-object/from16 v13, v16

    check-cast v13, Landroid/widget/Button;

    .line 1321
    .local v13, "searchForFileType":Landroid/widget/Button;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v16, v0

    const v17, 0x7f0f0053

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/myfiles/AbsMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/Button;

    move-object/from16 v11, v16

    check-cast v11, Landroid/widget/Button;

    .line 1322
    .local v11, "searchForDate":Landroid/widget/Button;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v16, v0

    const v17, 0x7f0f0054

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/myfiles/AbsMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/Button;

    move-object/from16 v12, v16

    check-cast v12, Landroid/widget/Button;

    .line 1324
    .local v12, "searchForExtension":Landroid/widget/Button;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSavedSearchKeyWord:Ljava/lang/String;

    move-object/from16 v16, v0

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_1

    .line 1325
    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Landroid/widget/Button;->setVisibility(I)V

    .line 1326
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSavedSearchKeyWord:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1327
    new-instance v16, Lcom/sec/android/app/myfiles/fragment/SearchFragment$9;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment$9;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)V

    invoke-virtual/range {v15 .. v16}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1336
    :cond_1
    const/16 v16, 0x0

    aget-boolean v16, v5, v16

    if-eqz v16, :cond_d

    .line 1337
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 1338
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationDevice()Z

    move-result v16

    if-eqz v16, :cond_2

    .line 1339
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const v17, 0x7f0b003e

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1340
    const-string v9, ", "

    .line 1342
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationSDcard()Z

    move-result v16

    if-eqz v16, :cond_3

    .line 1343
    sget-boolean v16, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v16, :cond_3

    .line 1344
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const v17, 0x7f0b003f

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1345
    const-string v9, ", "

    .line 1348
    :cond_3
    sget-boolean v16, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-eqz v16, :cond_9

    .line 1349
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBa()Z

    move-result v16

    if-eqz v16, :cond_4

    .line 1350
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const v17, 0x7f0b0042

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1351
    const-string v9, ", "

    .line 1353
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBb()Z

    move-result v16

    if-eqz v16, :cond_5

    .line 1354
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const v17, 0x7f0b0043

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1355
    const-string v9, ", "

    .line 1357
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBc()Z

    move-result v16

    if-eqz v16, :cond_6

    .line 1358
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const v17, 0x7f0b0044

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1359
    const-string v9, ", "

    .line 1361
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBd()Z

    move-result v16

    if-eqz v16, :cond_7

    .line 1362
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const v17, 0x7f0b0045

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1363
    const-string v9, ", "

    .line 1365
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBe()Z

    move-result v16

    if-eqz v16, :cond_8

    .line 1366
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const v17, 0x7f0b0046

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1367
    const-string v9, ", "

    .line 1369
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBf()Z

    move-result v16

    if-eqz v16, :cond_9

    .line 1370
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const v17, 0x7f0b0047

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1371
    const-string v9, ", "

    .line 1374
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationDropbox()Z

    move-result v16

    if-eqz v16, :cond_a

    .line 1375
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const v17, 0x7f0b0008

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1376
    const-string v9, ", "

    .line 1378
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationBaidu()Z

    move-result v16

    if-eqz v16, :cond_b

    .line 1379
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const v17, 0x7f0b00fa

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1380
    const-string v9, ", "

    .line 1383
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationPrivate()Z

    move-result v16

    if-eqz v16, :cond_c

    .line 1384
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const v17, 0x7f0b0151

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1385
    const-string v9, ", "

    .line 1387
    :cond_c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1388
    new-instance v16, Lcom/sec/android/app/myfiles/fragment/SearchFragment$10;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment$10;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)V

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1397
    :cond_d
    const/16 v16, 0x1

    aget-boolean v16, v5, v16

    if-eqz v16, :cond_10

    .line 1398
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    move-object/from16 v16, v0

    if-nez v16, :cond_e

    .line 1399
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f0b0001

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f0b008d

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    .line 1402
    :cond_e
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 1404
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1405
    .local v3, "builder":Ljava/lang/StringBuilder;
    const-string v8, ""

    .line 1407
    .local v8, "pre":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeAll()Z

    move-result v16

    if-eqz v16, :cond_13

    .line 1409
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const v17, 0x7f0b008b

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1443
    :cond_f
    :goto_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v16

    if-eqz v16, :cond_18

    .line 1445
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1452
    :goto_1
    new-instance v16, Lcom/sec/android/app/myfiles/fragment/SearchFragment$11;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment$11;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)V

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1461
    .end local v3    # "builder":Ljava/lang/StringBuilder;
    .end local v8    # "pre":Ljava/lang/String;
    :cond_10
    const/16 v16, 0x2

    aget-boolean v16, v5, v16

    if-eqz v16, :cond_11

    .line 1462
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v11, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 1463
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSavedSearchDate:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1464
    new-instance v16, Lcom/sec/android/app/myfiles/fragment/SearchFragment$12;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment$12;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)V

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1473
    :cond_11
    const/16 v16, 0x3

    aget-boolean v16, v5, v16

    if-eqz v16, :cond_12

    .line 1474
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v12, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 1475
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSavedSearchExtension:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1476
    new-instance v16, Lcom/sec/android/app/myfiles/fragment/SearchFragment$13;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment$13;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)V

    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1485
    :cond_12
    return-void

    .line 1412
    .restart local v3    # "builder":Ljava/lang/StringBuilder;
    .restart local v8    # "pre":Ljava/lang/String;
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeImages()Z

    move-result v16

    if-eqz v16, :cond_14

    .line 1414
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const v17, 0x7f0b0003

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1415
    const-string v8, ", "

    .line 1417
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeVideos()Z

    move-result v16

    if-eqz v16, :cond_15

    .line 1419
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v16, v0

    const/16 v17, 0x1

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeVideos(Z)V

    .line 1420
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const v17, 0x7f0b0004

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1421
    const-string v8, ", "

    .line 1423
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeMusic()Z

    move-result v16

    if-eqz v16, :cond_16

    .line 1425
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v16, v0

    const/16 v17, 0x1

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeMusic(Z)V

    .line 1426
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const v17, 0x7f0b0147

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1427
    const-string v8, ", "

    .line 1429
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeDocument()Z

    move-result v16

    if-eqz v16, :cond_17

    .line 1431
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v16, v0

    const/16 v17, 0x1

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeDocument(Z)V

    .line 1432
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const v17, 0x7f0b0006

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1433
    const-string v8, ", "

    .line 1435
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeDownloadedApps()Z

    move-result v16

    if-eqz v16, :cond_f

    .line 1437
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v16, v0

    const/16 v17, 0x1

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeDownloadedApps(Z)V

    .line 1438
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const v17, 0x7f0b0007

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1439
    const-string v8, ", "

    goto/16 :goto_0

    .line 1448
    :cond_18
    const v16, 0x7f0b008d

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1449
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1311
    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 1313
    :array_1
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method private cancelSearchTaskifRunning()V
    .locals 2

    .prologue
    .line 1279
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchTask:Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchTask:Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1281
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchTask:Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->cancel(Z)Z

    .line 1283
    :cond_0
    return-void
.end method

.method private createFolderForExtract(Ljava/lang/String;)Z
    .locals 9
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x6

    const/4 v6, 0x0

    .line 1516
    const/4 v2, 0x0

    .line 1520
    .local v2, "fileName":Ljava/lang/String;
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mExtractFile:Ljava/lang/String;

    .line 1522
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1524
    .local v5, "zipFile":Ljava/io/File;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_0

    .line 1526
    invoke-static {p1}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v3

    .line 1528
    .local v3, "fileTypeInt":I
    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->isArchiveFileType(I)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1530
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v8, "Please select .zip file"

    invoke-static {v7, v8, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    .line 1572
    .end local v3    # "fileTypeInt":I
    :cond_0
    :goto_0
    return v6

    .line 1535
    .restart local v3    # "fileTypeInt":I
    :cond_1
    const/16 v7, 0x2f

    invoke-virtual {p1, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1537
    const-string v7, ".zip"

    invoke-virtual {v2, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1539
    const/16 v7, 0x2e

    invoke-virtual {v2, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 1541
    .local v1, "dotIndex":I
    if-ltz v1, :cond_2

    .line 1543
    invoke-virtual {v2, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 1552
    .end local v1    # "dotIndex":I
    :cond_2
    new-instance v4, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    invoke-direct {v4}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;-><init>()V

    .line 1554
    .local v4, "inputName":Landroid/app/DialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1556
    .local v0, "argument":Landroid/os/Bundle;
    const-string v6, "title"

    const v7, 0x7f0b0074

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1558
    const-string v6, "kind_of_operation"

    invoke-virtual {v0, v6, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1560
    const-string v6, "current_item"

    invoke-virtual {v5}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1562
    const-string v6, "file_name"

    invoke-virtual {v0, v6, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1564
    const-string v6, "src_fragment_id"

    iget v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mFragmentId:I

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1566
    invoke-virtual {v4, v0}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 1568
    invoke-virtual {v4, p0, v8}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 1570
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "extract"

    invoke-virtual {v4, v6, v7}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1572
    const/4 v6, 0x1

    goto :goto_0
.end method

.method private createScrollListener()Landroid/widget/AbsListView$OnScrollListener;
    .locals 1

    .prologue
    .line 344
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment$4;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)V

    return-object v0
.end method

.method private createThumbnailCursor(Landroid/database/Cursor;Ljava/util/Set;)Landroid/database/Cursor;
    .locals 13
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 1617
    .local p2, "dataSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v6, Landroid/database/MatrixCursor;

    sget-object v10, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mFilesContentSearchProjection:[Ljava/lang/String;

    invoke-direct {v6, v10}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 1619
    .local v6, "contentSearchCursor":Landroid/database/MatrixCursor;
    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v10

    if-nez v10, :cond_2

    .line 1621
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1623
    :cond_0
    const-string v3, ""

    .line 1624
    .local v3, "ID":Ljava/lang/String;
    const-string v0, ""

    .line 1625
    .local v0, "DATA":Ljava/lang/String;
    const-string v5, ""

    .line 1626
    .local v5, "TITLE":Ljava/lang/String;
    const-string v2, ""

    .line 1627
    .local v2, "FORMAT":Ljava/lang/String;
    const-string v4, ""

    .line 1628
    .local v4, "SIZE":Ljava/lang/String;
    const-string v1, ""

    .line 1629
    .local v1, "DATE_MODIFIED":Ljava/lang/String;
    const-string v9, ""

    .line 1630
    .local v9, "show_contentsearch_head":Ljava/lang/String;
    const-string v8, ""

    .line 1631
    .local v8, "highlight_content":Ljava/lang/String;
    const/4 v10, 0x0

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1632
    const/4 v10, 0x1

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1633
    const/4 v10, 0x2

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1634
    const/4 v10, 0x3

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1635
    const/4 v10, 0x4

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1636
    const/4 v10, 0x5

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1637
    const/4 v10, 0x6

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 1638
    const/4 v10, 0x7

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1639
    invoke-interface {p2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 1640
    const/16 v10, 0x8

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object v3, v10, v11

    const/4 v11, 0x1

    aput-object v0, v10, v11

    const/4 v11, 0x2

    aput-object v5, v10, v11

    const/4 v11, 0x3

    aput-object v2, v10, v11

    const/4 v11, 0x4

    aput-object v4, v10, v11

    const/4 v11, 0x5

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x6

    aput-object v9, v10, v11

    const/4 v11, 0x7

    aput-object v8, v10, v11

    invoke-virtual {v6, v10}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 1644
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v10

    if-nez v10, :cond_0

    .end local v0    # "DATA":Ljava/lang/String;
    .end local v1    # "DATE_MODIFIED":Ljava/lang/String;
    .end local v2    # "FORMAT":Ljava/lang/String;
    .end local v3    # "ID":Ljava/lang/String;
    .end local v4    # "SIZE":Ljava/lang/String;
    .end local v5    # "TITLE":Ljava/lang/String;
    .end local v8    # "highlight_content":Ljava/lang/String;
    .end local v9    # "show_contentsearch_head":Ljava/lang/String;
    :cond_2
    move-object v10, v6

    .line 1654
    :goto_0
    return-object v10

    .line 1645
    :catch_0
    move-exception v7

    .line 1646
    .local v7, "ex":Ljava/lang/IllegalStateException;
    invoke-virtual {v7}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 1647
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Landroid/database/MatrixCursor;->isClosed()Z

    move-result v10

    if-nez v10, :cond_3

    .line 1648
    invoke-virtual {v6}, Landroid/database/MatrixCursor;->close()V

    .line 1649
    const/4 v6, 0x0

    .line 1651
    :cond_3
    const/4 v10, 0x0

    goto :goto_0
.end method

.method private goSearch(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 987
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchNavigation:Lcom/sec/android/app/myfiles/navigation/SearchNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->getAppEntriesList()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 988
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchNavigation:Lcom/sec/android/app/myfiles/navigation/SearchNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->getAppEntriesList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->setDownlodedAppsList(Ljava/util/ArrayList;)V

    .line 990
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 991
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchFromAdvance:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 992
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->afterAdvanceSearch()V

    .line 994
    :cond_1
    return-void
.end method

.method private setRenameReceiver()V
    .locals 3

    .prologue
    .line 316
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mRenameReceiverFilter:Landroid/content/IntentFilter;

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mRenameReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "rename_done"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 320
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mRenameReceiver:Landroid/content/BroadcastReceiver;

    .line 338
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mRenameReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mRenameReceiverFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 340
    return-void
.end method

.method private setScanFileReceiver()V
    .locals 3

    .prologue
    .line 287
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mScanFileReceiverFilter:Landroid/content/IntentFilter;

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mScanFileReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mScanFileReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 294
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mScanFileReceiver:Landroid/content/BroadcastReceiver;

    .line 310
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mScanFileReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mScanFileReceiverFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 311
    return-void
.end method

.method private startExtract(Ljava/lang/String;)V
    .locals 7
    .param p1, "targetFoler"    # Ljava/lang/String;

    .prologue
    .line 1575
    const/4 v5, 0x4

    invoke-static {v5}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(I)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v3

    .line 1579
    .local v3, "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    const/4 v5, 0x2

    :try_start_0
    invoke-virtual {v3, p0, v5}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1586
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1588
    .local v4, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mExtractFile:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 1590
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mExtractFile:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1594
    :cond_0
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 1596
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v5, "src_folder"

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1598
    const-string v5, "dst_folder"

    invoke-virtual {v0, v5, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1600
    const-string v5, "target_datas"

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1602
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    .line 1604
    .local v2, "fm":Landroid/app/FragmentManager;
    if-eqz v2, :cond_1

    .line 1606
    const-string v5, "extract"

    invoke-virtual {v3, v2, v5}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1608
    .end local v0    # "arguments":Landroid/os/Bundle;
    .end local v2    # "fm":Landroid/app/FragmentManager;
    .end local v4    # "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    :goto_0
    return-void

    .line 1581
    :catch_0
    move-exception v1

    .line 1583
    .local v1, "ex":Ljava/lang/IllegalStateException;
    goto :goto_0
.end method


# virtual methods
.method public AsyncOpenCompleted()V
    .locals 0

    .prologue
    .line 1211
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->onRefresh()V

    .line 1212
    return-void
.end method

.method public clean()V
    .locals 0

    .prologue
    .line 1189
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->resetSearchToEmpty()V

    .line 1190
    return-void
.end method

.method protected deleteSelectedItem()V
    .locals 0

    .prologue
    .line 1204
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->deleteSelectedItem()V

    .line 1206
    return-void
.end method

.method public getBrowserItem(I)Lcom/sec/android/app/myfiles/element/BrowserItem;
    .locals 10
    .param p1, "position"    # I

    .prologue
    const/4 v0, -0x1

    .line 1236
    const/4 v5, 0x0

    .line 1237
    .local v5, "item":Lcom/sec/android/app/myfiles/element/BrowserItem;
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v8, :cond_3

    .line 1238
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v8, p1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/database/Cursor;

    .line 1240
    .local v2, "cursor":Landroid/database/Cursor;
    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v8

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v9

    if-gt v8, v9, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v8

    if-ltz v8, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v8

    if-nez v8, :cond_1

    .line 1241
    :cond_0
    const/4 v8, 0x0

    .line 1261
    .end local v2    # "cursor":Landroid/database/Cursor;
    :goto_0
    return-object v8

    .line 1244
    .restart local v2    # "cursor":Landroid/database/Cursor;
    :cond_1
    const-string v8, "category"

    invoke-interface {v2, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 1246
    .local v1, "categoryInd":I
    if-eq v1, v0, :cond_2

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1248
    .local v0, "category":I
    :cond_2
    const-string v8, "_id"

    invoke-interface {v2, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v2, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1250
    .local v6, "id":J
    const-string v8, "_data"

    invoke-interface {v2, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1252
    .local v3, "data":Ljava/lang/String;
    const-string v8, "format"

    invoke-interface {v2, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v2, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 1254
    .local v4, "format":I
    new-instance v5, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v5    # "item":Lcom/sec/android/app/myfiles/element/BrowserItem;
    const/16 v8, 0x11

    invoke-direct {v5, v8, v6, v7, v3}, Lcom/sec/android/app/myfiles/element/BrowserItem;-><init>(IJLjava/lang/String;)V

    .line 1256
    .restart local v5    # "item":Lcom/sec/android/app/myfiles/element/BrowserItem;
    const/16 v8, 0x8

    if-ne v0, v8, :cond_3

    .line 1257
    iput v4, v5, Lcom/sec/android/app/myfiles/element/BrowserItem;->mFormat:I

    .line 1258
    iput v0, v5, Lcom/sec/android/app/myfiles/element/BrowserItem;->mSearchCategory:I

    .end local v0    # "category":I
    .end local v1    # "categoryInd":I
    .end local v2    # "cursor":Landroid/database/Cursor;
    .end local v3    # "data":Ljava/lang/String;
    .end local v4    # "format":I
    .end local v6    # "id":J
    :cond_3
    move-object v8, v5

    .line 1261
    goto :goto_0
.end method

.method public getCurrentFolderPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 490
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOptionsMenuResId(Z)I
    .locals 1
    .param p1, "isSelectMode"    # Z

    .prologue
    .line 976
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 978
    const/4 v0, -0x1

    .line 980
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0e0016

    goto :goto_0
.end method

.method public mResultCount()I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, -0x1

    .line 1487
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 1488
    .local v0, "c":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 1489
    .local v2, "result":I
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_2

    .line 1490
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1491
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 1493
    iget v4, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mViewMode:I

    if-eq v4, v6, :cond_0

    const-string v4, "show_titlesearch_head"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    if-eq v4, v5, :cond_0

    .line 1494
    const-string v4, "show_titlesearch_head"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1495
    .local v3, "ret":Ljava/lang/String;
    const-string v4, "true"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_0

    .line 1496
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    .line 1491
    .end local v3    # "ret":Ljava/lang/String;
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1500
    :cond_0
    iget v4, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mViewMode:I

    if-eq v4, v6, :cond_1

    const-string v4, "show_contentsearch_head"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    if-eq v4, v5, :cond_1

    .line 1501
    const-string v4, "show_contentsearch_head"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1502
    .restart local v3    # "ret":Ljava/lang/String;
    const-string v4, "true"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_1

    .line 1503
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1

    .line 1508
    .end local v3    # "ret":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 1509
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1

    .line 1512
    .end local v1    # "i":I
    :cond_2
    return v2
.end method

.method public moveToFolder(Ljava/lang/String;)V
    .locals 5
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 500
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchView:Landroid/widget/SearchView;

    const-string v3, ""

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 502
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mPathStack:Ljava/util/Stack;

    invoke-virtual {v2, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 504
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mPathStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInFolder(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 506
    .local v0, "c":Landroid/database/Cursor;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->updateAdapter(Landroid/database/Cursor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 511
    .end local v0    # "c":Landroid/database/Cursor;
    :goto_0
    return-void

    .line 508
    :catch_0
    move-exception v1

    .line 509
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 381
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 382
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_0

    .line 407
    :goto_0
    return-void

    .line 385
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0f0043

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 387
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 390
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchView:Landroid/widget/SearchView;

    .line 391
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mAdvancedSearchResult:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mAdvancedSearchResult:Landroid/widget/TextView;

    .line 406
    :goto_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->showTreeView(Z)V

    goto :goto_0

    .line 399
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0f00f8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SearchView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchView:Landroid/widget/SearchView;

    .line 400
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->clearFocus()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 401
    :catch_0
    move-exception v0

    .line 402
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 10
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 762
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 764
    sparse-switch p1, :sswitch_data_0

    .line 821
    :cond_0
    :goto_0
    return-void

    .line 767
    :sswitch_0
    const/4 v7, -0x1

    if-ne p2, v7, :cond_0

    .line 768
    if-eqz p3, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v7, :cond_0

    .line 769
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 770
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 771
    const-string v7, "FILE_OPERATION"

    const/4 v8, -0x1

    invoke-virtual {p3, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 772
    .local v4, "operationType":I
    const/4 v7, 0x1

    if-eq v4, v7, :cond_1

    if-nez v4, :cond_2

    .line 774
    :cond_1
    const-string v7, "src_fragment_id"

    const/16 v8, 0x12

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 775
    const-string v7, "FOLDERPATH"

    const-string v8, "target_folder"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 780
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mCallback:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$onCopyMoveSelectedListener;

    const-string v8, "target_folder"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v0, v8}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$onCopyMoveSelectedListener;->onCopyMoveSelected(Landroid/os/Bundle;Ljava/lang/String;)V

    goto :goto_0

    .line 782
    :cond_2
    const/4 v7, 0x4

    if-ne v4, v7, :cond_3

    .line 783
    new-instance v1, Ljava/io/File;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mExtractFile:Ljava/lang/String;

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 784
    .local v1, "dstfile":Ljava/io/File;
    const-string v7, "src_fragment_id"

    const/16 v8, 0x12

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 785
    const-string v7, "FOLDERPATH"

    invoke-virtual {v1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    new-instance v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-direct {v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;-><init>()V

    .line 790
    .local v3, "fragment":Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;
    const-string v7, "id"

    const/16 v8, 0x201

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 792
    invoke-virtual {v3, v0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->setArguments(Landroid/os/Bundle;)V

    .line 794
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v7, v7, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v7, :cond_0

    .line 795
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v7, Lcom/sec/android/app/myfiles/MainActivity;

    const/16 v8, 0x201

    invoke-virtual {v7, v8, v3}, Lcom/sec/android/app/myfiles/MainActivity;->fromSearchExtract(ILcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    goto :goto_0

    .line 797
    .end local v1    # "dstfile":Ljava/io/File;
    .end local v3    # "fragment":Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;
    :cond_3
    const/4 v7, 0x7

    if-ne v4, v7, :cond_4

    iget v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mContextMenuPosition:I

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getBrowserItem(I)Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-result-object v7

    if-eqz v7, :cond_4

    iget v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mContextMenuPosition:I

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getBrowserItem(I)Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v7

    const-string v8, "/storage"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 799
    const/4 v7, 0x0

    const-string v8, "SearchFragment"

    const-string v9, "onActivityResult : REQUEST_FILE_OPERATION, resultCode == Activity.RESULT_OK, refresh()"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 800
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->AsyncOpen()V

    goto/16 :goto_0

    .line 802
    :cond_4
    const/4 v7, 0x7

    if-eq v4, v7, :cond_5

    const/4 v7, 0x2

    if-ne v4, v7, :cond_0

    :cond_5
    iget v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mContextMenuPosition:I

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getBrowserItem(I)Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-result-object v7

    if-eqz v7, :cond_0

    iget v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mContextMenuPosition:I

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getBrowserItem(I)Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v7

    const-string v8, "/storage"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 803
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->isSearchRenameorDelete:Z

    goto/16 :goto_0

    .line 810
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v4    # "operationType":I
    :sswitch_1
    const/4 v7, -0x1

    if-ne p2, v7, :cond_0

    .line 811
    const-string v7, "result_value"

    invoke-virtual {p3, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 814
    .local v5, "targetFolderName":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mExtractFile:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 815
    .local v2, "extractFile":Ljava/io/File;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 816
    .local v6, "targetFolderPath":Ljava/lang/String;
    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->startExtract(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 764
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x6 -> :sswitch_1
    .end sparse-switch
.end method

.method public onBackPressed()Z
    .locals 5

    .prologue
    .line 466
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getmBackFragmentID()I

    move-result v2

    .line 467
    .local v2, "mBackFragment":I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 468
    .local v0, "b":Landroid/os/Bundle;
    if-nez v2, :cond_1

    .line 469
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView(Landroid/os/Bundle;)V

    .line 484
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v2    # "mBackFragment":I
    :cond_0
    :goto_0
    const/4 v3, 0x1

    return v3

    .line 470
    .restart local v0    # "b":Landroid/os/Bundle;
    .restart local v2    # "mBackFragment":I
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    instance-of v3, v3, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v3, :cond_0

    .line 471
    const/16 v3, 0x201

    if-eq v2, v3, :cond_2

    const/16 v3, 0x9

    if-eq v2, v3, :cond_2

    const/16 v3, 0xa

    if-eq v2, v3, :cond_2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_2

    const/16 v3, 0x1f

    if-ne v2, v3, :cond_3

    .line 473
    :cond_2
    const-string v3, "FOLDERPATH"

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentDirectory()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v3, v2, v0}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 480
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v2    # "mBackFragment":I
    :catch_0
    move-exception v1

    .line 482
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1272
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1273
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->showTreeView(Z)V

    .line 1274
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->configurationChanged(Landroid/content/res/Configuration;)V

    .line 1275
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 17
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 826
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v5

    check-cast v5, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 827
    .local v5, "info":Landroid/widget/AdapterView$AdapterContextMenuInfo;
    iget v11, v5, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    move-object/from16 v0, p0

    iput v11, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mContextMenuPosition:I

    .line 828
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-nez v11, :cond_0

    .line 829
    const/4 v11, 0x1

    .line 970
    :goto_0
    return v11

    .line 831
    :cond_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mContextMenuPosition:I

    invoke-virtual {v11, v12}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    .line 833
    .local v1, "cursor":Landroid/database/Cursor;
    if-nez v1, :cond_1

    .line 834
    const/4 v11, 0x1

    goto :goto_0

    .line 837
    :cond_1
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v11

    packed-switch v11, :pswitch_data_0

    .line 970
    :goto_1
    :pswitch_0
    const/4 v11, 0x1

    goto :goto_0

    .line 841
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->startShareAppList()V

    .line 842
    const/4 v11, 0x1

    goto :goto_0

    .line 847
    :pswitch_2
    const/4 v11, 0x0

    const-string v12, "SearchFragment"

    const-string v13, "onContextItemSelected : RENAME"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 848
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->startRename()V

    .line 849
    const/4 v11, 0x1

    goto :goto_0

    .line 854
    :pswitch_3
    new-instance v6, Landroid/content/Intent;

    const-string v11, "com.sec.android.app.myfiles.PICK_SELECT_PATH"

    invoke-direct {v6, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 856
    .local v6, "intent":Landroid/content/Intent;
    const-string v11, "FILE_OPERATION"

    const/4 v12, 0x0

    invoke-virtual {v6, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 858
    const-string v11, "SELECTOR_SOURCE_TYPE"

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mFragmentId:I

    invoke-virtual {v6, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 860
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v11, :cond_3

    .line 862
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 863
    .local v8, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mContextMenuPosition:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getBrowserItem(I)Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 865
    if-eqz v8, :cond_3

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lez v11, :cond_3

    .line 866
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v11

    new-array v9, v11, [Ljava/lang/String;

    .line 867
    .local v9, "paths":[Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-ge v4, v11, :cond_2

    .line 868
    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/myfiles/element/BrowserItem;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v4

    .line 867
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 870
    :cond_2
    const-string v11, "SELECTOR_SOURCE_PATH"

    invoke-virtual {v6, v11, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 874
    .end local v4    # "i":I
    .end local v8    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v9    # "paths":[Ljava/lang/String;
    :cond_3
    const-string v11, "SELECTOR_FTP_MOVE"

    const/4 v12, 0x0

    invoke-virtual {v6, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 878
    const/4 v11, 0x0

    :try_start_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v11}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 886
    :goto_3
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 879
    :catch_0
    move-exception v3

    .line 881
    .local v3, "e":Landroid/content/ActivityNotFoundException;
    const/4 v11, 0x2

    const-string v12, "SearchFragment"

    const-string v13, "com.sec.android.app.myfiles.PICK_SELECT_PATH not found - copy"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 890
    .end local v3    # "e":Landroid/content/ActivityNotFoundException;
    .end local v6    # "intent":Landroid/content/Intent;
    :pswitch_4
    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->showDetail(Ljava/lang/String;)V

    .line 891
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 895
    :pswitch_5
    new-instance v7, Landroid/content/Intent;

    const-string v11, "com.sec.android.app.myfiles.PICK_SELECT_PATH"

    invoke-direct {v7, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 897
    .local v7, "intentForMove":Landroid/content/Intent;
    const-string v11, "FILE_OPERATION"

    const/4 v12, 0x1

    invoke-virtual {v7, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 899
    const-string v11, "SELECTOR_SOURCE_TYPE"

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mFragmentId:I

    invoke-virtual {v7, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 901
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v11, :cond_5

    .line 903
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 904
    .restart local v8    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mContextMenuPosition:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getBrowserItem(I)Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 906
    if-eqz v8, :cond_5

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lez v11, :cond_5

    .line 907
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v11

    new-array v9, v11, [Ljava/lang/String;

    .line 908
    .restart local v9    # "paths":[Ljava/lang/String;
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_4
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-ge v4, v11, :cond_4

    .line 909
    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/myfiles/element/BrowserItem;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v4

    .line 908
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 911
    :cond_4
    const-string v11, "SELECTOR_SOURCE_PATH"

    invoke-virtual {v7, v11, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 915
    .end local v4    # "i":I
    .end local v8    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v9    # "paths":[Ljava/lang/String;
    :cond_5
    const-string v11, "SELECTOR_FTP_MOVE"

    const/4 v12, 0x1

    invoke-virtual {v7, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 920
    const/4 v11, 0x1

    :try_start_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v11}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 928
    :goto_5
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 921
    :catch_1
    move-exception v3

    .line 923
    .restart local v3    # "e":Landroid/content/ActivityNotFoundException;
    const/4 v11, 0x2

    const-string v12, "SearchFragment"

    const-string v13, "com.sec.android.app.myfiles.PICK_SELECT_PATH not found - move"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 932
    .end local v3    # "e":Landroid/content/ActivityNotFoundException;
    .end local v7    # "intentForMove":Landroid/content/Intent;
    :pswitch_6
    new-instance v2, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v2, v11}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 934
    .local v2, "dialog":Landroid/app/AlertDialog$Builder;
    const v11, 0x7f0b0020

    invoke-virtual {v2, v11}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 937
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0c0004

    const/4 v13, 0x1

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const/16 v16, 0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v11, v12, v13, v14}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 940
    .local v10, "selectText":Ljava/lang/String;
    invoke-virtual {v2, v10}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 943
    const v11, 0x7f0b0015

    new-instance v12, Lcom/sec/android/app/myfiles/fragment/SearchFragment$7;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment$7;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)V

    invoke-virtual {v2, v11, v12}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 953
    const v11, 0x7f0b0017

    new-instance v12, Lcom/sec/android/app/myfiles/fragment/SearchFragment$8;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment$8;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)V

    invoke-virtual {v2, v11, v12}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 961
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_1

    .line 837
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 158
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreate(Landroid/os/Bundle;)V

    .line 160
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->setScanFileReceiver()V

    .line 162
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->setRenameReceiver()V

    .line 164
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    .line 166
    const/16 v0, 0x201

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mCategoryType:I

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchNavigation:Lcom/sec/android/app/myfiles/navigation/SearchNavigation;

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setSupportAsync(Lcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    .line 178
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 180
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mIntentFilter:Landroid/content/IntentFilter;

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "Media_DB_Update_Finsished_by_Scanner"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.android.MTP.OBJECT_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "action.documentservice.index.finished"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 191
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 215
    new-instance v0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->dialog:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    .line 216
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 22
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 680
    if-nez p3, :cond_1

    .line 758
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v10, p3

    .line 684
    check-cast v10, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 685
    .local v10, "info":Landroid/widget/AdapterView$AdapterContextMenuInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v18, v0

    iget v0, v10, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v12

    .line 687
    .local v12, "item":Ljava/lang/Object;
    if-eqz v12, :cond_0

    .line 691
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v18, v0

    iget v0, v10, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/database/Cursor;

    .line 692
    .local v5, "cursor":Landroid/database/Cursor;
    const-string v18, "_data"

    move-object/from16 v0, v18

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 694
    .local v15, "path":Ljava/lang/String;
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_0

    .line 697
    const-string v18, "/storage"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_2

    const-string v18, "format"

    move-object/from16 v0, v18

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    const v19, 0x7f0b0007

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_7

    .line 700
    :cond_2
    const-string v18, "_display_name"

    move-object/from16 v0, v18

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 701
    .local v6, "displayname_column":I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v6, v0, :cond_5

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 702
    .local v14, "name":Ljava/lang/String;
    :goto_1
    const/16 v16, 0x0

    .line 704
    .local v16, "size":I
    :try_start_0
    const-string v18, "_size"

    move-object/from16 v0, v18

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v16

    .line 709
    :goto_2
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 711
    .local v8, "file":Ljava/io/File;
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_0

    .line 716
    if-nez v14, :cond_3

    .line 717
    invoke-static {v15}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getFileNameInPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 720
    :cond_3
    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    .line 722
    const/16 v18, 0x0

    const/16 v19, 0x2

    const/16 v20, 0x0

    const v21, 0x7f0b0020

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    move/from16 v4, v21

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 723
    invoke-virtual {v8}, Ljava/io/File;->isFile()Z

    move-result v18

    if-eqz v18, :cond_6

    .line 724
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mContextSelectedItemIsFile:Z

    .line 728
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mContextSelectedItemIsFile:Z

    move/from16 v18, v0

    if-eqz v18, :cond_4

    if-lez v16, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v0, v15}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isForwardable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 730
    const/16 v18, 0x0

    const/16 v19, 0x9

    const/16 v20, 0x0

    const v21, 0x7f0b001f

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    move/from16 v4, v21

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 732
    :cond_4
    const/16 v18, 0x0

    const/16 v19, 0x1

    const/16 v20, 0x0

    const v21, 0x7f0b0021

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    move/from16 v4, v21

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 733
    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const v21, 0x7f0b0022

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    move/from16 v4, v21

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 734
    const/16 v18, 0x0

    const/16 v19, 0x7

    const/16 v20, 0x0

    const v21, 0x7f0b0023

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    move/from16 v4, v21

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 735
    const/16 v18, 0x0

    const/16 v19, 0xa

    const/16 v20, 0x0

    const v21, 0x7f0b0025

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    move/from16 v4, v21

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 737
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchView:Landroid/widget/SearchView;

    move-object/from16 v18, v0

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchView:Landroid/widget/SearchView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/SearchView;->hasFocus()Z

    move-result v18

    if-eqz v18, :cond_0

    .line 738
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchView:Landroid/widget/SearchView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/SearchView;->clearFocus()V

    goto/16 :goto_0

    .line 701
    .end local v8    # "file":Ljava/io/File;
    .end local v14    # "name":Ljava/lang/String;
    .end local v16    # "size":I
    :cond_5
    const/4 v14, 0x0

    goto/16 :goto_1

    .line 706
    .restart local v14    # "name":Ljava/lang/String;
    .restart local v16    # "size":I
    :catch_0
    move-exception v7

    .line 707
    .local v7, "ex":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 726
    .end local v7    # "ex":Ljava/lang/Exception;
    .restart local v8    # "file":Ljava/io/File;
    :cond_6
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mContextSelectedItemIsFile:Z

    goto/16 :goto_3

    .line 741
    .end local v6    # "displayname_column":I
    .end local v8    # "file":Ljava/io/File;
    .end local v14    # "name":Ljava/lang/String;
    .end local v16    # "size":I
    :cond_7
    const-string v18, "/"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v13

    .line 742
    .local v13, "lastslash":I
    add-int/lit8 v18, v13, 0x1

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    .line 744
    .local v17, "title":Ljava/lang/String;
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    .line 745
    const/16 v18, 0x0

    const/16 v19, 0x2

    const/16 v20, 0x0

    const v21, 0x7f0b0020

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    move/from16 v4, v21

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 746
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v0, v15}, Lcom/sec/android/app/myfiles/utils/Utils;->isDirectoryOfDropbox(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    .line 747
    .local v11, "isDirectory":Ljava/lang/Boolean;
    invoke-static {v15}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v9

    .line 749
    .local v9, "fileTypeInt":I
    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v18

    if-nez v18, :cond_8

    invoke-static {v9}, Lcom/sec/android/app/myfiles/MediaFile;->isAudioFileType(I)Z

    move-result v18

    if-nez v18, :cond_8

    invoke-static {v9}, Lcom/sec/android/app/myfiles/MediaFile;->isVideoFileType(I)Z

    move-result v18

    if-nez v18, :cond_8

    .line 750
    const/16 v18, 0x0

    const/16 v19, 0x9

    const/16 v20, 0x0

    const v21, 0x7f0b001f

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    move/from16 v4, v21

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 753
    :cond_8
    const/16 v18, 0x0

    const/16 v19, 0x1

    const/16 v20, 0x0

    const v21, 0x7f0b0021

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    move/from16 v4, v21

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 754
    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const v21, 0x7f0b0022

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    move/from16 v4, v21

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 755
    const/16 v18, 0x0

    const/16 v19, 0x7

    const/16 v20, 0x0

    const v21, 0x7f0b0023

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    move/from16 v4, v21

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 756
    const/16 v18, 0x0

    const/16 v19, 0xa

    const/16 v20, 0x0

    const v21, 0x7f0b0025

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    move/from16 v4, v21

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    goto/16 :goto_0
.end method

.method protected onCreateDetailFragmentListener()Landroid/app/DialogFragment;
    .locals 2

    .prologue
    .line 1222
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mContextMenuPosition:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getBrowserItem(I)Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/storage"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1223
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/DetailFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/DetailFragment;-><init>()V

    .line 1229
    :goto_0
    return-object v0

    .line 1226
    :cond_0
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1227
    new-instance v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDetailFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDetailFragment;-><init>()V

    goto :goto_0

    .line 1229
    :cond_1
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/DropboxDetailFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/DropboxDetailFragment;-><init>()V

    goto :goto_0
.end method

.method protected onCreateFileOperationListener()Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;
    .locals 1

    .prologue
    .line 1197
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 517
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment$5;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)V

    return-object v0
.end method

.method protected onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1

    .prologue
    .line 668
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment$6;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)V

    return-object v0
.end method

.method protected onCreateMultipleDetailFragmentListener()Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 1266
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 269
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 272
    .local v0, "view":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setDividerHeight(I)V

    .line 273
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 275
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->createScrollListener()Landroid/widget/AbsListView$OnScrollListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 277
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchNavigation:Lcom/sec/android/app/myfiles/navigation/SearchNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->getEmptyCourso()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 279
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 281
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 283
    return-object v0
.end method

.method public onDestroy()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 221
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onDestroy()V

    .line 223
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->isSearchRenameorDeleteRefresh:Z

    .line 224
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->isSearchRenameorDelete:Z

    .line 226
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v2, :cond_0

    .line 227
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->closeCursor()V

    .line 229
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mScanFileReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_1

    .line 231
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mScanFileReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 237
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mRenameReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_2

    .line 241
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mRenameReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 249
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_3

    .line 251
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 258
    :cond_3
    :goto_2
    new-instance v1, Landroid/content/Intent;

    const-string v2, "action.index.SEARCH"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 259
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 260
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->dialog:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    if-eqz v2, :cond_4

    .line 261
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->dialog:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->dismissWaitProgressDialog()V

    .line 262
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->dialog:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    .line 264
    :cond_4
    return-void

    .line 232
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 233
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 243
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 245
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v2, "SearchFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onDestroy : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 252
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v0

    .line 253
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 1287
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onDestroyView()V

    .line 1288
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->cancelSearchTaskifRunning()V

    .line 1289
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v3, 0x7f0f0134

    const v2, 0x7f0f0133

    const/4 v1, 0x0

    .line 412
    if-eqz p1, :cond_1

    .line 414
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 415
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 417
    :cond_0
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 418
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 422
    :cond_1
    return-void
.end method

.method public onRefresh()V
    .locals 3

    .prologue
    .line 442
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mPathStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 444
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchView:Landroid/widget/SearchView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_1

    .line 446
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchView:Landroid/widget/SearchView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 459
    :cond_0
    :goto_0
    return-void

    .line 448
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchView:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mCurrentParam:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    if-eqz v0, :cond_0

    .line 450
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mCurrentParam:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->search(Lcom/sec/android/app/myfiles/utils/SearchParameter;)V

    goto :goto_0

    .line 456
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mPathStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInFolder(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->updateAdapter(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 428
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onResume()V

    .line 429
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->isSearchRenameorDeleteRefresh:Z

    .line 430
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->isSearchRenameorDelete:Z

    .line 431
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchView:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    .line 432
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->requestFocus()Z

    .line 434
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mClearFocusAfterSearch:Z

    .line 435
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->onRefresh()V

    .line 436
    return-void
.end method

.method public resetSearchToEmpty()V
    .locals 4

    .prologue
    .line 1000
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchView:Landroid/widget/SearchView;

    const-string v2, ""

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 1001
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchNavigation:Lcom/sec/android/app/myfiles/navigation/SearchNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->getEmptyCourso()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->updateAdapter(Landroid/database/Cursor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1005
    :goto_0
    return-void

    .line 1002
    :catch_0
    move-exception v0

    .line 1003
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public search(Lcom/sec/android/app/myfiles/utils/SearchParameter;)V
    .locals 10
    .param p1, "param"    # Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .prologue
    const-wide/16 v8, 0x3e8

    const/4 v6, 0x0

    .line 1011
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mPathStack:Ljava/util/Stack;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mPathStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 1012
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mPathStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->clear()V

    .line 1014
    :cond_0
    if-eqz p1, :cond_3

    .line 1015
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mCurrentParam:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .line 1016
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getKeyword()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSavedSearchKeyWord:Ljava/lang/String;

    .line 1017
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v2, v2, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v2, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mSearchFromAdvance:Z

    if-eqz v2, :cond_2

    .line 1018
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1019
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/EnumSet;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    .line 1020
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    const/16 v4, 0x5b

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    .line 1021
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    .line 1023
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateFrom()J

    move-result-wide v4

    mul-long/2addr v4, v8

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/Utils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1024
    .local v0, "fromDate":[Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateTo()J

    move-result-wide v4

    mul-long/2addr v4, v8

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/Utils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1025
    .local v1, "toDate":[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v3, v0, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  -  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSavedSearchDate:Ljava/lang/String;

    .line 1026
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getExtentionString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSavedSearchExtension:Ljava/lang/String;

    .line 1028
    .end local v0    # "fromDate":[Ljava/lang/String;
    .end local v1    # "toDate":[Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-nez v2, :cond_4

    .line 1042
    :cond_3
    :goto_0
    return-void

    .line 1031
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSavedSearchKeyWord:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getExtentionString()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getExtentionString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1032
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v2, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getExtentionString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->setSearchKeyword(Ljava/lang/String;)V

    .line 1038
    :goto_1
    new-instance v2, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchFragment;Lcom/sec/android/app/myfiles/utils/SearchParameter;)V

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchTask:Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;

    .line 1039
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchTask:Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;

    sget-object v3, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v4, v6, [Ljava/lang/Void;

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 1034
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v2, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSavedSearchKeyWord:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->setSearchKeyword(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public setSelectModeActionBar()V
    .locals 0

    .prologue
    .line 1217
    return-void
.end method
