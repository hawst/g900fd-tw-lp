.class Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$13;
.super Ljava/lang/Object;
.source "AbsListableFragment.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->showCheckBox()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;)V
    .locals 0

    .prologue
    .line 804
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$13;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 14

    .prologue
    const v11, 0x7f0f0037

    const-wide/16 v12, 0x190

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 808
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$13;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v7

    invoke-virtual {v7, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 810
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$13;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getChildCount()I

    move-result v2

    .line 811
    .local v2, "childCount":I
    if-nez v2, :cond_1

    .line 870
    :cond_0
    return v10

    .line 813
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$13;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v7, v10}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 814
    .local v6, "sampleCheckbox":Landroid/view/View;
    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 815
    .local v1, "checkboxWidth":I
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$13;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    const/4 v8, 0x1

    # setter for: Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mIsInEditMode:Z
    invoke-static {v7, v8}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->access$302(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;Z)Z

    .line 816
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$13;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    # setter for: Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mContainerPaddingLeft:I
    invoke-static {v7, v1}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->access$402(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;I)I

    .line 818
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v2, :cond_0

    .line 819
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$13;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v7, v5}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 820
    .local v0, "chb":Landroid/view/View;
    neg-int v7, v1

    int-to-float v7, v7

    invoke-virtual {v0, v7}, Landroid/view/View;->setTranslationX(F)V

    .line 821
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v7, v8}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    invoke-virtual {v7, v12, v13}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->SINE_IN_OUT_70:Landroid/view/animation/Interpolator;
    invoke-static {}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->access$500()Landroid/view/animation/Interpolator;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 829
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$13;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v7, v5}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f0f0030

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 830
    .local v3, "container":Landroid/widget/RelativeLayout;
    neg-int v7, v1

    int-to-float v7, v7

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->setTranslationX(F)V

    .line 831
    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->SINE_IN_OUT_70:Landroid/view/animation/Interpolator;
    invoke-static {}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->access$500()Landroid/view/animation/Interpolator;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    invoke-virtual {v7, v12, v13}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    new-instance v8, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$13$1;

    invoke-direct {v8, p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$13$1;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$13;)V

    invoke-virtual {v7, v8}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 856
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$13;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v7, v5}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f0f0064

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 857
    .local v4, "container3":Landroid/widget/LinearLayout;
    neg-int v7, v1

    int-to-float v7, v7

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->setTranslationX(F)V

    .line 858
    invoke-virtual {v4}, Landroid/widget/LinearLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->SINE_IN_OUT_70:Landroid/view/animation/Interpolator;
    invoke-static {}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->access$500()Landroid/view/animation/Interpolator;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    invoke-virtual {v7, v12, v13}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    new-instance v8, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$13$2;

    invoke-direct {v8, p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$13$2;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$13;)V

    invoke-virtual {v7, v8}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 818
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0
.end method
