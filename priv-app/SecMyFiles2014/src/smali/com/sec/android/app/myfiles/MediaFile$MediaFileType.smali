.class public Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;
.super Ljava/lang/Object;
.source "MediaFile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/MediaFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MediaFileType"
.end annotation


# instance fields
.field public description:Ljava/lang/String;

.field public fileType:I

.field public iconLarge:I

.field public iconSmall:I

.field public mimeType:Ljava/lang/String;


# direct methods
.method constructor <init>(ILjava/lang/String;Ljava/lang/String;II)V
    .locals 0
    .param p1, "fileType"    # I
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "desc"    # Ljava/lang/String;
    .param p4, "iconSmall"    # I
    .param p5, "iconLarge"    # I

    .prologue
    .line 370
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 371
    iput p1, p0, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;->fileType:I

    .line 372
    iput-object p2, p0, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;->mimeType:Ljava/lang/String;

    .line 373
    iput-object p3, p0, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;->description:Ljava/lang/String;

    .line 374
    iput p4, p0, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;->iconSmall:I

    .line 375
    iput p5, p0, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;->iconLarge:I

    .line 376
    return-void
.end method
