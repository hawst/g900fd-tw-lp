.class public Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;
.super Landroid/app/Fragment;
.source "SearchAdvancedFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final MODULE:Ljava/lang/String; = "SearchAdvancedFragment"


# instance fields
.field private final KEYBOARD_DISPLAY:I

.field private final KEYBOARD_HIDE:I

.field private final MAX_EXTENTION_TEXT_LIMIT:I

.field private final MAX_SEARCH_TEXT_LIMIT:I

.field deviceId:Ljava/lang/String;

.field deviceName:Ljava/lang/String;

.field private imm:Landroid/view/inputmethod/InputMethodManager;

.field private isAdvanceSearchDialog:Z

.field private isFileTypeAll:Z

.field private isFileTypeDocs:Z

.field private isFileTypeDownloads:Z

.field private isFileTypeImages:Z

.field private isFileTypeMusic:Z

.field private isFileTypeVideos:Z

.field private isLocationBaidu:Z

.field private isLocationDevice:Z

.field private isLocationDropbox:Z

.field private isLocationNearby:Z

.field private isLocationPrivate:Z

.field private isLocationSDcard:Z

.field private isLocationUSBa:Z

.field private isLocationUSBb:Z

.field private isLocationUSBc:Z

.field private isLocationUSBd:Z

.field private isLocationUSBe:Z

.field private isLocationUSBf:Z

.field private isNearBydDevice:Z

.field public mActionbar_cancel:Landroid/widget/TextView;

.field public mActionbar_ok:Landroid/widget/TextView;

.field private mActivity:Landroid/app/Activity;

.field private mCurrentFileTypes:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/sec/android/app/myfiles/utils/FileType;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentFromDate:J

.field private mCurrentToDate:J

.field mDateDelete:Landroid/widget/ImageView;

.field mDateEditText:Landroid/widget/RelativeLayout;

.field mDateImage:Landroid/widget/LinearLayout;

.field mDateSummary:Landroid/widget/TextView;

.field mExtentionImage:Landroid/widget/LinearLayout;

.field mExtentionSearchView:Landroid/widget/EditText;

.field mFileTypeImage:Landroid/widget/LinearLayout;

.field mFileTypeSummary:Landroid/widget/TextView;

.field private mIMEHandler:Landroid/os/Handler;

.field mLocationImage:Landroid/widget/LinearLayout;

.field mLocationSummary:Landroid/widget/TextView;

.field mNameImage:Landroid/widget/LinearLayout;

.field mNameSearchView:Landroid/widget/EditText;

.field private mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

.field public mStartDate:Ljava/lang/String;

.field public mStartExtension:Ljava/lang/String;

.field public mStartFileType:Ljava/lang/String;

.field public mStartLocation:Ljava/lang/String;

.field private selectedLocation:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 87
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->MAX_SEARCH_TEXT_LIMIT:I

    .line 88
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->MAX_EXTENTION_TEXT_LIMIT:I

    .line 98
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->KEYBOARD_DISPLAY:I

    .line 99
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->KEYBOARD_HIDE:I

    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isAdvanceSearchDialog:Z

    .line 509
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mIMEHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;
    .param p1, "x1"    # J

    .prologue
    .line 45
    iput-wide p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mCurrentToDate:J

    return-wide p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;
    .param p1, "x1"    # J

    .prologue
    .line 45
    iput-wide p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mCurrentFromDate:J

    return-wide p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;)Landroid/view/inputmethod/InputMethodManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->imm:Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isAdvanceSearchDialog:Z

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->clickOK()V

    return-void
.end method

.method private clickOK()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    .line 583
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 585
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "search_advance_name_intent_key"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mNameSearchView:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 591
    const-string v1, "SEARCH_LOCATION_CHANGED"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 593
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mFileTypeSummary:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0b008d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 594
    const-string v1, "SEARCH_FILETYPE_CHANGED"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 596
    :cond_0
    iget-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mCurrentFromDate:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_1

    iget-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mCurrentToDate:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_1

    .line 597
    const-string v1, "SEARCH_DATE_CHANGED"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 599
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mStartExtension:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mExtentionSearchView:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 600
    const-string v1, "SEARCH_EXTENSION_CHANGED"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 602
    :cond_2
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isNearBydDevice:Z

    if-eqz v1, :cond_3

    .line 603
    const-string v1, "search_set_location_is_near_by_device_intent_key"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 604
    const-string v1, "provider_id"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->deviceId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 605
    const-string v1, "provider_name"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 606
    iput-wide v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mCurrentFromDate:J

    .line 607
    iput-wide v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mCurrentToDate:J

    .line 609
    :cond_3
    const-string v1, "search_set_location_intent_key"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->selectedLocation:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 611
    const v1, 0x7f0b0001

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeAll:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 612
    const v1, 0x7f0b0003

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeImages:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 613
    const v1, 0x7f0b0004

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeVideos:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 614
    const v1, 0x7f0b0147

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeMusic:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 615
    const v1, 0x7f0b0006

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeDocs:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 616
    const v1, 0x7f0b0007

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeDownloads:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 617
    const-string v1, "search_from_date_intent_key"

    iget-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mCurrentFromDate:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 618
    const-string v1, "search_to_date_intent_key"

    iget-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mCurrentToDate:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 619
    const-string v1, "search_advance_extention_intent_key"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mExtentionSearchView:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 621
    const/4 v1, 0x6

    const/4 v2, -0x1

    invoke-virtual {p0, v1, v2, v0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->onClickSearchCallback(IILandroid/content/Intent;)V

    .line 622
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActivity:Landroid/app/Activity;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActivity:Landroid/app/Activity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v1, :cond_4

    .line 623
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActivity:Landroid/app/Activity;

    check-cast v1, Lcom/sec/android/app/myfiles/MainActivity;

    iput-boolean v4, v1, Lcom/sec/android/app/myfiles/MainActivity;->mSearchFromAdvance:Z

    .line 625
    :cond_4
    return-void
.end method

.method private getFileType(Landroid/content/Intent;)Ljava/util/EnumSet;
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/sec/android/app/myfiles/utils/FileType;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 867
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 868
    const v3, 0x7f0b0001

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 869
    .local v0, "all":Z
    if-eqz v0, :cond_1

    .line 870
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeAll:Z

    .line 871
    sget-object v2, Lcom/sec/android/app/myfiles/utils/FileType;->IMAGE:Lcom/sec/android/app/myfiles/utils/FileType;

    sget-object v3, Lcom/sec/android/app/myfiles/utils/FileType;->DOWNLOADEDAPPS:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-static {v2, v3}, Ljava/util/EnumSet;->range(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v2

    .line 919
    .end local v0    # "all":Z
    :cond_0
    :goto_0
    return-object v2

    .line 873
    .restart local v0    # "all":Z
    :cond_1
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeAll:Z

    .line 874
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 875
    .local v1, "fileTypes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/utils/FileType;>;"
    const v3, 0x7f0b0003

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 876
    sget-object v3, Lcom/sec/android/app/myfiles/utils/FileType;->IMAGE:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 877
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeImages:Z

    .line 882
    :goto_1
    const v3, 0x7f0b0147

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 883
    sget-object v3, Lcom/sec/android/app/myfiles/utils/FileType;->AUDIO:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 884
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeMusic:Z

    .line 889
    :goto_2
    const v3, 0x7f0b0004

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 890
    sget-object v3, Lcom/sec/android/app/myfiles/utils/FileType;->VIDEO:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 891
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeVideos:Z

    .line 896
    :goto_3
    const v3, 0x7f0b0006

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 897
    sget-object v3, Lcom/sec/android/app/myfiles/utils/FileType;->DOCUMENT:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 898
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeDocs:Z

    .line 903
    :goto_4
    const v3, 0x7f0b0007

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 904
    sget-object v3, Lcom/sec/android/app/myfiles/utils/FileType;->DOWNLOADEDAPPS:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 905
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeDownloads:Z

    .line 910
    :goto_5
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 913
    invoke-static {v1}, Ljava/util/EnumSet;->copyOf(Ljava/util/Collection;)Ljava/util/EnumSet;

    move-result-object v2

    goto :goto_0

    .line 879
    :cond_2
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeImages:Z

    goto :goto_1

    .line 886
    :cond_3
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeMusic:Z

    goto :goto_2

    .line 893
    :cond_4
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeVideos:Z

    goto :goto_3

    .line 900
    :cond_5
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeDocs:Z

    goto :goto_4

    .line 907
    :cond_6
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeDownloads:Z

    goto :goto_5
.end method


# virtual methods
.method public nearbySearchSet()V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const v3, 0x3ecccccd    # 0.4f

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1119
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isNearBydDevice:Z

    if-eqz v0, :cond_0

    .line 1120
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mFileTypeImage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1121
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mExtentionImage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1122
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateImage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1123
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateEditText:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 1124
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mExtentionSearchView:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 1125
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateImage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 1126
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mFileTypeImage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 1127
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mExtentionImage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 1138
    :goto_0
    return-void

    .line 1129
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mFileTypeImage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1130
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mExtentionImage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1131
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mExtentionSearchView:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 1132
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateEditText:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 1133
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateImage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 1134
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mFileTypeImage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 1135
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mExtentionImage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setAlpha(F)V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x2

    .line 465
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 467
    const/4 v0, -0x1

    .line 468
    .local v0, "focusable":I
    const/4 v2, 0x1

    .line 469
    .local v2, "index":I
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 470
    .local v6, "searchLocationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSearchFileLocation()Ljava/util/ArrayList;

    move-result-object v6

    .line 471
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 472
    .local v4, "locationStringBuilder":Ljava/lang/StringBuilder;
    const-string v5, ""

    .line 474
    .local v5, "preLocation":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 475
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "SEARCH_EXTENSION_FOCUS"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 477
    :cond_0
    const/4 v7, -0x1

    if-eq v0, v7, :cond_1

    if-ne v0, v9, :cond_6

    .line 478
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mNameSearchView:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->requestFocus()Z

    .line 479
    if-ne v0, v9, :cond_5

    .line 480
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 481
    .local v3, "item":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lt v2, v7, :cond_2

    .line 482
    move-object v5, v3

    .line 483
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 485
    :cond_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 486
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 487
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 490
    .end local v3    # "item":Ljava/lang/String;
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mLocationSummary:Landroid/widget/TextView;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 491
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isNearBydDevice:Z

    .line 492
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v7

    if-eqz v7, :cond_4

    .line 493
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "provider_id"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->deviceId:Ljava/lang/String;

    .line 494
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "provider_name"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->deviceName:Ljava/lang/String;

    .line 496
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->deviceId:Ljava/lang/String;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->deviceName:Ljava/lang/String;

    if-eqz v7, :cond_5

    .line 497
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mLocationSummary:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->deviceName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 498
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->nearbySearchSet()V

    .line 506
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_5
    :goto_1
    return-void

    .line 502
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mExtentionSearchView:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_1
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 16
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 689
    invoke-super/range {p0 .. p3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 690
    const/4 v10, -0x1

    move/from16 v0, p2

    if-ne v0, v10, :cond_1d

    .line 691
    packed-switch p1, :pswitch_data_0

    .line 860
    :cond_0
    :goto_0
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iput-boolean v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isAdvanceSearchDialog:Z

    .line 861
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mIMEHandler:Landroid/os/Handler;

    const/4 v11, 0x3

    const-wide/16 v12, 0x64

    invoke-virtual {v10, v11, v12, v13}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 862
    return-void

    .line 693
    :pswitch_0
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getFileType(Landroid/content/Intent;)Ljava/util/EnumSet;

    move-result-object v10

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mCurrentFileTypes:Ljava/util/EnumSet;

    .line 694
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mCurrentFileTypes:Ljava/util/EnumSet;

    if-eqz v10, :cond_7

    .line 695
    const/4 v4, 0x0

    .line 696
    .local v4, "fileTypes":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mCurrentFileTypes:Ljava/util/EnumSet;

    sget-object v11, Lcom/sec/android/app/myfiles/utils/FileType;->IMAGE:Lcom/sec/android/app/myfiles/utils/FileType;

    sget-object v12, Lcom/sec/android/app/myfiles/utils/FileType;->DOWNLOADEDAPPS:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-static {v11, v12}, Ljava/util/EnumSet;->range(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/EnumSet;->containsAll(Ljava/util/Collection;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 697
    const v10, 0x7f0b0001

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 728
    :goto_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mFileTypeSummary:Landroid/widget/TextView;

    invoke-virtual {v10, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 699
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 700
    .local v2, "builder":Ljava/lang/StringBuilder;
    const-string v7, ""

    .line 701
    .local v7, "pre":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mCurrentFileTypes:Ljava/util/EnumSet;

    sget-object v11, Lcom/sec/android/app/myfiles/utils/FileType;->IMAGE:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual {v10, v11}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 702
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeImages(Z)V

    .line 703
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v11, 0x7f0b0003

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 704
    const-string v7, ", "

    .line 706
    :cond_2
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mCurrentFileTypes:Ljava/util/EnumSet;

    sget-object v11, Lcom/sec/android/app/myfiles/utils/FileType;->VIDEO:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual {v10, v11}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 707
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeVideos(Z)V

    .line 708
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v11, 0x7f0b0004

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 709
    const-string v7, ", "

    .line 711
    :cond_3
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mCurrentFileTypes:Ljava/util/EnumSet;

    sget-object v11, Lcom/sec/android/app/myfiles/utils/FileType;->AUDIO:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual {v10, v11}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 712
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeMusic(Z)V

    .line 713
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v11, 0x7f0b0147

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 714
    const-string v7, ", "

    .line 716
    :cond_4
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mCurrentFileTypes:Ljava/util/EnumSet;

    sget-object v11, Lcom/sec/android/app/myfiles/utils/FileType;->DOCUMENT:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual {v10, v11}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 717
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeDocument(Z)V

    .line 718
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v11, 0x7f0b0006

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 719
    const-string v7, ", "

    .line 721
    :cond_5
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mCurrentFileTypes:Ljava/util/EnumSet;

    sget-object v11, Lcom/sec/android/app/myfiles/utils/FileType;->DOWNLOADEDAPPS:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual {v10, v11}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 722
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeDownloadedApps(Z)V

    .line 723
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v11, 0x7f0b0007

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 724
    const-string v7, ", "

    .line 726
    :cond_6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 730
    .end local v2    # "builder":Ljava/lang/StringBuilder;
    .end local v4    # "fileTypes":Ljava/lang/String;
    .end local v7    # "pre":Ljava/lang/String;
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    if-eqz v10, :cond_8

    .line 731
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mFileTypeSummary:Landroid/widget/TextView;

    const v11, 0x7f0b008d

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 733
    :cond_8
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mFileTypeSummary:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v11}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0b008d

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 738
    :pswitch_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 739
    .local v3, "builderLocation":Ljava/lang/StringBuilder;
    const-string v8, ""

    .line 740
    .local v8, "preLocation":Ljava/lang/String;
    const/4 v6, 0x1

    .line 741
    .local v6, "index":I
    if-eqz p3, :cond_f

    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    const-string v11, "search_set_location_is_near_by_device_intent_key"

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_f

    .line 742
    const/4 v10, 0x1

    move-object/from16 v0, p0

    iput-boolean v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isNearBydDevice:Z

    .line 743
    const-string v10, "provider_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->deviceId:Ljava/lang/String;

    .line 744
    const-string v10, "provider_name"

    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->deviceName:Ljava/lang/String;

    .line 745
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->deviceId:Ljava/lang/String;

    if-eqz v10, :cond_9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->deviceName:Ljava/lang/String;

    if-eqz v10, :cond_9

    .line 746
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mLocationSummary:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->deviceName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 747
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->nearbySearchSet()V

    .line 750
    :cond_9
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mLocationSummary:Landroid/widget/TextView;

    invoke-virtual {v10}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    const v11, 0x7f0b008c

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 751
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActionbar_ok:Landroid/widget/TextView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 827
    :cond_a
    :goto_2
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mLocationSummary:Landroid/widget/TextView;

    invoke-virtual {v10}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    const v11, 0x7f0b003e

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_d

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeAll:Z

    if-nez v10, :cond_b

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeDownloads:Z

    if-eqz v10, :cond_d

    .line 828
    :cond_b
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mFileTypeSummary:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mFileTypeSummary:Landroid/widget/TextView;

    invoke-virtual {v11}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v11

    invoke-interface {v11}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v11

    const v12, 0x7f0b0007

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v11, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 829
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iput-boolean v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeDownloads:Z

    .line 830
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeDownloadedApps(Z)V

    .line 831
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mFileTypeSummary:Landroid/widget/TextView;

    invoke-virtual {v10}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 832
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mFileTypeSummary:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mFileTypeSummary:Landroid/widget/TextView;

    invoke-virtual {v11}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v11

    invoke-interface {v11}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v11

    const-string v12, ","

    const-string v13, ""

    invoke-virtual {v11, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 834
    :cond_c
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mFileTypeSummary:Landroid/widget/TextView;

    invoke-virtual {v10}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    if-nez v10, :cond_d

    .line 835
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mFileTypeSummary:Landroid/widget/TextView;

    const v11, 0x7f0b008d

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 838
    :cond_d
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mLocationSummary:Landroid/widget/TextView;

    invoke-virtual {v10}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    const v11, 0x7f0b003e

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 839
    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeAll:Z

    if-eqz v10, :cond_0

    .line 840
    const/4 v10, 0x1

    move-object/from16 v0, p0

    iput-boolean v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeDownloads:Z

    .line 841
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeDownloadedApps(Z)V

    goto/16 :goto_0

    .line 753
    :cond_e
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActionbar_ok:Landroid/widget/TextView;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setEnabled(Z)V

    goto/16 :goto_2

    .line 757
    :cond_f
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iput-boolean v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isNearBydDevice:Z

    .line 758
    if-eqz p3, :cond_a

    .line 759
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    const-string v11, "search_set_location_intent_key"

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v10

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->selectedLocation:Ljava/util/ArrayList;

    .line 760
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationDevice()Z

    move-result v10

    if-eqz v10, :cond_10

    .line 761
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v11, 0x7f0b003e

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 762
    const-string v8, ", "

    .line 764
    :cond_10
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationSDcard()Z

    move-result v10

    if-eqz v10, :cond_11

    .line 765
    sget-boolean v10, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v10, :cond_11

    .line 766
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v11, 0x7f0b003f

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 767
    const-string v8, ", "

    .line 770
    :cond_11
    sget-boolean v10, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-eqz v10, :cond_17

    .line 771
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBa()Z

    move-result v10

    if-eqz v10, :cond_12

    .line 772
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v11, 0x7f0b0042

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 773
    const-string v8, ", "

    .line 775
    :cond_12
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBb()Z

    move-result v10

    if-eqz v10, :cond_13

    .line 776
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v11, 0x7f0b0043

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 777
    const-string v8, ", "

    .line 779
    :cond_13
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBc()Z

    move-result v10

    if-eqz v10, :cond_14

    .line 780
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v11, 0x7f0b0044

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 781
    const-string v8, ", "

    .line 783
    :cond_14
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBd()Z

    move-result v10

    if-eqz v10, :cond_15

    .line 784
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v11, 0x7f0b0045

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 785
    const-string v8, ", "

    .line 787
    :cond_15
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBe()Z

    move-result v10

    if-eqz v10, :cond_16

    .line 788
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v11, 0x7f0b0046

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 789
    const-string v8, ", "

    .line 791
    :cond_16
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBf()Z

    move-result v10

    if-eqz v10, :cond_17

    .line 792
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v11, 0x7f0b0047

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793
    const-string v8, ", "

    .line 796
    :cond_17
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationDropbox()Z

    move-result v10

    if-eqz v10, :cond_18

    .line 797
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v11, 0x7f0b0008

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 798
    const-string v8, ", "

    .line 800
    :cond_18
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationBaidu()Z

    move-result v10

    if-eqz v10, :cond_19

    .line 801
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v11, 0x7f0b00fa

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 802
    const-string v8, ", "

    .line 805
    :cond_19
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationPrivate()Z

    move-result v10

    if-eqz v10, :cond_1a

    .line 806
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v11, 0x7f0b0151

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 807
    const-string v8, ", "

    .line 809
    :cond_1a
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationNearby()Z

    move-result v10

    if-eqz v10, :cond_1b

    .line 810
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->selectedLocation:Ljava/util/ArrayList;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 811
    const/4 v10, 0x1

    move-object/from16 v0, p0

    iput-boolean v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isNearBydDevice:Z

    .line 815
    :goto_3
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mLocationSummary:Landroid/widget/TextView;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 817
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mLocationSummary:Landroid/widget/TextView;

    invoke-virtual {v10}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    const v11, 0x7f0b008c

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1c

    .line 818
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActionbar_ok:Landroid/widget/TextView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 823
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->nearbySearchSet()V

    goto/16 :goto_2

    .line 813
    :cond_1b
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iput-boolean v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isNearBydDevice:Z

    goto :goto_3

    .line 820
    :cond_1c
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActionbar_ok:Landroid/widget/TextView;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_4

    .line 846
    .end local v3    # "builderLocation":Ljava/lang/StringBuilder;
    .end local v6    # "index":I
    .end local v8    # "preLocation":Ljava/lang/String;
    :pswitch_2
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    const-string v11, "search_from_date_intent_key"

    const-wide/16 v12, 0x0

    invoke-virtual {v10, v11, v12, v13}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mCurrentFromDate:J

    .line 847
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    const-string v11, "search_to_date_intent_key"

    const-wide/16 v12, 0x0

    invoke-virtual {v10, v11, v12, v13}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mCurrentToDate:J

    .line 848
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActivity:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mCurrentFromDate:J

    const-wide/16 v14, 0x3e8

    mul-long/2addr v12, v14

    invoke-static {v10, v12, v13}, Lcom/sec/android/app/myfiles/utils/Utils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 849
    .local v5, "fromDate":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActivity:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mCurrentToDate:J

    const-wide/16 v14, 0x3e8

    mul-long/2addr v12, v14

    invoke-static {v10, v12, v13}, Lcom/sec/android/app/myfiles/utils/Utils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 850
    .local v9, "toDate":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateSummary:Landroid/widget/TextView;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v12, 0x0

    aget-object v12, v5, v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "  -  "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const/4 v12, 0x0

    aget-object v12, v9, v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 851
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateEditText:Landroid/widget/RelativeLayout;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 852
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateEditText:Landroid/widget/RelativeLayout;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 853
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateDelete:Landroid/widget/ImageView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 856
    .end local v5    # "fromDate":[Ljava/lang/String;
    .end local v9    # "toDate":[Ljava/lang/String;
    :cond_1d
    if-nez p2, :cond_0

    .line 857
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateEditText:Landroid/widget/RelativeLayout;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 858
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateEditText:Landroid/widget/RelativeLayout;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    goto/16 :goto_0

    .line 691
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 630
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->isClickable()Z

    move-result v6

    if-ne v6, v8, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isResumed()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 632
    iput-boolean v8, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isAdvanceSearchDialog:Z

    .line 634
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 636
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    .line 685
    :cond_0
    :goto_0
    return-void

    .line 639
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-class v7, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v6

    if-nez v6, :cond_0

    .line 640
    new-instance v2, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    invoke-direct {v2}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;-><init>()V

    .line 641
    .local v2, "dateFragment":Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 642
    .local v1, "dateArgs":Landroid/os/Bundle;
    const-string v6, "search_advance_intent_key"

    invoke-virtual {v1, v6, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 643
    const-string v6, "title"

    const v7, 0x7f0b008e

    invoke-virtual {v1, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 644
    const/4 v6, 0x4

    invoke-virtual {v2, p0, v6}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 645
    invoke-virtual {v2, v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->setArguments(Landroid/os/Bundle;)V

    .line 646
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-class v7, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 647
    invoke-virtual {p1, v9}, Landroid/view/View;->setClickable(Z)V

    goto :goto_0

    .line 652
    .end local v1    # "dateArgs":Landroid/os/Bundle;
    .end local v2    # "dateFragment":Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-class v7, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v6

    if-nez v6, :cond_0

    .line 653
    new-instance v4, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    invoke-direct {v4}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;-><init>()V

    .line 654
    .local v4, "fileTypeFragment":Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 655
    .local v3, "fileArgs":Landroid/os/Bundle;
    const-string v6, "search_advance_intent_key"

    invoke-virtual {v3, v6, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 656
    const-string v6, "title"

    const v7, 0x7f0b008d

    invoke-virtual {v3, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 657
    const-string v6, "search_advance_is_downloaded_app_enable"

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mLocationSummary:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f0b003e

    invoke-virtual {p0, v8}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    invoke-virtual {v3, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 658
    invoke-virtual {v4, v3}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->setArguments(Landroid/os/Bundle;)V

    .line 659
    invoke-virtual {v4, p0, v10}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 660
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-class v7, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 661
    invoke-virtual {p1, v9}, Landroid/view/View;->setClickable(Z)V

    goto/16 :goto_0

    .line 665
    .end local v3    # "fileArgs":Landroid/os/Bundle;
    .end local v4    # "fileTypeFragment":Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;
    :sswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-class v7, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v6

    if-nez v6, :cond_0

    .line 666
    new-instance v5, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    invoke-direct {v5}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;-><init>()V

    .line 667
    .local v5, "fragment":Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 668
    .local v0, "args":Landroid/os/Bundle;
    const-string v6, "search_advance_intent_key"

    invoke-virtual {v0, v6, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 669
    const-string v6, "title"

    const v7, 0x7f0b008c

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 671
    if-eqz v5, :cond_0

    .line 672
    const/4 v6, 0x2

    invoke-virtual {v5, p0, v6}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 673
    invoke-virtual {v5, v0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->setArguments(Landroid/os/Bundle;)V

    .line 674
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-class v7, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 676
    invoke-virtual {p1, v9}, Landroid/view/View;->setClickable(Z)V

    goto/16 :goto_0

    .line 636
    :sswitch_data_0
    .sparse-switch
        0x7f0f00e1 -> :sswitch_2
        0x7f0f00e5 -> :sswitch_1
        0x7f0f00ec -> :sswitch_0
    .end sparse-switch
.end method

.method protected onClickSearchCallback(IILandroid/content/Intent;)V
    .locals 5
    .param p1, "requestCode"    # I
    .param p2, "result"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 924
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 925
    .local v1, "activity":Landroid/app/Activity;
    if-eqz v1, :cond_1

    .line 928
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->imm:Landroid/view/inputmethod/InputMethodManager;

    if-nez v3, :cond_0

    .line 930
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "input_method"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodManager;

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->imm:Landroid/view/inputmethod/InputMethodManager;

    .line 933
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->imm:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 935
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActivity:Landroid/app/Activity;

    check-cast v3, Lcom/sec/android/app/myfiles/MainActivity;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/sec/android/app/myfiles/MainActivity;->mSearchFromAdvance:Z

    .line 937
    move-object v0, v1

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    move-object v2, v0

    .line 939
    .local v2, "mActivity":Lcom/sec/android/app/myfiles/MainActivity;
    invoke-virtual {v2, p1, p2, p3}, Lcom/sec/android/app/myfiles/MainActivity;->onActivityResult(IILandroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 943
    .end local v2    # "mActivity":Lcom/sec/android/app/myfiles/MainActivity;
    :cond_1
    :goto_0
    return-void

    .line 941
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 561
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 563
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090182

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v0, v1

    .line 565
    .local v0, "mItemHeight":I
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mNameImage:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setMinimumHeight(I)V

    .line 567
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mFileTypeImage:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setMinimumHeight(I)V

    .line 569
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateImage:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setMinimumHeight(I)V

    .line 571
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mLocationImage:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setMinimumHeight(I)V

    .line 573
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mExtentionImage:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setMinimumHeight(I)V

    .line 578
    .end local v0    # "mItemHeight":I
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 579
    return-void

    .line 575
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->selectmodeActionbar()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 106
    const/4 v0, 0x0

    const-string v1, "SearchAdvancedFragment"

    const-string v2, "onCreate "

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActivity:Landroid/app/Activity;

    .line 112
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->selectmodeActionbar()V

    .line 113
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 434
    const v0, 0x7f0e0003

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 435
    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 436
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 18
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 119
    const/4 v13, 0x0

    const-string v14, "SearchAdvancedFragment"

    const-string v15, "onCreateView "

    invoke-static {v13, v14, v15}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 121
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    invoke-virtual {v13}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    .line 123
    .local v5, "customTitleView":Landroid/view/LayoutInflater;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    invoke-static {v13}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v13

    if-nez v13, :cond_12

    .line 125
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    instance-of v13, v13, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v13, :cond_0

    .line 134
    :cond_0
    :goto_0
    const v13, 0x7f04003c

    const/4 v14, 0x0

    invoke-virtual {v5, v13, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v12

    .line 135
    .local v12, "v":Landroid/view/View;
    const v13, 0x7f0f00e0

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/EditText;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mNameSearchView:Landroid/widget/EditText;

    .line 136
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mNameSearchView:Landroid/widget/EditText;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActivity:Landroid/app/Activity;

    const/16 v15, 0x32

    invoke-static {v14, v15}, Lcom/sec/android/app/myfiles/utils/Utils;->getEditTextFilter(Landroid/content/Context;I)[Landroid/text/InputFilter;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 137
    const v13, 0x7f0f00f1

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/EditText;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mExtentionSearchView:Landroid/widget/EditText;

    .line 138
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mExtentionSearchView:Landroid/widget/EditText;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActivity:Landroid/app/Activity;

    const/4 v15, 0x6

    invoke-static {v14, v15}, Lcom/sec/android/app/myfiles/utils/Utils;->getEditTextMaxLengthFilter(Landroid/content/Context;I)[Landroid/text/InputFilter;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 139
    const v13, 0x7f0f00e4

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mLocationSummary:Landroid/widget/TextView;

    .line 140
    const v13, 0x7f0f00e8

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mFileTypeSummary:Landroid/widget/TextView;

    .line 141
    const v13, 0x7f0f00ed

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateSummary:Landroid/widget/TextView;

    .line 143
    const v13, 0x7f0f00df

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mNameImage:Landroid/widget/LinearLayout;

    .line 144
    const v13, 0x7f0f00e1

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mLocationImage:Landroid/widget/LinearLayout;

    .line 145
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mLocationImage:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    const v13, 0x7f0f00e5

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mFileTypeImage:Landroid/widget/LinearLayout;

    .line 147
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mFileTypeImage:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    const v13, 0x7f0f00e9

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateImage:Landroid/widget/LinearLayout;

    .line 149
    const v13, 0x7f0f00ec

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateEditText:Landroid/widget/RelativeLayout;

    .line 150
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateEditText:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateImage:Landroid/widget/LinearLayout;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 153
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateEditText:Landroid/widget/RelativeLayout;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 154
    const v13, 0x7f0f00ee

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateDelete:Landroid/widget/ImageView;

    .line 156
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateDelete:Landroid/widget/ImageView;

    new-instance v14, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$1;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;)V

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    const v13, 0x7f0f00ef

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mExtentionImage:Landroid/widget/LinearLayout;

    .line 170
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->selectedLocation:Ljava/util/ArrayList;

    .line 171
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSearchFileLocation()Ljava/util/ArrayList;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->selectedLocation:Ljava/util/ArrayList;

    .line 209
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSearchFromDate()J

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmp-long v13, v14, v16

    if-eqz v13, :cond_1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSearchToDate()J

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmp-long v13, v14, v16

    if-eqz v13, :cond_1

    .line 213
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSearchFromDate()J

    move-result-wide v14

    invoke-static {v13, v14, v15}, Lcom/sec/android/app/myfiles/utils/Utils;->getSmallFormatedDateFromLong(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 214
    .local v8, "fromDate":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSearchToDate()J

    move-result-wide v14

    invoke-static {v13, v14, v15}, Lcom/sec/android/app/myfiles/utils/Utils;->getSmallFormatedDateFromLong(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 215
    .local v11, "toDate":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateSummary:Landroid/widget/TextView;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v15, 0x0

    aget-object v15, v8, v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "  -  "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const/4 v15, 0x0

    aget-object v15, v11, v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSearchFromDate()J

    move-result-wide v14

    const-wide/16 v16, 0x3e8

    div-long v14, v14, v16

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mCurrentFromDate:J

    .line 217
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSearchToDate()J

    move-result-wide v14

    const-wide/16 v16, 0x3e8

    div-long v14, v14, v16

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mCurrentToDate:J

    .line 220
    .end local v8    # "fromDate":[Ljava/lang/String;
    .end local v11    # "toDate":[Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeImages()Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeImages:Z

    .line 221
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeAll()Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeAll:Z

    .line 222
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeVideos()Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeVideos:Z

    .line 223
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeMusic()Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeMusic:Z

    .line 224
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeDocument()Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeDocs:Z

    .line 225
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeDownloadedApps()Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeDownloads:Z

    .line 227
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationDevice()Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationDevice:Z

    .line 228
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationSDcard()Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationSDcard:Z

    .line 229
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBa()Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationUSBa:Z

    .line 230
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBb()Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationUSBb:Z

    .line 231
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBc()Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationUSBc:Z

    .line 232
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBd()Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationUSBd:Z

    .line 233
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBe()Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationUSBe:Z

    .line 234
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBf()Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationUSBf:Z

    .line 235
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationDropbox()Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationDropbox:Z

    .line 236
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationBaidu()Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationBaidu:Z

    .line 237
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationNearby()Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationNearby:Z

    .line 238
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationPrivate()Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationPrivate:Z

    .line 240
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 241
    .local v4, "builderLocation":Ljava/lang/StringBuilder;
    const-string v10, ""

    .line 242
    .local v10, "preLocation":Ljava/lang/String;
    const/4 v6, 0x0

    .line 244
    .local v6, "fileTypes":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeAll:Z

    if-nez v13, :cond_2

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeImages:Z

    if-eqz v13, :cond_13

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeVideos:Z

    if-eqz v13, :cond_13

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeMusic:Z

    if-eqz v13, :cond_13

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeDocs:Z

    if-eqz v13, :cond_13

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeDownloads:Z

    if-eqz v13, :cond_13

    .line 245
    :cond_2
    const v13, 0x7f0b0001

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 280
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationDevice:Z

    if-eqz v13, :cond_3

    .line 281
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f0b003e

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    const-string v10, ", "

    .line 284
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationSDcard:Z

    if-eqz v13, :cond_4

    .line 285
    sget-boolean v13, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v13, :cond_1a

    .line 286
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f0b003f

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 287
    const-string v10, ", "

    .line 292
    :cond_4
    :goto_2
    sget-boolean v13, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-eqz v13, :cond_1b

    .line 293
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationUSBa:Z

    if-eqz v13, :cond_5

    .line 294
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f0b0042

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    const-string v10, ", "

    .line 297
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationUSBb:Z

    if-eqz v13, :cond_6

    .line 298
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f0b0043

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    const-string v10, ", "

    .line 301
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationUSBc:Z

    if-eqz v13, :cond_7

    .line 302
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f0b0044

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    const-string v10, ", "

    .line 305
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationUSBd:Z

    if-eqz v13, :cond_8

    .line 306
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f0b0045

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 307
    const-string v10, ", "

    .line 309
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationUSBe:Z

    if-eqz v13, :cond_9

    .line 310
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f0b0046

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    const-string v10, ", "

    .line 313
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationUSBf:Z

    if-eqz v13, :cond_a

    .line 314
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f0b0047

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 315
    const-string v10, ", "

    .line 325
    :cond_a
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationDropbox:Z

    if-eqz v13, :cond_b

    .line 326
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f0b0008

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 327
    const-string v10, ", "

    .line 329
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationBaidu:Z

    if-eqz v13, :cond_c

    .line 330
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f0b00fa

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 331
    const-string v10, ", "

    .line 334
    :cond_c
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationNearby:Z

    if-eqz v13, :cond_1c

    .line 338
    const/4 v7, -0x1

    .line 339
    .local v7, "focusable":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v13

    if-eqz v13, :cond_d

    .line 340
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v13

    const-string v14, "SEARCH_EXTENSION_FOCUS"

    invoke-virtual {v13, v14}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 342
    :cond_d
    const/4 v13, 0x2

    if-eq v7, v13, :cond_e

    .line 343
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 344
    .local v2, "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const v13, 0x7f0b003e

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 345
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13, v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileLocation(Ljava/util/ArrayList;)V

    .line 346
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSearchFileLocation()Ljava/util/ArrayList;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->selectedLocation:Ljava/util/ArrayList;

    .line 347
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationNearby:Z

    .line 348
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationNearby(Z)V

    .line 393
    .end local v2    # "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7    # "focusable":I
    :cond_e
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationPrivate:Z

    if-eqz v13, :cond_f

    .line 394
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f0b0151

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 395
    const-string v10, ", "

    .line 398
    :cond_f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v13

    if-nez v13, :cond_10

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isLocationNearby:Z

    if-nez v13, :cond_10

    .line 399
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 400
    .restart local v2    # "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const v13, 0x7f0b003e

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 401
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationDevice(Z)V

    .line 402
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13, v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileLocation(Ljava/util/ArrayList;)V

    .line 403
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f0b003e

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 406
    .end local v2    # "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_10
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mFileTypeSummary:Landroid/widget/TextView;

    invoke-virtual {v13, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 408
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mLocationSummary:Landroid/widget/TextView;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 410
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mLocationSummary:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v13

    invoke-interface {v13}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mStartLocation:Ljava/lang/String;

    .line 411
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mFileTypeSummary:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v13

    invoke-interface {v13}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mStartFileType:Ljava/lang/String;

    .line 412
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateSummary:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v13

    invoke-interface {v13}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mStartDate:Ljava/lang/String;

    .line 413
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mExtentionSearchView:Landroid/widget/EditText;

    invoke-virtual {v13}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mStartExtension:Ljava/lang/String;

    .line 415
    const/4 v13, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->setHasOptionsMenu(Z)V

    .line 417
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateSummary:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v13

    invoke-interface {v13}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v13

    const-string v14, ""

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_11

    .line 418
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateDelete:Landroid/widget/ImageView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 421
    :cond_11
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mStartLocation:Ljava/lang/String;

    const v14, 0x7f0b008c

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_28

    .line 422
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActionbar_ok:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 427
    :goto_5
    return-object v12

    .line 131
    .end local v4    # "builderLocation":Ljava/lang/StringBuilder;
    .end local v6    # "fileTypes":Ljava/lang/String;
    .end local v10    # "preLocation":Ljava/lang/String;
    .end local v12    # "v":Landroid/view/View;
    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    invoke-virtual {v13}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    goto/16 :goto_0

    .line 247
    .restart local v4    # "builderLocation":Ljava/lang/StringBuilder;
    .restart local v6    # "fileTypes":Ljava/lang/String;
    .restart local v10    # "preLocation":Ljava/lang/String;
    .restart local v12    # "v":Landroid/view/View;
    :cond_13
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 248
    .local v3, "builder":Ljava/lang/StringBuilder;
    const-string v9, ""

    .line 249
    .local v9, "pre":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeImages:Z

    if-eqz v13, :cond_14

    .line 250
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f0b0003

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    const-string v9, ", "

    .line 253
    :cond_14
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeVideos:Z

    if-eqz v13, :cond_15

    .line 254
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeVideos(Z)V

    .line 255
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f0b0004

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    const-string v9, ", "

    .line 258
    :cond_15
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeMusic:Z

    if-eqz v13, :cond_16

    .line 259
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeMusic(Z)V

    .line 260
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f0b0147

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    const-string v9, ", "

    .line 263
    :cond_16
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeDocs:Z

    if-eqz v13, :cond_17

    .line 264
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeDocument(Z)V

    .line 265
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f0b0006

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    const-string v9, ", "

    .line 268
    :cond_17
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isFileTypeDownloads:Z

    if-eqz v13, :cond_18

    .line 269
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeDownloadedApps(Z)V

    .line 270
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f0b0007

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    const-string v9, ", "

    .line 273
    :cond_18
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v13

    if-eqz v13, :cond_19

    .line 274
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_1

    .line 276
    :cond_19
    const v13, 0x7f0b008d

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_1

    .line 289
    .end local v3    # "builder":Ljava/lang/StringBuilder;
    .end local v9    # "pre":Ljava/lang/String;
    :cond_1a
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationSDcard(Z)V

    goto/16 :goto_2

    .line 318
    :cond_1b
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationUSBa(Z)V

    .line 319
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationUSBb(Z)V

    .line 320
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationUSBc(Z)V

    .line 321
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationUSBd(Z)V

    .line 322
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationUSBe(Z)V

    .line 323
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationUSBf(Z)V

    goto/16 :goto_3

    .line 352
    :cond_1c
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 353
    .restart local v2    # "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationDevice()Z

    move-result v13

    if-eqz v13, :cond_1d

    .line 354
    const v13, 0x7f0b003e

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 356
    :cond_1d
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationSDcard()Z

    move-result v13

    if-eqz v13, :cond_1e

    .line 357
    sget-boolean v13, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v13, :cond_1e

    .line 358
    const v13, 0x7f0b003f

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 361
    :cond_1e
    sget-boolean v13, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-eqz v13, :cond_24

    .line 362
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBa()Z

    move-result v13

    if-eqz v13, :cond_1f

    .line 363
    const v13, 0x7f0b0042

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 365
    :cond_1f
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBb()Z

    move-result v13

    if-eqz v13, :cond_20

    .line 366
    const v13, 0x7f0b0043

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 368
    :cond_20
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBc()Z

    move-result v13

    if-eqz v13, :cond_21

    .line 369
    const v13, 0x7f0b0044

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    :cond_21
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBd()Z

    move-result v13

    if-eqz v13, :cond_22

    .line 372
    const v13, 0x7f0b0045

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 374
    :cond_22
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBe()Z

    move-result v13

    if-eqz v13, :cond_23

    .line 375
    const v13, 0x7f0b0046

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 377
    :cond_23
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBf()Z

    move-result v13

    if-eqz v13, :cond_24

    .line 378
    const v13, 0x7f0b0047

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 381
    :cond_24
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationDropbox()Z

    move-result v13

    if-eqz v13, :cond_25

    .line 382
    const v13, 0x7f0b0008

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 384
    :cond_25
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationBaidu()Z

    move-result v13

    if-eqz v13, :cond_26

    .line 385
    const v13, 0x7f0b00fa

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 387
    :cond_26
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationPrivate()Z

    move-result v13

    if-eqz v13, :cond_27

    .line 388
    const v13, 0x7f0b0151

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 390
    :cond_27
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13, v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileLocation(Ljava/util/ArrayList;)V

    .line 391
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSearchFileLocation()Ljava/util/ArrayList;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->selectedLocation:Ljava/util/ArrayList;

    goto/16 :goto_4

    .line 424
    .end local v2    # "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_28
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActionbar_ok:Landroid/widget/TextView;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setEnabled(Z)V

    goto/16 :goto_5
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 971
    const-string v0, "SearchAdvancedFragment"

    const-string v1, "onDestroyView "

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 973
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->imm:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mNameSearchView:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 974
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 975
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 947
    invoke-super {p0, p1}, Landroid/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 949
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 962
    :goto_0
    return v0

    .line 953
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->clickOK()V

    goto :goto_0

    .line 958
    :pswitch_1
    const/4 v2, 0x6

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v1, v3}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->onClickSearchCallback(IILandroid/content/Intent;)V

    goto :goto_0

    .line 949
    :pswitch_data_0
    .packed-switch 0x7f0f0133
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v4, 0x7f0f013a

    const v3, 0x7f0f0134

    const v2, 0x7f0f0133

    const/4 v1, 0x0

    .line 441
    invoke-super {p0, p1}, Landroid/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 443
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 445
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 450
    :cond_0
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 452
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 455
    :cond_1
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 457
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 460
    :cond_2
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x64

    const/4 v2, 0x0

    .line 532
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 533
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->refreshLocation()V

    .line 534
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mStartLocation:Ljava/lang/String;

    const v1, 0x7f0b008c

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActionbar_ok:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 540
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isAdvanceSearchDialog:Z

    if-nez v0, :cond_1

    .line 541
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->imm:Landroid/view/inputmethod/InputMethodManager;

    .line 542
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mIMEHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 548
    :goto_1
    const-string v0, "SearchAdvancedFragment"

    const-string v1, "onResume "

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 549
    return-void

    .line 537
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActionbar_ok:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    .line 544
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->imm:Landroid/view/inputmethod/InputMethodManager;

    .line 545
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mIMEHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1
.end method

.method public refreshLocation()V
    .locals 11

    .prologue
    const v10, 0x7f0b003e

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 978
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mLocationSummary:Landroid/widget/TextView;

    if-nez v7, :cond_1

    .line 1080
    :cond_0
    :goto_0
    return-void

    .line 981
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mLocationSummary:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 982
    .local v3, "locationString":Ljava/lang/String;
    const/4 v0, 0x0

    .line 983
    .local v0, "currentString":Ljava/lang/String;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 985
    .local v2, "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v9}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationDevice(Z)V

    .line 986
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v9}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationDropbox(Z)V

    .line 987
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v9}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationBaidu(Z)V

    .line 988
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v9}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationPrivate(Z)V

    .line 989
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v9}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationSDcard(Z)V

    .line 990
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v9}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationUSBa(Z)V

    .line 991
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v9}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationUSBb(Z)V

    .line 992
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v9}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationUSBc(Z)V

    .line 993
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v9}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationUSBd(Z)V

    .line 994
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v9}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationUSBe(Z)V

    .line 995
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v9}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationUSBf(Z)V

    .line 997
    invoke-virtual {p0, v10}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 998
    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 999
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1000
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationDevice(Z)V

    .line 1003
    :cond_2
    const v7, 0x7f0b0008

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1004
    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1005
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1006
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationDropbox(Z)V

    .line 1009
    :cond_3
    const v7, 0x7f0b00fa

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1010
    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1011
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationBaidu(Z)V

    .line 1015
    :cond_4
    const v7, 0x7f0b003f

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1016
    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_5

    sget-boolean v7, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v7, :cond_5

    .line 1017
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1018
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationSDcard(Z)V

    .line 1021
    :cond_5
    const v7, 0x7f0b0151

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1022
    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_6

    sget-boolean v7, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-eqz v7, :cond_6

    .line 1023
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1024
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationPrivate(Z)V

    .line 1027
    :cond_6
    const v7, 0x7f0b0042

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1028
    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_7

    sget-boolean v7, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-eqz v7, :cond_7

    .line 1029
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1030
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationUSBa(Z)V

    .line 1033
    :cond_7
    const v7, 0x7f0b0043

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1034
    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_8

    sget-boolean v7, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-eqz v7, :cond_8

    .line 1035
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1036
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationUSBb(Z)V

    .line 1039
    :cond_8
    const v7, 0x7f0b0044

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1040
    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_9

    sget-boolean v7, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-eqz v7, :cond_9

    .line 1041
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1042
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationUSBc(Z)V

    .line 1045
    :cond_9
    const v7, 0x7f0b0045

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1046
    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_a

    sget-boolean v7, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-eqz v7, :cond_a

    .line 1047
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1048
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationUSBd(Z)V

    .line 1051
    :cond_a
    const v7, 0x7f0b0046

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1052
    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_b

    sget-boolean v7, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-eqz v7, :cond_b

    .line 1053
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1054
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationUSBe(Z)V

    .line 1057
    :cond_b
    const v7, 0x7f0b0047

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1058
    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_c

    sget-boolean v7, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-eqz v7, :cond_c

    .line 1059
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1060
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationUSBf(Z)V

    .line 1063
    :cond_c
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-nez v7, :cond_d

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationNearby()Z

    move-result v7

    if-nez v7, :cond_d

    .line 1064
    invoke-virtual {p0, v10}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1065
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationDevice(Z)V

    .line 1068
    :cond_d
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-eqz v7, :cond_0

    .line 1069
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileLocation(Ljava/util/ArrayList;)V

    .line 1070
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->selectedLocation:Ljava/util/ArrayList;

    .line 1071
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 1072
    .local v6, "sb":Ljava/lang/StringBuilder;
    const-string v4, ""

    .line 1073
    .local v4, "pre":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1074
    .local v5, "s":Ljava/lang/String;
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1075
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1076
    const-string v4, ", "

    .line 1077
    goto :goto_1

    .line 1078
    .end local v5    # "s":Ljava/lang/String;
    :cond_e
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mLocationSummary:Landroid/widget/TextView;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method public selectmodeActionbar()V
    .locals 6

    .prologue
    const v5, 0x7f0b001d

    const v4, 0x7f0b0018

    const/4 v2, 0x0

    .line 1083
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 1084
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 1085
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 1086
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 1087
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040008

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1089
    .local v0, "customview":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 1090
    const v1, 0x7f0f0008

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActionbar_cancel:Landroid/widget/TextView;

    .line 1091
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActionbar_cancel:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0b0017

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1092
    const v1, 0x7f0f0009

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActionbar_ok:Landroid/widget/TextView;

    .line 1093
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActionbar_ok:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1094
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1095
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActionbar_cancel:Landroid/widget/TextView;

    new-instance v2, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1104
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v2, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$4;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1116
    return-void
.end method

.method public showAdvancedSearchKeyboard()V
    .locals 2

    .prologue
    .line 552
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isAdvanceSearchDialog:Z

    if-nez v0, :cond_0

    .line 553
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->imm:Landroid/view/inputmethod/InputMethodManager;

    .line 554
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mIMEHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 556
    :cond_0
    return-void
.end method
