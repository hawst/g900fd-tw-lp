.class final Lcom/sec/android/app/myfiles/utils/Utils$7;
.super Ljava/lang/Object;
.source "Utils.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/utils/Utils;->getEditTextMaxLengthFilter(Landroid/content/Context;I)[Landroid/text/InputFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$ctx:Landroid/content/Context;

.field final synthetic val$maxSize:I


# direct methods
.method constructor <init>(ILandroid/content/Context;)V
    .locals 0

    .prologue
    .line 3419
    iput p1, p0, Lcom/sec/android/app/myfiles/utils/Utils$7;->val$maxSize:I

    iput-object p2, p0, Lcom/sec/android/app/myfiles/utils/Utils$7;->val$ctx:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 9
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    const/4 v2, 0x0

    const v8, 0x7f0b00a1

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 3425
    if-nez p2, :cond_1

    if-nez p3, :cond_1

    .line 3487
    :cond_0
    :goto_0
    return-object v2

    .line 3430
    :cond_1
    iget v3, p0, Lcom/sec/android/app/myfiles/utils/Utils$7;->val$maxSize:I

    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v4

    sub-int v5, p6, p5

    sub-int/2addr v4, v5

    sub-int v1, v3, v4

    .line 3432
    .local v1, "keep":I
    if-gtz v1, :cond_5

    .line 3434
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v2

    if-nez v2, :cond_4

    .line 3436
    iget-object v2, p0, Lcom/sec/android/app/myfiles/utils/Utils$7;->val$ctx:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/utils/Utils$7;->val$ctx:Landroid/content/Context;

    new-array v4, v7, [Ljava/lang/Object;

    iget v5, p0, Lcom/sec/android/app/myfiles/utils/Utils$7;->val$maxSize:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v3, v8, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    # setter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->access$102(Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 3445
    :goto_1
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->tempEnd:I
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$200()I

    move-result v2

    if-gt v2, p3, :cond_2

    .line 3447
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 3450
    :cond_2
    # setter for: Lcom/sec/android/app/myfiles/utils/Utils;->tempEnd:I
    invoke-static {p3}, Lcom/sec/android/app/myfiles/utils/Utils;->access$202(I)I

    .line 3452
    if-ne p3, v7, :cond_3

    .line 3454
    const/4 v2, -0x1

    # setter for: Lcom/sec/android/app/myfiles/utils/Utils;->tempEnd:I
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->access$202(I)I

    .line 3457
    :cond_3
    const-string v2, ""

    goto :goto_0

    .line 3442
    :cond_4
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/utils/Utils$7;->val$ctx:Landroid/content/Context;

    new-array v4, v7, [Ljava/lang/Object;

    iget v5, p0, Lcom/sec/android/app/myfiles/utils/Utils$7;->val$maxSize:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v3, v8, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 3459
    :cond_5
    sub-int v3, p3, p2

    if-ge v1, v3, :cond_0

    .line 3463
    sub-int v3, p3, p2

    if-ge v1, v3, :cond_0

    .line 3466
    :try_start_0
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v2

    if-nez v2, :cond_6

    .line 3468
    iget-object v2, p0, Lcom/sec/android/app/myfiles/utils/Utils$7;->val$ctx:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/utils/Utils$7;->val$ctx:Landroid/content/Context;

    const v4, 0x7f0b00a1

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v7, p0, Lcom/sec/android/app/myfiles/utils/Utils$7;->val$maxSize:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    # setter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->access$102(Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 3477
    :goto_2
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 3479
    add-int v2, p2, v1

    invoke-interface {p1, p2, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    goto/16 :goto_0

    .line 3474
    :cond_6
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/utils/Utils$7;->val$ctx:Landroid/content/Context;

    const v4, 0x7f0b00a1

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v7, p0, Lcom/sec/android/app/myfiles/utils/Utils$7;->val$maxSize:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 3481
    :catch_0
    move-exception v0

    .line 3483
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string v2, ""

    goto/16 :goto_0
.end method
