.class Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$1;
.super Ljava/lang/Object;
.source "SearchsSetFileTypeFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

.field final synthetic val$adapter:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$MyListAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$MyListAdapter;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    iput-object p2, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$1;->val$adapter:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$MyListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v6, -0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 105
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->isChecked()Z

    move-result v1

    .line 106
    .local v1, "isCheked":Z
    if-nez p3, :cond_1

    .line 108
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 109
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    if-nez v1, :cond_0

    move v3, v4

    :goto_1
    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->setChecked(Z)V

    .line 108
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v3, v5

    .line 109
    goto :goto_1

    .line 113
    .end local v0    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    if-nez v1, :cond_4

    move v3, v4

    :goto_2
    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->setChecked(Z)V

    .line 115
    if-eqz v1, :cond_2

    .line 117
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v2, v5}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->setChecked(Z)V

    .line 121
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    # invokes: Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->isAllSelected()Z
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 123
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->setChecked(Z)V

    .line 130
    :goto_3
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    check-cast v2, Landroid/app/AlertDialog;

    if-eqz v2, :cond_3

    .line 131
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    # invokes: Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->isAnySelected()Z
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 138
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    iget-boolean v2, v2, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->isfromAdvance:Z

    if-nez v2, :cond_3

    .line 140
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    check-cast v2, Landroid/app/AlertDialog;

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 141
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    check-cast v2, Landroid/app/AlertDialog;

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setFocusable(Z)V

    .line 150
    :cond_3
    :goto_4
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$1;->val$adapter:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$MyListAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$MyListAdapter;->notifyDataSetChanged()V

    .line 152
    return-void

    :cond_4
    move v3, v5

    .line 113
    goto :goto_2

    .line 127
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v2, v5}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->setChecked(Z)V

    goto :goto_3

    .line 146
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    check-cast v2, Landroid/app/AlertDialog;

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 147
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    check-cast v2, Landroid/app/AlertDialog;

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setFocusable(Z)V

    goto :goto_4
.end method
