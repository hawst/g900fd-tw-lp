.class Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$1;
.super Landroid/content/BroadcastReceiver;
.source "BaiduNavigation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;-><init>(Landroid/content/Context;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x2

    .line 138
    const-string v4, "BaiduNavigation"

    const-string v5, "onReceive()"

    invoke-static {v6, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 139
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.sec.android.cloudagent.ACTION_ACCOUNT_SIGNED_IN"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 140
    const-string v4, "BaiduNavigation"

    const-string v5, "CLOUD_ACTION_SIGNED_IN"

    invoke-static {v6, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 141
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->baiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->access$000(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;)Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    sput-boolean v7, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->mSignInRequested:Z

    .line 142
    const-string v4, "result"

    invoke-virtual {p2, v4, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 143
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    # invokes: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->actionsOnSignIn()V
    invoke-static {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->access$100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;)V

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.sec.android.cloudagent.ACTION_RESPONSE_TOKEN"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 146
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 147
    .local v2, "b":Landroid/os/Bundle;
    const-string v4, "com.sec.android.cloudagent.baidu.credential.tokenkey"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 148
    .local v0, "authKey":Ljava/lang/String;
    const-string v4, "com.sec.android.cloudagent.baidu.credential.name"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 149
    .local v1, "authSecret":Ljava/lang/String;
    const-string v4, "BaiduNavigation"

    const-string v5, "CLOUD_ACTION_RESPONSE_TOKEN"

    invoke-static {v6, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 150
    if-eqz v0, :cond_2

    if-nez v1, :cond_3

    .line 151
    :cond_2
    const-string v4, "BaiduNavigation"

    const-string v5, "requestSignIn"

    invoke-static {v6, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 152
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mResigninRequested:Z
    invoke-static {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->access$200(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 153
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    const/4 v5, 0x1

    # setter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mResigninRequested:Z
    invoke-static {v4, v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->access$202(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;Z)Z

    .line 154
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->baiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->access$000(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;)Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->access$300(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->requestSignIn(Landroid/content/Context;)V

    goto :goto_0

    .line 157
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->access$400(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v3

    .line 158
    .local v3, "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    invoke-virtual {v3, v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setDropboxAuthKey(Ljava/lang/String;)V

    .line 159
    invoke-virtual {v3, v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setDropboxAuthSecret(Ljava/lang/String;)V

    .line 160
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->commit()Z

    .line 161
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->baiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->access$000(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;)Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->setAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    const-string v4, "BaiduNavigation"

    const-string v5, "cloudLoginReceiver AsyncOpen"

    invoke-static {v6, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 165
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->AsyncOpenWithFullSync()V

    goto :goto_0
.end method
