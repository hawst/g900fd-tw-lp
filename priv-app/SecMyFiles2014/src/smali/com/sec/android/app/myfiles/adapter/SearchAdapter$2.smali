.class Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;
.super Ljava/lang/Object;
.source "SearchAdapter.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

.field final synthetic val$bFolderFormat:Z

.field final synthetic val$category:I

.field final synthetic val$filePath:Ljava/lang/String;

.field final synthetic val$fileType:I

.field final synthetic val$hoverFilePath:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;IILjava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 822
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    iput p2, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->val$category:I

    iput p3, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->val$fileType:I

    iput-object p4, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->val$filePath:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->val$bFolderFormat:Z

    iput-object p6, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->val$hoverFilePath:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x2

    const/16 v1, 0x11

    const/4 v9, 0x0

    .line 827
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/sec/android/app/myfiles/utils/Utils;->isHoverSettingDisabled(Landroid/content/Context;Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->access$200(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->access$300(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->access$400(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mFolderHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->access$500(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->access$600(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 830
    const/4 v6, 0x0

    .line 831
    .local v6, "bSupportHover":Z
    const/4 v8, 0x0

    .line 833
    .local v8, "hoverMgr":Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 834
    .local v2, "hoverRect":Landroid/graphics/Rect;
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 836
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->val$category:I

    if-ne v0, v11, :cond_2

    .line 838
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->val$fileType:I

    invoke-static {v0}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 840
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->access$200(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v8

    .line 842
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->access$200(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->val$filePath:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v11, v2, v1, v3}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(ILandroid/graphics/Rect;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 844
    const/4 v6, 0x1

    .line 908
    :goto_0
    if-eqz v6, :cond_0

    .line 909
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 956
    .end local v2    # "hoverRect":Landroid/graphics/Rect;
    .end local v6    # "bSupportHover":Z
    .end local v8    # "hoverMgr":Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    :cond_0
    :goto_1
    :pswitch_0
    return v9

    .line 848
    .restart local v2    # "hoverRect":Landroid/graphics/Rect;
    .restart local v6    # "bSupportHover":Z
    .restart local v8    # "hoverMgr":Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    :cond_1
    const/4 v6, 0x0

    goto :goto_0

    .line 853
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->val$bFolderFormat:Z

    if-eqz v0, :cond_3

    .line 855
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v5

    .line 856
    .local v5, "hoverFolderNavi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->val$hoverFilePath:Ljava/lang/String;

    invoke-virtual {v5, v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInFolder(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 858
    .local v4, "hoverCursor":Landroid/database/Cursor;
    if-eqz v4, :cond_0

    .line 859
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mFolderHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->access$500(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v8

    .line 860
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mFolderHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->access$500(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->val$hoverFilePath:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(ILandroid/graphics/Rect;Ljava/lang/String;Landroid/database/Cursor;Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 865
    const/4 v6, 0x1

    .line 867
    goto :goto_0

    .end local v4    # "hoverCursor":Landroid/database/Cursor;
    .end local v5    # "hoverFolderNavi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    :cond_3
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->val$fileType:I

    invoke-static {v0}, Lcom/sec/android/app/myfiles/MediaFile;->isVideoFileType(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 869
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isOnCall(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 871
    const/4 v6, 0x0

    goto :goto_0

    .line 875
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->access$300(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v8

    .line 876
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->access$300(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->val$hoverFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(ILandroid/graphics/Rect;Ljava/lang/String;)V

    .line 877
    const/4 v6, 0x1

    goto :goto_0

    .line 880
    :cond_5
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->val$fileType:I

    invoke-static {v0}, Lcom/sec/android/app/myfiles/MediaFile;->isAudioFileType(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 881
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->access$400(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v8

    .line 882
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->access$400(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->val$hoverFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(ILandroid/graphics/Rect;Ljava/lang/String;)V

    .line 883
    const/4 v6, 0x1

    goto :goto_0

    .line 885
    :cond_6
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->val$fileType:I

    invoke-static {v0}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 886
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->access$200(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v8

    .line 887
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->access$200(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->val$hoverFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(ILandroid/graphics/Rect;Ljava/lang/String;)V

    .line 888
    const/4 v6, 0x1

    goto/16 :goto_0

    .line 890
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    iget v0, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mViewMode:I

    if-ne v0, v10, :cond_a

    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->val$fileType:I

    invoke-static {v0}, Lcom/sec/android/app/myfiles/MediaFile;->isDocumentFileType(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 892
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->val$fileType:I

    const/16 v1, 0x4c

    if-eq v0, v1, :cond_8

    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->val$fileType:I

    const/16 v1, 0x4b

    if-ne v0, v1, :cond_9

    .line 893
    :cond_8
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 895
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->access$600(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v8

    .line 896
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->access$600(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v0

    const/4 v1, 0x5

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->val$filePath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(ILandroid/graphics/Rect;Ljava/lang/String;)V

    .line 897
    const/4 v6, 0x1

    goto/16 :goto_0

    .line 902
    :cond_a
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 913
    :pswitch_1
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    if-ne v0, v10, :cond_b

    .line 915
    const/16 v0, 0xa

    const/4 v1, -0x1

    invoke-static {v0, v1}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 922
    :cond_b
    :goto_2
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;->val$bFolderFormat:Z

    if-eqz v0, :cond_c

    .line 923
    const/16 v0, 0x12c

    invoke-virtual {v8, v0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->sendDelayedMessage(I)V

    goto/16 :goto_1

    .line 918
    :catch_0
    move-exception v7

    .line 919
    .local v7, "e":Landroid/os/RemoteException;
    invoke-virtual {v7}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 925
    .end local v7    # "e":Landroid/os/RemoteException;
    :cond_c
    const/16 v0, 0x1f4

    invoke-virtual {v8, v0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->sendDelayedMessage(I)V

    goto/16 :goto_1

    .line 931
    :pswitch_2
    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->removeDelayedMessage()V

    .line 934
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    if-ne v0, v10, :cond_0

    .line 936
    const/4 v0, 0x1

    const/4 v1, -0x1

    invoke-static {v0, v1}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    .line 939
    :catch_1
    move-exception v7

    .line 941
    .restart local v7    # "e":Landroid/os/RemoteException;
    invoke-virtual {v7}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_1

    .line 909
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
