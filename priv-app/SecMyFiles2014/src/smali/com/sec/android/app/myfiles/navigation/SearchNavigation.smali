.class public Lcom/sec/android/app/myfiles/navigation/SearchNavigation;
.super Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
.source "SearchNavigation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/navigation/SearchNavigation$DropboxAsyncOpenOperationFragment;,
        Lcom/sec/android/app/myfiles/navigation/SearchNavigation$AsyncOpenRunnable;
    }
.end annotation


# static fields
.field private static final MODULE:Ljava/lang/String; = "SearchNavigation"

.field public static final mFilesApiProjection:[Ljava/lang/String;

.field public static final mFilesApiProjectionWithDownloadedAppsProjection:[Ljava/lang/String;

.field public static final mFilesContentSearchApiProjection:[Ljava/lang/String;

.field public static final mFilesContentSearchProjection:[Ljava/lang/String;

.field private static final mFilesProjection:[Ljava/lang/String;

.field public static final mFilesTitleSearchProjection:[Ljava/lang/String;


# instance fields
.field contentCursor:Landroid/database/Cursor;

.field private mAppEntriesList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mApplications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ApplicationInfo;",
            ">;"
        }
    .end annotation
.end field

.field public mAsyncDBtask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private mBaiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

.field mContentResolver:Landroid/content/ContentResolver;

.field private mCurId:J

.field private mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

.field private mEntriesMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mIsSyncing:Z

.field private mSearchType:I

.field matrixCursor:Landroid/database/MatrixCursor;

.field private mbAbort:Z

.field private mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

.field sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

.field private showDialog:Z

.field private storageType:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 82
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    const-string v1, "title"

    aput-object v1, v0, v5

    const-string v1, "format"

    aput-object v1, v0, v6

    const-string v1, "_size"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "date_modified"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "index"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mFilesApiProjectionWithDownloadedAppsProjection:[Ljava/lang/String;

    .line 92
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    const-string v1, "title"

    aput-object v1, v0, v5

    const-string v1, "format"

    aput-object v1, v0, v6

    const-string v1, "date_modified"

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mFilesProjection:[Ljava/lang/String;

    .line 99
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    const-string v1, "title"

    aput-object v1, v0, v5

    const-string v1, "format"

    aput-object v1, v0, v6

    const-string v1, "_size"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "date_modified"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mFilesApiProjection:[Ljava/lang/String;

    .line 107
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    const-string v1, "title"

    aput-object v1, v0, v5

    const-string v1, "format"

    aput-object v1, v0, v6

    const-string v1, "_size"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "date_modified"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "show_titlesearch_head"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "highlight_content"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mFilesTitleSearchProjection:[Ljava/lang/String;

    .line 118
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    const-string v1, "title"

    aput-object v1, v0, v5

    const-string v1, "format"

    aput-object v1, v0, v6

    const-string v1, "_size"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "date_modified"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "show_contentsearch_head"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "highlight_content"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mFilesContentSearchProjection:[Ljava/lang/String;

    .line 129
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    const-string v1, "title"

    aput-object v1, v0, v5

    const-string v1, "format"

    aput-object v1, v0, v6

    const-string v1, "_size"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "date_modified"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "content_search"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mFilesContentSearchApiProjection:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 139
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;-><init>(Landroid/content/Context;I)V

    .line 78
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mCurId:J

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    .line 80
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mSearchType:I

    .line 1729
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->showDialog:Z

    .line 1730
    iput-object v2, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    .line 1732
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mIsSyncing:Z

    .line 1733
    iput-object v2, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    .line 1734
    iput-object v2, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mBaiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    .line 140
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    .line 141
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContentResolver:Landroid/content/ContentResolver;

    .line 142
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentSortBy()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mCurrentSortBy:I

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentInOrder()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mCurrentInOrder:I

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mBaiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    .line 147
    return-void
.end method

.method private NewOpenAsyncTask()Landroid/os/AsyncTask;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1743
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$8;-><init>(Lcom/sec/android/app/myfiles/navigation/SearchNavigation;)V

    .line 1761
    .local v0, "task":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Ljava/lang/Void;>;"
    return-object v0
.end method

.method private checkDate(Lcom/sec/android/app/myfiles/utils/SearchParameter;Ljava/io/File;)Ljava/lang/Boolean;
    .locals 12
    .param p1, "param"    # Lcom/sec/android/app/myfiles/utils/SearchParameter;
    .param p2, "file"    # Ljava/io/File;

    .prologue
    const-wide/16 v10, 0x3e8

    const-wide/16 v8, 0x0

    const/4 v6, 0x1

    .line 978
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v4

    const/4 v5, 0x4

    if-eq v4, v5, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v4

    const/4 v5, 0x6

    if-ne v4, v5, :cond_2

    .line 979
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateFrom()J

    move-result-wide v4

    cmp-long v4, v4, v8

    if-eqz v4, :cond_2

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateTo()J

    move-result-wide v4

    cmp-long v4, v4, v8

    if-eqz v4, :cond_2

    .line 981
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateFrom()J

    move-result-wide v4

    mul-long v0, v4, v10

    .line 982
    .local v0, "dateFrom":J
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateTo()J

    move-result-wide v4

    mul-long v2, v4, v10

    .line 983
    .local v2, "dateTo":J
    invoke-virtual {p2}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    cmp-long v4, v4, v0

    if-ltz v4, :cond_1

    invoke-virtual {p2}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    cmp-long v4, v4, v2

    if-gtz v4, :cond_1

    .line 984
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 987
    .end local v0    # "dateFrom":J
    .end local v2    # "dateTo":J
    :goto_0
    return-object v4

    .line 985
    .restart local v0    # "dateFrom":J
    .restart local v2    # "dateTo":J
    :cond_1
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    goto :goto_0

    .line 987
    .end local v0    # "dateFrom":J
    .end local v2    # "dateTo":J
    :cond_2
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    goto :goto_0
.end method

.method private checkExt(Lcom/sec/android/app/myfiles/utils/SearchParameter;Ljava/io/File;)Ljava/lang/Boolean;
    .locals 6
    .param p1, "param"    # Lcom/sec/android/app/myfiles/utils/SearchParameter;
    .param p2, "file"    # Ljava/io/File;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 965
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getExtentionString()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getExtentionString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 966
    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 967
    .local v0, "filename":Ljava/lang/String;
    const-string v2, "\\."

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 968
    .local v1, "name_ext":[Ljava/lang/String;
    array-length v2, v1

    if-le v2, v4, :cond_1

    aget-object v2, v1, v5

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getKeyword()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v1, v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getExtentionString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 970
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 975
    .end local v0    # "filename":Ljava/lang/String;
    .end local v1    # "name_ext":[Ljava/lang/String;
    :goto_0
    return-object v2

    .line 972
    :cond_0
    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getKeyword()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 973
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 975
    :cond_1
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0
.end method

.method private checkLocation(Lcom/sec/android/app/myfiles/utils/SearchParameter;Ljava/io/File;)Ljava/lang/Boolean;
    .locals 4
    .param p1, "param"    # Lcom/sec/android/app/myfiles/utils/SearchParameter;
    .param p2, "file"    # Ljava/io/File;

    .prologue
    const/4 v3, 0x1

    .line 990
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getStorageType()Ljava/util/ArrayList;

    move-result-object v0

    .line 991
    .local v0, "storageType":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v1

    const/4 v2, 0x6

    if-ne v1, v2, :cond_a

    .line 992
    :cond_0
    if-eqz v0, :cond_a

    .line 993
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v2, 0x7f0b003e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 995
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 996
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1056
    :goto_0
    return-object v1

    .line 999
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v2, 0x7f0b003f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1001
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/storage/extSdCard"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1002
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    .line 1005
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0151

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1007
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1008
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    .line 1011
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0042

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1014
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/storage/UsbDriveA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1015
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    .line 1018
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0043

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1021
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/storage/UsbDriveB"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1022
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    .line 1025
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0044

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1028
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/storage/UsbDriveC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1029
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/16 :goto_0

    .line 1032
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0045

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1035
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/storage/UsbDriveD"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1036
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/16 :goto_0

    .line 1039
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0046

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1042
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/storage/UsbDriveE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1043
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/16 :goto_0

    .line 1046
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0047

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1049
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/storage/UsbDriveF"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1050
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/16 :goto_0

    .line 1053
    :cond_9
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/16 :goto_0

    .line 1056
    :cond_a
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/16 :goto_0
.end method

.method private checkMedia(Lcom/sec/android/app/myfiles/utils/SearchParameter;Ljava/io/File;)Ljava/lang/Boolean;
    .locals 7
    .param p1, "param"    # Lcom/sec/android/app/myfiles/utils/SearchParameter;
    .param p2, "file"    # Ljava/io/File;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1059
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v2

    const/4 v3, 0x6

    if-ne v2, v3, :cond_4

    .line 1060
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/EnumSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1062
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 1063
    .local v1, "path":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/myfiles/utils/FileType;->IMAGE:Lcom/sec/android/app/myfiles/utils/FileType;

    sget-object v4, Lcom/sec/android/app/myfiles/utils/FileType;->DOCUMENT:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-static {v3, v4}, Ljava/util/EnumSet;->range(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/EnumSet;->containsAll(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1065
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 1076
    .end local v1    # "path":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 1067
    .restart local v1    # "path":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/sec/android/app/myfiles/MediaFile;->getMediaFileType(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/myfiles/utils/FileType;

    move-result-object v0

    .line 1068
    .local v0, "filetype":Lcom/sec/android/app/myfiles/utils/FileType;
    if-nez v0, :cond_2

    .line 1069
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 1071
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1072
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 1074
    :cond_3
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 1076
    .end local v0    # "filetype":Lcom/sec/android/app/myfiles/utils/FileType;
    .end local v1    # "path":Ljava/lang/String;
    :cond_4
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0
.end method

.method private getDocExtentionWhereClause()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1080
    invoke-static {}, Lcom/sec/android/app/myfiles/MediaFile;->getDocumentExtensions()[Ljava/lang/String;

    move-result-object v3

    .line 1081
    .local v3, "whereArgs":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1082
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v4, "( "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1083
    const-string v2, ""

    .line 1084
    .local v2, "prefix":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_0

    .line 1085
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1086
    const-string v2, " OR "

    .line 1087
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_data LIKE \'%."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v3, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1084
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1089
    :cond_0
    const-string v4, " )"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1090
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private getEntryLocked(Landroid/content/pm/ApplicationInfo;)Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    .locals 6
    .param p1, "info"    # Landroid/content/pm/ApplicationInfo;

    .prologue
    .line 1664
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mEntriesMap:Ljava/util/HashMap;

    iget-object v2, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .line 1666
    .local v0, "entry":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    if-nez v0, :cond_1

    .line 1668
    new-instance v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .end local v0    # "entry":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    iget-wide v2, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mCurId:J

    const-wide/16 v4, 0x1

    add-long/2addr v4, v2

    iput-wide v4, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mCurId:J

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;-><init>(Landroid/content/Context;Landroid/content/pm/ApplicationInfo;J)V

    .line 1670
    .restart local v0    # "entry":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mEntriesMap:Ljava/util/HashMap;

    iget-object v2, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679
    :cond_0
    :goto_0
    return-object v0

    .line 1674
    :cond_1
    iget-object v1, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->info:Landroid/content/pm/ApplicationInfo;

    if-eq v1, p1, :cond_0

    .line 1676
    iput-object p1, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->info:Landroid/content/pm/ApplicationInfo;

    goto :goto_0
.end method

.method private getFilesInPath(Ljava/lang/String;III)Landroid/database/Cursor;
    .locals 22
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "sortBy"    # I
    .param p3, "inOrder"    # I
    .param p4, "target"    # I

    .prologue
    .line 1152
    const/4 v7, 0x0

    .line 1154
    .local v7, "where":Ljava/lang/String;
    const/16 v18, 0x0

    .line 1156
    .local v18, "whereArgs":[Ljava/lang/String;
    const/4 v4, 0x0

    move/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-static {v0, v1, v2, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesDB;->getOrderBy(IIIZ)Ljava/lang/String;

    move-result-object v15

    .line 1158
    .local v15, "orderBy":Ljava/lang/String;
    const-string v14, "format=12289"

    .line 1160
    .local v14, "onlyFolder":Ljava/lang/String;
    const-string v13, "NOT format=12289"

    .line 1162
    .local v13, "onlyFile":Ljava/lang/String;
    const-wide/16 v16, 0x0

    .line 1165
    .local v16, "pathId":J
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isAbsoluteRootFolder(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1167
    if-eqz p2, :cond_0

    if-eqz p2, :cond_a

    if-nez p4, :cond_a

    .line 1169
    :cond_0
    new-instance v12, Landroid/database/MatrixCursor;

    sget-object v4, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mFilesProjection:[Ljava/lang/String;

    invoke-direct {v12, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 1171
    .local v12, "mc":Landroid/database/MatrixCursor;
    new-instance v11, Ljava/io/File;

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getRootFolder()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v11, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1173
    .local v11, "file":Ljava/io/File;
    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "0"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {v11}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const/16 v6, 0x3001

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    invoke-virtual {v11}, Ljava/io/File;->lastModified()J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v12, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 1175
    sget-boolean v4, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v4, :cond_1

    .line 1177
    new-instance v11, Ljava/io/File;

    .end local v11    # "file":Ljava/io/File;
    sget-object v4, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardStoragePath:Ljava/lang/String;

    invoke-direct {v11, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1179
    .restart local v11    # "file":Ljava/io/File;
    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "1"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "/storage/extSdCard"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {v11}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const/16 v6, 0x3001

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    invoke-virtual {v11}, Ljava/io/File;->lastModified()J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v12, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 1182
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretMode(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1184
    new-instance v11, Ljava/io/File;

    .end local v11    # "file":Ljava/io/File;
    sget-object v4, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-direct {v11, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1186
    .restart local v11    # "file":Ljava/io/File;
    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "2"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/myfiles/utils/Utils;->getSecretBoxRootFolder(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {v11}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const/16 v6, 0x3001

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    invoke-virtual {v11}, Ljava/io/File;->lastModified()J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v12, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    :cond_2
    move-object/from16 v8, v18

    .line 1251
    .end local v11    # "file":Ljava/io/File;
    .end local v12    # "mc":Landroid/database/MatrixCursor;
    .end local v18    # "whereArgs":[Ljava/lang/String;
    .local v8, "whereArgs":[Ljava/lang/String;
    :goto_0
    return-object v12

    .line 1193
    .end local v8    # "whereArgs":[Ljava/lang/String;
    .restart local v18    # "whereArgs":[Ljava/lang/String;
    :cond_3
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isInternalRootFolder(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1195
    const-string v7, "_data LIKE ? AND parent=0"

    .line 1197
    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v4

    .line 1232
    .end local v18    # "whereArgs":[Ljava/lang/String;
    .restart local v8    # "whereArgs":[Ljava/lang/String;
    :goto_1
    packed-switch p4, :pswitch_data_0

    .line 1246
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowHiddenFileStatus()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1248
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND NOT (SUBSTR(LOWER("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_data"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "),LENGTH(RTRIM(LOWER("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_data"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "),"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\"abcdefghijklmnopqrstuvwxyz01234567890;.!@#$%^&()-_=+\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "))+1) LIKE \'.%\')"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1251
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "external"

    invoke-static {v5}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    move-object v9, v15

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    goto :goto_0

    .line 1199
    .end local v8    # "whereArgs":[Ljava/lang/String;
    .restart local v18    # "whereArgs":[Ljava/lang/String;
    :cond_5
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isExternalRootFolder(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1201
    const-string v7, "_data LIKE ? AND parent=0"

    .line 1203
    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "/storage/extSdCard%"

    aput-object v5, v8, v4

    .end local v18    # "whereArgs":[Ljava/lang/String;
    .restart local v8    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 1205
    .end local v8    # "whereArgs":[Ljava/lang/String;
    .restart local v18    # "whereArgs":[Ljava/lang/String;
    :cond_6
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1207
    const-string v7, "_data LIKE ? AND parent=0"

    .line 1209
    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->getSecretBoxRootFolder(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v4

    .end local v18    # "whereArgs":[Ljava/lang/String;
    .restart local v8    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 1213
    .end local v8    # "whereArgs":[Ljava/lang/String;
    .restart local v18    # "whereArgs":[Ljava/lang/String;
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContentResolver:Landroid/content/ContentResolver;

    const-string v5, "external"

    invoke-static {v5}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "_data LIKE ?"

    .end local v7    # "where":Ljava/lang/String;
    const/4 v9, 0x1

    new-array v8, v9, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1219
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_9

    .line 1221
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1223
    const-string v4, "_id"

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 1226
    :cond_8
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 1229
    :cond_9
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parent="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v16

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .restart local v7    # "where":Ljava/lang/String;
    move-object/from16 v8, v18

    .end local v18    # "whereArgs":[Ljava/lang/String;
    .restart local v8    # "whereArgs":[Ljava/lang/String;
    goto/16 :goto_1

    .line 1235
    .end local v10    # "cursor":Landroid/database/Cursor;
    :pswitch_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1236
    goto/16 :goto_2

    .line 1239
    :pswitch_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1240
    goto/16 :goto_2

    .end local v8    # "whereArgs":[Ljava/lang/String;
    .restart local v18    # "whereArgs":[Ljava/lang/String;
    :cond_a
    move-object/from16 v8, v18

    .end local v18    # "whereArgs":[Ljava/lang/String;
    .restart local v8    # "whereArgs":[Ljava/lang/String;
    goto/16 :goto_1

    .line 1232
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private goSearchChildNode(Ljava/io/File;Landroid/database/MatrixCursor;Lcom/sec/android/app/myfiles/utils/SearchParameter;)V
    .locals 11
    .param p1, "fSource"    # Ljava/io/File;
    .param p2, "mc"    # Landroid/database/MatrixCursor;
    .param p3, "param"    # Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 910
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 911
    const/4 v1, 0x0

    .line 912
    .local v1, "files":[Ljava/io/File;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->isEnableFiltering()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 913
    new-instance v2, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$2;-><init>(Lcom/sec/android/app/myfiles/navigation/SearchNavigation;)V

    invoke-virtual {p1, v2}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v1

    .line 932
    :goto_0
    if-eqz v1, :cond_7

    .line 933
    const/4 v0, 0x0

    .local v0, "ctr":I
    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_7

    .line 934
    aget-object v2, v1, v0

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->checkNotVewingItem(Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 933
    :cond_0
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 929
    .end local v0    # "ctr":I
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    goto :goto_0

    .line 937
    .restart local v0    # "ctr":I
    :cond_2
    aget-object v2, v1, v0

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 939
    aget-object v2, v1, v0

    invoke-direct {p0, p3, v2}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->checkMedia(Lcom/sec/android/app/myfiles/utils/SearchParameter;Ljava/io/File;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3

    aget-object v2, v1, v0

    invoke-direct {p0, p3, v2}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->checkDate(Lcom/sec/android/app/myfiles/utils/SearchParameter;Ljava/io/File;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3

    aget-object v2, v1, v0

    invoke-direct {p0, p3, v2}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->checkExt(Lcom/sec/android/app/myfiles/utils/SearchParameter;Ljava/io/File;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 941
    const/4 v2, 0x6

    new-array v3, v2, [Ljava/lang/String;

    add-int/lit8 v2, v0, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v6

    aget-object v2, v1, v0

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v7

    aget-object v2, v1, v0

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v8

    aget-object v2, v1, v0

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_4

    const/16 v2, 0x3001

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    :goto_3
    aput-object v2, v3, v9

    aget-object v2, v1, v0

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->getTotalSize(Ljava/io/File;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v10

    const/4 v2, 0x5

    aget-object v4, v1, v0

    invoke-virtual {v4}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-virtual {p2, v3}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 949
    :cond_3
    aget-object v2, v1, v0

    invoke-direct {p0, v2, p2, p3}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->goSearchChildNode(Ljava/io/File;Landroid/database/MatrixCursor;Lcom/sec/android/app/myfiles/utils/SearchParameter;)V

    goto :goto_2

    .line 941
    :cond_4
    const-string v2, "0"

    goto :goto_3

    .line 951
    :cond_5
    aget-object v2, v1, v0

    invoke-direct {p0, p3, v2}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->checkMedia(Lcom/sec/android/app/myfiles/utils/SearchParameter;Ljava/io/File;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    aget-object v2, v1, v0

    invoke-direct {p0, p3, v2}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->checkDate(Lcom/sec/android/app/myfiles/utils/SearchParameter;Ljava/io/File;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    aget-object v2, v1, v0

    invoke-direct {p0, p3, v2}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->checkExt(Lcom/sec/android/app/myfiles/utils/SearchParameter;Ljava/io/File;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 953
    const/4 v2, 0x6

    new-array v3, v2, [Ljava/lang/String;

    add-int/lit8 v2, v0, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v6

    aget-object v2, v1, v0

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v7

    aget-object v2, v1, v0

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v8

    aget-object v2, v1, v0

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_6

    const/16 v2, 0x3001

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    :goto_4
    aput-object v2, v3, v9

    aget-object v2, v1, v0

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->getTotalSize(Ljava/io/File;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v10

    const/4 v2, 0x5

    aget-object v4, v1, v0

    invoke-virtual {v4}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-virtual {p2, v3}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_6
    const-string v2, "0"

    goto :goto_4

    .line 963
    .end local v0    # "ctr":I
    .end local v1    # "files":[Ljava/io/File;
    :cond_7
    return-void
.end method

.method private loadInstalledApps()V
    .locals 12

    .prologue
    const/high16 v11, 0x40000

    const/4 v10, 0x1

    .line 1577
    iget-object v7, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 1580
    .local v6, "packageManager":Landroid/content/pm/PackageManager;
    const/16 v7, 0x2200

    :try_start_0
    invoke-virtual {v6, v7}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mApplications:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1587
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mApplications:Ljava/util/List;

    invoke-direct {v0, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1589
    .local v0, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1591
    .local v3, "filteredApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;>;"
    iget-object v7, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mEntriesMap:Ljava/util/HashMap;

    if-nez v7, :cond_3

    .line 1593
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    iput-object v7, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mEntriesMap:Ljava/util/HashMap;

    .line 1600
    :goto_1
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    if-ge v4, v7, :cond_5

    .line 1602
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ApplicationInfo;

    .line 1604
    .local v5, "info":Landroid/content/pm/ApplicationInfo;
    iget v7, v5, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit16 v7, v7, 0x80

    if-nez v7, :cond_0

    iget v7, v5, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v7, v7, 0x1

    if-nez v7, :cond_2

    iget v7, v5, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v8, 0x800000

    and-int/2addr v7, v8

    if-eqz v7, :cond_2

    .line 1607
    :cond_0
    iget v7, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mSearchType:I

    if-eq v7, v10, :cond_4

    .line 1609
    iget v7, v5, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/2addr v7, v11

    if-nez v7, :cond_1

    iget-object v7, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v9, 0x7f0b003e

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1612
    iget-object v7, v5, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    if-eqz v7, :cond_1

    .line 1614
    invoke-direct {p0, v5}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->getEntryLocked(Landroid/content/pm/ApplicationInfo;)Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    move-result-object v1

    .line 1616
    .local v1, "entry":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    iget-object v7, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v7}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->ensureLabel(Landroid/content/Context;)V

    .line 1618
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1623
    .end local v1    # "entry":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    :cond_1
    iget v7, v5, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/2addr v7, v11

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v9, 0x7f0b003f

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1627
    iget-object v7, v5, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    if-eqz v7, :cond_2

    .line 1629
    invoke-direct {p0, v5}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->getEntryLocked(Landroid/content/pm/ApplicationInfo;)Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    move-result-object v1

    .line 1631
    .restart local v1    # "entry":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    iget-object v7, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v7}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->ensureLabel(Landroid/content/Context;)V

    .line 1633
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1600
    .end local v1    # "entry":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    :cond_2
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1582
    .end local v0    # "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    .end local v3    # "filteredApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;>;"
    .end local v4    # "i":I
    .end local v5    # "info":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v2

    .line 1583
    .local v2, "ex":Ljava/lang/RuntimeException;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mApplications:Ljava/util/List;

    .line 1584
    invoke-virtual {v2}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto/16 :goto_0

    .line 1597
    .end local v2    # "ex":Ljava/lang/RuntimeException;
    .restart local v0    # "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    .restart local v3    # "filteredApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;>;"
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mEntriesMap:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->clear()V

    goto/16 :goto_1

    .line 1638
    .restart local v4    # "i":I
    .restart local v5    # "info":Landroid/content/pm/ApplicationInfo;
    :cond_4
    iget v7, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mSearchType:I

    if-ne v7, v10, :cond_2

    iget-object v7, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 1640
    iget-object v7, v5, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    if-eqz v7, :cond_2

    .line 1642
    invoke-direct {p0, v5}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->getEntryLocked(Landroid/content/pm/ApplicationInfo;)Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    move-result-object v1

    .line 1644
    .restart local v1    # "entry":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    iget-object v7, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v7}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->ensureLabel(Landroid/content/Context;)V

    .line 1646
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1654
    .end local v1    # "entry":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    .end local v5    # "info":Landroid/content/pm/ApplicationInfo;
    :cond_5
    iput-object v3, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mAppEntriesList:Ljava/util/ArrayList;

    .line 1655
    return-void
.end method


# virtual methods
.method public AsyncOpen()V
    .locals 12

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 1391
    new-instance v8, Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    invoke-direct {v8, v0}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    .line 1392
    .local v8, "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 1393
    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/db/CacheDB;->getDropboxCursor()Ljava/lang/String;

    move-result-object v9

    .line 1394
    .local v9, "delta":Ljava/lang/String;
    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 1396
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1397
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/Utils;->needUpdate:Z

    if-eqz v0, :cond_3

    .line 1398
    const-string v0, "SearchNavigation"

    const-string v1, "it needs update"

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 1400
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$4;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    new-instance v3, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$3;-><init>(Lcom/sec/android/app/myfiles/navigation/SearchNavigation;)V

    move-object v1, p0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$4;-><init>(Lcom/sec/android/app/myfiles/navigation/SearchNavigation;Lcom/sec/android/app/myfiles/fragment/AbsFragment;Ljava/lang/Runnable;ZZ)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    .line 1418
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-eqz v0, :cond_2

    .line 1419
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v11

    .line 1420
    .local v11, "fManager":Landroid/app/FragmentManager;
    if-nez v11, :cond_0

    .line 1421
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    .line 1422
    .local v6, "activity":Landroid/app/Activity;
    if-eqz v6, :cond_0

    .line 1423
    invoke-virtual {v6}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v11

    .line 1426
    .end local v6    # "activity":Landroid/app/Activity;
    :cond_0
    if-eqz v11, :cond_1

    .line 1427
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const-string v1, "OpenOperation"

    invoke-virtual {v0, v11, v1}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1428
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->showDialog:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1570
    .end local v11    # "fManager":Landroid/app/FragmentManager;
    :cond_2
    :goto_0
    return-void

    .line 1429
    .restart local v11    # "fManager":Landroid/app/FragmentManager;
    :catch_0
    move-exception v10

    .line 1430
    .local v10, "ex":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    .line 1431
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->showDialog:Z

    goto :goto_0

    .line 1436
    .end local v10    # "ex":Ljava/lang/Exception;
    .end local v11    # "fManager":Landroid/app/FragmentManager;
    :cond_3
    if-nez v9, :cond_6

    .line 1438
    const-string v0, "SearchNavigation"

    const-string v1, "delta is null 1st time"

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 1440
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$6;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    new-instance v3, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$5;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$5;-><init>(Lcom/sec/android/app/myfiles/navigation/SearchNavigation;)V

    move-object v1, p0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$6;-><init>(Lcom/sec/android/app/myfiles/navigation/SearchNavigation;Lcom/sec/android/app/myfiles/fragment/AbsFragment;Ljava/lang/Runnable;ZZ)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    .line 1457
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-eqz v0, :cond_2

    .line 1458
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v11

    .line 1459
    .restart local v11    # "fManager":Landroid/app/FragmentManager;
    if-nez v11, :cond_4

    .line 1460
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    .line 1461
    .restart local v6    # "activity":Landroid/app/Activity;
    if-eqz v6, :cond_4

    .line 1462
    invoke-virtual {v6}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v11

    .line 1465
    .end local v6    # "activity":Landroid/app/Activity;
    :cond_4
    if-eqz v11, :cond_5

    .line 1466
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const-string v1, "OpenOperation"

    invoke-virtual {v0, v11, v1}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1467
    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->showDialog:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 1468
    :catch_1
    move-exception v10

    .line 1469
    .restart local v10    # "ex":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    .line 1470
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->showDialog:Z

    goto :goto_0

    .line 1474
    .end local v10    # "ex":Ljava/lang/Exception;
    .end local v11    # "fManager":Landroid/app/FragmentManager;
    :cond_6
    const-string v0, "SearchNavigation"

    const-string v1, "2nd time"

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 1476
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    if-nez v0, :cond_7

    .line 1478
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->NewOpenAsyncTask()Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    .line 1479
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1480
    const-string v0, "SearchNavigation"

    const-string v1, "mAsyncDBtask is null"

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1483
    :cond_7
    const-string v0, "SearchNavigation"

    const-string v1, "mAsyncDBtask is not null"

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 1484
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    invoke-virtual {v0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_8

    .line 1486
    const-string v0, "SearchNavigation"

    const-string v1, "mAsyncDBtask Status.RUNNING"

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1487
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    invoke-virtual {v0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_2

    .line 1488
    const-string v0, "SearchNavigation"

    const-string v1, "mAsyncDBtask Status.FINISHED"

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 1489
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->NewOpenAsyncTask()Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    .line 1491
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 1498
    :cond_9
    if-nez v9, :cond_c

    .line 1500
    new-instance v7, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$DropboxAsyncOpenOperationFragment;

    invoke-direct {v7}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$DropboxAsyncOpenOperationFragment;-><init>()V

    .line 1502
    .local v7, "asyncUi":Lcom/sec/android/app/myfiles/navigation/SearchNavigation$DropboxAsyncOpenOperationFragment;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v7, v0}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$DropboxAsyncOpenOperationFragment;->setParentFragment(Lcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    .line 1504
    invoke-virtual {v7, v4}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$DropboxAsyncOpenOperationFragment;->setCancelButtonActive(Z)V

    .line 1506
    invoke-virtual {v7, v4}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$DropboxAsyncOpenOperationFragment;->setWaitOnCancel(Z)V

    .line 1508
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$AsyncOpenRunnable;

    invoke-direct {v0, p0, v7}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$AsyncOpenRunnable;-><init>(Lcom/sec/android/app/myfiles/navigation/SearchNavigation;Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;)V

    invoke-virtual {v7, v0}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$DropboxAsyncOpenOperationFragment;->setAsyncOpenRunnable(Lcom/sec/android/app/myfiles/navigation/SearchNavigation$AsyncOpenRunnable;)V

    .line 1510
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$7;-><init>(Lcom/sec/android/app/myfiles/navigation/SearchNavigation;)V

    invoke-virtual {v7, v0}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$DropboxAsyncOpenOperationFragment;->setCancelOperation(Ljava/lang/Runnable;)V

    .line 1521
    iput-object v7, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    .line 1523
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v0, :cond_2

    .line 1524
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v11

    .line 1525
    .restart local v11    # "fManager":Landroid/app/FragmentManager;
    if-nez v11, :cond_a

    .line 1526
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    .line 1527
    .restart local v6    # "activity":Landroid/app/Activity;
    if-eqz v6, :cond_a

    .line 1528
    invoke-virtual {v6}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v11

    .line 1531
    .end local v6    # "activity":Landroid/app/Activity;
    :cond_a
    if-eqz v11, :cond_b

    .line 1532
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const-string v1, "OpenOperation"

    invoke-virtual {v0, v11, v1}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1533
    :cond_b
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->showDialog:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 1534
    :catch_2
    move-exception v10

    .line 1535
    .restart local v10    # "ex":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    .line 1536
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->showDialog:Z

    goto/16 :goto_0

    .line 1546
    .end local v7    # "asyncUi":Lcom/sec/android/app/myfiles/navigation/SearchNavigation$DropboxAsyncOpenOperationFragment;
    .end local v10    # "ex":Ljava/lang/Exception;
    .end local v11    # "fManager":Landroid/app/FragmentManager;
    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    if-nez v0, :cond_d

    .line 1549
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->NewOpenAsyncTask()Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    .line 1550
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 1554
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    invoke-virtual {v0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_2

    .line 1557
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    invoke-virtual {v0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_2

    .line 1559
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->NewOpenAsyncTask()Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    .line 1561
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0
.end method

.method protected abortCurrentOperation()V
    .locals 1

    .prologue
    .line 1738
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mbAbort:Z

    .line 1739
    return-void
.end method

.method public getAppEntriesList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1658
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mAppEntriesList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getContentSearchCursor(Lcom/sec/android/app/myfiles/utils/SearchParameter;)Landroid/database/Cursor;
    .locals 48
    .param p1, "param"    # Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .prologue
    .line 624
    const-wide/16 v38, 0x0

    .local v38, "startTime":J
    const-wide/16 v28, 0x0

    .line 625
    .local v28, "endtime":J
    const/16 v21, 0x0

    .line 626
    .local v21, "countResults":I
    const/16 v16, 0x0

    .line 627
    .local v16, "addStoreKey":Z
    const/16 v17, 0x0

    .line 628
    .local v17, "allOpt":Z
    const/4 v15, 0x0

    .line 629
    .local v15, "addDateKey":Z
    const-string v8, "MyFilesContentSearch"

    .line 630
    .local v8, "APPNAME":Ljava/lang/String;
    const-string v13, "modifiedfromTime"

    .line 632
    .local v13, "MODIFIED_FROM_TIME":Ljava/lang/String;
    const-string v14, "modifiedtoTime"

    .line 634
    .local v14, "MODIFIED_TO_TIME":Ljava/lang/String;
    const-string v10, "fileExtension"

    .line 636
    .local v10, "FILE_EXTENSION":Ljava/lang/String;
    const-string v11, "parent"

    .line 637
    .local v11, "KEY_PARENT":Ljava/lang/String;
    const-string v12, "storage"

    .line 639
    .local v12, "KEY_STORAGE":Ljava/lang/String;
    const-string v9, "."

    .line 640
    .local v9, "DOT":Ljava/lang/String;
    new-instance v36, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyyMMdd"

    move-object/from16 v0, v36

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 642
    .local v36, "sdf":Ljava/text/SimpleDateFormat;
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getKeyword()Ljava/lang/String;

    move-result-object v5

    .line 643
    .local v5, "searchQuery":Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v38

    .line 644
    const-string v2, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v43, "search Start Time : "

    move-object/from16 v0, v43

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-wide/from16 v0, v38

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 648
    const/16 v33, 0x0

    .line 649
    .local v33, "keyvalues":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v33, Ljava/util/HashMap;

    .end local v33    # "keyvalues":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v2, 0x1

    move-object/from16 v0, v33

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 651
    .restart local v33    # "keyvalues":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 702
    :goto_0
    if-eqz v16, :cond_1

    .line 703
    new-instance v34, Ljava/io/File;

    const-string v2, "/storage/extSdCard/"

    move-object/from16 v0, v34

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 704
    .local v34, "sd":Ljava/io/File;
    const/16 v35, 0x0

    .line 713
    .local v35, "sdFileCnt":I
    if-eqz v34, :cond_0

    invoke-virtual/range {v34 .. v34}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 714
    invoke-virtual/range {v34 .. v34}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v0, v2

    move/from16 v35, v0

    .line 717
    :cond_0
    if-eqz v17, :cond_b

    .line 720
    if-lez v35, :cond_8

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-eqz v2, :cond_8

    .line 721
    const-string v2, "parent"

    const-string v7, "/storage/"

    move-object/from16 v0, v33

    invoke-virtual {v0, v2, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 778
    .end local v34    # "sd":Ljava/io/File;
    .end local v35    # "sdFileCnt":I
    :cond_1
    :goto_1
    if-eqz v15, :cond_2

    .line 779
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateFrom()J

    move-result-wide v44

    const-wide/16 v46, 0x3e8

    mul-long v24, v44, v46

    .line 780
    .local v24, "datefrom":J
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateTo()J

    move-result-wide v44

    const-wide/16 v46, 0x3e8

    mul-long v22, v44, v46

    .line 781
    .local v22, "dateTo":J
    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v36

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v40

    .line 782
    .local v40, "strDateFrom":Ljava/lang/String;
    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v36

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v41

    .line 784
    .local v41, "strDateTo":Ljava/lang/String;
    const-string v2, "modifiedfromTime"

    move-object/from16 v0, v33

    move-object/from16 v1, v40

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 785
    const-string v2, "modifiedtoTime"

    move-object/from16 v0, v33

    move-object/from16 v1, v41

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 788
    .end local v22    # "dateTo":J
    .end local v24    # "datefrom":J
    .end local v40    # "strDateFrom":Ljava/lang/String;
    .end local v41    # "strDateTo":Ljava/lang/String;
    :cond_2
    const/4 v2, 0x5

    new-array v4, v2, [Ljava/lang/String;

    .line 789
    .local v4, "keys":[Ljava/lang/String;
    const/4 v2, 0x5

    new-array v6, v2, [Ljava/lang/String;

    .line 791
    .local v6, "values":[Ljava/lang/String;
    const/16 v32, 0x0

    .line 792
    .local v32, "keycount":I
    invoke-virtual/range {v33 .. v33}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v31

    .local v31, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/util/Map$Entry;

    .line 793
    .local v27, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface/range {v27 .. v27}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    aput-object v2, v4, v32

    .line 794
    invoke-interface/range {v27 .. v27}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    aput-object v2, v6, v32

    .line 795
    add-int/lit8 v32, v32, 0x1

    .line 796
    goto :goto_2

    .line 653
    .end local v4    # "keys":[Ljava/lang/String;
    .end local v6    # "values":[Ljava/lang/String;
    .end local v27    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v31    # "i$":Ljava/util/Iterator;
    .end local v32    # "keycount":I
    :pswitch_0
    const/16 v16, 0x1

    .line 654
    const/16 v17, 0x1

    .line 655
    goto/16 :goto_0

    .line 657
    :pswitch_1
    const/16 v16, 0x1

    .line 658
    goto/16 :goto_0

    .line 660
    :pswitch_2
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    sget-object v7, Lcom/sec/android/app/myfiles/utils/FileType;->DOCUMENT:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual {v2, v7}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 663
    const/4 v2, 0x0

    .line 842
    :goto_3
    return-object v2

    .line 665
    :cond_3
    const/16 v16, 0x1

    .line 666
    goto/16 :goto_0

    .line 668
    :pswitch_3
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateFrom()J

    move-result-wide v44

    const-wide/16 v46, 0x0

    cmp-long v2, v44, v46

    if-eqz v2, :cond_4

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateTo()J

    move-result-wide v44

    const-wide/16 v46, 0x0

    cmp-long v2, v44, v46

    if-eqz v2, :cond_4

    .line 669
    const/16 v16, 0x1

    .line 670
    const/4 v15, 0x1

    goto/16 :goto_0

    .line 672
    :cond_4
    const/4 v2, 0x0

    goto :goto_3

    .line 678
    :pswitch_4
    const/4 v2, 0x0

    goto :goto_3

    .line 681
    :pswitch_5
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    sget-object v7, Lcom/sec/android/app/myfiles/utils/FileType;->DOCUMENT:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual {v2, v7}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 684
    const/4 v2, 0x0

    goto :goto_3

    .line 687
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateFrom()J

    move-result-wide v44

    const-wide/16 v46, 0x0

    cmp-long v2, v44, v46

    if-eqz v2, :cond_6

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateTo()J

    move-result-wide v44

    const-wide/16 v46, 0x0

    cmp-long v2, v44, v46

    if-eqz v2, :cond_6

    .line 688
    const/4 v15, 0x1

    .line 690
    :cond_6
    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    .line 691
    .local v30, "extString":Ljava/lang/StringBuilder;
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getExtentionString()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getExtentionString()Ljava/lang/String;

    move-result-object v2

    const-string v7, ""

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 693
    const-string v2, "."

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getExtentionString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 694
    const-string v2, "fileExtension"

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v33

    invoke-virtual {v0, v2, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 697
    :cond_7
    const/16 v16, 0x1

    goto/16 :goto_0

    .line 722
    .end local v30    # "extString":Ljava/lang/StringBuilder;
    .restart local v34    # "sd":Ljava/io/File;
    .restart local v35    # "sdFileCnt":I
    :cond_8
    if-lez v35, :cond_9

    .line 723
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants;->ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ":"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "/storage/extSdCard/"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    .line 725
    .local v42, "value":Ljava/lang/String;
    const-string v2, "parent"

    move-object/from16 v0, v33

    move-object/from16 v1, v42

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 726
    .end local v42    # "value":Ljava/lang/String;
    :cond_9
    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-eqz v2, :cond_a

    sget-object v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    if-eqz v2, :cond_a

    .line 728
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants;->ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ":"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v7, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    .line 730
    .restart local v42    # "value":Ljava/lang/String;
    const-string v2, "parent"

    move-object/from16 v0, v33

    move-object/from16 v1, v42

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 732
    .end local v42    # "value":Ljava/lang/String;
    :cond_a
    const-string v2, "parent"

    sget-object v7, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants;->ROOT_PATH:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-virtual {v0, v2, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 741
    :cond_b
    new-instance v37, Ljava/util/ArrayList;

    invoke-direct/range {v37 .. v37}, Ljava/util/ArrayList;-><init>()V

    .line 742
    .local v37, "storageType":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getStorageType()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_c

    .line 743
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getStorageType()Ljava/util/ArrayList;

    move-result-object v37

    .line 744
    :cond_c
    const-string v42, ""

    .line 746
    .restart local v42    # "value":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v2

    const/4 v7, 0x2

    if-eq v2, v7, :cond_d

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v2

    const/4 v7, 0x6

    if-ne v2, v7, :cond_1

    .line 748
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v7, 0x7f0b003e

    invoke-virtual {v2, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v37

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 750
    sget-object v42, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants;->ROOT_PATH:Ljava/lang/String;

    .line 752
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v7, 0x7f0b003f

    invoke-virtual {v2, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v37

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 754
    if-lez v35, :cond_f

    .line 755
    const-string v2, ""

    move-object/from16 v0, v42

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    const-string v42, "/storage/extSdCard/"

    .line 761
    :cond_f
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v7, 0x7f0b0151

    invoke-virtual {v2, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v37

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 763
    sget-object v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    if-eqz v2, :cond_10

    .line 764
    const-string v2, ""

    move-object/from16 v0, v42

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    sget-object v42, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    .line 768
    :cond_10
    :goto_5
    const-string v2, ""

    move-object/from16 v0, v42

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 770
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 755
    :cond_11
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v42

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ":"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "/storage/extSdCard/"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    goto :goto_4

    .line 764
    :cond_12
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v42

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ":"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v7, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    goto :goto_5

    .line 772
    :cond_13
    const-string v2, "parent"

    move-object/from16 v0, v33

    move-object/from16 v1, v42

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 798
    .end local v34    # "sd":Ljava/io/File;
    .end local v35    # "sdFileCnt":I
    .end local v37    # "storageType":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v42    # "value":Ljava/lang/String;
    .restart local v4    # "keys":[Ljava/lang/String;
    .restart local v6    # "values":[Ljava/lang/String;
    .restart local v31    # "i$":Ljava/util/Iterator;
    .restart local v32    # "keycount":I
    :cond_14
    :try_start_0
    const-string v2, "ContentSearch"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v43, "about to search with service for "

    move-object/from16 v0, v43

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 800
    const-string v2, "content://com.samsung.index.searchprovider/search"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 801
    .local v3, "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->contentCursor:Landroid/database/Cursor;

    .line 803
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->contentCursor:Landroid/database/Cursor;

    if-nez v2, :cond_15

    .line 804
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 805
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->contentCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_16

    .line 806
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->contentCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 807
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 809
    .end local v3    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v26

    .line 810
    .local v26, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 813
    .end local v26    # "e":Ljava/lang/Exception;
    :cond_16
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    .line 814
    const-string v2, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v43, "search Index End Time : "

    move-object/from16 v0, v43

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-wide/from16 v0, v28

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 815
    const-string v2, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v43, "search Index timetaken : "

    move-object/from16 v0, v43

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sub-long v44, v28, v38

    const-wide/16 v46, 0x3e8

    div-long v44, v44, v46

    move-wide/from16 v0, v44

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 818
    new-instance v20, Landroid/database/MatrixCursor;

    sget-object v2, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mFilesContentSearchProjection:[Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-direct {v0, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 819
    .local v20, "contentSearchHeading":Landroid/database/MatrixCursor;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->contentCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_17

    .line 822
    const/16 v2, 0x8

    new-array v0, v2, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/4 v2, 0x0

    const-string v7, ""

    invoke-virtual {v7}, Ljava/lang/String;->hashCode()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v19, v2

    const/4 v2, 0x1

    const-string v7, ""

    aput-object v7, v19, v2

    const/4 v2, 0x2

    const-string v7, ""

    aput-object v7, v19, v2

    const/4 v2, 0x3

    const-string v7, "0"

    aput-object v7, v19, v2

    const/4 v2, 0x4

    const-string v7, ""

    aput-object v7, v19, v2

    const/4 v2, 0x5

    const-string v7, ""

    aput-object v7, v19, v2

    const/4 v2, 0x6

    const-string v7, "true"

    aput-object v7, v19, v2

    const/4 v2, 0x7

    const-string v7, ""

    aput-object v7, v19, v2

    .line 824
    .local v19, "column":[Ljava/lang/Object;
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 832
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->contentCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_19

    .line 833
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentView()I

    move-result v2

    const/4 v7, 0x2

    if-ne v2, v7, :cond_18

    .line 834
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->contentCursor:Landroid/database/Cursor;

    goto/16 :goto_3

    .line 811
    .end local v19    # "column":[Ljava/lang/Object;
    .end local v20    # "contentSearchHeading":Landroid/database/MatrixCursor;
    :catchall_0
    move-exception v2

    throw v2

    .line 827
    .restart local v20    # "contentSearchHeading":Landroid/database/MatrixCursor;
    :cond_17
    const/16 v2, 0x8

    new-array v0, v2, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/4 v2, 0x0

    const-string v7, ""

    invoke-virtual {v7}, Ljava/lang/String;->hashCode()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v19, v2

    const/4 v2, 0x1

    const-string v7, ""

    aput-object v7, v19, v2

    const/4 v2, 0x2

    const-string v7, ""

    aput-object v7, v19, v2

    const/4 v2, 0x3

    const-string v7, "0"

    aput-object v7, v19, v2

    const/4 v2, 0x4

    const-string v7, ""

    aput-object v7, v19, v2

    const/4 v2, 0x5

    const-string v7, ""

    aput-object v7, v19, v2

    const/4 v2, 0x6

    const-string v7, "false"

    aput-object v7, v19, v2

    const/4 v2, 0x7

    const-string v7, ""

    aput-object v7, v19, v2

    .line 829
    .restart local v19    # "column":[Ljava/lang/Object;
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_6

    .line 836
    :cond_18
    const/4 v2, 0x2

    new-array v0, v2, [Landroid/database/Cursor;

    move-object/from16 v18, v0

    .line 837
    .local v18, "arrCursor":[Landroid/database/Cursor;
    const/4 v2, 0x0

    aput-object v20, v18, v2

    .line 838
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->contentCursor:Landroid/database/Cursor;

    aput-object v7, v18, v2

    .line 839
    new-instance v2, Landroid/database/MergeCursor;

    move-object/from16 v0, v18

    invoke-direct {v2, v0}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    goto/16 :goto_3

    .line 842
    .end local v18    # "arrCursor":[Landroid/database/Cursor;
    :cond_19
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->contentCursor:Landroid/database/Cursor;

    goto/16 :goto_3

    .line 651
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public getEmptyCourso()Landroid/database/Cursor;
    .locals 4

    .prologue
    .line 152
    new-instance v0, Landroid/database/MatrixCursor;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 154
    .local v0, "cursor":Landroid/database/MatrixCursor;
    return-object v0
.end method

.method public getFilesInCurrentFolder()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 1288
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFilesInFolder(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 1097
    iget v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mShowFolderFileType:I

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->getFilesInFolder(Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getFilesInFolder(Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 8
    .param p1, "folderPath"    # Ljava/lang/String;
    .param p2, "showFolderFileType"    # I

    .prologue
    const/4 v7, 0x1

    .line 1111
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1113
    .local v1, "cursors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/database/Cursor;>;"
    iget v4, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mCurrentSortBy:I

    packed-switch v4, :pswitch_data_0

    .line 1135
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v0, v4, [Landroid/database/Cursor;

    .line 1139
    .local v0, "cursorArray":[Landroid/database/Cursor;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_3

    .line 1141
    iget v4, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mCurrentInOrder:I

    if-nez v4, :cond_2

    move v3, v2

    .line 1143
    .local v3, "index":I
    :goto_2
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/database/Cursor;

    aput-object v4, v0, v2

    .line 1139
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1116
    .end local v0    # "cursorArray":[Landroid/database/Cursor;
    .end local v2    # "i":I
    .end local v3    # "index":I
    :pswitch_0
    iget v4, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mCurrentSortBy:I

    iget v5, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mCurrentInOrder:I

    invoke-direct {p0, p1, v4, v5, p2}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->getFilesInPath(Ljava/lang/String;III)Landroid/database/Cursor;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1123
    :pswitch_1
    if-eq p2, v7, :cond_1

    .line 1125
    iget v4, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mCurrentSortBy:I

    iget v5, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mCurrentInOrder:I

    const/4 v6, 0x0

    invoke-direct {p0, p1, v4, v5, v6}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->getFilesInPath(Ljava/lang/String;III)Landroid/database/Cursor;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1128
    :cond_1
    if-eqz p2, :cond_0

    .line 1130
    iget v4, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mCurrentSortBy:I

    iget v5, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mCurrentInOrder:I

    invoke-direct {p0, p1, v4, v5, v7}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->getFilesInPath(Ljava/lang/String;III)Landroid/database/Cursor;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1141
    .restart local v0    # "cursorArray":[Landroid/database/Cursor;
    .restart local v2    # "i":I
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    sub-int/2addr v4, v2

    add-int/lit8 v3, v4, -0x1

    goto :goto_2

    .line 1146
    :cond_3
    new-instance v4, Landroid/database/MergeCursor;

    invoke-direct {v4, v0}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    return-object v4

    .line 1113
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public getSearchCursor(Lcom/sec/android/app/myfiles/utils/SearchParameter;)Landroid/database/Cursor;
    .locals 42
    .param p1, "param"    # Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .prologue
    .line 171
    const/4 v14, 0x0

    .line 173
    .local v14, "cursor":Landroid/database/Cursor;
    const/16 v24, 0x0

    .local v24, "onlyDownloadedApps":Z
    const/16 v26, 0x0

    .line 174
    .local v26, "onlyUSBstorage":Z
    const/16 v25, 0x0

    .line 175
    .local v25, "onlyDropbox":Z
    const/16 v27, 0x0

    .line 176
    .local v27, "onlyUsbDropbox":Z
    const/16 v29, 0x0

    .line 177
    .local v29, "removeUSBa":Z
    const/16 v30, 0x0

    .line 178
    .local v30, "removeUSBb":Z
    const/16 v31, 0x0

    .line 179
    .local v31, "removeUSBc":Z
    const/16 v32, 0x0

    .line 180
    .local v32, "removeUSBd":Z
    const/16 v33, 0x0

    .line 181
    .local v33, "removeUSBe":Z
    const/16 v34, 0x0

    .line 183
    .local v34, "removeUSBf":Z
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getStorageType()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 184
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getStorageType()Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    .line 187
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mSearchType:I

    .line 189
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getKeyword()Ljava/lang/String;

    move-result-object v35

    .line 190
    .local v35, "searchKeyword":Ljava/lang/String;
    const-string v2, "%"

    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 191
    const-string v2, "%"

    const-string v3, "\\%"

    move-object/from16 v0, v35

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v35

    .line 193
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getKeyword()Ljava/lang/String;

    move-result-object v15

    .line 194
    .local v15, "extention":Ljava/lang/String;
    const-string v2, "%"

    invoke-virtual {v15, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 195
    const-string v2, "%"

    const-string v3, "\\%"

    invoke-virtual {v15, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v15

    .line 197
    :cond_2
    new-instance v38, Ljava/util/ArrayList;

    invoke-direct/range {v38 .. v38}, Ljava/util/ArrayList;-><init>()V

    .line 198
    .local v38, "whereList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v18

    .line 222
    .local v18, "isUsbLocation":Ljava/lang/Boolean;
    const-string v39, ""

    .line 223
    .local v39, "whereStorage":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    if-eqz v2, :cond_13

    .line 224
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0042

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0043

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0044

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0045

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0046

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0047

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 231
    :cond_3
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v18

    .line 239
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    if-nez v2, :cond_15

    .line 240
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mSearchType:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_5

    .line 241
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b003e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 293
    :cond_5
    :goto_0
    const/16 v20, 0x0

    .line 294
    .local v20, "localStorageCount":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b003e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 295
    add-int/lit8 v20, v20, 0x1

    .line 297
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b003f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 298
    add-int/lit8 v20, v20, 0x1

    .line 300
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0151

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 301
    add-int/lit8 v20, v20, 0x1

    .line 303
    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_9

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v2

    const/4 v3, 0x6

    if-ne v2, v3, :cond_d

    .line 304
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b003e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 306
    const/4 v2, 0x1

    move/from16 v0, v20

    if-ne v0, v2, :cond_22

    .line 307
    const-string v39, "storage_id = 65537"

    .line 312
    :cond_a
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b003f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 314
    const-string v2, ""

    move-object/from16 v0, v39

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_24

    .line 315
    const/4 v2, 0x1

    move/from16 v0, v20

    if-ne v0, v2, :cond_23

    .line 316
    const-string v39, "storage_id = 131073"

    .line 328
    :cond_b
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0151

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 330
    const-string v2, ""

    move-object/from16 v0, v39

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_26

    .line 331
    const-string v39, "storage_id = 196609"

    .line 339
    :cond_c
    :goto_3
    const-string v2, ""

    move-object/from16 v0, v39

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 340
    invoke-virtual/range {v38 .. v39}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 343
    :cond_d
    if-nez v26, :cond_35

    if-nez v25, :cond_35

    if-nez v27, :cond_35

    .line 344
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_e

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v2

    const/4 v3, 0x6

    if-ne v2, v3, :cond_f

    .line 345
    :cond_e
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    if-eqz v2, :cond_2e

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/EnumSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2e

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/EnumSet;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/myfiles/utils/FileType;->DOWNLOADEDAPPS:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2e

    .line 350
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/myfiles/utils/FileType;->IMAGE:Lcom/sec/android/app/myfiles/utils/FileType;

    sget-object v4, Lcom/sec/android/app/myfiles/utils/FileType;->DOCUMENT:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-static {v3, v4}, Ljava/util/EnumSet;->range(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/EnumSet;->containsAll(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_28

    .line 352
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "( media_type =\'1\' OR media_type =\'2\' OR media_type =\'3\' OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->getDocExtentionWhereClause()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " )"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v38

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 386
    :cond_f
    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_10

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v2

    const/4 v3, 0x6

    if-ne v2, v3, :cond_11

    .line 387
    :cond_10
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateFrom()J

    move-result-wide v2

    const-wide/16 v40, 0x0

    cmp-long v2, v2, v40

    if-eqz v2, :cond_11

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateTo()J

    move-result-wide v2

    const-wide/16 v40, 0x0

    cmp-long v2, v2, v40

    if-eqz v2, :cond_11

    .line 390
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "( date_modified BETWEEN "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateFrom()J

    move-result-wide v40

    move-wide/from16 v0, v40

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateTo()J

    move-result-wide v40

    move-wide/from16 v0, v40

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " )"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v38

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 393
    :cond_11
    const-string v5, "(_data LIKE ? OR title LIKE ? )"

    .line 395
    .local v5, "where":Ljava/lang/String;
    const/4 v6, 0x0

    .line 396
    .local v6, "selectionArgs":[Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v2

    const/4 v3, 0x6

    if-ne v2, v3, :cond_2f

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getExtentionString()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2f

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getExtentionString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2f

    .line 397
    const-string v2, "( format != 12289 )"

    move-object/from16 v0, v38

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 398
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getExtentionString()Ljava/lang/String;

    move-result-object v15

    .line 399
    const-string v2, "%"

    invoke-virtual {v15, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 400
    const-string v2, "%"

    const-string v3, "\\%"

    invoke-virtual {v15, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v15

    .line 403
    :cond_12
    const-string v5, "(( SUBSTR(LOWER(_data),LENGTH( RTRIM(LOWER(_data),\"abcdefghijklmnopqrstuvwxyz01234567890;!@#$%^&()-_=+\") )) like ? escape \'\\\') AND (title LIKE ? escape \'\\\'OR title LIKE ? escape \'\\\'))"

    .line 405
    const/4 v2, 0x3

    new-array v6, v2, [Ljava/lang/String;

    .end local v6    # "selectionArgs":[Ljava/lang/String;
    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    const/4 v2, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    .line 420
    .restart local v6    # "selectionArgs":[Ljava/lang/String;
    :goto_5
    invoke-virtual/range {v38 .. v38}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_32

    .line 422
    const-string v28, ""

    .line 423
    .local v28, "prefix":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 425
    .local v9, "builder":Ljava/lang/StringBuilder;
    const-string v2, " ( "

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 426
    invoke-virtual/range {v38 .. v38}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_31

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    .line 428
    .local v19, "list":Ljava/lang/String;
    move-object/from16 v0, v28

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 429
    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 430
    const-string v28, " AND "

    .line 431
    goto :goto_6

    .line 236
    .end local v5    # "where":Ljava/lang/String;
    .end local v6    # "selectionArgs":[Ljava/lang/String;
    .end local v9    # "builder":Ljava/lang/StringBuilder;
    .end local v17    # "i$":Ljava/util/Iterator;
    .end local v19    # "list":Ljava/lang/String;
    .end local v20    # "localStorageCount":I
    .end local v28    # "prefix":Ljava/lang/String;
    :cond_13
    const/16 v22, 0x0

    .line 618
    :cond_14
    :goto_7
    return-object v22

    .line 244
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_19

    .line 245
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0042

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0043

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0044

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0045

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0046

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0047

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 252
    :cond_16
    const/16 v26, 0x1

    goto/16 :goto_0

    .line 253
    :cond_17
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0008

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b00fa

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 255
    :cond_18
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 259
    :cond_19
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1c

    .line 260
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0042

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0043

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0044

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0045

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0046

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0047

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1c

    :cond_1a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0008

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b00fa

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 268
    :cond_1b
    const/16 v27, 0x1

    .line 272
    :cond_1c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0042

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 273
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0042

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 274
    const/16 v29, 0x1

    goto/16 :goto_0

    .line 275
    :cond_1d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0043

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 276
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0043

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 277
    const/16 v30, 0x1

    goto/16 :goto_0

    .line 278
    :cond_1e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0044

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 279
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0044

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 280
    const/16 v31, 0x1

    goto/16 :goto_0

    .line 281
    :cond_1f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0045

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_20

    .line 282
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0045

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 283
    const/16 v32, 0x1

    goto/16 :goto_0

    .line 284
    :cond_20
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0046

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_21

    .line 285
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0046

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 286
    const/16 v33, 0x1

    goto/16 :goto_0

    .line 287
    :cond_21
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0047

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 288
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0047

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 289
    const/16 v34, 0x1

    goto/16 :goto_0

    .line 309
    .restart local v20    # "localStorageCount":I
    :cond_22
    const-string v39, " ( storage_id = 65537"

    goto/16 :goto_1

    .line 318
    :cond_23
    const-string v39, " ( storage_id = 131073"

    goto/16 :goto_2

    .line 321
    :cond_24
    const/4 v2, 0x2

    move/from16 v0, v20

    if-ne v0, v2, :cond_25

    .line 322
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v39

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "storage_id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x20001

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ) "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    goto/16 :goto_2

    .line 324
    :cond_25
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v39

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "storage_id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x20001

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    goto/16 :goto_2

    .line 332
    :cond_26
    const/4 v2, 0x1

    move/from16 v0, v20

    if-le v0, v2, :cond_27

    .line 333
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v39

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "storage_id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x30001

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ) "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    goto/16 :goto_3

    .line 335
    :cond_27
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v39

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "storage_id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x30001

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    goto/16 :goto_3

    .line 355
    :cond_28
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 356
    .local v16, "fileType":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/myfiles/utils/FileType;->IMAGE:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual {v2, v3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_29

    .line 358
    const-string v2, " media_type =\'1\'"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 360
    :cond_29
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/myfiles/utils/FileType;->AUDIO:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual {v2, v3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 362
    const-string v2, " media_type =\'2\'"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 364
    :cond_2a
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/myfiles/utils/FileType;->VIDEO:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual {v2, v3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 366
    const-string v2, " media_type =\'3\'"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 368
    :cond_2b
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/myfiles/utils/FileType;->DOCUMENT:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual {v2, v3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2c

    .line 369
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->getDocExtentionWhereClause()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    :cond_2c
    const-string v28, " ( "

    .line 372
    .restart local v28    # "prefix":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 373
    .restart local v9    # "builder":Ljava/lang/StringBuilder;
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .restart local v17    # "i$":Ljava/util/Iterator;
    :goto_8
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2d

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    .line 375
    .restart local v19    # "list":Ljava/lang/String;
    move-object/from16 v0, v28

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 376
    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 377
    const-string v28, " OR "

    .line 378
    goto :goto_8

    .line 379
    .end local v19    # "list":Ljava/lang/String;
    :cond_2d
    const-string v2, " ) "

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 380
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v38

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 382
    .end local v9    # "builder":Ljava/lang/StringBuilder;
    .end local v16    # "fileType":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v17    # "i$":Ljava/util/Iterator;
    .end local v28    # "prefix":Ljava/lang/String;
    :cond_2e
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    if-eqz v2, :cond_f

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/EnumSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_f

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/EnumSet;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/myfiles/utils/FileType;->DOWNLOADEDAPPS:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 384
    const/16 v24, 0x1

    goto/16 :goto_4

    .line 408
    .restart local v5    # "where":Ljava/lang/String;
    .restart local v6    # "selectionArgs":[Ljava/lang/String;
    :cond_2f
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_30

    .line 410
    const-string v2, "( format != 12289 )"

    move-object/from16 v0, v38

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 411
    const-string v5, "( SUBSTR(LOWER(_data),LENGTH( RTRIM(LOWER(_data),\"abcdefghijklmnopqrstuvwxyz01234567890;!@#$%^&()-_=+\") )) like ? )"

    .line 412
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    .end local v6    # "selectionArgs":[Ljava/lang/String;
    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v35

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    .restart local v6    # "selectionArgs":[Ljava/lang/String;
    goto/16 :goto_5

    .line 415
    :cond_30
    const-string v5, "(_data LIKE ? escape \'\\\' OR  substr(_data,1+length(rtrim(_data,replace(_data,\'/\',\'\'))))  LIKE ? escape \'\\\'OR  substr(_data,1+length(rtrim(_data,replace(_data,\'/\',\'\'))))  LIKE ? escape \'\\\')"

    .line 417
    const/4 v2, 0x3

    new-array v6, v2, [Ljava/lang/String;

    .end local v6    # "selectionArgs":[Ljava/lang/String;
    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    const/4 v2, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    .restart local v6    # "selectionArgs":[Ljava/lang/String;
    goto/16 :goto_5

    .line 432
    .restart local v9    # "builder":Ljava/lang/StringBuilder;
    .restart local v17    # "i$":Ljava/util/Iterator;
    .restart local v28    # "prefix":Ljava/lang/String;
    :cond_31
    const-string v2, " ) "

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 433
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 437
    .end local v9    # "builder":Ljava/lang/StringBuilder;
    .end local v17    # "i$":Ljava/util/Iterator;
    .end local v28    # "prefix":Ljava/lang/String;
    :cond_32
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowHiddenFileStatus()Z

    move-result v2

    if-nez v2, :cond_33

    .line 439
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND NOT (SUBSTR(LOWER("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_data"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "),LENGTH(RTRIM(LOWER("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_data"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "),"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\"abcdefghijklmnopqrstuvwxyz01234567890;.!@#$%^&()-_=+\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "))) LIKE \'/.%\')"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 442
    :cond_33
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v2

    const/4 v3, 0x6

    if-ne v2, v3, :cond_34

    .line 444
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getKeyword()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_34

    .line 446
    const/4 v2, 0x1

    const-string v3, "%%%"

    aput-object v3, v6, v2

    .line 450
    :cond_34
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mCurrentSortBy:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mCurrentInOrder:I

    const/4 v4, 0x1

    const/16 v40, 0x0

    move/from16 v0, v40

    invoke-static {v2, v3, v4, v0}, Lcom/sec/android/app/myfiles/utils/MyFilesDB;->getOrderBy(IIIZ)Ljava/lang/String;

    move-result-object v7

    .line 451
    .local v7, "orderBy":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContentResolver:Landroid/content/ContentResolver;

    const-string v3, "external"

    invoke-static {v3}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 457
    .end local v5    # "where":Ljava/lang/String;
    .end local v6    # "selectionArgs":[Ljava/lang/String;
    .end local v7    # "orderBy":Ljava/lang/String;
    :cond_35
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_40

    .line 459
    const/4 v2, 0x2

    new-array v8, v2, [Landroid/database/Cursor;

    .line 460
    .local v8, "arrCursor":[Landroid/database/Cursor;
    const/4 v2, 0x0

    aput-object v14, v8, v2

    .line 461
    const/4 v2, 0x1

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getKeyword()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->performDownlodedAppSearch(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    aput-object v3, v8, v2

    .line 462
    new-instance v14, Landroid/database/MergeCursor;

    .end local v14    # "cursor":Landroid/database/Cursor;
    invoke-direct {v14, v8}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 481
    .end local v8    # "arrCursor":[Landroid/database/Cursor;
    .restart local v14    # "cursor":Landroid/database/Cursor;
    :cond_36
    :goto_9
    const/4 v11, 0x0

    .line 484
    .local v11, "contentCursor":Landroid/database/Cursor;
    new-instance v36, Landroid/database/MatrixCursor;

    sget-object v2, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mFilesTitleSearchProjection:[Ljava/lang/String;

    move-object/from16 v0, v36

    invoke-direct {v0, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 485
    .local v36, "titleSearchHeading":Landroid/database/MatrixCursor;
    if-eqz v14, :cond_44

    .line 486
    const/16 v2, 0x8

    new-array v10, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, ""

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v10, v2

    const/4 v2, 0x1

    const-string v3, ""

    aput-object v3, v10, v2

    const/4 v2, 0x2

    const-string v3, ""

    aput-object v3, v10, v2

    const/4 v2, 0x3

    const-string v3, "0"

    aput-object v3, v10, v2

    const/4 v2, 0x4

    const-string v3, ""

    aput-object v3, v10, v2

    const/4 v2, 0x5

    const-string v3, ""

    aput-object v3, v10, v2

    const/4 v2, 0x6

    const-string v3, "true"

    aput-object v3, v10, v2

    const/4 v2, 0x7

    const-string v3, ""

    aput-object v3, v10, v2

    .line 488
    .local v10, "column":[Ljava/lang/Object;
    move-object/from16 v0, v36

    invoke-virtual {v0, v10}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 495
    :goto_a
    const/4 v12, 0x0

    .line 497
    .local v12, "count":I
    if-eqz v14, :cond_37

    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_37

    .line 498
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentView()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_45

    .line 499
    add-int/lit8 v12, v12, 0x1

    .line 504
    :cond_37
    :goto_b
    if-eqz v11, :cond_38

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_38

    .line 505
    add-int/lit8 v12, v12, 0x1

    .line 508
    :cond_38
    new-array v8, v12, [Landroid/database/Cursor;

    .line 510
    .restart local v8    # "arrCursor":[Landroid/database/Cursor;
    const/4 v12, 0x0

    .line 512
    if-eqz v14, :cond_47

    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_47

    invoke-interface {v14}, Landroid/database/Cursor;->getColumnCount()I

    move-result v2

    if-eqz v2, :cond_47

    .line 513
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentView()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_46

    .line 514
    add-int/lit8 v13, v12, 0x1

    .end local v12    # "count":I
    .local v13, "count":I
    aput-object v36, v8, v12

    move v12, v13

    .line 518
    .end local v13    # "count":I
    .restart local v12    # "count":I
    :goto_c
    add-int/lit8 v13, v12, 0x1

    .end local v12    # "count":I
    .restart local v13    # "count":I
    new-instance v2, Lcom/sec/android/app/myfiles/SortCursor;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mCurrentSortBy:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mCurrentInOrder:I

    const/16 v40, 0x1

    move/from16 v0, v40

    invoke-direct {v2, v14, v3, v4, v0}, Lcom/sec/android/app/myfiles/SortCursor;-><init>(Landroid/database/Cursor;IIZ)V

    aput-object v2, v8, v12

    .line 523
    :goto_d
    if-eqz v11, :cond_57

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_57

    .line 524
    add-int/lit8 v12, v13, 0x1

    .end local v13    # "count":I
    .restart local v12    # "count":I
    new-instance v2, Lcom/sec/android/app/myfiles/SortCursor;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mCurrentSortBy:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mCurrentInOrder:I

    const/16 v40, 0x1

    move/from16 v0, v40

    invoke-direct {v2, v11, v3, v4, v0}, Lcom/sec/android/app/myfiles/SortCursor;-><init>(Landroid/database/Cursor;IIZ)V

    aput-object v2, v8, v13

    .line 528
    :goto_e
    array-length v2, v8

    if-lez v2, :cond_39

    .line 529
    new-instance v14, Landroid/database/MergeCursor;

    .end local v14    # "cursor":Landroid/database/Cursor;
    invoke-direct {v14, v8}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 531
    .restart local v14    # "cursor":Landroid/database/Cursor;
    :cond_39
    const/4 v2, 0x3

    new-array v0, v2, [Landroid/database/Cursor;

    move-object/from16 v37, v0

    .line 532
    .local v37, "usbDropboxArrCursor":[Landroid/database/Cursor;
    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-eqz v2, :cond_3c

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3c

    .line 533
    if-nez v26, :cond_3a

    if-eqz v27, :cond_48

    .line 534
    :cond_3a
    if-eqz v14, :cond_3b

    .line 535
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 536
    const/4 v14, 0x0

    .line 554
    :cond_3b
    :goto_f
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->getSearchCursorUsb(Lcom/sec/android/app/myfiles/utils/SearchParameter;)Landroid/database/MatrixCursor;

    move-result-object v21

    .line 555
    .local v21, "mc":Landroid/database/Cursor;
    const/16 v22, 0x0

    .line 556
    .local v22, "mergeCursor":Landroid/database/Cursor;
    const/4 v2, 0x2

    new-array v8, v2, [Landroid/database/Cursor;

    .line 557
    const/4 v2, 0x0

    aput-object v14, v8, v2

    .line 558
    const/4 v2, 0x1

    aput-object v21, v8, v2

    .line 560
    if-eqz v14, :cond_4e

    .line 561
    new-instance v22, Landroid/database/MergeCursor;

    .end local v22    # "mergeCursor":Landroid/database/Cursor;
    move-object/from16 v0, v22

    invoke-direct {v0, v8}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 562
    .restart local v22    # "mergeCursor":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0008

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b00fa

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 566
    const/4 v2, 0x0

    aput-object v14, v37, v2

    .line 567
    const/4 v2, 0x1

    aput-object v21, v37, v2

    .line 579
    .end local v21    # "mc":Landroid/database/Cursor;
    .end local v22    # "mergeCursor":Landroid/database/Cursor;
    :cond_3c
    :goto_10
    const/16 v23, 0x0

    .line 580
    .local v23, "onlyDownloadedApp":Z
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    if-eqz v2, :cond_3d

    .line 581
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/myfiles/utils/FileType;->DOWNLOADEDAPPS:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual {v2, v3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_51

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/myfiles/utils/FileType;->AUDIO:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual {v2, v3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_51

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/myfiles/utils/FileType;->IMAGE:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual {v2, v3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_51

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/myfiles/utils/FileType;->VIDEO:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual {v2, v3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_51

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/myfiles/utils/FileType;->DOCUMENT:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual {v2, v3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_51

    const/16 v23, 0x1

    .line 587
    :cond_3d
    :goto_11
    if-nez v23, :cond_55

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0008

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b00fa

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_55

    .line 588
    :cond_3e
    if-eqz v25, :cond_3f

    .line 589
    const/4 v14, 0x0

    .line 591
    :cond_3f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/sec/android/app/myfiles/utils/Utils;->getDropBoxFilteredContent(Landroid/content/Context;Lcom/sec/android/app/myfiles/utils/SearchParameter;)Landroid/database/Cursor;

    move-result-object v21

    .line 592
    .restart local v21    # "mc":Landroid/database/Cursor;
    const/16 v22, 0x0

    .line 593
    .restart local v22    # "mergeCursor":Landroid/database/Cursor;
    const/4 v2, 0x2

    new-array v8, v2, [Landroid/database/Cursor;

    .line 594
    const/4 v2, 0x0

    aput-object v14, v8, v2

    .line 595
    const/4 v2, 0x1

    aput-object v21, v8, v2

    .line 596
    if-eqz v14, :cond_53

    .line 598
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_52

    .line 599
    new-instance v22, Landroid/database/MergeCursor;

    .end local v22    # "mergeCursor":Landroid/database/Cursor;
    move-object/from16 v0, v22

    invoke-direct {v0, v8}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .restart local v22    # "mergeCursor":Landroid/database/Cursor;
    goto/16 :goto_7

    .line 463
    .end local v8    # "arrCursor":[Landroid/database/Cursor;
    .end local v10    # "column":[Ljava/lang/Object;
    .end local v11    # "contentCursor":Landroid/database/Cursor;
    .end local v12    # "count":I
    .end local v21    # "mc":Landroid/database/Cursor;
    .end local v22    # "mergeCursor":Landroid/database/Cursor;
    .end local v23    # "onlyDownloadedApp":Z
    .end local v36    # "titleSearchHeading":Landroid/database/MatrixCursor;
    .end local v37    # "usbDropboxArrCursor":[Landroid/database/Cursor;
    :cond_40
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v2

    const/4 v3, 0x6

    if-ne v2, v3, :cond_36

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    if-eqz v2, :cond_36

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/myfiles/utils/FileType;->DOWNLOADEDAPPS:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual {v2, v3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_36

    .line 466
    if-eqz v24, :cond_41

    .line 467
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getKeyword()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->performDownlodedAppSearch(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    goto/16 :goto_9

    .line 469
    :cond_41
    const/4 v2, 0x2

    new-array v8, v2, [Landroid/database/Cursor;

    .line 470
    .restart local v8    # "arrCursor":[Landroid/database/Cursor;
    const/4 v2, 0x0

    aput-object v14, v8, v2

    .line 472
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getExtentionString()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_42

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getExtentionString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_43

    .line 473
    :cond_42
    const/4 v2, 0x1

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getKeyword()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->performDownlodedAppSearch(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    aput-object v3, v8, v2

    .line 476
    :cond_43
    new-instance v14, Landroid/database/MergeCursor;

    .end local v14    # "cursor":Landroid/database/Cursor;
    invoke-direct {v14, v8}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .restart local v14    # "cursor":Landroid/database/Cursor;
    goto/16 :goto_9

    .line 490
    .end local v8    # "arrCursor":[Landroid/database/Cursor;
    .restart local v11    # "contentCursor":Landroid/database/Cursor;
    .restart local v36    # "titleSearchHeading":Landroid/database/MatrixCursor;
    :cond_44
    const/16 v2, 0x8

    new-array v10, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, ""

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v10, v2

    const/4 v2, 0x1

    const-string v3, ""

    aput-object v3, v10, v2

    const/4 v2, 0x2

    const-string v3, ""

    aput-object v3, v10, v2

    const/4 v2, 0x3

    const-string v3, "0"

    aput-object v3, v10, v2

    const/4 v2, 0x4

    const-string v3, ""

    aput-object v3, v10, v2

    const/4 v2, 0x5

    const-string v3, ""

    aput-object v3, v10, v2

    const/4 v2, 0x6

    const-string v3, "false"

    aput-object v3, v10, v2

    const/4 v2, 0x7

    const-string v3, ""

    aput-object v3, v10, v2

    .line 492
    .restart local v10    # "column":[Ljava/lang/Object;
    move-object/from16 v0, v36

    invoke-virtual {v0, v10}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_a

    .line 501
    .restart local v12    # "count":I
    :cond_45
    add-int/lit8 v12, v12, 0x2

    goto/16 :goto_b

    .line 516
    .restart local v8    # "arrCursor":[Landroid/database/Cursor;
    :cond_46
    invoke-virtual/range {v36 .. v36}, Landroid/database/MatrixCursor;->close()V

    goto/16 :goto_c

    .line 521
    :cond_47
    invoke-virtual/range {v36 .. v36}, Landroid/database/MatrixCursor;->close()V

    move v13, v12

    .end local v12    # "count":I
    .restart local v13    # "count":I
    goto/16 :goto_d

    .line 539
    .end local v13    # "count":I
    .restart local v12    # "count":I
    .restart local v37    # "usbDropboxArrCursor":[Landroid/database/Cursor;
    :cond_48
    if-eqz v29, :cond_49

    .line 540
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0042

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_f

    .line 541
    :cond_49
    if-eqz v30, :cond_4a

    .line 542
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0043

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_f

    .line 543
    :cond_4a
    if-eqz v31, :cond_4b

    .line 544
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0044

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_f

    .line 545
    :cond_4b
    if-eqz v32, :cond_4c

    .line 546
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0045

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_f

    .line 547
    :cond_4c
    if-eqz v33, :cond_4d

    .line 548
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0046

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_f

    .line 549
    :cond_4d
    if-eqz v34, :cond_3b

    .line 550
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0047

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_f

    .line 570
    .restart local v21    # "mc":Landroid/database/Cursor;
    .restart local v22    # "mergeCursor":Landroid/database/Cursor;
    :cond_4e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0008

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->storageType:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b00fa

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_50

    :cond_4f
    move-object/from16 v22, v21

    .line 572
    goto/16 :goto_7

    .line 574
    :cond_50
    const/4 v2, 0x0

    aput-object v21, v37, v2

    goto/16 :goto_10

    .line 581
    .end local v21    # "mc":Landroid/database/Cursor;
    .end local v22    # "mergeCursor":Landroid/database/Cursor;
    .restart local v23    # "onlyDownloadedApp":Z
    :cond_51
    const/16 v23, 0x0

    goto/16 :goto_11

    .line 601
    .restart local v21    # "mc":Landroid/database/Cursor;
    .restart local v22    # "mergeCursor":Landroid/database/Cursor;
    :cond_52
    const/4 v2, 0x2

    aput-object v21, v37, v2

    .line 602
    new-instance v22, Landroid/database/MergeCursor;

    .end local v22    # "mergeCursor":Landroid/database/Cursor;
    move-object/from16 v0, v22

    move-object/from16 v1, v37

    invoke-direct {v0, v1}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .restart local v22    # "mergeCursor":Landroid/database/Cursor;
    goto/16 :goto_7

    .line 606
    :cond_53
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_54

    move-object/from16 v22, v21

    .line 607
    goto/16 :goto_7

    .line 609
    :cond_54
    const/4 v2, 0x1

    aput-object v21, v37, v2

    .line 610
    new-instance v22, Landroid/database/MergeCursor;

    .end local v22    # "mergeCursor":Landroid/database/Cursor;
    move-object/from16 v0, v22

    move-object/from16 v1, v37

    invoke-direct {v0, v1}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 611
    .restart local v22    # "mergeCursor":Landroid/database/Cursor;
    goto/16 :goto_7

    .line 615
    .end local v21    # "mc":Landroid/database/Cursor;
    .end local v22    # "mergeCursor":Landroid/database/Cursor;
    :cond_55
    if-eqz v14, :cond_56

    move-object/from16 v22, v14

    .line 616
    goto/16 :goto_7

    .line 618
    :cond_56
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->getEmptyCourso()Landroid/database/Cursor;

    move-result-object v22

    goto/16 :goto_7

    .end local v12    # "count":I
    .end local v23    # "onlyDownloadedApp":Z
    .end local v37    # "usbDropboxArrCursor":[Landroid/database/Cursor;
    .restart local v13    # "count":I
    :cond_57
    move v12, v13

    .end local v13    # "count":I
    .restart local v12    # "count":I
    goto/16 :goto_e
.end method

.method public getSearchCursorUsb(Lcom/sec/android/app/myfiles/utils/SearchParameter;)Landroid/database/MatrixCursor;
    .locals 12
    .param p1, "param"    # Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .prologue
    .line 846
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getMountedUsbStorage()Ljava/util/ArrayList;

    move-result-object v6

    .line 848
    .local v6, "mountedUsbList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;>;"
    new-instance v4, Landroid/database/MatrixCursor;

    sget-object v7, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mFilesApiProjection:[Ljava/lang/String;

    invoke-direct {v4, v7}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 849
    .local v4, "mc":Landroid/database/MatrixCursor;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v3, v7, :cond_8

    .line 850
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;

    .line 851
    .local v5, "mountedUsb":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    new-instance v1, Ljava/io/File;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->getStoragePath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 852
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 853
    const/4 v2, 0x0

    .line 854
    .local v2, "files":[Ljava/io/File;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->isEnableFiltering()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 855
    new-instance v7, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$1;

    invoke-direct {v7, p0}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$1;-><init>(Lcom/sec/android/app/myfiles/navigation/SearchNavigation;)V

    invoke-virtual {v1, v7}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v2

    .line 874
    :goto_1
    if-eqz v2, :cond_7

    .line 875
    const/4 v0, 0x0

    .local v0, "ctr":I
    :goto_2
    array-length v7, v2

    if-ge v0, v7, :cond_7

    .line 876
    aget-object v7, v2, v0

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->checkNotVewingItem(Ljava/io/File;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 875
    :cond_0
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 871
    .end local v0    # "ctr":I
    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    goto :goto_1

    .line 879
    .restart local v0    # "ctr":I
    :cond_2
    aget-object v7, v2, v0

    invoke-virtual {v7}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 881
    aget-object v7, v2, v0

    invoke-direct {p0, p1, v7}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->checkMedia(Lcom/sec/android/app/myfiles/utils/SearchParameter;Ljava/io/File;)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_3

    aget-object v7, v2, v0

    invoke-direct {p0, p1, v7}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->checkDate(Lcom/sec/android/app/myfiles/utils/SearchParameter;Ljava/io/File;)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_3

    aget-object v7, v2, v0

    invoke-direct {p0, p1, v7}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->checkExt(Lcom/sec/android/app/myfiles/utils/SearchParameter;Ljava/io/File;)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 884
    const/4 v7, 0x6

    new-array v8, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    add-int/lit8 v9, v0, 0x1

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v7

    const/4 v7, 0x1

    aget-object v9, v2, v0

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v7

    const/4 v7, 0x2

    aget-object v9, v2, v0

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v7

    const/4 v9, 0x3

    aget-object v7, v2, v0

    invoke-virtual {v7}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_4

    const/16 v7, 0x3001

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    :goto_4
    aput-object v7, v8, v9

    const/4 v7, 0x4

    aget-object v9, v2, v0

    invoke-static {v9}, Lcom/sec/android/app/myfiles/utils/Utils;->getTotalSize(Ljava/io/File;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v7

    const/4 v7, 0x5

    aget-object v9, v2, v0

    invoke-virtual {v9}, Ljava/io/File;->lastModified()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v7

    invoke-virtual {v4, v8}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 892
    :cond_3
    aget-object v7, v2, v0

    invoke-direct {p0, v7, v4, p1}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->goSearchChildNode(Ljava/io/File;Landroid/database/MatrixCursor;Lcom/sec/android/app/myfiles/utils/SearchParameter;)V

    goto/16 :goto_3

    .line 884
    :cond_4
    const-string v7, "0"

    goto :goto_4

    .line 894
    :cond_5
    aget-object v7, v2, v0

    invoke-direct {p0, p1, v7}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->checkMedia(Lcom/sec/android/app/myfiles/utils/SearchParameter;Ljava/io/File;)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_0

    aget-object v7, v2, v0

    invoke-direct {p0, p1, v7}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->checkDate(Lcom/sec/android/app/myfiles/utils/SearchParameter;Ljava/io/File;)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_0

    aget-object v7, v2, v0

    invoke-direct {p0, p1, v7}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->checkExt(Lcom/sec/android/app/myfiles/utils/SearchParameter;Ljava/io/File;)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 896
    const/4 v7, 0x6

    new-array v8, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    add-int/lit8 v9, v0, 0x1

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v7

    const/4 v7, 0x1

    aget-object v9, v2, v0

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v7

    const/4 v7, 0x2

    aget-object v9, v2, v0

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v7

    const/4 v9, 0x3

    aget-object v7, v2, v0

    invoke-virtual {v7}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_6

    const/16 v7, 0x3001

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    :goto_5
    aput-object v7, v8, v9

    const/4 v7, 0x4

    aget-object v9, v2, v0

    invoke-static {v9}, Lcom/sec/android/app/myfiles/utils/Utils;->getTotalSize(Ljava/io/File;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v7

    const/4 v7, 0x5

    aget-object v9, v2, v0

    invoke-virtual {v9}, Ljava/io/File;->lastModified()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v7

    invoke-virtual {v4, v8}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_3

    :cond_6
    const-string v7, "0"

    goto :goto_5

    .line 849
    .end local v0    # "ctr":I
    .end local v2    # "files":[Ljava/io/File;
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 907
    .end local v1    # "file":Ljava/io/File;
    .end local v5    # "mountedUsb":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    :cond_8
    return-object v4
.end method

.method public goBack()Z
    .locals 1

    .prologue
    .line 1283
    const/4 v0, 0x0

    return v0
.end method

.method public goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z
    .locals 1
    .param p1, "folderPath"    # Ljava/lang/String;
    .param p2, "isBack"    # Ljava/lang/Boolean;

    .prologue
    .line 1268
    const/4 v0, 0x0

    return v0
.end method

.method public goToRoot(Ljava/lang/Boolean;)Z
    .locals 1
    .param p1, "isBack"    # Ljava/lang/Boolean;

    .prologue
    .line 1273
    const/4 v0, 0x0

    return v0
.end method

.method public goUp()Z
    .locals 1

    .prologue
    .line 1278
    const/4 v0, 0x0

    return v0
.end method

.method public isCurrentFolderRoot()Z
    .locals 1

    .prologue
    .line 1263
    const/4 v0, 0x0

    return v0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 166
    invoke-super {p0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->onPause()V

    .line 167
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 161
    invoke-super {p0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->onResume()V

    .line 162
    return-void
.end method

.method performDownlodedAppSearch(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1, "serchKeyword"    # Ljava/lang/String;

    .prologue
    .line 1684
    new-instance v2, Landroid/database/MatrixCursor;

    sget-object v3, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mFilesApiProjectionWithDownloadedAppsProjection:[Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 1685
    .local v2, "mDownAppsCursor":Landroid/database/MatrixCursor;
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->loadInstalledApps()V

    .line 1686
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mAppEntriesList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 1687
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mAppEntriesList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .line 1688
    .local v0, "appEntry":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getLabel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1690
    const/4 v3, 0x7

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getPackageName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getLabel()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "2131427335"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getTotalSize()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getDate()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x6

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 1686
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 1702
    .end local v0    # "appEntry":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    :cond_1
    return-object v2
.end method

.method public refreshNavigation()V
    .locals 0

    .prologue
    .line 1574
    return-void
.end method

.method public setCurrentPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 1104
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 1106
    return-void
.end method

.method public synclist(Z)V
    .locals 13
    .param p1, "bUsingTransaction"    # Z

    .prologue
    .line 1769
    const/4 v9, 0x2

    :try_start_0
    const-string v10, "SearchNavigation"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SyncList Dropbox syncing bUsingTransaction = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1772
    iget-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mIsSyncing:Z

    if-eqz v9, :cond_0

    .line 1773
    const/4 v9, 0x0

    const-string v10, "SearchNavigation"

    const-string v11, "SyncList  Dropbox syncing cancelling, syncing has already started"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1775
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mbAbort:Z

    .line 1930
    :goto_0
    return-void

    .line 1779
    :cond_0
    const/4 v9, 0x2

    const-string v10, "SearchNavigation"

    const-string v11, "SyncList  Dropbox syncing has started"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1784
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mIsSyncing:Z

    .line 1785
    const/4 v0, 0x0

    .line 1786
    .local v0, "CloudDeltaCursor":Ljava/lang/String;
    new-instance v3, Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    invoke-direct {v3, v9}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    .line 1787
    .local v3, "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 1788
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->getDropboxCursor()Ljava/lang/String;

    move-result-object v0

    .line 1789
    if-eqz v0, :cond_1

    const-string v9, ""

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1790
    :cond_1
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->clearCloudData()J

    .line 1791
    :cond_2
    const/4 v7, 0x0

    .line 1793
    .local v7, "nodes":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudNode;>;"
    const/4 v1, 0x0

    .line 1795
    .local v1, "SyncCount":I
    const/4 v2, 0x0

    .line 1797
    .local v2, "TotalCount":I
    iget-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mbAbort:Z

    if-eqz v9, :cond_5

    .line 1799
    const/4 v9, 0x2

    const-string v10, "SearchNavigation"

    const-string v11, "SyncList Dropbox syncing has End mbAbort is ture"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1801
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mIsSyncing:Z

    .line 1803
    iget-object v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-eqz v9, :cond_3

    .line 1804
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->showDialog:Z

    .line 1805
    iget-object v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setCompletedCondition(Z)V

    .line 1807
    :cond_3
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mbAbort:Z

    .line 1808
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1916
    .end local v0    # "CloudDeltaCursor":Ljava/lang/String;
    .end local v1    # "SyncCount":I
    .end local v2    # "TotalCount":I
    .end local v3    # "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    .end local v7    # "nodes":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudNode;>;"
    :catch_0
    move-exception v4

    .line 1918
    .local v4, "e":Ljava/lang/Exception;
    const/4 v9, 0x2

    const-string v10, "SearchNavigation"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SyncList Dropbox syncing exception "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 1920
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 1922
    iget-object v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-eqz v9, :cond_4

    .line 1923
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->showDialog:Z

    .line 1924
    iget-object v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setCompletedCondition(Z)V

    .line 1926
    :cond_4
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mIsSyncing:Z

    .line 1927
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mbAbort:Z

    goto :goto_0

    .line 1813
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v0    # "CloudDeltaCursor":Ljava/lang/String;
    .restart local v1    # "SyncCount":I
    .restart local v2    # "TotalCount":I
    .restart local v3    # "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    .restart local v7    # "nodes":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudNode;>;"
    :cond_5
    move-object v8, v0

    .line 1817
    .local v8, "workingDelta":Ljava/lang/String;
    :cond_6
    :try_start_1
    iget-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mbAbort:Z

    if-eqz v9, :cond_9

    .line 1819
    const/4 v9, 0x2

    const-string v10, "SearchNavigation"

    const-string v11, "SyncList Dropbox mbAbort is ture break"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1821
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mbAbort:Z

    .line 1906
    :cond_7
    :goto_1
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 1907
    iget-object v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-eqz v9, :cond_8

    .line 1908
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->showDialog:Z

    .line 1909
    iget-object v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setCompletedCondition(Z)V

    .line 1913
    :cond_8
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mIsSyncing:Z

    .line 1915
    const/4 v9, 0x2

    const-string v10, "SearchNavigation"

    const-string v11, "SyncList Dropbox syncing has End"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1825
    :cond_9
    iget-object v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    invoke-virtual {v9, v8}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->getDeltaList(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;

    move-result-object v7

    .line 1827
    if-eqz v7, :cond_7

    .line 1830
    invoke-virtual {v7}, Lcom/samsung/scloud/data/SCloudNodeList;->getCursor()Ljava/lang/String;

    move-result-object v8

    .line 1831
    const/4 v9, 0x2

    const-string v10, "SearchNavigation"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SyncList getList - nodes :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v7}, Lcom/samsung/scloud/data/SCloudNodeList;->getNodes()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1834
    if-eqz p1, :cond_a

    .line 1836
    invoke-virtual {v7}, Lcom/samsung/scloud/data/SCloudNodeList;->getNodes()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_a

    .line 1837
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->beginTransaction()V

    .line 1840
    :cond_a
    if-eqz v7, :cond_b

    .line 1841
    invoke-virtual {v7}, Lcom/samsung/scloud/data/SCloudNodeList;->getNodes()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/scloud/data/SCloudNode;

    .line 1843
    .local v6, "node":Lcom/samsung/scloud/data/SCloudNode;
    iget-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mbAbort:Z

    if-eqz v9, :cond_d

    .line 1845
    const/4 v9, 0x2

    const-string v10, "SearchNavigation"

    const-string v11, "SyncList Dropbox mbAbort is ture break for loop"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1864
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "node":Lcom/samsung/scloud/data/SCloudNode;
    :cond_b
    const/4 v9, 0x2

    const-string v10, "SearchNavigation"

    const-string v11, "SyncList db.insert done "

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1866
    if-eqz p1, :cond_c

    .line 1868
    invoke-virtual {v7}, Lcom/samsung/scloud/data/SCloudNodeList;->getNodes()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    .line 1869
    invoke-virtual {v7}, Lcom/samsung/scloud/data/SCloudNodeList;->getNodes()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    add-int/2addr v2, v9

    .line 1873
    if-lez v1, :cond_c

    .line 1875
    const/4 v9, 0x2

    const-string v10, "SearchNavigation"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SyncList SyncCount Transcation : SyncCount = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1877
    const/4 v9, 0x2

    const-string v10, "SearchNavigation"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SyncList SyncCount Transcation : TotalCount = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1880
    const/4 v1, 0x0

    .line 1882
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->setTransactionSuccessful()V

    .line 1883
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->endTransaction()V

    .line 1885
    const/4 v9, 0x2

    const-string v10, "SearchNavigation"

    const-string v11, "SyncList SyncCount Transcation : endTransaction "

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1891
    :cond_c
    iget-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mbAbort:Z

    if-eqz v9, :cond_f

    .line 1892
    const/4 v9, 0x2

    const-string v10, "SearchNavigation"

    const-string v11, "SyncList Dropbox mbAbort is ture break ,do while loop"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1894
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mbAbort:Z

    goto/16 :goto_1

    .line 1850
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v6    # "node":Lcom/samsung/scloud/data/SCloudNode;
    :cond_d
    invoke-virtual {v6}, Lcom/samsung/scloud/data/SCloudNode;->isDeleted()Z

    move-result v9

    if-eqz v9, :cond_e

    .line 1851
    invoke-virtual {v3, v6}, Lcom/sec/android/app/myfiles/db/CacheDB;->deleteCloudData(Lcom/samsung/scloud/data/SCloudNode;)I

    goto/16 :goto_2

    .line 1853
    :cond_e
    invoke-virtual {v3, v6, p1}, Lcom/sec/android/app/myfiles/db/CacheDB;->insertCloudData(Lcom/samsung/scloud/data/SCloudNode;Z)J

    goto/16 :goto_2

    .line 1899
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "node":Lcom/samsung/scloud/data/SCloudNode;
    :cond_f
    move-object v0, v8

    .line 1900
    invoke-virtual {v3, v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->setDropboxCursor(Ljava/lang/String;)V

    .line 1904
    invoke-virtual {v7}, Lcom/samsung/scloud/data/SCloudNodeList;->getHasMore()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v9

    const/4 v10, 0x1

    if-eq v9, v10, :cond_6

    goto/16 :goto_1
.end method
