.class public Lcom/sec/android/app/myfiles/ftp/FTPParams;
.super Ljava/lang/Object;
.source "FTPParams.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/ftp/FTPParams$1;,
        Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;
    }
.end annotation


# static fields
.field private static final AT:Ljava/lang/String; = "@"

.field private static final COLON:Ljava/lang/String; = ":"

.field private static final FTP_PREFIX:Ljava/lang/String; = "ftp://"

.field private static final ID_ANONYMOUS:Ljava/lang/String; = "anonymous"

.field private static final ID_ENCODING:Ljava/lang/String; = "encoding"

.field private static final ID_ENCRYPTION:Ljava/lang/String; = "encryption"

.field private static final ID_FTP_TYPE:Ljava/lang/String; = "type"

.field private static final ID_HIDDEN_FILES:Ljava/lang/String; = "hiddenFiles"

.field private static final ID_HOST:Ljava/lang/String; = "host"

.field private static final ID_MODE:Ljava/lang/String; = "mode"

.field private static final ID_PASSWORD:Ljava/lang/String; = "password"

.field private static final ID_PORT:Ljava/lang/String; = "port"

.field private static final ID_ROOT_SESSION_NAME:Ljava/lang/String; = "rootSessionName"

.field private static final ID_SESSION_NAME:Ljava/lang/String; = "sessionName"

.field private static final ID_START_PATH:Ljava/lang/String; = "startpath"

.field private static final ID_USERNAME:Ljava/lang/String; = "username"

.field private static final SEP:Ljava/lang/String; = ","

.field private static final SFTP_PREFIX:Ljava/lang/String; = "sftp://"


# instance fields
.field private final mAnonymous:Z

.field private final mEncoding:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

.field private final mEncryption:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

.field private final mHiddenFiles:Z

.field private final mHost:Ljava/lang/String;

.field private final mMode:Lcom/sec/android/app/myfiles/ftp/FTPMode;

.field private final mPassword:Ljava/lang/String;

.field private final mPort:I

.field private final mRootSessionName:Ljava/lang/String;

.field private final mSessionName:Ljava/lang/String;

.field private final mStartPath:Ljava/lang/String;

.field private final mType:Lcom/sec/android/app/myfiles/ftp/FTPType;

.field private final mUserName:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    # getter for: Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mType:Lcom/sec/android/app/myfiles/ftp/FTPType;
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->access$000(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)Lcom/sec/android/app/myfiles/ftp/FTPType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mType:Lcom/sec/android/app/myfiles/ftp/FTPType;

    .line 30
    # getter for: Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mSessionName:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->access$100(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mSessionName:Ljava/lang/String;

    .line 31
    # getter for: Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mHost:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->access$200(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mHost:Ljava/lang/String;

    .line 32
    # getter for: Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mPort:I
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->access$300(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mPort:I

    .line 33
    # getter for: Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mMode:Lcom/sec/android/app/myfiles/ftp/FTPMode;
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->access$400(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)Lcom/sec/android/app/myfiles/ftp/FTPMode;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mMode:Lcom/sec/android/app/myfiles/ftp/FTPMode;

    .line 34
    # getter for: Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mUserName:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->access$500(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mUserName:Ljava/lang/String;

    .line 35
    # getter for: Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mPassword:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->access$600(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mPassword:Ljava/lang/String;

    .line 36
    # getter for: Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mAnonymous:Z
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->access$700(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mAnonymous:Z

    .line 37
    # getter for: Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mEncoding:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->access$800(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mEncoding:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    .line 38
    # getter for: Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mEncryption:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->access$900(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mEncryption:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    .line 39
    # getter for: Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mHiddenFiles:Z
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->access$1000(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mHiddenFiles:Z

    .line 40
    # getter for: Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mStartPath:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->access$1100(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mStartPath:Ljava/lang/String;

    .line 41
    # getter for: Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mRootSessionName:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->access$1200(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mRootSessionName:Ljava/lang/String;

    .line 42
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;Lcom/sec/android/app/myfiles/ftp/FTPParams$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;
    .param p2, "x1"    # Lcom/sec/android/app/myfiles/ftp/FTPParams$1;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)V

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "that"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "label"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iget-object v0, p1, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mType:Lcom/sec/android/app/myfiles/ftp/FTPType;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mType:Lcom/sec/android/app/myfiles/ftp/FTPType;

    .line 75
    iput-object p2, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mSessionName:Ljava/lang/String;

    .line 76
    iget-object v0, p1, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mHost:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mHost:Ljava/lang/String;

    .line 77
    iget v0, p1, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mPort:I

    iput v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mPort:I

    .line 78
    iget-object v0, p1, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mMode:Lcom/sec/android/app/myfiles/ftp/FTPMode;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mMode:Lcom/sec/android/app/myfiles/ftp/FTPMode;

    .line 79
    iget-object v0, p1, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mUserName:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mUserName:Ljava/lang/String;

    .line 80
    iget-object v0, p1, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mPassword:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mPassword:Ljava/lang/String;

    .line 81
    iget-boolean v0, p1, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mAnonymous:Z

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mAnonymous:Z

    .line 82
    iget-object v0, p1, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mEncoding:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mEncoding:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    .line 83
    iget-object v0, p1, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mEncryption:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mEncryption:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    .line 84
    iget-boolean v0, p1, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mHiddenFiles:Z

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mHiddenFiles:Z

    .line 85
    iput-object p3, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mStartPath:Ljava/lang/String;

    .line 86
    iget-object v0, p1, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mRootSessionName:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mRootSessionName:Ljava/lang/String;

    .line 87
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4
    .param p1, "paramString"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 47
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 48
    .local v1, "json":Lorg/json/JSONObject;
    const-string v2, "type"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/ftp/FTPType;->valueOf(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPType;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mType:Lcom/sec/android/app/myfiles/ftp/FTPType;

    .line 49
    const-string v2, "sessionName"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mSessionName:Ljava/lang/String;

    .line 50
    const-string v2, "host"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mHost:Ljava/lang/String;

    .line 51
    const-string v2, "port"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mPort:I

    .line 52
    const-string v2, "mode"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/ftp/FTPMode;->valueOf(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPMode;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mMode:Lcom/sec/android/app/myfiles/ftp/FTPMode;

    .line 53
    const-string v2, "username"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mUserName:Ljava/lang/String;

    .line 54
    const-string v2, "password"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mPassword:Ljava/lang/String;

    .line 55
    const-string v2, "anonymous"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mAnonymous:Z

    .line 56
    const-string v2, "encoding"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->valueOf(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mEncoding:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    .line 57
    const-string v2, "encryption"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/ftp/FTPEncryption;->valueOf(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mEncryption:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    .line 58
    const-string v2, "hiddenFiles"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mHiddenFiles:Z

    .line 59
    const-string v2, "startpath"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mStartPath:Ljava/lang/String;

    .line 60
    const-string v2, "rootSessionName"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 61
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mSessionName:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mRootSessionName:Ljava/lang/String;

    .line 71
    :goto_0
    return-void

    .line 63
    :cond_0
    const-string v2, "rootSessionName"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mRootSessionName:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 65
    .end local v1    # "json":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 66
    .local v0, "e":Lorg/json/JSONException;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 69
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2
.end method

.method private getEncodingStr()Ljava/lang/String;
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mEncoding:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    if-eqz v0, :cond_0

    .line 184
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mEncoding:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 188
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ","

    goto :goto_0
.end method

.method private getModeStr()Ljava/lang/String;
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mMode:Lcom/sec/android/app/myfiles/ftp/FTPMode;

    if-eqz v0, :cond_0

    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mMode:Lcom/sec/android/app/myfiles/ftp/FTPMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 174
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ","

    goto :goto_0
.end method

.method private getPrefix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 146
    sget-object v0, Lcom/sec/android/app/myfiles/ftp/FTPParams$1;->$SwitchMap$com$sec$android$app$myfiles$ftp$FTPType:[I

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mType:Lcom/sec/android/app/myfiles/ftp/FTPType;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/ftp/FTPType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 160
    const-string v0, ""

    :goto_0
    return-object v0

    .line 152
    :pswitch_0
    const-string v0, "ftp://"

    goto :goto_0

    .line 156
    :pswitch_1
    const-string v0, "sftp://"

    goto :goto_0

    .line 146
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getStr()Ljava/lang/String;
    .locals 4

    .prologue
    .line 286
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 287
    .local v0, "arg":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "port"

    iget v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mPort:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    const-string v2, "type"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mType:Lcom/sec/android/app/myfiles/ftp/FTPType;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/ftp/FTPType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    const-string v2, "host"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mHost:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    const-string v2, "mode"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mMode:Lcom/sec/android/app/myfiles/ftp/FTPMode;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/ftp/FTPMode;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    const-string v2, "username"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mUserName:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    const-string v2, "encoding"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mEncoding:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    const-string v2, "rootSessionName"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mRootSessionName:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 295
    .local v1, "json":Lorg/json/JSONObject;
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public equals(Lcom/sec/android/app/myfiles/ftp/FTPParams;)Z
    .locals 2
    .param p1, "that"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .prologue
    .line 239
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mType:Lcom/sec/android/app/myfiles/ftp/FTPType;

    iget-object v1, p1, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mType:Lcom/sec/android/app/myfiles/ftp/FTPType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/ftp/FTPType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mHost:Ljava/lang/String;

    iget-object v1, p1, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mHost:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mPort:I

    iget v1, p1, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mPort:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mMode:Lcom/sec/android/app/myfiles/ftp/FTPMode;

    iget-object v1, p1, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mMode:Lcom/sec/android/app/myfiles/ftp/FTPMode;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/ftp/FTPMode;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mUserName:Ljava/lang/String;

    iget-object v1, p1, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mUserName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mPassword:Ljava/lang/String;

    iget-object v1, p1, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mPassword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mAnonymous:Z

    iget-boolean v1, p1, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mAnonymous:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mEncoding:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    iget-object v1, p1, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mEncoding:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mEncryption:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    iget-object v1, p1, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mEncryption:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/ftp/FTPEncryption;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mHiddenFiles:Z

    iget-boolean v1, p1, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mHiddenFiles:Z

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 253
    instance-of v0, p1, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->equals(Lcom/sec/android/app/myfiles/ftp/FTPParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAnonymous()Z
    .locals 1

    .prologue
    .line 215
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mAnonymous:Z

    return v0
.end method

.method public getEncoding()Lcom/sec/android/app/myfiles/ftp/FTPEncoding;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mEncoding:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    return-object v0
.end method

.method public getEncryption()Lcom/sec/android/app/myfiles/ftp/FTPEncryption;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mEncryption:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    return-object v0
.end method

.method public getFullLabel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 132
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getPrefix()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getModeStr()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getEncodingStr()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getHiddenFiles()Z
    .locals 1

    .prologue
    .line 227
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mHiddenFiles:Z

    return v0
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mHost:Ljava/lang/String;

    return-object v0
.end method

.method public getInfoLabel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 118
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getPrefix()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getEncodingStr()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getLabel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 99
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getAnonymous()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 100
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mHost:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    iget v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mPort:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 103
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 110
    :goto_0
    return-object v1

    .line 105
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mUserName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mHost:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    iget v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mPort:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 110
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getMode()Lcom/sec/android/app/myfiles/ftp/FTPMode;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mMode:Lcom/sec/android/app/myfiles/ftp/FTPMode;

    return-object v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mPassword:Ljava/lang/String;

    return-object v0
.end method

.method public getPort()I
    .locals 1

    .prologue
    .line 199
    iget v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mPort:I

    return v0
.end method

.method public getRootSessionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mRootSessionName:Ljava/lang/String;

    return-object v0
.end method

.method public getSessionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mSessionName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mSessionName:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getLabel()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getStartPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mStartPath:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/sec/android/app/myfiles/ftp/FTPType;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mType:Lcom/sec/android/app/myfiles/ftp/FTPType;

    return-object v0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mUserName:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 280
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getStr()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 259
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 260
    .local v0, "arg":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "port"

    iget v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mPort:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    const-string v2, "type"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mType:Lcom/sec/android/app/myfiles/ftp/FTPType;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/ftp/FTPType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    const-string v2, "sessionName"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mSessionName:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    const-string v2, "host"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mHost:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    const-string v2, "mode"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mMode:Lcom/sec/android/app/myfiles/ftp/FTPMode;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/ftp/FTPMode;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    const-string v2, "username"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mUserName:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    const-string v2, "password"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mPassword:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    const-string v2, "anonymous"

    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mAnonymous:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    const-string v2, "encoding"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mEncoding:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    const-string v2, "encryption"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mEncryption:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/ftp/FTPEncryption;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    const-string v2, "hiddenFiles"

    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mHiddenFiles:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    const-string v2, "startpath"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mStartPath:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    const-string v2, "rootSessionName"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams;->mRootSessionName:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 274
    .local v1, "json":Lorg/json/JSONObject;
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
