.class public final enum Lcom/sec/android/app/myfiles/element/HistoryItem$Category;
.super Ljava/lang/Enum;
.source "HistoryItem.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/element/HistoryItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Category"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/myfiles/element/HistoryItem$Category;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

.field public static final enum APPLICATION:Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

.field public static final enum FILE:Lcom/sec/android/app/myfiles/element/HistoryItem$Category;


# instance fields
.field private final mId:I

.field private final mLabel:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 73
    new-instance v0, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    const-string v1, "APPLICATION"

    const-string v2, "APPLICATION"

    invoke-direct {v0, v1, v3, v3, v2}, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;->APPLICATION:Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    .line 74
    new-instance v0, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    const-string v1, "FILE"

    const-string v2, "FILE"

    invoke-direct {v0, v1, v4, v4, v2}, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;->FILE:Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    .line 71
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    sget-object v1, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;->APPLICATION:Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;->FILE:Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;->$VALUES:[Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p3, "id"    # I
    .param p4, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 78
    iput p3, p0, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;->mId:I

    .line 79
    iput-object p4, p0, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;->mLabel:Ljava/lang/String;

    .line 81
    return-void
.end method

.method public static getCategory(I)Lcom/sec/android/app/myfiles/element/HistoryItem$Category;
    .locals 3
    .param p0, "id"    # I

    .prologue
    .line 87
    packed-switch p0, :pswitch_data_0

    .line 90
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "There is no ID of id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 88
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;->APPLICATION:Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    .line 89
    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;->FILE:Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    goto :goto_0

    .line 87
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/myfiles/element/HistoryItem$Category;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 71
    const-class v0, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/myfiles/element/HistoryItem$Category;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;->$VALUES:[Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    invoke-virtual {v0}, [Lcom/sec/android/app/myfiles/element/HistoryItem$Category;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    return-object v0
.end method


# virtual methods
.method public getId()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;->mId:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;->mLabel:Ljava/lang/String;

    return-object v0
.end method
