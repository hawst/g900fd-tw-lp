.class public Lcom/sec/android/app/myfiles/db/CacheDB;
.super Ljava/lang/Object;
.source "CacheDB.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/db/CacheDB$1;,
        Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;,
        Lcom/sec/android/app/myfiles/db/CacheDB$HistoryCacheCols;,
        Lcom/sec/android/app/myfiles/db/CacheDB$FTPShortcutsTableCols;,
        Lcom/sec/android/app/myfiles/db/CacheDB$FTPIsDir;,
        Lcom/sec/android/app/myfiles/db/CacheDB$FTPFileCacheCols;,
        Lcom/sec/android/app/myfiles/db/CacheDB$FTPBufferCols;,
        Lcom/sec/android/app/myfiles/db/CacheDB$FTPCachedCols;,
        Lcom/sec/android/app/myfiles/db/CacheDB$FTPTableCols;,
        Lcom/sec/android/app/myfiles/db/CacheDB$NearyBy_Table_column;
    }
.end annotation


# static fields
.field private static final DATABASE_NAME:Ljava/lang/String; = "CacheDB.db"

.field private static final DATABASE_VERSION:I = 0x10

.field private static final MODULE:Ljava/lang/String;

.field private static final QUERY_DEEP:I = 0x3de

.field private static final TABLE_CLOUD:Ljava/lang/String; = "cloud_dropbox"

.field private static final TABLE_CLOUD_DROPBOX_CURSOR:Ljava/lang/String; = "cloud_dropbox_cursor"

.field private static final TABLE_FTP_BUFFER:Ljava/lang/String; = "FTP_buffer"

.field private static final TABLE_FTP_CACHED:Ljava/lang/String; = "FTP_cache"

.field private static final TABLE_FTP_CONTENT:Ljava/lang/String; = "FTP_content"

.field private static final TABLE_FTP_FILE_CACHE:Ljava/lang/String; = "FTP_file_cache"

.field private static final TABLE_HISTORY_CACHE:Ljava/lang/String; = "history_cache"

.field private static final TABLE_NEARBY_CONTENTS:Ljava/lang/String; = "NearyBy_contents"

.field private static final TABLE_NEARBY_DEVICE_ID:Ljava/lang/String; = "NearyBy_deviceID"

.field private static db:Landroid/database/sqlite/SQLiteDatabase;


# instance fields
.field private DBHelper:Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;

.field private mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

.field private final mcontext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 2801
    const-class v0, Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->MODULE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184
    iput-object p1, p0, Lcom/sec/android/app/myfiles/db/CacheDB;->mcontext:Landroid/content/Context;

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/myfiles/db/CacheDB;->mcontext:Landroid/content/Context;

    const-string v1, "CacheDB.db"

    const/4 v2, 0x0

    const/16 v3, 0x10

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;->getInstance(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/db/CacheDB;->DBHelper:Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/myfiles/db/CacheDB;->mcontext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/db/CacheDB;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    .line 195
    return-void
.end method

.method public static finalClose()V
    .locals 3

    .prologue
    .line 253
    const/4 v0, 0x1

    sget-object v1, Lcom/sec/android/app/myfiles/db/CacheDB;->MODULE:Ljava/lang/String;

    const-string v2, "finalClose"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 255
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    .line 257
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 265
    :cond_0
    return-void
.end method

.method private static getExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 2385
    if-nez p0, :cond_1

    .line 2399
    :cond_0
    :goto_0
    return-object v1

    .line 2391
    :cond_1
    const/16 v2, 0x2e

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 2393
    .local v0, "lastDot":I
    if-ltz v0, :cond_0

    .line 2399
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static getFTPSortByString(II)Ljava/lang/String;
    .locals 2
    .param p0, "sortBy"    # I
    .param p1, "order"    # I

    .prologue
    .line 2169
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2171
    .local v0, "sortByStr":Ljava/lang/StringBuilder;
    packed-switch p0, :pswitch_data_0

    .line 2186
    :goto_0
    const/4 v1, 0x1

    if-eq p0, v1, :cond_2

    .line 2187
    if-nez p1, :cond_1

    const-string v1, " ASC"

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2191
    :goto_2
    const/4 v1, 0x2

    if-eq p0, v1, :cond_0

    .line 2192
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2193
    const-string v1, "title"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2194
    const-string v1, " COLLATE LOCALIZED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2197
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 2173
    :pswitch_0
    const-string v1, "date"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2176
    :pswitch_1
    const-string v1, "file_size"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2179
    :pswitch_2
    const-string v1, "file_type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2182
    :pswitch_3
    const-string v1, "title"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2183
    const-string v1, " COLLATE LOCALIZED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2187
    :cond_1
    const-string v1, " DESC"

    goto :goto_1

    .line 2189
    :cond_2
    if-nez p1, :cond_3

    const-string v1, " DESC"

    :goto_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_3
    const-string v1, " ASC"

    goto :goto_3

    .line 2171
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method private static getFTPWhereClause(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 2251
    .local p0, "exts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 2252
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2253
    .local v1, "sb":Ljava/lang/StringBuilder;
    const-string v2, "is_directory"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2254
    const-string v2, " = 1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2255
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2256
    const-string v2, " OR "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2257
    const-string v2, "extension"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2258
    const-string v2, " = \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2259
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2260
    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2255
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2262
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2264
    .end local v0    # "i":I
    .end local v1    # "sb":Ljava/lang/StringBuilder;
    :goto_1
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private static getHistorySortByString(II)Ljava/lang/String;
    .locals 4
    .param p0, "sortBy"    # I
    .param p1, "orderBy"    # I

    .prologue
    const/4 v3, 0x2

    .line 2202
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2204
    .local v0, "sb":Ljava/lang/StringBuilder;
    packed-switch p0, :pswitch_data_0

    .line 2222
    :goto_0
    if-ne p0, v3, :cond_0

    .line 2223
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_MyFiles_EnablePinyinSort"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2224
    const-string v1, " COLLATE LOCALIZED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2230
    :cond_0
    :goto_1
    if-nez p1, :cond_4

    const-string v1, " ASC"

    :goto_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2232
    if-eq p0, v3, :cond_1

    .line 2234
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2236
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_MyFiles_EnablePinyinSort"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2237
    const-string v1, "_name_for_pinyin_sort"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2242
    :goto_3
    const-string v1, " COLLATE LOCALIZED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2246
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 2206
    :pswitch_0
    const-string v1, "_date"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2207
    :pswitch_1
    const-string v1, "_size"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2208
    :pswitch_2
    const-string v1, "_type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2210
    :pswitch_3
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_MyFiles_EnablePinyinSort"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2211
    const-string v1, "_name_for_pinyin_sort"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2214
    :cond_2
    const-string v1, "_name"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2226
    :cond_3
    const-string v1, " COLLATE NOCASE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2230
    :cond_4
    const-string v1, " DESC"

    goto :goto_2

    .line 2239
    :cond_5
    const-string v1, "_name"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 2204
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method private getSortByStr(II)Ljava/lang/String;
    .locals 2
    .param p1, "sortBy"    # I
    .param p2, "order"    # I

    .prologue
    .line 1050
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1051
    .local v0, "sortByStr":Ljava/lang/StringBuilder;
    packed-switch p1, :pswitch_data_0

    .line 1067
    :goto_0
    if-nez p2, :cond_0

    const-string v1, " ASC"

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1069
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 1053
    :pswitch_0
    const-string v1, "Date"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1056
    :pswitch_1
    const-string v1, "FileSize"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1059
    :pswitch_2
    const-string v1, "Extension"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1060
    const-string v1, " COLLATE LOCALIZED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1063
    :pswitch_3
    const-string v1, "title"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1064
    const-string v1, " COLLATE LOCALIZED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1067
    :cond_0
    const-string v1, " DESC"

    goto :goto_1

    .line 1051
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method private static getTitleFromPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 2270
    if-eqz p0, :cond_0

    .line 2272
    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2276
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private insertIntoFtpCached(Ljava/lang/String;Ljava/lang/String;)J
    .locals 2
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 2304
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->insertIntoFtpCached(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private insertIntoFtpCached(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 4
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "timestamp"    # Ljava/lang/String;

    .prologue
    .line 2310
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2312
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v1, "server"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2314
    const-string v1, "path"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2316
    if-eqz p3, :cond_0

    .line 2318
    const-string v1, "timestamp"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2326
    :goto_0
    sget-object v1, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "FTP_cache"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    return-wide v2

    .line 2322
    :cond_0
    const-string v1, "timestamp"

    const-string v2, "no_tstmp"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private insertIntoFtpContent(Ljava/lang/String;Ljava/lang/String;IIJJJLjava/lang/String;)J
    .locals 13
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "fullPath"    # Ljava/lang/String;
    .param p3, "fileType"    # I
    .param p4, "isDirectory"    # I
    .param p5, "fileSize"    # J
    .param p7, "cachedId"    # J
    .param p9, "date"    # J
    .param p11, "extension"    # Ljava/lang/String;

    .prologue
    .line 2333
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2335
    .local v2, "cv":Landroid/content/ContentValues;
    if-eqz p1, :cond_0

    .line 2337
    const-string v3, "title"

    invoke-virtual {v2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2341
    :cond_0
    if-eqz p2, :cond_1

    .line 2343
    const-string v3, "_data"

    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2347
    :cond_1
    const-string v3, "file_type"

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v2, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2349
    const-string v3, "is_directory"

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v2, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2351
    const-string v3, "file_size"

    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v2, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2353
    const-string v3, "cached"

    invoke-static/range {p7 .. p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v2, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2355
    const-string v3, "date"

    invoke-static/range {p9 .. p10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v2, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2357
    if-eqz p11, :cond_2

    .line 2359
    const-string v3, "extension"

    move-object/from16 v0, p11

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2363
    :cond_2
    sget-object v3, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v8, "FTP_content"

    const/4 v9, 0x0

    invoke-virtual {v3, v8, v9, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v6

    .line 2365
    .local v6, "res":J
    const-wide/16 v8, -0x1

    cmp-long v3, v6, v8

    if-nez v3, :cond_3

    .line 2367
    sget-object v3, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v8, "FTP_content"

    const-string v9, "cached = ? AND title = ?"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-static/range {p7 .. p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object p1, v10, v11

    invoke-virtual {v3, v8, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    int-to-long v4, v3

    .line 2372
    .local v4, "deleteRes":J
    const/4 v3, 0x2

    sget-object v8, Lcom/sec/android/app/myfiles/db/CacheDB;->MODULE:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Cannot insert the record ("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-wide/from16 v0, p7

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") deletingRes: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v3, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 2375
    sget-object v3, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v8, "FTP_content"

    const/4 v9, 0x0

    invoke-virtual {v3, v8, v9, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v6

    .line 2379
    .end local v4    # "deleteRes":J
    :cond_3
    return-wide v6
.end method

.method private moveFtpFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 18
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "from"    # Ljava/lang/String;
    .param p3, "to"    # Ljava/lang/String;

    .prologue
    .line 1813
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1815
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->checkFtpFileExist(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1873
    :cond_0
    :goto_0
    return-void

    .line 1819
    :cond_1
    invoke-static/range {p3 .. p3}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getParentDir(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpPathcachedId(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v16

    .line 1821
    .local v16, "targetCachedId":J
    const-wide/16 v2, 0x0

    cmp-long v2, v16, v2

    if-ltz v2, :cond_0

    .line 1823
    sget-object v2, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "FTP_content INNER JOIN FTP_cache"

    const/4 v4, 0x0

    const-string v5, "FTP_content.cached = FTP_cache.id AND FTP_cache.server = ? AND FTP_content._data = ?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    aput-object p2, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 1829
    .local v11, "contentCr":Landroid/database/Cursor;
    const/4 v12, 0x0

    .line 1830
    .local v12, "cursor":Landroid/database/Cursor;
    if-eqz v11, :cond_0

    .line 1834
    :try_start_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1836
    const-string v2, "_id"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 1838
    .local v14, "id":J
    new-instance v13, Landroid/content/ContentValues;

    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 1840
    .local v13, "cv":Landroid/content/ContentValues;
    const-string v2, "_data"

    move-object/from16 v0, p3

    invoke-virtual {v13, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1841
    const-string v2, "cached"

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v13, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1843
    sget-object v2, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "FTP_content"

    const/4 v4, 0x0

    const-string v5, "_id = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 1845
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1846
    const-string v2, "title"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static/range {p3 .. p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->getTitleFromPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1848
    const-string v2, "title"

    invoke-static/range {p3 .. p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->getTitleFromPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v13, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1852
    :cond_2
    sget-object v2, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "FTP_content"

    const-string v4, "_id = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v13, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1861
    .end local v13    # "cv":Landroid/content/ContentValues;
    .end local v14    # "id":J
    :cond_3
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1862
    if-eqz v12, :cond_0

    .line 1863
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1861
    :catchall_0
    move-exception v2

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1862
    if-eqz v12, :cond_4

    .line 1863
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v2
.end method

.method private renameFtpFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 16
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "from"    # Ljava/lang/String;
    .param p3, "to"    # Ljava/lang/String;

    .prologue
    .line 1995
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1997
    sget-object v2, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "FTP_content INNER JOIN FTP_cache"

    const/4 v4, 0x0

    const-string v5, "FTP_content.cached = FTP_cache.id AND FTP_cache.server = ? AND FTP_content._data = ?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    aput-object p2, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 2004
    .local v11, "contentCr":Landroid/database/Cursor;
    if-eqz v11, :cond_0

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2006
    const-string v2, "_id"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 2008
    .local v14, "id":J
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 2010
    .local v12, "cv":Landroid/content/ContentValues;
    const-string v2, "title"

    invoke-static/range {p3 .. p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->getTitleFromPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2012
    const-string v2, "_data"

    move-object/from16 v0, p3

    invoke-virtual {v12, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2014
    sget-object v2, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "FTP_content"

    const-string v4, "_id = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v12, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2021
    .end local v12    # "cv":Landroid/content/ContentValues;
    .end local v14    # "id":J
    :cond_0
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 2025
    .end local v11    # "contentCr":Landroid/database/Cursor;
    :cond_1
    return-void
.end method

.method private renameOrMoveFtpFolder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 24
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "from"    # Ljava/lang/String;
    .param p3, "to"    # Ljava/lang/String;
    .param p4, "move"    # Z

    .prologue
    .line 1877
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 1880
    sget-object v4, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "FTP_cache"

    const/4 v6, 0x0

    const-string v7, "server = ? AND path LIKE ?"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    const/4 v9, 0x1

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "%"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v4 .. v12}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 1885
    .local v13, "cachedCr":Landroid/database/Cursor;
    if-eqz v13, :cond_3

    .line 1887
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1889
    const-string v4, "id"

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 1891
    .local v14, "cachedIdIndex":I
    const-string v4, "path"

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 1895
    .local v15, "cachedPathIndex":I
    :cond_0
    invoke-interface {v13, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 1897
    .local v22, "id":J
    invoke-interface {v13, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 1899
    .local v21, "path":Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1901
    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    .line 1903
    .local v19, "cv":Landroid/content/ContentValues;
    const-string v4, "path"

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1906
    :try_start_0
    sget-object v4, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "FTP_cache"

    const-string v6, "id = ?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    move-object/from16 v0, v19

    invoke-virtual {v4, v5, v0, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1917
    .end local v19    # "cv":Landroid/content/ContentValues;
    :cond_1
    :goto_0
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1920
    .end local v14    # "cachedIdIndex":I
    .end local v15    # "cachedPathIndex":I
    .end local v21    # "path":Ljava/lang/String;
    .end local v22    # "id":J
    :cond_2
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 1925
    :cond_3
    sget-object v4, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "FTP_content INNER JOIN FTP_cache"

    const/4 v6, 0x0

    const-string v7, "FTP_content.cached = FTP_cache.id AND FTP_cache.server = ? AND FTP_content._data LIKE ?"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    const/4 v9, 0x1

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "%"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v4 .. v12}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 1933
    .local v16, "contentCr":Landroid/database/Cursor;
    if-nez v16, :cond_5

    .line 1991
    .end local v13    # "cachedCr":Landroid/database/Cursor;
    .end local v16    # "contentCr":Landroid/database/Cursor;
    :cond_4
    :goto_1
    return-void

    .line 1911
    .restart local v13    # "cachedCr":Landroid/database/Cursor;
    .restart local v14    # "cachedIdIndex":I
    .restart local v15    # "cachedPathIndex":I
    .restart local v19    # "cv":Landroid/content/ContentValues;
    .restart local v21    # "path":Ljava/lang/String;
    .restart local v22    # "id":J
    :catch_0
    move-exception v20

    .line 1912
    .local v20, "e":Ljava/lang/RuntimeException;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_0

    .line 1939
    .end local v14    # "cachedIdIndex":I
    .end local v15    # "cachedPathIndex":I
    .end local v19    # "cv":Landroid/content/ContentValues;
    .end local v20    # "e":Ljava/lang/RuntimeException;
    .end local v21    # "path":Ljava/lang/String;
    .end local v22    # "id":J
    .restart local v16    # "contentCr":Landroid/database/Cursor;
    :cond_5
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_6

    .line 1941
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 1946
    :cond_6
    if-eqz v16, :cond_4

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1948
    const-string v4, "_id"

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 1950
    .local v17, "contentIdIndex":I
    const-string v4, "_data"

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 1954
    .local v18, "contentPathIndex":I
    :cond_7
    invoke-interface/range {v16 .. v17}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 1956
    .restart local v22    # "id":J
    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 1958
    .restart local v21    # "path":Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_a

    .line 1960
    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    .line 1962
    .restart local v19    # "cv":Landroid/content/ContentValues;
    const-string v4, "_data"

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1964
    move-object/from16 v0, v21

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1966
    if-eqz p4, :cond_8

    .line 1968
    const-string v4, "cached"

    invoke-static/range {p3 .. p3}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getParentDir(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v5}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpPathcachedId(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1972
    :cond_8
    const-string v4, "title"

    invoke-static/range {p3 .. p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->getTitleFromPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1976
    :cond_9
    sget-object v4, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "FTP_content"

    const-string v6, "_id = ?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    move-object/from16 v0, v19

    invoke-virtual {v4, v5, v0, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1983
    .end local v19    # "cv":Landroid/content/ContentValues;
    :cond_a
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_7

    .line 1985
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1
.end method


# virtual methods
.method public appendHistoryCache(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/element/HistoryItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2565
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/element/HistoryItem;>;"
    if-eqz p1, :cond_2

    .line 2567
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/element/HistoryItem;

    .line 2572
    .local v2, "item":Lcom/sec/android/app/myfiles/element/HistoryItem;
    :try_start_0
    sget-object v3, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v3, :cond_1

    sget-object v3, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2574
    sget-object v3, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "history_cache"

    const/4 v5, 0x0

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getContentValues()Landroid/content/ContentValues;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2582
    :catch_0
    move-exception v0

    .line 2584
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 2576
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :cond_1
    :try_start_1
    sget-object v3, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2578
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 2579
    sget-object v3, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "history_cache"

    const/4 v5, 0x0

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getContentValues()Landroid/content/ContentValues;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 2591
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "item":Lcom/sec/android/app/myfiles/element/HistoryItem;
    :cond_2
    return-void
.end method

.method public beginTransaction()V
    .locals 1

    .prologue
    .line 232
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 234
    return-void
.end method

.method public cacheFtpFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "fileFullPath"    # Ljava/lang/String;

    .prologue
    .line 2405
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2407
    invoke-static {p2}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getParentDir(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2409
    .local v3, "parent":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2411
    invoke-virtual {p0, p1, v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpPathcachedId(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    .line 2413
    .local v0, "cachedId":J
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpSizeMdtmPair(Ljava/lang/String;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v4

    .line 2415
    .local v4, "sizeMdtmPair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    const-wide/16 v6, 0x0

    cmp-long v5, v0, v6

    if-ltz v5, :cond_0

    .line 2417
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2419
    .local v2, "cv":Landroid/content/ContentValues;
    const-string v5, "filename"

    invoke-virtual {v2, v5, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2421
    const-string v5, "cached"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2423
    const-string v6, "size"

    iget-object v5, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v2, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2425
    const-string v6, "date"

    iget-object v5, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v2, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2427
    sget-object v5, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v6, "FTP_file_cache"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 2435
    .end local v0    # "cachedId":J
    .end local v2    # "cv":Landroid/content/ContentValues;
    .end local v3    # "parent":Ljava/lang/String;
    .end local v4    # "sizeMdtmPair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    :cond_0
    return-void
.end method

.method public checkDeviceID(Ljava/lang/String;)Z
    .locals 12
    .param p1, "deviceID"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 848
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 849
    .local v9, "newNode":Landroid/content/ContentValues;
    const-string v0, "deviceID"

    invoke-virtual {v9, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 851
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "NearyBy_deviceID"

    const-string v3, "deviceID = ?"

    new-array v4, v11, [Ljava/lang/String;

    aput-object p1, v4, v10

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 855
    .local v8, "cr":Landroid/database/Cursor;
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eq v0, v11, :cond_0

    .line 856
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move v0, v10

    .line 860
    :goto_0
    return v0

    .line 859
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move v0, v11

    .line 860
    goto :goto_0
.end method

.method public checkFtpFileExist(Ljava/lang/String;)Z
    .locals 12
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 1797
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v10

    .line 1808
    :goto_0
    return v0

    .line 1800
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "FTP_content"

    const-string v3, "_data = ?"

    new-array v4, v10, [Ljava/lang/String;

    aput-object p1, v4, v11

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1801
    .local v8, "contentCr":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    .line 1802
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v9

    .line 1803
    .local v9, "count":I
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1804
    if-lez v9, :cond_1

    move v0, v10

    .line 1805
    goto :goto_0

    .end local v9    # "count":I
    :cond_1
    move v0, v11

    .line 1808
    goto :goto_0
.end method

.method public cleanCacheForCachedItem(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 1680
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 1682
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpPathCachedId(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    .line 1684
    .local v0, "id":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    .line 1686
    sget-object v2, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "FTP_content"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cached = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1688
    sget-object v2, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "FTP_cache"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "id = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1694
    .end local v0    # "id":J
    :cond_0
    return-void
.end method

.method public clearCloudData()J
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 324
    const-string v0, ""

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteThumbnail(Ljava/lang/String;)V

    .line 327
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    new-instance v0, Ljava/io/File;

    const-string v1, "/storage/sdcard0/Android/data/com.baidu.netdisk/files/scratch"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteDropboxThumbnailCache(Ljava/io/File;)V

    .line 333
    :goto_0
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "cloud_dropbox_cursor"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 334
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "cloud_dropbox"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    return-wide v0

    .line 330
    :cond_0
    new-instance v0, Ljava/io/File;

    const-string v1, "/storage/sdcard0/Android/data/com.dropbox.android/files/scratch"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteDropboxThumbnailCache(Ljava/io/File;)V

    goto :goto_0
.end method

.method public clearFTPData()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1452
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "FTP_buffer"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1454
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "FTP_content"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1456
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "FTP_cache"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1458
    return-void
.end method

.method public clearFtpBuffer()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1462
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "FTP_buffer"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1464
    return-void
.end method

.method public clearNearByData()J
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 940
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "NearyBy_deviceID"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 941
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "NearyBy_contents"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public close()V
    .locals 0

    .prologue
    .line 249
    return-void
.end method

.method public deleteCloudData(Lcom/samsung/scloud/data/SCloudNode;)I
    .locals 6
    .param p1, "node"    # Lcom/samsung/scloud/data/SCloudNode;

    .prologue
    const/4 v2, 0x0

    .line 294
    if-nez p1, :cond_0

    .line 299
    :goto_0
    return v2

    .line 296
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/scloud/data/SCloudNode;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 297
    .local v0, "absolutePath":Ljava/lang/String;
    const-string v1, "_data = ?   COLLATE NOCASE "

    .line 298
    .local v1, "query":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteThumbnail(Ljava/lang/String;)V

    .line 299
    sget-object v3, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "cloud_dropbox"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    aput-object v0, v5, v2

    invoke-virtual {v3, v4, v1, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    goto :goto_0
.end method

.method public deleteCloudData(Ljava/lang/String;)I
    .locals 8
    .param p1, "ServerPath"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 303
    if-nez p1, :cond_0

    move v2, v3

    .line 320
    :goto_0
    return v2

    .line 305
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteThumbnail(Ljava/lang/String;)V

    .line 306
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/db/CacheDB;->isDirectory(Ljava/lang/String;)Z

    move-result v0

    .line 307
    .local v0, "isDirectory":Z
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 308
    const/4 v2, -0x1

    .line 309
    .local v2, "ret":I
    if-eqz v0, :cond_1

    .line 310
    sget-object v3, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "cloud_dropbox"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_data LIKE \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->shadeQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' OR _data LIKE \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->shadeQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/%\' "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 319
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    goto :goto_0

    .line 312
    :cond_1
    const-string v4, "\'"

    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_2

    .line 313
    const-string v1, "_data = ? "

    .line 314
    .local v1, "query":Ljava/lang/String;
    sget-object v4, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "cloud_dropbox"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    aput-object p1, v6, v3

    invoke-virtual {v4, v5, v1, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 315
    goto :goto_1

    .line 316
    .end local v1    # "query":Ljava/lang/String;
    :cond_2
    sget-object v3, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "cloud_dropbox"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_data LIKE \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->shadeQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    goto :goto_1
.end method

.method public deleteFtpCachedData(Ljava/lang/String;)V
    .locals 7
    .param p1, "server"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 2282
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2284
    new-array v2, v6, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    .line 2286
    .local v2, "whereArgs":[Ljava/lang/String;
    sget-object v3, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "FTP_content"

    const-string v5, "cached IN ( SELECT id FROM FTP_cache WHERE server = ?)"

    invoke-virtual {v3, v4, v5, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 2291
    .local v1, "contentItemsDeleted":I
    sget-object v3, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "FTP_cache"

    const-string v5, "server = ?"

    invoke-virtual {v3, v4, v5, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 2296
    .local v0, "cachedItemsDeleted":I
    sget-object v3, Lcom/sec/android/app/myfiles/db/CacheDB;->MODULE:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deleteCachedData content("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "), cached("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 2300
    .end local v0    # "cachedItemsDeleted":I
    .end local v1    # "contentItemsDeleted":I
    .end local v2    # "whereArgs":[Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public deleteFtpFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 2052
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2054
    invoke-static {p2}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getParentDir(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpPathCachedId(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    .line 2056
    .local v0, "cachedId":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    .line 2058
    sget-object v2, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "FTP_content"

    const-string v4, "cached = ? AND _data = ?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object p2, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2066
    .end local v0    # "cachedId":J
    :cond_0
    return-void
.end method

.method public deleteFtpFileFromCache(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "fileFullPath"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x2

    .line 2439
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2441
    sget-object v1, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "FTP_file_cache"

    const-string v3, "cached IN ( SELECT id FROM FTP_cache WHERE server = ? AND path = ?) AND filename = ?"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    invoke-static {p2}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getParentDir(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    aput-object p2, v4, v7

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 2450
    .local v0, "itemsDeleted":I
    sget-object v1, Lcom/sec/android/app/myfiles/db/CacheDB;->MODULE:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "items deleted from FTP_file_cache: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v7, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 2454
    .end local v0    # "itemsDeleted":I
    :cond_0
    return-void
.end method

.method public deleteFtpFolder(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2029
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2032
    sget-object v2, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "FTP_cache"

    const-string v4, "server = ? AND path = ?"

    new-array v5, v6, [Ljava/lang/String;

    aput-object p1, v5, v7

    aput-object p2, v5, v8

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2036
    invoke-static {p2}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getParentDir(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpPathCachedId(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    .line 2038
    .local v0, "parentCachedId":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    .line 2040
    sget-object v2, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "FTP_content"

    const-string v4, "cached = ? AND _data = ?"

    new-array v5, v6, [Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    aput-object p2, v5, v8

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2048
    .end local v0    # "parentCachedId":J
    :cond_0
    return-void
.end method

.method public deleteNearByData(Lcom/samsung/android/allshare/Item;)I
    .locals 4
    .param p1, "item"    # Lcom/samsung/android/allshare/Item;

    .prologue
    .line 934
    if-nez p1, :cond_0

    .line 935
    const/4 v0, 0x0

    .line 937
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "NearyBy_contents"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_data = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public endTransaction()V
    .locals 1

    .prologue
    .line 242
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 243
    return-void
.end method

.method public getCloudChildFileCount(Ljava/lang/String;I)I
    .locals 3
    .param p1, "dirpath"    # Ljava/lang/String;
    .param p2, "depth"    # I

    .prologue
    .line 494
    const/4 v0, 0x0

    .line 495
    .local v0, "Count":I
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 497
    add-int/lit8 v2, p2, 0x1

    invoke-virtual {p0, p1, v2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getCloudDirList(Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v1

    .line 498
    .local v1, "cr":Landroid/database/Cursor;
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 500
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 501
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 503
    return v0
.end method

.method public getCloudDirList(Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 4
    .param p1, "dirpath"    # Ljava/lang/String;
    .param p2, "depth"    # I

    .prologue
    .line 341
    const/4 v2, 0x1

    if-ne p2, v2, :cond_0

    .line 343
    const-string v1, " select * from cloud_dropbox where ( length(_data) - length( replace(_data,\'/\',\'\')) = 1 ) "

    .line 362
    .local v1, "sql":Ljava/lang/String;
    :goto_0
    sget-object v2, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 364
    .local v0, "cr":Landroid/database/Cursor;
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 366
    return-object v0

    .line 350
    .end local v0    # "cr":Landroid/database/Cursor;
    .end local v1    # "sql":Ljava/lang/String;
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " select * from  cloud_dropbox where ( length(_data) - length( replace(_data,\'/\',\'\')) = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ) "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND _data like "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->shadeQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/%\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "sql":Ljava/lang/String;
    goto :goto_0
.end method

.method public getCloudDirList(Ljava/lang/String;III)Landroid/database/Cursor;
    .locals 6
    .param p1, "dirpath"    # Ljava/lang/String;
    .param p2, "depth"    # I
    .param p3, "sortBy"    # I
    .param p4, "orderBy"    # I

    .prologue
    .line 372
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 373
    .local v1, "sortByStr":Ljava/lang/StringBuilder;
    packed-switch p3, :pswitch_data_0

    .line 388
    :goto_0
    if-nez p4, :cond_1

    const-string v3, " ASC"

    :goto_1
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 392
    const/4 v3, 0x1

    if-ne p2, v3, :cond_2

    .line 394
    const-string v2, " select * from cloud_dropbox where ( length(_data) - length( replace(_data,\'/\',\'\')) = 1 ) "

    .line 413
    .local v2, "sql":Ljava/lang/String;
    :goto_2
    sget-object v3, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " order by "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 415
    .local v0, "cr":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 416
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 418
    :cond_0
    return-object v0

    .line 375
    .end local v0    # "cr":Landroid/database/Cursor;
    .end local v2    # "sql":Ljava/lang/String;
    :pswitch_0
    const-string v3, "date_modified"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 378
    :pswitch_1
    const-string v3, "_size"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 381
    :pswitch_2
    const-string v3, "mime_type"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 384
    :pswitch_3
    const-string v3, "title"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 385
    const-string v3, " COLLATE LOCALIZED"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 388
    :cond_1
    const-string v3, " DESC"

    goto :goto_1

    .line 401
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " select * from  cloud_dropbox where ( length(_data) - length( replace(_data,\'/\',\'\')) = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ) "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND _data like "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->shadeQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/%\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "sql":Ljava/lang/String;
    goto :goto_2

    .line 373
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public getCloudDirListByFilter(Ljava/lang/String;IIILjava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "dirpath"    # Ljava/lang/String;
    .param p2, "depth"    # I
    .param p3, "sortBy"    # I
    .param p4, "orderBy"    # I
    .param p5, "where"    # Ljava/lang/String;
    .param p6, "whereArgs"    # [Ljava/lang/String;

    .prologue
    .line 426
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 427
    .local v1, "sortByStr":Ljava/lang/StringBuilder;
    packed-switch p3, :pswitch_data_0

    .line 442
    :goto_0
    if-nez p4, :cond_1

    const-string v3, " ASC"

    :goto_1
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 446
    const/4 v3, 0x1

    if-ne p2, v3, :cond_2

    .line 448
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " select * from cloud_dropbox where ( length(_data) - length( replace(_data,\'/\',\'\')) = 1 )  AND  ( "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ) "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 475
    .local v2, "sql":Ljava/lang/String;
    :goto_2
    sget-object v3, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " order by "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, p6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 477
    .local v0, "cr":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 478
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 480
    :cond_0
    return-object v0

    .line 429
    .end local v0    # "cr":Landroid/database/Cursor;
    .end local v2    # "sql":Ljava/lang/String;
    :pswitch_0
    const-string v3, "date_modified"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 432
    :pswitch_1
    const-string v3, "_size"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 435
    :pswitch_2
    const-string v3, "mime_type"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 438
    :pswitch_3
    const-string v3, "title"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 439
    const-string v3, " COLLATE LOCALIZED"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 442
    :cond_1
    const-string v3, " DESC"

    goto :goto_1

    .line 459
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " select * from  cloud_dropbox where ( length(_data) - length( replace(_data,\'/\',\'\')) = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ) "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND _data like "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->shadeQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/%\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ( "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ) "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "sql":Ljava/lang/String;
    goto/16 :goto_2

    .line 427
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public getCloudFileSize(Ljava/lang/String;)J
    .locals 6
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 663
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_2

    .line 664
    :cond_0
    const-wide/16 v2, 0x0

    .line 677
    :cond_1
    :goto_0
    return-wide v2

    .line 665
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 666
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SELECT _size FROM cloud_dropbox WHERE _data LIKE \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->shadeQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 667
    .local v1, "sql":Ljava/lang/String;
    sget-object v4, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v5, 0x0

    invoke-virtual {v4, v1, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 668
    .local v0, "cr":Landroid/database/Cursor;
    const-wide/16 v2, 0x0

    .line 669
    .local v2, "size":J
    if-eqz v0, :cond_1

    .line 670
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_3

    .line 671
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 672
    const/4 v4, 0x0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 674
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public getCloudFormats(Ljava/util/ArrayList;)Ljava/util/HashMap;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v10, 0x0

    .line 540
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-gtz v9, :cond_2

    :cond_0
    move-object v6, v10

    .line 598
    :cond_1
    :goto_0
    return-object v6

    .line 544
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 546
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 548
    .local v6, "ret":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 550
    .local v7, "sb":Ljava/lang/StringBuilder;
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    div-int/lit16 v9, v9, 0x3df

    add-int/lit8 v9, v9, 0x1

    if-ge v4, v9, :cond_1

    .line 552
    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 554
    const-string v9, " WHERE "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 556
    add-int/lit8 v9, v4, 0x1

    mul-int/lit16 v9, v9, 0x3de

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-le v9, v11, :cond_4

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 558
    .local v0, "ceil":I
    :goto_2
    mul-int/lit16 v3, v4, 0x3de

    .local v3, "i":I
    :goto_3
    if-ge v3, v0, :cond_5

    .line 560
    mul-int/lit16 v9, v4, 0x3de

    if-le v3, v9, :cond_3

    .line 562
    const-string v9, " OR "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564
    :cond_3
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "_data LIKE \'"

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v9}, Lcom/sec/android/app/myfiles/utils/Utils;->shadeQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v11, "\'"

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 558
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 556
    .end local v0    # "ceil":I
    .end local v3    # "i":I
    :cond_4
    add-int/lit8 v9, v4, 0x1

    mul-int/lit16 v0, v9, 0x3de

    goto :goto_2

    .line 568
    .restart local v0    # "ceil":I
    .restart local v3    # "i":I
    :cond_5
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "SELECT _data,format FROM cloud_dropbox"

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 570
    .local v8, "sql":Ljava/lang/String;
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v9, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 572
    sget-object v9, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v9, v8, v10}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 574
    .local v1, "cr":Landroid/database/Cursor;
    if-eqz v1, :cond_6

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v9

    if-nez v9, :cond_8

    .line 576
    :cond_6
    if-eqz v1, :cond_7

    .line 578
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_7
    move-object v6, v10

    .line 580
    goto/16 :goto_0

    .line 583
    :cond_8
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 587
    :cond_9
    const-string v9, "_data"

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 589
    .local v5, "name":Ljava/lang/String;
    const-string v9, "format"

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 591
    .local v2, "format":I
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v5, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 593
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v9

    if-nez v9, :cond_9

    .line 595
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 550
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1
.end method

.method public getCloudPathItemList(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 4
    .param p1, "dirpath"    # Ljava/lang/String;

    .prologue
    .line 485
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 486
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SELECT * FROM  cloud_dropbox WHERE _data LIKE \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->shadeQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/%\' ORDER BY "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_data"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 487
    .local v1, "sql":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 488
    .local v0, "cr":Landroid/database/Cursor;
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 489
    return-object v0
.end method

.method public getCloudPathSizes(Ljava/util/ArrayList;)J
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .local p1, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v12, 0x0

    .line 603
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-gtz v9, :cond_2

    .line 605
    :cond_0
    const-wide/16 v6, 0x0

    .line 658
    :cond_1
    return-wide v6

    .line 607
    :cond_2
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/db/CacheDB;->getCloudFormats(Ljava/util/ArrayList;)Ljava/util/HashMap;

    move-result-object v2

    .line 609
    .local v2, "formats":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 611
    const-wide/16 v6, 0x0

    .line 613
    .local v6, "retSize":J
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 615
    .local v5, "sb":Ljava/lang/StringBuilder;
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    div-int/lit16 v9, v9, 0x3df

    add-int/lit8 v9, v9, 0x1

    if-ge v4, v9, :cond_1

    .line 617
    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 619
    const-string v9, " WHERE "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 621
    add-int/lit8 v9, v4, 0x1

    mul-int/lit16 v9, v9, 0x3de

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-le v9, v10, :cond_4

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 623
    .local v0, "ceil":I
    :goto_1
    mul-int/lit16 v3, v4, 0x3de

    .local v3, "i":I
    :goto_2
    if-ge v3, v0, :cond_6

    .line 625
    mul-int/lit16 v9, v4, 0x3de

    if-le v3, v9, :cond_3

    .line 627
    const-string v9, " OR "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 629
    :cond_3
    if-eqz v2, :cond_5

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_5

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    const/16 v10, 0x3001

    if-ne v9, v10, :cond_5

    .line 631
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "_data LIKE \'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v9}, Lcom/sec/android/app/myfiles/utils/Utils;->shadeQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/%\'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 623
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 621
    .end local v0    # "ceil":I
    .end local v3    # "i":I
    :cond_4
    add-int/lit8 v9, v4, 0x1

    mul-int/lit16 v0, v9, 0x3de

    goto :goto_1

    .line 635
    .restart local v0    # "ceil":I
    .restart local v3    # "i":I
    :cond_5
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "_data LIKE \'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v9}, Lcom/sec/android/app/myfiles/utils/Utils;->shadeQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 639
    :cond_6
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "SELECT SUM(_size) FROM cloud_dropbox"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 642
    .local v8, "sql":Ljava/lang/String;
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v9, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 644
    sget-object v9, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v10, 0x0

    invoke-virtual {v9, v8, v10}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 646
    .local v1, "cr":Landroid/database/Cursor;
    if-eqz v1, :cond_7

    .line 648
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 650
    invoke-interface {v1, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    add-long/2addr v6, v10

    .line 652
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 615
    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0
.end method

.method public getCloudTotalSize()J
    .locals 8

    .prologue
    .line 509
    const-wide/16 v4, 0x0

    .line 513
    .local v4, "totalSize":J
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 515
    const-string v2, " select TOTAL(_size) from cloud_dropbox"

    .line 517
    .local v2, "sql":Ljava/lang/String;
    sget-object v3, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v6, 0x0

    invoke-virtual {v3, v2, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 519
    .local v0, "cr":Landroid/database/Cursor;
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 521
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 523
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 525
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-wide v6, v4

    .line 531
    .end local v0    # "cr":Landroid/database/Cursor;
    .end local v2    # "sql":Ljava/lang/String;
    :goto_0
    return-wide v6

    .line 529
    :catch_0
    move-exception v1

    .line 531
    .local v1, "e":Ljava/lang/Exception;
    const-wide/16 v6, 0x0

    goto :goto_0
.end method

.method public getDropBoxFilteredContent(Lcom/sec/android/app/myfiles/utils/SearchParameter;)Landroid/database/Cursor;
    .locals 22
    .param p1, "param"    # Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .prologue
    .line 702
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 703
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getKeyword()Ljava/lang/String;

    move-result-object v11

    .line 704
    .local v11, "searchKeyword":Ljava/lang/String;
    if-eqz v11, :cond_0

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v17

    if-nez v17, :cond_0

    .line 706
    const-string v11, "%"

    .line 708
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateFrom()J

    move-result-wide v18

    const-wide/16 v20, 0x3e8

    mul-long v4, v18, v20

    .line 709
    .local v4, "dateFrom":J
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateTo()J

    move-result-wide v18

    const-wide/16 v20, 0x3e8

    mul-long v6, v18, v20

    .line 710
    .local v6, "dateTo":J
    const/4 v8, 0x0

    .line 711
    .local v8, "extention":Ljava/lang/String;
    const/4 v12, 0x0

    .line 713
    .local v12, "skipFiletypeClause":Z
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getExtentionString()Ljava/lang/String;

    move-result-object v17

    if-eqz v17, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getExtentionString()Ljava/lang/String;

    move-result-object v17

    const-string v18, ""

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_1

    .line 714
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getExtentionString()Ljava/lang/String;

    move-result-object v8

    .line 716
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v17

    const/16 v18, 0x5

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    .line 717
    move-object v8, v11

    .line 718
    const/4 v11, 0x0

    .line 719
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "A."

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/myfiles/MediaFile;->isDrmFileType(I)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 720
    const/4 v12, 0x1

    .line 723
    :cond_2
    if-eqz v11, :cond_3

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v17

    if-nez v17, :cond_5

    :cond_3
    if-eqz v8, :cond_4

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v17

    if-nez v17, :cond_5

    :cond_4
    const-wide/16 v18, 0x0

    cmp-long v17, v4, v18

    if-nez v17, :cond_5

    const-wide/16 v18, 0x0

    cmp-long v17, v6, v18

    if-nez v17, :cond_5

    .line 724
    const/4 v3, 0x0

    .line 803
    :goto_0
    return-object v3

    .line 726
    :cond_5
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 727
    .local v16, "whereList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v11, :cond_6

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v17

    if-eqz v17, :cond_6

    .line 728
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "title LIKE \"%"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "%\""

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 731
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v17

    const/16 v18, 0x6

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_15

    .line 732
    if-eqz v8, :cond_8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v17

    if-eqz v17, :cond_8

    .line 733
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "A."

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/myfiles/MediaFile;->isDrmFileType(I)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 734
    const/4 v12, 0x1

    .line 735
    :cond_7
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "_data LIKE \"%."

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "\""

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 736
    const-string v17, "format != 12289"

    invoke-virtual/range {v16 .. v17}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 739
    :cond_8
    const-wide/16 v18, 0x0

    cmp-long v17, v4, v18

    if-eqz v17, :cond_9

    .line 740
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "date_modified > "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 742
    :cond_9
    const-wide/16 v18, 0x0

    cmp-long v17, v6, v18

    if-eqz v17, :cond_a

    .line 743
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "date_modified < "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 746
    :cond_a
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v17

    if-eqz v17, :cond_b

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v17

    sget-object v18, Lcom/sec/android/app/myfiles/utils/FileType;->IMAGE:Lcom/sec/android/app/myfiles/utils/FileType;

    sget-object v19, Lcom/sec/android/app/myfiles/utils/FileType;->DOCUMENT:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-static/range {v18 .. v19}, Ljava/util/EnumSet;->range(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/EnumSet;->containsAll(Ljava/util/Collection;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 747
    const/4 v12, 0x1

    .line 749
    :cond_b
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v17

    if-eqz v17, :cond_15

    if-nez v12, :cond_15

    .line 750
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 751
    .local v9, "fTypes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v17

    sget-object v18, Lcom/sec/android/app/myfiles/utils/FileType;->IMAGE:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual/range {v17 .. v18}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_c

    .line 752
    const-string v17, "mime_type LIKE \"%image%\""

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 754
    :cond_c
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v17

    sget-object v18, Lcom/sec/android/app/myfiles/utils/FileType;->AUDIO:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual/range {v17 .. v18}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_d

    .line 755
    const-string v17, "mime_type LIKE \"%audio%\""

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 757
    :cond_d
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v17

    sget-object v18, Lcom/sec/android/app/myfiles/utils/FileType;->VIDEO:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual/range {v17 .. v18}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_e

    .line 758
    const-string v17, "mime_type LIKE \"%video%\""

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 760
    :cond_e
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v17

    sget-object v18, Lcom/sec/android/app/myfiles/utils/FileType;->DOCUMENT:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual/range {v17 .. v18}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_11

    .line 761
    invoke-static {}, Lcom/sec/android/app/myfiles/MediaFile;->getDocumentExtensions()[Ljava/lang/String;

    move-result-object v15

    .line 762
    .local v15, "whereArgs":[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 763
    .local v2, "builder":Ljava/lang/StringBuilder;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    array-length v0, v15

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v10, v0, :cond_10

    .line 764
    if-lez v10, :cond_f

    .line 765
    const-string v17, " OR "

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 766
    :cond_f
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "_data LIKE \'%."

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    aget-object v18, v15, v10

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "\'"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 763
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 768
    :cond_10
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v17

    if-lez v17, :cond_11

    .line 769
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "("

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ")"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 771
    .end local v2    # "builder":Ljava/lang/StringBuilder;
    .end local v10    # "i":I
    .end local v15    # "whereArgs":[Ljava/lang/String;
    :cond_11
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/EnumSet;->size()I

    move-result v17

    const/16 v18, 0x5

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_12

    .line 772
    const-string v17, "mime_type LIKE \"%application%\""

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 774
    :cond_12
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 775
    .restart local v2    # "builder":Ljava/lang/StringBuilder;
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_2
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v10, v0, :cond_14

    .line 776
    if-lez v10, :cond_13

    .line 777
    const-string v17, " OR "

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 778
    :cond_13
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 775
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 780
    :cond_14
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v17

    if-lez v17, :cond_15

    .line 781
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "("

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ")"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 784
    .end local v2    # "builder":Ljava/lang/StringBuilder;
    .end local v9    # "fTypes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v10    # "i":I
    :cond_15
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 785
    .restart local v2    # "builder":Ljava/lang/StringBuilder;
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_3
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v10, v0, :cond_17

    .line 786
    if-lez v10, :cond_16

    .line 787
    const-string v17, " AND "

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 788
    :cond_16
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 785
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 790
    :cond_17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 791
    .local v14, "where":Ljava/lang/String;
    if-eqz v14, :cond_18

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v17

    if-nez v17, :cond_19

    .line 792
    :cond_18
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 793
    :cond_19
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "SELECT _id, _data, title, mime_type, _size, date_modified, format, 8 AS category FROM cloud_dropbox WHERE "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 802
    .local v13, "sql":Ljava/lang/String;
    sget-object v17, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v13, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 803
    .local v3, "cursor":Landroid/database/Cursor;
    goto/16 :goto_0
.end method

.method public getDropboxCursor()Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 2750
    const/4 v9, 0x0

    .line 2751
    .local v9, "cursor":Ljava/lang/String;
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "cloud_dropbox_cursor"

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 2752
    .local v8, "cr":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    .line 2753
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 2754
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2755
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 2757
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 2759
    :cond_1
    return-object v9
.end method

.method public getFTPList(II)Landroid/database/Cursor;
    .locals 9
    .param p1, "sortBy"    # I
    .param p2, "order"    # I

    .prologue
    const/4 v2, 0x0

    .line 1570
    const/4 v8, 0x0

    .line 1572
    .local v8, "cr":Landroid/database/Cursor;
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "FTP_content"

    invoke-static {p1, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFTPSortByString(II)Ljava/lang/String;

    move-result-object v7

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1576
    if-eqz v8, :cond_0

    .line 1578
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1582
    :cond_0
    return-object v8
.end method

.method public getFTPList(Ljava/util/ArrayList;II)Landroid/database/Cursor;
    .locals 9
    .param p2, "sortBy"    # I
    .param p3, "order"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;II)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .local p1, "exts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 1551
    const/4 v8, 0x0

    .line 1553
    .local v8, "cr":Landroid/database/Cursor;
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "FTP_content"

    invoke-static {p1}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFTPWhereClause(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFTPSortByString(II)Ljava/lang/String;

    move-result-object v7

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1558
    if-eqz v8, :cond_0

    .line 1560
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1564
    :cond_0
    return-object v8
.end method

.method public getFtpBufferContent(II)Landroid/database/Cursor;
    .locals 9
    .param p1, "sortBy"    # I
    .param p2, "order"    # I

    .prologue
    const/4 v2, 0x0

    .line 1468
    const/4 v8, 0x0

    .line 1470
    .local v8, "cr":Landroid/database/Cursor;
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "FTP_buffer"

    invoke-static {p1, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFTPSortByString(II)Ljava/lang/String;

    move-result-object v7

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1473
    if-eqz v8, :cond_0

    .line 1475
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1479
    :cond_0
    return-object v8
.end method

.method public getFtpBufferContent(IILjava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p1, "sortBy"    # I
    .param p2, "order"    # I
    .param p3, "server"    # Ljava/lang/String;
    .param p4, "path"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1503
    const/4 v8, 0x0

    .line 1505
    .local v8, "cr":Landroid/database/Cursor;
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1507
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "FTP_content INNER JOIN FTP_cache"

    const-string v3, "cached = id AND path = ? AND server = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p4, v4, v5

    const/4 v5, 0x1

    aput-object p3, v4, v5

    invoke-static {p1, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFTPSortByString(II)Ljava/lang/String;

    move-result-object v7

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1515
    :cond_0
    if-eqz v8, :cond_1

    .line 1517
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1521
    :cond_1
    return-object v8
.end method

.method public getFtpBufferContent(Ljava/util/ArrayList;II)Landroid/database/Cursor;
    .locals 9
    .param p2, "sortBy"    # I
    .param p3, "order"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;II)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .local p1, "exts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 1485
    const/4 v8, 0x0

    .line 1487
    .local v8, "cr":Landroid/database/Cursor;
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "FTP_buffer"

    invoke-static {p1}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFTPWhereClause(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFTPSortByString(II)Ljava/lang/String;

    move-result-object v7

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1491
    if-eqz v8, :cond_0

    .line 1493
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1497
    :cond_0
    return-object v8
.end method

.method public getFtpBufferContent(Ljava/util/ArrayList;IILjava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p2, "sortBy"    # I
    .param p3, "order"    # I
    .param p4, "server"    # Ljava/lang/String;
    .param p5, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;II",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .local p1, "exts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 1527
    const/4 v8, 0x0

    .line 1529
    .local v8, "cr":Landroid/database/Cursor;
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1531
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "FTP_content INNER JOIN FTP_cache"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cached = id AND path = ? AND server = ? AND ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFTPWhereClause(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p5, v4, v5

    const/4 v5, 0x1

    aput-object p4, v4, v5

    invoke-static {p2, p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFTPSortByString(II)Ljava/lang/String;

    move-result-object v7

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1539
    :cond_0
    if-eqz v8, :cond_1

    .line 1541
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1545
    :cond_1
    return-object v8
.end method

.method public getFtpList(JII)Landroid/database/Cursor;
    .locals 9
    .param p1, "id"    # J
    .param p3, "sortBy"    # I
    .param p4, "order"    # I

    .prologue
    const/4 v2, 0x0

    .line 1588
    const/4 v8, 0x0

    .line 1590
    .local v8, "cr":Landroid/database/Cursor;
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    .line 1592
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "FTP_content"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cached = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p3, p4}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFTPSortByString(II)Ljava/lang/String;

    move-result-object v7

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1598
    if-eqz v8, :cond_0

    .line 1600
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1606
    :cond_0
    return-object v8
.end method

.method public getFtpList(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;II)Landroid/database/Cursor;
    .locals 6
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "timestamp"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;
    .param p4, "sortBy"    # I
    .param p5, "order"    # I

    .prologue
    .line 1612
    if-eqz p1, :cond_0

    .line 1614
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/database/Cursor;

    move-result-object v0

    .line 1618
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFtpList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/database/Cursor;
    .locals 4
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "timestamp"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;
    .param p4, "sortBy"    # I
    .param p5, "order"    # I

    .prologue
    .line 1626
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpPathCachedId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    .line 1628
    .local v0, "id":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    invoke-virtual {p0, v0, v1, p4, p5}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpList(JII)Landroid/database/Cursor;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getFtpPathCachedId(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;)J
    .locals 2
    .param p1, "server"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "timestamp"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 1674
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpPathCachedId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getFtpPathCachedId(Ljava/lang/String;Ljava/lang/String;)J
    .locals 12
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    const-wide/16 v10, -0x1

    const/4 v2, 0x0

    .line 1698
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 1700
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "FTP_cache"

    const-string v3, "server = ? AND path = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    aput-object p2, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1705
    .local v8, "cr":Landroid/database/Cursor;
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1707
    const-string v0, "id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 1709
    .local v10, "id":J
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1723
    .end local v8    # "cr":Landroid/database/Cursor;
    .end local v10    # "id":J
    :cond_0
    :goto_0
    return-wide v10

    .line 1715
    .restart local v8    # "cr":Landroid/database/Cursor;
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public getFtpPathCachedId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 12
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "timestamp"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    const-wide/16 v10, -0x1

    const/4 v2, 0x0

    .line 2101
    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    .line 2103
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "FTP_cache"

    const-string v3, "server = ? AND path = ? AND timestamp = ?"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    aput-object p3, v4, v5

    const/4 v5, 0x2

    aput-object p2, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 2108
    .local v8, "cr":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 2110
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2112
    const-string v0, "id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 2114
    .local v10, "id":J
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 2130
    .end local v8    # "cr":Landroid/database/Cursor;
    .end local v10    # "id":J
    :cond_0
    :goto_0
    return-wide v10

    .line 2120
    .restart local v8    # "cr":Landroid/database/Cursor;
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public getFtpPathcachedId(Ljava/lang/String;Ljava/lang/String;)J
    .locals 12
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    const-wide/16 v10, -0x1

    const/4 v2, 0x0

    .line 1256
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 1258
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "FTP_cache"

    const-string v3, "server = ? AND path = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    aput-object p2, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1263
    .local v8, "cr":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 1265
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1267
    const-string v0, "id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 1269
    .local v10, "id":J
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1285
    .end local v8    # "cr":Landroid/database/Cursor;
    .end local v10    # "id":J
    :cond_0
    :goto_0
    return-wide v10

    .line 1275
    .restart local v8    # "cr":Landroid/database/Cursor;
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public getFtpSizeMdtmPair(Ljava/lang/String;Ljava/lang/String;)Landroid/util/Pair;
    .locals 14
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "fileFullPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1217
    const-wide/16 v12, -0x1

    .local v12, "size":J
    const-wide/16 v10, -0x1

    .line 1219
    .local v10, "mdtm":J
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1221
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "FTP_content INNER JOIN FTP_cache"

    const/4 v2, 0x0

    const-string v3, "FTP_content.cached = FTP_cache.id AND FTP_cache.server = ? AND FTP_content._data = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    aput-object p2, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1228
    .local v8, "cr":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    .line 1232
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1234
    const-string v0, "file_size"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 1236
    const-string v0, "date"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v10

    .line 1242
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1250
    .end local v8    # "cr":Landroid/database/Cursor;
    :cond_1
    new-instance v0, Landroid/util/Pair;

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    .line 1242
    .restart local v8    # "cr":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public getHistoryCacheCursor(II)Landroid/database/Cursor;
    .locals 9
    .param p1, "sortBy"    # I
    .param p2, "orderBy"    # I

    .prologue
    const/4 v2, 0x0

    .line 2537
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "history_cache"

    invoke-static {p1, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getHistorySortByString(II)Ljava/lang/String;

    move-result-object v7

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 2541
    .local v8, "cr":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 2543
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2547
    :cond_0
    return-object v8
.end method

.method public getIfDropboxFileExist(Ljava/lang/String;)Z
    .locals 5
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 807
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 808
    const/4 v1, 0x1

    .line 809
    .local v1, "exist":Z
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SELECT _data FROM cloud_dropbox where _data LIKE \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->shadeQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 810
    .local v2, "sql":Ljava/lang/String;
    sget-object v3, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 811
    .local v0, "cr":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    .line 812
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 813
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-gtz v3, :cond_0

    .line 814
    const/4 v1, 0x0

    .line 816
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 820
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 821
    return v1

    .line 819
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLastUpdateTime(Ljava/lang/String;Ljava/lang/String;)J
    .locals 13
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 1291
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1293
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "FTP_cache"

    const/4 v2, 0x0

    const-string v3, "server = ? AND path = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    aput-object p2, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1301
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    .line 1305
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1307
    const-string v0, "timestamp"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1311
    .local v12, "timestampStr":Ljava/lang/String;
    :try_start_0
    invoke-static {v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v10

    .line 1325
    .end local v12    # "timestampStr":Ljava/lang/String;
    .local v10, "timestamp":J
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1331
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v10    # "timestamp":J
    :goto_1
    return-wide v10

    .line 1313
    .restart local v8    # "cursor":Landroid/database/Cursor;
    .restart local v12    # "timestampStr":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 1315
    .local v9, "e":Ljava/lang/NumberFormatException;
    const/4 v0, 0x1

    sget-object v1, Lcom/sec/android/app/myfiles/db/CacheDB;->MODULE:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cannot parse the timestamp: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v9}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 1317
    const-wide/16 v10, -0x1

    .restart local v10    # "timestamp":J
    goto :goto_0

    .line 1322
    .end local v9    # "e":Ljava/lang/NumberFormatException;
    .end local v10    # "timestamp":J
    .end local v12    # "timestampStr":Ljava/lang/String;
    :cond_0
    const-wide/16 v10, -0x1

    .restart local v10    # "timestamp":J
    goto :goto_0

    .line 1331
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v10    # "timestamp":J
    :cond_1
    const-wide/16 v10, -0x1

    goto :goto_1
.end method

.method public getNearByList(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1, "deviceID"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 946
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 947
    .local v9, "newNode":Landroid/content/ContentValues;
    const-string v0, "deviceID"

    invoke-virtual {v9, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 949
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "NearyBy_contents"

    const-string v3, "deviceID = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 953
    .local v8, "cr":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 954
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 956
    :cond_0
    return-object v8
.end method

.method public getNearByList(Ljava/lang/String;II)Landroid/database/Cursor;
    .locals 9
    .param p1, "deviceId"    # Ljava/lang/String;
    .param p2, "sortBy"    # I
    .param p3, "order"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 975
    const/4 v8, 0x0

    .line 976
    .local v8, "cr":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/db/CacheDB;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowHiddenFileStatus()Z

    move-result v0

    if-nez v0, :cond_1

    .line 977
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "NearyBy_contents"

    const-string v3, "deviceID = ? AND title NOT LIKE ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v5

    const-string v5, ".%"

    aput-object v5, v4, v6

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->getSortByStr(II)Ljava/lang/String;

    move-result-object v7

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 989
    :goto_0
    if-eqz v8, :cond_0

    .line 990
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 993
    :cond_0
    return-object v8

    .line 982
    :cond_1
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "NearyBy_contents"

    const-string v3, "deviceID = ?"

    new-array v4, v6, [Ljava/lang/String;

    aput-object p1, v4, v5

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->getSortByStr(II)Ljava/lang/String;

    move-result-object v7

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    goto :goto_0
.end method

.method public getNearByPathSizes(Ljava/util/ArrayList;)J
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .local p1, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-wide/16 v4, 0x0

    const/4 v8, 0x0

    .line 681
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-gtz v6, :cond_1

    .line 697
    :cond_0
    :goto_0
    return-wide v4

    .line 683
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 684
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 685
    .local v2, "sb":Ljava/lang/StringBuilder;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " WHERE _data = \""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 686
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_2

    .line 687
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " OR _data = \""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 686
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 689
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SELECT SUM(FileSize) FROM NearyBy_contents"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 690
    .local v3, "sql":Ljava/lang/String;
    sget-object v6, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v7, 0x0

    invoke-virtual {v6, v3, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 691
    .local v0, "cr":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 692
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 693
    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 694
    .local v4, "size":J
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method public getNearbyFilteredContent(Ljava/lang/String;IILcom/sec/android/app/myfiles/utils/SearchParameter;)Landroid/database/Cursor;
    .locals 9
    .param p1, "deviceId"    # Ljava/lang/String;
    .param p2, "sortBy"    # I
    .param p3, "order"    # I
    .param p4, "param"    # Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .prologue
    const/4 v2, 0x0

    .line 960
    const/4 v8, 0x0

    .line 962
    .local v8, "cr":Landroid/database/Cursor;
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "NearyBy_contents"

    invoke-virtual {p0, p4}, Lcom/sec/android/app/myfiles/db/CacheDB;->getNearbyFilteredWhereClause(Lcom/sec/android/app/myfiles/utils/SearchParameter;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->getSortByStr(II)Ljava/lang/String;

    move-result-object v7

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 967
    if-eqz v8, :cond_0

    .line 968
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 971
    :cond_0
    return-object v8
.end method

.method public getNearbyFilteredWhereClause(Lcom/sec/android/app/myfiles/utils/SearchParameter;)Ljava/lang/String;
    .locals 10
    .param p1, "param"    # Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .prologue
    const-wide/16 v8, 0x0

    const/4 v5, 0x6

    const/4 v7, 0x4

    const/4 v6, 0x3

    .line 997
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 999
    .local v3, "where":Ljava/lang/StringBuilder;
    const-string v4, "deviceID = ?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1000
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v4

    if-eq v4, v7, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v4

    if-ne v4, v5, :cond_3

    .line 1003
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateFrom()J

    move-result-wide v4

    cmp-long v4, v4, v8

    if-lez v4, :cond_1

    .line 1004
    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1005
    const-string v4, "Date"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1006
    const-string v4, " >= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1007
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateFrom()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1009
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateTo()J

    move-result-wide v4

    cmp-long v4, v4, v8

    if-lez v4, :cond_2

    .line 1010
    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1011
    const-string v4, "Date"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1012
    const-string v4, " <= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1013
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateTo()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1046
    :cond_2
    :goto_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 1015
    :cond_3
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v4

    if-eq v4, v6, :cond_4

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v4

    if-ne v4, v5, :cond_2

    .line 1018
    :cond_4
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/EnumSet;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 1019
    const/4 v0, 0x0

    .line 1020
    .local v0, "curr":I
    const-string v4, " AND ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1021
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/utils/FileType;

    .line 1022
    .local v1, "fileType":Lcom/sec/android/app/myfiles/utils/FileType;
    const-string v4, "MediaType"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1023
    const-string v4, " == "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1024
    sget-object v4, Lcom/sec/android/app/myfiles/db/CacheDB$1;->$SwitchMap$com$sec$android$app$myfiles$utils$FileType:[I

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/FileType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1038
    :goto_2
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/EnumSet;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_5

    .line 1039
    const-string v4, " OR "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1041
    :cond_5
    add-int/lit8 v0, v0, 0x1

    .line 1042
    goto :goto_1

    .line 1026
    :pswitch_0
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1029
    :pswitch_1
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1032
    :pswitch_2
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1035
    :pswitch_3
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1043
    .end local v1    # "fileType":Lcom/sec/android/app/myfiles/utils/FileType;
    :cond_6
    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1024
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getPathOfId(Ljava/lang/String;J)Ljava/lang/String;
    .locals 12
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "id"    # J

    .prologue
    const/4 v2, 0x0

    .line 2136
    const/4 v9, 0x0

    .line 2138
    .local v9, "path":Ljava/lang/String;
    if-eqz p1, :cond_1

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_1

    .line 2140
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "FTP_cache"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "id = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 2144
    .local v8, "cr":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    .line 2146
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2148
    const-string v0, "server"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 2150
    .local v10, "srv":Ljava/lang/String;
    invoke-virtual {p1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2152
    const-string v0, "path"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 2158
    .end local v10    # "srv":Ljava/lang/String;
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 2164
    .end local v8    # "cr":Landroid/database/Cursor;
    :cond_1
    return-object v9
.end method

.method public insertCloudData(Lcom/samsung/scloud/data/SCloudNode;Z)J
    .locals 6
    .param p1, "node"    # Lcom/samsung/scloud/data/SCloudNode;
    .param p2, "InitInsert"    # Z

    .prologue
    .line 269
    if-nez p1, :cond_0

    .line 270
    const-wide/16 v2, 0x0

    .line 290
    :goto_0
    return-wide v2

    .line 272
    :cond_0
    if-nez p2, :cond_1

    .line 273
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/db/CacheDB;->deleteCloudData(Lcom/samsung/scloud/data/SCloudNode;)I

    .line 274
    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 275
    .local v1, "newNode":Landroid/content/ContentValues;
    const-string v2, "_data"

    invoke-virtual {p1}, Lcom/samsung/scloud/data/SCloudNode;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    const-string v2, "title"

    invoke-virtual {p1}, Lcom/samsung/scloud/data/SCloudNode;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    const-string v2, "_size"

    invoke-virtual {p1}, Lcom/samsung/scloud/data/SCloudNode;->getSize()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 278
    const-string v2, "date_modified"

    invoke-virtual {p1}, Lcom/samsung/scloud/data/SCloudNode;->getUpdatedTimestamp()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 279
    const-string v2, "mime_type"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    invoke-virtual {p1}, Lcom/samsung/scloud/data/SCloudNode;->getNodeType()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/samsung/scloud/data/SCloudNode;->TYPE_FOLDER:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 281
    const-string v2, "format"

    const/16 v3, 0x3001

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 282
    const-string v2, "mime_type"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :cond_2
    :goto_1
    sget-object v2, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "cloud_dropbox"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 284
    check-cast v0, Lcom/samsung/scloud/data/SCloudFile;

    .line 285
    .local v0, "file":Lcom/samsung/scloud/data/SCloudFile;
    const-string v2, "format"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 286
    if-eqz v0, :cond_2

    .line 287
    const-string v2, "mime_type"

    invoke-virtual {v0}, Lcom/samsung/scloud/data/SCloudFile;->getMimeType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public insertFTPData(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)J
    .locals 6
    .param p1, "item"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .prologue
    .line 1080
    if-nez p1, :cond_0

    .line 1082
    const-wide/16 v0, 0x0

    .line 1109
    :goto_0
    return-wide v0

    .line 1086
    :cond_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1088
    .local v2, "newNode":Landroid/content/ContentValues;
    const-string v3, "title"

    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1090
    const-string v3, "extension"

    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getExtension()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1092
    const-string v3, "file_type"

    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getType()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1094
    const-string v3, "file_size"

    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getSize()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1096
    const-string v3, "_data"

    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1097
    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isSymbolicLink()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1098
    const-string v3, "is_directory"

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1103
    :goto_1
    const-string v3, "date"

    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getDate()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1105
    sget-object v3, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "FTP_content"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 1107
    .local v0, "id":J
    invoke-interface {p1, v0, v1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->setId(J)V

    goto :goto_0

    .line 1100
    .end local v0    # "id":J
    :cond_1
    const-string v4, "is_directory"

    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    :goto_2
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public insertFtpData(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPItem;)J
    .locals 10
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "timestamp"    # Ljava/lang/String;
    .param p3, "item"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .prologue
    .line 1337
    if-eqz p3, :cond_7

    if-eqz p1, :cond_7

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_7

    .line 1339
    invoke-interface {p3}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getDir()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, p1, p2, v6}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpPathCachedId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    .line 1341
    .local v2, "id":J
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-gez v6, :cond_2

    .line 1343
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1345
    .local v0, "cachedCV":Landroid/content/ContentValues;
    const-string v6, "timestamp"

    invoke-virtual {v0, v6, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1347
    const-string v6, "path"

    invoke-interface {p3}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getDir()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1349
    const-string v6, "server"

    invoke-virtual {v0, v6, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1350
    sget-boolean v6, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-eqz v6, :cond_1

    .line 1351
    const/4 v6, 0x2

    const-string v7, "ftp"

    const-string v8, "mAbortBrowsing ture insertFtpData  RETURN !!!"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1352
    const-wide/16 v4, 0x0

    .line 1406
    .end local v0    # "cachedCV":Landroid/content/ContentValues;
    .end local v2    # "id":J
    :cond_0
    :goto_0
    return-wide v4

    .line 1355
    .restart local v0    # "cachedCV":Landroid/content/ContentValues;
    .restart local v2    # "id":J
    :cond_1
    sget-object v6, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v7, "FTP_cache"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 1359
    .end local v0    # "cachedCV":Landroid/content/ContentValues;
    :cond_2
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-ltz v6, :cond_6

    .line 1361
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1363
    .local v1, "newItem":Landroid/content/ContentValues;
    const-string v6, "title"

    invoke-interface {p3}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1365
    const-string v6, "extension"

    invoke-interface {p3}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getExtension()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1367
    const-string v6, "file_type"

    invoke-interface {p3}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getType()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1369
    const-string v6, "file_size"

    invoke-interface {p3}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getSize()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1371
    const-string v6, "_data"

    invoke-interface {p3}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1373
    invoke-interface {p3}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isSymbolicLink()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1374
    const-string v6, "is_directory"

    const/4 v7, 0x2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1379
    :goto_1
    const-string v6, "date"

    invoke-interface {p3}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getDate()Ljava/util/Calendar;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1381
    const-string v6, "cached"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1383
    sget-boolean v6, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-eqz v6, :cond_5

    .line 1384
    const/4 v6, 0x2

    const-string v7, "ftp"

    const-string v8, "mAbortBrowsing ture db.insert(TABLE_FTP_CONTENT !!!"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1385
    const-wide/16 v4, 0x0

    goto/16 :goto_0

    .line 1376
    :cond_3
    const-string v7, "is_directory"

    invoke-interface {p3}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_4

    const/4 v6, 0x1

    :goto_2
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v7, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    :cond_4
    const/4 v6, 0x0

    goto :goto_2

    .line 1388
    :cond_5
    sget-object v6, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v7, "FTP_content"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 1390
    .local v4, "itemId":J
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-ltz v6, :cond_0

    .line 1392
    invoke-interface {p3, v4, v5}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->setId(J)V

    goto/16 :goto_0

    .end local v1    # "newItem":Landroid/content/ContentValues;
    .end local v4    # "itemId":J
    :cond_6
    move-wide v4, v2

    .line 1400
    goto/16 :goto_0

    .line 1406
    .end local v2    # "id":J
    :cond_7
    const-wide/16 v4, -0x1

    goto/16 :goto_0
.end method

.method public declared-synchronized insertFtpData(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 17
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "timestamp"    # Ljava/lang/String;
    .param p4, "rawPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1115
    .local p3, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    monitor-enter p0

    if-eqz p3, :cond_1

    :try_start_0
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_1

    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_1

    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 1117
    invoke-static/range {p4 .. p4}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->trimFtpPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1119
    .local v10, "path":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v10}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpPathCachedId(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v4

    .line 1121
    .local v4, "id":J
    const-wide/16 v12, 0x0

    cmp-long v11, v4, v12

    if-ltz v11, :cond_0

    .line 1123
    sget-object v11, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v12, "FTP_content"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "cached = \'"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\'"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v11, v12, v13, v14}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1127
    :cond_0
    sget-boolean v11, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-eqz v11, :cond_2

    .line 1128
    const/4 v11, 0x2

    const-string v12, "FTP"

    const-string v13, "Cancel Called !  insertFtpData RETURN !!!"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1201
    .end local v4    # "id":J
    .end local v10    # "path":Ljava/lang/String;
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 1132
    .restart local v4    # "id":J
    .restart local v10    # "path":Ljava/lang/String;
    :cond_2
    const-wide/16 v12, 0x0

    cmp-long v11, v4, v12

    if-ltz v11, :cond_4

    .line 1134
    :try_start_1
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1136
    .local v2, "cachedCV":Landroid/content/ContentValues;
    const-string v11, "timestamp"

    move-object/from16 v0, p2

    invoke-virtual {v2, v11, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1138
    sget-object v11, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v12, "FTP_cache"

    const-string v13, "id = ?"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v11, v12, v2, v13, v14}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1154
    :goto_1
    const-wide/16 v12, 0x0

    cmp-long v11, v4, v12

    if-ltz v11, :cond_1

    .line 1156
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 1158
    .local v6, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 1160
    .local v7, "newItem":Landroid/content/ContentValues;
    const-string v11, "title"

    invoke-interface {v6}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1162
    const-string v11, "extension"

    invoke-interface {v6}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getExtension()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1164
    const-string v11, "file_type"

    invoke-interface {v6}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getType()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v7, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1166
    const-string v11, "file_size"

    invoke-interface {v6}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getSize()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v7, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1168
    const-string v11, "_data"

    invoke-interface {v6}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1170
    invoke-interface {v6}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isSymbolicLink()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1171
    const-string v11, "is_directory"

    const/4 v12, 0x2

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v7, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1176
    :goto_3
    const-string v11, "date"

    invoke-interface {v6}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getDate()Ljava/util/Calendar;

    move-result-object v12

    if-eqz v12, :cond_7

    invoke-interface {v6}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getDate()Ljava/util/Calendar;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v12

    :goto_4
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v7, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1178
    const-string v11, "cached"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v7, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1180
    sget-boolean v11, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-eqz v11, :cond_8

    .line 1181
    const/4 v11, 0x2

    const-string v12, "ftp"

    const-string v13, "mAbortBrowsing ture db.insert(TABLE_FTP_CONTENT, null, newItem);  RETURN !!!"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 1115
    .end local v2    # "cachedCV":Landroid/content/ContentValues;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "id":J
    .end local v6    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v7    # "newItem":Landroid/content/ContentValues;
    .end local v10    # "path":Ljava/lang/String;
    :catchall_0
    move-exception v11

    monitor-exit p0

    throw v11

    .line 1142
    .restart local v4    # "id":J
    .restart local v10    # "path":Ljava/lang/String;
    :cond_4
    :try_start_2
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1144
    .restart local v2    # "cachedCV":Landroid/content/ContentValues;
    const-string v11, "timestamp"

    move-object/from16 v0, p2

    invoke-virtual {v2, v11, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1146
    const-string v11, "path"

    invoke-virtual {v2, v11, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1148
    const-string v11, "server"

    move-object/from16 v0, p1

    invoke-virtual {v2, v11, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1150
    sget-object v11, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v12, "FTP_cache"

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    goto/16 :goto_1

    .line 1173
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v6    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .restart local v7    # "newItem":Landroid/content/ContentValues;
    :cond_5
    const-string v12, "is_directory"

    invoke-interface {v6}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isDirectory()Z

    move-result v11

    if-eqz v11, :cond_6

    const/4 v11, 0x1

    :goto_5
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v7, v12, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_3

    :cond_6
    const/4 v11, 0x0

    goto :goto_5

    .line 1176
    :cond_7
    const-wide/16 v12, 0x0

    goto :goto_4

    .line 1185
    :cond_8
    sget-object v11, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1187
    sget-object v11, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v12, "FTP_content"

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13, v7}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v8

    .line 1189
    .local v8, "itemId":J
    const-wide/16 v12, 0x0

    cmp-long v11, v8, v12

    if-ltz v11, :cond_3

    .line 1191
    invoke-interface {v6, v8, v9}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->setId(J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2
.end method

.method public insertFtpIntoBuffer(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V
    .locals 5
    .param p1, "item"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .prologue
    const/4 v4, 0x2

    .line 1414
    if-eqz p1, :cond_0

    .line 1416
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1418
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v1, "date"

    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getDate()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1420
    const-string v1, "extension"

    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getExtension()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1422
    const-string v1, "file_type"

    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1424
    const-string v1, "file_size"

    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getSize()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1426
    const-string v1, "_data"

    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1428
    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isSymbolicLink()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1429
    const-string v1, "is_directory"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1434
    :goto_0
    const-string v1, "title"

    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1436
    sget-boolean v1, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-eqz v1, :cond_3

    .line 1438
    const-string v1, "ftp"

    const-string v2, "mAbortBrowsing ture db.insert(TABLE_FTP_BUFFER, null, cv);  RETURN !!!"

    invoke-static {v4, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1448
    .end local v0    # "cv":Landroid/content/ContentValues;
    :cond_0
    :goto_1
    return-void

    .line 1431
    .restart local v0    # "cv":Landroid/content/ContentValues;
    :cond_1
    const-string v2, "is_directory"

    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    .line 1444
    :cond_3
    sget-object v1, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "FTP_buffer"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_1
.end method

.method public insertFtpNewFolder(Ljava/lang/String;Ljava/lang/String;)J
    .locals 18
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 2070
    const-wide/16 v16, -0x1

    .line 2072
    .local v16, "ret":J
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2074
    invoke-static/range {p2 .. p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getTitleFromPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2076
    .local v4, "title":Ljava/lang/String;
    invoke-static/range {p2 .. p2}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getParentDir(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2078
    .local v2, "parent":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpPathCachedId(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v10

    .line 2080
    .local v10, "cachedId":J
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-wide/16 v6, 0x0

    cmp-long v3, v10, v6

    if-ltz v3, :cond_0

    .line 2082
    const/4 v6, 0x0

    const/4 v7, 0x1

    const-wide/16 v8, 0x0

    invoke-static {}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getCurrentTime()J

    move-result-wide v12

    const/4 v14, 0x0

    move-object/from16 v3, p0

    move-object/from16 v5, p2

    invoke-direct/range {v3 .. v14}, Lcom/sec/android/app/myfiles/db/CacheDB;->insertIntoFtpContent(Ljava/lang/String;Ljava/lang/String;IIJJJLjava/lang/String;)J

    move-result-wide v16

    .line 2087
    :cond_0
    const-wide/16 v6, 0x0

    cmp-long v3, v16, v6

    if-ltz v3, :cond_1

    .line 2089
    invoke-direct/range {p0 .. p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->insertIntoFtpCached(Ljava/lang/String;Ljava/lang/String;)J

    .line 2095
    .end local v2    # "parent":Ljava/lang/String;
    .end local v4    # "title":Ljava/lang/String;
    .end local v10    # "cachedId":J
    :cond_1
    return-wide v16
.end method

.method public insertNearByData(Ljava/lang/String;Lcom/samsung/android/allshare/Item;)J
    .locals 8
    .param p1, "deviceID"    # Ljava/lang/String;
    .param p2, "item"    # Lcom/samsung/android/allshare/Item;

    .prologue
    .line 875
    if-nez p2, :cond_0

    .line 876
    const-wide/16 v4, 0x0

    .line 929
    :goto_0
    return-wide v4

    .line 878
    :cond_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 880
    .local v3, "newNode":Landroid/content/ContentValues;
    const-string v4, "deviceID"

    invoke-virtual {v3, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 881
    const-string v4, "title"

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x2f

    const/16 v7, 0x5c

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 883
    invoke-virtual {p2}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 884
    invoke-virtual {p2}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 885
    .local v0, "FileUri":Ljava/lang/String;
    const-string v4, "_data"

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 892
    .end local v0    # "FileUri":Ljava/lang/String;
    :cond_1
    const/4 v2, 0x0

    .line 894
    .local v2, "fileType":I
    sget-object v4, Lcom/sec/android/app/myfiles/db/CacheDB$1;->$SwitchMap$com$samsung$android$allshare$Item$MediaType:[I

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Item;->getType()Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/allshare/Item$MediaType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 912
    const/4 v2, 0x0

    .line 916
    :goto_1
    const-string v4, "MediaType"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 917
    const-string v4, "Extension"

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Item;->getExtension()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 918
    const-string v4, "resolution"

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Item;->getResolution()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 919
    const-string v4, "FileSize"

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Item;->getFileSize()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 920
    const-string v4, "MimeType"

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Item;->getMimetype()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 922
    invoke-virtual {p2}, Lcom/samsung/android/allshare/Item;->getThumbnail()Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 923
    const-string v4, "ThumbUri"

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Item;->getThumbnail()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 925
    :cond_2
    invoke-virtual {p2}, Lcom/samsung/android/allshare/Item;->getDate()Ljava/util/Date;

    move-result-object v1

    .line 926
    .local v1, "date":Ljava/util/Date;
    if-eqz v1, :cond_3

    .line 927
    const-string v4, "Date"

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 929
    :cond_3
    sget-object v4, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "NearyBy_contents"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    goto/16 :goto_0

    .line 896
    .end local v1    # "date":Ljava/util/Date;
    :pswitch_0
    const/4 v2, 0x2

    .line 897
    goto :goto_1

    .line 900
    :pswitch_1
    const/4 v2, 0x3

    .line 901
    goto :goto_1

    .line 904
    :pswitch_2
    const/4 v2, 0x4

    .line 905
    goto :goto_1

    .line 908
    :pswitch_3
    const/4 v2, 0x1

    .line 909
    goto :goto_1

    .line 894
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public insertNearByDeviceID(Ljava/lang/String;)J
    .locals 4
    .param p1, "deviceID"    # Ljava/lang/String;

    .prologue
    .line 867
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 868
    .local v0, "newNode":Landroid/content/ContentValues;
    const-string v1, "deviceID"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    sget-object v1, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "NearyBy_deviceID"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    return-wide v2
.end method

.method public isDirectory(Ljava/lang/String;)Z
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 825
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 826
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->shadeQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 827
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SELECT format FROM cloud_dropbox where _data LIKE \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 828
    .local v2, "sql":Ljava/lang/String;
    sget-object v4, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 829
    .local v0, "cr":Landroid/database/Cursor;
    if-nez v0, :cond_1

    .line 841
    :cond_0
    :goto_0
    return v3

    .line 831
    :cond_1
    const/16 v1, 0x3001

    .line 832
    .local v1, "isFolder":I
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_2

    .line 833
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 834
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 836
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 837
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 838
    const/16 v4, 0x3001

    if-ne v1, v4, :cond_0

    .line 839
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public isDropBoxCloudDataExist()I
    .locals 5

    .prologue
    .line 2783
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 2785
    const-string v2, " select COUNT(_id) from cloud_dropbox"

    .line 2787
    .local v2, "sql":Ljava/lang/String;
    sget-object v3, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2789
    .local v0, "cr":Landroid/database/Cursor;
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2791
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 2793
    .local v1, "noOfRows":I
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2795
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 2797
    return v1
.end method

.method public isFileCached(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 15
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "fileFullPath"    # Ljava/lang/String;
    .param p3, "cachedFullPath"    # Ljava/lang/String;

    .prologue
    .line 2458
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 2460
    new-instance v5, Ljava/io/File;

    move-object/from16 v0, p3

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2462
    .local v5, "cachedFile":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2464
    const-string v3, "diff"

    .line 2466
    .local v3, "DIFF":Ljava/lang/String;
    const-string v4, "size_diff"

    .line 2468
    .local v4, "SIZE_DIFF":Ljava/lang/String;
    const-string v2, "cached_id"

    .line 2470
    .local v2, "CACHED_ID":Ljava/lang/String;
    sget-object v7, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v12, "SELECT (FTP_content.date - FTP_file_cache.date) AS diff, (FTP_content.file_size - FTP_file_cache.size) AS size_diff, FTP_file_cache.cached AS cached_id FROM FTP_file_cache JOIN FTP_content ON FTP_file_cache.filename = FTP_content._data JOIN FTP_cache ON FTP_file_cache.cached = FTP_cache.id WHERE FTP_cache.id = FTP_content.cached AND FTP_cache.server = ? AND FTP_content._data = ?"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    aput-object p1, v13, v14

    const/4 v14, 0x1

    aput-object p2, v13, v14

    invoke-virtual {v7, v12, v13}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2483
    .local v6, "cr":Landroid/database/Cursor;
    if-eqz v6, :cond_3

    .line 2487
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2489
    const-string v7, "diff"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 2491
    .local v8, "diff":J
    const-string v7, "size_diff"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 2493
    .local v10, "sizeDiff":J
    const-wide/16 v12, 0x0

    cmp-long v7, v8, v12

    if-nez v7, :cond_0

    const-wide/16 v12, 0x0

    cmp-long v7, v10, v12

    if-eqz v7, :cond_1

    .line 2495
    :cond_0
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 2497
    invoke-virtual/range {p0 .. p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->deleteFtpFileFromCache(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2499
    const/4 v7, 0x0

    .line 2511
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2525
    .end local v2    # "CACHED_ID":Ljava/lang/String;
    .end local v3    # "DIFF":Ljava/lang/String;
    .end local v4    # "SIZE_DIFF":Ljava/lang/String;
    .end local v5    # "cachedFile":Ljava/io/File;
    .end local v6    # "cr":Landroid/database/Cursor;
    .end local v8    # "diff":J
    .end local v10    # "sizeDiff":J
    :goto_0
    return v7

    .line 2503
    .restart local v2    # "CACHED_ID":Ljava/lang/String;
    .restart local v3    # "DIFF":Ljava/lang/String;
    .restart local v4    # "SIZE_DIFF":Ljava/lang/String;
    .restart local v5    # "cachedFile":Ljava/io/File;
    .restart local v6    # "cr":Landroid/database/Cursor;
    .restart local v8    # "diff":J
    .restart local v10    # "sizeDiff":J
    :cond_1
    const/4 v7, 0x1

    .line 2511
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .end local v8    # "diff":J
    .end local v10    # "sizeDiff":J
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2525
    .end local v2    # "CACHED_ID":Ljava/lang/String;
    .end local v3    # "DIFF":Ljava/lang/String;
    .end local v4    # "SIZE_DIFF":Ljava/lang/String;
    .end local v5    # "cachedFile":Ljava/io/File;
    .end local v6    # "cr":Landroid/database/Cursor;
    :cond_3
    :goto_1
    const/4 v7, 0x0

    goto :goto_0

    .line 2511
    .restart local v2    # "CACHED_ID":Ljava/lang/String;
    .restart local v3    # "DIFF":Ljava/lang/String;
    .restart local v4    # "SIZE_DIFF":Ljava/lang/String;
    .restart local v5    # "cachedFile":Ljava/io/File;
    .restart local v6    # "cr":Landroid/database/Cursor;
    :catchall_0
    move-exception v7

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v7

    .line 2519
    .end local v2    # "CACHED_ID":Ljava/lang/String;
    .end local v3    # "DIFF":Ljava/lang/String;
    .end local v4    # "SIZE_DIFF":Ljava/lang/String;
    .end local v6    # "cr":Landroid/database/Cursor;
    :cond_4
    invoke-virtual/range {p0 .. p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->deleteFtpFileFromCache(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public isFtpPathCached(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)Z
    .locals 1
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 1205
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->isFtpPathCached(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isFtpPathCached(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "timestamp"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 1662
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpPathCachedId(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFtpPathCached(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 1211
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpPathcachedId(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFtpPathCached(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "timestamp"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 1668
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpPathCachedId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadFtpBufferWithContent(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;II)V
    .locals 6
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "sortBy"    # I
    .param p4, "order"    # I

    .prologue
    const/4 v5, 0x0

    .line 1634
    if-eqz p1, :cond_0

    .line 1636
    sget-object v3, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "FTP_buffer"

    invoke-virtual {v3, v4, v5, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1638
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v3

    invoke-static {p2}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->trimFtpPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpPathCachedId(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    .line 1640
    .local v0, "id":J
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1642
    .local v2, "query":Ljava/lang/StringBuilder;
    const-string v3, "INSERT INTO FTP_buffer (title, file_type, extension, date, file_size, _data, is_directory) SELECT title, file_type, extension, date, file_size, _data, is_directory FROM FTP_content WHERE cached = \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1650
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1652
    const-string v3, "\';"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1654
    sget-object v3, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1658
    .end local v0    # "id":J
    .end local v2    # "query":Ljava/lang/StringBuilder;
    :cond_0
    return-void
.end method

.method public open()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/sqlite/SQLiteException;
        }
    .end annotation

    .prologue
    .line 201
    const/4 v1, 0x1

    .line 203
    .local v1, "open":Z
    sget-object v2, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v2, :cond_0

    .line 205
    sget-object v2, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 207
    const/4 v1, 0x0

    .line 213
    :cond_0
    if-eqz v1, :cond_1

    .line 217
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/db/CacheDB;->DBHelper:Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 228
    :cond_1
    :goto_0
    return-void

    .line 219
    :catch_0
    move-exception v0

    .line 221
    .local v0, "ex":Landroid/database/sqlite/SQLiteException;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/db/CacheDB;->DBHelper:Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    goto :goto_0
.end method

.method public renameFtpItem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "from"    # Ljava/lang/String;
    .param p3, "to"    # Ljava/lang/String;

    .prologue
    const-wide/16 v4, 0x0

    .line 1760
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1762
    invoke-static {p2}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getParentDir(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1764
    .local v1, "srcParentDir":Ljava/lang/String;
    invoke-static {p3}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getParentDir(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1766
    .local v0, "dstParentDir":Ljava/lang/String;
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1768
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpPathcachedId(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 1770
    const/4 v2, 0x1

    invoke-direct {p0, p1, p2, p3, v2}, Lcom/sec/android/app/myfiles/db/CacheDB;->renameOrMoveFtpFolder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1794
    .end local v0    # "dstParentDir":Ljava/lang/String;
    .end local v1    # "srcParentDir":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 1774
    .restart local v0    # "dstParentDir":Ljava/lang/String;
    .restart local v1    # "srcParentDir":Ljava/lang/String;
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->moveFtpFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1780
    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpPathcachedId(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-ltz v2, :cond_3

    .line 1782
    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3, v2}, Lcom/sec/android/app/myfiles/db/CacheDB;->renameOrMoveFtpFolder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 1786
    :cond_3
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->renameFtpFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public replaceHistoryCache(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/element/HistoryItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/element/HistoryItem;>;"
    const/4 v2, 0x0

    .line 2553
    if-eqz p1, :cond_0

    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2555
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "history_cache"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2557
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/db/CacheDB;->appendHistoryCache(Ljava/util/List;)V

    .line 2561
    :cond_0
    return-void
.end method

.method public setDropboxCursor(Ljava/lang/String;)V
    .locals 7
    .param p1, "cursor"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 2765
    if-nez p1, :cond_0

    .line 2766
    const-string v3, ""

    .line 2770
    .local v3, "tmpCursor":Ljava/lang/String;
    :goto_0
    sget-object v4, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "cloud_dropbox_cursor"

    invoke-virtual {v4, v5, v6, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2772
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2773
    .local v2, "newNode":Landroid/content/ContentValues;
    const-string v4, "cursor"

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2775
    sget-object v4, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "cloud_dropbox_cursor"

    invoke-virtual {v4, v5, v6, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 2776
    .local v0, "a":J
    const-wide/16 v4, 0x1

    add-long/2addr v0, v4

    .line 2779
    return-void

    .line 2768
    .end local v0    # "a":J
    .end local v2    # "newNode":Landroid/content/ContentValues;
    .end local v3    # "tmpCursor":Ljava/lang/String;
    :cond_0
    move-object v3, p1

    .restart local v3    # "tmpCursor":Ljava/lang/String;
    goto :goto_0
.end method

.method public setTransactionSuccessful()V
    .locals 1

    .prologue
    .line 238
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 239
    return-void
.end method

.method public uploadFtpFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 16
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "from"    # Ljava/lang/String;
    .param p3, "to"    # Ljava/lang/String;

    .prologue
    .line 1732
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1734
    invoke-static/range {p3 .. p3}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getParentDir(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpPathCachedId(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v10

    .line 1736
    .local v10, "cachedId":J
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1738
    .local v2, "srcFile":Ljava/io/File;
    invoke-static/range {p3 .. p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->getTitleFromPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 1740
    .local v15, "title":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    const-wide/16 v4, 0x0

    cmp-long v3, v10, v4

    if-ltz v3, :cond_0

    .line 1742
    invoke-static/range {p3 .. p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->getTitleFromPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v15}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v6

    const/4 v7, 0x0

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v8

    invoke-static {}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getCurrentTime()J

    move-result-wide v12

    invoke-static/range {p3 .. p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v3, p0

    move-object/from16 v5, p3

    invoke-direct/range {v3 .. v14}, Lcom/sec/android/app/myfiles/db/CacheDB;->insertIntoFtpContent(Ljava/lang/String;Ljava/lang/String;IIJJJLjava/lang/String;)J

    .line 1756
    .end local v2    # "srcFile":Ljava/io/File;
    .end local v10    # "cachedId":J
    .end local v15    # "title":Ljava/lang/String;
    :cond_0
    return-void
.end method
