.class public final Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;
.super Ljava/lang/Object;
.source "FTPParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/ftp/FTPParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private mAnonymous:Z

.field private mEncoding:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

.field private mEncryption:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

.field private mHiddenFiles:Z

.field private mHost:Ljava/lang/String;

.field private mMode:Lcom/sec/android/app/myfiles/ftp/FTPMode;

.field private mPassword:Ljava/lang/String;

.field private mPort:I

.field private mRootSessionName:Ljava/lang/String;

.field private mSessionName:Ljava/lang/String;

.field private mStartPath:Ljava/lang/String;

.field private mType:Lcom/sec/android/app/myfiles/ftp/FTPType;

.field private mUserName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 336
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 406
    sget-object v0, Lcom/sec/android/app/myfiles/ftp/FTPType;->FTP:Lcom/sec/android/app/myfiles/ftp/FTPType;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mType:Lcom/sec/android/app/myfiles/ftp/FTPType;

    .line 409
    const/16 v0, 0x16

    iput v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mPort:I

    .line 410
    sget-object v0, Lcom/sec/android/app/myfiles/ftp/FTPMode;->PASSIVE:Lcom/sec/android/app/myfiles/ftp/FTPMode;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mMode:Lcom/sec/android/app/myfiles/ftp/FTPMode;

    .line 413
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mAnonymous:Z

    .line 414
    sget-object v0, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->UTF_8:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mEncoding:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    .line 415
    sget-object v0, Lcom/sec/android/app/myfiles/ftp/FTPEncryption;->IMPLICIT:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mEncryption:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    .line 416
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mHiddenFiles:Z

    .line 417
    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mStartPath:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)Lcom/sec/android/app/myfiles/ftp/FTPType;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mType:Lcom/sec/android/app/myfiles/ftp/FTPType;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mSessionName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    .prologue
    .line 336
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mHiddenFiles:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mStartPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mRootSessionName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mHost:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    .prologue
    .line 336
    iget v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mPort:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)Lcom/sec/android/app/myfiles/ftp/FTPMode;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mMode:Lcom/sec/android/app/myfiles/ftp/FTPMode;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mUserName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mPassword:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    .prologue
    .line 336
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mAnonymous:Z

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)Lcom/sec/android/app/myfiles/ftp/FTPEncoding;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mEncoding:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;)Lcom/sec/android/app/myfiles/ftp/FTPEncryption;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mEncryption:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    return-object v0
.end method


# virtual methods
.method public anonymous(Z)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;
    .locals 0
    .param p1, "anonymous"    # Z

    .prologue
    .line 372
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mAnonymous:Z

    .line 373
    return-object p0
.end method

.method public build()Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .locals 2

    .prologue
    .line 338
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;Lcom/sec/android/app/myfiles/ftp/FTPParams$1;)V

    return-object v0
.end method

.method public encoding(Lcom/sec/android/app/myfiles/ftp/FTPEncoding;)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;
    .locals 0
    .param p1, "encoding"    # Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    .prologue
    .line 377
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mEncoding:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    .line 378
    return-object p0
.end method

.method public encryption(Lcom/sec/android/app/myfiles/ftp/FTPEncryption;)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;
    .locals 0
    .param p1, "encryption"    # Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    .prologue
    .line 382
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mEncryption:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    .line 383
    return-object p0
.end method

.method public hiddenFiles(Z)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;
    .locals 0
    .param p1, "hiddenFiles"    # Z

    .prologue
    .line 387
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mHiddenFiles:Z

    .line 388
    return-object p0
.end method

.method public host(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;
    .locals 0
    .param p1, "host"    # Ljava/lang/String;

    .prologue
    .line 342
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mHost:Ljava/lang/String;

    .line 343
    return-object p0
.end method

.method public mode(Lcom/sec/android/app/myfiles/ftp/FTPMode;)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;
    .locals 0
    .param p1, "mode"    # Lcom/sec/android/app/myfiles/ftp/FTPMode;

    .prologue
    .line 357
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mMode:Lcom/sec/android/app/myfiles/ftp/FTPMode;

    .line 358
    return-object p0
.end method

.method public password(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;
    .locals 0
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 367
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mPassword:Ljava/lang/String;

    .line 368
    return-object p0
.end method

.method public port(I)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;
    .locals 0
    .param p1, "port"    # I

    .prologue
    .line 352
    iput p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mPort:I

    .line 353
    return-object p0
.end method

.method public rootSessionName(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;
    .locals 0
    .param p1, "rootSessionName"    # Ljava/lang/String;

    .prologue
    .line 402
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mRootSessionName:Ljava/lang/String;

    .line 403
    return-object p0
.end method

.method public sessionName(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;
    .locals 0
    .param p1, "sessionName"    # Ljava/lang/String;

    .prologue
    .line 347
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mSessionName:Ljava/lang/String;

    .line 348
    return-object p0
.end method

.method public startPath(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;
    .locals 0
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 397
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mStartPath:Ljava/lang/String;

    .line 398
    return-object p0
.end method

.method public type(Lcom/sec/android/app/myfiles/ftp/FTPType;)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;
    .locals 0
    .param p1, "type"    # Lcom/sec/android/app/myfiles/ftp/FTPType;

    .prologue
    .line 392
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mType:Lcom/sec/android/app/myfiles/ftp/FTPType;

    .line 393
    return-object p0
.end method

.method public username(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;
    .locals 0
    .param p1, "username"    # Ljava/lang/String;

    .prologue
    .line 362
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mUserName:Ljava/lang/String;

    .line 363
    return-object p0
.end method
