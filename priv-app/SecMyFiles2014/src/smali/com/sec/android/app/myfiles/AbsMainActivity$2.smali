.class Lcom/sec/android/app/myfiles/AbsMainActivity$2;
.super Landroid/content/BroadcastReceiver;
.source "AbsMainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/AbsMainActivity;->setScannerFinisedReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/AbsMainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/AbsMainActivity;)V
    .locals 0

    .prologue
    .line 399
    iput-object p1, p0, Lcom/sec/android/app/myfiles/AbsMainActivity$2;->this$0:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x0

    .line 404
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 406
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 408
    const-string v3, "AbsMainActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Received the "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 410
    const-string v3, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 412
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getDialogInstance()Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    move-result-object v1

    .line 414
    .local v1, "dialog":Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    if-eqz v1, :cond_0

    .line 416
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->dismissWaitProgressDialog()V

    .line 419
    :cond_0
    invoke-static {v6}, Lcom/sec/android/app/myfiles/utils/Utils;->setMediaScannerScannig(Z)V

    .line 421
    new-instance v2, Landroid/content/Intent;

    const-string v3, "Media_DB_Update_Finsished_by_Scanner"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 423
    .local v2, "updateFinishintent":Landroid/content/Intent;
    invoke-virtual {p1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 426
    .end local v1    # "dialog":Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    .end local v2    # "updateFinishintent":Landroid/content/Intent;
    :cond_1
    return-void
.end method
