.class Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$2;
.super Ljava/lang/Object;
.source "NearByDeviceFileBrowserAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->handleSetIcon(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Ljava/lang/String;Landroid/widget/AdapterView;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

.field final synthetic val$isVideo:Z

.field final synthetic val$parent:Landroid/widget/AdapterView;

.field final synthetic val$position:I

.field final synthetic val$thumbnailImageView:Landroid/widget/ImageView;

.field final synthetic val$thumbnailUri:Ljava/lang/String;

.field final synthetic val$uri:Landroid/net/Uri;

.field final synthetic val$viewHolder:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;Landroid/widget/AdapterView;ILandroid/net/Uri;Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;ZLandroid/widget/ImageView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 508
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    iput-object p2, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$2;->val$parent:Landroid/widget/AdapterView;

    iput p3, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$2;->val$position:I

    iput-object p4, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$2;->val$uri:Landroid/net/Uri;

    iput-object p5, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$2;->val$viewHolder:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;

    iput-boolean p6, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$2;->val$isVideo:Z

    iput-object p7, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$2;->val$thumbnailImageView:Landroid/widget/ImageView;

    iput-object p8, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$2;->val$thumbnailUri:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 511
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$2;->val$parent:Landroid/widget/AdapterView;

    iget v2, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$2;->val$position:I

    # invokes: Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->isPositionVisible(Landroid/widget/AdapterView;I)Z
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->access$300(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;Landroid/widget/AdapterView;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mLoadingThumbnailsCanceled:Z
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->access$400(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 512
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mNearbyDrawableProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->access$500(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;)Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$2;->val$uri:Landroid/net/Uri;

    new-instance v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1UpdateThumbnail;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$2;->val$thumbnailImageView:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$2;->val$thumbnailUri:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$2;->val$isVideo:Z

    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$2;->val$viewHolder:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1UpdateThumbnail;-><init>(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;Landroid/widget/ImageView;Ljava/lang/String;ZLcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;)V

    invoke-virtual {v6, v7, v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;->setDrawable(Landroid/net/Uri;Lcom/sec/android/app/myfiles/nearbydevices/DrawableUpdatable;)V

    .line 514
    :cond_0
    return-void
.end method
