.class public Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;
.super Ljava/lang/Object;
.source "BaiduAPI.java"

# interfaces
.implements Lcom/samsung/scloud/SCloudAPI;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI$3;
    }
.end annotation


# static fields
.field private static final DEBUG:Z

.field private static final TAG:Ljava/lang/String; = "BaiduAPI"

.field private static baiduErrCode:I

.field private static isContinued:Z

.field private static mInstance:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;


# instance fields
.field private final MAX_RETRY:I

.field private accessToken:Ljava/lang/String;

.field private isAndroid:Z

.field private isAuthSuccess:Z

.field private mContext:Landroid/content/Context;

.field private pcsClient:Lcom/baidu/pcs/BaiduPCSClient;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 76
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->isContinued:Z

    .line 78
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->baiduErrCode:I

    .line 80
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->DEBUG:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->isAuthSuccess:Z

    .line 62
    iput-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->accessToken:Ljava/lang/String;

    .line 64
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->MAX_RETRY:I

    .line 66
    iput-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->mContext:Landroid/content/Context;

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->isAndroid:Z

    .line 84
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->mContext:Landroid/content/Context;

    .line 85
    new-instance v0, Lcom/baidu/pcs/BaiduPCSClient;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSClient;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    .line 86
    return-void
.end method

.method private DateToLong(Ljava/lang/String;)J
    .locals 6
    .param p1, "dateInString"    # Ljava/lang/String;

    .prologue
    .line 1323
    const-wide/16 v2, -0x1

    .line 1324
    .local v2, "dateInLong":J
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "yyyy-DD-MM\'T\'HH:MM:SS"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1326
    .local v4, "formatter":Ljava/text/DateFormat;
    :try_start_0
    invoke-virtual {v4, p1}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 1327
    .local v0, "date":Ljava/util/Date;
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1332
    .end local v0    # "date":Ljava/util/Date;
    :goto_0
    return-wide v2

    .line 1329
    :catch_0
    move-exception v1

    .line 1330
    .local v1, "e":Ljava/text/ParseException;
    invoke-virtual {v1}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0
.end method

.method private NodeName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 2048
    const/16 v1, 0x2f

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 2049
    .local v0, "ind":I
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->isToContinue()Z

    move-result v0

    return v0
.end method

.method private dd(Ljava/lang/String;)V
    .locals 1
    .param p1, "out"    # Ljava/lang/String;

    .prologue
    .line 1623
    sget-boolean v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 1624
    const-string v0, "BaiduAPI"

    invoke-static {v0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1625
    :cond_0
    return-void
.end method

.method private doBackOff(I)V
    .locals 8
    .param p1, "numTry"    # I

    .prologue
    .line 1652
    if-ltz p1, :cond_0

    const/4 v1, 0x5

    if-le p1, v1, :cond_1

    .line 1659
    :cond_0
    :goto_0
    return-void

    .line 1653
    :cond_1
    const-wide/16 v4, 0x3e8

    rem-int/lit8 v1, p1, 0x5

    add-int/lit8 v1, v1, 0x1

    xor-int/lit8 v1, v1, 0x2

    int-to-long v6, v1

    mul-long v2, v4, v6

    .line 1655
    .local v2, "ms":J
    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1656
    :catch_0
    move-exception v0

    .line 1657
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 90
    const-class v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->mInstance:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->mInstance:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    :goto_0
    monitor-exit v1

    return-object v0

    .line 91
    :cond_0
    :try_start_1
    new-instance v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->mInstance:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    .line 92
    sget-object v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->mInstance:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 90
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getMediaType(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 2053
    const/4 v1, 0x0

    const-string v2, "/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2054
    .local v0, "type":Ljava/lang/String;
    return-object v0
.end method

.method private handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V
    .locals 5
    .param p1, "e"    # Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 1708
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleBaiduExceptions:  error_code = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1710
    iget v1, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    .line 1711
    .local v1, "errno":I
    const-string v0, "handleBaiduExceptions"

    .line 1713
    .local v0, "callerMethodName":Ljava/lang/String;
    sput v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->baiduErrCode:I

    .line 1715
    const/16 v2, 0x6e

    if-ne v1, v2, :cond_0

    .line 1716
    new-instance v2, Lcom/samsung/scloud/exception/SCloudAuthException;

    const-string v3, "server token invalid"

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudAuthException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1718
    :cond_0
    const/16 v2, 0x6f

    if-ne v1, v2, :cond_1

    .line 1719
    new-instance v2, Lcom/samsung/scloud/exception/SCloudAuthException;

    const-string v3, "server token expired"

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudAuthException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1722
    :cond_1
    new-instance v2, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private isRetryNeeded(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)Z
    .locals 3
    .param p1, "e"    # Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    .prologue
    .line 1663
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isRetryNeeded:  error_code = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1665
    const/4 v0, 0x0

    .line 1667
    .local v0, "value":Z
    iget v1, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    sparse-switch v1, :sswitch_data_0

    .line 1703
    :goto_0
    return v0

    .line 1697
    :sswitch_0
    const/4 v0, 0x1

    .line 1698
    goto :goto_0

    .line 1667
    nop

    :sswitch_data_0
    .sparse-switch
        0x7919 -> :sswitch_0
        0x791a -> :sswitch_0
        0x791b -> :sswitch_0
        0x792d -> :sswitch_0
        0x792e -> :sswitch_0
        0x7931 -> :sswitch_0
        0x795b -> :sswitch_0
        0x795c -> :sswitch_0
        0x795d -> :sswitch_0
        0x795e -> :sswitch_0
        0x795f -> :sswitch_0
        0x7960 -> :sswitch_0
        0x7961 -> :sswitch_0
        0x7969 -> :sswitch_0
        0x796a -> :sswitch_0
        0x796b -> :sswitch_0
        0x797d -> :sswitch_0
        0x797e -> :sswitch_0
        0x797f -> :sswitch_0
        0x79a5 -> :sswitch_0
        0x79e9 -> :sswitch_0
        0x79ec -> :sswitch_0
        0x79ed -> :sswitch_0
        0x79ee -> :sswitch_0
        0x79ef -> :sswitch_0
        0x79f0 -> :sswitch_0
        0x79f1 -> :sswitch_0
        0x7a42 -> :sswitch_0
        0x7a43 -> :sswitch_0
    .end sparse-switch
.end method

.method private isShipMode()Z
    .locals 7

    .prologue
    .line 1628
    const/4 v3, 0x0

    .line 1630
    .local v3, "result":Z
    const/4 v1, 0x0

    .line 1632
    .local v1, "method":Ljava/lang/reflect/Method;
    :try_start_0
    const-class v4, Landroid/os/Debug;

    const-string v5, "isProductShip"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Class;

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 1633
    const/4 v4, 0x0

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v2

    .line 1635
    .local v2, "mode":I
    const/4 v4, 0x1

    if-ne v2, v4, :cond_0

    .line 1636
    const/4 v3, 0x1

    .line 1648
    .end local v2    # "mode":I
    :cond_0
    :goto_0
    return v3

    .line 1638
    :catch_0
    move-exception v0

    .line 1639
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 1640
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 1641
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 1642
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 1643
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 1644
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 1645
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method

.method private isToContinue()Z
    .locals 2

    .prologue
    .line 1614
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isToContinue isProgressContinue : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->isContinued:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1616
    sget-boolean v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->isContinued:Z

    return v0
.end method

.method private parseAlbum(Lcom/dropbox/client2/SamsungDropboxAPI$Album;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 1
    .param p1, "album"    # Lcom/dropbox/client2/SamsungDropboxAPI$Album;

    .prologue
    .line 2089
    const-string v0, "parseAlbum"

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2090
    const/4 v0, 0x0

    return-object v0
.end method

.method private parseAlbumEntry(Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;)Lcom/samsung/scloud/data/SCloudNode;
    .locals 2
    .param p1, "metadata"    # Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;

    .prologue
    .line 1179
    new-instance v0, Lcom/samsung/scloud/data/SCloudFile;

    invoke-direct {v0}, Lcom/samsung/scloud/data/SCloudFile;-><init>()V

    .line 1180
    .local v0, "ret":Lcom/samsung/scloud/data/SCloudNode;
    const-string v1, "parseAlbumEntry"

    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1250
    return-object v0
.end method

.method private parseAlbumMetadata(Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 1
    .param p1, "metadata"    # Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;

    .prologue
    .line 2067
    const-string v0, "parseAlbumMetadata"

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2068
    const/4 v0, 0x0

    return-object v0
.end method

.method private parseEntry(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;)Lcom/samsung/scloud/data/SCloudNode;
    .locals 6
    .param p1, "e"    # Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    .prologue
    const-wide/16 v4, 0x3e8

    .line 1726
    const/4 v1, 0x0

    .line 1730
    .local v1, "ret":Lcom/samsung/scloud/data/SCloudNode;
    iget-boolean v2, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;->isDir:Z

    if-eqz v2, :cond_0

    .line 1731
    new-instance v1, Lcom/samsung/scloud/data/SCloudFolder;

    .end local v1    # "ret":Lcom/samsung/scloud/data/SCloudNode;
    invoke-direct {v1}, Lcom/samsung/scloud/data/SCloudFolder;-><init>()V

    .line 1732
    .restart local v1    # "ret":Lcom/samsung/scloud/data/SCloudNode;
    sget-object v2, Lcom/samsung/scloud/data/SCloudNode;->TYPE_FOLDER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/scloud/data/SCloudNode;->setNodeType(Ljava/lang/String;)V

    .line 1752
    :goto_0
    iget-object v2, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;->path:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/scloud/data/SCloudNode;->setId(Ljava/lang/String;)V

    .line 1753
    iget-wide v2, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;->size:J

    invoke-virtual {v1, v2, v3}, Lcom/samsung/scloud/data/SCloudNode;->setSize(J)V

    .line 1756
    iget-object v2, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;->path:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/scloud/data/SCloudNode;->setAbsolutePath(Ljava/lang/String;)V

    .line 1757
    iget-object v2, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;->path:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->NodeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/scloud/data/SCloudNode;->setName(Ljava/lang/String;)V

    .line 1758
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/scloud/data/SCloudNode;->setThumbExists(Z)V

    .line 1759
    iget-wide v2, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;->mTime:J

    mul-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Lcom/samsung/scloud/data/SCloudNode;->setUpdatedTimestamp(J)V

    .line 1767
    return-object v1

    .line 1736
    :cond_0
    new-instance v1, Lcom/samsung/scloud/data/SCloudFile;

    .end local v1    # "ret":Lcom/samsung/scloud/data/SCloudNode;
    invoke-direct {v1}, Lcom/samsung/scloud/data/SCloudFile;-><init>()V

    .line 1737
    .restart local v1    # "ret":Lcom/samsung/scloud/data/SCloudNode;
    sget-object v2, Lcom/samsung/scloud/data/SCloudNode;->TYPE_FILE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/scloud/data/SCloudNode;->setNodeType(Ljava/lang/String;)V

    .line 1738
    iget-object v2, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;->path:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeTypeFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1740
    .local v0, "mimeType":Ljava/lang/String;
    if-eqz v0, :cond_1

    move-object v2, v1

    .line 1741
    check-cast v2, Lcom/samsung/scloud/data/SCloudFile;

    invoke-virtual {v2, v0}, Lcom/samsung/scloud/data/SCloudFile;->setMimeType(Ljava/lang/String;)V

    move-object v2, v1

    .line 1742
    check-cast v2, Lcom/samsung/scloud/data/SCloudFile;

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getMediaType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/scloud/data/SCloudFile;->setMediaType(Ljava/lang/String;)V

    :goto_1
    move-object v2, v1

    .line 1747
    check-cast v2, Lcom/samsung/scloud/data/SCloudFile;

    iget-object v3, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;->blockList:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/samsung/scloud/data/SCloudFile;->setMd5Checksum(Ljava/lang/String;)V

    .line 1748
    iget-wide v2, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;->cTime:J

    mul-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Lcom/samsung/scloud/data/SCloudNode;->setCreatedTimestamp(J)V

    goto :goto_0

    :cond_1
    move-object v2, v1

    .line 1744
    check-cast v2, Lcom/samsung/scloud/data/SCloudFile;

    const-string v3, "etc"

    invoke-virtual {v2, v3}, Lcom/samsung/scloud/data/SCloudFile;->setMimeType(Ljava/lang/String;)V

    move-object v2, v1

    .line 1745
    check-cast v2, Lcom/samsung/scloud/data/SCloudFile;

    const-string v3, "etc"

    invoke-virtual {v2, v3}, Lcom/samsung/scloud/data/SCloudFile;->setMediaType(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private parseList(Ljava/util/List;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;",
            ">;)",
            "Lcom/samsung/scloud/data/SCloudFile;"
        }
    .end annotation

    .prologue
    .line 2002
    .local p1, "infoList":Ljava/util/List;, "Ljava/util/List<Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;>;"
    const-string v4, "parseList"

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2003
    if-nez p1, :cond_1

    const/4 v2, 0x0

    .line 2021
    :cond_0
    return-object v2

    .line 2005
    :cond_1
    new-instance v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;-><init>()V

    .line 2006
    .local v0, "data1":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;
    const/4 v2, 0x0

    .line 2008
    .local v2, "file":Lcom/samsung/scloud/data/SCloudFile;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "infoList.size()="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2010
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 2011
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "data1":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;
    check-cast v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;

    .line 2012
    .restart local v0    # "data1":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parseList to="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;->to:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2015
    :try_start_0
    iget-object v4, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;->to:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getFileInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFile;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 2010
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2016
    :catch_0
    move-exception v1

    .line 2017
    .local v1, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v1}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    goto :goto_1
.end method

.method private parseListFolder(Ljava/util/List;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;",
            ">;)",
            "Lcom/samsung/scloud/data/SCloudFolder;"
        }
    .end annotation

    .prologue
    .line 1979
    .local p1, "infoList":Ljava/util/List;, "Ljava/util/List<Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;>;"
    const-string v4, "parseListFolder"

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1980
    if-nez p1, :cond_1

    const/4 v2, 0x0

    .line 1998
    :cond_0
    return-object v2

    .line 1982
    :cond_1
    new-instance v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;-><init>()V

    .line 1983
    .local v0, "data1":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;
    const/4 v2, 0x0

    .line 1985
    .local v2, "folder":Lcom/samsung/scloud/data/SCloudFolder;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "infoList.size()="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1987
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 1988
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "data1":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;
    check-cast v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;

    .line 1989
    .restart local v0    # "data1":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parseListFolder to="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;->to:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1992
    :try_start_0
    iget-object v4, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;->to:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getFolderInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1987
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1993
    :catch_0
    move-exception v1

    .line 1994
    .local v1, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v1}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    goto :goto_1
.end method

.method private parseListToFile(Ljava/util/List;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 6
    .param p1, "list"    # Ljava/util/List;

    .prologue
    .line 1799
    const-string v4, "parseListToFile"

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1800
    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 1813
    :cond_0
    return-object v0

    .line 1802
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 1803
    .local v3, "size":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "size="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1804
    const/4 v0, 0x0

    .line 1805
    .local v0, "file":Lcom/samsung/scloud/data/SCloudFile;
    const/4 v2, 0x0

    .line 1807
    .local v2, "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 1809
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;
    check-cast v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    .line 1810
    .restart local v2    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;
    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->parseEntry(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v0

    .end local v0    # "file":Lcom/samsung/scloud/data/SCloudFile;
    check-cast v0, Lcom/samsung/scloud/data/SCloudFile;

    .line 1807
    .restart local v0    # "file":Lcom/samsung/scloud/data/SCloudFile;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private parseListToFolder(Ljava/util/List;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 7
    .param p1, "list"    # Ljava/util/List;

    .prologue
    .line 1773
    const-string v6, "parseListToFolder"

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1774
    if-nez p1, :cond_0

    const/4 v2, 0x0

    .line 1795
    :goto_0
    return-object v2

    .line 1776
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    .line 1777
    .local v5, "size":I
    const/4 v4, 0x0

    .line 1779
    .local v4, "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;
    new-instance v2, Lcom/samsung/scloud/data/SCloudFolder;

    invoke-direct {v2}, Lcom/samsung/scloud/data/SCloudFolder;-><init>()V

    .line 1780
    .local v2, "folder":Lcom/samsung/scloud/data/SCloudFolder;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1782
    .local v1, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudFile;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v5, :cond_1

    .line 1783
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;
    check-cast v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    .line 1784
    .restart local v4    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;
    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->parseEntry(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v0

    check-cast v0, Lcom/samsung/scloud/data/SCloudFile;

    .line 1785
    .local v0, "file":Lcom/samsung/scloud/data/SCloudFile;
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1782
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1793
    .end local v0    # "file":Lcom/samsung/scloud/data/SCloudFile;
    :cond_1
    invoke-virtual {v2, v1}, Lcom/samsung/scloud/data/SCloudFolder;->setFiles(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private parseMusicEntry(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;)Lcom/samsung/scloud/data/SCloudNode;
    .locals 10
    .param p1, "meta"    # Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;

    .prologue
    const-wide/16 v6, 0x3e8

    const-wide/16 v8, 0x0

    .line 1033
    new-instance v3, Lcom/samsung/scloud/data/SCloudFile;

    invoke-direct {v3}, Lcom/samsung/scloud/data/SCloudFile;-><init>()V

    .line 1035
    .local v3, "ret":Lcom/samsung/scloud/data/SCloudNode;
    const-string v4, "parseMusicEntry"

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1037
    if-eqz p1, :cond_0

    iget-object v4, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    if-eqz v4, :cond_0

    move-object v4, v3

    .line 1038
    check-cast v4, Lcom/samsung/scloud/data/SCloudFile;

    iget-object v5, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    iget-object v5, v5, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;->blockList:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/samsung/scloud/data/SCloudFile;->setMd5Checksum(Ljava/lang/String;)V

    .line 1039
    sget-object v4, Lcom/samsung/scloud/data/SCloudNode;->TYPE_FILE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/scloud/data/SCloudNode;->setNodeType(Ljava/lang/String;)V

    .line 1040
    iget-object v4, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    iget-object v4, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;->path:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeTypeFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1041
    .local v2, "mimeType":Ljava/lang/String;
    if-eqz v2, :cond_2

    move-object v4, v3

    .line 1042
    check-cast v4, Lcom/samsung/scloud/data/SCloudFile;

    invoke-virtual {v4, v2}, Lcom/samsung/scloud/data/SCloudFile;->setMimeType(Ljava/lang/String;)V

    move-object v4, v3

    .line 1043
    check-cast v4, Lcom/samsung/scloud/data/SCloudFile;

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getMediaType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/scloud/data/SCloudFile;->setMediaType(Ljava/lang/String;)V

    .line 1048
    :goto_0
    iget-object v4, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    iget-wide v4, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;->cTime:J

    mul-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Lcom/samsung/scloud/data/SCloudNode;->setCreatedTimestamp(J)V

    .line 1052
    iget-object v4, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    iget-object v4, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;->path:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/scloud/data/SCloudNode;->setId(Ljava/lang/String;)V

    .line 1053
    iget-object v4, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    iget-wide v4, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;->size:J

    invoke-virtual {v3, v4, v5}, Lcom/samsung/scloud/data/SCloudNode;->setSize(J)V

    .line 1055
    iget-object v4, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    iget-wide v4, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;->mTime:J

    mul-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Lcom/samsung/scloud/data/SCloudNode;->setUpdatedTimestamp(J)V

    .line 1056
    iget-object v4, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    iget-object v4, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;->path:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/scloud/data/SCloudNode;->setAbsolutePath(Ljava/lang/String;)V

    .line 1057
    iget-object v4, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    iget-object v4, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;->path:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->NodeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/scloud/data/SCloudNode;->setName(Ljava/lang/String;)V

    .line 1058
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/samsung/scloud/data/SCloudNode;->setThumbExists(Z)V

    .line 1061
    .end local v2    # "mimeType":Ljava/lang/String;
    :cond_0
    if-eqz p1, :cond_1

    .line 1064
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parseMusicEntry Meta: meta type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->type:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse$MediaType;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1065
    sget-object v4, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI$3;->$SwitchMap$com$baidu$pcs$BaiduPCSActionInfo$PCSMetaResponse$MediaType:[I

    iget-object v5, p1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->type:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse$MediaType;

    invoke-virtual {v5}, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse$MediaType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1112
    :cond_1
    :goto_1
    return-object v3

    .restart local v2    # "mimeType":Ljava/lang/String;
    :cond_2
    move-object v4, v3

    .line 1045
    check-cast v4, Lcom/samsung/scloud/data/SCloudFile;

    const-string v5, "etc"

    invoke-virtual {v4, v5}, Lcom/samsung/scloud/data/SCloudFile;->setMimeType(Ljava/lang/String;)V

    move-object v4, v3

    .line 1046
    check-cast v4, Lcom/samsung/scloud/data/SCloudFile;

    const-string v5, "etc"

    invoke-virtual {v4, v5}, Lcom/samsung/scloud/data/SCloudFile;->setMediaType(Ljava/lang/String;)V

    goto :goto_0

    .line 1067
    .end local v2    # "mimeType":Ljava/lang/String;
    :pswitch_0
    const-string v4, "parseMusicEntry Meta: AUDIO"

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    move-object v0, p1

    .line 1068
    check-cast v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;

    .line 1069
    .local v0, "audioInfo":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;
    if-eqz v0, :cond_1

    .line 1070
    const-string v4, "parseMusicEntry Meta: not null 0"

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1071
    new-instance v1, Lcom/samsung/scloud/data/AudioMetaData;

    invoke-direct {v1}, Lcom/samsung/scloud/data/AudioMetaData;-><init>()V

    .line 1072
    .local v1, "audioMetaData":Lcom/samsung/scloud/data/AudioMetaData;
    iget-object v4, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->albumTitle:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 1073
    iget-object v4, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->albumTitle:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/samsung/scloud/data/AudioMetaData;->setAlbumTitle(Ljava/lang/String;)V

    .line 1074
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parseMusicEntry Meta: not null 1 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->albumTitle:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1076
    :cond_3
    iget-object v4, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->artistName:Ljava/lang/String;

    if-eqz v4, :cond_4

    .line 1077
    iget-object v4, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->artistName:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/samsung/scloud/data/AudioMetaData;->setArtistName(Ljava/lang/String;)V

    .line 1078
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parseMusicEntry Meta: not null 2 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->artistName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1080
    :cond_4
    iget-wide v4, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->duration:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 1081
    iget-wide v4, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->duration:J

    invoke-virtual {v1, v4, v5}, Lcom/samsung/scloud/data/AudioMetaData;->setDuration(J)V

    .line 1082
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parseMusicEntry Meta: not null 3 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->duration:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1084
    :cond_5
    iget-object v4, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->genre:Ljava/lang/String;

    if-eqz v4, :cond_6

    .line 1085
    iget-object v4, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->genre:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/samsung/scloud/data/AudioMetaData;->setGenre(Ljava/lang/String;)V

    .line 1086
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parseMusicEntry Meta: not null 4 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->genre:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1088
    :cond_6
    iget-object v4, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->trackTitle:Ljava/lang/String;

    if-eqz v4, :cond_7

    .line 1089
    iget-object v4, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->trackTitle:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/samsung/scloud/data/AudioMetaData;->setTrackTitle(Ljava/lang/String;)V

    .line 1090
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parseMusicEntry Meta: not null 5 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->trackTitle:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1093
    :cond_7
    iget-wide v4, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->trackNumber:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 1094
    iget-wide v4, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->trackNumber:J

    cmp-long v4, v4, v8

    if-gez v4, :cond_9

    .line 1095
    invoke-virtual {v1, v8, v9}, Lcom/samsung/scloud/data/AudioMetaData;->setTrackNumber(J)V

    .line 1096
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parseMusicEntry Meta: not null 6 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->trackNumber:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    :cond_8
    :goto_2
    move-object v4, v3

    .line 1102
    check-cast v4, Lcom/samsung/scloud/data/SCloudFile;

    invoke-virtual {v4, v1}, Lcom/samsung/scloud/data/SCloudFile;->setAudioMetaData(Lcom/samsung/scloud/data/AudioMetaData;)V

    goto/16 :goto_1

    .line 1098
    :cond_9
    iget-wide v4, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->trackNumber:J

    invoke-virtual {v1, v4, v5}, Lcom/samsung/scloud/data/AudioMetaData;->setTrackNumber(J)V

    .line 1099
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parseMusicEntry Meta: not null 7 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->trackNumber:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    goto :goto_2

    .line 1065
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private putNewFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 6
    .param p1, "sourceFile"    # Ljava/io/File;
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "destinationFolderIdOrPath"    # Ljava/lang/String;
    .param p4, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2149
    const/4 v0, 0x0

    .line 2151
    .local v0, "authToken":Ljava/lang/String;
    const-string v2, "putNewFile"

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2154
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getAuthToken()Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2160
    :goto_0
    if-eqz v0, :cond_2

    .line 2161
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v2, v0}, Lcom/baidu/pcs/BaiduPCSClient;->setAccessToken(Ljava/lang/String;)V

    .line 2162
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5, v3, v3}, Lcom/baidu/pcs/BaiduPCSClient;->uploadFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;

    move-result-object v1

    .line 2164
    .local v1, "ret":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;
    if-eqz v1, :cond_2

    .line 2165
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "putNewFile:  "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v2, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v2, :cond_0

    const-string v2, "success"

    :goto_1
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2166
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v2, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v2, :cond_1

    .line 2167
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->parseEntry(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v2

    check-cast v2, Lcom/samsung/scloud/data/SCloudFile;

    .line 2217
    .end local v1    # "ret":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;
    :goto_2
    return-object v2

    .line 2165
    .restart local v1    # "ret":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v5, v5, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 2170
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "putNewFile: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v4, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2171
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    .end local v1    # "ret":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;
    :cond_2
    move-object v2, v3

    .line 2217
    goto :goto_2

    .line 2156
    :catch_0
    move-exception v2

    goto/16 :goto_0
.end method


# virtual methods
.method public addComment(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/Comment;
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2985
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public addPushTrigger(Lcom/dropbox/client2/SamsungDropboxAPI$Hook;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "hook_type"    # Lcom/dropbox/client2/SamsungDropboxAPI$Hook;
    .param p2, "deltaCursor"    # Ljava/lang/String;
    .param p3, "registrationId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 1565
    const/4 v0, 0x0

    return-object v0
.end method

.method public copy(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/scloud/data/SCloudNode;
    .locals 4
    .param p1, "fromPath"    # Ljava/lang/String;
    .param p2, "toPath"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 2442
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v2, v3, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v2, v3, :cond_1

    .line 2443
    :cond_0
    new-instance v2, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v2}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v2

    .line 2445
    :cond_1
    const/4 v0, 0x0

    .line 2447
    .local v0, "authToken":Ljava/lang/String;
    const-string v2, "copy"

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2450
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getAuthToken()Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2456
    :goto_0
    if-eqz v0, :cond_3

    .line 2457
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v2, v0}, Lcom/baidu/pcs/BaiduPCSClient;->setAccessToken(Ljava/lang/String;)V

    .line 2459
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v2, p1, p2}, Lcom/baidu/pcs/BaiduPCSClient;->copy(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;

    move-result-object v1

    .line 2461
    .local v1, "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;
    if-eqz v1, :cond_2

    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v2, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v2, :cond_2

    .line 2462
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;->list:Ljava/util/List;

    if-eqz v2, :cond_3

    .line 2463
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;->list:Ljava/util/List;

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->parseList(Ljava/util/List;)Lcom/samsung/scloud/data/SCloudFile;

    move-result-object v2

    .line 2471
    .end local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;
    :goto_1
    return-object v2

    .line 2467
    .restart local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "copy failed: from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v3, v3, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2468
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    .line 2471
    .end local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 2452
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public createRepository(Lcom/samsung/scloud/data/Repository;)Lcom/samsung/scloud/data/Repository;
    .locals 1
    .param p1, "repo"    # Lcom/samsung/scloud/data/Repository;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2913
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public createRepository(Ljava/lang/String;J)Lcom/samsung/scloud/data/Repository;
    .locals 1
    .param p1, "repositoryName"    # Ljava/lang/String;
    .param p2, "spaceAmount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2905
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public createShareURL(Ljava/lang/String;Ljava/lang/String;J)Lcom/samsung/scloud/data/ShareInfo;
    .locals 7
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "endTimestamp"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2576
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    if-ge v3, v4, :cond_1

    .line 2577
    :cond_0
    new-instance v3, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v3}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v3

    .line 2579
    :cond_1
    const/4 v0, 0x0

    .line 2580
    .local v0, "authToken":Ljava/lang/String;
    const/4 v2, 0x0

    .line 2582
    .local v2, "shareInfo":Lcom/samsung/scloud/data/ShareInfo;
    const-string v3, "createShareURL"

    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2585
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getAuthToken()Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2591
    :goto_0
    if-eqz v0, :cond_2

    .line 2592
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v3, v0}, Lcom/baidu/pcs/BaiduPCSClient;->setAccessToken(Ljava/lang/String;)V

    .line 2593
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v3, p1}, Lcom/baidu/pcs/BaiduPCSClient;->createFileLink(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;

    move-result-object v1

    .line 2595
    .local v1, "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;
    if-eqz v1, :cond_2

    iget-object v3, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    if-eqz v3, :cond_2

    .line 2596
    iget-object v3, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v3, v3, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v3, :cond_3

    .line 2597
    iget-object v3, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;->links:Ljava/util/List;

    if-eqz v3, :cond_2

    iget-object v3, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;->links:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 2598
    new-instance v2, Lcom/samsung/scloud/data/ShareInfo;

    .end local v2    # "shareInfo":Lcom/samsung/scloud/data/ShareInfo;
    invoke-direct {v2}, Lcom/samsung/scloud/data/ShareInfo;-><init>()V

    .line 2599
    .restart local v2    # "shareInfo":Lcom/samsung/scloud/data/ShareInfo;
    iget-object v3, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;->links:Ljava/util/List;

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/samsung/scloud/data/ShareInfo;->setShareURL(Ljava/lang/String;)V

    .line 2601
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createShareURL success : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v3, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;->links:Ljava/util/List;

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2611
    .end local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;
    :cond_2
    :goto_1
    return-object v2

    .line 2605
    .restart local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createShareURL failed : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v4, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2606
    iget-object v3, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    goto :goto_1

    .line 2587
    .end local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;
    :catch_0
    move-exception v3

    goto/16 :goto_0
.end method

.method public delete(Ljava/lang/String;)Z
    .locals 4
    .param p1, "fileOrFolderPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 376
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v3, v2, :cond_1

    :cond_0
    new-instance v2, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v2}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v2

    .line 378
    :cond_1
    const/4 v0, 0x0

    .line 380
    .local v0, "authToken":Ljava/lang/String;
    const-string v3, "delete"

    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 383
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getAuthToken()Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 389
    :goto_0
    if-eqz v0, :cond_3

    .line 390
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v3, v0}, Lcom/baidu/pcs/BaiduPCSClient;->setAccessToken(Ljava/lang/String;)V

    .line 391
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v3, p1}, Lcom/baidu/pcs/BaiduPCSClient;->deleteFile(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    move-result-object v1

    .line 393
    .local v1, "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    if-eqz v1, :cond_3

    .line 394
    iget v3, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v3, :cond_2

    .line 403
    .end local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    :goto_1
    return v2

    .line 398
    .restart local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "delete failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 399
    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    .line 403
    .end local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 385
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public deleteDeviceProfile()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2897
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public deleteRepository(Ljava/lang/String;)V
    .locals 1
    .param p1, "repositoryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2961
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public editComment(Ljava/lang/String;JLjava/lang/String;)Lcom/samsung/scloud/data/Comment;
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .param p2, "commentID"    # J
    .param p4, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2977
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public finishAuth()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 1506
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getAuthToken()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 1467
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->baiduErrCode:I

    .line 1469
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->accessToken:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1470
    const-string v0, "getAuthToken:accessToken is null"

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1471
    const/4 v0, 0x0

    .line 1476
    :goto_0
    return-object v0

    .line 1474
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getAuthToken:accessToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->accessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1476
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->accessToken:Ljava/lang/String;

    goto :goto_0
.end method

.method public getCollaboration(Ljava/lang/String;)Lcom/samsung/scloud/data/Collaboration;
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 3017
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getCollaborations()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Collaboration;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 3033
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getComments(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Comment;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2969
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getDeltaList(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .locals 8
    .param p1, "cursor"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 1347
    const/4 v4, 0x0

    .line 1349
    .local v4, "ret":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;
    new-instance v2, Lcom/samsung/scloud/data/SCloudNodeList;

    invoke-direct {v2}, Lcom/samsung/scloud/data/SCloudNodeList;-><init>()V

    .line 1350
    .local v2, "nodeList":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudNode;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1352
    .local v3, "nodes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudNode;>;"
    const-string v6, "BaiduAPI--getDeltaList"

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1354
    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v6, p1}, Lcom/baidu/pcs/BaiduPCSClient;->diff(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;

    move-result-object v4

    .line 1356
    if-eqz v4, :cond_5

    .line 1357
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "BaiduAPI--ret.status.errorCode: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v7, v7, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1358
    iget-object v6, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v6, v6, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v6, :cond_4

    .line 1359
    const-string v6, "getDeltaList No_Error"

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1360
    iget-object v6, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;->cursor:Ljava/lang/String;

    invoke-virtual {v2, v6}, Lcom/samsung/scloud/data/SCloudNodeList;->setCursor(Ljava/lang/String;)V

    .line 1361
    iget-boolean v6, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;->hasMore:Z

    invoke-virtual {v2, v6}, Lcom/samsung/scloud/data/SCloudNodeList;->setHasMore(Z)V

    .line 1362
    iget-boolean v6, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;->isReseted:Z

    invoke-virtual {v2, v6}, Lcom/samsung/scloud/data/SCloudNodeList;->setReset(Z)V

    .line 1363
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ret.hasMore:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;->hasMore:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", ret.isReseted:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;->isReseted:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1364
    const/4 v5, 0x0

    .line 1365
    .local v5, "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    iget-object v6, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;->entries:Ljava/util/List;

    if-eqz v6, :cond_3

    .line 1366
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getDeltaList ret.entries is not null ret.entries.size()="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;->entries:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1367
    iget-object v6, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;->entries:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDifferEntryInfo;

    .line 1368
    .local v0, "entry":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDifferEntryInfo;
    const-string v6, "getDeltaList  for loop"

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1369
    if-eqz v0, :cond_0

    .line 1370
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getDeltaList entry.commonFileInfo.path: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDifferEntryInfo;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    iget-object v7, v7, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;->path:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1372
    iget-boolean v6, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDifferEntryInfo;->isDeleted:Z

    if-eqz v6, :cond_1

    .line 1373
    const-string v6, "getDeltaList ***metadata is null***"

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1379
    new-instance v5, Lcom/samsung/scloud/data/SCloudFile;

    .end local v5    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    invoke-direct {v5}, Lcom/samsung/scloud/data/SCloudFile;-><init>()V

    .line 1380
    .restart local v5    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    iget-object v6, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDifferEntryInfo;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    iget-object v6, v6, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;->path:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/samsung/scloud/data/SCloudNode;->setId(Ljava/lang/String;)V

    .line 1381
    iget-object v6, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDifferEntryInfo;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    iget-object v6, v6, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;->path:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/samsung/scloud/data/SCloudNode;->setAbsolutePath(Ljava/lang/String;)V

    .line 1382
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/samsung/scloud/data/SCloudNode;->setDeleted(Z)V

    .line 1388
    :cond_0
    :goto_1
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1383
    :cond_1
    iget-object v6, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDifferEntryInfo;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    if-eqz v6, :cond_0

    .line 1384
    const-string v6, "entry.commonFileInfo is not null"

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1385
    iget-object v6, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDifferEntryInfo;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->parseEntry(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v5

    goto :goto_1

    .line 1390
    .end local v0    # "entry":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDifferEntryInfo;
    :cond_2
    const-string v6, "getDeltaList end for loop"

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1392
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_3
    const-string v6, "getDeltaList setNodes"

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1393
    invoke-virtual {v2, v3}, Lcom/samsung/scloud/data/SCloudNodeList;->setNodes(Ljava/util/List;)V

    .line 1403
    .end local v5    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    :goto_2
    const-string v6, "return nodeList"

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1404
    return-object v2

    .line 1396
    :cond_4
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getDeltaList failed: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v7, v7, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1397
    iget-object v6, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    goto :goto_2

    .line 1401
    :cond_5
    const-string v6, "ret is null"

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public getDeviceProfile()Lcom/samsung/scloud/data/Device;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2865
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getDeviceProfile(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2873
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getDownloadURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 1414
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_1

    :cond_0
    new-instance v2, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v2}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v2

    .line 1415
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDownloadURL "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1417
    const/4 v0, 0x0

    .line 1420
    .local v0, "authToken":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getAuthToken()Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1426
    :goto_0
    if-eqz v0, :cond_3

    .line 1427
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v2, v0}, Lcom/baidu/pcs/BaiduPCSClient;->setAccessToken(Ljava/lang/String;)V

    .line 1429
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v2, p1}, Lcom/baidu/pcs/BaiduPCSClient;->createFileLink(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;

    move-result-object v1

    .line 1430
    .local v1, "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;
    if-eqz v1, :cond_3

    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    if-eqz v2, :cond_3

    .line 1431
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v2, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v2, :cond_2

    .line 1432
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;->links:Ljava/util/List;

    if-eqz v2, :cond_3

    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;->links:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 1433
    const-string v2, "getDownloadURL Success"

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1434
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;->links:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1444
    .end local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;
    :goto_1
    return-object v2

    .line 1438
    .restart local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDownloadURL Failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v3, v3, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1439
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    .line 1444
    .end local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 1422
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public getErrorCode()I
    .locals 2

    .prologue
    .line 1601
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getErrorCode baiduErrCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->baiduErrCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1603
    sget v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->baiduErrCode:I

    return v0
.end method

.method public getFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)V
    .locals 1
    .param p1, "localFile"    # Ljava/io/File;
    .param p2, "serverFileIdOrPath"    # Ljava/lang/String;
    .param p3, "version"    # Ljava/lang/String;
    .param p4, "overwrite"    # Z
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2534
    const-string v0, "getFile - X"

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2535
    return-void
.end method

.method public getFile(Ljava/io/File;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)V
    .locals 5
    .param p1, "localFile"    # Ljava/io/File;
    .param p2, "serverFileIdOrPath"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .param p4, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 421
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_1

    :cond_0
    new-instance v2, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v2}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v2

    .line 423
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    if-nez p3, :cond_2

    .line 424
    new-instance v2, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v2}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v2

    .line 426
    :cond_2
    const-string v2, "getFile"

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 428
    const/4 v0, 0x0

    .line 431
    .local v0, "authToken":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getAuthToken()Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 437
    :goto_0
    if-nez v0, :cond_4

    .line 438
    const-string v2, "authToken is null"

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 442
    :goto_1
    if-eqz v0, :cond_3

    .line 443
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v2, v0}, Lcom/baidu/pcs/BaiduPCSClient;->setAccessToken(Ljava/lang/String;)V

    .line 444
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI$1;

    invoke-direct {v4, p0, p1, p4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI$1;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;Ljava/io/File;Lcom/samsung/scloud/response/ProgressListener;)V

    invoke-virtual {v2, p2, v3, v4}, Lcom/baidu/pcs/BaiduPCSClient;->downloadFile(Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    move-result-object v1

    .line 461
    .local v1, "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    if-eqz v1, :cond_3

    .line 462
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Download files:  info.errorCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", BaiduPCSErrorCode.No_Error="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 463
    iget v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v2, :cond_6

    .line 464
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Download files:  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v2, :cond_5

    const-string v2, "success"

    :goto_2
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 465
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Download files:  localFilePath="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", serverFilePath="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 473
    .end local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    :cond_3
    :goto_3
    return-void

    .line 440
    :cond_4
    const-string v2, "authToken is not null"

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 464
    .restart local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "failed  "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 468
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Download failed:  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 469
    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    goto :goto_3

    .line 433
    .end local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    :catch_0
    move-exception v2

    goto/16 :goto_0
.end method

.method public getFileInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 5
    .param p1, "filIdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 280
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_1

    :cond_0
    new-instance v2, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v2}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v2

    .line 281
    :cond_1
    const/4 v0, 0x0

    .line 283
    .local v0, "authToken":Ljava/lang/String;
    const-string v2, "getFileInfo"

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 286
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getAuthToken()Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 292
    :goto_0
    if-eqz v0, :cond_4

    .line 293
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v2, v0}, Lcom/baidu/pcs/BaiduPCSClient;->setAccessToken(Ljava/lang/String;)V

    .line 294
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v2, p1}, Lcom/baidu/pcs/BaiduPCSClient;->meta(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;

    move-result-object v1

    .line 295
    .local v1, "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;
    if-eqz v1, :cond_4

    .line 296
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Meta:  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v2, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v2, :cond_2

    const-string v2, "success"

    :goto_1
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 297
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v2, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v2, :cond_3

    .line 298
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->parseEntry(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v2

    check-cast v2, Lcom/samsung/scloud/data/SCloudFile;

    .line 307
    .end local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;
    :goto_2
    return-object v2

    .line 296
    .restart local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "failed"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v4, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 301
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Meta: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v3, v3, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 302
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    .line 307
    .end local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;
    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    .line 288
    :catch_0
    move-exception v2

    goto/16 :goto_0
.end method

.method public getFilesAsZip(Ljava/io/File;Ljava/util/List;ZLcom/samsung/scloud/response/ProgressListener;)V
    .locals 1
    .param p1, "localFile"    # Ljava/io/File;
    .param p3, "overwrite"    # Z
    .param p4, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "Lcom/samsung/scloud/response/ProgressListener;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2543
    .local p2, "IdOrpath":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v0, "getFilesAsZip"

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2545
    return-void
.end method

.method public getFolderInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 5
    .param p1, "folderIdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 198
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_1

    :cond_0
    new-instance v2, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v2}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v2

    .line 199
    :cond_1
    const/4 v0, 0x0

    .line 201
    .local v0, "authToken":Ljava/lang/String;
    const-string v2, "getFolderInfo"

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 204
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getAuthToken()Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 210
    :goto_0
    if-eqz v0, :cond_4

    .line 211
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v2, v0}, Lcom/baidu/pcs/BaiduPCSClient;->setAccessToken(Ljava/lang/String;)V

    .line 212
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v2, p1}, Lcom/baidu/pcs/BaiduPCSClient;->meta(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;

    move-result-object v1

    .line 213
    .local v1, "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;
    if-eqz v1, :cond_4

    .line 214
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Meta:  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v2, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v2, :cond_2

    const-string v2, "success"

    :goto_1
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 215
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v2, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v2, :cond_3

    .line 216
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->parseEntry(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v2

    check-cast v2, Lcom/samsung/scloud/data/SCloudFolder;

    .line 225
    .end local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;
    :goto_2
    return-object v2

    .line 214
    .restart local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "failed"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v4, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 219
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Meta: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v3, v3, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 220
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    .line 225
    .end local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;
    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    .line 206
    :catch_0
    move-exception v2

    goto/16 :goto_0
.end method

.method public getHooks(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/client2/SamsungDropboxAPI$Hook;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 1535
    const/4 v0, 0x0

    return-object v0
.end method

.method public getList(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 5
    .param p1, "folderIdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 238
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_1

    :cond_0
    new-instance v2, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v2}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v2

    .line 240
    :cond_1
    const/4 v0, 0x0

    .line 242
    .local v0, "authToken":Ljava/lang/String;
    const-string v2, "getList"

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 245
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getAuthToken()Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 251
    :goto_0
    if-eqz v0, :cond_4

    .line 252
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v2, v0}, Lcom/baidu/pcs/BaiduPCSClient;->setAccessToken(Ljava/lang/String;)V

    .line 253
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    const-string v3, "name"

    const-string v4, "asc"

    invoke-virtual {v2, p1, v3, v4}, Lcom/baidu/pcs/BaiduPCSClient;->list(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;

    move-result-object v1

    .line 254
    .local v1, "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;
    if-eqz v1, :cond_4

    .line 255
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getList:  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v2, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v2, :cond_2

    const-string v2, "success"

    :goto_1
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v3, v3, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 256
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v2, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v2, :cond_3

    .line 257
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;->list:Ljava/util/List;

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->parseListToFolder(Ljava/util/List;)Lcom/samsung/scloud/data/SCloudFolder;

    move-result-object v2

    .line 266
    .end local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;
    :goto_2
    return-object v2

    .line 255
    .restart local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;
    :cond_2
    const-string v2, "failed"

    goto :goto_1

    .line 260
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getList failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v3, v3, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 261
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    .line 266
    .end local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;
    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    .line 247
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public getMetadata(Ljava/lang/String;)Lcom/samsung/scloud/data/Metadata;
    .locals 10
    .param p1, "fileIdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2354
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x1

    if-ge v7, v8, :cond_1

    :cond_0
    new-instance v7, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v7}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v7

    .line 2356
    :cond_1
    const/4 v1, 0x0

    .line 2357
    .local v1, "authToken":Ljava/lang/String;
    const/4 v5, 0x0

    .line 2359
    .local v5, "ret":Lcom/samsung/scloud/data/Metadata;
    const-string v7, "getMetadata"

    invoke-direct {p0, v7}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2362
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getAuthToken()Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2368
    :goto_0
    if-eqz v1, :cond_3

    .line 2369
    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v7, v1}, Lcom/baidu/pcs/BaiduPCSClient;->setAccessToken(Ljava/lang/String;)V

    .line 2370
    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v7, p1}, Lcom/baidu/pcs/BaiduPCSClient;->meta(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;

    move-result-object v3

    .line 2371
    .local v3, "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;
    const/4 v4, 0x0

    .line 2372
    .local v4, "metadataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v3, :cond_3

    .line 2373
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getMetadata:  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v7, v3, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v7, v7, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v7, :cond_4

    const-string v7, "success"

    :goto_1
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2374
    iget-object v7, v3, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v7, v7, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v7, :cond_5

    .line 2375
    sget-object v7, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI$3;->$SwitchMap$com$baidu$pcs$BaiduPCSActionInfo$PCSMetaResponse$MediaType:[I

    iget-object v8, v3, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->type:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse$MediaType;

    invoke-virtual {v8}, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse$MediaType;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 2418
    :cond_2
    :goto_2
    new-instance v5, Lcom/samsung/scloud/data/Metadata;

    .end local v5    # "ret":Lcom/samsung/scloud/data/Metadata;
    invoke-direct {v5, v4}, Lcom/samsung/scloud/data/Metadata;-><init>(Ljava/util/Map;)V

    .line 2427
    .end local v3    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;
    .end local v4    # "metadataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v5    # "ret":Lcom/samsung/scloud/data/Metadata;
    :cond_3
    :goto_3
    return-object v5

    .line 2373
    .restart local v3    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;
    .restart local v4    # "metadataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_4
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "failed"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v9, v3, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v9, v9, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    :pswitch_0
    move-object v0, v3

    .line 2377
    check-cast v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;

    .line 2378
    .local v0, "audioInfo":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;
    if-eqz v0, :cond_2

    .line 2379
    const-string v7, "albumArt"

    iget-object v8, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->albumArt:Ljava/lang/String;

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2380
    const-string v7, "albumArtist"

    iget-object v8, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->albumArtist:Ljava/lang/String;

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2381
    const-string v7, "albumTitle"

    iget-object v8, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->albumTitle:Ljava/lang/String;

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2382
    const-string v7, "artistName"

    iget-object v8, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->artistName:Ljava/lang/String;

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2383
    const-string v7, "compilation"

    iget-object v8, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->compilation:Ljava/lang/String;

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2384
    const-string v7, "composer"

    iget-object v8, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->composer:Ljava/lang/String;

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2385
    const-string v7, "date"

    iget-object v8, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->date:Ljava/lang/String;

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2386
    const-string v7, "duration"

    iget-wide v8, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->duration:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2387
    const-string v7, "genre"

    iget-object v8, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->genre:Ljava/lang/String;

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2388
    const-string v7, "hasthumbnail"

    iget-boolean v8, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->hasthumbnail:Z

    invoke-static {v8}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2389
    const-string v7, "trackNumber"

    iget-wide v8, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->trackNumber:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2390
    const-string v7, "trackTitle"

    iget-object v8, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;->trackTitle:Ljava/lang/String;

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .end local v0    # "audioInfo":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;
    :pswitch_1
    move-object v6, v3

    .line 2395
    check-cast v6, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSVideoMetaResponse;

    .line 2396
    .local v6, "videoInfo":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSVideoMetaResponse;
    if-eqz v6, :cond_2

    .line 2397
    const-string v7, "category"

    iget-object v8, v6, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSVideoMetaResponse;->cateogry:Ljava/lang/String;

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2398
    const-string v7, "dateTaken"

    iget-wide v8, v6, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSVideoMetaResponse;->dateTaken:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2399
    const-string v7, "duration"

    iget-wide v8, v6, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSVideoMetaResponse;->duration:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2400
    const-string v7, "hasthumbnail"

    iget-boolean v8, v6, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSVideoMetaResponse;->hasthumbnail:Z

    invoke-static {v8}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2401
    const-string v7, "resolution"

    iget-object v8, v6, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSVideoMetaResponse;->resolution:Ljava/lang/String;

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .end local v6    # "videoInfo":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSVideoMetaResponse;
    :pswitch_2
    move-object v2, v3

    .line 2406
    check-cast v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSImageMetaResponse;

    .line 2407
    .local v2, "imageInfo":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSImageMetaResponse;
    if-eqz v2, :cond_2

    .line 2408
    const-string v7, "dateTaken"

    iget-wide v8, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSImageMetaResponse;->dateTaken:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2409
    const-string v7, "hasthumbnail"

    iget-boolean v8, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSImageMetaResponse;->hasthumbnail:Z

    invoke-static {v8}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2410
    const-string v7, "latitude"

    iget-wide v8, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSImageMetaResponse;->latitude:D

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2411
    const-string v7, "longitude"

    iget-wide v8, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSImageMetaResponse;->longtitude:D

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2412
    const-string v7, "resolution"

    iget-object v8, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSImageMetaResponse;->resolution:Ljava/lang/String;

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 2421
    .end local v2    # "imageInfo":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSImageMetaResponse;
    :cond_5
    iget-object v7, v3, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v7}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    goto/16 :goto_3

    .line 2364
    .end local v3    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;
    .end local v4    # "metadataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_0
    move-exception v7

    goto/16 :goto_0

    .line 2375
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getMusicCoverArt(Ljava/lang/String;Ljava/io/File;)V
    .locals 11
    .param p1, "serverFileIdOrPath"    # Ljava/lang/String;
    .param p2, "localFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    const/16 v10, 0x64

    .line 2624
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    const/4 v9, 0x1

    if-ge v8, v9, :cond_1

    .line 2626
    :cond_0
    new-instance v8, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v8}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v8

    .line 2628
    :cond_1
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2629
    new-instance v8, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v8}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v8

    .line 2631
    :cond_2
    const-string v8, "getMusicCoverArt"

    invoke-direct {p0, v8}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2633
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    .line 2634
    .local v4, "format":Landroid/graphics/Bitmap$CompressFormat;
    const/4 v0, 0x0

    .line 2635
    .local v0, "authToken":Ljava/lang/String;
    const/16 v7, 0x100

    .line 2636
    .local v7, "width":I
    const/16 v5, 0x100

    .line 2639
    .local v5, "height":I
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getAuthToken()Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_7

    move-result-object v0

    .line 2645
    :goto_0
    const/4 v3, 0x0

    .line 2648
    .local v3, "fileOut":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v3, Ljava/io/FileOutputStream;

    .end local v3    # "fileOut":Ljava/io/FileOutputStream;
    const/4 v8, 0x0

    invoke-direct {v3, p2, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2653
    .restart local v3    # "fileOut":Ljava/io/FileOutputStream;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getMusicCoverArt server path : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2655
    if-eqz v0, :cond_a

    .line 2656
    iget-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v8, v0}, Lcom/baidu/pcs/BaiduPCSClient;->setAccessToken(Ljava/lang/String;)V

    .line 2658
    iget-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v8, p1, v10, v7, v5}, Lcom/baidu/pcs/BaiduPCSClient;->getMusicCoverArt(Ljava/lang/String;III)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;

    move-result-object v6

    .line 2660
    .local v6, "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;
    if-eqz v6, :cond_6

    iget-object v8, v6, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v8, v8, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v8, :cond_6

    .line 2661
    const-string v8, "getMusicCoverArt success"

    invoke-direct {p0, v8}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2662
    iget-object v8, v6, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v8, :cond_4

    .line 2664
    :try_start_2
    iget-object v8, v6, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->bitmap:Landroid/graphics/Bitmap;

    const/16 v9, 0x64

    invoke-virtual {v8, v4, v9, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 2666
    if-eqz v3, :cond_3

    .line 2667
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 2720
    .end local v6    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;
    :cond_3
    :goto_1
    return-void

    .line 2649
    .end local v3    # "fileOut":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v1

    .line 2650
    .local v1, "e":Ljava/lang/Exception;
    new-instance v8, Lcom/samsung/scloud/exception/SCloudIOException;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 2669
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v3    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v6    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;
    :catch_1
    move-exception v1

    .line 2671
    .restart local v1    # "e":Ljava/lang/Exception;
    if-eqz v3, :cond_3

    .line 2672
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 2673
    :catch_2
    move-exception v2

    .line 2674
    .local v2, "e1":Ljava/io/IOException;
    new-instance v8, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v9, "file close failed"

    invoke-direct {v8, v9}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 2679
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "e1":Ljava/io/IOException;
    :cond_4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getMusicCoverArt   failed: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v6, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v9, v9, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "   "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v6, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v9, v9, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2681
    if-eqz v3, :cond_5

    .line 2682
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 2687
    :cond_5
    iget-object v8, v6, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v8}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    goto :goto_1

    .line 2683
    :catch_3
    move-exception v2

    .line 2684
    .restart local v2    # "e1":Ljava/io/IOException;
    new-instance v8, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v9, "file close failed"

    invoke-direct {v8, v9}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 2690
    .end local v2    # "e1":Ljava/io/IOException;
    :cond_6
    if-nez v6, :cond_8

    .line 2691
    const-string v8, "getMusicCoverArt   failed: info is null"

    invoke-direct {p0, v8}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2693
    if-eqz v3, :cond_7

    .line 2694
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 2699
    :cond_7
    new-instance v8, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v9, "no thumbnail"

    invoke-direct {v8, v9}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 2695
    :catch_4
    move-exception v2

    .line 2696
    .restart local v2    # "e1":Ljava/io/IOException;
    new-instance v8, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v9, "file close failed"

    invoke-direct {v8, v9}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 2701
    .end local v2    # "e1":Ljava/io/IOException;
    :cond_8
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getMusicCoverArt   failed: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v6, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v9, v9, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "   "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v6, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v9, v9, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2703
    if-eqz v3, :cond_9

    .line 2704
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 2709
    :cond_9
    iget-object v8, v6, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v8}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    goto/16 :goto_1

    .line 2705
    :catch_5
    move-exception v2

    .line 2706
    .restart local v2    # "e1":Ljava/io/IOException;
    new-instance v8, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v9, "file close failed"

    invoke-direct {v8, v9}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 2714
    .end local v2    # "e1":Ljava/io/IOException;
    .end local v6    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;
    :cond_a
    if-eqz v3, :cond_3

    .line 2715
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    goto/16 :goto_1

    .line 2716
    :catch_6
    move-exception v2

    .line 2717
    .restart local v2    # "e1":Ljava/io/IOException;
    new-instance v8, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v9, "file close failed"

    invoke-direct {v8, v9}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 2641
    .end local v2    # "e1":Ljava/io/IOException;
    .end local v3    # "fileOut":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v8

    goto/16 :goto_0
.end method

.method public getMyCloudProvider()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 1454
    const-string v0, "Baidu"

    return-object v0
.end method

.method public getMyContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1510
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getRecycleBin(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 3057
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getRepository(Ljava/lang/String;)Lcom/samsung/scloud/data/Repository;
    .locals 1
    .param p1, "repositoryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2929
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getRepositoryInfo()Lcom/samsung/scloud/data/RepositoryInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2921
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getShareURLInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/ShareInfo;
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2770
    const-string v0, "getShareURLInfo"

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2771
    const/4 v0, 0x0

    return-object v0
.end method

.method public getStreamingInfoForMusic(Ljava/lang/String;)Lcom/samsung/scloud/data/StreamingInfo;
    .locals 6
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2730
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x1

    if-ge v4, v5, :cond_1

    .line 2731
    :cond_0
    new-instance v4, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v4}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v4

    .line 2733
    :cond_1
    new-instance v3, Lcom/samsung/scloud/data/StreamingInfo;

    invoke-direct {v3}, Lcom/samsung/scloud/data/StreamingInfo;-><init>()V

    .line 2734
    .local v3, "streamData":Lcom/samsung/scloud/data/StreamingInfo;
    const/4 v0, 0x0

    .line 2735
    .local v0, "URL":Ljava/lang/String;
    const/4 v1, 0x0

    .line 2737
    .local v1, "authToken":Ljava/lang/String;
    const-string v4, "getStreamingInfoForMusic"

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2740
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getAuthToken()Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2746
    :goto_0
    if-eqz v1, :cond_2

    .line 2747
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v4, v1}, Lcom/baidu/pcs/BaiduPCSClient;->setAccessToken(Ljava/lang/String;)V

    .line 2748
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    const-string v5, "M3U8_AUDIO_128"

    invoke-virtual {v4, p1, v5}, Lcom/baidu/pcs/BaiduPCSClient;->getMusicStreamingURL(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;

    move-result-object v2

    .line 2750
    .local v2, "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;
    if-eqz v2, :cond_2

    .line 2751
    iget-object v4, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v4, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v4, :cond_3

    .line 2752
    iget-object v0, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;->url:Ljava/lang/String;

    .line 2753
    invoke-virtual {v3, v0}, Lcom/samsung/scloud/data/StreamingInfo;->setStreamUrl(Ljava/lang/String;)V

    .line 2762
    .end local v2    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;
    :cond_2
    :goto_1
    return-object v3

    .line 2756
    .restart local v2    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getMusicStreamingURL: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v5, v5, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2757
    iget-object v4, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    goto :goto_1

    .line 2742
    .end local v2    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public getStreamingInfoForVideo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/StreamingInfo;
    .locals 6
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "screenResolution"    # Ljava/lang/String;
    .param p3, "connectionType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 880
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x1

    if-ge v4, v5, :cond_1

    .line 881
    :cond_0
    new-instance v4, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v4}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v4

    .line 883
    :cond_1
    new-instance v3, Lcom/samsung/scloud/data/StreamingInfo;

    invoke-direct {v3}, Lcom/samsung/scloud/data/StreamingInfo;-><init>()V

    .line 884
    .local v3, "ret":Lcom/samsung/scloud/data/StreamingInfo;
    const/4 v0, 0x0

    .line 885
    .local v0, "URL":Ljava/lang/String;
    const/4 v1, 0x0

    .line 887
    .local v1, "authToken":Ljava/lang/String;
    const-string v4, "getStreamingInfoForVideo"

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 890
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getAuthToken()Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 896
    :goto_0
    if-eqz v1, :cond_2

    .line 897
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v4, v1}, Lcom/baidu/pcs/BaiduPCSClient;->setAccessToken(Ljava/lang/String;)V

    .line 898
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    const-string v5, "M3U8_640_360"

    invoke-virtual {v4, p1, v5}, Lcom/baidu/pcs/BaiduPCSClient;->getStreamingURL(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;

    move-result-object v2

    .line 899
    .local v2, "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;
    const-string v4, "mediaTranscode: first"

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 901
    if-eqz v2, :cond_2

    .line 902
    iget-object v4, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v4, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v4, :cond_3

    .line 903
    iget-object v0, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;->url:Ljava/lang/String;

    .line 905
    invoke-virtual {v3, v0}, Lcom/samsung/scloud/data/StreamingInfo;->setStreamUrl(Ljava/lang/String;)V

    .line 914
    .end local v2    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;
    :cond_2
    :goto_1
    return-object v3

    .line 908
    .restart local v2    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getStreamingInfo: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v5, v5, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 909
    iget-object v4, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    goto :goto_1

    .line 892
    .end local v2    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public getThumbnail(Ljava/io/File;Ljava/lang/String;IILcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 9
    .param p1, "localFile"    # Ljava/io/File;
    .param p2, "serverFilePath"    # Ljava/lang/String;
    .param p3, "thumbnail_type"    # I
    .param p4, "thumbnail_format"    # I
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 497
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x1

    if-ge v7, v8, :cond_1

    :cond_0
    new-instance v7, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v7}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v7

    .line 499
    :cond_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getThumbnail:  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " type: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " format: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 501
    const/4 v2, 0x0

    .line 502
    .local v2, "format":Landroid/graphics/Bitmap$CompressFormat;
    const/4 v0, 0x0

    .line 503
    .local v0, "authToken":Ljava/lang/String;
    const/16 v6, 0x40

    .line 504
    .local v6, "width":I
    const/16 v4, 0x40

    .line 507
    .local v4, "height":I
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getAuthToken()Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_8

    move-result-object v0

    .line 512
    :goto_0
    const/4 v7, 0x1

    if-ne p4, v7, :cond_3

    .line 513
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    .line 519
    :goto_1
    if-nez p3, :cond_5

    .line 520
    const/16 v6, 0x800

    .line 521
    const/16 v4, 0x600

    .line 550
    :goto_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getThumbnail: width = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " height = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 552
    const/4 v3, 0x0

    .line 554
    .local v3, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v3, Ljava/io/FileOutputStream;

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    const/4 v7, 0x0

    invoke-direct {v3, p1, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 559
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    if-eqz v0, :cond_16

    .line 560
    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v7, v0}, Lcom/baidu/pcs/BaiduPCSClient;->setAccessToken(Ljava/lang/String;)V

    .line 561
    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    const/16 v8, 0x64

    invoke-virtual {v7, p2, v8, v6, v4}, Lcom/baidu/pcs/BaiduPCSClient;->thumbnail(Ljava/lang/String;III)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;

    move-result-object v5

    .line 563
    .local v5, "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getThumbnail: info.status.serverFilePath = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 564
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getThumbnail: info.status.errorCode = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v5, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v8, v8, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 566
    if-eqz v5, :cond_12

    iget-object v7, v5, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v7, v7, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v7, :cond_12

    .line 567
    const-string v7, "Thumbnail success"

    invoke-direct {p0, v7}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 569
    iget-object v7, v5, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v7, :cond_f

    .line 571
    :try_start_2
    iget-object v7, v5, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->bitmap:Landroid/graphics/Bitmap;

    const/16 v8, 0x64

    invoke-virtual {v7, v2, v8, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 572
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 573
    const/4 v7, 0x0

    .line 577
    if-eqz v3, :cond_2

    .line 579
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 630
    .end local v5    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;
    :cond_2
    :goto_3
    return-object v7

    .line 514
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    :cond_3
    if-nez p4, :cond_4

    .line 515
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    goto/16 :goto_1

    .line 517
    :cond_4
    new-instance v7, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    const-string v8, "wrong thumbnail type"

    invoke-direct {v7, v8}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 522
    :cond_5
    const/4 v7, 0x1

    if-ne p3, v7, :cond_6

    .line 523
    const/16 v6, 0x400

    .line 524
    const/16 v4, 0x300

    goto/16 :goto_2

    .line 525
    :cond_6
    const/4 v7, 0x5

    if-ne p3, v7, :cond_7

    .line 526
    const/16 v6, 0x140

    .line 527
    const/16 v4, 0xf0

    goto/16 :goto_2

    .line 528
    :cond_7
    const/4 v7, 0x4

    if-ne p3, v7, :cond_8

    .line 529
    const/16 v6, 0x1e0

    .line 530
    const/16 v4, 0x140

    goto/16 :goto_2

    .line 531
    :cond_8
    const/4 v7, 0x3

    if-ne p3, v7, :cond_9

    .line 532
    const/16 v6, 0x280

    .line 533
    const/16 v4, 0x1e0

    goto/16 :goto_2

    .line 534
    :cond_9
    const/4 v7, 0x2

    if-ne p3, v7, :cond_a

    .line 535
    const/16 v6, 0x3c0

    .line 536
    const/16 v4, 0x280

    goto/16 :goto_2

    .line 537
    :cond_a
    const/4 v7, 0x6

    if-ne p3, v7, :cond_b

    .line 538
    const/16 v6, 0x100

    .line 539
    const/16 v4, 0x100

    goto/16 :goto_2

    .line 540
    :cond_b
    const/4 v7, 0x7

    if-ne p3, v7, :cond_c

    .line 541
    const/16 v6, 0x80

    .line 542
    const/16 v4, 0x80

    goto/16 :goto_2

    .line 543
    :cond_c
    const/16 v7, 0x8

    if-ne p3, v7, :cond_d

    .line 544
    const/16 v6, 0x40

    .line 545
    const/16 v4, 0x40

    goto/16 :goto_2

    .line 547
    :cond_d
    new-instance v7, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    const-string v8, "wrong thumbnail size"

    invoke-direct {v7, v8}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 555
    :catch_0
    move-exception v1

    .line 556
    .local v1, "e":Ljava/lang/Exception;
    new-instance v7, Lcom/samsung/scloud/exception/SCloudIOException;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 580
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;
    :catch_1
    move-exception v1

    .line 581
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 574
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 575
    .local v1, "e":Ljava/lang/Exception;
    :try_start_4
    new-instance v7, Lcom/samsung/scloud/exception/SCloudIOException;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 577
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    if-eqz v3, :cond_e

    .line 579
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 582
    :cond_e
    :goto_4
    throw v7

    .line 580
    :catch_3
    move-exception v1

    .line 581
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 586
    .end local v1    # "e":Ljava/io/IOException;
    :cond_f
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Thumbnail   failed: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v5, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v8, v8, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "   "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v5, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v8, v8, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 588
    if-eqz v3, :cond_10

    .line 590
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 595
    :cond_10
    :goto_5
    iget-object v7, v5, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v7}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    .line 630
    .end local v5    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;
    :cond_11
    :goto_6
    const/4 v7, 0x0

    goto/16 :goto_3

    .line 591
    .restart local v5    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;
    :catch_4
    move-exception v1

    .line 592
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 598
    .end local v1    # "e":Ljava/io/IOException;
    :cond_12
    if-nez v5, :cond_14

    .line 599
    const-string v7, "Thumbnail   failed: info is null"

    invoke-direct {p0, v7}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 600
    if-eqz v3, :cond_13

    .line 602
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 607
    :cond_13
    :goto_7
    new-instance v7, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v8, "no thumbnail"

    invoke-direct {v7, v8}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 603
    :catch_5
    move-exception v1

    .line 604
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 609
    .end local v1    # "e":Ljava/io/IOException;
    :cond_14
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Thumbnail   failed: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v5, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v8, v8, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "   "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v5, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v8, v8, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 611
    if-eqz v3, :cond_15

    .line 613
    :try_start_8
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    .line 618
    :cond_15
    :goto_8
    iget-object v7, v5, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v7}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    goto :goto_6

    .line 614
    :catch_6
    move-exception v1

    .line 615
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 622
    .end local v1    # "e":Ljava/io/IOException;
    .end local v5    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;
    :cond_16
    if-eqz v3, :cond_11

    .line 624
    :try_start_9
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    goto :goto_6

    .line 625
    :catch_7
    move-exception v1

    .line 626
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 508
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_8
    move-exception v7

    goto/16 :goto_0
.end method

.method public getThumbnailWithMeta(Ljava/io/File;Ljava/lang/String;IILcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 15
    .param p1, "localFile"    # Ljava/io/File;
    .param p2, "serverFilePath"    # Ljava/lang/String;
    .param p3, "thumbnail_type"    # I
    .param p4, "thumbnail_format"    # I
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 657
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v13

    const/4 v14, 0x1

    if-ge v13, v14, :cond_1

    :cond_0
    new-instance v13, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v13}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v13

    .line 659
    :cond_1
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "getThumbnailWithMeta:  "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " type: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " format: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p4

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {p0, v13}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 661
    const/4 v5, 0x0

    .line 662
    .local v5, "format":Landroid/graphics/Bitmap$CompressFormat;
    const/4 v1, 0x0

    .line 663
    .local v1, "authToken":Ljava/lang/String;
    const/16 v12, 0x40

    .line 664
    .local v12, "width":I
    const/16 v7, 0x40

    .line 667
    .local v7, "height":I
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getAuthToken()Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_8

    move-result-object v1

    .line 673
    :goto_0
    const/4 v13, 0x1

    move/from16 v0, p4

    if-ne v0, v13, :cond_5

    .line 674
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    .line 680
    :goto_1
    if-nez p3, :cond_7

    .line 681
    const/16 v12, 0x800

    .line 682
    const/16 v7, 0x600

    .line 711
    :goto_2
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "getThumbnailWithMeta: width = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " height = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {p0, v13}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 713
    const/4 v6, 0x0

    .line 715
    .local v6, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v6, Ljava/io/FileOutputStream;

    .end local v6    # "fos":Ljava/io/FileOutputStream;
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-direct {v6, v0, v13}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 720
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    const/4 v3, 0x0

    .line 721
    .local v3, "fileData":Lcom/samsung/scloud/data/SCloudFile;
    const/4 v10, 0x0

    .line 723
    .local v10, "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    if-eqz v1, :cond_1a

    .line 724
    iget-object v13, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v13, v1}, Lcom/baidu/pcs/BaiduPCSClient;->setAccessToken(Ljava/lang/String;)V

    .line 725
    iget-object v13, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    const/16 v14, 0x64

    move-object/from16 v0, p2

    invoke-virtual {v13, v0, v14, v12, v7}, Lcom/baidu/pcs/BaiduPCSClient;->thumbnail(Ljava/lang/String;III)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;

    move-result-object v8

    .line 727
    .local v8, "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;
    if-eqz v8, :cond_16

    iget-object v13, v8, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v13, v13, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v13, :cond_16

    .line 728
    const-string v13, "getThumbnailWithMeta success"

    invoke-direct {p0, v13}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 730
    iget-object v13, v8, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v13, :cond_13

    .line 732
    :try_start_2
    iget-object v13, v8, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->bitmap:Landroid/graphics/Bitmap;

    const/16 v14, 0x64

    invoke-virtual {v13, v5, v14, v6}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 733
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    .line 735
    new-instance v11, Lcom/samsung/scloud/data/PhotoMetaData;

    invoke-direct {v11}, Lcom/samsung/scloud/data/PhotoMetaData;-><init>()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 737
    .end local v10    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .local v11, "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    :try_start_3
    iget v13, v8, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->width:I

    invoke-virtual {v11, v13}, Lcom/samsung/scloud/data/PhotoMetaData;->setWidth(I)V

    .line 738
    iget v13, v8, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->height:I

    invoke-virtual {v11, v13}, Lcom/samsung/scloud/data/PhotoMetaData;->setHeight(I)V

    .line 739
    iget-object v13, v8, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->orientation:Ljava/lang/String;

    if-eqz v13, :cond_3

    .line 740
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Thumbnail success"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v8, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->orientation:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {p0, v13}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 741
    const/4 v9, 0x0

    .line 742
    .local v9, "orientation":I
    iget-object v13, v8, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->orientation:Ljava/lang/String;

    const-string v14, "RightTopOrientation"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_10

    .line 743
    const/16 v9, 0x5a

    .line 749
    :cond_2
    :goto_3
    invoke-virtual {v11, v9}, Lcom/samsung/scloud/data/PhotoMetaData;->setOrientation(I)V

    .line 752
    .end local v9    # "orientation":I
    :cond_3
    new-instance v4, Lcom/samsung/scloud/data/SCloudFile;

    invoke-direct {v4}, Lcom/samsung/scloud/data/SCloudFile;-><init>()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 753
    .end local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .local v4, "fileData":Lcom/samsung/scloud/data/SCloudFile;
    :try_start_4
    invoke-virtual {v4, v11}, Lcom/samsung/scloud/data/SCloudFile;->setPhotoMetaData(Lcom/samsung/scloud/data/PhotoMetaData;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_a
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 759
    if-eqz v6, :cond_4

    .line 761
    :try_start_5
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :cond_4
    :goto_4
    move-object v10, v11

    .end local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .restart local v10    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    move-object v3, v4

    .line 812
    .end local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .end local v8    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;
    :goto_5
    return-object v4

    .line 675
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .end local v10    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    :cond_5
    if-nez p4, :cond_6

    .line 676
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    goto/16 :goto_1

    .line 678
    :cond_6
    new-instance v13, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    const-string v14, "wrong thumbnail type"

    invoke-direct {v13, v14}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 683
    :cond_7
    const/4 v13, 0x1

    move/from16 v0, p3

    if-ne v0, v13, :cond_8

    .line 684
    const/16 v12, 0x400

    .line 685
    const/16 v7, 0x300

    goto/16 :goto_2

    .line 686
    :cond_8
    const/4 v13, 0x5

    move/from16 v0, p3

    if-ne v0, v13, :cond_9

    .line 687
    const/16 v12, 0x140

    .line 688
    const/16 v7, 0xf0

    goto/16 :goto_2

    .line 689
    :cond_9
    const/4 v13, 0x4

    move/from16 v0, p3

    if-ne v0, v13, :cond_a

    .line 690
    const/16 v12, 0x1e0

    .line 691
    const/16 v7, 0x140

    goto/16 :goto_2

    .line 692
    :cond_a
    const/4 v13, 0x3

    move/from16 v0, p3

    if-ne v0, v13, :cond_b

    .line 693
    const/16 v12, 0x280

    .line 694
    const/16 v7, 0x1e0

    goto/16 :goto_2

    .line 695
    :cond_b
    const/4 v13, 0x2

    move/from16 v0, p3

    if-ne v0, v13, :cond_c

    .line 696
    const/16 v12, 0x3c0

    .line 697
    const/16 v7, 0x280

    goto/16 :goto_2

    .line 698
    :cond_c
    const/4 v13, 0x6

    move/from16 v0, p3

    if-ne v0, v13, :cond_d

    .line 699
    const/16 v12, 0x100

    .line 700
    const/16 v7, 0x100

    goto/16 :goto_2

    .line 701
    :cond_d
    const/4 v13, 0x7

    move/from16 v0, p3

    if-ne v0, v13, :cond_e

    .line 702
    const/16 v12, 0x80

    .line 703
    const/16 v7, 0x80

    goto/16 :goto_2

    .line 704
    :cond_e
    const/16 v13, 0x8

    move/from16 v0, p3

    if-ne v0, v13, :cond_f

    .line 705
    const/16 v12, 0x40

    .line 706
    const/16 v7, 0x40

    goto/16 :goto_2

    .line 708
    :cond_f
    new-instance v13, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    const-string v14, "wrong thumbnail size"

    invoke-direct {v13, v14}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 716
    :catch_0
    move-exception v2

    .line 717
    .local v2, "e":Ljava/lang/Exception;
    new-instance v13, Lcom/samsung/scloud/exception/SCloudIOException;

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 744
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;
    .restart local v9    # "orientation":I
    .restart local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    :cond_10
    :try_start_6
    iget-object v13, v8, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->orientation:Ljava/lang/String;

    const-string v14, "BottomRightOrientation"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_11

    .line 745
    const/16 v9, 0xb4

    goto/16 :goto_3

    .line 746
    :cond_11
    iget-object v13, v8, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->orientation:Ljava/lang/String;

    const-string v14, "LeftBottomOrientation"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_9
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result v13

    if-eqz v13, :cond_2

    .line 747
    const/16 v9, 0x10e

    goto/16 :goto_3

    .line 762
    .end local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .end local v9    # "orientation":I
    .restart local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    :catch_1
    move-exception v2

    .line 763
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_4

    .line 756
    .end local v2    # "e":Ljava/io/IOException;
    .end local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .end local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .restart local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v10    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    :catch_2
    move-exception v2

    .line 757
    .local v2, "e":Ljava/lang/Exception;
    :goto_6
    :try_start_7
    new-instance v13, Lcom/samsung/scloud/exception/SCloudIOException;

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v13
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 759
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v13

    :goto_7
    if-eqz v6, :cond_12

    .line 761
    :try_start_8
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    .line 764
    :cond_12
    :goto_8
    throw v13

    .line 762
    :catch_3
    move-exception v2

    .line 763
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 769
    .end local v2    # "e":Ljava/io/IOException;
    :cond_13
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "getThumbnailWithMeta   failed: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v8, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v14, v14, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "   "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v8, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v14, v14, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {p0, v13}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 770
    if-eqz v6, :cond_14

    .line 772
    :try_start_9
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    .line 776
    :cond_14
    :goto_9
    iget-object v13, v8, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v13}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    .end local v8    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;
    :cond_15
    :goto_a
    move-object v4, v3

    .line 812
    .end local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    goto/16 :goto_5

    .line 773
    .end local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v8    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;
    :catch_4
    move-exception v2

    .line 774
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 779
    .end local v2    # "e":Ljava/io/IOException;
    :cond_16
    if-nez v8, :cond_18

    .line 780
    const-string v13, "getThumbnailWithMeta   failed: info is null"

    invoke-direct {p0, v13}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 781
    if-eqz v6, :cond_17

    .line 783
    :try_start_a
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    .line 788
    :cond_17
    :goto_b
    new-instance v13, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v14, "no thumbnail"

    invoke-direct {v13, v14}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 784
    :catch_5
    move-exception v2

    .line 785
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 791
    .end local v2    # "e":Ljava/io/IOException;
    :cond_18
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "getThumbnailWithMeta   failed: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v8, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v14, v14, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "   "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v8, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v14, v14, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {p0, v13}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 793
    if-eqz v6, :cond_19

    .line 795
    :try_start_b
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6

    .line 800
    :cond_19
    :goto_c
    iget-object v13, v8, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v13}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    goto :goto_a

    .line 796
    :catch_6
    move-exception v2

    .line 797
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c

    .line 804
    .end local v2    # "e":Ljava/io/IOException;
    .end local v8    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;
    :cond_1a
    if-eqz v6, :cond_15

    .line 806
    :try_start_c
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_7

    goto :goto_a

    .line 807
    :catch_7
    move-exception v2

    .line 808
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 669
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .end local v10    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    :catch_8
    move-exception v13

    goto/16 :goto_0

    .line 759
    .restart local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;
    .restart local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    :catchall_1
    move-exception v13

    move-object v10, v11

    .end local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .restart local v10    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    goto/16 :goto_7

    .end local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .end local v10    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .restart local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    :catchall_2
    move-exception v13

    move-object v10, v11

    .end local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .restart local v10    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    move-object v3, v4

    .end local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    goto/16 :goto_7

    .line 756
    .end local v10    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .restart local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    :catch_9
    move-exception v2

    move-object v10, v11

    .end local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .restart local v10    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    goto/16 :goto_6

    .end local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .end local v10    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .restart local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    :catch_a
    move-exception v2

    move-object v10, v11

    .end local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .restart local v10    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    move-object v3, v4

    .end local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    goto/16 :goto_6
.end method

.method public getUpdate(JJ)Ljava/util/ArrayList;
    .locals 1
    .param p1, "beginTimeStamp"    # J
    .param p3, "endTimeStamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Update;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2857
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getUserInfo()Lcom/samsung/scloud/data/User;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 110
    const/4 v0, 0x0

    .line 111
    .local v0, "authToken":Ljava/lang/String;
    const/4 v2, 0x0

    .line 113
    .local v2, "ret":Lcom/samsung/scloud/data/User;
    const-string v3, "getUserInfo"

    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 116
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getAuthToken()Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 122
    :goto_0
    if-eqz v0, :cond_1

    .line 123
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v3, v0}, Lcom/baidu/pcs/BaiduPCSClient;->setAccessToken(Ljava/lang/String;)V

    .line 124
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v3}, Lcom/baidu/pcs/BaiduPCSClient;->quota()Lcom/baidu/pcs/BaiduPCSActionInfo$PCSQuotaResponse;

    move-result-object v1

    .line 126
    .local v1, "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSQuotaResponse;
    if-nez v1, :cond_0

    const/4 v3, 0x0

    .line 148
    .end local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSQuotaResponse;
    :goto_1
    return-object v3

    .line 128
    .restart local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSQuotaResponse;
    :cond_0
    iget-object v3, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSQuotaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v3, v3, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v3, :cond_2

    .line 129
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Quota success: total:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSQuotaResponse;->total:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  used: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSQuotaResponse;->used:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 131
    new-instance v2, Lcom/samsung/scloud/data/User;

    .end local v2    # "ret":Lcom/samsung/scloud/data/User;
    invoke-direct {v2}, Lcom/samsung/scloud/data/User;-><init>()V

    .line 132
    .restart local v2    # "ret":Lcom/samsung/scloud/data/User;
    if-eqz v2, :cond_1

    .line 137
    iget-wide v4, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSQuotaResponse;->total:J

    invoke-virtual {v2, v4, v5}, Lcom/samsung/scloud/data/User;->setQuotaBytesTotal(J)V

    .line 138
    iget-wide v4, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSQuotaResponse;->used:J

    invoke-virtual {v2, v4, v5}, Lcom/samsung/scloud/data/User;->setQuotaBytesUsed(J)V

    .end local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSQuotaResponse;
    :cond_1
    :goto_2
    move-object v3, v2

    .line 148
    goto :goto_1

    .line 142
    .restart local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSQuotaResponse;
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Quota failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSQuotaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v4, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 143
    iget-object v3, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSQuotaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    goto :goto_2

    .line 118
    .end local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSQuotaResponse;
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public getVersions(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Version;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2552
    const-string v0, "getVersions"

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2553
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWholeFileList(ILjava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .locals 1
    .param p1, "maxResults"    # I
    .param p2, "nextToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 3073
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getWholeFolderList(ILjava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .locals 9
    .param p1, "maxResults"    # I
    .param p2, "nextToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudFolder;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 824
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getWholeFolderList:  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 826
    const/4 v4, 0x0

    .line 828
    .local v4, "ret":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;
    new-instance v2, Lcom/samsung/scloud/data/SCloudNodeList;

    invoke-direct {v2}, Lcom/samsung/scloud/data/SCloudNodeList;-><init>()V

    .line 829
    .local v2, "nodeList":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudFolder;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 832
    .local v3, "nodes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudFolder;>;"
    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    const-string v8, "all"

    invoke-virtual {v7, v6, v8}, Lcom/baidu/pcs/BaiduPCSClient;->diffStream(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;

    move-result-object v4

    .line 834
    if-eqz v4, :cond_4

    .line 835
    iget-object v7, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v7, v7, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v7, :cond_3

    .line 836
    iget-object v6, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;->cursor:Ljava/lang/String;

    invoke-virtual {v2, v6}, Lcom/samsung/scloud/data/SCloudNodeList;->setCursor(Ljava/lang/String;)V

    .line 837
    iget-boolean v6, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;->hasMore:Z

    invoke-virtual {v2, v6}, Lcom/samsung/scloud/data/SCloudNodeList;->setHasMore(Z)V

    .line 838
    iget-boolean v6, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;->isReseted:Z

    invoke-virtual {v2, v6}, Lcom/samsung/scloud/data/SCloudNodeList;->setReset(Z)V

    .line 839
    const/4 v5, 0x0

    .line 840
    .local v5, "updatedNode":Lcom/samsung/scloud/data/SCloudFolder;
    iget-object v6, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;->entries:Ljava/util/List;

    if-eqz v6, :cond_2

    .line 841
    iget-object v6, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;->entries:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDifferEntryInfo;

    .line 842
    .local v0, "entry":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDifferEntryInfo;
    if-eqz v0, :cond_0

    .line 843
    iget-boolean v6, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDifferEntryInfo;->isDeleted:Z

    if-eqz v6, :cond_1

    .line 849
    new-instance v5, Lcom/samsung/scloud/data/SCloudFolder;

    .end local v5    # "updatedNode":Lcom/samsung/scloud/data/SCloudFolder;
    invoke-direct {v5}, Lcom/samsung/scloud/data/SCloudFolder;-><init>()V

    .line 850
    .restart local v5    # "updatedNode":Lcom/samsung/scloud/data/SCloudFolder;
    iget-object v6, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDifferEntryInfo;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    iget-object v6, v6, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;->path:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/samsung/scloud/data/SCloudFolder;->setId(Ljava/lang/String;)V

    .line 851
    iget-object v6, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDifferEntryInfo;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    iget-object v6, v6, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;->path:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/samsung/scloud/data/SCloudFolder;->setAbsolutePath(Ljava/lang/String;)V

    .line 852
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/samsung/scloud/data/SCloudFolder;->setDeleted(Z)V

    .line 857
    :cond_0
    :goto_1
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 853
    :cond_1
    iget-object v6, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDifferEntryInfo;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    if-eqz v6, :cond_0

    .line 854
    iget-object v6, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDifferEntryInfo;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->parseEntry(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v5

    .end local v5    # "updatedNode":Lcom/samsung/scloud/data/SCloudFolder;
    check-cast v5, Lcom/samsung/scloud/data/SCloudFolder;

    .restart local v5    # "updatedNode":Lcom/samsung/scloud/data/SCloudFolder;
    goto :goto_1

    .line 860
    .end local v0    # "entry":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDifferEntryInfo;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    invoke-virtual {v2, v3}, Lcom/samsung/scloud/data/SCloudNodeList;->setNodes(Ljava/util/List;)V

    .line 868
    .end local v2    # "nodeList":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudFolder;>;"
    .end local v5    # "updatedNode":Lcom/samsung/scloud/data/SCloudFolder;
    :goto_2
    return-object v2

    .line 864
    .restart local v2    # "nodeList":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudFolder;>;"
    :cond_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getWholeFolderList failed: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v8, v8, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 865
    iget-object v7, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v7}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    :cond_4
    move-object v2, v6

    .line 868
    goto :goto_2
.end method

.method public inviteCollaborators(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Collaborator;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 3041
    .local p2, "collaborators":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/Collaborator;>;"
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public isAuthSucceed()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2807
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->isAuthSuccess:Z

    return v0
.end method

.method public isSignin()Z
    .locals 1

    .prologue
    .line 2025
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    if-nez v0, :cond_0

    .line 2026
    const-string v0, "isSignin is null"

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2028
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->accessToken:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2029
    const-string v0, "accessToken is null"

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2031
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->accessToken:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2032
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->accessToken:Ljava/lang/String;

    .line 2033
    const-string v0, "pcsClient isLogin"

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2035
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v0}, Lcom/baidu/pcs/BaiduPCSClient;->isLogin()Z

    move-result v0

    .line 2039
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public login(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/User;
    .locals 1
    .param p1, "ID"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 3049
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public logout()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2815
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->accessToken:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2816
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->accessToken:Ljava/lang/String;

    .line 2819
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v1}, Lcom/baidu/pcs/BaiduPCSClient;->logout()Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    move-result-object v0

    .line 2821
    .local v0, "ret":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    if-eqz v0, :cond_0

    .line 2822
    iget v1, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v1, :cond_1

    .line 2823
    const-string v1, "Baidu logout is success"

    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2848
    .end local v0    # "ret":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    :cond_0
    :goto_0
    return-void

    .line 2825
    .restart local v0    # "ret":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Baidu logout is failed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public makeCurrentVersion(Ljava/lang/String;J)Ljava/util/ArrayList;
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .param p2, "versionId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Version;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2561
    const-string v0, "makeCurrentVersion"

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2562
    const/4 v0, 0x0

    return-object v0
.end method

.method public makeFolder(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 5
    .param p1, "folderName"    # Ljava/lang/String;
    .param p2, "parentPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 157
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v2, v3, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v2, v3, :cond_1

    :cond_0
    new-instance v2, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v2}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v2

    .line 159
    :cond_1
    const/4 v0, 0x0

    .line 161
    .local v0, "authToken":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "makeFolder ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 164
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getAuthToken()Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 170
    :goto_0
    if-eqz v0, :cond_4

    .line 171
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v2, v0}, Lcom/baidu/pcs/BaiduPCSClient;->setAccessToken(Ljava/lang/String;)V

    .line 172
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/baidu/pcs/BaiduPCSClient;->makeDir(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;

    move-result-object v1

    .line 173
    .local v1, "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;
    if-eqz v1, :cond_4

    .line 174
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Mkdir:  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v2, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v2, :cond_2

    const-string v2, "success"

    :goto_1
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v3, v3, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 175
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v2, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v2, :cond_3

    .line 176
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->parseEntry(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v2

    check-cast v2, Lcom/samsung/scloud/data/SCloudFolder;

    .line 184
    .end local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;
    :goto_2
    return-object v2

    .line 174
    .restart local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;
    :cond_2
    const-string v2, "failed"

    goto :goto_1

    .line 179
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Mkdir failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v3, v3, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 180
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    .line 184
    .end local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;
    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    .line 166
    :catch_0
    move-exception v2

    goto/16 :goto_0
.end method

.method public mediaTranscode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/StreamingInfo;
    .locals 6
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "screenResolution"    # Ljava/lang/String;
    .param p3, "connectionType"    # Ljava/lang/String;
    .param p4, "container"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 929
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mediaTranscode path"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "screenResolution "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "connectionType "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "container "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 931
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x1

    if-ge v4, v5, :cond_1

    .line 932
    :cond_0
    new-instance v4, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v4}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v4

    .line 934
    :cond_1
    new-instance v3, Lcom/samsung/scloud/data/StreamingInfo;

    invoke-direct {v3}, Lcom/samsung/scloud/data/StreamingInfo;-><init>()V

    .line 935
    .local v3, "ret":Lcom/samsung/scloud/data/StreamingInfo;
    const/4 v0, 0x0

    .line 936
    .local v0, "URL":Ljava/lang/String;
    const/4 v1, 0x0

    .line 939
    .local v1, "authToken":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getAuthToken()Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 945
    :goto_0
    if-eqz v1, :cond_2

    .line 946
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v4, v1}, Lcom/baidu/pcs/BaiduPCSClient;->setAccessToken(Ljava/lang/String;)V

    .line 947
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    const-string v5, "M3U8_640_360"

    invoke-virtual {v4, p1, v5}, Lcom/baidu/pcs/BaiduPCSClient;->getStreamingURL(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;

    move-result-object v2

    .line 948
    .local v2, "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;
    const-string v4, "mediaTranscode: second"

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 950
    if-eqz v2, :cond_2

    .line 951
    iget-object v4, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v4, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v4, :cond_3

    .line 952
    iget-object v0, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;->url:Ljava/lang/String;

    .line 954
    invoke-virtual {v3, v0}, Lcom/samsung/scloud/data/StreamingInfo;->setStreamUrl(Ljava/lang/String;)V

    .line 963
    .end local v2    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;
    :cond_2
    :goto_1
    return-object v3

    .line 957
    .restart local v2    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mediaTranscode: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v5, v5, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 958
    iget-object v4, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    goto :goto_1

    .line 941
    .end local v2    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public move(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/scloud/data/SCloudNode;
    .locals 4
    .param p1, "fromPath"    # Ljava/lang/String;
    .param p2, "toPath"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 2486
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v2, v3, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v2, v3, :cond_1

    .line 2487
    :cond_0
    new-instance v2, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v2}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v2

    .line 2489
    :cond_1
    const/4 v0, 0x0

    .line 2491
    .local v0, "authToken":Ljava/lang/String;
    const-string v2, "move"

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2494
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getAuthToken()Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2500
    :goto_0
    if-eqz v0, :cond_3

    .line 2501
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v2, v0}, Lcom/baidu/pcs/BaiduPCSClient;->setAccessToken(Ljava/lang/String;)V

    .line 2503
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v2, p1, p2}, Lcom/baidu/pcs/BaiduPCSClient;->move(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;

    move-result-object v1

    .line 2505
    .local v1, "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;
    if-eqz v1, :cond_2

    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v2, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v2, :cond_2

    .line 2506
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;->list:Ljava/util/List;

    if-eqz v2, :cond_3

    .line 2507
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;->list:Ljava/util/List;

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->parseList(Ljava/util/List;)Lcom/samsung/scloud/data/SCloudFile;

    move-result-object v2

    .line 2515
    .end local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;
    :goto_1
    return-object v2

    .line 2511
    .restart local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "move failed: from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v3, v3, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2512
    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    .line 2515
    .end local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 2496
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public musicDelta(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .locals 8
    .param p1, "cursor"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 981
    const/4 v4, 0x0

    .line 982
    .local v4, "ret":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffWithMetaResponse;
    new-instance v2, Lcom/samsung/scloud/data/SCloudNodeList;

    invoke-direct {v2}, Lcom/samsung/scloud/data/SCloudNodeList;-><init>()V

    .line 983
    .local v2, "nodeList":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudNode;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 984
    .local v3, "nodes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudNode;>;"
    const-string v6, "musicDelta start."

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 986
    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    const-string v7, "audio"

    invoke-virtual {v6, p1, v7}, Lcom/baidu/pcs/BaiduPCSClient;->diffStreamWithMeta(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffWithMetaResponse;

    move-result-object v4

    .line 988
    if-eqz v4, :cond_3

    .line 989
    iget-object v6, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffWithMetaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v6, v6, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v6, :cond_4

    .line 990
    iget-object v6, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffWithMetaResponse;->cursor:Ljava/lang/String;

    invoke-virtual {v2, v6}, Lcom/samsung/scloud/data/SCloudNodeList;->setCursor(Ljava/lang/String;)V

    .line 991
    iget-boolean v6, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffWithMetaResponse;->hasMore:Z

    invoke-virtual {v2, v6}, Lcom/samsung/scloud/data/SCloudNodeList;->setHasMore(Z)V

    .line 992
    iget-boolean v6, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffWithMetaResponse;->isReseted:Z

    invoke-virtual {v2, v6}, Lcom/samsung/scloud/data/SCloudNodeList;->setReset(Z)V

    .line 993
    const/4 v5, 0x0

    .line 994
    .local v5, "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    iget-object v6, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffWithMetaResponse;->entries:Ljava/util/List;

    if-eqz v6, :cond_2

    .line 995
    iget-object v6, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffWithMetaResponse;->entries:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;

    .line 996
    .local v0, "entry":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;
    if-eqz v0, :cond_0

    .line 997
    iget-boolean v6, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->isDeleted:Z

    if-eqz v6, :cond_1

    .line 1003
    new-instance v5, Lcom/samsung/scloud/data/SCloudFile;

    .end local v5    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    invoke-direct {v5}, Lcom/samsung/scloud/data/SCloudFile;-><init>()V

    .line 1004
    .restart local v5    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    iget-object v6, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    iget-object v6, v6, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;->path:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/samsung/scloud/data/SCloudNode;->setId(Ljava/lang/String;)V

    .line 1005
    iget-object v6, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    iget-object v6, v6, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;->path:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/samsung/scloud/data/SCloudNode;->setAbsolutePath(Ljava/lang/String;)V

    .line 1006
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/samsung/scloud/data/SCloudNode;->setDeleted(Z)V

    .line 1011
    :cond_0
    :goto_1
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1008
    :cond_1
    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->parseMusicEntry(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v5

    goto :goto_1

    .line 1014
    .end local v0    # "entry":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    invoke-virtual {v2, v3}, Lcom/samsung/scloud/data/SCloudNodeList;->setNodes(Ljava/util/List;)V

    .line 1022
    .end local v5    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    :cond_3
    :goto_2
    return-object v2

    .line 1017
    :cond_4
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getDeltaList failed: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffWithMetaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v7, v7, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1018
    iget-object v6, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffWithMetaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    goto :goto_2
.end method

.method public photoDelta(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .locals 1
    .param p1, "cursor"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 1127
    const-string v0, "SCloudNodeList"

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1128
    const/4 v0, 0x0

    return-object v0
.end method

.method public putFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 2
    .param p1, "sourceFile"    # Ljava/io/File;
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "destinationFolderIdOrPath"    # Ljava/lang/String;
    .param p4, "overwrite"    # Z
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 2126
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v1, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v1, :cond_1

    .line 2128
    :cond_0
    new-instance v0, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v0

    .line 2130
    :cond_1
    const-string v0, "putFile"

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2131
    if-eqz p4, :cond_2

    .line 2132
    new-instance v0, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    const-string v1, "Google Drive not support the overwriting the file with the filename, you should overwrite the file with file Id"

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2134
    :cond_2
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->putNewFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;

    move-result-object v0

    return-object v0
.end method

.method public putFile(Ljava/io/File;Ljava/lang/String;ZLjava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 9
    .param p1, "sourceFile"    # Ljava/io/File;
    .param p2, "destinationFolderIdOrPath"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .param p4, "parentRev"    # Ljava/lang/String;
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2234
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v6, 0x1

    if-ge v4, v6, :cond_1

    .line 2236
    :cond_0
    new-instance v4, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v4}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v4

    .line 2239
    :cond_1
    const-string v4, "putFile2"

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2241
    const/4 v0, 0x0

    .line 2245
    .local v0, "authToken":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    .line 2246
    .local v2, "sourceFileName":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 2249
    .local v3, "sourceFilePath":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getAuthToken()Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2255
    :goto_0
    const/4 v1, 0x0

    .line 2257
    .local v1, "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;
    if-eqz v0, :cond_5

    .line 2258
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v4, v0}, Lcom/baidu/pcs/BaiduPCSClient;->setAccessToken(Ljava/lang/String;)V

    .line 2259
    if-nez p5, :cond_2

    .line 2260
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7, v5, v5}, Lcom/baidu/pcs/BaiduPCSClient;->uploadFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;

    move-result-object v1

    .line 2281
    :goto_1
    if-eqz v1, :cond_5

    .line 2282
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "putFile:  "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v4, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v4, :cond_3

    const-string v4, "success"

    :goto_2
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2283
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "putFile:  "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2285
    iget-object v4, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v4, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v4, :cond_4

    .line 2286
    iget-object v4, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;->commonFileInfo:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->parseEntry(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v4

    check-cast v4, Lcom/samsung/scloud/data/SCloudFile;

    .line 2345
    :goto_3
    return-object v4

    .line 2263
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI$2;

    invoke-direct {v8, p0, p5, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI$2;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;Lcom/samsung/scloud/response/ProgressListener;Ljava/lang/String;)V

    invoke-virtual {v4, v6, v7, v5, v8}, Lcom/baidu/pcs/BaiduPCSClient;->uploadFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;

    move-result-object v1

    goto/16 :goto_1

    .line 2282
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed  "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v7, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v7, v7, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_2

    .line 2289
    :cond_4
    iget-object v4, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    :cond_5
    move-object v4, v5

    .line 2345
    goto :goto_3

    .line 2251
    .end local v1    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;
    :catch_0
    move-exception v4

    goto/16 :goto_0
.end method

.method public putFile(Ljava/io/FileInputStream;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 1
    .param p1, "sourceFileInputStream"    # Ljava/io/FileInputStream;
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "destinationFolderIdOrPath"    # Ljava/lang/String;
    .param p4, "overwrite"    # Z
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2524
    const-string v0, "putFile3"

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2525
    const/4 v0, 0x0

    return-object v0
.end method

.method public removeCollaboration(J)V
    .locals 1
    .param p1, "collaborationId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 3025
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public removeComment(Ljava/lang/String;J)V
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .param p2, "commentID"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2993
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public removePushTrigger(Lcom/dropbox/client2/SamsungDropboxAPI$Hook;Ljava/lang/String;)Z
    .locals 1
    .param p1, "hook_type"    # Lcom/dropbox/client2/SamsungDropboxAPI$Hook;
    .param p2, "triggerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 1596
    const/4 v0, 0x0

    return v0
.end method

.method public removeShareURL(Ljava/lang/String;)Z
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2779
    const-string v0, "removeShareURL"

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2780
    const/4 v0, 0x0

    return v0
.end method

.method public rename(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/scloud/data/SCloudNode;
    .locals 7
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .param p2, "newName"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 323
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v3, v6, :cond_0

    const-string v3, "/"

    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    if-eq v3, v4, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v3, v6, :cond_1

    .line 325
    :cond_0
    new-instance v3, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v3}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v3

    .line 327
    :cond_1
    const-string v3, "rename"

    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 329
    const/4 v1, 0x0

    .line 331
    .local v1, "builder":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .line 334
    .local v0, "authToken":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getAuthToken()Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 340
    :goto_0
    if-eqz v0, :cond_4

    .line 341
    const-string v3, "."

    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v4, :cond_2

    .line 342
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "/"

    invoke-virtual {p1, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {p1, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 348
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v3, v0}, Lcom/baidu/pcs/BaiduPCSClient;->setAccessToken(Ljava/lang/String;)V

    .line 349
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Lcom/baidu/pcs/BaiduPCSClient;->move(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;

    move-result-object v2

    .line 351
    .local v2, "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;
    if-eqz v2, :cond_4

    iget-object v3, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    if-eqz v3, :cond_4

    .line 352
    iget-object v3, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v3, v3, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v3, :cond_3

    .line 353
    iget-object v3, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;->list:Ljava/util/List;

    if-eqz v3, :cond_4

    .line 354
    iget-object v3, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;->list:Ljava/util/List;

    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->parseListToFile(Ljava/util/List;)Lcom/samsung/scloud/data/SCloudFile;

    move-result-object v3

    .line 363
    .end local v2    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;
    :goto_2
    return-object v3

    .line 344
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "/"

    invoke-virtual {p1, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {p1, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {p1, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto :goto_1

    .line 358
    .restart local v2    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rename failed: from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v4, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 359
    iget-object v3, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    .line 363
    .end local v2    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;
    :cond_4
    const/4 v3, 0x0

    goto :goto_2

    .line 336
    :catch_0
    move-exception v3

    goto/16 :goto_0
.end method

.method public rename(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/samsung/scloud/data/SCloudNode;
    .locals 7
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .param p2, "newName"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .param p4, "isDir"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 1831
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v3, v4, :cond_0

    const-string v3, "/"

    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    if-eq v3, v5, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v3, v4, :cond_1

    .line 1833
    :cond_0
    new-instance v3, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v3}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v3

    .line 1835
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rename IdOrPath="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1837
    const/4 v1, 0x0

    .line 1839
    .local v1, "builder":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .line 1842
    .local v0, "authToken":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->getAuthToken()Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1848
    :goto_0
    if-eqz v0, :cond_5

    .line 1849
    const-string v3, "."

    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v5, :cond_2

    .line 1850
    const-string v3, "rename StringBuilder. 1"

    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1851
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "/"

    invoke-virtual {p1, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {p1, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1858
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v3, v0}, Lcom/baidu/pcs/BaiduPCSClient;->setAccessToken(Ljava/lang/String;)V

    .line 1859
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Lcom/baidu/pcs/BaiduPCSClient;->move(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;

    move-result-object v2

    .line 1861
    .local v2, "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rename IdOrPath="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1862
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rename  builder.toString()="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1864
    if-eqz v2, :cond_5

    iget-object v3, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    if-eqz v3, :cond_5

    .line 1865
    iget-object v3, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v3, v3, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v3, :cond_4

    .line 1866
    iget-object v3, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;->list:Ljava/util/List;

    if-eqz v3, :cond_5

    .line 1867
    if-eqz p4, :cond_3

    .line 1868
    iget-object v3, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;->list:Ljava/util/List;

    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->parseListFolder(Ljava/util/List;)Lcom/samsung/scloud/data/SCloudFolder;

    move-result-object v3

    .line 1879
    .end local v2    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;
    :goto_2
    return-object v3

    .line 1853
    :cond_2
    const-string v3, "rename StringBuilder. 2"

    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1854
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "/"

    invoke-virtual {p1, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {p1, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {p1, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto :goto_1

    .line 1870
    .restart local v2    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;
    :cond_3
    iget-object v3, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;->list:Ljava/util/List;

    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->parseList(Ljava/util/List;)Lcom/samsung/scloud/data/SCloudFile;

    move-result-object v3

    goto :goto_2

    .line 1874
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rename failed: from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v4, v4, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1875
    iget-object v3, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->handleBaiduExceptions(Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;)V

    .line 1879
    .end local v2    # "info":Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;
    :cond_5
    const/4 v3, 0x0

    goto :goto_2

    .line 1844
    :catch_0
    move-exception v3

    goto/16 :goto_0
.end method

.method public restoreFromRecycleBin(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "recycledBinIdOrPath"    # Ljava/lang/String;
    .param p2, "IdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 3065
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public search(Ljava/lang/String;Ljava/lang/String;IZ)Lcom/samsung/scloud/data/SearchResult;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "fileLimit"    # I
    .param p4, "includeDeleted"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2789
    const-string v0, "search"

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2790
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public setActiveRepository(Ljava/lang/String;)V
    .locals 1
    .param p1, "repositoryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2953
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public setAuthToken(Ljava/lang/String;)V
    .locals 2
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 1488
    const-string v0, "setAuthToken:"

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1490
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1492
    const-string v0, "pcsClient.setAccessToken:"

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1493
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->accessToken:Ljava/lang/String;

    .line 1494
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->pcsClient:Lcom/baidu/pcs/BaiduPCSClient;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->accessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSClient;->setAccessToken(Ljava/lang/String;)V

    .line 1495
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "accessToken:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->accessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1499
    :goto_0
    return-void

    .line 1497
    :cond_0
    const-string v0, "Token is null !"

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setDescription(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 3001
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public setDeviceProfile(Lcom/samsung/scloud/data/Device;)V
    .locals 1
    .param p1, "deviceProfile"    # Lcom/samsung/scloud/data/Device;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2881
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public setDeviceProfile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2889
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public setToContinue(Z)V
    .locals 2
    .param p1, "isContinue"    # Z

    .prologue
    .line 1608
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setToContinue checkCancel : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1609
    sput-boolean p1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->isContinued:Z

    .line 1610
    return-void
.end method

.method public startAuth(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2798
    const-string v0, "startAuth !!!"

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 2800
    return-void
.end method

.method public unsetDescription(Ljava/lang/String;)V
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 3009
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public updateRepository(Lcom/samsung/scloud/data/Repository;)Lcom/samsung/scloud/data/Repository;
    .locals 1
    .param p1, "repo"    # Lcom/samsung/scloud/data/Repository;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2945
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public updateRepository(Ljava/lang/String;J)Lcom/samsung/scloud/data/Repository;
    .locals 1
    .param p1, "repositoryName"    # Ljava/lang/String;
    .param p2, "spaceAmount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2937
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public videoDelta(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .locals 1
    .param p1, "cursor"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 1266
    const-string v0, "SCloudNodeList2"

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V

    .line 1267
    const/4 v0, 0x0

    return-object v0
.end method
