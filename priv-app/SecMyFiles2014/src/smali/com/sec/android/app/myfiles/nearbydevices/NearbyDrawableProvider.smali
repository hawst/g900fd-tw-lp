.class public Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;
.super Ljava/lang/Object;
.source "NearbyDrawableProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$1;,
        Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;,
        Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIcon;
    }
.end annotation


# static fields
.field private static final MODULE:Ljava/lang/String; = "NearbyDrawableProvider"

.field private static final mRequests:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;"
        }
    .end annotation
.end field

.field private static final mTasks:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/AsyncTask",
            "<",
            "Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;",
            "Ljava/lang/Void;",
            "Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;->mRequests:Ljava/util/concurrent/ConcurrentMap;

    .line 55
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;->mTasks:Ljava/util/concurrent/ConcurrentMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    if-nez p1, :cond_0

    .line 65
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 67
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;->mContext:Landroid/content/Context;

    .line 68
    return-void
.end method

.method static synthetic access$100()Ljava/util/concurrent/ConcurrentMap;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;->mTasks:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method

.method public static getBitmap(Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 15
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 132
    const/16 v11, 0x80

    .line 133
    .local v11, "width":I
    const/16 v3, 0x80

    .line 134
    .local v3, "height":I
    const/4 v9, 0x1

    .line 135
    .local v9, "sampleSize":I
    const/4 v0, 0x0

    .line 136
    .local v0, "bmp":Landroid/graphics/Bitmap;
    const/4 v10, 0x0

    .line 138
    .local v10, "tmpBitmap":Landroid/graphics/Bitmap;
    const-string v12, "Nearby helper get picture"

    invoke-static {v12}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;)Landroid/net/http/AndroidHttpClient;

    move-result-object v4

    .line 143
    .local v4, "httpClient":Landroid/net/http/AndroidHttpClient;
    :try_start_0
    new-instance v8, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v8, v12}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 144
    .local v8, "rq":Lorg/apache/http/client/methods/HttpUriRequest;
    sget-object v12, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;->mRequests:Ljava/util/concurrent/ConcurrentMap;

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_0

    .line 146
    sget-object v12, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;->mRequests:Ljava/util/concurrent/ConcurrentMap;

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13, v8}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    :cond_0
    new-instance v12, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v12}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v7

    .line 150
    .local v7, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v12

    invoke-interface {v12}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v12

    const/16 v13, 0xc8

    if-eq v12, v13, :cond_2

    .line 152
    new-instance v12, Ljava/lang/IllegalStateException;

    invoke-direct {v12}, Ljava/lang/IllegalStateException;-><init>()V

    throw v12
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 194
    .end local v7    # "response":Lorg/apache/http/HttpResponse;
    .end local v8    # "rq":Lorg/apache/http/client/methods/HttpUriRequest;
    :catch_0
    move-exception v1

    .line 195
    .local v1, "e":Ljava/io/IOException;
    :try_start_1
    const-string v12, "MyFiles[NearbyDevices]"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "getIcon: IOException - "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 200
    if-eqz v4, :cond_1

    .line 201
    invoke-virtual {v4}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 205
    .end local v1    # "e":Ljava/io/IOException;
    :cond_1
    :goto_0
    return-object v0

    .line 155
    .restart local v7    # "response":Lorg/apache/http/HttpResponse;
    .restart local v8    # "rq":Lorg/apache/http/client/methods/HttpUriRequest;
    :cond_2
    :try_start_2
    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v2

    .line 156
    .local v2, "entity":Lorg/apache/http/HttpEntity;
    if-eqz v2, :cond_8

    .line 158
    const/4 v5, 0x0

    .line 161
    .local v5, "inputStream":Ljava/io/InputStream;
    :try_start_3
    new-instance v6, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v6}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 162
    .local v6, "option":Landroid/graphics/BitmapFactory$Options;
    const/4 v12, 0x1

    iput-boolean v12, v6, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 163
    const/4 v12, 0x0

    iput-boolean v12, v6, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 164
    iput v9, v6, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 165
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v5

    .line 166
    const/4 v12, 0x0

    invoke-static {v5, v12, v6}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 167
    if-eqz v0, :cond_4

    .line 169
    iget v12, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-ne v12, v11, :cond_3

    iget v12, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-eq v12, v3, :cond_4

    .line 171
    :cond_3
    const/4 v12, 0x1

    invoke-static {v0, v11, v3, v12}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 173
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 175
    move-object v0, v10

    .line 180
    :cond_4
    if-eqz v0, :cond_5

    if-nez v10, :cond_7

    .line 182
    :cond_5
    new-instance v12, Ljava/lang/IllegalStateException;

    invoke-direct {v12}, Ljava/lang/IllegalStateException;-><init>()V

    throw v12
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 187
    .end local v6    # "option":Landroid/graphics/BitmapFactory$Options;
    :catchall_0
    move-exception v12

    if-eqz v5, :cond_6

    .line 189
    :try_start_4
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    :cond_6
    throw v12
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 196
    .end local v2    # "entity":Lorg/apache/http/HttpEntity;
    .end local v5    # "inputStream":Ljava/io/InputStream;
    .end local v7    # "response":Lorg/apache/http/HttpResponse;
    .end local v8    # "rq":Lorg/apache/http/client/methods/HttpUriRequest;
    :catch_1
    move-exception v1

    .line 197
    .local v1, "e":Ljava/lang/Exception;
    :try_start_5
    const-string v12, "MyFiles[NearbyDevices]"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "getIcon: probably you are trying to perform network operation in the main thread - "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 200
    if-eqz v4, :cond_1

    .line 201
    invoke-virtual {v4}, Landroid/net/http/AndroidHttpClient;->close()V

    goto :goto_0

    .line 187
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v2    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v5    # "inputStream":Ljava/io/InputStream;
    .restart local v6    # "option":Landroid/graphics/BitmapFactory$Options;
    .restart local v7    # "response":Lorg/apache/http/HttpResponse;
    .restart local v8    # "rq":Lorg/apache/http/client/methods/HttpUriRequest;
    :cond_7
    if-eqz v5, :cond_8

    .line 189
    :try_start_6
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 200
    .end local v5    # "inputStream":Ljava/io/InputStream;
    .end local v6    # "option":Landroid/graphics/BitmapFactory$Options;
    :cond_8
    if-eqz v4, :cond_1

    .line 201
    invoke-virtual {v4}, Landroid/net/http/AndroidHttpClient;->close()V

    goto :goto_0

    .line 200
    .end local v2    # "entity":Lorg/apache/http/HttpEntity;
    .end local v7    # "response":Lorg/apache/http/HttpResponse;
    .end local v8    # "rq":Lorg/apache/http/client/methods/HttpUriRequest;
    :catchall_1
    move-exception v12

    if-eqz v4, :cond_9

    .line 201
    invoke-virtual {v4}, Landroid/net/http/AndroidHttpClient;->close()V

    :cond_9
    throw v12
.end method


# virtual methods
.method public abort()V
    .locals 8

    .prologue
    .line 72
    sget-object v7, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;->mTasks:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v7}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v6

    .line 73
    .local v6, "tkeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 77
    .local v5, "tit":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 79
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 80
    .local v2, "next":Ljava/lang/String;
    sget-object v7, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;->mTasks:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v7, v2}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/AsyncTask;

    .line 82
    .local v4, "task":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;Ljava/lang/Void;Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;>;"
    if-eqz v4, :cond_0

    .line 83
    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Landroid/os/AsyncTask;->cancel(Z)Z

    goto :goto_0

    .line 87
    .end local v2    # "next":Ljava/lang/String;
    .end local v4    # "task":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;Ljava/lang/Void;Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;>;"
    :cond_1
    sget-object v7, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;->mTasks:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v7}, Ljava/util/concurrent/ConcurrentMap;->clear()V

    .line 90
    sget-object v7, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;->mRequests:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v7}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 91
    .local v1, "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 95
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 97
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 98
    .restart local v2    # "next":Ljava/lang/String;
    sget-object v7, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;->mRequests:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v7, v2}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/http/client/methods/HttpUriRequest;

    .line 100
    .local v3, "request":Lorg/apache/http/client/methods/HttpUriRequest;
    if-eqz v3, :cond_2

    .line 101
    invoke-interface {v3}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    goto :goto_1

    .line 105
    .end local v2    # "next":Ljava/lang/String;
    .end local v3    # "request":Lorg/apache/http/client/methods/HttpUriRequest;
    :cond_3
    sget-object v7, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;->mRequests:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v7}, Ljava/util/concurrent/ConcurrentMap;->clear()V

    .line 106
    return-void
.end method

.method public setDrawable(Landroid/net/Uri;Lcom/sec/android/app/myfiles/nearbydevices/DrawableUpdatable;)V
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "updatable"    # Lcom/sec/android/app/myfiles/nearbydevices/DrawableUpdatable;

    .prologue
    .line 117
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 118
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 120
    :cond_1
    sget-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;->mTasks:Ljava/util/concurrent/ConcurrentMap;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIcon;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIcon;-><init>(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$1;)V

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;

    const/4 v4, 0x0

    new-instance v5, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6, p1, p2}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;-><init>(Landroid/content/Context;Landroid/net/Uri;Lcom/sec/android/app/myfiles/nearbydevices/DrawableUpdatable;)V

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIcon;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    return-void
.end method
