.class Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$1;
.super Ljava/lang/Object;
.source "DownloadedAppAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

.field final synthetic val$item:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;)V
    .locals 0

    .prologue
    .line 318
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    iput-object p2, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$1;->val$item:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 323
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mSelectedItem:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;)Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mSelectedItem:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;)Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$1;->val$item:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 325
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mSelectedItem:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;)Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mSelectedItem:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;)Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->setSelected(Z)V

    .line 330
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$1;->val$item:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    # setter for: Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mSelectedItem:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->access$002(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;)Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mSelectedItem:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;)Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->setSelected(Z)V

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->notifyToSelectedItemChangeListener()V

    .line 336
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->notifyDataSetChanged()V

    .line 338
    :cond_2
    return-void
.end method
