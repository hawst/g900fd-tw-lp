.class public Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;
.super Ljava/lang/Object;
.source "NearbyDevicesNotifier.java"


# static fields
.field private static volatile instance:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;


# instance fields
.field private mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->instance:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;
    .locals 2

    .prologue
    .line 21
    sget-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->instance:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;

    if-nez v0, :cond_1

    .line 22
    const-class v1, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;

    monitor-enter v1

    .line 23
    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->instance:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;

    if-nez v0, :cond_0

    .line 24
    new-instance v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->instance:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;

    .line 26
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 28
    :cond_1
    sget-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->instance:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;

    return-object v0

    .line 26
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public registerListenerToProviderAddNotification(Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;)Z
    .locals 1
    .param p1, "item"    # Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->registerToAddNotification(Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;)V

    .line 34
    const/4 v0, 0x1

    .line 36
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public registerListenerToProviderRemoveNotification(Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;)Z
    .locals 1
    .param p1, "item"    # Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->registerToRemoveNotification(Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;)V

    .line 42
    const/4 v0, 0x1

    .line 44
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public registerProvider(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)V
    .locals 0
    .param p1, "provider"    # Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    .line 65
    return-void
.end method

.method public unregisterListenerFromProviderAddNotification(Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;)Z
    .locals 1
    .param p1, "item"    # Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->unregisterFromAddNotification(Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;)V

    .line 50
    const/4 v0, 0x1

    .line 52
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public unregisterListenerFromProviderRemoveNotification(Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;)Z
    .locals 1
    .param p1, "item"    # Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->unregisterFromRemovedNotification(Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;)V

    .line 58
    const/4 v0, 0x1

    .line 60
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
