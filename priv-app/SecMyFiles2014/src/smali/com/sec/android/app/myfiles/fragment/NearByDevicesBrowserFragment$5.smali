.class Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$5;
.super Ljava/lang/Object;
.source "NearByDevicesBrowserFragment.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;)V
    .locals 0

    .prologue
    .line 786
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPathSelected(ILjava/lang/String;)V
    .locals 4
    .param p1, "targetFolderID"    # I
    .param p2, "targetFolderPath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 790
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->isSelectMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 791
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->finishSelectMode()V

    .line 793
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v1, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->cancelThumbnailsLoading(Z)V

    .line 794
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    # setter for: Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->bBack:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->access$002(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;Z)Z

    .line 796
    const-string v1, "/"

    invoke-virtual {p2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    .line 797
    .local v0, "len":I
    if-nez v0, :cond_1

    .line 798
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 800
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, p2, v2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    .line 802
    :goto_0
    const-string v1, "/"

    invoke-virtual {p2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 803
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 806
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->invalidateOptionsMenu()V

    .line 807
    return-void
.end method
