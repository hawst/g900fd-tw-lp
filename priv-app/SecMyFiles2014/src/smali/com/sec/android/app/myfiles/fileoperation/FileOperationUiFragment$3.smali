.class Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$3;
.super Ljava/lang/Object;
.source "FileOperationUiFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

.field final synthetic val$button:Landroid/widget/Button;

.field final synthetic val$dialog:Landroid/app/AlertDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;Landroid/widget/Button;Landroid/app/AlertDialog;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    iput-object p2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$3;->val$button:Landroid/widget/Button;

    iput-object p3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$3;->val$dialog:Landroid/app/AlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$3;->val$button:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$3;->val$dialog:Landroid/app/AlertDialog;

    # invokes: Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setUiToCancelling(Landroid/widget/Button;Landroid/app/Dialog;)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->access$100(Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;Landroid/widget/Button;Landroid/app/Dialog;)V

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->cancel()V

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 215
    :cond_0
    return-void
.end method
