.class public Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;
.super Ljava/lang/Object;
.source "AbsNavigation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "HistoricalPathRepository"
.end annotation


# instance fields
.field private mLimit:I

.field private mPaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "limit"    # I

    .prologue
    .line 413
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 415
    iput p1, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->mLimit:I

    .line 417
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->mPaths:Ljava/util/ArrayList;

    .line 418
    return-void
.end method


# virtual methods
.method public get()Ljava/lang/String;
    .locals 2

    .prologue
    .line 444
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 446
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->mPaths:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 450
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 455
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->mPaths:Ljava/util/ArrayList;

    return-object v0
.end method

.method public pop()Ljava/lang/String;
    .locals 2

    .prologue
    .line 434
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 436
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->mPaths:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 439
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public push(Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 423
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->mLimit:I

    if-ne v0, v1, :cond_0

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->mPaths:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 428
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 429
    return-void
.end method
