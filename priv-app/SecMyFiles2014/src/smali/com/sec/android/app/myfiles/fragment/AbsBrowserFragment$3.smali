.class Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$3;
.super Landroid/view/View$DragShadowBuilder;
.source "AbsBrowserFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startDragMode(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

.field final synthetic val$dragView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;Landroid/view/View;Landroid/view/View;)V
    .locals 0
    .param p2, "x0"    # Landroid/view/View;

    .prologue
    .line 1125
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iput-object p3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$3;->val$dragView:Landroid/view/View;

    invoke-direct {p0, p2}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onProvideShadowMetrics(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 2
    .param p1, "shadowSize"    # Landroid/graphics/Point;
    .param p2, "shadowTouchPoint"    # Landroid/graphics/Point;

    .prologue
    .line 1130
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$3;->val$dragView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$3;->val$dragView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 1132
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$3;->val$dragView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$3;->val$dragView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 1133
    return-void
.end method
