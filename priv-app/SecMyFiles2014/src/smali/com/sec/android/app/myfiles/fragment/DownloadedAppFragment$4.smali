.class Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$4;
.super Ljava/lang/Object;
.source "DownloadedAppFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

.field final synthetic val$dialogId:I

.field final synthetic val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;ILcom/sec/android/app/myfiles/utils/SharedDataStore;)V
    .locals 0

    .prologue
    .line 551
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iput p2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$4;->val$dialogId:I

    iput-object p3, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$4;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x5

    .line 556
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 558
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$4;->val$dialogId:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$4;->val$dialogId:I

    if-ne v0, v2, :cond_1

    .line 560
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$4;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->bDoNotCheckDropBox:Z
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setDoNotShowCheckDropBox(Z)V

    .line 561
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$4;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->commitShowDataWarning()Z

    .line 563
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_1

    .line 565
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$4;->val$dialogId:I

    if-ne v0, v2, :cond_2

    .line 566
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    const/16 v1, 0x1f

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    .line 572
    :cond_1
    :goto_0
    return-void

    .line 568
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    const/16 v1, 0x8

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto :goto_0
.end method
