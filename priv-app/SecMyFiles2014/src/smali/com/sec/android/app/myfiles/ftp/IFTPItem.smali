.class public interface abstract Lcom/sec/android/app/myfiles/ftp/IFTPItem;
.super Ljava/lang/Object;
.source "IFTPItem.java"


# virtual methods
.method public abstract getDate()Ljava/util/Calendar;
.end method

.method public abstract getDir()Ljava/lang/String;
.end method

.method public abstract getExtension()Ljava/lang/String;
.end method

.method public abstract getFullPath()Ljava/lang/String;
.end method

.method public abstract getID()J
.end method

.method public abstract getSize()J
.end method

.method public abstract getThumbnail()Landroid/graphics/drawable/Drawable;
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method public abstract getType()I
.end method

.method public abstract hasId()Z
.end method

.method public abstract isDirectory()Z
.end method

.method public abstract isFile()Z
.end method

.method public abstract isRoot()Z
.end method

.method public abstract isSymbolicLink()Z
.end method

.method public abstract setId(J)V
.end method
