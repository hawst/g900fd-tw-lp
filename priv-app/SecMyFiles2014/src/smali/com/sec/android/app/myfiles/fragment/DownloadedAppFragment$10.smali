.class Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;
.super Ljava/lang/Object;
.source "DownloadedAppFragment.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->onCreateSelectedItemChangeListener()Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)V
    .locals 0

    .prologue
    .line 1055
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public multipleSelectionChanged(II)V
    .locals 12
    .param p1, "totalItems"    # I
    .param p2, "selectedItems"    # I

    .prologue
    const v11, 0x7f0b0016

    const/4 v10, 0x5

    const v9, 0x7f0b0038

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1060
    const/4 v3, 0x0

    .line 1062
    .local v3, "selAllItem":Landroid/view/MenuItem;
    const/4 v4, 0x0

    .line 1064
    .local v4, "unselAllItem":Landroid/view/MenuItem;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # setter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectedItemCount:I
    invoke-static {v5, p2}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$302(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;I)I

    .line 1066
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 1068
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v1, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeAddShortcut:Landroid/view/MenuItem;

    .local v1, "menuItem":Landroid/view/MenuItem;
    if-eqz v1, :cond_0

    .line 1070
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1072
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v1, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeAddShortcutHome:Landroid/view/MenuItem;

    if-eqz v1, :cond_1

    .line 1074
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1076
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v1, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    if-eqz v1, :cond_2

    .line 1078
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1081
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v1, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeRename:Landroid/view/MenuItem;

    if-eqz v1, :cond_3

    .line 1083
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1086
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v1, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDetail:Landroid/view/MenuItem;

    if-eqz v1, :cond_4

    .line 1088
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1091
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v1, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeCopy:Landroid/view/MenuItem;

    if-eqz v1, :cond_5

    .line 1093
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1096
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v1, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeMove:Landroid/view/MenuItem;

    if-eqz v1, :cond_6

    .line 1098
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1100
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v1, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeShare:Landroid/view/MenuItem;

    if-eqz v1, :cond_7

    .line 1102
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1104
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v1, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmoveMoveToPrivate:Landroid/view/MenuItem;

    if-eqz v1, :cond_8

    .line 1106
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1108
    :cond_8
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v1, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmoveRemoveFromPrivate:Landroid/view/MenuItem;

    if-eqz v1, :cond_9

    .line 1110
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1113
    :cond_9
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v8, "easy_mode_switch"

    invoke-static {v5, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-nez v5, :cond_a

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v8, "easy_mode_myfiles"

    invoke-static {v5, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-eqz v5, :cond_d

    .line 1116
    :cond_a
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v1, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeZip:Landroid/view/MenuItem;

    if-eqz v1, :cond_b

    .line 1118
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1121
    :cond_b
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v1, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeExtract:Landroid/view/MenuItem;

    if-eqz v1, :cond_c

    .line 1123
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1126
    :cond_c
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v1, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeExtractHere:Landroid/view/MenuItem;

    if-eqz v1, :cond_d

    .line 1128
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1133
    :cond_d
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v1, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDownload:Landroid/view/MenuItem;

    if-eqz v1, :cond_e

    .line 1135
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1137
    :cond_e
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v1, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeFtpEdit:Landroid/view/MenuItem;

    if-eqz v1, :cond_f

    .line 1139
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1141
    :cond_f
    sget-object v5, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mOptionsMenu:Landroid/view/Menu;

    if-eqz v5, :cond_10

    .line 1142
    sget-object v5, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mOptionsMenu:Landroid/view/Menu;

    const v8, 0x7f0f0121

    invoke-interface {v5, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1144
    .local v0, "deleteMenuItem":Landroid/view/MenuItem;
    if-eqz v0, :cond_10

    .line 1146
    if-eqz p2, :cond_14

    move v5, v6

    :goto_0
    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1148
    if-eqz p2, :cond_15

    move v5, v6

    :goto_1
    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1151
    .end local v0    # "deleteMenuItem":Landroid/view/MenuItem;
    :cond_10
    if-nez p2, :cond_18

    .line 1152
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->getSelectModeFrom()I

    move-result v5

    if-nez v5, :cond_16

    .line 1154
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v1, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    if-eqz v1, :cond_11

    .line 1156
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1159
    :cond_11
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    .line 1161
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    if-eqz v5, :cond_12

    .line 1162
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1259
    :cond_12
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->getSelectModeFrom()I

    move-result v5

    if-nez v5, :cond_13

    .line 1261
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v5, v9, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1263
    .local v2, "msg":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    if-eqz v5, :cond_13

    .line 1264
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1265
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/sec/android/app/myfiles/MainActivity;->setSpinnerWidth(Ljava/lang/String;)V

    .line 1268
    .end local v2    # "msg":Ljava/lang/String;
    :cond_13
    :goto_3
    return-void

    .restart local v0    # "deleteMenuItem":Landroid/view/MenuItem;
    :cond_14
    move v5, v7

    .line 1146
    goto :goto_0

    :cond_15
    move v5, v7

    .line 1148
    goto :goto_1

    .line 1164
    .end local v0    # "deleteMenuItem":Landroid/view/MenuItem;
    :cond_16
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->getSelectModeFrom()I

    move-result v5

    if-ne v5, v10, :cond_12

    .line 1165
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    .line 1166
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v1, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    if-eqz v1, :cond_12

    .line 1167
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1168
    const/4 v5, 0x0

    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 1169
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    invoke-interface {v5, v11}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 1171
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    if-eqz v5, :cond_17

    .line 1172
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1175
    :cond_17
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v5, v9, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1177
    .restart local v2    # "msg":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    if-eqz v5, :cond_13

    .line 1178
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1179
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/sec/android/app/myfiles/MainActivity;->setSpinnerWidth(Ljava/lang/String;)V

    goto :goto_3

    .line 1186
    .end local v2    # "msg":Ljava/lang/String;
    :cond_18
    if-ne p1, p2, :cond_1c

    .line 1188
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->getSelectModeFrom()I

    move-result v5

    if-nez v5, :cond_1a

    .line 1190
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v1, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    if-eqz v1, :cond_19

    .line 1192
    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1195
    :cond_19
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    invoke-virtual {v5, v7, v6}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    .line 1197
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    if-eqz v5, :cond_12

    .line 1198
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setSelected(Z)V

    goto/16 :goto_2

    .line 1200
    :cond_1a
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->getSelectModeFrom()I

    move-result v5

    if-ne v5, v10, :cond_12

    .line 1201
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    invoke-virtual {v5, v7, v6}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    .line 1202
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v1, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    if-eqz v1, :cond_12

    .line 1203
    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1204
    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1205
    const/4 v5, 0x0

    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 1206
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    invoke-interface {v5, v11}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 1208
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    if-eqz v5, :cond_1b

    .line 1209
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1212
    :cond_1b
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v5, v9, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1214
    .restart local v2    # "msg":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    if-eqz v5, :cond_13

    .line 1215
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1216
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/sec/android/app/myfiles/MainActivity;->setSpinnerWidth(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1224
    .end local v2    # "msg":Ljava/lang/String;
    :cond_1c
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->getSelectModeFrom()I

    move-result v5

    if-nez v5, :cond_1e

    .line 1226
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v1, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    if-eqz v1, :cond_1d

    .line 1228
    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1231
    :cond_1d
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    invoke-virtual {v5, v6, v6}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    .line 1233
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    if-eqz v5, :cond_12

    .line 1234
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setSelected(Z)V

    goto/16 :goto_2

    .line 1236
    :cond_1e
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->getSelectModeFrom()I

    move-result v5

    if-ne v5, v10, :cond_12

    .line 1237
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    invoke-virtual {v5, v6, v6}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    .line 1238
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v1, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    if-eqz v1, :cond_12

    .line 1239
    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1240
    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1241
    const/4 v5, 0x0

    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 1242
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    invoke-interface {v5, v11}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 1244
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    if-eqz v5, :cond_1f

    .line 1245
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1248
    :cond_1f
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v5, v9, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1250
    .restart local v2    # "msg":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    if-eqz v5, :cond_13

    .line 1251
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1252
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/sec/android/app/myfiles/MainActivity;->setSpinnerWidth(Ljava/lang/String;)V

    goto/16 :goto_3
.end method

.method public singleSelectionChanged(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 1273
    return-void
.end method
