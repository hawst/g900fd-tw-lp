.class public Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
.source "CloudListAdapter.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesShortcutAdapter;


# instance fields
.field private categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

.field private mContext:Landroid/content/Context;

.field private mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

.field private mIsDropBoxEnabled:Z

.field private mIsSelectMode:Z

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;"
        }
    .end annotation
.end field

.field private mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

.field private mSelectItemPosition:I

.field private mTotalSize:J

.field public notifyViaConnectionChange:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "categoryHomeFragment"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;-><init>()V

    .line 38
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mIsDropBoxEnabled:Z

    .line 40
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->notifyViaConnectionChange:Z

    .line 43
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mContext:Landroid/content/Context;

    .line 44
    iput-object p2, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 51
    .local v8, "res":Landroid/content/res/Resources;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isDropBoxEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    iget-object v10, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    const/16 v1, 0x1f

    const v2, 0x7f0b00fa

    invoke-virtual {v8, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    invoke-virtual {v10, v4, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 60
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->loadShortcuts()Ljava/util/ArrayList;

    move-result-object v9

    .line 61
    .local v9, "shortcuts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 62
    .local v7, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 56
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v7    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .end local v9    # "shortcuts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isDropBoxEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    iget-object v10, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    const/16 v1, 0x8

    const v2, 0x7f0b0008

    invoke-virtual {v8, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    invoke-virtual {v10, v4, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 64
    .restart local v6    # "i$":Ljava/util/Iterator;
    .restart local v9    # "shortcuts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    :cond_2
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "categoryHomeFragment"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    const/4 v0, 0x0

    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;-><init>()V

    .line 38
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mIsDropBoxEnabled:Z

    .line 40
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->notifyViaConnectionChange:Z

    .line 67
    iput-object p2, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .line 69
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mContext:Landroid/content/Context;

    .line 71
    iput-object p3, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    .line 73
    return-void
.end method

.method private isApplyMarqueeToSize()Z
    .locals 2

    .prologue
    .line 595
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 597
    .local v0, "languageCode":Ljava/lang/String;
    const-string v1, "ar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method


# virtual methods
.method public addItem(Lcom/sec/android/app/myfiles/element/ShortcutItem;)V
    .locals 2
    .param p1, "item"    # Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .prologue
    .line 78
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v1

    .line 80
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    monitor-exit v1

    .line 82
    return-void

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public afterRenameShortcut()V
    .locals 1

    .prologue
    .line 522
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->loadShortcuts()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    .line 523
    invoke-super {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->notifyDataSetChanged()V

    .line 524
    return-void
.end method

.method public finishSelectMode()V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mIsSelectMode:Z

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->unselectAllItem()V

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->notifyDataSetChanged()V

    .line 108
    return-void
.end method

.method public getArrayList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 601
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCount()I
    .locals 3

    .prologue
    .line 206
    const/4 v0, 0x0

    .line 208
    .local v0, "value":I
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v2

    .line 210
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 211
    monitor-exit v2

    .line 213
    return v0

    .line 211
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 227
    const/4 v1, 0x0

    .line 231
    .local v1, "obj":Ljava/lang/Object;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 234
    monitor-exit v3

    .line 241
    .end local v1    # "obj":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 234
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 236
    :catch_0
    move-exception v0

    .line 238
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 247
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getSelectableItemsCount()I
    .locals 2

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->getCount()I

    move-result v0

    .line 218
    .local v0, "cnt":I
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isDropBoxEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 219
    add-int/lit8 v0, v0, -0x1

    .line 221
    :cond_0
    return v0
.end method

.method public getSelectedItem()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 168
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 170
    .local v2, "selectedItem":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v4

    .line 172
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 174
    .local v1, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 176
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 179
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 181
    return-object v2
.end method

.method public getSelectedItemCount()I
    .locals 5

    .prologue
    .line 187
    const/4 v2, 0x0

    .line 189
    .local v2, "result":I
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v4

    .line 191
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 193
    .local v1, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 195
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 198
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_1
    monitor-exit v4

    .line 200
    return v2

    .line 198
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public getSelectedItemPosition()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 162
    const/4 v0, 0x0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 264
    iget-object v9, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mContext:Landroid/content/Context;

    const-string v10, "layout_inflater"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 265
    .local v2, "inflater":Landroid/view/LayoutInflater;
    if-nez p2, :cond_0

    .line 266
    const v9, 0x7f040031

    const/4 v10, 0x0

    invoke-virtual {v2, v9, p3, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 268
    :cond_0
    const v9, 0x7f0f00c4

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 269
    .local v8, "v":Landroid/view/View;
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f090010

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    float-to-int v10, v10

    iput v10, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 270
    iget-object v9, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v9, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 271
    .local v3, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    const v9, 0x7f0f0031

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 272
    .local v1, "icon":Landroid/widget/ImageView;
    const v9, 0x7f0f0032

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 273
    .local v7, "title":Landroid/widget/TextView;
    const v9, 0x7f0f0067

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout;

    .line 274
    .local v6, "sizeContainer":Landroid/widget/RelativeLayout;
    const v9, 0x7f0f005a

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 275
    .local v5, "size":Landroid/widget/TextView;
    const v9, 0x7f0f0069

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 276
    .local v0, "date":Landroid/widget/TextView;
    const v9, 0x7f0f0037

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 277
    .local v4, "mSelectCheckBox":Landroid/widget/CheckBox;
    const/4 v9, 0x0

    invoke-virtual {v4, v9}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 278
    const/16 v9, 0x8

    invoke-virtual {v6, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 279
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v9

    if-eqz v9, :cond_7

    .line 281
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v9

    const/16 v10, 0x9

    if-ne v9, v10, :cond_1

    .line 282
    const v9, 0x7f0f0061

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    .line 283
    const/4 v9, 0x0

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 286
    :cond_1
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 303
    :cond_2
    :goto_0
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getTitle()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 304
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->isSelectMode()Z

    move-result v9

    if-eqz v9, :cond_d

    .line 305
    const/4 v9, 0x0

    invoke-virtual {v4, v9}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 306
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelected()Z

    move-result v9

    if-eqz v9, :cond_c

    .line 307
    const/4 v9, 0x1

    invoke-virtual {v4, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 314
    :goto_1
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v9

    const/16 v10, 0x8

    if-eq v9, v10, :cond_3

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v9

    const/16 v10, 0x1f

    if-ne v9, v10, :cond_13

    .line 316
    :cond_3
    iget-object v9, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "sec_container_"

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_4

    iget-object v9, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_4

    iget-boolean v9, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mIsSelectMode:Z

    if-eqz v9, :cond_e

    .line 319
    :cond_4
    const/4 v9, 0x0

    const-string v10, "MyFiles"

    const-string v11, "mIsDropBoxEnabled = false;"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 320
    const/4 v9, 0x0

    invoke-virtual {p2, v9}, Landroid/view/View;->setEnabled(Z)V

    .line 321
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mIsDropBoxEnabled:Z

    .line 323
    const v9, 0x3ecccccd    # 0.4f

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 324
    const v9, -0x777778

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 325
    const/16 v9, 0x8

    invoke-virtual {v4, v9}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 338
    :goto_2
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v9

    const/16 v10, 0x9

    if-eq v9, v10, :cond_6

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v9

    const/16 v10, 0xa

    if-eq v9, v10, :cond_6

    .line 339
    const/4 v9, 0x0

    invoke-virtual {v6, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 340
    const/16 v9, 0x8

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 342
    const/4 v9, 0x0

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 344
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v9

    if-eqz v9, :cond_f

    .line 345
    iget-object v9, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mContext:Landroid/content/Context;

    iget-wide v10, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mTotalSize:J

    invoke-static {v9, v10, v11}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 359
    :cond_5
    :goto_3
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->isApplyMarqueeToSize()Z

    move-result v9

    if-eqz v9, :cond_12

    .line 361
    sget-object v9, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 363
    const/4 v9, -0x1

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setMarqueeRepeatLimit(I)V

    .line 365
    const/4 v9, 0x1

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setSelected(Z)V

    .line 384
    :cond_6
    :goto_4
    return-object p2

    .line 288
    :cond_7
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v9

    const/16 v10, 0x9

    if-ne v9, v10, :cond_8

    .line 290
    const/16 v9, 0x8

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 291
    const v9, 0x7f0f0061

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 294
    :cond_8
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v9

    const/16 v10, 0x1a

    if-ne v9, v10, :cond_9

    .line 295
    const v9, 0x7f020032

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 296
    :cond_9
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v9

    const/16 v10, 0xa

    if-ne v9, v10, :cond_a

    .line 297
    const v9, 0x7f020027

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 298
    :cond_a
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v9

    const/16 v10, 0x8

    if-ne v9, v10, :cond_b

    .line 299
    const v9, 0x7f020026

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 300
    :cond_b
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v9

    const/16 v10, 0x1f

    if-ne v9, v10, :cond_2

    .line 301
    const v9, 0x7f020021

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 309
    :cond_c
    const/4 v9, 0x0

    invoke-virtual {v4, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_1

    .line 312
    :cond_d
    const/16 v9, 0x8

    invoke-virtual {v4, v9}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto/16 :goto_1

    .line 330
    :cond_e
    const/4 v9, 0x0

    const-string v10, "MyFiles"

    const-string v11, "mIsDropBoxEnabled = true;"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 331
    const/4 v9, 0x1

    invoke-virtual {p2, v9}, Landroid/view/View;->setEnabled(Z)V

    .line 332
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mIsDropBoxEnabled:Z

    .line 334
    const/high16 v9, 0x3f800000    # 1.0f

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 335
    iget-object v9, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f080019

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_2

    .line 346
    :cond_f
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v9

    const/16 v10, 0x8

    if-ne v9, v10, :cond_5

    .line 347
    iget-object v9, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/sec/android/app/myfiles/utils/Utils;->isDropboxAccountIsSignin(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_10

    .line 348
    iget-object v9, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mContext:Landroid/content/Context;

    iget-wide v10, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mTotalSize:J

    invoke-static {v9, v10, v11}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 350
    :cond_10
    iget-object v9, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/sec/android/app/myfiles/utils/Utils;->isDropboxAccountAdded(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_11

    .line 351
    const-string v9, ""

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 352
    const/16 v9, 0x8

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 356
    :goto_5
    const/4 v9, 0x1

    const/high16 v10, 0x41700000    # 15.0f

    invoke-virtual {v5, v9, v10}, Landroid/widget/TextView;->setTextSize(IF)V

    goto/16 :goto_3

    .line 354
    :cond_11
    iget-object v9, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0b014f

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 369
    :cond_12
    const/4 v9, 0x0

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 371
    const/4 v9, 0x0

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setSelected(Z)V

    goto/16 :goto_4

    .line 375
    :cond_13
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v9

    const/16 v10, 0x9

    if-ne v9, v10, :cond_6

    .line 377
    iget-object v9, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    if-eqz v9, :cond_6

    iget-object v9, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->checkAllshareEnabled()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 379
    const/4 v9, 0x1

    invoke-virtual {p2, v9}, Landroid/view/View;->setEnabled(Z)V

    .line 380
    const/high16 v9, 0x3f800000    # 1.0f

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 381
    iget-object v9, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f080019

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_4
.end method

.method public isEnabled(I)Z
    .locals 4
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x1

    .line 252
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 253
    .local v0, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v2

    const/16 v3, 0x1f

    if-ne v2, v3, :cond_1

    .line 254
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mIsDropBoxEnabled:Z

    if-eqz v2, :cond_2

    .line 260
    :cond_1
    :goto_0
    return v1

    .line 257
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isSelectMode()Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mIsSelectMode:Z

    return v0
.end method

.method public loadShortcuts()Ljava/util/ArrayList;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 388
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 389
    .local v13, "hashMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 393
    .local v16, "shortcutList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getLastUsedShortcutIndex(Landroid/content/Context;)I

    move-result v15

    .line 394
    .local v15, "maxItems":I
    const/4 v4, 0x0

    .line 395
    .local v4, "where":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    const-string v6, "shortcut_index ASC"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 397
    .local v11, "cursor":Landroid/database/Cursor;
    if-eqz v11, :cond_0

    .line 398
    :goto_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 400
    const-string v1, "shortcut_index"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 401
    .local v10, "index":I
    const-string v1, "type"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 403
    .local v6, "shortcutType":I
    const/4 v5, 0x0

    .line 405
    .local v5, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    packed-switch v6, :pswitch_data_0

    .line 421
    new-instance v5, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .end local v5    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    const-string v1, "title"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v1, "_data"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    invoke-direct/range {v5 .. v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    .line 433
    .restart local v5    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :goto_1
    const-string v1, "shortcut_drawable"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->convertByteArrayToDrawable([B)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 446
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v13, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 463
    .end local v4    # "where":Ljava/lang/String;
    .end local v5    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .end local v6    # "shortcutType":I
    .end local v10    # "index":I
    .end local v11    # "cursor":Landroid/database/Cursor;
    .end local v15    # "maxItems":I
    :catch_0
    move-exception v12

    .line 466
    .local v12, "e":Ljava/lang/Exception;
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    .line 468
    .end local v12    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_2
    return-object v16

    .line 409
    .restart local v4    # "where":Ljava/lang/String;
    .restart local v5    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .restart local v6    # "shortcutType":I
    .restart local v10    # "index":I
    .restart local v11    # "cursor":Landroid/database/Cursor;
    .restart local v15    # "maxItems":I
    :pswitch_0
    :try_start_1
    new-instance v5, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .end local v5    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    const-string v1, "title"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Security;->getInstance()Lcom/sec/android/app/myfiles/utils/Security;

    move-result-object v1

    const-string v2, "_data"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/utils/Security;->decrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    invoke-direct/range {v5 .. v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    .line 417
    .restart local v5    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    goto :goto_1

    .line 448
    .end local v5    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .end local v6    # "shortcutType":I
    .end local v10    # "index":I
    :cond_1
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_3
    add-int/lit8 v1, v15, 0x1

    if-ge v14, v1, :cond_4

    .line 449
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v13, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 451
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v13, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v1

    const/16 v2, 0x9

    if-eq v1, v2, :cond_2

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v13, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_3

    .line 452
    :cond_2
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v13, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 448
    :cond_3
    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    .line 460
    :cond_4
    invoke-interface {v11}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 405
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method public notifyDataSetChanged()V
    .locals 17

    .prologue
    .line 545
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->notifyViaConnectionChange:Z

    if-nez v1, :cond_2

    .line 547
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v13

    .line 548
    .local v13, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 549
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->loadShortcuts()Ljava/util/ArrayList;

    move-result-object v14

    .line 550
    .local v14, "shortcuts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 551
    .local v9, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    .local v10, "obj":Ljava/lang/Object;
    move-object v11, v10

    .line 552
    check-cast v11, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 554
    .local v11, "prevItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v9, v11}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 556
    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelected()Z

    move-result v1

    invoke-virtual {v9, v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setSelected(Z)V

    goto :goto_1

    .line 559
    .end local v10    # "obj":Ljava/lang/Object;
    .end local v11    # "prevItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 562
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v9    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .end local v13    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v14    # "shortcuts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    :cond_2
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->notifyViaConnectionChange:Z

    .line 566
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v1

    const/16 v2, 0x1f

    if-eq v1, v2, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 567
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 570
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 576
    .local v12, "res":Landroid/content/res/Resources;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 577
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isDropBoxEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 578
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    const/16 v16, 0x0

    new-instance v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    const/16 v2, 0x1f

    const v3, 0x7f0b00fa

    invoke-virtual {v12, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    move/from16 v0, v16

    invoke-virtual {v15, v0, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 585
    :cond_5
    :goto_2
    invoke-super/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->notifyDataSetChanged()V

    .line 586
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->CloudListRefresh()V

    .line 587
    return-void

    .line 581
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isDropBoxEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 582
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    const/16 v16, 0x0

    new-instance v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    const/16 v2, 0x8

    const v3, 0x7f0b0008

    invoke-virtual {v12, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    move/from16 v0, v16

    invoke-virtual {v15, v0, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_2
.end method

.method public onRefresh()V
    .locals 0

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->notifyDataSetChanged()V

    .line 88
    return-void
.end method

.method public removeAllItem(Ljava/util/ArrayList;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 498
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    if-eqz p1, :cond_0

    .line 500
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v1

    .line 502
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    monitor-exit v1

    .line 507
    :goto_0
    return v0

    .line 503
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 507
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeItem(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 512
    const/4 v0, 0x0

    .line 514
    .local v0, "result":Z
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v2

    .line 516
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 517
    monitor-exit v2

    .line 519
    return v0

    .line 517
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public selectAllItem()V
    .locals 5

    .prologue
    .line 132
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v3

    .line 134
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 136
    .local v1, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v2

    const/16 v4, 0x1f

    if-eq v2, v4, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v2

    const/16 v4, 0x8

    if-eq v2, v4, :cond_0

    .line 139
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setSelected(Z)V

    goto :goto_0

    .line 141
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 142
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setAllItemCount()V

    .line 143
    invoke-super {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->notifyDataSetChanged()V

    .line 144
    return-void
.end method

.method public selectItem(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 119
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v2

    .line 121
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 123
    .local v0, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelected()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setSelected(Z)V

    .line 124
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setAllItemCount()V

    .line 126
    invoke-super {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->notifyDataSetChanged()V

    .line 127
    return-void

    .line 123
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 124
    .end local v0    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public selectMode(Z)V
    .locals 0
    .param p1, "selectMode"    # Z

    .prologue
    .line 605
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mIsSelectMode:Z

    .line 606
    return-void
.end method

.method public setCurrentWorkingIndex()V
    .locals 3

    .prologue
    .line 486
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v2

    .line 488
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 490
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 491
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getIndex()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mSelectItemPosition:I

    .line 488
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 494
    :cond_1
    monitor-exit v2

    .line 495
    return-void

    .line 494
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setDevicesCollection(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;)V
    .locals 0
    .param p1, "mDevicesCollection"    # Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    .prologue
    .line 531
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    .line 532
    return-void
.end method

.method public setNearbyDevicesProvider(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)V
    .locals 0
    .param p1, "mNearbyDevicesProvider"    # Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    .prologue
    .line 527
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    .line 528
    return-void
.end method

.method public setmTotalSize(J)V
    .locals 1
    .param p1, "mTotalSize"    # J

    .prologue
    .line 591
    iput-wide p1, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mTotalSize:J

    .line 592
    return-void
.end method

.method public shortcutEdit()V
    .locals 5

    .prologue
    .line 471
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 472
    .local v2, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->setCurrentWorkingIndex()V

    .line 473
    const/16 v1, 0xd

    .line 474
    .local v1, "categoryType":I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 475
    .local v0, "argument":Landroid/os/Bundle;
    const-string v3, "FOLDERPATH"

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    const-string v3, "shortcut_working_index_intent_key"

    iget v4, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mSelectItemPosition:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 477
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mContext:Landroid/content/Context;

    instance-of v3, v3, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v3, :cond_0

    .line 478
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v3, v1, v0}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    .line 481
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->finishSelectMode()V

    .line 483
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->finishSelectMode()V

    .line 484
    return-void
.end method

.method public startSelectMode(II)V
    .locals 1
    .param p1, "selectType"    # I
    .param p2, "from"    # I

    .prologue
    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mIsSelectMode:Z

    .line 96
    invoke-super {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->notifyDataSetChanged()V

    .line 97
    return-void
.end method

.method public unselectAllItem()V
    .locals 4

    .prologue
    .line 149
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v3

    .line 151
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 153
    .local v1, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setSelected(Z)V

    goto :goto_0

    .line 155
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 156
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setAllItemCount()V

    .line 157
    invoke-super {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->notifyDataSetChanged()V

    .line 158
    return-void
.end method
