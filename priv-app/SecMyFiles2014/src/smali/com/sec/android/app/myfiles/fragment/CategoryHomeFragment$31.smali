.class Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$31;
.super Landroid/content/pm/IPackageStatsObserver$Stub;
.source "CategoryHomeFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getDownloadedAppSize()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V
    .locals 0

    .prologue
    .line 3452
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$31;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-direct {p0}, Landroid/content/pm/IPackageStatsObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetStatsCompleted(Landroid/content/pm/PackageStats;Z)V
    .locals 10
    .param p1, "stats"    # Landroid/content/pm/PackageStats;
    .param p2, "succeeded"    # Z

    .prologue
    .line 3456
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$31;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mUpdateCategorySizeHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$800(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Landroid/os/Handler;

    move-result-object v4

    monitor-enter v4

    .line 3458
    :try_start_0
    iget-wide v6, p1, Landroid/content/pm/PackageStats;->externalCodeSize:J

    iget-wide v8, p1, Landroid/content/pm/PackageStats;->externalObbSize:J

    add-long/2addr v6, v8

    iget-wide v8, p1, Landroid/content/pm/PackageStats;->externalDataSize:J

    add-long/2addr v6, v8

    iget-wide v8, p1, Landroid/content/pm/PackageStats;->externalMediaSize:J

    add-long/2addr v6, v8

    iget-wide v8, p1, Landroid/content/pm/PackageStats;->codeSize:J

    add-long/2addr v6, v8

    iget-wide v8, p1, Landroid/content/pm/PackageStats;->dataSize:J

    add-long v2, v6, v8

    .line 3462
    .local v2, "size":J
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$31;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mUpdateCategorySizeHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$800(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Landroid/os/Handler;

    move-result-object v1

    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 3464
    .local v0, "msg":Landroid/os/Message;
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 3466
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$31;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mUpdateCategorySizeHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$800(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 3467
    monitor-exit v4

    .line 3468
    return-void

    .line 3467
    .end local v0    # "msg":Landroid/os/Message;
    .end local v2    # "size":J
    :catchall_0
    move-exception v1

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
