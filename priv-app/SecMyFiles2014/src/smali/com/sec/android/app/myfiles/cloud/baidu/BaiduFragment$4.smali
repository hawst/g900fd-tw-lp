.class Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$4;
.super Ljava/lang/Object;
.source "BaiduFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)V
    .locals 0

    .prologue
    .line 1235
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$4;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 8
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1240
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$4;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$3700(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-result-object v5

    instance-of v5, v5, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v5, :cond_0

    .line 1241
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$4;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$3800(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchMode:Z

    if-eqz v5, :cond_0

    move v5, v6

    .line 1312
    :goto_0
    return v5

    .line 1246
    :cond_0
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    .line 1248
    .local v1, "cursor":Landroid/database/Cursor;
    const/4 v4, 0x0

    .line 1249
    .local v4, "path":Ljava/lang/String;
    const/4 v2, 0x0

    .line 1252
    .local v2, "format":I
    if-eqz v1, :cond_6

    .line 1254
    const-string v5, "_data"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1256
    const-string v5, "format"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 1258
    .local v3, "index":I
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 1261
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$4;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->isSelectMode()Z

    move-result v5

    if-nez v5, :cond_3

    .line 1263
    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->isInternalRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_6

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->isExternalRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_6

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 1266
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$4;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    invoke-virtual {v5, v7, p3, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->startSelectMode(III)V

    .line 1268
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$4;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$3900(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$4;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$4000(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$4;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$4100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1271
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$4;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mCurrentListableView:Lcom/sec/android/app/myfiles/view/IDirectDragable;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$4200(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/view/IDirectDragable;

    move-result-object v5

    invoke-interface {v5, v7}, Lcom/sec/android/app/myfiles/view/IDirectDragable;->setDirectDragMode(Z)V

    :goto_1
    move v5, v7

    .line 1280
    goto :goto_0

    .line 1277
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$4;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mCurrentListableView:Lcom/sec/android/app/myfiles/view/IDirectDragable;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$4300(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/view/IDirectDragable;

    move-result-object v5

    invoke-interface {v5, v6}, Lcom/sec/android/app/myfiles/view/IDirectDragable;->setDirectDragMode(Z)V

    goto :goto_1

    .line 1285
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$4;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$4400(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemPosition()Ljava/util/ArrayList;

    move-result-object v5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1287
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$4;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$4500(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 1289
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$4;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # invokes: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->isShowTreeView()Z
    invoke-static {v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$4600(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Z

    move-result v5

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$4;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$4700(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$4;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$4800(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v5

    if-nez v5, :cond_5

    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$4;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mSideSyncControl:Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$5000(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$4;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$4900(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;->isKMSRunning(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$4;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mSideSyncControl:Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$5100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;

    invoke-static {}, Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;->isPSSRunning()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1293
    :cond_5
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1295
    .local v0, "arg":Landroid/os/Bundle;
    const-string v5, "baidu"

    invoke-virtual {v0, v5, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1296
    const-string v5, "ITEM_INITIATING_DRAG_PATH"

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1297
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$4;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    invoke-virtual {v5, p2, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->startDragMode(Landroid/view/View;Landroid/os/Bundle;)V

    .end local v0    # "arg":Landroid/os/Bundle;
    .end local v3    # "index":I
    :cond_6
    :goto_2
    move v5, v6

    .line 1312
    goto/16 :goto_0

    .line 1302
    .restart local v3    # "index":I
    :cond_7
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1304
    .restart local v0    # "arg":Landroid/os/Bundle;
    const-string v5, "baidu"

    invoke-virtual {v0, v5, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1306
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$4;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    invoke-virtual {v5, p2, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->startDragMode(Landroid/view/View;Landroid/os/Bundle;)V

    goto :goto_2
.end method
