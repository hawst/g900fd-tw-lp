.class Lcom/sec/android/app/myfiles/ftp/FTPSupplier$3;
.super Ljava/lang/Object;
.source "FTPSupplier.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/ftp/FTPAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->moveFiles([Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

.field final synthetic val$properListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/ftp/FTPSupplier;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 0

    .prologue
    .line 327
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$3;->this$0:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    iput-object p2, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$3;->val$properListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getWorkElement()Ljava/lang/String;
    .locals 1

    .prologue
    .line 356
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDone(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "result"    # Landroid/os/Bundle;

    .prologue
    .line 350
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$3;->val$properListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;->onMoveFilesSelfCompleted()V

    .line 351
    return-void
.end method

.method public work(Landroid/os/Bundle;)Z
    .locals 7
    .param p1, "arg"    # Landroid/os/Bundle;

    .prologue
    .line 330
    if-eqz p1, :cond_2

    .line 331
    const-string v4, "rename_from"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 332
    .local v2, "targetsFrom":[Ljava/lang/String;
    const-string v4, "rename_to"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 333
    .local v3, "targetsTo":[Ljava/lang/String;
    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    array-length v4, v2

    array-length v5, v3

    if-ne v4, v5, :cond_2

    .line 335
    const/4 v1, 0x1

    .line 336
    .local v1, "success":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_0

    .line 337
    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$3;->this$0:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    # getter for: Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->access$000(Lcom/sec/android/app/myfiles/ftp/FTPSupplier;)Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-result-object v4

    aget-object v5, v2, v0

    aget-object v6, v3, v0

    invoke-interface {v4, v5, v6}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncRenameFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 338
    if-nez v1, :cond_1

    .line 345
    .end local v0    # "i":I
    .end local v1    # "success":Z
    .end local v2    # "targetsFrom":[Ljava/lang/String;
    .end local v3    # "targetsTo":[Ljava/lang/String;
    :cond_0
    :goto_1
    return v1

    .line 336
    .restart local v0    # "i":I
    .restart local v1    # "success":Z
    .restart local v2    # "targetsFrom":[Ljava/lang/String;
    .restart local v3    # "targetsTo":[Ljava/lang/String;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 345
    .end local v0    # "i":I
    .end local v1    # "success":Z
    .end local v2    # "targetsFrom":[Ljava/lang/String;
    .end local v3    # "targetsTo":[Ljava/lang/String;
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method
