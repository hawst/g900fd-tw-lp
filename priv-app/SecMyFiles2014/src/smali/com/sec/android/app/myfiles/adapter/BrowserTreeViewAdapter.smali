.class public Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;
.super Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;
.source "BrowserTreeViewAdapter.java"


# static fields
.field private static final MODULE:Ljava/lang/String; = "BrowserTreeViewAdapter"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDragView:Landroid/view/View;

.field private mDragViewHolder:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;

.field private mItemOriginalTextColor:I

.field private mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

.field private mSelectedItemBackground:I

.field private mSelectedItemTextColor:I

.field private mUnavailableItemTextColor:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    const/4 v0, 0x0

    .line 86
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;-><init>(Landroid/content/Context;)V

    .line 70
    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mDragView:Landroid/view/View;

    .line 72
    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mDragViewHolder:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;

    .line 88
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mContext:Landroid/content/Context;

    .line 90
    iput-object p2, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .line 92
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08003d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mSelectedItemBackground:I

    .line 94
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08003b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mSelectedItemTextColor:I

    .line 96
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08003c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mUnavailableItemTextColor:I

    .line 98
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItemOriginalTextColor:I

    .line 106
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->prepareRoot()V

    .line 107
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mDragView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mDragView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mSelectedItemBackground:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mDragViewHolder:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;)Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;
    .param p1, "x1"    # Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mDragViewHolder:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mSelectedItemTextColor:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItemOriginalTextColor:I

    return v0
.end method

.method private getDepth(Ljava/lang/String;)I
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 770
    const/4 v0, 0x0

    .line 772
    .local v0, "depth":I
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 774
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    sget-char v3, Ljava/io/File;->separatorChar:C

    if-ne v2, v3, :cond_0

    .line 776
    add-int/lit8 v0, v0, 0x1

    .line 772
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 780
    :cond_1
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isInternalRootFolder(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 782
    const/4 v0, 0x1

    .line 789
    :cond_2
    :goto_1
    return v0

    .line 784
    :cond_3
    sget-object v2, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 786
    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method private getIndex(Ljava/lang/String;)I
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 755
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mPathItemMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .line 757
    .local v0, "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    if-nez v0, :cond_0

    .line 759
    const/4 v1, -0x1

    .line 763
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    goto :goto_0
.end method

.method private hasChildFolder(Ljava/lang/String;)Z
    .locals 9
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 795
    const/4 v6, 0x0

    .line 797
    .local v6, "cursor":Landroid/database/Cursor;
    const-string v3, "_data LIKE ? AND format = ?"

    .line 799
    .local v3, "where":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    const-string v0, "12289"

    aput-object v0, v4, v7

    .line 801
    .local v4, "whereArgs":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "external"

    invoke-static {v1}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-array v2, v7, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v2, v8

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 807
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 809
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v7

    .line 820
    :goto_0
    return v0

    .line 815
    :cond_0
    if-eqz v6, :cond_1

    .line 817
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    move v0, v8

    .line 820
    goto :goto_0
.end method

.method private prepareRoot()V
    .locals 11

    .prologue
    const v10, 0x7f0200af

    .line 114
    sget-boolean v7, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v7, :cond_0

    .line 120
    :cond_0
    sget-boolean v7, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-eqz v7, :cond_1

    .line 122
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getMountedUsbStorage()Ljava/util/ArrayList;

    move-result-object v3

    .line 124
    .local v3, "listUsbStorages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;

    .line 126
    .local v5, "usbStorage":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->getStoragePath()Ljava/lang/String;

    move-result-object v6

    .line 127
    .local v6, "usbStoragePath":Ljava/lang/String;
    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    goto :goto_0

    .line 133
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v3    # "listUsbStorages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;>;"
    .end local v5    # "usbStorage":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    .end local v6    # "usbStoragePath":Ljava/lang/String;
    :cond_1
    sget-boolean v7, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-eqz v7, :cond_2

    .line 139
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 155
    .local v4, "res":Landroid/content/res/Resources;
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->isDropBoxEnabled(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 157
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 166
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 172
    const-string v7, "9"

    const v8, 0x7f0b007c

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x9

    invoke-virtual {p0, v7, v8, v10, v9}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->prepareRootFolders(Ljava/lang/String;Ljava/lang/String;II)Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .line 175
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->isRemoteShareEnabled(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 176
    const-string v7, "38"

    const v8, 0x7f0b0009

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x26

    invoke-virtual {p0, v7, v8, v10, v9}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->prepareRootFolders(Ljava/lang/String;Ljava/lang/String;II)Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .line 177
    const-string v7, "39"

    const v8, 0x7f0b000a

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x27

    invoke-virtual {p0, v7, v8, v10, v9}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->prepareRootFolders(Ljava/lang/String;Ljava/lang/String;II)Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .line 180
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 181
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 182
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/myfiles/element/TreeViewItem;>;"
    :cond_6
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 183
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .line 184
    .local v1, "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    iget v7, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    const/16 v8, 0x1f

    if-eq v7, v8, :cond_7

    iget v7, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    const/16 v8, 0x8

    if-eq v7, v8, :cond_7

    iget v7, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    const/4 v8, 0x7

    if-eq v7, v8, :cond_7

    iget v7, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    const/16 v8, 0xd

    if-eq v7, v8, :cond_7

    iget v7, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    const/4 v8, 0x6

    if-eq v7, v8, :cond_7

    iget v7, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    const/16 v8, 0x28

    if-ne v7, v8, :cond_6

    .line 190
    :cond_7
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 195
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    .end local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/myfiles/element/TreeViewItem;>;"
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->notifyDataSetChanged()V

    .line 197
    return-void
.end method


# virtual methods
.method public addItem(Ljava/lang/String;IIZZZ)Lcom/sec/android/app/myfiles/element/TreeViewItem;
    .locals 7
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "iconResId"    # I
    .param p3, "expandedIconResId"    # I
    .param p4, "withSynchronized"    # Z
    .param p5, "withExpand"    # Z
    .param p6, "hasChildFolder"    # Z

    .prologue
    .line 683
    const/4 v3, 0x0

    .line 685
    .local v3, "parentPath":Ljava/lang/String;
    const/4 v4, 0x0

    .line 687
    .local v4, "sync":Ljava/lang/Object;
    if-eqz p4, :cond_2

    .line 689
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItems:Ljava/util/ArrayList;

    .line 696
    .end local v4    # "sync":Ljava/lang/Object;
    :goto_0
    monitor-enter v4

    .line 698
    :try_start_0
    new-instance v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;

    invoke-direct {v1}, Lcom/sec/android/app/myfiles/element/TreeViewItem;-><init>()V

    .line 700
    .local v1, "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    const/4 v5, 0x0

    iput v5, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    .line 702
    iput-object p1, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    .line 704
    const/16 v5, 0x2f

    invoke-virtual {p1, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mTitle:Ljava/lang/String;

    .line 706
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->getDepth(Ljava/lang/String;)I

    move-result v5

    iput v5, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mDepth:I

    .line 708
    iput p2, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mIconResId:I

    .line 710
    iput p3, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mExpandedIconResId:I

    .line 712
    iput-boolean p6, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mHasChildFolder:Z

    .line 714
    if-eqz p5, :cond_0

    .line 716
    const/4 v5, 0x1

    iput-boolean v5, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mIsExpanded:Z

    .line 718
    const/4 v5, 0x0

    invoke-virtual {p0, p1, v5}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->expandChild(Ljava/lang/String;Z)Z

    .line 721
    :cond_0
    iget v5, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mDepth:I

    if-lez v5, :cond_1

    .line 723
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getParentFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 727
    :cond_1
    const/4 v0, 0x0

    .line 729
    .local v0, "index":I
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 731
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mPathItemMap:Ljava/util/HashMap;

    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .line 733
    .local v2, "parentItem":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    if-nez v2, :cond_3

    .line 735
    const/4 v1, 0x0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 748
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    .end local v2    # "parentItem":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    :goto_1
    return-object v1

    .line 693
    .end local v0    # "index":I
    .restart local v4    # "sync":Ljava/lang/Object;
    :cond_2
    new-instance v4, Ljava/lang/Object;

    .end local v4    # "sync":Ljava/lang/Object;
    invoke-direct {v4}, Ljava/lang/Object;-><init>()V

    .restart local v4    # "sync":Ljava/lang/Object;
    goto :goto_0

    .line 739
    .end local v4    # "sync":Ljava/lang/Object;
    .restart local v0    # "index":I
    .restart local v1    # "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    .restart local v2    # "parentItem":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    :cond_3
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v5

    add-int/lit8 v0, v5, 0x1

    .line 742
    .end local v2    # "parentItem":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v5, v0, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 744
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mPathItemMap:Ljava/util/HashMap;

    invoke-virtual {v5, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 746
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->notifyDataSetChanged()V

    .line 748
    monitor-exit v4

    goto :goto_1

    .line 749
    .end local v0    # "index":I
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    :catchall_0
    move-exception v5

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5
.end method

.method public expandChild(Ljava/lang/String;)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 382
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->expandChild(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method protected expandChild(Ljava/lang/String;Z)Z
    .locals 21
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "withSynchronized"    # Z

    .prologue
    .line 389
    const/16 v16, 0x0

    .line 391
    .local v16, "sync":Ljava/lang/Object;
    if-eqz p2, :cond_0

    .line 393
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItems:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    .line 400
    .end local v16    # "sync":Ljava/lang/Object;
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mFoldedItemMap:Ljava/util/HashMap;

    move-object/from16 v18, v0

    monitor-enter v18

    .line 402
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mFoldedItemMap:Ljava/util/HashMap;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/ArrayList;

    .line 404
    .local v9, "foldedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/TreeViewItem;>;"
    if-eqz v9, :cond_2

    .line 406
    monitor-enter v16
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 408
    :try_start_1
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->getIndex(Ljava/lang/String;)I

    move-result v11

    .line 410
    .local v11, "index":I
    const/16 v17, -0x1

    move/from16 v0, v17

    if-ne v11, v0, :cond_1

    .line 412
    const/16 v17, 0x0

    monitor-exit v16
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v18
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 606
    .end local v11    # "index":I
    :goto_1
    return v17

    .line 397
    .end local v9    # "foldedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/TreeViewItem;>;"
    .restart local v16    # "sync":Ljava/lang/Object;
    :cond_0
    new-instance v16, Ljava/lang/Object;

    .end local v16    # "sync":Ljava/lang/Object;
    invoke-direct/range {v16 .. v16}, Ljava/lang/Object;-><init>()V

    .restart local v16    # "sync":Ljava/lang/Object;
    goto :goto_0

    .line 416
    .end local v16    # "sync":Ljava/lang/Object;
    .restart local v9    # "foldedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/TreeViewItem;>;"
    .restart local v11    # "index":I
    :cond_1
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItems:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    add-int/lit8 v19, v11, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1, v9}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 418
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mPathItemMap:Ljava/util/HashMap;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/sec/android/app/myfiles/element/TreeViewItem;

    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, v17

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mIsExpanded:Z

    .line 420
    monitor-exit v16
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 602
    .end local v11    # "index":I
    :goto_2
    :try_start_4
    monitor-exit v18
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 604
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->notifyDataSetChanged()V

    .line 606
    const/16 v17, 0x1

    goto :goto_1

    .line 420
    :catchall_0
    move-exception v17

    :try_start_5
    monitor-exit v16
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v17

    .line 602
    .end local v9    # "foldedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/TreeViewItem;>;"
    :catchall_1
    move-exception v17

    monitor-exit v18
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v17

    .line 425
    .restart local v9    # "foldedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/TreeViewItem;>;"
    :cond_2
    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v17, v0

    const/16 v19, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInFolder(Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v4

    .line 427
    .local v4, "c":Landroid/database/Cursor;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 429
    .local v6, "childItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/TreeViewItem;>;"
    monitor-enter v16
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 431
    if-eqz v4, :cond_c

    .line 433
    :try_start_8
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v17

    if-lez v17, :cond_d

    .line 435
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    .line 437
    const/4 v12, 0x0

    .line 441
    .local v12, "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    :cond_3
    new-instance v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .end local v12    # "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    invoke-direct {v12}, Lcom/sec/android/app/myfiles/element/TreeViewItem;-><init>()V

    .line 443
    .restart local v12    # "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    const-string v17, "_data"

    move-object/from16 v0, v17

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move/from16 v0, v17

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    .line 445
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 447
    const/16 v17, 0x0

    move/from16 v0, v17

    iput-boolean v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mHasChildFolder:Z

    .line 449
    iget-object v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v20, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/Android"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 451
    new-instance v14, Ljava/io/File;

    iget-object v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v14, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 453
    .local v14, "mFile":Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v15

    .line 455
    .local v15, "mFileList":[Ljava/io/File;
    if-eqz v15, :cond_4

    .line 457
    move-object v3, v15

    .local v3, "arr$":[Ljava/io/File;
    array-length v13, v3

    .local v13, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_3
    if-ge v10, v13, :cond_4

    aget-object v8, v3, v10

    .line 459
    .local v8, "f":Ljava/io/File;
    if-eqz v8, :cond_5

    .line 461
    invoke-virtual {v8}, Ljava/io/File;->isDirectory()Z

    move-result v17

    if-eqz v17, :cond_5

    .line 463
    const/16 v17, 0x1

    move/from16 v0, v17

    iput-boolean v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mHasChildFolder:Z

    .line 480
    .end local v3    # "arr$":[Ljava/io/File;
    .end local v8    # "f":Ljava/io/File;
    .end local v10    # "i$":I
    .end local v13    # "len$":I
    .end local v14    # "mFile":Ljava/io/File;
    .end local v15    # "mFileList":[Ljava/io/File;
    :cond_4
    :goto_4
    iget-object v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/myfiles/utils/Utils;->isInternalRootFolder(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 482
    const/16 v17, 0x1

    move/from16 v0, v17

    iput-boolean v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mHasChildFolder:Z

    .line 484
    const v17, 0x7f020022

    move/from16 v0, v17

    iput v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mIconResId:I

    .line 486
    const v17, 0x7f020022

    move/from16 v0, v17

    iput v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mExpandedIconResId:I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 490
    :try_start_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v19, 0x7f0b003e

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mTitle:Ljava/lang/String;
    :try_end_9
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 563
    :goto_5
    :try_start_a
    iget-object v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->getDepth(Ljava/lang/String;)I

    move-result v17

    move/from16 v0, v17

    iput v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mDepth:I

    .line 565
    const/16 v17, 0x0

    move/from16 v0, v17

    iput v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    .line 567
    invoke-virtual {v6, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 569
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v17

    if-nez v17, :cond_3

    .line 571
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->getIndex(Ljava/lang/String;)I

    move-result v11

    .line 573
    .restart local v11    # "index":I
    const/16 v17, -0x1

    move/from16 v0, v17

    if-ne v11, v0, :cond_a

    .line 575
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 577
    const/16 v17, 0x0

    monitor-exit v16
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :try_start_b
    monitor-exit v18
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto/16 :goto_1

    .line 457
    .end local v11    # "index":I
    .restart local v3    # "arr$":[Ljava/io/File;
    .restart local v8    # "f":Ljava/io/File;
    .restart local v10    # "i$":I
    .restart local v13    # "len$":I
    .restart local v14    # "mFile":Ljava/io/File;
    .restart local v15    # "mFileList":[Ljava/io/File;
    :cond_5
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 473
    .end local v3    # "arr$":[Ljava/io/File;
    .end local v8    # "f":Ljava/io/File;
    .end local v10    # "i$":I
    .end local v13    # "len$":I
    .end local v14    # "mFile":Ljava/io/File;
    .end local v15    # "mFileList":[Ljava/io/File;
    :cond_6
    :try_start_c
    iget-object v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->hasChildFolder(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 475
    const/16 v17, 0x1

    move/from16 v0, v17

    iput-boolean v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mHasChildFolder:Z

    goto :goto_4

    .line 600
    .end local v12    # "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    :catchall_2
    move-exception v17

    monitor-exit v16
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    :try_start_d
    throw v17
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 492
    .restart local v12    # "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    :catch_0
    move-exception v7

    .line 494
    .local v7, "e":Landroid/content/res/Resources$NotFoundException;
    :try_start_e
    invoke-virtual {v7}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    .line 496
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 498
    const/16 v17, 0x0

    monitor-exit v16
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    :try_start_f
    monitor-exit v18
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    goto/16 :goto_1

    .line 501
    .end local v7    # "e":Landroid/content/res/Resources$NotFoundException;
    :cond_7
    :try_start_10
    iget-object v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/myfiles/utils/Utils;->isExternalRootFolder(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 503
    const/16 v17, 0x1

    move/from16 v0, v17

    iput-boolean v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mHasChildFolder:Z

    .line 505
    const v17, 0x7f02002f

    move/from16 v0, v17

    iput v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mIconResId:I

    .line 507
    const v17, 0x7f02002f

    move/from16 v0, v17

    iput v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mExpandedIconResId:I
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    .line 511
    :try_start_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v19, 0x7f0b0040

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mTitle:Ljava/lang/String;
    :try_end_11
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_11 .. :try_end_11} :catch_1
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    goto/16 :goto_5

    .line 513
    :catch_1
    move-exception v7

    .line 515
    .restart local v7    # "e":Landroid/content/res/Resources$NotFoundException;
    :try_start_12
    invoke-virtual {v7}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    .line 517
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 519
    const/16 v17, 0x0

    monitor-exit v16
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_2

    :try_start_13
    monitor-exit v18
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    goto/16 :goto_1

    .line 522
    .end local v7    # "e":Landroid/content/res/Resources$NotFoundException;
    :cond_8
    :try_start_14
    iget-object v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/myfiles/utils/Utils;->isUsbStorageRootFolder(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 524
    const/16 v17, 0x1

    move/from16 v0, v17

    iput-boolean v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mHasChildFolder:Z

    .line 526
    const v17, 0x7f020069

    move/from16 v0, v17

    iput v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mIconResId:I

    .line 528
    const v17, 0x7f020069

    move/from16 v0, v17

    iput v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mExpandedIconResId:I
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_2

    .line 532
    :try_start_15
    iget-object v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    move-object/from16 v17, v0

    iget-object v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x2f

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v19

    add-int/lit8 v19, v19, 0x1

    iget-object v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    move-object/from16 v0, v17

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mTitle:Ljava/lang/String;
    :try_end_15
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_15 .. :try_end_15} :catch_2
    .catchall {:try_start_15 .. :try_end_15} :catchall_2

    goto/16 :goto_5

    .line 534
    :catch_2
    move-exception v7

    .line 536
    .restart local v7    # "e":Landroid/content/res/Resources$NotFoundException;
    :try_start_16
    invoke-virtual {v7}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    .line 538
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 540
    const/16 v17, 0x0

    monitor-exit v16
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_2

    :try_start_17
    monitor-exit v18
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_1

    goto/16 :goto_1

    .line 545
    .end local v7    # "e":Landroid/content/res/Resources$NotFoundException;
    :cond_9
    const v17, 0x7f0200af

    :try_start_18
    move/from16 v0, v17

    iput v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mIconResId:I

    .line 547
    const v17, 0x7f0200b0

    move/from16 v0, v17

    iput v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mExpandedIconResId:I
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_2

    .line 551
    :try_start_19
    iget-object v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    move-object/from16 v17, v0

    iget-object v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x2f

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v19

    add-int/lit8 v19, v19, 0x1

    iget-object v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    move-object/from16 v0, v17

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v12, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mTitle:Ljava/lang/String;
    :try_end_19
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_19 .. :try_end_19} :catch_3
    .catchall {:try_start_19 .. :try_end_19} :catchall_2

    goto/16 :goto_5

    .line 553
    :catch_3
    move-exception v7

    .line 555
    .restart local v7    # "e":Landroid/content/res/Resources$NotFoundException;
    :try_start_1a
    invoke-virtual {v7}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    .line 557
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 559
    const/16 v17, 0x0

    monitor-exit v16
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_2

    :try_start_1b
    monitor-exit v18
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_1

    goto/16 :goto_1

    .line 581
    .end local v7    # "e":Landroid/content/res/Resources$NotFoundException;
    .restart local v11    # "index":I
    :cond_a
    :try_start_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItems:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    add-int/lit8 v19, v11, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1, v6}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 583
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_b

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .line 585
    .local v5, "child":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mPathItemMap:Ljava/util/HashMap;

    move-object/from16 v17, v0

    iget-object v0, v5, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6

    .line 588
    .end local v5    # "child":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mPathItemMap:Ljava/util/HashMap;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/sec/android/app/myfiles/element/TreeViewItem;

    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, v17

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mIsExpanded:Z

    .line 591
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 600
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "index":I
    .end local v12    # "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    :cond_c
    monitor-exit v16

    goto/16 :goto_2

    .line 595
    :cond_d
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 597
    const/16 v17, 0x0

    monitor-exit v16
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_2

    :try_start_1d
    monitor-exit v18
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_1

    goto/16 :goto_1
.end method

.method public foldChild(Ljava/lang/String;)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 613
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->foldChild(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method protected foldChild(Ljava/lang/String;Z)Z
    .locals 8
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "withSynchronized"    # Z

    .prologue
    const/4 v6, 0x0

    .line 620
    const/4 v5, 0x0

    .line 622
    .local v5, "sync":Ljava/lang/Object;
    if-eqz p2, :cond_0

    .line 624
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItems:Ljava/util/ArrayList;

    .line 631
    .end local v5    # "sync":Ljava/lang/Object;
    :goto_0
    monitor-enter v5

    .line 633
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mPathItemMap:Ljava/util/HashMap;

    invoke-virtual {v7, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .line 635
    .local v3, "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    if-nez v3, :cond_1

    .line 637
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 675
    :goto_1
    return v6

    .line 628
    .end local v3    # "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    .restart local v5    # "sync":Ljava/lang/Object;
    :cond_0
    new-instance v5, Ljava/lang/Object;

    .end local v5    # "sync":Ljava/lang/Object;
    invoke-direct {v5}, Ljava/lang/Object;-><init>()V

    .restart local v5    # "sync":Ljava/lang/Object;
    goto :goto_0

    .line 640
    .end local v5    # "sync":Ljava/lang/Object;
    .restart local v3    # "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    :cond_1
    :try_start_1
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 642
    .local v2, "index":I
    iget v0, v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mDepth:I

    .line 644
    .local v0, "depth":I
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 646
    .local v1, "foldedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/TreeViewItem;>;"
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItems:Ljava/util/ArrayList;

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .line 648
    .local v4, "nextItem":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    :goto_2
    iget v6, v4, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mDepth:I

    if-le v6, v0, :cond_2

    .line 650
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 652
    add-int/lit8 v2, v2, 0x1

    .line 654
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v2, v6, :cond_2

    .line 656
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "nextItem":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    check-cast v4, Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .restart local v4    # "nextItem":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    goto :goto_2

    .line 664
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 666
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mFoldedItemMap:Ljava/util/HashMap;

    monitor-enter v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 668
    :try_start_2
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mFoldedItemMap:Ljava/util/HashMap;

    invoke-virtual {v6, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 669
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 671
    const/4 v6, 0x0

    :try_start_3
    iput-boolean v6, v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mIsExpanded:Z

    .line 673
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->notifyDataSetChanged()V

    .line 675
    const/4 v6, 0x1

    monitor-exit v5

    goto :goto_1

    .line 676
    .end local v0    # "depth":I
    .end local v1    # "foldedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/TreeViewItem;>;"
    .end local v2    # "index":I
    .end local v3    # "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    .end local v4    # "nextItem":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    :catchall_0
    move-exception v6

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v6

    .line 669
    .restart local v0    # "depth":I
    .restart local v1    # "foldedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/TreeViewItem;>;"
    .restart local v2    # "index":I
    .restart local v3    # "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    .restart local v4    # "nextItem":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    :catchall_1
    move-exception v6

    :try_start_4
    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 828
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 830
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;

    .line 832
    .local v1, "vh":Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;
    iget-object v2, v1, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mExpandIndicator:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .line 834
    .local v0, "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    new-instance v2, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$1;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$1;-><init>(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;Lcom/sec/android/app/myfiles/element/TreeViewItem;)V

    iput-object v2, v1, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mExpandIndicatorClickListener:Landroid/view/View$OnClickListener;

    .line 843
    iget-object v2, v1, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mExpandIndicator:Landroid/widget/ImageView;

    iget-object v3, v1, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mExpandIndicatorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 845
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->isDragMode()Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    if-nez v2, :cond_1

    :cond_0
    iget-object v2, v1, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    iget-boolean v2, v2, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mEnabled:Z

    if-nez v2, :cond_2

    .line 847
    :cond_1
    iget-object v2, v1, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 849
    iget-object v2, v1, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    iget v3, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mUnavailableItemTextColor:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 859
    :goto_0
    new-instance v2, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;-><init>(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;)V

    invoke-virtual {p2, v2}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 1097
    return-object p2

    .line 853
    :cond_2
    iget-object v2, v1, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    iget-object v3, v1, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    iget-boolean v3, v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mEnabled:Z

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 855
    iget-object v2, v1, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    iget v3, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItemOriginalTextColor:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public goTo(Ljava/lang/String;Z)Z
    .locals 11
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "addIfNotExist"    # Z

    .prologue
    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 234
    const/4 v5, 0x0

    .line 236
    .local v5, "result":Z
    const/4 v6, 0x0

    .line 238
    .local v6, "targetPath":Ljava/lang/StringBuilder;
    const/4 v4, 0x0

    .line 240
    .local v4, "pathToken":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 242
    .local v3, "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    iget-object v8, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v8

    .line 244
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .line 246
    .local v0, "existItem":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    iget v9, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    if-nez v9, :cond_0

    iget-object v9, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    invoke-virtual {v9, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 248
    move-object v3, v0

    .line 253
    .end local v0    # "existItem":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    :cond_1
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 255
    if-eqz v3, :cond_3

    .line 257
    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->changeCurrentItem(Lcom/sec/android/app/myfiles/element/TreeViewItem;)V

    .line 259
    const/4 v5, 0x1

    :goto_0
    move v7, v5

    .line 327
    :cond_2
    return v7

    .line 253
    .end local v2    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v7

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v7

    .line 263
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_3
    if-eqz p2, :cond_9

    .line 265
    new-instance v6, Ljava/lang/StringBuilder;

    .end local v6    # "targetPath":Ljava/lang/StringBuilder;
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 267
    .restart local v6    # "targetPath":Ljava/lang/StringBuilder;
    if-eqz p1, :cond_2

    .line 272
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_2

    .line 277
    invoke-virtual {p1, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 279
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v7, v4

    if-ge v1, v7, :cond_8

    .line 281
    const/16 v7, 0x2f

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 283
    aget-object v7, v4, v1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->isEmulatedEntryFolder(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 279
    :cond_4
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 290
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mPathItemMap:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    check-cast v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .line 292
    .restart local v3    # "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    if-eqz v3, :cond_7

    .line 294
    array-length v7, v4

    add-int/lit8 v7, v7, -0x1

    if-ge v1, v7, :cond_6

    .line 296
    iget-boolean v7, v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mIsExpanded:Z

    if-nez v7, :cond_4

    .line 302
    iget-object v7, v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    invoke-virtual {p0, v7, v10}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->expandChild(Ljava/lang/String;Z)Z

    move-result v7

    if-nez v7, :cond_4

    .line 304
    const/4 v5, 0x0

    goto :goto_2

    .line 310
    :cond_6
    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->changeCurrentItem(Lcom/sec/android/app/myfiles/element/TreeViewItem;)V

    goto :goto_2

    .line 315
    :cond_7
    const/4 v5, 0x0

    goto :goto_2

    .line 319
    :cond_8
    const/4 v5, 0x1

    goto :goto_0

    .line 323
    .end local v1    # "i":I
    :cond_9
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public onRefresh()V
    .locals 0

    .prologue
    .line 334
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->clearAll()V

    .line 342
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->prepareRoot()V

    .line 343
    return-void
.end method

.method prepareRootFolders(Ljava/lang/String;Ljava/lang/String;II)Lcom/sec/android/app/myfiles/element/TreeViewItem;
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "IconResId"    # I
    .param p4, "type"    # I

    .prologue
    const/4 v2, 0x0

    .line 200
    new-instance v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/element/TreeViewItem;-><init>()V

    .line 202
    .local v0, "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    iput p4, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    .line 204
    iput-object p1, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    .line 206
    iput-object p2, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mTitle:Ljava/lang/String;

    .line 208
    iput v2, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mDepth:I

    .line 210
    iput p3, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mIconResId:I

    .line 212
    iput p3, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mExpandedIconResId:I

    .line 214
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mHasChildFolder:Z

    .line 216
    iput-boolean v2, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mIsExpanded:Z

    .line 218
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 220
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mPathItemMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    return-object v0
.end method

.method public refresh(Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 2
    .param p1, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    .line 354
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v1

    .line 356
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 357
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 359
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mPathItemMap:Ljava/util/HashMap;

    monitor-enter v1

    .line 361
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mPathItemMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 362
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 364
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mFoldedItemMap:Ljava/util/HashMap;

    monitor-enter v1

    .line 366
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mFoldedItemMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 367
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 375
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->prepareRoot()V

    .line 376
    return-void

    .line 357
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 362
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 367
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0
.end method

.method public setCategoryItemListSelection(Ljava/lang/String;)I
    .locals 2
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 1101
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->getIndex(Ljava/lang/String;)I

    move-result v0

    .line 1102
    .local v0, "current":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1103
    const/4 v0, 0x0

    .line 1105
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setCurrentPosition(I)V

    .line 1106
    return v0
.end method

.method public setSortBy(II)V
    .locals 1
    .param p1, "sortBy"    # I
    .param p2, "inOrder"    # I

    .prologue
    .line 1112
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v0, :cond_0

    .line 1113
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setSortBy(II)V

    .line 1114
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mFoldedItemMap:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 1115
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mFoldedItemMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1117
    :cond_0
    return-void
.end method

.method public update(Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 0
    .param p1, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    .line 348
    return-void
.end method
