.class Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "CacheDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/db/CacheDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DatabaseHelper"
.end annotation


# static fields
.field private static volatile mInstance:Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2745
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;->mInstance:Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "factory"    # Landroid/database/sqlite/SQLiteDatabase$CursorFactory;
    .param p4, "version"    # I

    .prologue
    .line 2617
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 2619
    return-void
.end method

.method public static getInstance(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "factory"    # Landroid/database/sqlite/SQLiteDatabase$CursorFactory;
    .param p3, "version"    # I

    .prologue
    .line 2597
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;->mInstance:Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;

    if-nez v0, :cond_1

    .line 2599
    const-class v1, Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;

    monitor-enter v1

    .line 2601
    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;->mInstance:Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;

    if-nez v0, :cond_0

    .line 2603
    new-instance v0, Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    sput-object v0, Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;->mInstance:Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;

    .line 2607
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2611
    :cond_1
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;->mInstance:Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;

    return-object v0

    .line 2607
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 2625
    const-string v0, "CREATE TABLE IF NOT EXISTS cloud_dropbox_cursor ( _id INTEGER PRIMARY KEY AUTOINCREMENT, cursor  TEXT );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2630
    const-string v0, "CREATE TABLE IF NOT EXISTS cloud_dropbox ( _id INTEGER PRIMARY KEY AUTOINCREMENT,_data TEXT, title TEXT, mime_type TEXT, _size INTEGER, date_modified INTEGER, format INTEGER );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2644
    const-string v0, "CREATE TABLE IF NOT EXISTS NearyBy_deviceID ( _id INTEGER PRIMARY KEY AUTOINCREMENT,deviceID TEXT );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2652
    const-string v0, "CREATE TABLE IF NOT EXISTS NearyBy_contents ( _id INTEGER PRIMARY KEY AUTOINCREMENT,deviceID TEXT, title TEXT, _data TEXT, MediaType INTEGER, resolution TEXT, FileSize INTEGER, Extension TEXT, Date INTEGER, MimeType TEXT, ThumbUri TEXT );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2667
    const-string v0, "CREATE TABLE IF NOT EXISTS FTP_content(_id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, _data TEXT, extension TEXT, file_type INTEGER, is_directory INTEGER, file_size INTEGER, date INTEGER, cached INTEGER NOT NULL, FOREIGN KEY(cached) REFERENCES FTP_cache(id) UNIQUE (cached, title));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2681
    const-string v0, "CREATE TABLE IF NOT EXISTS FTP_cache(id INTEGER PRIMARY KEY AUTOINCREMENT, server TEXT NOT NULL, path TEXT NOT NULL, timestamp TEXT NOT NULL, UNIQUE (server, path));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2689
    const-string v0, "CREATE TABLE IF NOT EXISTS FTP_buffer(_id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, _data TEXT, extension TEXT, file_type INTEGER, is_directory INTEGER, file_size INTEGER, date INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2700
    const-string v0, "CREATE TABLE IF NOT EXISTS FTP_file_cache(_id INTEGER PRIMARY KEY AUTOINCREMENT, filename TEXT NOT NULL, cached INTEGER NOT NULL, date INTEGER NOT NULL, size INTEGER NOT NULL, FOREIGN KEY(filename) REFERENCES FTP_content(_data), FOREIGN KEY(cached) REFERENCES FTP_cache(id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2710
    const-string v0, "CREATE TABLE IF NOT EXISTS history_cache(_id INTEGER PRIMARY KEY AUTOINCREMENT, _name TEXT NOT NULL, _name_for_pinyin_sort TEXT NOT NULL, _type INTEGER NOT NULL, _category INTEGER NOT NULL, _date INTEGER NOT NULL, _size INTEGER, _data TEXT, _source TEXT, _extern_id INTEGER, _vendor TEXT, _version TEXT, _mime_type TEXT, _description TEXT, _status INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2728
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 2733
    const-string v0, "DROP TABLE IF EXISTS cloud_dropbox"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2734
    const-string v0, "DROP TABLE IF EXISTS cloud_dropbox_cursor"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2735
    const-string v0, "DROP TABLE IF EXISTS NearyBy_contents"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2736
    const-string v0, "DROP TABLE IF EXISTS NearyBy_deviceID"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2737
    const-string v0, "DROP TABLE IF EXISTS FTP_content"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2738
    const-string v0, "DROP TABLE IF EXISTS FTP_cache"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2739
    const-string v0, "DROP TABLE IF EXISTS FTP_buffer"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2740
    const-string v0, "DROP TABLE IF EXISTS FTP_file_cache"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2741
    const-string v0, "DROP TABLE IF EXISTS history_cache"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2742
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/db/CacheDB$DatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2743
    return-void
.end method
