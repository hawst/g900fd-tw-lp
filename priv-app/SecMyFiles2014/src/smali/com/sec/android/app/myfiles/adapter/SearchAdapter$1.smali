.class Lcom/sec/android/app/myfiles/adapter/SearchAdapter$1;
.super Ljava/lang/Object;
.source "SearchAdapter.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

.field final synthetic val$filePath:Ljava/lang/String;

.field final synthetic val$format:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 702
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    iput p2, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$1;->val$format:I

    iput-object p3, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$1;->val$filePath:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 13
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 707
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    .line 709
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 812
    :cond_0
    :goto_0
    const/4 v11, 0x1

    :goto_1
    return v11

    .line 712
    :pswitch_0
    iget-object v11, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    iget-boolean v11, v11, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mIsDragMode:Z

    if-nez v11, :cond_0

    iget v11, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$1;->val$format:I

    const/16 v12, 0x3001

    if-ne v11, v12, :cond_0

    .line 714
    iget-object v11, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mSelectedItemBackground:I
    invoke-static {v11}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)I

    move-result v11

    invoke-virtual {p1, v11}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 719
    :pswitch_1
    iget-object v11, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    iget-boolean v11, v11, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mIsDragMode:Z

    if-nez v11, :cond_0

    iget v11, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$1;->val$format:I

    const/16 v12, 0x3001

    if-ne v11, v12, :cond_0

    .line 721
    const/4 v11, 0x0

    invoke-virtual {p1, v11}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 726
    :pswitch_2
    iget-object v11, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    iget-boolean v11, v11, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mIsDragMode:Z

    if-nez v11, :cond_7

    .line 728
    const/4 v11, 0x0

    invoke-virtual {p1, v11}, Landroid/view/View;->setBackgroundColor(I)V

    .line 730
    iget-object v11, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    iget-object v11, v11, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mDropListener:Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;

    if-eqz v11, :cond_6

    .line 732
    invoke-virtual {p2}, Landroid/view/DragEvent;->getClipData()Landroid/content/ClipData;

    move-result-object v1

    .line 734
    .local v1, "clipData":Landroid/content/ClipData;
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v11

    if-eqz v11, :cond_5

    .line 736
    invoke-virtual {v1}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/ClipDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v7

    .line 738
    .local v7, "label":Ljava/lang/CharSequence;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_4

    const-string v11, "selectedUri"

    invoke-virtual {v11, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 740
    invoke-virtual {v1}, Landroid/content/ClipData;->getItemCount()I

    move-result v5

    .line 742
    .local v5, "itemCount":I
    const/4 v9, 0x0

    .line 744
    .local v9, "selectedFilePath":Ljava/lang/String;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 746
    .local v8, "selectedFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    if-ge v3, v5, :cond_2

    .line 748
    invoke-virtual {v1, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v6

    .line 750
    .local v6, "itemUri":Landroid/content/ClipData$Item;
    if-eqz v6, :cond_1

    .line 752
    invoke-virtual {v6}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v10

    .line 754
    .local v10, "uri":Landroid/net/Uri;
    iget-object v11, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Landroid/content/Context;

    move-result-object v11

    invoke-static {v11, v10}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getFilePathFromUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v9

    .line 756
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 758
    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 746
    .end local v10    # "uri":Landroid/net/Uri;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 763
    .end local v6    # "itemUri":Landroid/content/ClipData$Item;
    :cond_2
    const/4 v2, 0x0

    .line 765
    .local v2, "dstFolder":Ljava/lang/String;
    iget v11, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$1;->val$format:I

    const/16 v12, 0x3001

    if-ne v11, v12, :cond_3

    .line 767
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$1;->val$filePath:Ljava/lang/String;

    .line 774
    :goto_3
    iget-object v11, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    iget-object v11, v11, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mDropListener:Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;

    invoke-interface {v11, v2, v8}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;->onDrop(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 771
    :cond_3
    iget-object v11, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$1;->val$filePath:Ljava/lang/String;

    invoke-static {v11}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getParentFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 779
    .end local v2    # "dstFolder":Ljava/lang/String;
    .end local v3    # "i":I
    .end local v5    # "itemCount":I
    .end local v8    # "selectedFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v9    # "selectedFilePath":Ljava/lang/String;
    :cond_4
    const/4 v11, 0x0

    goto/16 :goto_1

    .line 785
    .end local v7    # "label":Ljava/lang/CharSequence;
    :cond_5
    const/4 v11, 0x0

    goto/16 :goto_1

    .line 791
    .end local v1    # "clipData":Landroid/content/ClipData;
    :cond_6
    const/4 v11, 0x0

    goto/16 :goto_1

    .line 795
    :cond_7
    new-instance v4, Landroid/content/Intent;

    const-string v11, "com.sec.android.action.FILE_OPERATION_DONE"

    invoke-direct {v4, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 797
    .local v4, "intent":Landroid/content/Intent;
    const-string v11, "success"

    const/4 v12, 0x0

    invoke-virtual {v4, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 799
    iget-object v11, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 805
    .end local v4    # "intent":Landroid/content/Intent;
    :pswitch_3
    iget-object v11, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    iget-boolean v11, v11, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mIsDragMode:Z

    if-eqz v11, :cond_0

    .line 807
    iget-object v11, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->finishDrag()V

    goto/16 :goto_0

    .line 709
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
