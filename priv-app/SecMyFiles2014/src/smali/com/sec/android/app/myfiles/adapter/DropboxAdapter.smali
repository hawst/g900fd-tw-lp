.class public Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
.source "DropboxAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$ConcreteProgressListener;,
        Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;
    }
.end annotation


# static fields
.field private static final MAX_THUMBNAIL_TASK_NUMBER:I = 0xa


# instance fields
.field private mContext:Landroid/content/Context;

.field private mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

.field private mSelectedItemBackground:I

.field private mThumbnailCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field mThumbnailWorkerTasks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;",
            ">;"
        }
    .end annotation
.end field

.field private navi:Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I
    .param p5, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    .line 80
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 71
    const/4 v2, 0x2

    const/16 v3, 0x55

    const/16 v4, 0x6a

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mSelectedItemBackground:I

    .line 74
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 501
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mThumbnailWorkerTasks:Ljava/util/ArrayList;

    .line 82
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mContext:Landroid/content/Context;

    .line 84
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v2

    const-wide/16 v4, 0x400

    div-long/2addr v2, v4

    long-to-int v1, v2

    .line 86
    .local v1, "maxMemory":I
    div-int/lit8 v0, v1, 0x8

    .line 88
    .local v0, "cacheSize":I
    new-instance v2, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$1;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$1;-><init>(Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;I)V

    iput-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mThumbnailCache:Landroid/util/LruCache;

    .line 98
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.feature.hovering_ui"

    invoke-static {v2, v3}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 100
    new-instance v2, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 102
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;)Landroid/util/LruCache;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mThumbnailCache:Landroid/util/LruCache;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mSelectedItemBackground:I

    return v0
.end method

.method private retrieveThumbnailFromFile(Ljava/lang/String;Landroid/widget/ImageView;)Z
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "displayImageView"    # Landroid/widget/ImageView;

    .prologue
    .line 508
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->getFileThumbnailFolderName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "large.jpg"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 509
    .local v0, "ThumbnailCacheFilePath":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 510
    .local v2, "thumbFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 511
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 512
    .local v1, "bitmapImage":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 513
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mThumbnailCache:Landroid/util/LruCache;

    invoke-virtual {v3, v0, v1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 514
    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 515
    const/4 v3, 0x1

    .line 518
    .end local v1    # "bitmapImage":Landroid/graphics/Bitmap;
    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 17
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 108
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;

    .line 110
    .local v10, "vh":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    invoke-super/range {p0 .. p3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 118
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    if-eqz v11, :cond_0

    .line 121
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 124
    :cond_0
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v11, :cond_1

    .line 126
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 127
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 130
    :cond_1
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 131
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 133
    const-string v11, "_data"

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 135
    .local v4, "filePath":Ljava/lang/String;
    const/16 v11, 0x2f

    invoke-virtual {v4, v11}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v4, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 137
    .local v3, "fileName":Ljava/lang/String;
    const-string v11, "format"

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 140
    .local v6, "format":I
    const/16 v11, 0x3001

    if-ne v6, v11, :cond_b

    .line 142
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v11}, Landroid/widget/TextView;->getVisibility()I

    move-result v11

    const/16 v12, 0x8

    if-ne v11, v12, :cond_2

    .line 143
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 144
    :cond_2
    const/4 v11, 0x0

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mHasThumbnail:Z

    .line 147
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mSelectModeFrom:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_3

    .line 149
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mSelectionType:I

    if-nez v11, :cond_a

    .line 151
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectRadioButton:Landroid/widget/RadioButton;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 160
    :cond_3
    :goto_0
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_4

    .line 162
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v11, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const v12, 0x7f0200af

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 168
    :cond_4
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v11, :cond_5

    .line 169
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 226
    :cond_5
    :goto_1
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mViewMode:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_7

    .line 231
    const/16 v11, 0x3001

    if-ne v6, v11, :cond_14

    .line 233
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 235
    .local v9, "sb":Ljava/lang/StringBuilder;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->navi:Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;

    .line 245
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0c0002

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->navi:Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;

    invoke-virtual {v13, v4}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->getItemsInFolder(Ljava/lang/String;)I

    move-result v13

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->navi:Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->getItemsInFolder(Ljava/lang/String;)I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v11, v12, v13, v14}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 254
    .end local v9    # "sb":Ljava/lang/StringBuilder;
    .local v2, "content":Ljava/lang/String;
    :goto_2
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    if-eqz v11, :cond_6

    .line 255
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    invoke-virtual {v11, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 259
    :cond_6
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mContext:Landroid/content/Context;

    const-string v12, "date_modified"

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/myfiles/utils/Utils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    .line 261
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    if-eqz v11, :cond_7

    .line 262
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    invoke-virtual {v11, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 266
    .end local v2    # "content":Ljava/lang/String;
    :cond_7
    const/16 v11, 0x3001

    if-eq v6, v11, :cond_9

    .line 268
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v5

    .line 271
    .local v5, "fileType":I
    new-instance v7, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$2;

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v5, v4}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$2;-><init>(Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;ILjava/lang/String;)V

    .line 350
    .local v7, "hoverListener":Landroid/view/View$OnHoverListener;
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mSelectModeFrom:I

    const/4 v12, 0x1

    if-eq v11, v12, :cond_9

    .line 353
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mViewMode:I

    if-eqz v11, :cond_8

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mViewMode:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_15

    .line 355
    :cond_8
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v11, v7}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 366
    .end local v5    # "fileType":I
    .end local v7    # "hoverListener":Landroid/view/View$OnHoverListener;
    :cond_9
    :goto_3
    new-instance v11, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;

    move-object/from16 v0, p0

    invoke-direct {v11, v0, v6, v4}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;-><init>(Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;ILjava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 499
    return-void

    .line 153
    :cond_a
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mSelectionType:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_3

    .line 155
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto/16 :goto_0

    .line 176
    :cond_b
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mSelectModeFrom:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_c

    .line 178
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mSelectionType:I

    if-nez v11, :cond_f

    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v11}, Landroid/widget/RadioButton;->getVisibility()I

    move-result v11

    const/16 v12, 0x8

    if-ne v11, v12, :cond_f

    .line 181
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectRadioButton:Landroid/widget/RadioButton;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 194
    :cond_c
    :goto_4
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_d

    .line 197
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowFileExtensionStatus()Z

    move-result v11

    if-eqz v11, :cond_12

    .line 199
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v11, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    :cond_d
    :goto_5
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v11, :cond_e

    .line 219
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 222
    :cond_e
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v4}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->loadThumbnail(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 183
    :cond_f
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mSelectionType:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_10

    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v11}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v11

    const/16 v12, 0x8

    if-ne v11, v12, :cond_10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mContext:Landroid/content/Context;

    invoke-static {v11}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v11

    if-nez v11, :cond_11

    :cond_10
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mSelectionType:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_c

    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v11}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v11

    const/16 v12, 0x8

    if-ne v11, v12, :cond_c

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mContext:Landroid/content/Context;

    invoke-static {v11}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentView()I

    move-result v11

    const/4 v12, 0x2

    if-ne v11, v12, :cond_c

    .line 190
    :cond_11
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_4

    .line 203
    :cond_12
    const/16 v11, 0x2e

    invoke-virtual {v3, v11}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v8

    .line 206
    .local v8, "lastDot":I
    if-lez v8, :cond_13

    .line 208
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const/4 v12, 0x0

    invoke-virtual {v3, v12, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 212
    :cond_13
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v11, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 251
    .end local v8    # "lastDot":I
    :cond_14
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mContext:Landroid/content/Context;

    const-string v12, "_size"

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    invoke-static {v11, v12, v13}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "content":Ljava/lang/String;
    goto/16 :goto_2

    .line 359
    .end local v2    # "content":Ljava/lang/String;
    .restart local v5    # "fileType":I
    .restart local v7    # "hoverListener":Landroid/view/View$OnHoverListener;
    :cond_15
    iget-object v11, v10, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v11, v7}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto/16 :goto_3
.end method

.method protected getContentType()I
    .locals 1

    .prologue
    .line 713
    const/16 v0, 0x8

    return v0
.end method

.method public getItemSize(Ljava/lang/String;)J
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 737
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 738
    .local v0, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 739
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/sec/android/app/myfiles/utils/Utils;->getPathDropboxFilesSize(Landroid/content/Context;Ljava/util/ArrayList;)J

    move-result-wide v2

    return-wide v2
.end method

.method protected getItemType(Ljava/lang/String;)I
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 729
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isDirectoryOfDropbox(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 730
    const/16 v0, 0x3001

    .line 732
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getThumbnailCache()Landroid/util/LruCache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 743
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mThumbnailCache:Landroid/util/LruCache;

    return-object v0
.end method

.method protected loadThumbnail(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Landroid/database/Cursor;)V
    .locals 8
    .param p1, "vh"    # Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v7, 0x1

    .line 526
    const-string v5, "_data"

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 527
    .local v2, "filePath":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-gtz v5, :cond_1

    .line 597
    :cond_0
    :goto_0
    return-void

    .line 530
    :cond_1
    invoke-static {v2}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v3

    .line 531
    .local v3, "fileType":I
    const/4 v1, 0x0

    .line 533
    .local v1, "displayImageView":Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->isVideoFileType(I)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 535
    :cond_2
    iget v5, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mViewMode:I

    if-eqz v5, :cond_3

    iget v5, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mViewMode:I

    if-ne v5, v7, :cond_4

    .line 536
    :cond_3
    iget-object v1, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    .line 541
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mThumbnailCache:Landroid/util/LruCache;

    invoke-virtual {v5, v2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 543
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_5

    .line 544
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 538
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_4
    iget-object v1, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    goto :goto_1

    .line 547
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_5
    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->retrieveThumbnailFromFile(Ljava/lang/String;Landroid/widget/ImageView;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 550
    const/4 v4, 0x0

    .line 552
    .local v4, "task":Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 553
    new-instance v4, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;

    .end local v4    # "task":Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;
    const/4 v5, 0x2

    invoke-direct {v4, p0, v1, v5, v2}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;-><init>(Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;Landroid/widget/ImageView;ILjava/lang/String;)V

    .line 557
    .restart local v4    # "task":Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;
    :goto_2
    iput-object v4, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnailWorker:Landroid/os/AsyncTask;

    .line 558
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mThumbnailWorkerTasks:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 560
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mThumbnailWorkerTasks:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/16 v6, 0xa

    if-ge v5, v6, :cond_0

    .line 561
    iput-boolean v7, v4, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->mStarted:Z

    .line 562
    sget-object v5, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v6, 0x0

    new-array v6, v6, [Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$IdPath;

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 555
    :cond_6
    new-instance v4, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;

    .end local v4    # "task":Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;
    const/4 v5, 0x3

    invoke-direct {v4, p0, v1, v5, v2}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;-><init>(Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;Landroid/widget/ImageView;ILjava/lang/String;)V

    .restart local v4    # "task":Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;
    goto :goto_2

    .line 595
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v4    # "task":Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$DropBoxThumbnailWorkerTask;
    :cond_7
    iget-object v5, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/MediaFile;->getSmallIcon(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public stopThumbnailsDownload()V
    .locals 1

    .prologue
    .line 504
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mThumbnailWorkerTasks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 505
    return-void
.end method
