.class public Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "TreeViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "ViewHolder"
.end annotation


# instance fields
.field public mContentsContainer:Landroid/widget/LinearLayout;

.field public mExpandIndicator:Landroid/widget/ImageView;

.field public mExpandIndicatorClickListener:Landroid/view/View$OnClickListener;

.field public mIcon:Landroid/widget/ImageView;

.field public mItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

.field public mPositionIndicator:Landroid/widget/ImageView;

.field public mTitle:Landroid/widget/TextView;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
