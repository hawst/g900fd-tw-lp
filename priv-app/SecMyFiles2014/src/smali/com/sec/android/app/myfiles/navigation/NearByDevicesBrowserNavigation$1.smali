.class Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$1;
.super Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;
.source "NearByDevicesBrowserNavigation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->AsyncOpen()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;Lcom/sec/android/app/myfiles/fragment/AbsFragment;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .prologue
    .line 275
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    invoke-direct {p0, p2}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    return-void
.end method


# virtual methods
.method public AsyncOpenExcute()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 279
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getCurrentItem()Lcom/samsung/android/allshare/Item;

    move-result-object v0

    .line 281
    .local v0, "currentItem":Lcom/samsung/android/allshare/Item;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->isFolder(Lcom/samsung/android/allshare/Item;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 283
    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$1;->setCompletedCondition(Z)V

    .line 285
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mRequests:Ljava/util/Map;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mRequests:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 286
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mRequests:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkConnectedNearByDevice(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 289
    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$1;->setCompletedCondition(Z)V

    .line 290
    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$1;->setmWifiDisconnect(Z)V

    .line 292
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    const/16 v2, 0x32

    invoke-virtual {v1, v0, v3, v2}, Lcom/samsung/android/allshare/media/Provider;->browse(Lcom/samsung/android/allshare/Item;II)V

    .line 300
    :goto_0
    return-void

    .line 296
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->popItem()V

    .line 297
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->openItem(Lcom/samsung/android/allshare/Item;)V

    .line 298
    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$1;->setCompletedCondition(Z)V

    goto :goto_0
.end method
