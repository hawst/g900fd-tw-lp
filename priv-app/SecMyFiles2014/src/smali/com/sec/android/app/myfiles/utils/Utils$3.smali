.class final Lcom/sec/android/app/myfiles/utils/Utils$3;
.super Ljava/lang/Object;
.source "Utils.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/utils/Utils;->getEditTextFilterDigit(Landroid/content/Context;I)[Landroid/text/InputFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$ctx:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 3000
    iput-object p1, p0, Lcom/sec/android/app/myfiles/utils/Utils$3;->val$ctx:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 8
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    .line 3005
    if-nez p2, :cond_0

    if-nez p3, :cond_0

    .line 3007
    const/4 v2, 0x0

    .line 3107
    :goto_0
    return-object v2

    .line 3010
    :cond_0
    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v2

    sub-int v3, p6, p5

    sub-int/2addr v2, v3

    rsub-int/lit8 v1, v2, 0x32

    .line 3012
    .local v1, "keep":I
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->stringSize:I
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$000()I

    move-result v2

    if-eqz v2, :cond_2

    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->stringSize:I
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$000()I

    move-result v2

    add-int/lit8 v2, v2, 0x32

    const/16 v3, 0xff

    if-lt v2, v3, :cond_2

    .line 3014
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->stringSize:I
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$000()I

    move-result v2

    rsub-int v2, v2, 0xff

    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v3

    sub-int v4, p6, p5

    sub-int/2addr v3, v4

    sub-int v1, v2, v3

    .line 3016
    if-gtz v1, :cond_2

    .line 3018
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v2

    if-nez v2, :cond_1

    .line 3020
    iget-object v2, p0, Lcom/sec/android/app/myfiles/utils/Utils$3;->val$ctx:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/utils/Utils$3;->val$ctx:Landroid/content/Context;

    const v4, 0x7f0b0077

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/16 v7, 0x32

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    # setter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->access$102(Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 3029
    :goto_1
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 3031
    const-string v2, ""

    goto :goto_0

    .line 3026
    :cond_1
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/utils/Utils$3;->val$ctx:Landroid/content/Context;

    const v4, 0x7f0b0077

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/16 v7, 0x32

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 3035
    :cond_2
    if-gtz v1, :cond_6

    .line 3037
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v2

    if-nez v2, :cond_5

    .line 3039
    iget-object v2, p0, Lcom/sec/android/app/myfiles/utils/Utils$3;->val$ctx:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/utils/Utils$3;->val$ctx:Landroid/content/Context;

    const v4, 0x7f0b00a1

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/16 v7, 0x32

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    # setter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->access$102(Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 3048
    :goto_2
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->tempEnd:I
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$200()I

    move-result v2

    if-ge v2, p3, :cond_3

    .line 3050
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 3053
    :cond_3
    # setter for: Lcom/sec/android/app/myfiles/utils/Utils;->tempEnd:I
    invoke-static {p3}, Lcom/sec/android/app/myfiles/utils/Utils;->access$202(I)I

    .line 3055
    const/4 v2, 0x1

    if-ne p3, v2, :cond_4

    .line 3057
    const/4 v2, -0x1

    # setter for: Lcom/sec/android/app/myfiles/utils/Utils;->tempEnd:I
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->access$202(I)I

    .line 3060
    :cond_4
    const-string v2, ""

    goto/16 :goto_0

    .line 3045
    :cond_5
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/utils/Utils$3;->val$ctx:Landroid/content/Context;

    const v4, 0x7f0b00a1

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/16 v7, 0x32

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 3062
    :cond_6
    sub-int v2, p3, p2

    if-lt v1, v2, :cond_7

    .line 3064
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3066
    :cond_7
    sub-int v2, p3, p2

    if-ge v1, v2, :cond_b

    .line 3070
    :try_start_0
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->stringSize:I
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$000()I

    move-result v2

    const/16 v3, 0xcd

    if-le v2, v3, :cond_9

    .line 3072
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v2

    if-nez v2, :cond_8

    .line 3074
    iget-object v2, p0, Lcom/sec/android/app/myfiles/utils/Utils$3;->val$ctx:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/utils/Utils$3;->val$ctx:Landroid/content/Context;

    const v4, 0x7f0b0077

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/16 v7, 0x32

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    # setter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->access$102(Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 3097
    :goto_3
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 3099
    add-int v2, p2, v1

    invoke-interface {p1, p2, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    goto/16 :goto_0

    .line 3080
    :cond_8
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/utils/Utils$3;->val$ctx:Landroid/content/Context;

    const v4, 0x7f0b0077

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/16 v7, 0x32

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 3101
    :catch_0
    move-exception v0

    .line 3103
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string v2, ""

    goto/16 :goto_0

    .line 3085
    .end local v0    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_9
    :try_start_1
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v2

    if-nez v2, :cond_a

    .line 3087
    iget-object v2, p0, Lcom/sec/android/app/myfiles/utils/Utils$3;->val$ctx:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/utils/Utils$3;->val$ctx:Landroid/content/Context;

    const v4, 0x7f0b00a1

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/16 v7, 0x32

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    # setter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->access$102(Landroid/widget/Toast;)Landroid/widget/Toast;

    goto :goto_3

    .line 3093
    :cond_a
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/utils/Utils$3;->val$ctx:Landroid/content/Context;

    const v4, 0x7f0b00a1

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/16 v7, 0x32

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 3107
    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_0
.end method
