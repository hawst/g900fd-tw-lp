.class public Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;
.super Ljava/lang/Object;
.source "SearchFragmentManager.java"


# static fields
.field private static final BAIDU_SEARCH_FRAGMENT_TAG:Ljava/lang/String; = "BaiduSearchFragment"

.field private static final DROPBOX_SEARCH_FRAGMENT_TAG:Ljava/lang/String; = "DropboxSearchFragment"

.field private static final NEARBY_DEVICES_SEARCH_FRAGMENT_TAG:Ljava/lang/String; = "NearbyDevicesSearchFragment"

.field public static final SEARCH_FRAGMENT_TAG:Ljava/lang/String; = "SearchFragment"


# instance fields
.field private FRAGMENT_LAYOUT_ID:I

.field private mContext:Landroid/content/Context;

.field private final mFragmentManager:Landroid/app/FragmentManager;


# direct methods
.method public constructor <init>(Landroid/app/FragmentManager;Landroid/content/Context;I)V
    .locals 0
    .param p1, "fm"    # Landroid/app/FragmentManager;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "LayoutId"    # I

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;->mFragmentManager:Landroid/app/FragmentManager;

    .line 36
    iput-object p2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;->mContext:Landroid/content/Context;

    .line 38
    iput p3, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;->FRAGMENT_LAYOUT_ID:I

    .line 40
    return-void
.end method

.method private getTag(I)Ljava/lang/String;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 88
    sparse-switch p1, :sswitch_data_0

    .line 101
    const-string v0, "SearchFragment"

    .line 105
    .local v0, "tag":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 90
    .end local v0    # "tag":Ljava/lang/String;
    :sswitch_0
    const-string v0, "NearbyDevicesSearchFragment"

    .line 91
    .restart local v0    # "tag":Ljava/lang/String;
    goto :goto_0

    .line 93
    .end local v0    # "tag":Ljava/lang/String;
    :sswitch_1
    const-string v0, "DropboxSearchFragment"

    .line 94
    .restart local v0    # "tag":Ljava/lang/String;
    goto :goto_0

    .line 97
    .end local v0    # "tag":Ljava/lang/String;
    :sswitch_2
    const-string v0, "BaiduSearchFragment"

    .line 98
    .restart local v0    # "tag":Ljava/lang/String;
    goto :goto_0

    .line 88
    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x20 -> :sswitch_2
    .end sparse-switch
.end method

.method private prepareBundle(IIILjava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 4
    .param p1, "id"    # I
    .param p2, "selectionType"    # I
    .param p3, "showFolderFileType"    # I
    .param p4, "startPath"    # Ljava/lang/String;
    .param p5, "arg"    # Landroid/os/Bundle;

    .prologue
    .line 110
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 111
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "selection_type"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 112
    const-string v2, "show_folder_file_type"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 113
    const-string v2, "id"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 114
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 115
    const-string v2, "FOLDERPATH"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_0
    if-eqz p5, :cond_1

    const-string v2, "search_option_type"

    invoke-virtual {p5, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "search_option_type"

    invoke-virtual {p5, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x6

    if-ne v2, v3, :cond_1

    .line 118
    if-eqz v0, :cond_1

    .line 119
    const-string v2, "search_option_type"

    const-string v3, "search_option_type"

    invoke-virtual {p5, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 120
    const-string v2, "search_keyword_intent_key"

    const-string v3, "search_keyword_intent_key"

    invoke-virtual {p5, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    :cond_1
    if-eqz p5, :cond_2

    .line 124
    const-string v2, "provider_id"

    invoke-virtual {p5, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 125
    .local v1, "providerId":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 126
    const-string v2, "FOLDERPATH"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    .end local v1    # "providerId":Ljava/lang/String;
    :cond_2
    return-object v0
.end method


# virtual methods
.method public RemoveFragment(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 151
    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    .line 153
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 155
    .local v0, "fragmentTransaction":Landroid/app/FragmentTransaction;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;->getTag(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 157
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;->getTag(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 159
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;->getTag(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 161
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 166
    .end local v0    # "fragmentTransaction":Landroid/app/FragmentTransaction;
    :cond_0
    return-void
.end method

.method public RemoveFragment(Ljava/lang/String;)V
    .locals 2
    .param p1, "Tag"    # Ljava/lang/String;

    .prologue
    .line 136
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 138
    .local v0, "fragmentTransaction":Landroid/app/FragmentTransaction;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v1, p1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 140
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v1, p1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 142
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v1, p1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 144
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 147
    :cond_0
    return-void
.end method

.method public setFragment(IIILjava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    .locals 5
    .param p1, "id"    # I
    .param p2, "selectionType"    # I
    .param p3, "showFolderFileType"    # I
    .param p4, "startPath"    # Ljava/lang/String;
    .param p5, "arg"    # Landroid/os/Bundle;

    .prologue
    .line 43
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 44
    .local v1, "ft":Landroid/app/FragmentTransaction;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;->getTag(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 45
    .local v0, "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    if-nez v0, :cond_2

    .line 46
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;->prepareBundle(IIILjava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v2

    .line 47
    .local v2, "preparedArg":Landroid/os/Bundle;
    sparse-switch p1, :sswitch_data_0

    .line 60
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    .end local v0    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;-><init>()V

    .line 63
    .restart local v0    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    :goto_0
    invoke-virtual {v0, v2}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 75
    .end local v2    # "preparedArg":Landroid/os/Bundle;
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    .line 76
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 77
    iget v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;->FRAGMENT_LAYOUT_ID:I

    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;->getTag(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v0, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 80
    :cond_1
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 82
    return-object v0

    .line 49
    .restart local v2    # "preparedArg":Landroid/os/Bundle;
    :sswitch_0
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    .end local v0    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;-><init>()V

    .line 50
    .restart local v0    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    goto :goto_0

    .line 52
    :sswitch_1
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/DropboxSearchFragment;

    .end local v0    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/DropboxSearchFragment;-><init>()V

    .line 53
    .restart local v0    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    goto :goto_0

    .line 56
    :sswitch_2
    new-instance v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    .end local v0    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    invoke-direct {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;-><init>()V

    .line 57
    .restart local v0    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    goto :goto_0

    .line 66
    .end local v2    # "preparedArg":Landroid/os/Bundle;
    :cond_2
    if-eqz p5, :cond_0

    const-string v3, "search_option_type"

    invoke-virtual {p5, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "search_option_type"

    invoke-virtual {p5, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x6

    if-ne v3, v4, :cond_0

    move-object v3, v0

    .line 71
    check-cast v3, Lcom/sec/android/app/myfiles/fragment/DropboxSearchFragment;

    const-string v4, "search_keyword_intent_key"

    invoke-virtual {p5, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/fragment/DropboxSearchFragment;->setSearchKeyword(Ljava/lang/String;)V

    goto :goto_1

    .line 47
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x20 -> :sswitch_2
    .end sparse-switch
.end method
