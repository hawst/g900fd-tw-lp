.class Lcom/sec/android/app/myfiles/MainActivity$4;
.super Landroid/content/BroadcastReceiver;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/MainActivity;->setSecretReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/MainActivity;)V
    .locals 0

    .prologue
    .line 1626
    iput-object p1, p0, Lcom/sec/android/app/myfiles/MainActivity$4;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private isPersonalDirectory(Landroid/net/Uri;)Z
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1629
    if-nez p1, :cond_0

    .line 1630
    const/4 v1, 0x0

    .line 1634
    :goto_0
    return v1

    .line 1633
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "file://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$4;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/MainActivity;->access$300(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/privatemode/PrivateModeManager;->getPrivateStorageDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1634
    .local v0, "personalPageUri":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1640
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$4;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mId:I

    if-nez v0, :cond_0

    .line 1641
    const-string v0, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1642
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/MainActivity$4;->isPersonalDirectory(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1643
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$4;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->StorageListRefresh()V

    .line 1655
    :cond_0
    :goto_0
    return-void

    .line 1645
    :cond_1
    const-string v0, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1646
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/MainActivity$4;->isPersonalDirectory(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1647
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$4;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->StorageListRefresh()V

    .line 1649
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$4;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->onRefresh()V

    goto :goto_0

    .line 1651
    :cond_2
    const-string v0, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1652
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$4;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->onRefresh()V

    goto :goto_0
.end method
