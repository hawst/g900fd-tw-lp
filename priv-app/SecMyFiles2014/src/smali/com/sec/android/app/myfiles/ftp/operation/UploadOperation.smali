.class public Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;
.super Landroid/os/AsyncTask;
.source "UploadOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/os/Bundle;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private volatile mAbort:Z

.field private final mContext:Landroid/content/Context;

.field private final mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

.field private final mDialogSync:Ljava/lang/Object;

.field private final mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

.field private final mMove:Z

.field private final mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

.field private final mSource:Ljava/lang/String;

.field private final mTarget:Ljava/lang/String;

.field private final mUploadTarget:Lcom/sec/android/app/myfiles/ftp/FTPParams;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/FTPParams;)V
    .locals 2
    .param p1, "arg"    # Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;
    .param p2, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "subtitle"    # Ljava/lang/String;
    .param p5, "targetDir"    # Ljava/lang/String;
    .param p6, "sourceDir"    # Ljava/lang/String;
    .param p7, "move"    # Z
    .param p8, "uploadTarget"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 192
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mAbort:Z

    .line 39
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mContext:Landroid/content/Context;

    .line 40
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getFTPConnectionHandler()Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    .line 41
    iput-object p2, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .line 42
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getDialogSync()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mDialogSync:Ljava/lang/Object;

    .line 44
    const-string v0, "/"

    invoke-virtual {p5, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mTarget:Ljava/lang/String;

    .line 49
    :goto_0
    const-string v0, "/"

    invoke-virtual {p6, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mSource:Ljava/lang/String;

    .line 54
    :goto_1
    iput-object p8, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mUploadTarget:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .line 55
    iput-boolean p7, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mMove:Z

    .line 56
    new-instance v0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->setTitle(Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    invoke-virtual {v0, p4}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->setSubtitle(Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    new-instance v1, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation$1;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->setAbortAction(Ljava/lang/Runnable;)V

    .line 67
    return-void

    .line 47
    :cond_0
    iput-object p5, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mTarget:Ljava/lang/String;

    goto :goto_0

    .line 52
    :cond_1
    const-string v0, "/"

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mSource:Ljava/lang/String;

    goto :goto_1
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mAbort:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;)Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;[Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;[Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;)Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    return-object v0
.end method

.method private getTarget(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "file"    # Ljava/lang/String;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mSource:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mTarget:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private setFilename(Ljava/lang/String;)V
    .locals 2
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation$4;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation$4;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 179
    :cond_0
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Landroid/os/Bundle;)Ljava/lang/Boolean;
    .locals 21
    .param p1, "args"    # [Landroid/os/Bundle;

    .prologue
    .line 71
    const/4 v15, 0x0

    .line 72
    .local v15, "targetHandler":Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mUploadTarget:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1

    .line 73
    invoke-static {}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->getInstance()Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mUploadTarget:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->getFTPConnectionHandler(Lcom/sec/android/app/myfiles/ftp/FTPParams;)Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-result-object v15

    .line 74
    if-eqz v15, :cond_0

    invoke-interface {v15}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncConnect()Z

    move-result v17

    if-nez v17, :cond_0

    .line 75
    const/4 v15, 0x0

    .line 77
    :cond_0
    if-eqz v15, :cond_1

    invoke-interface {v15}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncLogIn()Z

    move-result v17

    if-nez v17, :cond_1

    .line 78
    const/4 v15, 0x0

    .line 81
    :cond_1
    const/4 v14, 0x0

    .line 82
    .local v14, "success":Z
    if-eqz p1, :cond_a

    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v17, v0

    if-lez v17, :cond_a

    .line 83
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mDialogSync:Ljava/lang/Object;

    move-object/from16 v18, v0

    monitor-enter v18

    .line 84
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    check-cast v17, Landroid/app/Activity;

    invoke-virtual/range {v17 .. v17}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v17

    const-string v20, "fetching_dialog"

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 86
    const/16 v17, 0x0

    aget-object v3, p1, v17

    .line 87
    .local v3, "arg":Landroid/os/Bundle;
    const-string v17, "recur_disc_folders"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 88
    .local v10, "folders":[Ljava/lang/String;
    const-string v17, "recur_disc_files"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 89
    .local v8, "files":[Ljava/lang/String;
    if-nez v8, :cond_2

    const/16 v16, 0x0

    .line 90
    .local v16, "total":I
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mAbort:Z

    move/from16 v17, v0

    if-nez v17, :cond_4

    if-eqz v10, :cond_4

    array-length v0, v10

    move/from16 v17, v0

    if-lez v17, :cond_4

    .line 91
    move-object v4, v10

    .local v4, "arr$":[Ljava/lang/String;
    array-length v13, v4

    .local v13, "len$":I
    const/4 v12, 0x0

    .local v12, "i$":I
    :goto_1
    if-ge v12, v13, :cond_4

    aget-object v9, v4, v12

    .line 92
    .local v9, "folder":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->setFilename(Ljava/lang/String;)V

    .line 93
    if-nez v15, :cond_3

    .line 94
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v9}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncCreateFolder(Ljava/lang/String;)Z

    move-result v14

    .line 91
    :goto_2
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 89
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v9    # "folder":Ljava/lang/String;
    .end local v12    # "i$":I
    .end local v13    # "len$":I
    .end local v16    # "total":I
    :cond_2
    array-length v0, v8

    move/from16 v16, v0

    goto :goto_0

    .line 96
    .restart local v4    # "arr$":[Ljava/lang/String;
    .restart local v9    # "folder":Ljava/lang/String;
    .restart local v12    # "i$":I
    .restart local v13    # "len$":I
    .restart local v16    # "total":I
    :cond_3
    invoke-interface {v15, v9}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncCreateFolder(Ljava/lang/String;)Z

    goto :goto_2

    .line 142
    .end local v3    # "arg":Landroid/os/Bundle;
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v8    # "files":[Ljava/lang/String;
    .end local v9    # "folder":Ljava/lang/String;
    .end local v10    # "folders":[Ljava/lang/String;
    .end local v12    # "i$":I
    .end local v13    # "len$":I
    .end local v16    # "total":I
    :catchall_0
    move-exception v17

    monitor-exit v18
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v17

    .line 101
    .restart local v3    # "arg":Landroid/os/Bundle;
    .restart local v8    # "files":[Ljava/lang/String;
    .restart local v10    # "folders":[Ljava/lang/String;
    .restart local v16    # "total":I
    :cond_4
    if-eqz v8, :cond_5

    :try_start_1
    array-length v0, v8

    move/from16 v17, v0

    if-lez v17, :cond_5

    .line 102
    const/4 v11, 0x0

    .line 103
    .local v11, "i":I
    move-object v4, v8

    .restart local v4    # "arr$":[Ljava/lang/String;
    array-length v13, v4

    .restart local v13    # "len$":I
    const/4 v12, 0x0

    .restart local v12    # "i$":I
    :goto_3
    if-ge v12, v13, :cond_5

    aget-object v6, v4, v12

    .line 104
    .local v6, "file":Ljava/lang/String;
    add-int/lit8 v11, v11, 0x1

    move v5, v11

    .line 105
    .local v5, "current":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mAbort:Z

    move/from16 v17, v0

    if-eqz v17, :cond_6

    .line 127
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v5    # "current":I
    .end local v6    # "file":Ljava/lang/String;
    .end local v11    # "i":I
    .end local v12    # "i$":I
    .end local v13    # "len$":I
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mMove:Z

    move/from16 v17, v0

    if-eqz v17, :cond_9

    if-eqz v14, :cond_9

    .line 128
    if-eqz v8, :cond_8

    .line 129
    move-object v4, v8

    .restart local v4    # "arr$":[Ljava/lang/String;
    array-length v13, v4

    .restart local v13    # "len$":I
    const/4 v12, 0x0

    .restart local v12    # "i$":I
    :goto_4
    if-ge v12, v13, :cond_8

    aget-object v7, v4, v12

    .line 130
    .local v7, "filePath":Ljava/lang/String;
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 131
    .local v6, "file":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 129
    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    .line 108
    .end local v7    # "filePath":Ljava/lang/String;
    .restart local v5    # "current":I
    .local v6, "file":Ljava/lang/String;
    .restart local v11    # "i":I
    :cond_6
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->setFilename(Ljava/lang/String;)V

    .line 109
    if-nez v15, :cond_7

    .line 110
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->getTarget(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    new-instance v20, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation$2;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move/from16 v2, v16

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation$2;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;II)V

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-interface {v0, v6, v1, v2}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncUploadFileFromLocal(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/openoperation/IProgressUpdatable;)Z

    move-result v14

    .line 103
    :goto_5
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    .line 117
    :cond_7
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->getTarget(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    new-instance v19, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation$3;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move/from16 v2, v16

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation$3;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;II)V

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-interface {v15, v6, v0, v1}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncUploadFileFromLocal(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/openoperation/IProgressUpdatable;)Z

    move-result v14

    goto :goto_5

    .line 134
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v5    # "current":I
    .end local v6    # "file":Ljava/lang/String;
    .end local v11    # "i":I
    .end local v12    # "i$":I
    .end local v13    # "len$":I
    :cond_8
    if-eqz v10, :cond_9

    .line 135
    array-length v0, v10

    move/from16 v17, v0

    add-int/lit8 v11, v17, -0x1

    .restart local v11    # "i":I
    :goto_6
    if-ltz v11, :cond_9

    .line 136
    new-instance v9, Ljava/io/File;

    aget-object v17, v10, v11

    move-object/from16 v0, v17

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 137
    .local v9, "folder":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 135
    add-int/lit8 v11, v11, -0x1

    goto :goto_6

    .line 141
    .end local v9    # "folder":Ljava/io/File;
    .end local v11    # "i":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->dismissAllowingStateLoss()V

    .line 142
    monitor-exit v18
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 144
    .end local v3    # "arg":Landroid/os/Bundle;
    .end local v8    # "files":[Ljava/lang/String;
    .end local v10    # "folders":[Ljava/lang/String;
    .end local v16    # "total":I
    :cond_a
    invoke-static {v14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v17

    return-object v17
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 37
    check-cast p1, [Landroid/os/Bundle;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->doInBackground([Landroid/os/Bundle;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mMove:Z

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mSource:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;->onUploadCompleted(ZLjava/lang/String;)V

    .line 158
    :goto_0
    return-void

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;->onUploadCompleted(ZLjava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 37
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 4
    .param p1, "values"    # [Ljava/lang/Integer;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 162
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 163
    array-length v0, p1

    if-ne v0, v3, :cond_1

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    aget-object v1, p1, v2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->updateProgress(I)V

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    array-length v0, p1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    aget-object v1, p1, v2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aget-object v2, p1, v3

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x2

    aget-object v3, p1, v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->updateProgress(III)V

    goto :goto_0
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 37
    check-cast p1, [Ljava/lang/Integer;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
