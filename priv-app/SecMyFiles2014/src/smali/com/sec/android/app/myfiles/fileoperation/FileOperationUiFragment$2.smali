.class Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$2;
.super Ljava/lang/Object;
.source "FileOperationUiFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWaitOnCancel:Z
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->access$000(Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->cancel()V

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 178
    :cond_0
    return-void
.end method
