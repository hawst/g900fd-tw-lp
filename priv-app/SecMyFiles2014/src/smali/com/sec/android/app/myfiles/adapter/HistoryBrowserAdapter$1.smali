.class Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;
.super Ljava/lang/Object;
.source "HistoryBrowserAdapter.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

.field final synthetic val$filePath:Ljava/lang/String;

.field final synthetic val$filePosition:I

.field final synthetic val$fileType:I

.field final synthetic val$hoverFilePath:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;ILjava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    iput p2, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->val$fileType:I

    iput-object p3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->val$hoverFilePath:Ljava/lang/String;

    iput p4, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->val$filePosition:I

    iput-object p5, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->val$filePath:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    const/16 v10, 0x201

    const/4 v9, 0x2

    const/4 v5, 0x0

    .line 171
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->isSelectMode()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3, p2}, Lcom/sec/android/app/myfiles/utils/Utils;->isHoverSettingDisabled(Landroid/content/Context;Landroid/view/MotionEvent;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->access$200(Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 174
    const/4 v0, 0x0

    .line 176
    .local v0, "bSupportHover":Z
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 178
    .local v2, "hoverRect":Landroid/graphics/Rect;
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    check-cast v3, Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 180
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    iget v7, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v8, v3

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    iget v3, v3, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mViewMode:I

    if-ne v3, v9, :cond_1

    move v3, v4

    :goto_0
    invoke-virtual {v6, v7, v8, v3}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->setHoverLeft(IIZ)I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 182
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    iget v4, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v3, v4, v6}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->setHoverTop(II)I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 184
    iget v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->val$fileType:I

    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->isVideoFileType(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 186
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isOnCall(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 188
    const/4 v0, 0x0

    .line 235
    :goto_1
    if-eqz v0, :cond_0

    .line 237
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 284
    .end local v0    # "bSupportHover":Z
    .end local v2    # "hoverRect":Landroid/graphics/Rect;
    :cond_0
    :goto_2
    :pswitch_0
    return v5

    .restart local v0    # "bSupportHover":Z
    .restart local v2    # "hoverRect":Landroid/graphics/Rect;
    :cond_1
    move v3, v5

    .line 180
    goto :goto_0

    .line 192
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 194
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->val$hoverFilePath:Ljava/lang/String;

    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->val$filePosition:I

    invoke-virtual {v3, v10, v2, v4, v6}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(ILandroid/graphics/Rect;Ljava/lang/String;I)V

    .line 196
    const/4 v0, 0x1

    goto :goto_1

    .line 199
    :cond_3
    iget v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->val$fileType:I

    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->isAudioFileType(I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 201
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->access$200(Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 203
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->access$200(Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->val$hoverFilePath:Ljava/lang/String;

    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->val$filePosition:I

    invoke-virtual {v3, v10, v2, v4, v6}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(ILandroid/graphics/Rect;Ljava/lang/String;I)V

    .line 205
    const/4 v0, 0x1

    goto :goto_1

    .line 207
    :cond_4
    iget v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->val$fileType:I

    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 209
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 211
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->val$hoverFilePath:Ljava/lang/String;

    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->val$filePosition:I

    invoke-virtual {v3, v10, v2, v4, v6}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(ILandroid/graphics/Rect;Ljava/lang/String;I)V

    .line 213
    const/4 v0, 0x1

    goto :goto_1

    .line 215
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    iget v3, v3, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mViewMode:I

    if-ne v3, v9, :cond_8

    iget v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->val$fileType:I

    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->isDocumentFileType(I)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 217
    iget v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->val$fileType:I

    const/16 v4, 0x4c

    if-eq v3, v4, :cond_6

    iget v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->val$fileType:I

    const/16 v4, 0x4b

    if-ne v3, v4, :cond_7

    .line 219
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 222
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->access$300(Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 224
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->access$300(Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v3

    const/4 v4, 0x5

    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->val$filePath:Ljava/lang/String;

    iget v7, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->val$filePosition:I

    invoke-virtual {v3, v4, v2, v6, v7}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(ILandroid/graphics/Rect;Ljava/lang/String;I)V

    .line 226
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 231
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 242
    :pswitch_1
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v3

    if-ne v3, v9, :cond_9

    .line 244
    const/16 v3, 0xa

    const/4 v4, -0x1

    invoke-static {v3, v4}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 252
    :cond_9
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    const/16 v4, 0x1f4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->sendDelayedMessage(I)V

    goto/16 :goto_2

    .line 247
    :catch_0
    move-exception v1

    .line 249
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3

    .line 259
    .end local v1    # "e":Landroid/os/RemoteException;
    :pswitch_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->removeDelayedMessage()V

    .line 262
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v3

    if-ne v3, v9, :cond_0

    .line 264
    const/4 v3, 0x1

    const/4 v4, -0x1

    invoke-static {v3, v4}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_2

    .line 266
    :catch_1
    move-exception v1

    .line 268
    .restart local v1    # "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_2

    .line 237
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
