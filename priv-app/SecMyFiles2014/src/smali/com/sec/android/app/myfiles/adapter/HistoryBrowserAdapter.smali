.class public Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
.source "HistoryBrowserAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$2;
    }
.end annotation


# static fields
.field private static final MODULE:Ljava/lang/String;

.field public static final VIEW_CATEGORY:Ljava/lang/String; = "category"

.field public static final VIEW_DATA:Ljava/lang/String; = "data"

.field public static final VIEW_EXTERNAL_ID:Ljava/lang/String; = "external_id"

.field public static final VIEW_NAME:Ljava/lang/String; = "name"

.field public static final VIEW_SOURCE:Ljava/lang/String; = "source"


# instance fields
.field private mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

.field private mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

.field public mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

.field private mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private final mSds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

.field private mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 442
    const-class v0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->MODULE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I
    .param p5, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    const/4 v0, 0x0

    .line 70
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 56
    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 58
    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 60
    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 62
    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 64
    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 72
    iput-object p3, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mCursor:Landroid/database/Cursor;

    .line 74
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 76
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mSds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-static {v0, v1}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    new-instance v0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 82
    new-instance v0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 84
    new-instance v0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 86
    new-instance v0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 88
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    return-object v0
.end method

.method private getTitle(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 343
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mSds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-static {v0, p1}, Lcom/sec/android/app/myfiles/utils/Utils;->getFileTitleWithRespectToShowExtensionSettings(Lcom/sec/android/app/myfiles/utils/SharedDataStore;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getTypeText(Lcom/sec/android/app/myfiles/element/HistoryItem;)Ljava/lang/String;
    .locals 2
    .param p1, "item"    # Lcom/sec/android/app/myfiles/element/HistoryItem;

    .prologue
    .line 349
    sget-object v0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$2;->$SwitchMap$com$sec$android$app$myfiles$element$HistoryItem$Category:[I

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getCategory()Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 353
    const-string v0, ""

    :goto_0
    return-object v0

    .line 351
    :pswitch_0
    const-string v0, "APP"

    goto :goto_0

    .line 352
    :pswitch_1
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getData()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/myfiles/MediaFile;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 349
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setViewIcon(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Lcom/sec/android/app/myfiles/element/HistoryItem;)V
    .locals 12
    .param p1, "vh"    # Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    .param p2, "item"    # Lcom/sec/android/app/myfiles/element/HistoryItem;

    .prologue
    .line 379
    sget-object v6, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$2;->$SwitchMap$com$sec$android$app$myfiles$element$HistoryItem$Category:[I

    invoke-virtual {p2}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getCategory()Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 424
    :cond_0
    :goto_0
    return-void

    .line 383
    :pswitch_0
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {p2}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getData()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 384
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v6, :cond_0

    .line 385
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v6, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 387
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :catch_0
    move-exception v2

    .line 388
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    sget-object v7, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->MODULE:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "NameNotFoundException: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p2}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getDate()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 395
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :pswitch_1
    const/4 v0, 0x1

    .line 396
    .local v0, "bExist":Z
    invoke-virtual {p2}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getData()Ljava/lang/String;

    move-result-object v5

    .line 398
    .local v5, "path":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 400
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 402
    .local v3, "file":Ljava/io/File;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_1

    .line 403
    const/4 v0, 0x0

    .line 408
    :cond_1
    if-nez v0, :cond_2

    .line 410
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5, v6}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;Landroid/content/Context;)I

    move-result v4

    .line 411
    .local v4, "fileType":I
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v6, :cond_0

    .line 412
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5, v7, v4}, Lcom/sec/android/app/myfiles/MediaFile;->getSmallIcon(Ljava/lang/String;Landroid/content/Context;I)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 417
    .end local v4    # "fileType":I
    :cond_2
    invoke-virtual {p2}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getData()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, p1, v6}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->loadThumbnail(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Ljava/lang/String;)V

    goto :goto_0

    .line 379
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 16
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 93
    invoke-super/range {p0 .. p3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 95
    invoke-static/range {p3 .. p3}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getFromCacheCursor(Landroid/database/Cursor;)Lcom/sec/android/app/myfiles/element/HistoryItem;

    move-result-object v10

    .line 97
    .local v10, "item":Lcom/sec/android/app/myfiles/element/HistoryItem;
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;

    .line 99
    .local v11, "vh":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 100
    .local v8, "data":Landroid/os/Bundle;
    const-string v3, "data"

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getData()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v3, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const-string v3, "source"

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getSource()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v3, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const-string v3, "external_id"

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getExternalId()J

    move-result-wide v12

    invoke-virtual {v8, v3, v12, v13}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 103
    const-string v3, "name"

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v3, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const-string v3, "category"

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getCategory()Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;->getId()I

    move-result v12

    invoke-virtual {v8, v3, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 105
    iput-object v8, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mData:Landroid/os/Bundle;

    .line 107
    iget-object v3, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v3, :cond_0

    .line 109
    iget-object v3, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/4 v12, 0x0

    invoke-virtual {v3, v12}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 110
    iget-object v3, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/4 v12, 0x0

    invoke-virtual {v3, v12}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 113
    :cond_0
    iget-object v3, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    if-eqz v3, :cond_1

    .line 114
    iget-object v3, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    const/4 v12, 0x0

    invoke-virtual {v3, v12}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 116
    :cond_1
    iget-object v3, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v3, :cond_2

    .line 117
    iget-object v3, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/4 v12, 0x0

    invoke-virtual {v3, v12}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 120
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mViewMode:I

    packed-switch v3, :pswitch_data_0

    .line 138
    :goto_0
    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getData()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxFolder(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 140
    iget-object v3, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mPersonalContentsIcon:Landroid/widget/ImageView;

    const/4 v12, 0x0

    invoke-virtual {v3, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 146
    :goto_1
    iget-object v3, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDivider:Landroid/widget/ImageView;

    if-eqz v3, :cond_3

    .line 148
    iget-object v3, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDivider:Landroid/widget/ImageView;

    const/16 v12, 0x8

    invoke-virtual {v3, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 152
    :cond_3
    const-string v3, "_data"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 154
    .local v7, "filePath":Ljava/lang/String;
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 156
    .local v9, "file":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_6

    .line 318
    :cond_4
    :goto_2
    return-void

    .line 123
    .end local v7    # "filePath":Ljava/lang/String;
    .end local v9    # "file":Ljava/io/File;
    :pswitch_0
    iget-object v3, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getDate()J

    move-result-wide v12

    const-wide/16 v14, 0x3e8

    mul-long/2addr v12, v14

    move-object/from16 v0, p2

    invoke-static {v0, v12, v13}, Lcom/sec/android/app/myfiles/utils/Utils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v3, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    move-object/from16 v0, p2

    invoke-static {v0, v10}, Lcom/sec/android/app/myfiles/utils/Utils;->getFileSizeFormat(Landroid/content/Context;Lcom/sec/android/app/myfiles/element/HistoryItem;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v3, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDescriptionViewGroup:Landroid/view/ViewGroup;

    const/4 v12, 0x0

    invoke-virtual {v3, v12}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 126
    iget-object v3, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDescriptionTextView:Landroid/widget/TextView;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getDescription()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    :pswitch_1
    iget-object v3, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getName()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->getTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    move-object/from16 v0, p0

    invoke-direct {v0, v11, v10}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->setViewIcon(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Lcom/sec/android/app/myfiles/element/HistoryItem;)V

    goto :goto_0

    .line 132
    :pswitch_2
    iget-object v3, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getName()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->getTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    move-object/from16 v0, p0

    invoke-direct {v0, v11, v10}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->setViewIcon(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Lcom/sec/android/app/myfiles/element/HistoryItem;)V

    goto/16 :goto_0

    .line 143
    :cond_5
    iget-object v3, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mPersonalContentsIcon:Landroid/widget/ImageView;

    const/16 v12, 0x8

    invoke-virtual {v3, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 160
    .restart local v7    # "filePath":Ljava/lang/String;
    .restart local v9    # "file":Ljava/io/File;
    :cond_6
    invoke-static {v7}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v4

    .line 162
    .local v4, "fileType":I
    move-object v5, v7

    .line 164
    .local v5, "hoverFilePath":Ljava/lang/String;
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    .line 166
    .local v6, "filePosition":I
    new-instance v2, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter$1;-><init>(Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;ILjava/lang/String;ILjava/lang/String;)V

    .line 288
    .local v2, "hoverListener":Landroid/view/View$OnHoverListener;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mContext:Landroid/content/Context;

    const-string v12, "com.sec.feature.hovering_ui"

    invoke-static {v3, v12}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 290
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mSelectModeFrom:I

    const/4 v12, 0x2

    if-eq v3, v12, :cond_4

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mSelectModeFrom:I

    const/4 v12, 0x1

    if-eq v3, v12, :cond_4

    .line 293
    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->isInternalRootFolder(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->isExternalRootFolder(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->isUsbStorageRootFolder(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 296
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mViewMode:I

    if-eqz v3, :cond_7

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->mViewMode:I

    const/4 v12, 0x1

    if-ne v3, v12, :cond_8

    .line 298
    :cond_7
    iget-object v3, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto/16 :goto_2

    .line 302
    :cond_8
    iget-object v3, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto/16 :goto_2

    .line 308
    :cond_9
    iget-object v3, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v3, :cond_a

    .line 309
    iget-object v3, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/4 v12, 0x0

    invoke-virtual {v3, v12}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 311
    :cond_a
    iget-object v3, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v3, :cond_4

    .line 312
    iget-object v3, v11, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/4 v12, 0x0

    invoke-virtual {v3, v12}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto/16 :goto_2

    .line 120
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected getContentType()I
    .locals 1

    .prologue
    .line 429
    const/16 v0, 0x28

    return v0
.end method

.method protected getIconResourceForListView(Ljava/lang/String;Landroid/content/Context;I)I
    .locals 2
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "fileType"    # I

    .prologue
    .line 360
    invoke-static {p1, p2, p3}, Lcom/sec/android/app/myfiles/MediaFile;->getSmallIcon(Ljava/lang/String;Landroid/content/Context;I)I

    move-result v0

    .line 361
    .local v0, "imageResource":I
    const v1, 0x7f0200ad

    if-ne v0, v1, :cond_0

    .line 362
    const v0, 0x7f02009d

    .line 364
    .end local v0    # "imageResource":I
    :cond_0
    return v0
.end method

.method protected getIconResourceForThumbnailView(Ljava/lang/String;Landroid/content/Context;I)I
    .locals 2
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "fileType"    # I

    .prologue
    .line 369
    invoke-static {p1, p2, p3}, Lcom/sec/android/app/myfiles/MediaFile;->getLargeIcon(Ljava/lang/String;Landroid/content/Context;I)I

    move-result v0

    .line 370
    .local v0, "imageResource":I
    const v1, 0x7f0200ae

    if-ne v0, v1, :cond_0

    .line 371
    const v0, 0x7f02009d

    .line 373
    .end local v0    # "imageResource":I
    :cond_0
    return v0
.end method

.method public getSelectedHistoryItems()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/element/HistoryItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 322
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->getSelectedItemPosition()Ljava/util/ArrayList;

    move-result-object v3

    .line 324
    .local v3, "selectedItemsPositions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 326
    .local v2, "selectedItems":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/app/myfiles/element/HistoryItem;>;"
    if-eqz v3, :cond_0

    .line 328
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 330
    .local v1, "position":I
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/database/Cursor;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getFromCacheCursor(Landroid/database/Cursor;)Lcom/sec/android/app/myfiles/element/HistoryItem;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 337
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "position":I
    :cond_0
    return-object v2
.end method
