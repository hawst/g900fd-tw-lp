.class Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$27;
.super Ljava/lang/Object;
.source "AbsBrowserFragment.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V
    .locals 0

    .prologue
    .line 5856
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 13
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    const/4 v11, 0x0

    .line 5861
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    .line 5863
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 6001
    :cond_0
    :goto_0
    const/4 v10, 0x1

    :goto_1
    return v10

    .line 5867
    :pswitch_0
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-boolean v10, v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mIsDragMode:Z

    if-nez v10, :cond_0

    goto :goto_0

    .line 5877
    :pswitch_1
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-boolean v10, v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mIsDragMode:Z

    if-nez v10, :cond_0

    goto :goto_0

    .line 5887
    :pswitch_2
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-boolean v10, v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mIsDragMode:Z

    if-nez v10, :cond_c

    .line 5891
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget v10, v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v12, 0x201

    if-eq v10, v12, :cond_1

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget v10, v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v12, 0x8

    if-eq v10, v12, :cond_1

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget v10, v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v12, 0x1f

    if-eq v10, v12, :cond_1

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget v10, v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v12, 0xa

    if-ne v10, v12, :cond_b

    .line 5895
    :cond_1
    invoke-virtual {p2}, Landroid/view/DragEvent;->getClipData()Landroid/content/ClipData;

    move-result-object v1

    .line 5897
    .local v1, "clipData":Landroid/content/ClipData;
    if-eqz v1, :cond_a

    invoke-virtual {v1}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v10

    if-eqz v10, :cond_a

    .line 5899
    invoke-virtual {v1}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/ClipDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v6

    .line 5901
    .local v6, "label":Ljava/lang/CharSequence;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_9

    const-string v10, "selectedUri"

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v12, "galleryURI"

    move-object v10, v6

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v12, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, "selectedFTPUri"

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, "selectedCloudUri"

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 5905
    :cond_2
    invoke-virtual {v1}, Landroid/content/ClipData;->getItemCount()I

    move-result v4

    .line 5907
    .local v4, "itemCount":I
    const/4 v8, 0x0

    .line 5909
    .local v8, "selectedFilePath":Ljava/lang/String;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 5911
    .local v7, "selectedFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-ge v2, v4, :cond_6

    .line 5913
    invoke-virtual {v1, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v5

    .line 5915
    .local v5, "itemUri":Landroid/content/ClipData$Item;
    if-eqz v5, :cond_3

    .line 5917
    invoke-virtual {v5}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v9

    .line 5919
    .local v9, "uri":Landroid/net/Uri;
    if-eqz v9, :cond_5

    .line 5921
    const-string v10, "selectedFTPUri"

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 5923
    invoke-static {v9}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getItemPathFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v8

    .line 5937
    :goto_3
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 5939
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5911
    .end local v9    # "uri":Landroid/net/Uri;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 5927
    .restart local v9    # "uri":Landroid/net/Uri;
    :cond_4
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v10, v9}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getFilePathFromUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v8

    goto :goto_3

    .line 5933
    :cond_5
    invoke-virtual {v5}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_3

    .line 5944
    .end local v5    # "itemUri":Landroid/content/ClipData$Item;
    .end local v9    # "uri":Landroid/net/Uri;
    :cond_6
    const-string v10, "selectedCloudUri"

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 5946
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getmPathForDropboxMultiDrag()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_7

    .line 5947
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v11, v11, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getmPathForDropboxMultiDrag()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11, v7}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onDrop(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 5949
    :cond_7
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v11, v11, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11, v7}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onDrop(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 5954
    :cond_8
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v11, v11, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11, v7}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onDrop(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .end local v2    # "i":I
    .end local v4    # "itemCount":I
    .end local v7    # "selectedFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8    # "selectedFilePath":Ljava/lang/String;
    :cond_9
    move v10, v11

    .line 5961
    goto/16 :goto_1

    .end local v6    # "label":Ljava/lang/CharSequence;
    :cond_a
    move v10, v11

    .line 5967
    goto/16 :goto_1

    .end local v1    # "clipData":Landroid/content/ClipData;
    :cond_b
    move v10, v11

    .line 5973
    goto/16 :goto_1

    .line 5978
    :cond_c
    new-instance v3, Landroid/content/Intent;

    const-string v10, "com.sec.android.action.FILE_OPERATION_DONE"

    invoke-direct {v3, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5980
    .local v3, "intent":Landroid/content/Intent;
    const-string v10, "success"

    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 5982
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v10, :cond_0

    .line 5984
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v10, v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 5993
    .end local v3    # "intent":Landroid/content/Intent;
    :pswitch_3
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-boolean v10, v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mIsDragMode:Z

    if-eqz v10, :cond_0

    .line 5995
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishDragMode()V

    goto/16 :goto_0

    .line 5863
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
