.class public Lcom/sec/android/app/myfiles/view/PathIndicator;
.super Landroid/widget/HorizontalScrollView;
.source "PathIndicator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;,
        Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;,
        Lcom/sec/android/app/myfiles/view/PathIndicator$PathElement;
    }
.end annotation


# static fields
.field private static final MODULE:Ljava/lang/String; = "PathIndicator"


# instance fields
.field private mAnimation:Landroid/view/animation/Animation;

.field private mContext:Landroid/content/Context;

.field private mCurrentPathIndicatorPath:Ljava/lang/String;

.field private mFadeAnimation:Landroid/view/animation/Animation;

.field private mFolderButtonClickListener:Landroid/view/View$OnClickListener;

.field private mFolderButtonContainer:Landroid/widget/RelativeLayout;

.field private mFolderButtonKeyListener:Landroid/view/View$OnKeyListener;

.field private mFolderButtonViewHolderList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;",
            ">;"
        }
    .end annotation
.end field

.field private mFolderList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mForceRefreshPathIndicatorBar:Z

.field private mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

.field private mPrePathIndicatorPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mCurrentPathIndicatorPath:Ljava/lang/String;

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mForceRefreshPathIndicatorBar:Z

    .line 523
    new-instance v0, Lcom/sec/android/app/myfiles/view/PathIndicator$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/view/PathIndicator$3;-><init>(Lcom/sec/android/app/myfiles/view/PathIndicator;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonKeyListener:Landroid/view/View$OnKeyListener;

    .line 544
    new-instance v0, Lcom/sec/android/app/myfiles/view/PathIndicator$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/view/PathIndicator$4;-><init>(Lcom/sec/android/app/myfiles/view/PathIndicator;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonClickListener:Landroid/view/View$OnClickListener;

    .line 72
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    .line 74
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/view/PathIndicator;->createComponent()V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mCurrentPathIndicatorPath:Ljava/lang/String;

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mForceRefreshPathIndicatorBar:Z

    .line 523
    new-instance v0, Lcom/sec/android/app/myfiles/view/PathIndicator$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/view/PathIndicator$3;-><init>(Lcom/sec/android/app/myfiles/view/PathIndicator;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonKeyListener:Landroid/view/View$OnKeyListener;

    .line 544
    new-instance v0, Lcom/sec/android/app/myfiles/view/PathIndicator$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/view/PathIndicator$4;-><init>(Lcom/sec/android/app/myfiles/view/PathIndicator;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonClickListener:Landroid/view/View$OnClickListener;

    .line 81
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/view/PathIndicator;->createComponent()V

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "arg2"    # I

    .prologue
    .line 88
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mCurrentPathIndicatorPath:Ljava/lang/String;

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mForceRefreshPathIndicatorBar:Z

    .line 523
    new-instance v0, Lcom/sec/android/app/myfiles/view/PathIndicator$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/view/PathIndicator$3;-><init>(Lcom/sec/android/app/myfiles/view/PathIndicator;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonKeyListener:Landroid/view/View$OnKeyListener;

    .line 544
    new-instance v0, Lcom/sec/android/app/myfiles/view/PathIndicator$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/view/PathIndicator$4;-><init>(Lcom/sec/android/app/myfiles/view/PathIndicator;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonClickListener:Landroid/view/View$OnClickListener;

    .line 90
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    .line 92
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/view/PathIndicator;->createComponent()V

    .line 93
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/view/PathIndicator;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/view/PathIndicator;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/view/PathIndicator;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/view/PathIndicator;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonViewHolderList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/view/PathIndicator;)Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/view/PathIndicator;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/view/PathIndicator;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/view/PathIndicator;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderList:Ljava/util/ArrayList;

    return-object v0
.end method

.method private addPath(Ljava/lang/String;IIZ)V
    .locals 15
    .param p1, "folderName"    # Ljava/lang/String;
    .param p2, "index"    # I
    .param p3, "totalSize"    # I
    .param p4, "isAdd"    # Z

    .prologue
    .line 268
    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 270
    .local v8, "res":Landroid/content/res/Resources;
    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const-string v12, "layout_inflater"

    invoke-virtual {v11, v12}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    .line 272
    .local v5, "inflater":Landroid/view/LayoutInflater;
    const v11, 0x7f04003b

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v5, v11, v12, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/FrameLayout;

    .line 274
    .local v10, "view":Landroid/widget/FrameLayout;
    const v11, 0x7f0f00db

    invoke-virtual {v10, v11}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/FrameLayout;

    .line 276
    .local v4, "folderButtonContainer":Landroid/widget/FrameLayout;
    const v11, 0x7f0f00dc

    invoke-virtual {v10, v11}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 278
    .local v3, "folderButton":Landroid/widget/TextView;
    const v11, 0x7f0f00dd

    invoke-virtual {v10, v11}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 280
    .local v2, "arrow":Landroid/widget/ImageView;
    const v11, 0x7f0f00da

    invoke-virtual {v10, v11}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    .line 282
    .local v7, "pathBackground":Landroid/widget/RelativeLayout;
    new-instance v9, Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;

    invoke-direct {v9, v10, v3, v2}, Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;-><init>(Landroid/widget/FrameLayout;Landroid/widget/TextView;Landroid/widget/ImageView;)V

    .line 284
    .local v9, "vh":Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;
    add-int/lit8 v11, p2, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/FrameLayout;->setId(I)V

    .line 286
    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 288
    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 290
    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v10, v11}, Landroid/widget/FrameLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 292
    if-nez p2, :cond_3

    .line 294
    const/4 v11, 0x4

    invoke-virtual {v2, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 301
    :goto_0
    if-nez p2, :cond_4

    const-string v11, "storage"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 303
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isCheckMountedStorage()Z

    move-result v11

    if-nez v11, :cond_0

    .line 305
    const/4 v11, 0x0

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 312
    :cond_0
    :goto_1
    move/from16 v0, p3

    move/from16 v1, p2

    if-ne v0, v1, :cond_5

    if-nez p4, :cond_5

    .line 318
    :goto_2
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v11, -0x2

    const/4 v12, -0x1

    invoke-direct {v6, v11, v12}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 320
    .local v6, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonViewHolderList:Ljava/util/ArrayList;

    if-eqz v11, :cond_1

    .line 322
    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonViewHolderList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lez v11, :cond_6

    .line 328
    const v11, 0x7f090138

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {v6, v11}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 330
    const/16 v12, 0x11

    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonViewHolderList:Ljava/util/ArrayList;

    iget-object v13, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonViewHolderList:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    invoke-virtual {v11, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;

    iget-object v11, v11, Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v11}, Landroid/widget/FrameLayout;->getId()I

    move-result v11

    invoke-virtual {v6, v12, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 349
    :goto_3
    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonViewHolderList:Ljava/util/ArrayList;

    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 353
    :cond_1
    move/from16 v0, p3

    move/from16 v1, p2

    if-ne v0, v1, :cond_2

    .line 354
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/PathIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v11

    if-nez v11, :cond_8

    .line 356
    if-eqz p4, :cond_7

    .line 358
    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const/high16 v12, 0x7f050000

    invoke-static {v11, v12}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFadeAnimation:Landroid/view/animation/Animation;

    .line 359
    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v12, 0x7f050001

    invoke-static {v11, v12}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mAnimation:Landroid/view/animation/Animation;

    .line 381
    :goto_4
    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v7, v11}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 382
    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mAnimation:Landroid/view/animation/Animation;

    new-instance v12, Lcom/sec/android/app/myfiles/view/PathIndicator$1;

    move/from16 v0, p4

    invoke-direct {v12, p0, v0, v10}, Lcom/sec/android/app/myfiles/view/PathIndicator$1;-><init>(Lcom/sec/android/app/myfiles/view/PathIndicator;ZLandroid/widget/FrameLayout;)V

    invoke-virtual {v11, v12}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 402
    :cond_2
    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v10, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 404
    new-instance v11, Lcom/sec/android/app/myfiles/view/PathIndicator$2;

    invoke-direct {v11, p0}, Lcom/sec/android/app/myfiles/view/PathIndicator$2;-><init>(Lcom/sec/android/app/myfiles/view/PathIndicator;)V

    invoke-virtual {p0, v11}, Lcom/sec/android/app/myfiles/view/PathIndicator;->post(Ljava/lang/Runnable;)Z

    .line 418
    return-void

    .line 298
    .end local v6    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    const/4 v11, 0x0

    invoke-virtual {v2, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 308
    :cond_4
    const/4 v11, 0x1

    move/from16 v0, p2

    if-ne v0, v11, :cond_0

    const-string v11, "emulated"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 310
    const/4 v11, 0x0

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setEnabled(Z)V

    goto/16 :goto_1

    .line 315
    :cond_5
    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderList:Ljava/util/ArrayList;

    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/myfiles/view/PathIndicator;->getAbsoluteFolderName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 341
    .restart local v6    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_6
    const v11, 0x7f09013a

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v12

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingEnd()I

    move-result v13

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v14

    invoke-virtual {v3, v11, v12, v13, v14}, Landroid/widget/TextView;->setPaddingRelative(IIII)V

    .line 346
    const/16 v11, 0x14

    const/4 v12, -0x1

    invoke-virtual {v6, v11, v12}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto/16 :goto_3

    .line 362
    :cond_7
    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v12, 0x7f050003

    invoke-static {v11, v12}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFadeAnimation:Landroid/view/animation/Animation;

    .line 363
    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v12, 0x7f050004

    invoke-static {v11, v12}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mAnimation:Landroid/view/animation/Animation;

    .line 364
    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonViewHolderList:Ljava/util/ArrayList;

    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_4

    .line 368
    :cond_8
    if-eqz p4, :cond_9

    .line 370
    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const/high16 v12, 0x7f050000

    invoke-static {v11, v12}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFadeAnimation:Landroid/view/animation/Animation;

    .line 371
    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v12, 0x7f050002

    invoke-static {v11, v12}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mAnimation:Landroid/view/animation/Animation;

    goto/16 :goto_4

    .line 374
    :cond_9
    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v12, 0x7f050003

    invoke-static {v11, v12}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFadeAnimation:Landroid/view/animation/Animation;

    .line 375
    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v12, 0x7f050005

    invoke-static {v11, v12}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mAnimation:Landroid/view/animation/Animation;

    .line 376
    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonViewHolderList:Ljava/util/ArrayList;

    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_4
.end method

.method private addPath([Ljava/lang/String;Z)V
    .locals 3
    .param p1, "foldersArray"    # [Ljava/lang/String;
    .param p2, "isAdd"    # Z

    .prologue
    .line 642
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 643
    aget-object v1, p1, v0

    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    invoke-direct {p0, v1, v0, v2, p2}, Lcom/sec/android/app/myfiles/view/PathIndicator;->addPath(Ljava/lang/String;IIZ)V

    .line 642
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 645
    :cond_0
    return-void
.end method

.method private createComponent()V
    .locals 5

    .prologue
    const v4, 0x7f09013d

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 98
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/PathIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/PathIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0, v0, v3, v1, v3}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPadding(IIII)V

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderList:Ljava/util/ArrayList;

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonViewHolderList:Ljava/util/ArrayList;

    .line 104
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonContainer:Landroid/widget/RelativeLayout;

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonContainer:Landroid/widget/RelativeLayout;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/myfiles/view/PathIndicator;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 107
    return-void
.end method

.method private getAbsoluteFolderName(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const v6, 0x7f0b0043

    const v5, 0x7f0b0042

    const v4, 0x7f0b003f

    const v3, 0x7f0b012b

    const v2, 0x7f0b003e

    .line 706
    move-object v0, p1

    .line 708
    .local v0, "folderPath":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isCheckMountedStorage()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 710
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 712
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "emulated/0"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 765
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v2, 0x7f0b00fa

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 766
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v2, 0x7f0b00fa

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/Baidu"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 768
    :cond_1
    return-object v0

    .line 714
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 716
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "extSdCard"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 718
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0151

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 720
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0151

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Private"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 722
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 724
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "UsbDriveA"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 726
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 728
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "UsbDriveB"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 730
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0044

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 732
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0044

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "UsbDriveC"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 734
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0045

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 736
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0045

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "UsbDriveD"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 738
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0046

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 740
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0046

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "UsbDriveE"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 742
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0047

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 744
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0047

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "UsbDriveF"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 746
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 748
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/storage"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 754
    :cond_b
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 756
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 758
    :cond_c
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 760
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/storage"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private getCurPathIndicatorPath()Ljava/lang/String;
    .locals 4

    .prologue
    .line 246
    iget-object v3, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 248
    .local v1, "length":I
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 250
    .local v2, "path":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 252
    iget-object v3, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 254
    add-int/lit8 v3, v1, -0x1

    if-eq v0, v3, :cond_0

    .line 256
    const/16 v3, 0x2f

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 250
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 260
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mCurrentPathIndicatorPath:Ljava/lang/String;

    .line 262
    iget-object v3, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mCurrentPathIndicatorPath:Ljava/lang/String;

    return-object v3
.end method

.method private getDisPlayedPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const v9, 0x7f0b0044

    const v8, 0x7f0b0043

    const v7, 0x7f0b0042

    const v6, 0x7f0b003f

    const v5, 0x7f0b003e

    .line 649
    move-object v0, p1

    .line 650
    .local v0, "folderPath":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v4, 0x7f0b012b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 651
    .local v1, "localPath":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isCheckMountedStorage()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 652
    sget-object v2, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 653
    sget-object v2, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 698
    :cond_0
    :goto_0
    const-string v2, "/Baidu"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 699
    const-string v2, "/Baidu"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v5, 0x7f0b00fa

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 701
    :cond_1
    return-object v0

    .line 654
    :cond_2
    const-string v2, "/storage/extSdCard"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 655
    sget-object v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardStoragePath:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 656
    :cond_3
    const-string v2, "/storage/UsbDrive"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 657
    const-string v2, "/storage/UsbDriveA"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 658
    const-string v2, "/storage/UsbDriveA"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 659
    :cond_4
    const-string v2, "/storage/UsbDriveB"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 660
    const-string v2, "/storage/UsbDriveB"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 661
    :cond_5
    const-string v2, "/storage/UsbDriveC"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 662
    const-string v2, "/storage/UsbDriveC"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 663
    :cond_6
    const-string v2, "/storage/UsbDriveD"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 664
    const-string v2, "/storage/UsbDriveD"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v5, 0x7f0b0045

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 665
    :cond_7
    const-string v2, "/storage/UsbDriveE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 666
    const-string v2, "/storage/UsbDriveE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v5, 0x7f0b0046

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 667
    :cond_8
    const-string v2, "/storage/UsbDriveF"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 668
    const-string v2, "/storage/UsbDriveF"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v5, 0x7f0b0047

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 670
    :cond_9
    sget-object v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 671
    sget-object v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v5, 0x7f0b0151

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 672
    :cond_a
    const-string v2, "/storage"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 673
    const-string v2, "/storage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v5, 0x7f0b012b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 676
    :cond_b
    sget-object v2, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 677
    sget-object v2, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 678
    :cond_c
    const-string v2, "/storage/extSdCard"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 679
    sget-object v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardStoragePath:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 680
    :cond_d
    const-string v2, "/storage/UsbDrive"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 681
    const-string v2, "/storage/UsbDriveA"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 682
    const-string v2, "/storage/UsbDriveA"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 683
    :cond_e
    const-string v2, "/storage/UsbDriveB"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 684
    const-string v2, "/storage/UsbDriveB"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 685
    :cond_f
    const-string v2, "/storage/UsbDriveC"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 686
    const-string v2, "/storage/UsbDriveC"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 687
    :cond_10
    const-string v2, "/storage/UsbDriveD"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 688
    const-string v2, "/storage/UsbDriveD"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v5, 0x7f0b0045

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 689
    :cond_11
    const-string v2, "/storage/UsbDriveE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 690
    const-string v2, "/storage/UsbDriveE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v5, 0x7f0b0046

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 691
    :cond_12
    const-string v2, "/storage/UsbDriveF"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 692
    const-string v2, "/storage/UsbDriveF"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v5, 0x7f0b0047

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 694
    :cond_13
    sget-object v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 695
    sget-object v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mContext:Landroid/content/Context;

    const v5, 0x7f0b0151

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 509
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonViewHolderList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 511
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonViewHolderList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 513
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 514
    return-void
.end method

.method public goUp()V
    .locals 3

    .prologue
    .line 459
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonViewHolderList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonViewHolderList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gtz v2, :cond_1

    .line 474
    :cond_0
    :goto_0
    return-void

    .line 463
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonViewHolderList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    .line 465
    .local v1, "lastElementIndex":I
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonViewHolderList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;

    iget-object v0, v2, Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;->mContainer:Landroid/widget/FrameLayout;

    .line 467
    .local v0, "folderButton":Landroid/widget/FrameLayout;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 469
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v2, v1, :cond_2

    .line 471
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 473
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonViewHolderList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method

.method public setForceRefreshPathIndicatorBar(Z)V
    .locals 0
    .param p1, "force"    # Z

    .prologue
    .line 772
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mForceRefreshPathIndicatorBar:Z

    .line 773
    return-void
.end method

.method public setOnPathChangeListener(Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

    .prologue
    .line 519
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

    .line 520
    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 13
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 112
    if-nez p1, :cond_1

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    const-string v7, "/"

    invoke-virtual {p1, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {p1, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 119
    .local v1, "currentFolderName":Ljava/lang/String;
    const-string v4, ""

    .line 121
    .local v4, "preFolderName":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_2

    invoke-direct {p0}, Lcom/sec/android/app/myfiles/view/PathIndicator;->getCurPathIndicatorPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    iget-boolean v7, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mForceRefreshPathIndicatorBar:Z

    if-eqz v7, :cond_0

    .line 125
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    if-gt v7, v11, :cond_3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v7, v11, :cond_0

    invoke-virtual {p1, v12}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x2f

    if-ne v7, v8, :cond_0

    .line 128
    :cond_3
    const/4 v7, 0x2

    const-string v8, "PathIndicator"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "setPath() - folderPath : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 130
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/view/PathIndicator;->getDisPlayedPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 131
    .local v2, "folders":[Ljava/lang/String;
    new-array v5, v11, [Ljava/lang/String;

    const-string v7, ""

    aput-object v7, v5, v12

    .line 132
    .local v5, "preFolders":[Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mPrePathIndicatorPath:Ljava/lang/String;

    if-eqz v7, :cond_4

    .line 133
    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mPrePathIndicatorPath:Ljava/lang/String;

    invoke-direct {p0, v7}, Lcom/sec/android/app/myfiles/view/PathIndicator;->getDisPlayedPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 134
    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mPrePathIndicatorPath:Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mPrePathIndicatorPath:Ljava/lang/String;

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    iget-object v9, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mPrePathIndicatorPath:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 137
    :cond_4
    array-length v7, v5

    array-length v8, v2

    if-ne v7, v8, :cond_7

    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mPrePathIndicatorPath:Ljava/lang/String;

    if-eqz v7, :cond_7

    .line 139
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/PathIndicator;->clear()V

    .line 140
    invoke-direct {p0, v2, v11}, Lcom/sec/android/app/myfiles/view/PathIndicator;->addPath([Ljava/lang/String;Z)V

    .line 172
    :cond_5
    :goto_1
    iget-boolean v7, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mForceRefreshPathIndicatorBar:Z

    if-eqz v7, :cond_6

    .line 173
    iput-boolean v12, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mForceRefreshPathIndicatorBar:Z

    .line 176
    :cond_6
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mPrePathIndicatorPath:Ljava/lang/String;

    .line 178
    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonViewHolderList:Ljava/util/ArrayList;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonViewHolderList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-eqz v7, :cond_0

    .line 180
    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonViewHolderList:Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonViewHolderList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;

    .line 181
    .local v6, "v":Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;
    if-eqz v6, :cond_0

    iget-object v7, v6, Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;->mPathIndicatorButton:Landroid/widget/TextView;

    if-eqz v7, :cond_0

    .line 182
    iget-object v7, v6, Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;->mPathIndicatorButton:Landroid/widget/TextView;

    invoke-virtual {v7, v12}, Landroid/widget/TextView;->setClickable(Z)V

    .line 183
    iget-object v7, v6, Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;->mPathIndicatorButton:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/PathIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f08002a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 184
    iget-object v7, v6, Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;->mPathIndicatorButton:Landroid/widget/TextView;

    sget-object v8, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto/16 :goto_0

    .line 150
    .end local v6    # "v":Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/PathIndicator;->clear()V

    .line 151
    array-length v7, v5

    array-length v8, v2

    if-ge v7, v8, :cond_8

    .line 152
    invoke-direct {p0, v2, v11}, Lcom/sec/android/app/myfiles/view/PathIndicator;->addPath([Ljava/lang/String;Z)V

    goto :goto_1

    .line 153
    :cond_8
    array-length v7, v5

    array-length v8, v2

    if-le v7, v8, :cond_a

    .line 155
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 156
    .local v0, "currentFolder":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mPrePathIndicatorPath:Ljava/lang/String;

    invoke-direct {v3, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 157
    .local v3, "preFolder":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 158
    invoke-direct {p0, v5, v12}, Lcom/sec/android/app/myfiles/view/PathIndicator;->addPath([Ljava/lang/String;Z)V

    goto :goto_1

    .line 160
    :cond_9
    invoke-direct {p0, v2, v11}, Lcom/sec/android/app/myfiles/view/PathIndicator;->addPath([Ljava/lang/String;Z)V

    goto :goto_1

    .line 162
    .end local v0    # "currentFolder":Ljava/io/File;
    .end local v3    # "preFolder":Ljava/io/File;
    :cond_a
    array-length v7, v5

    array-length v8, v2

    if-ne v7, v8, :cond_5

    .line 163
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    const-string v7, ""

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_b

    iget-boolean v7, p0, Lcom/sec/android/app/myfiles/view/PathIndicator;->mForceRefreshPathIndicatorBar:Z

    if-eqz v7, :cond_0

    .line 166
    :cond_b
    invoke-direct {p0, v2, v11}, Lcom/sec/android/app/myfiles/view/PathIndicator;->addPath([Ljava/lang/String;Z)V

    goto/16 :goto_1
.end method
