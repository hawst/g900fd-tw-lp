.class Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$4;
.super Ljava/lang/Object;
.source "CreateFolderFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->clickOK()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

.field final synthetic val$currentPath:Ljava/lang/String;

.field final synthetic val$newFolderName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    iput-object p2, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$4;->val$currentPath:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$4;->val$newFolderName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 248
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 250
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_1

    .line 252
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$4;->val$currentPath:Ljava/lang/String;

    .line 254
    .local v1, "dropboxCurrentPath":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 256
    const-string v1, "/"

    .line 260
    :cond_0
    invoke-static {v0}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    move-result-object v3

    .line 262
    .local v3, "mDBAPIHelper":Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$4;->val$newFolderName:Ljava/lang/String;

    invoke-virtual {v3, v4, v1}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->createFolder(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;

    move-result-object v2

    .line 264
    .local v2, "folder":Lcom/samsung/scloud/data/SCloudFolder;
    if-eqz v2, :cond_2

    .line 266
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 268
    const-string v4, "CreateFolderFragment"

    const-string v5, "Dropbox MESSAGE_CREATEFOLDER_SUCCESS"

    invoke-static {v6, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 293
    .end local v1    # "dropboxCurrentPath":Ljava/lang/String;
    .end local v2    # "folder":Lcom/samsung/scloud/data/SCloudFolder;
    .end local v3    # "mDBAPIHelper":Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;
    :cond_1
    :goto_0
    return-void

    .line 272
    .restart local v1    # "dropboxCurrentPath":Ljava/lang/String;
    .restart local v2    # "folder":Lcom/samsung/scloud/data/SCloudFolder;
    .restart local v3    # "mDBAPIHelper":Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;
    :cond_2
    const-string v4, "CreateFolderFragment"

    const-string v5, "Dropbox creation_failed"

    invoke-static {v6, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 274
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 276
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$4$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$4$1;-><init>(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$4;)V

    invoke-virtual {v4, v5}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
