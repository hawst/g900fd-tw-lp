.class public Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
.super Landroid/app/DialogFragment;
.source "FileOperationUiFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;
    }
.end annotation


# static fields
.field private static final FILE_OPERATION_UI_FRAGMENT_TAG:Ljava/lang/String; = "file_operation_ui_fragment_tag"

.field protected static final MODULE:Ljava/lang/String; = "FileOperationUiFragment"


# instance fields
.field private mAbortReceiver:Landroid/content/BroadcastReceiver;

.field private mCancelButtonActive:Z

.field private mDismissFLag:Z

.field mOperationType:I

.field mProgressView:Landroid/view/View;

.field private mWaitOnCancel:Z

.field mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 74
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mDismissFLag:Z

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mCancelButtonActive:Z

    .line 80
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWaitOnCancel:Z

    .line 520
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWaitOnCancel:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;Landroid/widget/Button;Landroid/app/Dialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    .param p1, "x1"    # Landroid/widget/Button;
    .param p2, "x2"    # Landroid/app/Dialog;

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setUiToCancelling(Landroid/widget/Button;Landroid/app/Dialog;)V

    return-void
.end method

.method private getTitleResId(I)I
    .locals 5
    .param p1, "operationType"    # I

    .prologue
    .line 294
    packed-switch p1, :pswitch_data_0

    .line 375
    const/4 v2, -0x1

    .line 380
    .local v2, "titleResId":I
    :goto_0
    return v2

    .line 298
    .end local v2    # "titleResId":I
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getTag()Ljava/lang/String;

    move-result-object v1

    .line 300
    .local v1, "tag":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v3, "cacheDownload"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 302
    const v2, 0x7f0b00d2

    .restart local v2    # "titleResId":I
    goto :goto_0

    .line 306
    .end local v2    # "titleResId":I
    :cond_0
    const v2, 0x7f0b0022

    .line 309
    .restart local v2    # "titleResId":I
    goto :goto_0

    .line 314
    .end local v1    # "tag":Ljava/lang/String;
    .end local v2    # "titleResId":I
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "dst_folder"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 316
    .local v0, "mDstFolder":Ljava/lang/String;
    if-eqz v0, :cond_1

    sget-object v3, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 318
    const v2, 0x7f0b0163

    .restart local v2    # "titleResId":I
    goto :goto_0

    .line 322
    .end local v2    # "titleResId":I
    :cond_1
    const v2, 0x7f0b0021

    .line 326
    .restart local v2    # "titleResId":I
    goto :goto_0

    .line 331
    .end local v0    # "mDstFolder":Ljava/lang/String;
    .end local v2    # "titleResId":I
    :pswitch_2
    const v2, 0x7f0b0020

    .line 333
    .restart local v2    # "titleResId":I
    goto :goto_0

    .line 338
    .end local v2    # "titleResId":I
    :pswitch_3
    const v2, 0x7f0b0027

    .line 340
    .restart local v2    # "titleResId":I
    goto :goto_0

    .line 345
    .end local v2    # "titleResId":I
    :pswitch_4
    const v2, 0x7f0b0028

    .line 347
    .restart local v2    # "titleResId":I
    goto :goto_0

    .line 351
    .end local v2    # "titleResId":I
    :pswitch_5
    const v2, 0x7f0b0019

    .line 353
    .restart local v2    # "titleResId":I
    goto :goto_0

    .line 357
    .end local v2    # "titleResId":I
    :pswitch_6
    const v2, 0x7f0b0023

    .line 359
    .restart local v2    # "titleResId":I
    goto :goto_0

    .line 363
    .end local v2    # "titleResId":I
    :pswitch_7
    const v2, 0x7f0b002a

    .line 365
    .restart local v2    # "titleResId":I
    goto :goto_0

    .line 369
    .end local v2    # "titleResId":I
    :pswitch_8
    const v2, 0x7f0b00ff

    .line 371
    .restart local v2    # "titleResId":I
    goto :goto_0

    .line 294
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_8
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method public static newInstance(I)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    .locals 1
    .param p0, "operationType"    # I

    .prologue
    .line 84
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(IZ)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance(IZ)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    .locals 3
    .param p0, "operationType"    # I
    .param p1, "cancelButtonActive"    # Z

    .prologue
    .line 90
    new-instance v1, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    invoke-direct {v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;-><init>()V

    .line 92
    .local v1, "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 94
    .local v0, "argument":Landroid/os/Bundle;
    const-string v2, "operation"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 96
    invoke-virtual {v1, v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setArguments(Landroid/os/Bundle;)V

    .line 98
    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mDismissFLag:Z

    .line 100
    iput-boolean p1, v1, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mCancelButtonActive:Z

    .line 102
    return-object v1
.end method

.method public static newInstance(IZZ)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    .locals 1
    .param p0, "operationType"    # I
    .param p1, "cancelButtonActive"    # Z
    .param p2, "waitOnCancel"    # Z

    .prologue
    .line 108
    invoke-static {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(IZ)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v0

    .line 110
    .local v0, "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    iput-boolean p2, v0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWaitOnCancel:Z

    .line 112
    return-object v0
.end method

.method private setAbortReceiver()V
    .locals 3

    .prologue
    .line 237
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 239
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 241
    const-string v1, "android.intent.action.MEDIA_SCAN"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 243
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 245
    const-string v1, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 247
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 249
    new-instance v1, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$4;-><init>(Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mAbortReceiver:Landroid/content/BroadcastReceiver;

    .line 286
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mAbortReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 287
    return-void
.end method

.method private setUiToCancelling(Landroid/widget/Button;Landroid/app/Dialog;)V
    .locals 1
    .param p1, "button"    # Landroid/widget/Button;
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 225
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 227
    const v0, 0x7f0b009b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 229
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 233
    :cond_0
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 386
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 388
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 391
    .local v1, "fm":Landroid/app/FragmentManager;
    const-string v2, "work"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    .line 394
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    if-nez v2, :cond_0

    .line 396
    new-instance v2, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    invoke-direct {v2}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    .line 398
    new-instance v0, Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 400
    .local v0, "arguments":Landroid/os/Bundle;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->setArguments(Landroid/os/Bundle;)V

    .line 402
    const-string v2, "file_operation_ui_fragment_tag"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    const/4 v3, 0x0

    invoke-virtual {v2, p0, v3}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 407
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mProgressView:Landroid/view/View;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->setProgressView(Landroid/view/View;)V

    .line 409
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    const-string v4, "work"

    invoke-virtual {v2, v3, v4}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    .line 411
    .end local v0    # "arguments":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    .line 416
    invoke-super {p0, p1, p2, p3}, Landroid/app/DialogFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 418
    if-ne p2, v4, :cond_4

    .line 420
    if-eqz p3, :cond_1

    iget v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mOperationType:I

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mOperationType:I

    if-eq v2, v5, :cond_0

    iget v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mOperationType:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 421
    :cond_0
    const-string v2, "target_folder"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFolder:Ljava/lang/String;

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 422
    const-string v2, "FILE_OPERATION"

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mOperationType:I

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 423
    const-string v2, "dest_fragment_id"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    iget v3, v3, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 426
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 427
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getTargetRequestCode()I

    move-result v3

    invoke-virtual {v2, v3, v4, p3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 431
    :cond_2
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    if-eqz v2, :cond_3

    .line 432
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    invoke-virtual {v2, v3}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 479
    :cond_3
    :goto_0
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->dismissAllowingStateLoss()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 487
    :goto_1
    return-void

    .line 434
    :catch_0
    move-exception v0

    .line 436
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 442
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    if-nez p2, :cond_6

    .line 444
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 446
    new-instance p3, Landroid/content/Intent;

    .end local p3    # "data":Landroid/content/Intent;
    invoke-direct {p3}, Landroid/content/Intent;-><init>()V

    .line 448
    .restart local p3    # "data":Landroid/content/Intent;
    const-string v2, "FILE_OPERATION"

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mOperationType:I

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 450
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Fragment;->isAdded()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 452
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getTargetRequestCode()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, p3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 458
    :cond_5
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    if-eqz v2, :cond_3

    .line 460
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWorkFragment:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    invoke-virtual {v2, v3}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 463
    :catch_1
    move-exception v0

    .line 465
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 472
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->dismissAllowingStateLoss()V

    goto :goto_1

    .line 481
    :catch_2
    move-exception v1

    .line 483
    .local v1, "ex":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 485
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mDismissFLag:Z

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 118
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 119
    new-instance v0, Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 121
    .local v0, "argument":Landroid/os/Bundle;
    const-string v1, "operation"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mOperationType:I

    .line 123
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setAbortReceiver()V

    .line 125
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setCancelable(Z)V

    .line 126
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 132
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 134
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mOperationType:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getTitleResId(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 136
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 140
    .local v1, "factory":Landroid/view/LayoutInflater;
    const v2, 0x7f040039

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mProgressView:Landroid/view/View;

    .line 142
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mProgressView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 144
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 146
    new-instance v2, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$1;-><init>(Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 160
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mCancelButtonActive:Z

    if-eqz v2, :cond_0

    .line 162
    const v2, 0x7f0b0017

    new-instance v3, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$2;-><init>(Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 183
    :cond_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 503
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 505
    const/4 v0, 0x0

    const-string v1, "FileOperationUiFragment"

    const-string v2, "onDestroy"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 507
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mAbortReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 508
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 492
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 494
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mDismissFLag:Z

    if-eqz v0, :cond_0

    .line 496
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->dismiss()V

    .line 498
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 190
    invoke-super {p0}, Landroid/app/DialogFragment;->onStart()V

    .line 192
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->mWaitOnCancel:Z

    if-eqz v2, :cond_0

    .line 194
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    check-cast v1, Landroid/app/AlertDialog;

    .line 196
    .local v1, "dialog":Landroid/app/AlertDialog;
    if-eqz v1, :cond_0

    .line 198
    const/4 v2, -0x2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 200
    .local v0, "button":Landroid/widget/Button;
    if-eqz v0, :cond_0

    .line 202
    new-instance v2, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$3;

    invoke-direct {v2, p0, v0, v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$3;-><init>(Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;Landroid/widget/Button;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 220
    .end local v0    # "button":Landroid/widget/Button;
    .end local v1    # "dialog":Landroid/app/AlertDialog;
    :cond_0
    return-void
.end method
