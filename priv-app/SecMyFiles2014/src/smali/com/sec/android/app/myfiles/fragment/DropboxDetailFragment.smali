.class public Lcom/sec/android/app/myfiles/fragment/DropboxDetailFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;
.source "DropboxDetailFragment.java"


# instance fields
.field mAdapter:Landroid/widget/BaseAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public getFileLastModifiedTime(Ljava/lang/String;I)J
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 67
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/DropboxDetailFragment;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v1, p2}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 68
    .local v0, "cursor":Landroid/database/Cursor;
    const-string v1, "date_modified"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 70
    .local v2, "modificationTime":J
    return-wide v2
.end method

.method public getFileName(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 48
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DropboxDetailFragment;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v2, p2}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 50
    .local v0, "cursor":Landroid/database/Cursor;
    const-string v2, "title"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 52
    .local v1, "fileName":Ljava/lang/String;
    return-object v1
.end method

.method public getFileSize(Ljava/lang/String;I)J
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 60
    .local v0, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 61
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DropboxDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/android/app/myfiles/utils/Utils;->getPathDropboxFilesSize(Landroid/content/Context;Ljava/util/ArrayList;)J

    move-result-wide v2

    return-wide v2
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v1, 0x12

    .line 36
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->onCreate(Landroid/os/Bundle;)V

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DropboxDetailFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/MainActivity;->getCurrentFragmentID()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DropboxDetailFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getAdapter(I)Landroid/widget/BaseAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DropboxDetailFragment;->mAdapter:Landroid/widget/BaseAdapter;

    .line 43
    :goto_0
    return-void

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DropboxDetailFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getAdapter(I)Landroid/widget/BaseAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DropboxDetailFragment;->mAdapter:Landroid/widget/BaseAdapter;

    goto :goto_0
.end method
