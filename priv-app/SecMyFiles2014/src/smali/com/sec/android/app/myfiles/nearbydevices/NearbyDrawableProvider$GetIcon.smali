.class Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIcon;
.super Landroid/os/AsyncTask;
.source "NearbyDrawableProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GetIcon"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;",
        "Ljava/lang/Void;",
        "Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;",
        ">;"
    }
.end annotation


# instance fields
.field private volatile mCancelled:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 210
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 212
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIcon;->mCancelled:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$1;

    .prologue
    .line 210
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIcon;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;)Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;
    .locals 4
    .param p1, "args"    # [Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;

    .prologue
    .line 229
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIcon;->mCancelled:Z

    if-nez v3, :cond_1

    .line 231
    array-length v3, p1

    if-lez v3, :cond_1

    .line 232
    const/4 v2, 0x0

    .line 233
    .local v2, "drawable":Landroid/graphics/drawable/Drawable;
    const/4 v3, 0x0

    aget-object v0, p1, v3

    .line 234
    .local v0, "arg":Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;->getBitmap(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 235
    .local v1, "bmp":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_0

    .line 236
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    .end local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 240
    .restart local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    :goto_0
    invoke-virtual {v0, v2}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;->setResult(Landroid/graphics/drawable/Drawable;)V

    .line 245
    .end local v0    # "arg":Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;
    .end local v1    # "bmp":Landroid/graphics/Bitmap;
    .end local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    :goto_1
    return-object v0

    .line 238
    .restart local v0    # "arg":Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;
    .restart local v1    # "bmp":Landroid/graphics/Bitmap;
    .restart local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;->getDrawableUpdatable()Lcom/sec/android/app/myfiles/nearbydevices/DrawableUpdatable;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/app/myfiles/nearbydevices/DrawableUpdatable;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    goto :goto_0

    .line 245
    .end local v0    # "arg":Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;
    .end local v1    # "bmp":Landroid/graphics/Bitmap;
    .end local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 210
    check-cast p1, [Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIcon;->doInBackground([Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;)Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 250
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIcon;->mCancelled:Z

    .line 251
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 252
    return-void
.end method

.method protected onPostExecute(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;)V
    .locals 2
    .param p1, "result"    # Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;

    .prologue
    .line 216
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;->getResult()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 217
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;->getDrawableUpdatable()Lcom/sec/android/app/myfiles/nearbydevices/DrawableUpdatable;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;->getResult()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/myfiles/nearbydevices/DrawableUpdatable;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 220
    # getter for: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;->mTasks:Ljava/util/concurrent/ConcurrentMap;
    invoke-static {}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;->access$100()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    # getter for: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;->mTasks:Ljava/util/concurrent/ConcurrentMap;
    invoke-static {}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;->access$100()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 225
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 210
    check-cast p1, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIcon;->onPostExecute(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;)V

    return-void
.end method
