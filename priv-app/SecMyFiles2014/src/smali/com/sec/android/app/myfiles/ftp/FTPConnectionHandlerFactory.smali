.class public Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;
.super Ljava/lang/Object;
.source "FTPConnectionHandlerFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory$1;,
        Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory$DisconnectHandlerTask;
    }
.end annotation


# static fields
.field private static volatile mInstance:Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;


# instance fields
.field private final mHandlerMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/FTPParams;",
            "Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final mHandlingMdtm:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/FTPParams;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final mServiceHandlerMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/FTPParams;",
            "Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mInstance:Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 253
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mHandlerMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 256
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mServiceHandlerMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 259
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mHandlingMdtm:Ljava/util/concurrent/ConcurrentHashMap;

    .line 25
    return-void
.end method

.method private static disconnectHandler(Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;)V
    .locals 4
    .param p0, "handler"    # Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    .prologue
    .line 193
    if-eqz p0, :cond_0

    invoke-interface {p0}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 195
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory$DisconnectHandlerTask;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory$DisconnectHandlerTask;-><init>(Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory$1;)V

    .line 197
    .local v0, "task":Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory$DisconnectHandlerTask;
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory$DisconnectHandlerTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 201
    .end local v0    # "task":Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory$DisconnectHandlerTask;
    :cond_0
    return-void
.end method

.method private static getConnectionHandler(Lcom/sec/android/app/myfiles/ftp/FTPParams;)Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    .locals 3
    .param p0, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .prologue
    const/4 v0, 0x0

    .line 205
    if-eqz p0, :cond_0

    .line 207
    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory$1;->$SwitchMap$com$sec$android$app$myfiles$ftp$FTPType:[I

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getType()Lcom/sec/android/app/myfiles/ftp/FTPType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 226
    :cond_0
    :goto_0
    return-object v0

    .line 212
    :pswitch_0
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;-><init>(Lcom/sec/android/app/myfiles/ftp/FTPParams;)V

    goto :goto_0

    .line 207
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static getInstance()Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;
    .locals 2

    .prologue
    .line 29
    sget-object v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mInstance:Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;

    if-nez v0, :cond_1

    .line 31
    const-class v1, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;

    monitor-enter v1

    .line 33
    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mInstance:Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mInstance:Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;

    .line 39
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 43
    :cond_1
    sget-object v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mInstance:Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;

    return-object v0

    .line 39
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public getFTPConnectionHandler(Lcom/sec/android/app/myfiles/ftp/FTPParams;)Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    .locals 2
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .prologue
    .line 49
    if-eqz p1, :cond_2

    .line 51
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mHandlerMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 53
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mHandlerMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    .line 71
    :goto_0
    return-object v1

    .line 57
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->getConnectionHandler(Lcom/sec/android/app/myfiles/ftp/FTPParams;)Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-result-object v0

    .line 59
    .local v0, "handler":Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    if-eqz v0, :cond_1

    .line 61
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mHandlerMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    move-object v1, v0

    .line 65
    goto :goto_0

    .line 71
    .end local v0    # "handler":Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getFTPConnectionHandlerForService(Lcom/sec/android/app/myfiles/ftp/FTPParams;)Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    .locals 2
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .prologue
    .line 81
    if-eqz p1, :cond_2

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mServiceHandlerMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mServiceHandlerMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    .line 103
    :goto_0
    return-object v1

    .line 89
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->getConnectionHandler(Lcom/sec/android/app/myfiles/ftp/FTPParams;)Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-result-object v0

    .line 91
    .local v0, "handler":Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    if-eqz v0, :cond_1

    .line 93
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mServiceHandlerMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    move-object v1, v0

    .line 97
    goto :goto_0

    .line 103
    .end local v0    # "handler":Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isHandlingMdtm(Lcom/sec/android/app/myfiles/ftp/FTPParams;)Z
    .locals 1
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .prologue
    .line 179
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mHandlingMdtm:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mHandlingMdtm:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 185
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public removeAllHandlers()V
    .locals 3

    .prologue
    .line 147
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mHandlerMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    .line 149
    .local v0, "handler":Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->disconnectHandler(Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;)V

    goto :goto_0

    .line 153
    .end local v0    # "handler":Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mServiceHandlerMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    .line 155
    .restart local v0    # "handler":Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->disconnectHandler(Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;)V

    goto :goto_1

    .line 159
    .end local v0    # "handler":Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mHandlerMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 161
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mServiceHandlerMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 163
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mHandlingMdtm:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 165
    return-void
.end method

.method public removeFTPConnectionHandler(Lcom/sec/android/app/myfiles/ftp/FTPParams;)V
    .locals 2
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .prologue
    .line 111
    if-eqz p1, :cond_0

    .line 113
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mHandlerMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mHandlerMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    .line 117
    .local v0, "handler":Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mHandlerMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    invoke-static {v0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->disconnectHandler(Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;)V

    .line 125
    .end local v0    # "handler":Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    :cond_0
    return-void
.end method

.method public removeFTPConnectionHandlerForService(Lcom/sec/android/app/myfiles/ftp/FTPParams;)V
    .locals 2
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .prologue
    .line 129
    if-eqz p1, :cond_0

    .line 131
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mServiceHandlerMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 133
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mServiceHandlerMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    .line 135
    .local v0, "handler":Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mServiceHandlerMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    invoke-static {v0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->disconnectHandler(Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;)V

    .line 143
    .end local v0    # "handler":Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    :cond_0
    return-void
.end method

.method public setHandlingMdtm(Lcom/sec/android/app/myfiles/ftp/FTPParams;Z)V
    .locals 2
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "handlingMdtm"    # Z

    .prologue
    .line 169
    if-eqz p1, :cond_0

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->mHandlingMdtm:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    :cond_0
    return-void
.end method
