.class Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$1;
.super Ljava/lang/Object;
.source "AbsBrowserAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

.field final synthetic val$data:Ljava/lang/String;

.field final synthetic val$id:J


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;JLjava/lang/String;)V
    .locals 0

    .prologue
    .line 304
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    iput-wide p2, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$1;->val$id:J

    iput-object p4, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$1;->val$data:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 309
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 311
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    iget-wide v2, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$1;->val$id:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$1;->val$data:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyToSelectedItemChangeListener()V

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyDataSetChanged()V

    .line 316
    return-void
.end method
