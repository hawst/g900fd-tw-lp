.class public Lcom/sec/android/app/myfiles/MediaFile;
.super Ljava/lang/Object;
.source "MediaFile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/MediaFile$SCCFileUtil;,
        Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;
    }
.end annotation


# static fields
.field public static final FILE_ICON_DEFAULT_LARGE:I = 0x7f0200ae

.field public static final FILE_ICON_DEFAULT_SMALL:I = 0x7f0200ad

.field public static final FILE_TYPE_3G2_AUDIO:I = 0x12

.field public static final FILE_TYPE_3GA:I = 0x9

.field public static final FILE_TYPE_3GPP:I = 0x21

.field public static final FILE_TYPE_3GPP2:I = 0x22

.field public static final FILE_TYPE_3GPP_AUDIO:I = 0x14

.field public static final FILE_TYPE_3GP_AUDIO:I = 0x11

.field public static final FILE_TYPE_AAC:I = 0x8

.field public static final FILE_TYPE_AK3G:I = 0x2e

.field public static final FILE_TYPE_AMR:I = 0x4

.field public static final FILE_TYPE_APK:I = 0x64

.field public static final FILE_TYPE_ASC:I = 0x4e

.field public static final FILE_TYPE_ASF:I = 0x25

.field public static final FILE_TYPE_ASF_AUDIO:I = 0x13

.field public static final FILE_TYPE_AVI:I = 0x26

.field public static final FILE_TYPE_AWB:I = 0x5

.field public static final FILE_TYPE_BMP:I = 0x40

.field public static final FILE_TYPE_CSV:I = 0x50

.field public static final FILE_TYPE_DCF:I = 0x12c

.field public static final FILE_TYPE_DIVX:I = 0x27

.field public static final FILE_TYPE_DOC:I = 0x52

.field public static final FILE_TYPE_EBOOK:I = 0x59

.field public static final FILE_TYPE_EML:I = 0x8e

.field public static final FILE_TYPE_FLAC:I = 0xa

.field public static final FILE_TYPE_FLV:I = 0x28

.field public static final FILE_TYPE_GIF:I = 0x3e

.field public static final FILE_TYPE_GOLF:I = 0x44

.field public static final FILE_TYPE_GUL:I = 0x57

.field public static final FILE_TYPE_HTML:I = 0x7e

.field public static final FILE_TYPE_HWDT:I = 0x97

.field public static final FILE_TYPE_HWP:I = 0x4d

.field public static final FILE_TYPE_HWT:I = 0x96

.field public static final FILE_TYPE_ICS:I = 0x77

.field public static final FILE_TYPE_IMY:I = 0x18

.field public static final FILE_TYPE_ISMA:I = 0xd

.field public static final FILE_TYPE_ISMV:I = 0x33

.field public static final FILE_TYPE_JAD:I = 0x6e

.field public static final FILE_TYPE_JAR:I = 0x6f

.field public static final FILE_TYPE_JPEG:I = 0x3d

.field public static final FILE_TYPE_K3G:I = 0x2d

.field public static final FILE_TYPE_M3U:I = 0x47

.field public static final FILE_TYPE_M4A:I = 0x2

.field public static final FILE_TYPE_M4B:I = 0xb

.field public static final FILE_TYPE_M4V:I = 0x20

.field public static final FILE_TYPE_MID:I = 0x16

.field public static final FILE_TYPE_MKV:I = 0x29

.field public static final FILE_TYPE_MOV:I = 0x2a

.field public static final FILE_TYPE_MP3:I = 0x1

.field public static final FILE_TYPE_MP4:I = 0x1f

.field public static final FILE_TYPE_MP4_AUDIO:I = 0x10

.field public static final FILE_TYPE_MPG:I = 0x24

.field public static final FILE_TYPE_MPO:I = 0x43

.field public static final FILE_TYPE_ODF:I = 0x12e

.field public static final FILE_TYPE_OGG:I = 0x7

.field public static final FILE_TYPE_P12:I = 0x94

.field public static final FILE_TYPE_PDF:I = 0x51

.field public static final FILE_TYPE_PLS:I = 0x48

.field public static final FILE_TYPE_PNG:I = 0x3f

.field public static final FILE_TYPE_PPS:I = 0x4f

.field public static final FILE_TYPE_PPSX:I = 0x55

.field public static final FILE_TYPE_PPT:I = 0x54

.field public static final FILE_TYPE_PYA:I = 0xc

.field public static final FILE_TYPE_PYV:I = 0x2b

.field public static final FILE_TYPE_QCP:I = 0x15

.field public static final FILE_TYPE_RM:I = 0x30

.field public static final FILE_TYPE_RMVB:I = 0x31

.field public static final FILE_TYPE_SASF:I = 0x90

.field public static final FILE_TYPE_SCC:I = 0x92

.field public static final FILE_TYPE_SDP:I = 0x32

.field public static final FILE_TYPE_SKM:I = 0x2c

.field public static final FILE_TYPE_SM4:I = 0x12d

.field public static final FILE_TYPE_SMF:I = 0x17

.field public static final FILE_TYPE_SNB:I = 0x4c

.field public static final FILE_TYPE_SOL:I = 0x93

.field public static final FILE_TYPE_SPD:I = 0x4b

.field public static final FILE_TYPE_SPM:I = 0x19

.field public static final FILE_TYPE_SRW:I = 0x42

.field public static final FILE_TYPE_SSF:I = 0x4a

.field public static final FILE_TYPE_SVG:I = 0x5b

.field public static final FILE_TYPE_SWF:I = 0x5a

.field public static final FILE_TYPE_TS:I = 0x34

.field public static final FILE_TYPE_TXT:I = 0x56

.field public static final FILE_TYPE_VCF:I = 0x79

.field public static final FILE_TYPE_VCS:I = 0x78

.field public static final FILE_TYPE_VNT:I = 0x7a

.field public static final FILE_TYPE_VTS:I = 0x7b

.field public static final FILE_TYPE_WAV:I = 0x3

.field public static final FILE_TYPE_WBMP:I = 0x41

.field public static final FILE_TYPE_WEBM:I = 0x2f

.field public static final FILE_TYPE_WEBP:I = 0x45

.field public static final FILE_TYPE_WGT:I = 0x65

.field public static final FILE_TYPE_WMA:I = 0x6

.field public static final FILE_TYPE_WMV:I = 0x23

.field public static final FILE_TYPE_WPL:I = 0x49

.field public static final FILE_TYPE_XHTML:I = 0x80

.field public static final FILE_TYPE_XLS:I = 0x53

.field public static final FILE_TYPE_XLSM:I = 0x95

.field public static final FILE_TYPE_XML:I = 0x7f

.field public static final FILE_TYPE_ZIP:I = 0xc8

.field public static final FIRST_ARCHIVE_FILE_TYPE:I = 0xc8

.field private static final FIRST_AUDIO_FILE_TYPE:I = 0x1

.field private static final FIRST_DOCUMENT_FILE_TYPE:I = 0x4b

.field private static final FIRST_DRM_FILE_TYPE:I = 0x12c

.field private static final FIRST_FLASH_FILE_TYPE:I = 0x5a

.field private static final FIRST_IMAGE_FILE_TYPE:I = 0x3d

.field private static final FIRST_INSTALL_FILE_TYPE:I = 0x64

.field private static final FIRST_JAVA_FILE_TYPE:I = 0x6e

.field private static final FIRST_MIDI_FILE_TYPE:I = 0x16

.field private static final FIRST_PLAYLIST_FILE_TYPE:I = 0x47

.field private static final FIRST_VIDEO_FILE_TYPE:I = 0x1f

.field public static final LAST_ARCHIVE_FILE_TYPE:I = 0xc8

.field private static final LAST_AUDIO_FILE_TYPE:I = 0x15

.field private static final LAST_DOCUMENT_FILE_TYPE:I = 0x57

.field private static final LAST_DRM_FILE_TYPE:I = 0x12e

.field private static final LAST_FLASH_FILE_TYPE:I = 0x5b

.field private static final LAST_IMAGE_FILE_TYPE:I = 0x45

.field private static final LAST_INSTALL_FILE_TYPE:I = 0x65

.field private static final LAST_JAVA_FILE_TYPE:I = 0x6f

.field private static final LAST_MIDI_FILE_TYPE:I = 0x19

.field private static final LAST_PLAYLIST_FILE_TYPE:I = 0x49

.field private static final LAST_VIDEO_FILE_TYPE:I = 0x34

.field private static final MODULE:Ljava/lang/String; = "MediaFile"

.field public static final UNKNOWN_STRING:Ljava/lang/String; = "<unknown>"

.field private static sDocumentExtensions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sExtensionToMediaFileTypeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;",
            ">;"
        }
    .end annotation
.end field

.field private static sExtensionToMimeType:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static sFileExtensions:Ljava/lang/String;

.field private static sMimeTypeToExtensionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private static sMimeTypeToFileTypeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const v12, 0x7f0200ad

    const v11, 0x7f0200c3

    const v10, 0x7f0200c2

    const v9, 0x7f0200ab

    const v8, 0x7f0200aa

    .line 64
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/MediaFile;->sExtensionToMediaFileTypeMap:Ljava/util/HashMap;

    .line 66
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/MediaFile;->sMimeTypeToFileTypeMap:Ljava/util/HashMap;

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/MediaFile;->sMimeTypeToExtensionMap:Ljava/util/HashMap;

    .line 70
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/MediaFile;->sExtensionToMimeType:Ljava/util/HashMap;

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/MediaFile;->sDocumentExtensions:Ljava/util/ArrayList;

    .line 413
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/MyFilesFeatures;->ATT_BASE_MODEL:Z

    if-eqz v0, :cond_4

    .line 415
    const-string v0, "EML"

    const/16 v1, 0x8e

    const-string v2, "message/rfc822"

    const-string v3, "EML"

    const v4, 0x7f020081

    const v5, 0x7f020082

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 422
    :goto_0
    const-string v0, "MP3"

    const/4 v1, 0x1

    const-string v2, "audio/mpeg"

    const-string v3, "Mpeg"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 423
    const-string v0, "M4A"

    const/4 v1, 0x2

    const-string v2, "audio/mp4"

    const-string v3, "M4A"

    const v4, 0x7f02007a

    const v5, 0x7f02007b

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 424
    const-string v0, "WAV"

    const/4 v1, 0x3

    const-string v2, "audio/x-wav"

    const-string v3, "WAVE"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 425
    const-string v0, "AMR"

    const/4 v1, 0x4

    const-string v2, "audio/amr"

    const-string v3, "AMR"

    const v4, 0x7f02007a

    const v5, 0x7f02007b

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 426
    const-string v0, "AWB"

    const/4 v1, 0x5

    const-string v2, "audio/amr-wb"

    const-string v3, "AWB"

    const v4, 0x7f02007a

    const v5, 0x7f02007a

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 427
    const-string v0, "WMA"

    const/4 v1, 0x6

    const-string v2, "audio/x-ms-wma"

    const-string v3, "WMA"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 428
    const-string v0, "OGG"

    const/4 v1, 0x7

    const-string v2, "audio/ogg"

    const-string v3, "OGG"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 429
    const-string v0, "OGA"

    const/4 v1, 0x7

    const-string v2, "audio/ogg"

    const-string v3, "OGA"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 430
    const-string v0, "AAC"

    const/16 v1, 0x8

    const-string v2, "audio/aac"

    const-string v3, "AAC"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 431
    const-string v0, "3GA"

    const/16 v1, 0x9

    const-string v2, "audio/3gpp"

    const-string v3, "3GA"

    const v4, 0x7f02007a

    const v5, 0x7f02007b

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 432
    const-string v0, "FLAC"

    const/16 v1, 0xa

    const-string v2, "audio/flac"

    const-string v3, "FLAC"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 433
    const-string v0, "MPGA"

    const/4 v1, 0x1

    const-string v2, "audio/mpeg"

    const-string v3, "MPGA"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 434
    const-string v0, "MP4_A"

    const/16 v1, 0x10

    const-string v2, "audio/mp4"

    const-string v3, "MP4 Audio"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 435
    const-string v0, "MP4A"

    const/16 v1, 0x10

    const-string v2, "audio/mp4"

    const-string v3, "MP4 Audio"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 436
    const-string v0, "3GP_A"

    const/16 v1, 0x11

    const-string v2, "audio/3gpp"

    const-string v3, "3GP Audio"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 437
    const-string v0, "3G2_A"

    const/16 v1, 0x12

    const-string v2, "audio/3gpp2"

    const-string v3, "3G2 Audio"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 438
    const-string v0, "ASF_A"

    const/16 v1, 0x13

    const-string v2, "audio/x-ms-asf"

    const-string v3, "ASF Audio"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 439
    const-string v0, "3GPP_A"

    const/16 v1, 0x14

    const-string v2, "audio/3gpp"

    const-string v3, "3GPP"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 440
    const-string v0, "MID"

    const/16 v1, 0x16

    const-string v2, "audio/midi"

    const-string v3, "MIDI"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 441
    const-string v0, "MID_A"

    const/16 v1, 0x16

    const-string v2, "audio/mid"

    const-string v3, "MIDI"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 442
    const-string v0, "XMF"

    const/16 v1, 0x16

    const-string v2, "audio/midi"

    const-string v3, "XMF"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 443
    const-string v0, "MXMF"

    const/16 v1, 0x16

    const-string v2, "audio/midi"

    const-string v3, "MXMF"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 444
    const-string v0, "RTTTL"

    const/16 v1, 0x16

    const-string v2, "audio/midi"

    const-string v3, "RTTTL"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 445
    const-string v0, "SMF"

    const/16 v1, 0x17

    const-string v2, "audio/sp-midi"

    const-string v3, "SMF"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 446
    const-string v0, "SPMID"

    const/16 v1, 0x17

    const-string v2, "audio/sp-midi"

    const-string v3, "SPMID"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 447
    const-string v0, "IMY"

    const/16 v1, 0x18

    const-string v2, "audio/imelody"

    const-string v3, "IMY"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 448
    const-string v0, "MMF"

    const/16 v1, 0x16

    const-string v2, "audio/midi"

    const-string v3, "MMF"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 458
    const-string v0, "MIDI"

    const/16 v1, 0x16

    const-string v2, "audio/midi"

    const-string v3, "MIDI"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 459
    const-string v0, "RTX"

    const/16 v1, 0x16

    const-string v2, "audio/midi"

    const-string v3, "MIDI"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 460
    const-string v0, "OTA"

    const/16 v1, 0x16

    const-string v2, "audio/midi"

    const-string v3, "MIDI"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 463
    const-string v0, "PYA"

    const/16 v1, 0xc

    const-string v2, "audio/vnd.ms-playready.media.pya"

    const-string v3, "PYA"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 468
    const-string v0, "M4B"

    const/16 v1, 0xb

    const-string v2, "audio/mp4"

    const-string v3, "M4B"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 469
    const-string v0, "ISMA"

    const/16 v1, 0xd

    const-string v2, "audio/isma"

    const-string v3, "ISMA"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 473
    invoke-static {}, Lcom/sec/android/app/myfiles/MediaFile;->isQCPEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 474
    const-string v0, "QCP"

    const/16 v1, 0x15

    const-string v2, "audio/qcelp"

    const-string v3, "QCP"

    move v4, v8

    move v5, v9

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 477
    :cond_0
    const-string v0, "MPEG"

    const/16 v1, 0x24

    const-string v2, "video/mpeg"

    const-string v3, "MPEG"

    move v4, v10

    move v5, v11

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 478
    const-string v0, "MPG"

    const/16 v1, 0x24

    const-string v2, "video/mpeg"

    const-string v3, "MPEG"

    move v4, v10

    move v5, v11

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 479
    const-string v0, "MP4"

    const/16 v1, 0x1f

    const-string v2, "video/mp4"

    const-string v3, "MP4"

    move v4, v10

    move v5, v11

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 480
    const-string v0, "M4V"

    const/16 v1, 0x20

    const-string v2, "video/mp4"

    const-string v3, "M4V"

    move v4, v10

    move v5, v11

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 481
    const-string v0, "3GP"

    const/16 v1, 0x21

    const-string v2, "video/3gpp"

    const-string v3, "3GP"

    move v4, v10

    move v5, v11

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 482
    const-string v0, "3GPP"

    const/16 v1, 0x21

    const-string v2, "video/3gpp"

    const-string v3, "3GPP"

    move v4, v10

    move v5, v11

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 483
    const-string v0, "3G2"

    const/16 v1, 0x22

    const-string v2, "video/3gpp2"

    const-string v3, "3G2"

    move v4, v10

    move v5, v11

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 484
    const-string v0, "3GPP2"

    const/16 v1, 0x22

    const-string v2, "video/3gpp2"

    const-string v3, "3GPP2"

    move v4, v10

    move v5, v11

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 485
    const-string v0, "WMV"

    const/16 v1, 0x23

    const-string v2, "video/x-ms-wmv"

    const-string v3, "WMV"

    move v4, v10

    move v5, v11

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 486
    const-string v0, "ASF"

    const/16 v1, 0x25

    const-string v2, "video/x-ms-asf"

    const-string v3, "ASF"

    move v4, v10

    move v5, v11

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 487
    const-string v0, "AVI"

    const/16 v1, 0x26

    const-string v2, "video/avi"

    const-string v3, "AVI"

    move v4, v10

    move v5, v11

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 488
    const-string v0, "DIVX"

    const/16 v1, 0x27

    const-string v2, "video/divx"

    const-string v3, "DIVX"

    move v4, v10

    move v5, v11

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 489
    const-string v0, "FLV"

    const/16 v1, 0x28

    const-string v2, "video/flv"

    const-string v3, "FLV"

    move v4, v10

    move v5, v11

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 490
    const-string v0, "MKV"

    const/16 v1, 0x29

    const-string v2, "video/mkv"

    const-string v3, "MKV"

    move v4, v10

    move v5, v11

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 491
    const-string v0, "SDP"

    const/16 v1, 0x32

    const-string v2, "application/sdp"

    const-string v3, "SDP"

    move v4, v10

    move v5, v11

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 492
    const-string v0, "TS"

    const/16 v1, 0x34

    const-string v2, "video/mp2ts"

    const-string v3, "TS"

    move v4, v10

    move v5, v11

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 501
    const-string v0, "SAMSUNG-SGH-I777"

    invoke-static {v0}, Lcom/sec/android/app/myfiles/MediaFile;->isDevice(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 502
    const-string v0, "MOV"

    const/16 v1, 0x2a

    const-string v2, "video/quicktime"

    const-string v3, "MOV"

    move v4, v10

    move v5, v11

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 506
    :cond_1
    const-string v0, "PYV"

    const/16 v1, 0x2b

    const-string v2, "video/vnd.ms-playready.media.pyv"

    const-string v3, "PYV"

    move v4, v10

    move v5, v11

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 511
    const-string v0, "ISMV"

    const/16 v1, 0x33

    const-string v2, "video/ismv"

    const-string v3, "ISMV"

    move v4, v10

    move v5, v11

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 516
    invoke-static {}, Lcom/sec/android/app/myfiles/MediaFile;->isKOREnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 517
    const-string v0, "SKM"

    const/16 v1, 0x2c

    const-string v2, "video/skm"

    const-string v3, "SKM"

    move v4, v10

    move v5, v11

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 518
    const-string v0, "K3G"

    const/16 v1, 0x2d

    const-string v2, "video/k3g"

    const-string v3, "K3G"

    move v4, v10

    move v5, v11

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 519
    const-string v0, "AK3G"

    const/16 v1, 0x2e

    const-string v2, "video/ak3g"

    const-string v3, "AK3G"

    move v4, v10

    move v5, v11

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 524
    :cond_2
    const-string v0, "WEBM"

    const/16 v1, 0x2f

    const-string v2, "video/webm"

    const-string v3, "WEBM"

    move v4, v10

    move v5, v11

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 525
    const-string v0, "JPG"

    const/16 v1, 0x3d

    const-string v2, "image/jpeg"

    const-string v3, "JPEG"

    const v4, 0x7f0200b4

    const v5, 0x7f0200b5

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 526
    const-string v0, "JPEG"

    const/16 v1, 0x3d

    const-string v2, "image/jpeg"

    const-string v3, "JPEG"

    const v4, 0x7f0200b4

    const v5, 0x7f0200b5

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 527
    const-string v0, "MY5"

    const/16 v1, 0x3d

    const-string v2, "image/vnd.tmo.my5"

    const-string v3, "JPEG"

    const v4, 0x7f0200b4

    const v5, 0x7f0200b5

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 528
    const-string v0, "GIF"

    const/16 v1, 0x3e

    const-string v2, "image/gif"

    const-string v3, "GIF"

    const v4, 0x7f0200b4

    const v5, 0x7f0200b5

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 529
    const-string v0, "PNG"

    const/16 v1, 0x3f

    const-string v2, "image/png"

    const-string v3, "PNG"

    const v4, 0x7f0200b4

    const v5, 0x7f0200b5

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 530
    const-string v0, "BMP"

    const/16 v1, 0x40

    const-string v2, "image/x-ms-bmp"

    const-string v3, "Microsoft BMP"

    const v4, 0x7f0200b4

    const v5, 0x7f0200b5

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 531
    const-string v0, "WBMP"

    const/16 v1, 0x41

    const-string v2, "image/vnd.wap.wbmp"

    const-string v3, "Wireless BMP"

    const v4, 0x7f0200b4

    const v5, 0x7f0200b5

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 532
    const-string v0, "WEBP"

    const/16 v1, 0x45

    const-string v2, "image/webp"

    const-string v3, "WEBP"

    const v4, 0x7f0200b4

    const v5, 0x7f0200b5

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 534
    const-string v0, "GOLF"

    const/16 v1, 0x44

    const-string v2, "image/golf"

    const-string v3, "GOLF"

    const v4, 0x7f0200b4

    const v5, 0x7f0200b5

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 540
    const-string v0, "M3U"

    const/16 v1, 0x47

    const-string v2, "audio/x-mpegurl"

    const-string v3, "M3U"

    const v5, 0x7f0200ae

    move v4, v12

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 541
    const-string v0, "PLS"

    const/16 v1, 0x48

    const-string v2, "audio/x-scpls"

    const-string v3, "PLS"

    const v5, 0x7f0200ae

    move v4, v12

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 542
    const-string v0, "WPL"

    const/16 v1, 0x49

    const-string v2, "application/vnd.ms-wpl"

    const-string v3, "WPL"

    const v5, 0x7f0200ae

    move v4, v12

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 543
    const-string v0, "PDF"

    const/16 v1, 0x51

    const-string v2, "application/pdf"

    const-string v3, "Acrobat PDF"

    const v4, 0x7f02008c

    const v5, 0x7f02008d

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 544
    const-string v0, "RTF"

    const/16 v1, 0x52

    const-string v2, "application/msword"

    const-string v3, "Microsoft Office WORD"

    const v4, 0x7f02007e

    const v5, 0x7f02007f

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 545
    const-string v0, "DOC"

    const/16 v1, 0x52

    const-string v2, "application/msword"

    const-string v3, "Microsoft Office WORD"

    const v4, 0x7f02007e

    const v5, 0x7f02007f

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 546
    const-string v0, "DOCX"

    const/16 v1, 0x52

    const-string v2, "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

    const-string v3, "Microsoft Office WORD"

    const v4, 0x7f02007e

    const v5, 0x7f02007f

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 549
    const-string v0, "DOT"

    const/16 v1, 0x52

    const-string v2, "application/msword"

    const-string v3, "Microsoft Office WORD"

    const v4, 0x7f02007e

    const v5, 0x7f02007f

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 552
    const-string v0, "DOTX"

    const/16 v1, 0x52

    const-string v2, "application/vnd.openxmlformats-officedocument.wordprocessingml.template"

    const-string v3, "Microsoft Office WORD"

    const v4, 0x7f02007e

    const v5, 0x7f02007f

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 555
    const-string v0, "CSV"

    const/16 v1, 0x50

    const-string v2, "text/comma-separated-values"

    const-string v3, "Microsoft Office Excel"

    const v4, 0x7f0200a7

    const v5, 0x7f0200a8

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 558
    const-string v0, "XLS"

    const/16 v1, 0x53

    const-string v2, "application/vnd.ms-excel"

    const-string v3, "Microsoft Office Excel"

    const v4, 0x7f0200a7

    const v5, 0x7f0200a8

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 561
    const-string v0, "XLSX"

    const/16 v1, 0x53

    const-string v2, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    const-string v3, "Microsoft Office Excel"

    const v4, 0x7f0200a7

    const v5, 0x7f0200a8

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 564
    const-string v0, "XLT"

    const/16 v1, 0x53

    const-string v2, "application/vnd.ms-excel"

    const-string v3, "Microsoft Office Excel"

    const v4, 0x7f0200a7

    const v5, 0x7f0200a8

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 567
    const-string v0, "XLTX"

    const/16 v1, 0x53

    const-string v2, "application/vnd.openxmlformats-officedocument.spreadsheetml.template"

    const-string v3, "Microsoft Office Excel"

    const v4, 0x7f0200a7

    const v5, 0x7f0200a8

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 570
    const-string v0, "XLSM"

    const/16 v1, 0x95

    const-string v2, "application/vnd.ms-excel.sheet.macroenabled.12"

    const-string v3, "Microsoft Office Excel"

    const v4, 0x7f0200a7

    const v5, 0x7f0200a8

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 573
    const-string v0, "PPS"

    const/16 v1, 0x4f

    const-string v2, "application/vnd.ms-powerpoint"

    const-string v3, "Microsoft Office PowerPoint"

    const v4, 0x7f02008e

    const v5, 0x7f02008f

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 576
    const-string v0, "PPT"

    const/16 v1, 0x54

    const-string v2, "application/vnd.ms-powerpoint"

    const-string v3, "Microsoft Office PowerPoint"

    const v4, 0x7f02008e

    const v5, 0x7f02008f

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 579
    const-string v0, "PPTX"

    const/16 v1, 0x54

    const-string v2, "application/vnd.openxmlformats-officedocument.presentationml.presentation"

    const-string v3, "Microsoft Office PowerPoint"

    const v4, 0x7f02008e

    const v5, 0x7f02008f

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 582
    const-string v0, "POT"

    const/16 v1, 0x54

    const-string v2, "application/vnd.ms-powerpoint"

    const-string v3, "Microsoft Office PowerPoint"

    const v4, 0x7f02008e

    const v5, 0x7f02008f

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 585
    const-string v0, "POTX"

    const/16 v1, 0x54

    const-string v2, "application/vnd.openxmlformats-officedocument.presentationml.template"

    const-string v3, "Microsoft Office PowerPoint"

    const v4, 0x7f02008e

    const v5, 0x7f02008f

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 587
    const-string v0, "PPSX"

    const/16 v1, 0x55

    const-string v2, "application/vnd.openxmlformats-officedocument.presentationml.slideshow"

    const-string v3, "Microsoft Office PowerPoint"

    const v4, 0x7f02008e

    const v5, 0x7f02008f

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 591
    const-string v0, "ASC"

    const/16 v1, 0x4e

    const-string v2, "text/plain"

    const-string v3, "Text Document"

    const v4, 0x7f020096

    const v5, 0x7f020097

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 592
    const-string v0, "TXT"

    const/16 v1, 0x56

    const-string v2, "text/plain"

    const-string v3, "Text Document"

    const v4, 0x7f020096

    const v5, 0x7f020097

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 593
    const-string v0, "GUL"

    const/16 v1, 0x57

    const-string v2, "application/jungumword"

    const-string v3, "Jungum Word"

    const v5, 0x7f0200ae

    move v4, v12

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 596
    const-string v0, "EPUB"

    const/16 v1, 0x59

    const-string v2, "application/epub+zip"

    const-string v3, "eBookReader"

    const v4, 0x7f020096

    const v5, 0x7f020097

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 597
    const-string v0, "ACSM"

    const/16 v1, 0x59

    const-string v2, "application/vnd.adobe.adept+xml"

    const-string v3, "eBookReader"

    const v4, 0x7f020096

    const v5, 0x7f020097

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 600
    const-string v0, "SWF"

    const/16 v1, 0x5a

    const-string v2, "application/x-shockwave-flash"

    const-string v3, "SWF"

    const v4, 0x7f0200b4

    const v5, 0x7f0200b5

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 601
    const-string v0, "SVG"

    const/16 v1, 0x5b

    const-string v2, "image/svg+xml"

    const-string v3, "SVG"

    const v5, 0x7f0200ae

    move v4, v12

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 611
    const-string v0, "DCF"

    const/16 v1, 0x12c

    const-string v2, "application/vnd.oma.drm.content"

    const-string v3, "DRM Content"

    const v5, 0x7f0200ae

    move v4, v12

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 612
    const-string v0, "ODF"

    const/16 v1, 0x12e

    const-string v2, "application/vnd.oma.drm.content"

    const-string v3, "DRM Content"

    const v5, 0x7f0200ae

    move v4, v12

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 613
    const-string v0, "SM4"

    const/16 v1, 0x12d

    const-string v2, "video/vnd.sdrm-media.sm4"

    const-string v3, "DRM Content"

    const v5, 0x7f0200ae

    move v4, v12

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 618
    const-string v0, "APK"

    const/16 v1, 0x64

    const-string v2, "application/apk"

    const-string v3, "Android package install file"

    const v4, 0x7f02007c

    const v5, 0x7f02007d

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 619
    const-string v0, "JAD"

    const/16 v1, 0x6e

    const-string v2, "text/vnd.sun.j2me.app-descriptor"

    const-string v3, "JAD"

    const v5, 0x7f0200ae

    move v4, v12

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 620
    const-string v0, "JAR"

    const/16 v1, 0x6f

    const-string v2, "application/java-archive"

    const-string v3, "JAR"

    const v5, 0x7f0200ae

    move v4, v12

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 621
    const-string v0, "VCS"

    const/16 v1, 0x78

    const-string v2, "text/x-vCalendar"

    const-string v3, "VCS"

    const v4, 0x7f0200be

    const v5, 0x7f0200bf

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 622
    const-string v0, "ICS"

    const/16 v1, 0x78

    const-string v2, "text/x-vCalendar"

    const-string v3, "ICS"

    const v4, 0x7f0200be

    const v5, 0x7f0200bf

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 623
    const-string v0, "VTS"

    const/16 v1, 0x7b

    const-string v2, "text/x-vtodo"

    const-string v3, "VTS"

    const v4, 0x7f0200bb

    const v5, 0x7f0200bc

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 624
    const-string v0, "VCF"

    const/16 v1, 0x79

    const-string v2, "text/x-vcard"

    const-string v3, "VCF"

    const v4, 0x7f0200c0

    const v5, 0x7f0200c1

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 625
    const-string v0, "VNT"

    const/16 v1, 0x7a

    const-string v2, "text/x-vnote"

    const-string v3, "VNT"

    const v4, 0x7f0200c4

    const v5, 0x7f0200c5

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 626
    const-string v0, "HTML"

    const/16 v1, 0x7e

    const-string v2, "text/html"

    const-string v3, "HTML"

    const v4, 0x7f020086

    const v5, 0x7f020087

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 627
    const-string v0, "HTM"

    const/16 v1, 0x7e

    const-string v2, "text/html"

    const-string v3, "HTML"

    const v4, 0x7f020086

    const v5, 0x7f020087

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 628
    const-string v0, "XHTML"

    const/16 v1, 0x80

    const-string v2, "text/html"

    const-string v3, "XHTML"

    const v4, 0x7f020086

    const v5, 0x7f020087

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 629
    const-string v0, "XML"

    const/16 v1, 0x7f

    const-string v2, "application/xhtml+xml"

    const-string v3, "XML"

    const v4, 0x7f020086

    const v5, 0x7f020087

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 630
    const-string v0, "WGT"

    const/16 v1, 0x65

    const-string v2, "application/vnd.samsung.widget"

    const-string v3, "WGT"

    const v4, 0x7f020086

    const v5, 0x7f020087

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 631
    const-string v0, "HWP"

    const/16 v1, 0x4d

    const-string v2, "application/x-hwp"

    const-string v3, "HWP"

    const v4, 0x7f020088

    const v5, 0x7f020089

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 632
    const-string v0, "HWT"

    const/16 v1, 0x96

    const-string v2, "application/hwt"

    const-string v3, "HWT"

    const v4, 0x7f020088

    const v5, 0x7f020089

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 633
    const-string v0, "HWDT"

    const/16 v1, 0x97

    const-string v2, "application/hancomhwdt"

    const-string v3, "HWDT"

    const v4, 0x7f02007e

    const v5, 0x7f02007f

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 634
    const-string v0, "SSF"

    const/16 v1, 0x4a

    const-string v2, "application/ssf"

    const-string v3, "SSF"

    const v4, 0x7f0200b9

    const v5, 0x7f0200ba

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 635
    const-string v0, "SNB"

    const/16 v1, 0x4c

    const-string v2, "application/snb"

    const-string v3, "SNB"

    const v4, 0x7f020092

    const v5, 0x7f020093

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 636
    const-string v0, "SPD"

    const/16 v1, 0x4c

    const-string v2, "application/spd"

    const-string v3, "SPD"

    const v4, 0x7f020094

    const v5, 0x7f020095

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 637
    const-string v0, "ZIP"

    const/16 v1, 0xc8

    const-string v2, "application/zip"

    const-string v3, "ZIP"

    const v4, 0x7f0200c6

    const v5, 0x7f0200c7

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 638
    const-string v0, "SASF"

    const/16 v1, 0x90

    const-string v2, "application/x-sasf"

    const-string v3, "SASF"

    const v5, 0x7f0200ae

    move v4, v12

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 639
    const-string v0, "SOL"

    const/16 v1, 0x93

    const-string v2, "application/com.sec.soloist"

    const-string v3, "SOL"

    const v5, 0x7f0200ae

    move v4, v12

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 640
    const-string v0, "SCC"

    const/16 v1, 0x92

    const-string v2, "application/vnd.samsung.scc.storyalbum"

    const-string v3, "SCC"

    const v4, 0x7f0200b9

    const v5, 0x7f0200ba

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 642
    const-string v0, "PFX"

    const/16 v1, 0x94

    const-string v2, "application/x-pkcs12"

    const-string v3, "PFX"

    const v5, 0x7f0200ae

    move v4, v12

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 643
    const-string v0, "P12"

    const/16 v1, 0x94

    const-string v2, "application/x-pkcs12"

    const-string v3, "P12"

    const v5, 0x7f0200ae

    move v4, v12

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 645
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 647
    .local v6, "builder":Ljava/lang/StringBuilder;
    sget-object v0, Lcom/sec/android/app/myfiles/MediaFile;->sExtensionToMediaFileTypeMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 649
    .local v7, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 651
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 653
    const/16 v0, 0x2c

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 656
    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 419
    .end local v6    # "builder":Ljava/lang/StringBuilder;
    .end local v7    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_4
    const-string v0, "EML"

    const/16 v1, 0x8e

    const-string v2, "message/rfc822"

    const-string v3, "EML"

    const v4, 0x7f020080

    const v5, 0x7f020083

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    .line 659
    .restart local v6    # "builder":Ljava/lang/StringBuilder;
    .restart local v7    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_5
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/MediaFile;->sFileExtensions:Ljava/lang/String;

    .line 660
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1316
    return-void
.end method

.method static addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V
    .locals 7
    .param p0, "extension"    # Ljava/lang/String;
    .param p1, "fileType"    # I
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "desc"    # Ljava/lang/String;
    .param p4, "iconSmall"    # I
    .param p5, "iconLarge"    # I

    .prologue
    .line 382
    new-instance v0, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;-><init>(ILjava/lang/String;Ljava/lang/String;II)V

    .line 384
    .local v0, "mediaFileType":Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;
    sget-object v1, Lcom/sec/android/app/myfiles/MediaFile;->sExtensionToMediaFileTypeMap:Ljava/util/HashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 386
    sget-object v1, Lcom/sec/android/app/myfiles/MediaFile;->sMimeTypeToFileTypeMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p2, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 388
    const/4 v6, 0x0

    .line 390
    .local v6, "extensionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v1, Lcom/sec/android/app/myfiles/MediaFile;->sMimeTypeToExtensionMap:Ljava/util/HashMap;

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "extensionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    check-cast v6, Ljava/util/ArrayList;

    .restart local v6    # "extensionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v6, :cond_1

    .line 392
    new-instance v6, Ljava/util/ArrayList;

    .end local v6    # "extensionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 394
    .restart local v6    # "extensionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v6, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 396
    sget-object v1, Lcom/sec/android/app/myfiles/MediaFile;->sMimeTypeToExtensionMap:Ljava/util/HashMap;

    invoke-virtual {v1, p2, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    :goto_0
    sget-object v1, Lcom/sec/android/app/myfiles/MediaFile;->sExtensionToMimeType:Ljava/util/HashMap;

    invoke-virtual {v1, p0, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 405
    invoke-static {p1}, Lcom/sec/android/app/myfiles/MediaFile;->isDocumentFileType(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 407
    sget-object v1, Lcom/sec/android/app/myfiles/MediaFile;->sDocumentExtensions:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 409
    :cond_0
    return-void

    .line 400
    :cond_1
    invoke-virtual {v6, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static getDescription(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 886
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->getFileType(Ljava/lang/String;)Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;

    move-result-object v0

    .line 887
    .local v0, "mediaType":Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;
    if-nez v0, :cond_0

    const-string v1, ""

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;->description:Ljava/lang/String;

    goto :goto_0
.end method

.method public static getDocumentExtensions()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 1313
    sget-object v0, Lcom/sec/android/app/myfiles/MediaFile;->sDocumentExtensions:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/android/app/myfiles/MediaFile;->sDocumentExtensions:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public static getExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1289
    if-nez p0, :cond_1

    .line 1301
    :cond_0
    :goto_0
    return-object v1

    .line 1294
    :cond_1
    const/16 v2, 0x2e

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 1296
    .local v0, "lastDot":I
    if-ltz v0, :cond_0

    .line 1301
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getExtensionFromMimeType(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7
    .param p0, "mimeType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1126
    const-string v5, "*"

    invoke-virtual {p0, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1128
    const/4 v5, 0x0

    const/16 v6, 0x2f

    invoke-virtual {p0, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 1132
    .local v4, "prefix":Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1134
    .local v0, "extensionResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v5, Lcom/sec/android/app/myfiles/MediaFile;->sMimeTypeToExtensionMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 1136
    .local v2, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1138
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1140
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .local v3, "matchedMimeType":Ljava/lang/String;
    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1142
    sget-object v5, Lcom/sec/android/app/myfiles/MediaFile;->sMimeTypeToExtensionMap:Ljava/util/HashMap;

    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Collection;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 1146
    .end local v3    # "matchedMimeType":Ljava/lang/String;
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-nez v5, :cond_2

    .line 1148
    const/4 v0, 0x0

    .line 1155
    .end local v0    # "extensionResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v2    # "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v4    # "prefix":Ljava/lang/String;
    :cond_2
    :goto_1
    return-object v0

    :cond_3
    sget-object v5, Lcom/sec/android/app/myfiles/MediaFile;->sMimeTypeToExtensionMap:Ljava/util/HashMap;

    invoke-virtual {v5, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    move-object v0, v5

    goto :goto_1
.end method

.method public static getFileType(Ljava/lang/String;)Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 769
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 771
    .local v0, "ext":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 772
    const/4 v1, 0x0

    .line 776
    :goto_0
    return-object v1

    .line 774
    :cond_0
    sget-object v2, Lcom/sec/android/app/myfiles/MediaFile;->sExtensionToMediaFileTypeMap:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;

    .line 776
    .local v1, "mediaType":Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;
    goto :goto_0
.end method

.method public static getFileType(Ljava/lang/String;Landroid/content/Context;I)Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;
    .locals 5
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fileType"    # I

    .prologue
    .line 781
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 783
    .local v0, "ext":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 784
    const/4 v1, 0x0

    .line 798
    :cond_0
    :goto_0
    return-object v1

    .line 786
    :cond_1
    sget-object v2, Lcom/sec/android/app/myfiles/MediaFile;->sExtensionToMediaFileTypeMap:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;

    .line 788
    .local v1, "mediaType":Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->needToCheckMimeType(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "SCC"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 790
    invoke-static {p2}, Lcom/sec/android/app/myfiles/MediaFile;->isAudioFileType(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 792
    sget-object v2, Lcom/sec/android/app/myfiles/MediaFile;->sExtensionToMediaFileTypeMap:Ljava/util/HashMap;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_A"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "mediaType":Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;
    check-cast v1, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;

    .restart local v1    # "mediaType":Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;
    goto :goto_0
.end method

.method public static getFileTypeForMimeType(Ljava/lang/String;)I
    .locals 2
    .param p0, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 1112
    sget-object v1, Lcom/sec/android/app/myfiles/MediaFile;->sMimeTypeToFileTypeMap:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1114
    .local v0, "value":Ljava/lang/Integer;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public static getFileTypeInt(Ljava/lang/String;)I
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 803
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->getFileType(Ljava/lang/String;)Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;

    move-result-object v0

    .line 804
    .local v0, "mediaType":Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget v1, v0, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;->fileType:I

    goto :goto_0
.end method

.method public static getFileTypeInt(Ljava/lang/String;Landroid/content/Context;)I
    .locals 3
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 809
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->needToCheckMimeType(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 810
    invoke-static {p0, p1}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeTypeFromMediaStore(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 812
    .local v1, "mimeType":Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeForMimeType(Ljava/lang/String;)I

    move-result v2

    .line 815
    .end local v1    # "mimeType":Ljava/lang/String;
    :goto_0
    return v2

    .line 814
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->getFileType(Ljava/lang/String;)Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;

    move-result-object v0

    .line 815
    .local v0, "mediaType":Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;
    if-nez v0, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    iget v2, v0, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;->fileType:I

    goto :goto_0
.end method

.method public static getLargeIcon(Ljava/lang/String;)I
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 949
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->needToCheckMimeType(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 951
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 953
    .local v0, "ext":Ljava/lang/String;
    const-string v2, "SCC"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 955
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile$SCCFileUtil;->getSmallIcon(Ljava/lang/String;)I

    move-result v2

    .line 965
    .end local v0    # "ext":Ljava/lang/String;
    :goto_0
    return v2

    .line 958
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->isEncryptionFile(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 960
    const v2, 0x7f020085

    goto :goto_0

    .line 964
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->getFileType(Ljava/lang/String;)Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;

    move-result-object v1

    .line 965
    .local v1, "mediaType":Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;
    if-nez v1, :cond_2

    const v2, 0x7f0200ae

    goto :goto_0

    :cond_2
    iget v2, v1, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;->iconLarge:I

    goto :goto_0
.end method

.method public static getLargeIcon(Ljava/lang/String;Landroid/content/Context;I)I
    .locals 5
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fileType"    # I

    .prologue
    const v3, 0x7f0200ad

    .line 970
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->needToCheckMimeType(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 972
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 974
    .local v0, "ext":Ljava/lang/String;
    const-string v4, "SCC"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 976
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile$SCCFileUtil;->getSmallIcon(Ljava/lang/String;)I

    move-result v3

    .line 1002
    .end local v0    # "ext":Ljava/lang/String;
    :cond_0
    :goto_0
    return v3

    .line 980
    :cond_1
    invoke-static {p1, p0}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isDRMFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 981
    invoke-static {p1, p0}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->getRealMimeTypeOfDRM(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 982
    .local v2, "realMimeType":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 983
    const-string v4, "audio"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 986
    const-string v4, "video"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 989
    const-string v4, "image"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 995
    .end local v2    # "realMimeType":Ljava/lang/String;
    :cond_2
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->isEncryptionFile(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 997
    const v3, 0x7f020085

    goto :goto_0

    .line 1001
    :cond_3
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/myfiles/MediaFile;->getFileType(Ljava/lang/String;Landroid/content/Context;I)Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;

    move-result-object v1

    .line 1002
    .local v1, "mediaType":Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;
    if-nez v1, :cond_4

    const v3, 0x7f0200ae

    goto :goto_0

    :cond_4
    iget v3, v1, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;->iconLarge:I

    goto :goto_0
.end method

.method public static getLargeIconDrawable(Ljava/io/File;Landroid/app/Activity;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p0, "f"    # Ljava/io/File;
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    .line 1062
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1064
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 1066
    .local v0, "folderPath":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isExternalRootFolder(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1068
    const v1, 0x7f0200b3

    .line 1106
    .end local v0    # "folderPath":Ljava/lang/String;
    .local v1, "icon":I
    :goto_0
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    return-object v2

    .line 1070
    .end local v1    # "icon":I
    .restart local v0    # "folderPath":Ljava/lang/String;
    :cond_0
    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isUsbStorageRootFolder(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1073
    const v1, 0x7f0200b3

    .restart local v1    # "icon":I
    goto :goto_0

    .line 1075
    .end local v1    # "icon":I
    :cond_1
    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1078
    const v1, 0x7f0200b3

    .restart local v1    # "icon":I
    goto :goto_0

    .line 1082
    .end local v1    # "icon":I
    :cond_2
    const v1, 0x7f0200b1

    .restart local v1    # "icon":I
    goto :goto_0

    .line 1088
    .end local v0    # "folderPath":Ljava/lang/String;
    .end local v1    # "icon":I
    :cond_3
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/MediaFile;->needToCheckMimeType(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1090
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/sec/android/app/myfiles/MediaFile;->isAudioInMediaStore(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1092
    const v1, 0x7f0200ab

    .restart local v1    # "icon":I
    goto :goto_0

    .line 1096
    .end local v1    # "icon":I
    :cond_4
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/MediaFile;->getLargeIcon(Ljava/lang/String;)I

    move-result v1

    .restart local v1    # "icon":I
    goto :goto_0

    .line 1101
    .end local v1    # "icon":I
    :cond_5
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/MediaFile;->getLargeIcon(Ljava/lang/String;)I

    move-result v1

    .restart local v1    # "icon":I
    goto :goto_0
.end method

.method public static getMediaFileType(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/myfiles/utils/FileType;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 820
    invoke-static {p1}, Lcom/sec/android/app/myfiles/MediaFile;->getFileType(Ljava/lang/String;)Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;

    move-result-object v0

    .line 821
    .local v0, "mediaType":Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;
    if-eqz v0, :cond_7

    .line 822
    iget v2, v0, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;->fileType:I

    .line 823
    .local v2, "type":I
    const/16 v3, 0x12c

    if-eq v2, v3, :cond_0

    const/16 v3, 0x12d

    if-eq v2, v3, :cond_0

    const/16 v3, 0x12e

    if-ne v2, v3, :cond_1

    .line 824
    :cond_0
    invoke-static {p0, p1}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isOMADrmFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 825
    invoke-static {p0, p1}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->getRealMimeTypeOfDRM(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 826
    .local v1, "mimeType":Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeForMimeType(Ljava/lang/String;)I

    move-result v2

    .line 829
    .end local v1    # "mimeType":Ljava/lang/String;
    :cond_1
    const/4 v3, 0x1

    if-lt v2, v3, :cond_2

    const/16 v3, 0x15

    if-le v2, v3, :cond_3

    :cond_2
    const/16 v3, 0x16

    if-lt v2, v3, :cond_4

    const/16 v3, 0x19

    if-gt v2, v3, :cond_4

    .line 831
    :cond_3
    sget-object v3, Lcom/sec/android/app/myfiles/utils/FileType;->AUDIO:Lcom/sec/android/app/myfiles/utils/FileType;

    .line 841
    .end local v2    # "type":I
    :goto_0
    return-object v3

    .line 832
    .restart local v2    # "type":I
    :cond_4
    const/16 v3, 0x1f

    if-lt v2, v3, :cond_5

    const/16 v3, 0x34

    if-gt v2, v3, :cond_5

    .line 833
    sget-object v3, Lcom/sec/android/app/myfiles/utils/FileType;->VIDEO:Lcom/sec/android/app/myfiles/utils/FileType;

    goto :goto_0

    .line 834
    :cond_5
    const/16 v3, 0x4b

    if-lt v2, v3, :cond_6

    const/16 v3, 0x57

    if-gt v2, v3, :cond_6

    .line 835
    sget-object v3, Lcom/sec/android/app/myfiles/utils/FileType;->DOCUMENT:Lcom/sec/android/app/myfiles/utils/FileType;

    goto :goto_0

    .line 836
    :cond_6
    const/16 v3, 0x3d

    if-lt v2, v3, :cond_7

    const/16 v3, 0x45

    if-gt v2, v3, :cond_7

    .line 837
    sget-object v3, Lcom/sec/android/app/myfiles/utils/FileType;->IMAGE:Lcom/sec/android/app/myfiles/utils/FileType;

    goto :goto_0

    .line 841
    .end local v2    # "type":I
    :cond_7
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static getMimeType(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 845
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->getFileType(Ljava/lang/String;)Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;

    move-result-object v0

    .line 862
    .local v0, "mediaType":Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;
    if-nez v0, :cond_0

    const-string v1, ""

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;->mimeType:Ljava/lang/String;

    goto :goto_0
.end method

.method public static getMimeTypeForExtention(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "extention"    # Ljava/lang/String;

    .prologue
    .line 1120
    sget-object v0, Lcom/sec/android/app/myfiles/MediaFile;->sExtensionToMimeType:Ljava/util/HashMap;

    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public static getMimeTypeFromFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "$filePath"    # Ljava/lang/String;

    .prologue
    .line 1435
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v0

    .line 1436
    .local v0, "type":Landroid/webkit/MimeTypeMap;
    invoke-static {p0}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getMimeTypeFromMediaStore(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1198
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1200
    .local v1, "ext":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 1202
    const/4 v2, 0x0

    .line 1228
    :goto_0
    return-object v2

    .line 1205
    :cond_0
    const/4 v2, 0x0

    .line 1209
    .local v2, "mimetype":Ljava/lang/String;
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile$SCCFileUtil;->isSCCFile(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1211
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile$SCCFileUtil;->getMimetypeFromSCCFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1212
    :cond_1
    invoke-static {p0, p1}, Lcom/sec/android/app/myfiles/MediaFile;->isAudioInMediaStore(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1214
    sget-object v3, Lcom/sec/android/app/myfiles/MediaFile;->sExtensionToMediaFileTypeMap:Ljava/util/HashMap;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_A"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;

    iget-object v2, v3, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;->mimeType:Ljava/lang/String;

    goto :goto_0

    .line 1218
    :cond_2
    sget-object v3, Lcom/sec/android/app/myfiles/MediaFile;->sExtensionToMediaFileTypeMap:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;

    iget-object v2, v3, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;->mimeType:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1221
    :catch_0
    move-exception v0

    .line 1223
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1225
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getShareMimeType(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 868
    const/4 v1, 0x0

    .line 870
    .local v1, "mimeType":Ljava/lang/String;
    invoke-static {p1, p0}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isOMADrmFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 871
    invoke-static {p1, p0}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->getRealMimeTypeOfDRM(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 881
    :goto_0
    return-object v1

    .line 872
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->getFileType(Ljava/lang/String;)Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;

    move-result-object v0

    .local v0, "mediaType":Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;
    if-nez v0, :cond_1

    .line 873
    const-string v1, "application/octet-stream"

    goto :goto_0

    .line 875
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->needToCheckMimeType(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 876
    invoke-static {p0, p1}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeTypeFromMediaStore(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 878
    :cond_2
    iget-object v1, v0, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;->mimeType:Ljava/lang/String;

    goto :goto_0
.end method

.method public static getSmallIcon(Ljava/lang/String;)I
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 893
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->needToCheckMimeType(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 895
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 897
    .local v0, "ext":Ljava/lang/String;
    const-string v2, "SCC"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 899
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile$SCCFileUtil;->getSmallIcon(Ljava/lang/String;)I

    move-result v2

    .line 908
    .end local v0    # "ext":Ljava/lang/String;
    :goto_0
    return v2

    .line 902
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->isEncryptionFile(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 904
    const v2, 0x7f020084

    goto :goto_0

    .line 907
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->getFileType(Ljava/lang/String;)Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;

    move-result-object v1

    .line 908
    .local v1, "mediaType":Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;
    if-nez v1, :cond_2

    const v2, 0x7f0200ad

    goto :goto_0

    :cond_2
    iget v2, v1, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;->iconSmall:I

    goto :goto_0
.end method

.method public static getSmallIcon(Ljava/lang/String;Landroid/content/Context;I)I
    .locals 5
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fileType"    # I

    .prologue
    const v3, 0x7f0200ae

    .line 913
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->needToCheckMimeType(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 915
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 917
    .local v0, "ext":Ljava/lang/String;
    const-string v4, "SCC"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 919
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile$SCCFileUtil;->getSmallIcon(Ljava/lang/String;)I

    move-result v3

    .line 943
    .end local v0    # "ext":Ljava/lang/String;
    :cond_0
    :goto_0
    return v3

    .line 922
    :cond_1
    invoke-static {p1, p0}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isDRMFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 923
    invoke-static {p1, p0}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->getRealMimeTypeOfDRM(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 924
    .local v2, "realMimeType":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 925
    const-string v4, "audio"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 928
    const-string v4, "video"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 931
    const-string v4, "image"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 937
    .end local v2    # "realMimeType":Ljava/lang/String;
    :cond_2
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->isEncryptionFile(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 939
    const v3, 0x7f020084

    goto :goto_0

    .line 942
    :cond_3
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/myfiles/MediaFile;->getFileType(Ljava/lang/String;Landroid/content/Context;I)Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;

    move-result-object v1

    .line 943
    .local v1, "mediaType":Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;
    if-nez v1, :cond_4

    const v3, 0x7f0200ad

    goto :goto_0

    :cond_4
    iget v3, v1, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;->iconSmall:I

    goto :goto_0
.end method

.method public static getSmallIconDrawable(Ljava/io/File;Landroid/app/Activity;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p0, "f"    # Ljava/io/File;
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    .line 1010
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1012
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 1014
    .local v0, "folderPath":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isExternalRootFolder(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1016
    const v1, 0x7f0200b2

    .line 1054
    .end local v0    # "folderPath":Ljava/lang/String;
    .local v1, "icon":I
    :goto_0
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    return-object v2

    .line 1018
    .end local v1    # "icon":I
    .restart local v0    # "folderPath":Ljava/lang/String;
    :cond_0
    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isUsbStorageRootFolder(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1021
    const v1, 0x7f0200b2

    .restart local v1    # "icon":I
    goto :goto_0

    .line 1023
    .end local v1    # "icon":I
    :cond_1
    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1026
    const v1, 0x7f0200b2

    .restart local v1    # "icon":I
    goto :goto_0

    .line 1030
    .end local v1    # "icon":I
    :cond_2
    const v1, 0x7f0200af

    .restart local v1    # "icon":I
    goto :goto_0

    .line 1036
    .end local v0    # "folderPath":Ljava/lang/String;
    .end local v1    # "icon":I
    :cond_3
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/MediaFile;->needToCheckMimeType(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1038
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/sec/android/app/myfiles/MediaFile;->isAudioInMediaStore(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1040
    const v1, 0x7f0200aa

    .restart local v1    # "icon":I
    goto :goto_0

    .line 1044
    .end local v1    # "icon":I
    :cond_4
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/MediaFile;->getSmallIcon(Ljava/lang/String;)I

    move-result v1

    .restart local v1    # "icon":I
    goto :goto_0

    .line 1049
    .end local v1    # "icon":I
    :cond_5
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/MediaFile;->getSmallIcon(Ljava/lang/String;)I

    move-result v1

    .restart local v1    # "icon":I
    goto :goto_0
.end method

.method public static isArchiveFileType(I)Z
    .locals 1
    .param p0, "fileType"    # I

    .prologue
    const/16 v0, 0xc8

    .line 755
    if-lt p0, v0, :cond_0

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isAudioFileType(I)Z
    .locals 2
    .param p0, "fileType"    # I

    .prologue
    const/4 v0, 0x1

    .line 687
    if-lt p0, v0, :cond_0

    const/16 v1, 0x15

    if-le p0, v1, :cond_1

    :cond_0
    const/16 v1, 0x16

    if-lt p0, v1, :cond_2

    const/16 v1, 0x19

    if-gt p0, v1, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isAudioInMediaStore(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 4
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1235
    const/4 v1, 0x0

    .line 1236
    .local v1, "mimeType":Ljava/lang/String;
    new-instance v2, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v2}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 1240
    .local v2, "retriever":Landroid/media/MediaMetadataRetriever;
    if-eqz p0, :cond_0

    .line 1241
    :try_start_0
    invoke-virtual {v2, p0}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 1242
    const/16 v3, 0xc

    invoke-virtual {v2, v3}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1249
    :cond_0
    :goto_0
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 1251
    if-eqz v1, :cond_1

    .line 1252
    invoke-static {v1}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeForMimeType(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->isAudioFileType(I)Z

    move-result v3

    .line 1254
    :goto_1
    return v3

    .line 1245
    :catch_0
    move-exception v0

    .line 1246
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1254
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private static isDevice(Ljava/lang/String;)Z
    .locals 1
    .param p0, "device"    # Ljava/lang/String;

    .prologue
    .line 1307
    const-string v0, "ro.product.model"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isDocumentFileType(I)Z
    .locals 1
    .param p0, "fileType"    # I

    .prologue
    .line 707
    const/16 v0, 0x4b

    if-lt p0, v0, :cond_0

    const/16 v0, 0x57

    if-le p0, v0, :cond_1

    :cond_0
    const/16 v0, 0x95

    if-eq p0, v0, :cond_1

    const/16 v0, 0x96

    if-eq p0, v0, :cond_1

    const/16 v0, 0x97

    if-ne p0, v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isDrmFileType(I)Z
    .locals 1
    .param p0, "fileType"    # I

    .prologue
    .line 732
    const/16 v0, 0x12c

    if-lt p0, v0, :cond_0

    const/16 v0, 0x12e

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isFlashFileType(I)Z
    .locals 1
    .param p0, "fileType"    # I

    .prologue
    .line 717
    const/16 v0, 0x5a

    if-lt p0, v0, :cond_0

    const/16 v0, 0x5b

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isImageFileType(I)Z
    .locals 1
    .param p0, "fileType"    # I

    .prologue
    .line 697
    const/16 v0, 0x3d

    if-lt p0, v0, :cond_0

    const/16 v0, 0x45

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isInstallFileType(I)Z
    .locals 1
    .param p0, "fileType"    # I

    .prologue
    .line 722
    const/16 v0, 0x64

    if-lt p0, v0, :cond_0

    const/16 v0, 0x65

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isJavaFileType(I)Z
    .locals 1
    .param p0, "fileType"    # I

    .prologue
    .line 727
    const/16 v0, 0x6e

    if-lt p0, v0, :cond_0

    const/16 v0, 0x6f

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isKOREnabled()Z
    .locals 2

    .prologue
    .line 677
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 679
    .local v0, "region":Ljava/lang/String;
    const-string v1, "SKC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SKT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "KTC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "KTT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "LUC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "LGT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 680
    :cond_0
    const/4 v1, 0x1

    .line 683
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isMIDFileType(I)Z
    .locals 1
    .param p0, "fileType"    # I

    .prologue
    .line 737
    const/16 v0, 0x16

    if-lt p0, v0, :cond_0

    const/16 v0, 0x19

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPlayListFileType(I)Z
    .locals 1
    .param p0, "fileType"    # I

    .prologue
    .line 702
    const/16 v0, 0x47

    if-lt p0, v0, :cond_0

    const/16 v0, 0x49

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPlayReadyType(I)Z
    .locals 1
    .param p0, "fileType"    # I

    .prologue
    .line 750
    const/16 v0, 0xc

    if-eq p0, v0, :cond_0

    const/16 v0, 0x2b

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isQCPEnabled()Z
    .locals 2

    .prologue
    .line 666
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 668
    .local v0, "region":Ljava/lang/String;
    const-string v1, "SPR"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "VZW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "BST"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "VMU"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "XAS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 669
    :cond_0
    const/4 v1, 0x1

    .line 672
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isTxtFileType(I)Z
    .locals 1
    .param p0, "fileType"    # I

    .prologue
    .line 712
    const/16 v0, 0x56

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isVideoFileType(I)Z
    .locals 1
    .param p0, "fileType"    # I

    .prologue
    .line 692
    const/16 v0, 0x1f

    if-lt p0, v0, :cond_0

    const/16 v0, 0x34

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWmFileType(I)Z
    .locals 1
    .param p0, "fileType"    # I

    .prologue
    .line 743
    const/4 v0, 0x6

    if-eq p0, v0, :cond_0

    const/16 v0, 0x23

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static needThumbnail(Ljava/lang/String;)Z
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 761
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v0

    .line 762
    .local v0, "fileType":I
    invoke-static {v0}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sec/android/app/myfiles/MediaFile;->isVideoFileType(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sec/android/app/myfiles/MediaFile;->isInstallFileType(I)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static needToCheckMimeType(Ljava/lang/String;)Z
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1162
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1164
    .local v0, "ext":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 1176
    :cond_0
    :goto_0
    return v1

    .line 1169
    :cond_1
    const-string v2, "MP4"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "3GP"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "3G2"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "ASF"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "3GPP"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "SCC"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1173
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static needToCheckMimeTypeForAudioFilterExt(Ljava/lang/String;)Z
    .locals 2
    .param p0, "ext"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1181
    if-nez p0, :cond_1

    .line 1192
    :cond_0
    :goto_0
    return v0

    .line 1186
    :cond_1
    const-string v1, "MP4_A"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "3GP_A"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "3G2_A"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "ASF_A"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "3GPP_A"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1189
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
