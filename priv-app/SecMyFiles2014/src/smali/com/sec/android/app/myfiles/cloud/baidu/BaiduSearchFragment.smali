.class public Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;
.super Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;
.source "BaiduSearchFragment.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/fragment/ISearchable;


# instance fields
.field public mAdvancedSearchResult:Landroid/widget/TextView;

.field private mLastSearchKeyowrd:Ljava/lang/String;

.field private mSavedSearchDate:Ljava/lang/String;

.field private mSavedSearchExtension:Ljava/lang/String;

.field private mSavedSearchFileType:Ljava/lang/String;

.field private mSearchNavigation:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchNavigation;

.field private mSearchView:Landroid/widget/SearchView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 54
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->startDownload(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 54
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->startDownload(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method private afterAdvanceSearch()V
    .locals 15

    .prologue
    const/4 v14, 0x4

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 349
    iget-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v8}, Landroid/widget/SearchView;->clearFocus()V

    .line 350
    iget-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSearchView:Landroid/widget/SearchView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/SearchView;->setVisibility(I)V

    .line 352
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const v9, 0x7f0f004e

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    move-object v2, v8

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 353
    .local v2, "searchFor":Landroid/widget/RelativeLayout;
    invoke-virtual {v2, v12}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 355
    iget-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v11, 0x7f0b0145

    invoke-virtual {v10, v11}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCount()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 356
    iget-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v8

    invoke-virtual {v8, v13}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 357
    iget-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v8, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/MainActivity;->invalidateOptionsMenu()V

    .line 358
    new-array v1, v14, [Z

    fill-array-data v1, :array_0

    .line 359
    .local v1, "changedItems":[Z
    iget-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getmChangedItemsForAdvanceSearch()[Z

    move-result-object v1

    .line 360
    new-array v0, v14, [Z

    fill-array-data v0, :array_1

    .line 361
    .local v0, "a":[Z
    iget-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setmChangedItemsForAdvanceSearch([Z)V

    .line 362
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const v9, 0x7f0f0050

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    move-object v7, v8

    check-cast v7, Landroid/widget/Button;

    .line 363
    .local v7, "searchForName":Landroid/widget/Button;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const v9, 0x7f0f0051

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    move-object v6, v8

    check-cast v6, Landroid/widget/Button;

    .line 364
    .local v6, "searchForLocation":Landroid/widget/Button;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const v9, 0x7f0f0052

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    move-object v5, v8

    check-cast v5, Landroid/widget/Button;

    .line 365
    .local v5, "searchForFileType":Landroid/widget/Button;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const v9, 0x7f0f0053

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    move-object v3, v8

    check-cast v3, Landroid/widget/Button;

    .line 366
    .local v3, "searchForDate":Landroid/widget/Button;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const v9, 0x7f0f0054

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    move-object v4, v8

    check-cast v4, Landroid/widget/Button;

    .line 368
    .local v4, "searchForExtension":Landroid/widget/Button;
    iget-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mLastSearchKeyowrd:Ljava/lang/String;

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 369
    invoke-virtual {v7, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 370
    iget-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mLastSearchKeyowrd:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 371
    new-instance v8, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment$2;

    invoke-direct {v8, p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment$2;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)V

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 380
    :cond_0
    aget-boolean v8, v1, v12

    if-eqz v8, :cond_1

    .line 381
    invoke-virtual {v6, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 382
    iget-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSearchFileLocation()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/CharSequence;

    invoke-virtual {v6, v8}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 383
    new-instance v8, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment$3;

    invoke-direct {v8, p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment$3;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)V

    invoke-virtual {v6, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 392
    :cond_1
    aget-boolean v8, v1, v13

    if-eqz v8, :cond_3

    .line 393
    iget-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    if-nez v8, :cond_2

    .line 394
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0b0001

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0b008d

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    .line 397
    :cond_2
    invoke-virtual {v5, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 398
    iget-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    invoke-virtual {v5, v8}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 399
    new-instance v8, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment$4;

    invoke-direct {v8, p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment$4;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)V

    invoke-virtual {v5, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 408
    :cond_3
    const/4 v8, 0x2

    aget-boolean v8, v1, v8

    if-eqz v8, :cond_4

    .line 409
    invoke-virtual {v3, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 410
    iget-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSavedSearchDate:Ljava/lang/String;

    invoke-virtual {v3, v8}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 411
    new-instance v8, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment$5;

    invoke-direct {v8, p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment$5;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)V

    invoke-virtual {v3, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 420
    :cond_4
    const/4 v8, 0x3

    aget-boolean v8, v1, v8

    if-eqz v8, :cond_5

    .line 421
    invoke-virtual {v4, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 422
    iget-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSavedSearchExtension:Ljava/lang/String;

    invoke-virtual {v4, v8}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 423
    new-instance v8, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment$6;

    invoke-direct {v8, p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment$6;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)V

    invoke-virtual {v4, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 432
    :cond_5
    return-void

    .line 358
    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 360
    :array_1
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method


# virtual methods
.method public AsyncOpenCompleted()V
    .locals 0

    .prologue
    .line 279
    return-void
.end method

.method public clean()V
    .locals 0

    .prologue
    .line 269
    return-void
.end method

.method public getOptionsMenuResId(Z)I
    .locals 1
    .param p1, "isSelectMode"    # Z

    .prologue
    .line 329
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 331
    const/4 v0, -0x1

    .line 333
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0e0016

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 92
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 93
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0f0043

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSearchView:Landroid/widget/SearchView;

    .line 98
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mAdvancedSearchResult:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mAdvancedSearchResult:Landroid/widget/TextView;

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSearchView:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mAdvancedSearchResult:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 111
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "search_option_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "search_option_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_4

    .line 113
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 115
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "search_keyword_intent_key"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->onQueryTextSubmit(Ljava/lang/String;)Z

    .line 130
    :cond_1
    :goto_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->showTreeView(Z)V

    .line 131
    return-void

    .line 108
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0f00f8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSearchView:Landroid/widget/SearchView;

    goto :goto_0

    .line 119
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "search_keyword_intent_key"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->onQueryTextSubmit(Ljava/lang/String;)Z

    goto :goto_1

    .line 126
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mLastSearchKeyowrd:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mLastSearchKeyowrd:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->onQueryTextSubmit(Ljava/lang/String;)Z

    goto :goto_1
.end method

.method public onBackPressed()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 283
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSearchNavigation:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchNavigation;->getPathStack()Ljava/util/Stack;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 285
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 287
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getmBackFragmentID()I

    move-result v1

    .line 288
    .local v1, "mBackFragment":I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 289
    .local v0, "b":Landroid/os/Bundle;
    if-nez v1, :cond_1

    .line 290
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView(Landroid/os/Bundle;)V

    .line 315
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "mBackFragment":I
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->onBackPressed()Z

    move-result v2

    :goto_0
    return v2

    .line 291
    .restart local v0    # "b":Landroid/os/Bundle;
    .restart local v1    # "mBackFragment":I
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    instance-of v2, v2, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v2, :cond_0

    .line 292
    const/16 v2, 0x201

    if-eq v1, v2, :cond_2

    const/16 v2, 0x9

    if-eq v1, v2, :cond_2

    const/16 v2, 0xa

    if-ne v1, v2, :cond_3

    .line 294
    :cond_2
    const-string v2, "FOLDERPATH"

    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentDirectory()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v2, v1, v0}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    move v2, v3

    .line 298
    goto :goto_0

    .line 303
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "mBackFragment":I
    :cond_4
    const/4 v2, 0x0

    goto :goto_0

    .line 308
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSearchNavigation:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchNavigation;->getPathStack()Ljava/util/Stack;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 309
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSearchNavigation:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchNavigation;->getPathStack()Ljava/util/Stack;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSearchView:Landroid/widget/SearchView;

    if-eqz v2, :cond_0

    .line 311
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSearchView:Landroid/widget/SearchView;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mLastSearchKeyowrd:Ljava/lang/String;

    invoke-virtual {v2, v4, v3}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    move v2, v3

    .line 312
    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 344
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 345
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->showTreeView(Z)V

    .line 346
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 67
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->onCreate(Landroid/os/Bundle;)V

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchNavigation;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchNavigation;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSearchNavigation:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchNavigation;

    .line 72
    :cond_0
    return-void
.end method

.method protected onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 160
    new-instance v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment$1;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)V

    return-object v0
.end method

.method protected onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 85
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 86
    .local v0, "view":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSearchNavigation:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchNavigation;->getEmptyCourso()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 87
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 76
    invoke-super {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->onDestroy()V

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->closeCursor()V

    .line 80
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 324
    return-void
.end method

.method public onRefresh()V
    .locals 0

    .prologue
    .line 274
    return-void
.end method

.method public search(Lcom/sec/android/app/myfiles/utils/SearchParameter;)V
    .locals 10
    .param p1, "param"    # Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .prologue
    const-wide/16 v8, 0x3e8

    const/4 v6, 0x0

    .line 135
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSearchNavigation:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchNavigation;->getPathStack()Ljava/util/Stack;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Stack;->clear()V

    .line 136
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getKeyword()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mLastSearchKeyowrd:Ljava/lang/String;

    .line 137
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v2, v2, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v2, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mSearchFromAdvance:Z

    if-eqz v2, :cond_1

    .line 138
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 139
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/EnumSet;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    .line 140
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    const/16 v4, 0x5b

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    .line 141
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    .line 143
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateFrom()J

    move-result-wide v4

    mul-long/2addr v4, v8

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/Utils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 144
    .local v0, "fromDate":[Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateTo()J

    move-result-wide v4

    mul-long/2addr v4, v8

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/Utils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 145
    .local v1, "toDate":[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v3, v0, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  -  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSavedSearchDate:Ljava/lang/String;

    .line 146
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getExtentionString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mSavedSearchExtension:Ljava/lang/String;

    .line 148
    .end local v0    # "fromDate":[Ljava/lang/String;
    .end local v1    # "toDate":[Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v2, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mLastSearchKeyowrd:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->setSearchKeyword(Ljava/lang/String;)V

    .line 149
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v2, p1}, Lcom/sec/android/app/myfiles/utils/Utils;->getDropBoxFilteredContent(Landroid/content/Context;Lcom/sec/android/app/myfiles/utils/SearchParameter;)Landroid/database/Cursor;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 150
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v2, v2, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v2, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mSearchFromAdvance:Z

    if-eqz v2, :cond_2

    .line 151
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->afterAdvanceSearch()V

    .line 153
    :cond_2
    return-void
.end method

.method public setSearchKeyword(Ljava/lang/String;)V
    .locals 0
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 338
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mLastSearchKeyowrd:Ljava/lang/String;

    .line 340
    return-void
.end method
