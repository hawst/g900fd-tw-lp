.class public Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;
.super Landroid/os/AsyncTask;
.source "FolderHoverPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GetThumbnailTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;",
        "Ljava/lang/Void;",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field private mFileName:Landroid/widget/TextView;

.field private mIconImageView:Landroid/widget/ImageView;

.field private mIsVideoFile:Z

.field private mLoadThumbSuccess:Z

.field private mOverlayIcon:Landroid/widget/ImageView;

.field private mThumbImageView:Landroid/widget/ImageView;

.field final synthetic this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V
    .locals 1
    .param p2, "thumbImageView"    # Landroid/widget/ImageView;
    .param p3, "fileIcon"    # Landroid/widget/ImageView;
    .param p4, "overlayIcon"    # Landroid/widget/ImageView;
    .param p5, "fileName"    # Landroid/widget/TextView;
    .param p6, "isVideo"    # Z

    .prologue
    .line 758
    iput-object p1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 756
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mIsVideoFile:Z

    .line 760
    iput-object p2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mThumbImageView:Landroid/widget/ImageView;

    .line 762
    iput-object p3, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mIconImageView:Landroid/widget/ImageView;

    .line 764
    iput-object p5, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mFileName:Landroid/widget/TextView;

    .line 766
    iput-object p4, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mOverlayIcon:Landroid/widget/ImageView;

    .line 768
    iput-boolean p6, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mIsVideoFile:Z

    .line 769
    return-void
.end method

.method private setThumbnailVisibility(I)V
    .locals 3
    .param p1, "visibility"    # I

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 872
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mThumbImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mIconImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mFileName:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 874
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mThumbImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 876
    if-nez p1, :cond_1

    .line 878
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mIconImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 879
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mFileName:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 887
    :cond_0
    :goto_0
    return-void

    .line 883
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mIconImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 884
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mFileName:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;)Landroid/graphics/drawable/Drawable;
    .locals 8
    .param p1, "item"    # [Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 774
    const/4 v1, -0x1

    .line 776
    .local v1, "rsrcId":I
    aget-object v3, p1, v6

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mFilePath:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->access$700(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getThumbnailDrawableWithoutMakeCache(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 778
    .local v2, "taskDrawable":Landroid/graphics/drawable/Drawable;
    if-nez v2, :cond_0

    .line 780
    aget-object v3, p1, v6

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mFilePath:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->access$700(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3, v4}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    .line 782
    .local v0, "fileType":I
    iget-object v3, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    iget v4, v4, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mCategoryType:I

    aget-object v5, p1, v6

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mFilePath:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->access$700(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v0, v5}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getThumbnailDrawableWithMakeCache(Landroid/content/Context;IILjava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 785
    .end local v0    # "fileType":I
    :cond_0
    aget-object v3, p1, v6

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->isVidoeFile()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 788
    iput-boolean v7, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mLoadThumbSuccess:Z

    .line 790
    if-nez v2, :cond_1

    .line 792
    const-string v3, "FolderHoverPopup"

    const-string v4, "No thumbnail, getting from resources"

    invoke-static {v6, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 798
    const v1, 0x7f0200c2

    .line 800
    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mLoadThumbSuccess:Z

    .line 842
    :cond_1
    :goto_0
    if-nez v2, :cond_2

    const/4 v3, -0x1

    if-eq v1, v3, :cond_2

    .line 844
    iget-object v3, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 847
    :cond_2
    return-object v2

    .line 803
    :cond_3
    aget-object v3, p1, v6

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->isImageFile()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 806
    iput-boolean v7, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mLoadThumbSuccess:Z

    .line 808
    if-nez v2, :cond_1

    .line 810
    const-string v3, "FolderHoverPopup"

    const-string v4, "No thumbnail, getting from resources"

    invoke-static {v6, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 816
    const v1, 0x7f0200b4

    .line 818
    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mLoadThumbSuccess:Z

    goto :goto_0

    .line 821
    :cond_4
    aget-object v3, p1, v6

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->isAudioFile()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 824
    iput-boolean v7, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mLoadThumbSuccess:Z

    .line 826
    if-nez v2, :cond_1

    .line 828
    const-string v3, "FolderHoverPopup"

    const-string v4, "No thumbnail, getting from resources"

    invoke-static {v6, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 831
    const v1, 0x7f0200aa

    .line 833
    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mLoadThumbSuccess:Z

    goto :goto_0

    .line 838
    :cond_5
    const/4 v2, 0x0

    .line 839
    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mLoadThumbSuccess:Z

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 744
    check-cast p1, [Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->doInBackground([Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1, "thumb"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 852
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 854
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mLoadThumbSuccess:Z

    if-eqz v0, :cond_1

    .line 856
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->setThumbnailVisibility(I)V

    .line 858
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mThumbImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 860
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mIsVideoFile:Z

    if-eqz v0, :cond_0

    .line 862
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->mOverlayIcon:Landroid/widget/ImageView;

    const v1, 0x7f020072

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 868
    :cond_0
    :goto_0
    return-void

    .line 866
    :cond_1
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->setThumbnailVisibility(I)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 744
    check-cast p1, Landroid/graphics/drawable/Drawable;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->onPostExecute(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
