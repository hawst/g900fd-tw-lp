.class Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$2;
.super Landroid/content/BroadcastReceiver;
.source "CategoryBrowserFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->setScanFileReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x7

    const/4 v4, 0x0

    .line 182
    const/4 v0, 0x2

    const-string v1, "CategoryBrowserFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Broadcast receive : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 184
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 210
    :cond_1
    :goto_0
    return-void

    .line 192
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->showWaitProgressDialog(Z)V

    .line 196
    const-string v0, "CategoryBrowserFragment"

    const-string v1, "scan finished, hide dialog"

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 198
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mFragmentId:I

    if-ne v0, v5, :cond_1

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isMountedStateCheck(Landroid/content/Context;)V

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->onRefresh()V

    goto :goto_0

    .line 203
    :cond_4
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mFragmentId:I

    if-ne v0, v5, :cond_1

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isMountedStateCheck(Landroid/content/Context;)V

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->onRefresh()V

    goto :goto_0
.end method
