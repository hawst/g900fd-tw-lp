.class public Lcom/sec/android/app/myfiles/ftp/operation/CreateDirectoryOnFtpOperation;
.super Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;
.source "CreateDirectoryOnFtpOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/ftp/operation/CreateDirectoryOnFtpOperation$1;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "operation"    # I
    .param p3, "contentType"    # I
    .param p4, "progressView"    # Landroid/view/View;
    .param p5, "ftpHandlerParams"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;)V

    .line 37
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;
    .locals 4
    .param p1, "args"    # [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .prologue
    const/4 v3, 0x0

    .line 49
    if-eqz p1, :cond_2

    array-length v2, p1

    if-lez v2, :cond_2

    aget-object v2, p1, v3

    if-eqz v2, :cond_2

    aget-object v2, p1, v3

    iget-object v2, v2, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 51
    aget-object v2, p1, v3

    iget-object v0, v2, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    .line 53
    .local v0, "newDir":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CreateDirectoryOnFtpOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v2}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncPrepareConnection()Z

    .line 55
    const/4 v1, 0x0

    .line 57
    .local v1, "success":Z
    sget-object v2, Lcom/sec/android/app/myfiles/ftp/FTPErr;->CREATE_FOLDER_FAILED:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CreateDirectoryOnFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 59
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CreateDirectoryOnFtpOperation;->isCancelled:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CreateDirectoryOnFtpOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v2}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->isReady()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 61
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CreateDirectoryOnFtpOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v2, v0}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncCreateFolder(Ljava/lang/String;)Z

    move-result v1

    .line 65
    :cond_0
    if-eqz v1, :cond_1

    .line 67
    sget-object v2, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CreateDirectoryOnFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 77
    .end local v0    # "newDir":Ljava/lang/String;
    .end local v1    # "success":Z
    :cond_1
    :goto_0
    const/4 v2, 0x0

    return-object v2

    .line 73
    :cond_2
    sget-object v2, Lcom/sec/android/app/myfiles/ftp/FTPErr;->WRONG_ARGUMENT:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CreateDirectoryOnFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 30
    check-cast p1, [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/CreateDirectoryOnFtpOperation;->doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V
    .locals 4
    .param p1, "result"    # Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .prologue
    const/4 v3, 0x1

    .line 84
    sget-object v1, Lcom/sec/android/app/myfiles/ftp/operation/CreateDirectoryOnFtpOperation$1;->$SwitchMap$com$sec$android$app$myfiles$ftp$FTPErr:[I

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CreateDirectoryOnFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPErr;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 102
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CreateDirectoryOnFtpOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    if-eqz v1, :cond_0

    .line 104
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CreateDirectoryOnFtpOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;->onCancelled(Ljava/util/ArrayList;I)V

    .line 108
    :cond_0
    const v1, 0x7f0b009e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/CreateDirectoryOnFtpOperation;->showToast(I)V

    .line 114
    :cond_1
    :goto_0
    return-void

    .line 88
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CreateDirectoryOnFtpOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    if-eqz v1, :cond_1

    .line 90
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 92
    .local v0, "arg":Landroid/os/Bundle;
    const-string v1, "FILE_OPERATION"

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 94
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CreateDirectoryOnFtpOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    invoke-interface {v1, v3, v3, v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;->onCompleted(IILandroid/os/Bundle;)V

    goto :goto_0

    .line 84
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 30
    check-cast p1, Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/CreateDirectoryOnFtpOperation;->onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    return-void
.end method

.method protected setupProgressView()V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/CreateDirectoryOnFtpOperation;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 44
    return-void
.end method
