.class Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$2;
.super Ljava/lang/Object;
.source "AllFileBrowserNavigation.java"

# interfaces
.implements Ljava/io/FileFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->getFilesInPath(Ljava/lang/String;III)Landroid/database/Cursor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;)V
    .locals 0

    .prologue
    .line 762
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$2;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Ljava/io/File;)Z
    .locals 7
    .param p1, "pathname"    # Ljava/io/File;

    .prologue
    .line 767
    const/4 v2, 0x0

    .line 769
    .local v2, "result":Z
    const/4 v4, 0x0

    .line 771
    .local v4, "showFileByMimeType":Z
    const/4 v3, 0x0

    .line 773
    .local v3, "showFileByExtension":Z
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 775
    const/4 v2, 0x1

    .line 841
    :cond_0
    :goto_0
    if-nez v2, :cond_1

    if-nez v3, :cond_1

    if-eqz v4, :cond_9

    :cond_1
    const/4 v5, 0x1

    :goto_1
    return v5

    .line 779
    :cond_2
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 781
    .local v0, "extension":Ljava/lang/String;
    const/16 v5, 0x2e

    invoke-virtual {v0, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v5, v6}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 784
    const-string v5, "SCC"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 786
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$2;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mContext:Landroid/content/Context;

    invoke-static {v5, v6}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeTypeFromMediaStore(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 789
    .local v1, "mimetype":Ljava/lang/String;
    if-eqz v1, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$2;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mMimetypeForFilter:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    move-result v5

    if-lez v5, :cond_3

    .line 791
    iget-object v5, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$2;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mMimetypeForFilter:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    .line 795
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$2;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    move-result v5

    if-lez v5, :cond_4

    .line 797
    iget-object v5, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$2;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 801
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$2;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mExceptFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    move-result v5

    if-lez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$2;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mExceptFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 804
    const/4 v3, 0x0

    goto :goto_0

    .line 809
    .end local v1    # "mimetype":Ljava/lang/String;
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$2;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    move-result v5

    if-lez v5, :cond_7

    .line 811
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/myfiles/MediaFile;->needToCheckMimeType(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 813
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$2;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mContext:Landroid/content/Context;

    invoke-static {v5, v6}, Lcom/sec/android/app/myfiles/MediaFile;->isAudioInMediaStore(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 816
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_A"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 820
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$2;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 824
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$2;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mMimetypeForFilter:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    move-result v5

    if-lez v5, :cond_8

    .line 826
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 829
    .restart local v1    # "mimetype":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$2;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mMimetypeForFilter:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    .line 833
    .end local v1    # "mimetype":Ljava/lang/String;
    :cond_8
    iget-object v5, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$2;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mExceptFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    move-result v5

    if-lez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$2;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mExceptFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 836
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 841
    .end local v0    # "extension":Ljava/lang/String;
    :cond_9
    const/4 v5, 0x0

    goto/16 :goto_1
.end method
