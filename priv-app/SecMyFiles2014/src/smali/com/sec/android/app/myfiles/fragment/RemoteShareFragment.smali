.class public Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;
.source "RemoteShareFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$OnDownloadedListener;
    }
.end annotation


# static fields
.field public static final ACTION_REMOTE_SHARE_DOWNLOAD_MANAGER:Ljava/lang/String; = "com.sec.rshareshell.ACTION_DOWNLOAD"

.field public static final EXTRA_CONTENT_ID_LIST:Ljava/lang/String; = "extra_content_id_list"

.field public static final EXTRA_MEDIA_URI:Ljava/lang/String; = "extra_media_uri"

.field private static final MODULE:Ljava/lang/String; = "RemoteShareFragment"

.field public static final RESULT_CANCEL:I = 0x5

.field public static final RESULT_ERROR:I = 0x3

.field public static final RESULT_PAUSE:I = 0x4

.field public static final RESULT_PROGRESS:I = 0x2

.field public static final RESULT_START:I = 0x1

.field public static final RESULT_SUCCESS:I = 0x0

.field public static final TOKEN_REQ_DOWNLOAD:I = 0x7d1

.field private static final WIFI_ONLY_FILE_SIZE_LIMIT:J = 0x2faf080L


# instance fields
.field private mActionBarSubtitleText:Ljava/lang/String;

.field private mCurrentFolder:Ljava/lang/String;

.field private mCurrentFolderExpireDate:Ljava/lang/String;

.field private mFromSelector:Z

.field private mIsFromNotification:Z

.field private mOnDownloadedListener:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$OnDownloadedListener;

.field private mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

.field private mRealViewMode:I

.field private mRemoteSharePathIndicator:Ljava/lang/String;

.field private mRemoteShareRoot:Ljava/lang/String;

.field private mSelectableItemsCount:I

.field private mStartFolder:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 94
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;-><init>()V

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRemoteSharePathIndicator:Ljava/lang/String;

    .line 65
    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRemoteShareRoot:Ljava/lang/String;

    .line 66
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mFromSelector:Z

    .line 69
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRealViewMode:I

    .line 70
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mIsFromNotification:Z

    .line 752
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$7;-><init>(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

    .line 95
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRemoteShareRoot:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRemoteShareRoot:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRemoteSharePathIndicator:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRemoteSharePathIndicator:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mCurrentFolderExpireDate:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mCurrentFolderExpireDate:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mActionBarSubtitleText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mActionBarSubtitleText:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->menuDownload(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->backToRealViewMode()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;Landroid/net/Uri;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->sendOpenFileIntent(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$702(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$OnDownloadedListener;)Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$OnDownloadedListener;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;
    .param p1, "x1"    # Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$OnDownloadedListener;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mOnDownloadedListener:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$OnDownloadedListener;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->sendOpenFileIntent(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mSelectableItemsCount:I

    return v0
.end method

.method private backToRealViewMode()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 621
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRealViewMode:I

    if-eq v0, v2, :cond_1

    .line 622
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRealViewMode:I

    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mViewMode:I

    if-eq v0, v1, :cond_0

    .line 623
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 624
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRealViewMode:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->changeViewMode(I)V

    .line 626
    :cond_0
    iput v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRealViewMode:I

    .line 628
    :cond_1
    return-void
.end method

.method private menuDownload(Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 456
    .local p1, "selItemsInt":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v6, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isSelectMode()Z

    move-result v6

    if-nez v6, :cond_1

    if-nez p1, :cond_1

    .line 485
    :cond_0
    :goto_0
    return-void

    .line 459
    :cond_1
    if-nez p1, :cond_3

    .line 460
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v0

    .line 461
    .local v0, "count":I
    if-lez v0, :cond_0

    .line 462
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mOnDownloadedListener:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$OnDownloadedListener;

    .line 463
    new-instance p1, Ljava/util/ArrayList;

    .end local p1    # "selItemsInt":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 464
    .restart local p1    # "selItemsInt":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v5

    .line 465
    .local v5, "selItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 466
    .local v4, "o":Ljava/lang/Object;
    check-cast v4, Ljava/lang/Integer;

    .end local v4    # "o":Ljava/lang/Object;
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 468
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->finishSelectMode()V

    .line 474
    .end local v0    # "count":I
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v5    # "selItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_3
    sget-object v6, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->CONTENT_URI:Landroid/net/Uri;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 475
    .local v3, "mediaUri":Landroid/net/Uri;
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v7, 0x1

    if-le v6, v7, :cond_4

    .line 476
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v6, :cond_0

    .line 477
    new-instance v2, Landroid/content/Intent;

    const-string v6, "com.sec.rshareshell.ACTION_DOWNLOAD"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 478
    .local v2, "intent":Landroid/content/Intent;
    const-string v6, "extra_media_uri"

    invoke-virtual {v2, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 479
    const-string v6, "extra_content_id_list"

    invoke-virtual {v2, v6, p1}, Landroid/content/Intent;->putIntegerArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 480
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6, v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 483
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_4
    const/4 v6, 0x0

    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v7

    const-string v8, ""

    const/16 v9, 0x1b

    invoke-virtual {p0, v6, v7, v8, v9}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->startRemoteShareDownload(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private sendOpenFileIntent(Landroid/net/Uri;)V
    .locals 5
    .param p1, "fileUri"    # Landroid/net/Uri;

    .prologue
    .line 612
    if-eqz p1, :cond_0

    .line 613
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 614
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 615
    const/16 v1, 0x201

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->getCurrentSortBy()I

    move-result v4

    invoke-static {v1, v0, v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/FileUtils;->openFile(ILjava/io/File;Landroid/app/Activity;ZI)V

    .line 618
    .end local v0    # "file":Ljava/io/File;
    :cond_0
    return-void
.end method

.method private sendOpenFileIntent(Ljava/lang/String;)V
    .locals 3
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 607
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/myfiles/utils/Constant;->RSHARE_DOWNLOAD_DIR_URI:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 608
    .local v0, "fileUri":Landroid/net/Uri;
    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->sendOpenFileIntent(Landroid/net/Uri;)V

    .line 609
    return-void
.end method

.method private showDownloadLargeFilesDialog(Ljava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "selItemsInt":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    .line 426
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-nez v5, :cond_0

    .line 453
    :goto_0
    return-void

    .line 430
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v0, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 431
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 432
    .local v4, "res":Landroid/content/res/Resources;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v6, 0x7f040050

    invoke-static {v5, v6, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 433
    .local v1, "checkBoxView":Landroid/view/View;
    if-eqz v1, :cond_1

    const v5, 0x7f0f011b

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    move-object v2, v5

    .line 434
    .local v2, "checkbox":Landroid/widget/CheckBox;
    :cond_1
    const v5, 0x7f0b000b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 435
    const v5, 0x7f0b000d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f0b000e

    new-instance v7, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$3;

    invoke-direct {v7, p0, v2, p1}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;Landroid/widget/CheckBox;Ljava/util/ArrayList;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f0b0017

    new-instance v7, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$2;

    invoke-direct {v7, p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 451
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    .line 452
    .local v3, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method


# virtual methods
.method public AsyncOpenCompleted()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const v7, 0x7f0b000a

    const v6, 0x7f0b0009

    .line 769
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mCurrentFolder:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mCurrentFolder:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 770
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v3, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->setFolderExpiredDate(J)V

    .line 772
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mCurrentFolder:Ljava/lang/String;

    .line 773
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v1

    .line 774
    .local v1, "c":Landroid/database/Cursor;
    if-eqz v1, :cond_2

    .line 775
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 777
    :cond_2
    const/4 v0, 0x0

    .line 778
    .local v0, "actionBar":Landroid/app/ActionBar;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v3, :cond_3

    .line 779
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 781
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_c

    .line 782
    const/4 v2, 0x0

    .line 783
    .local v2, "res":Landroid/content/res/Resources;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v3, :cond_4

    .line 784
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 786
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    instance-of v3, v3, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v3, :cond_6

    .line 787
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v3, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->getMessageFolder()I

    move-result v3

    const/16 v4, 0x27

    if-ne v3, v4, :cond_a

    .line 788
    if-eqz v2, :cond_5

    .line 789
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRemoteShareRoot:Ljava/lang/String;

    .line 791
    :cond_5
    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 792
    invoke-virtual {v0, v7}, Landroid/app/ActionBar;->setTitle(I)V

    .line 803
    :cond_6
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRemoteShareRoot:Ljava/lang/String;

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRemoteSharePathIndicator:Ljava/lang/String;

    .line 804
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    if-eqz v3, :cond_7

    .line 805
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRemoteSharePathIndicator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 807
    :cond_7
    if-eqz v0, :cond_8

    if-eqz v2, :cond_8

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 808
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f0b0070

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 816
    .end local v2    # "res":Landroid/content/res/Resources;
    :cond_8
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v3, :cond_9

    .line 817
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->invalidateOptionsMenu()V

    .line 819
    :cond_9
    return-void

    .line 795
    .restart local v2    # "res":Landroid/content/res/Resources;
    :cond_a
    if-eqz v2, :cond_b

    .line 796
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRemoteShareRoot:Ljava/lang/String;

    .line 798
    :cond_b
    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 799
    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setTitle(I)V

    goto :goto_0

    .line 811
    .end local v2    # "res":Landroid/content/res/Resources;
    :cond_c
    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 812
    invoke-virtual {v0, v8}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 813
    invoke-virtual {v0, v8}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public addCurrentFolderToShortcut()V
    .locals 0

    .prologue
    .line 593
    return-void
.end method

.method protected changeViewMode(I)V
    .locals 4
    .param p1, "mode"    # I

    .prologue
    .line 249
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mActionBarSubtitleText:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 252
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 254
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_0

    .line 256
    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mCurrentFolderExpireDate:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 258
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mActionBarSubtitleText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0011

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mCurrentFolderExpireDate:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 269
    .end local v0    # "actionBar":Landroid/app/ActionBar;
    :cond_0
    :goto_0
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRealViewMode:I

    .line 270
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->changeViewMode(I)V

    .line 271
    return-void

    .line 264
    .restart local v0    # "actionBar":Landroid/app/ActionBar;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mActionBarSubtitleText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public getOptionsMenuResId(Z)I
    .locals 1
    .param p1, "isSelectMode"    # Z

    .prologue
    .line 597
    const v0, 0x7f0e0015

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 562
    packed-switch p1, :pswitch_data_0

    .line 572
    :cond_0
    :goto_0
    return-void

    .line 564
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 565
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->refreshNavigation()V

    .line 566
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mOnDownloadedListener:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$OnDownloadedListener;

    if-eqz v0, :cond_0

    .line 567
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mOnDownloadedListener:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$OnDownloadedListener;

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$OnDownloadedListener;->onDownloaded()V

    goto :goto_0

    .line 562
    :pswitch_data_0
    .packed-switch 0x1b
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 315
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isSelectMode()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 316
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->finishSelectMode()V

    .line 345
    :cond_0
    :goto_0
    return v1

    .line 320
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 322
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 324
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "remote_share_viewer"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 326
    const-string v3, "remote_share_viewer"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 328
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->finish()V

    .line 332
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->backToRealViewMode()V

    .line 333
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goUp()Z

    move-result v3

    if-nez v3, :cond_0

    .line 336
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->getSelectModeFrom()I

    move-result v3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->getSelectModeFrom()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->getSelectModeFrom()I

    move-result v3

    if-nez v3, :cond_5

    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v3, :cond_5

    .line 340
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    goto :goto_0

    .line 342
    :cond_5
    const-string v1, "RemoteShareFragment"

    const-string v3, " failed to go up"

    invoke-static {v2, v1, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 343
    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, -0x1

    .line 100
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreate(Landroid/os/Bundle;)V

    .line 102
    const/16 v2, 0x25

    iput v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mCategoryType:I

    .line 104
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 106
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .line 109
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v2, p0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setSupportAsync(Lcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    .line 111
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mSelectedItemChangeListener:Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    if-eqz v2, :cond_1

    .line 113
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mSelectedItemChangeListener:Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->setSelectedItemChangeListener(Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;)V

    .line 116
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 117
    .local v0, "argument":Landroid/os/Bundle;
    if-eqz v0, :cond_2

    .line 118
    const-string v2, "REMOTE_SHARE_ID"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mStartFolder:Ljava/lang/String;

    .line 119
    const-string v2, "REMOTE_SHARE_DIR"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 120
    .local v1, "dir":I
    if-eq v1, v4, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    instance-of v2, v2, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    if-eqz v2, :cond_2

    .line 121
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v2, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->setMessageFolder(I)V

    .line 124
    .end local v1    # "dir":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    instance-of v2, v2, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    if-eqz v2, :cond_3

    .line 125
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v2, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    new-instance v3, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->setOnFolderInfoReceivedListener(Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$FolderInfoReceiverListener;)V

    .line 226
    :cond_3
    return-void
.end method

.method protected onCreateDetailFragmentListener()Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 913
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareDetailFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareDetailFragment;-><init>()V

    return-object v0
.end method

.method protected onCreateFileOperationListener()Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;
    .locals 1

    .prologue
    .line 738
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$6;-><init>(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)V

    return-object v0
.end method

.method protected onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 633
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;-><init>(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)V

    return-object v0
.end method

.method protected onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1

    .prologue
    .line 716
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$5;-><init>(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)V

    return-object v0
.end method

.method protected onCreateMultipleDetailFragmentListener()Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 918
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareMultipleDetailFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareMultipleDetailFragment;-><init>()V

    return-object v0
.end method

.method public onCreateSelectedItemChangeListener()Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;
    .locals 1

    .prologue
    .line 835
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$8;-><init>(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 231
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 232
    .local v0, "view":Landroid/view/View;
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mFromSelector:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->getSelectionType()I

    move-result v1

    if-le v1, v4, :cond_0

    .line 233
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mCreateFolderContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 235
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setOnPathChangeListener(Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;)V

    .line 236
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mStartFolder:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mStartFolder:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 237
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mIsFromNotification:Z

    .line 238
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mStartFolder:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    .line 239
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mStartFolder:Ljava/lang/String;

    .line 243
    :goto_0
    return-object v0

    .line 241
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goToRoot(Ljava/lang/Boolean;)Z

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 351
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v1, :cond_0

    .line 352
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 353
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_0

    .line 354
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 355
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 358
    .end local v0    # "actionBar":Landroid/app/ActionBar;
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onDestroy()V

    .line 359
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v1, :cond_1

    .line 360
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->closeCursor()V

    .line 362
    :cond_1
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 12
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 490
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    .line 556
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v7

    :goto_0
    return v7

    .line 492
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isSelectMode()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 493
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    instance-of v7, v7, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;

    if-nez v7, :cond_1

    .line 494
    :cond_0
    const/4 v7, 0x0

    goto :goto_0

    .line 496
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_2

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v7, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->getSelectedItemsSize()J

    move-result-wide v8

    const-wide/32 v10, 0x2faf080

    cmp-long v7, v8, v10

    if-ltz v7, :cond_2

    .line 497
    const/4 v7, 0x0

    invoke-direct {p0, v7}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->showDownloadLargeFilesDialog(Ljava/util/ArrayList;)V

    .line 504
    :goto_1
    const/4 v7, 0x1

    goto :goto_0

    .line 499
    :cond_2
    const/4 v7, 0x0

    invoke-direct {p0, v7}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->menuDownload(Ljava/util/ArrayList;)V

    goto :goto_1

    .line 502
    :cond_3
    const/4 v7, 0x1

    const/4 v8, -0x1

    const/4 v9, 0x0

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->startSelectMode(III)V

    goto :goto_1

    .line 507
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isSelectMode()Z

    move-result v7

    if-nez v7, :cond_7

    .line 508
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v1

    .line 509
    .local v1, "cursor":Landroid/database/Cursor;
    if-eqz v1, :cond_7

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 510
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 511
    .local v5, "selItemsInt":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-wide/16 v2, 0x0

    .line 513
    .local v2, "filesSize":J
    :cond_4
    const-string v7, "_id"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 514
    .local v0, "colInd":I
    const-string v7, "file_size"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 515
    .local v6, "sizeColId":I
    const/4 v7, -0x1

    if-eq v0, v7, :cond_5

    .line 516
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 518
    :cond_5
    const/4 v7, -0x1

    if-eq v6, v7, :cond_6

    .line 519
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    add-long/2addr v2, v8

    .line 521
    :cond_6
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-nez v7, :cond_4

    .line 522
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_8

    const-wide/32 v8, 0x2faf080

    cmp-long v7, v2, v8

    if-ltz v7, :cond_8

    .line 523
    invoke-direct {p0, v5}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->showDownloadLargeFilesDialog(Ljava/util/ArrayList;)V

    .line 529
    .end local v0    # "colInd":I
    .end local v1    # "cursor":Landroid/database/Cursor;
    .end local v2    # "filesSize":J
    .end local v5    # "selItemsInt":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v6    # "sizeColId":I
    :cond_7
    :goto_2
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 525
    .restart local v0    # "colInd":I
    .restart local v1    # "cursor":Landroid/database/Cursor;
    .restart local v2    # "filesSize":J
    .restart local v5    # "selItemsInt":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v6    # "sizeColId":I
    :cond_8
    invoke-direct {p0, v5}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->menuDownload(Ljava/util/ArrayList;)V

    goto :goto_2

    .line 532
    .end local v0    # "colInd":I
    .end local v1    # "cursor":Landroid/database/Cursor;
    .end local v2    # "filesSize":J
    .end local v5    # "selItemsInt":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v6    # "sizeColId":I
    :sswitch_2
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v7, :cond_9

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    if-eqz v7, :cond_9

    .line 534
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 536
    .local v4, "intent":Landroid/content/Intent;
    const-string v7, "remote_share_viewer"

    const/4 v8, 0x0

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 538
    const-string v7, "remote_share_viewer"

    invoke-virtual {v4, v7}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 542
    .end local v4    # "intent":Landroid/content/Intent;
    :cond_9
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->backToRealViewMode()V

    .line 543
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v7, :cond_a

    .line 544
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    .line 545
    :cond_a
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 548
    :sswitch_3
    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->showDialog(I)V

    .line 549
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 552
    :sswitch_4
    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->showDetail(Ljava/lang/String;)V

    .line 553
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 490
    :sswitch_data_0
    .sparse-switch
        0x7f0f0130 -> :sswitch_4
        0x7f0f0131 -> :sswitch_0
        0x7f0f013b -> :sswitch_3
        0x7f0f0149 -> :sswitch_2
        0x7f0f014a -> :sswitch_1
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 6
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 367
    const/4 v2, 0x0

    .line 368
    .local v2, "menuItem":Landroid/view/MenuItem;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_8

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isSelectMode()Z

    move-result v3

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    instance-of v3, v3, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v3, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->getSelectableItemsCount()I

    move-result v3

    if-lez v3, :cond_8

    move v0, v4

    .line 371
    .local v0, "downloadAvailable":Z
    :goto_0
    const v3, 0x7f0f0131

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 372
    if-eqz v2, :cond_1

    .line 373
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isSelectMode()Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v3

    if-lez v3, :cond_9

    :cond_0
    move v3, v4

    :goto_1
    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 376
    :cond_1
    const v3, 0x7f0f014a

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 377
    if-eqz v2, :cond_2

    .line 378
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 381
    :cond_2
    const v3, 0x7f0f0130

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 382
    if-eqz v2, :cond_4

    .line 383
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isSelectMode()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isSelectMode()Z

    move-result v3

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v3

    if-nez v3, :cond_a

    .line 385
    :cond_3
    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 391
    :cond_4
    :goto_2
    const v3, 0x7f0f0149

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 392
    if-eqz v2, :cond_5

    .line 393
    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 396
    :cond_5
    const v3, 0x7f0f013c

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 397
    if-eqz v2, :cond_6

    .line 398
    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 401
    :cond_6
    const v3, 0x7f0f013b

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 403
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v3, :cond_b

    .line 405
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 407
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_b

    const-string v3, "remote_share_viewer"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isRemoteShareEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 410
    if-eqz v2, :cond_b

    .line 412
    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 422
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_7
    :goto_3
    return-void

    .end local v0    # "downloadAvailable":Z
    :cond_8
    move v0, v5

    .line 368
    goto/16 :goto_0

    .restart local v0    # "downloadAvailable":Z
    :cond_9
    move v3, v5

    .line 373
    goto :goto_1

    .line 386
    :cond_a
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isSelectMode()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 387
    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_2

    .line 419
    :cond_b
    if-eqz v2, :cond_7

    .line 420
    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_3
.end method

.method public onRefresh()V
    .locals 3

    .prologue
    .line 309
    const/4 v0, 0x0

    const-string v1, "RemoteShareFragment"

    const-string v2, "refresh"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 310
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 302
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onResume()V

    .line 303
    const/4 v0, 0x0

    const-string v1, "RemoteShareFragment"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 304
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->showPathIndicator(Z)V

    .line 305
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x2

    .line 275
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 276
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 277
    .local v0, "argument":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    const-string v2, "run_from"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const/16 v3, 0x15

    if-ne v2, v3, :cond_0

    .line 278
    const-string v2, "selection_type"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    const/4 v4, 0x1

    invoke-virtual {p0, v2, v3, v4}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->startSelectMode(III)V

    .line 280
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mIsFromNotification:Z

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mViewMode:I

    if-eq v2, v5, :cond_1

    .line 281
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mViewMode:I

    .line 282
    .local v1, "initialViewMode":I
    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->changeViewMode(I)V

    .line 283
    iput v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRealViewMode:I

    .line 285
    .end local v1    # "initialViewMode":I
    :cond_1
    return-void
.end method

.method protected selectAllItem()V
    .locals 1

    .prologue
    .line 576
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->selectAllItem()V

    .line 577
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 578
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->selectAllItem()V

    .line 580
    :cond_0
    return-void
.end method

.method public selectmodeActionbar()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 923
    const/4 v0, 0x0

    .line 924
    .local v0, "actionBar":Landroid/app/ActionBar;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v2, :cond_0

    .line 925
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 927
    :cond_0
    if-eqz v0, :cond_1

    .line 928
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 929
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 930
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 931
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 932
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const/high16 v3, 0x7f040000

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 933
    .local v1, "customview":Landroid/view/View;
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 934
    const v2, 0x7f0f0003

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mSelectAllButton:Landroid/widget/TextView;

    .line 936
    .end local v1    # "customview":Landroid/view/View;
    :cond_1
    return-void
.end method

.method public setMainActionBar()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 289
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v1, :cond_0

    .line 290
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 291
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_0

    .line 292
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 293
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 294
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 295
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 298
    .end local v0    # "actionBar":Landroid/app/ActionBar;
    :cond_0
    return-void
.end method

.method protected showDetail(Ljava/lang/String;)V
    .locals 22
    .param p1, "nearbyPath"    # Ljava/lang/String;

    .prologue
    .line 941
    const/16 v16, 0x0

    .line 943
    .local v16, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v16

    .line 945
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v19

    if-gtz v19, :cond_1

    .line 1076
    :cond_0
    :goto_0
    return-void

    .line 949
    :cond_1
    const/4 v15, 0x0

    .line 951
    .local v15, "selItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemPosition()Ljava/util/ArrayList;

    move-result-object v15

    .line 953
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v14

    .line 955
    .local v14, "position":I
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_5

    .line 957
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v20, v0

    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v19

    move-object/from16 v0, v20

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/database/Cursor;

    .line 959
    .local v3, "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->onCreateDetailFragmentListener()Landroid/app/DialogFragment;

    move-result-object v4

    .line 961
    .local v4, "detail":Landroid/app/DialogFragment;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 963
    .local v2, "argument":Landroid/os/Bundle;
    const-string v19, "local_file"

    move-object/from16 v0, v19

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 965
    .local v6, "filePath":Ljava/lang/String;
    if-eqz v6, :cond_3

    .line 967
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v18

    .line 969
    .local v18, "uri":Landroid/net/Uri;
    invoke-virtual/range {v18 .. v18}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v6

    .line 993
    .end local v18    # "uri":Landroid/net/Uri;
    :cond_2
    :goto_1
    const-string v19, "detail_item_path"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 995
    const-string v19, "detail_item_index"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0, v14}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 997
    if-eqz v4, :cond_0

    .line 999
    invoke-virtual {v4, v2}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 1001
    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 1003
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v19

    const-string v20, "detail"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v4, v0, v1}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 973
    :cond_3
    const/16 v17, 0x0

    .line 975
    .local v17, "startDownload":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    instance-of v0, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;

    move/from16 v19, v0

    if-eqz v19, :cond_4

    .line 977
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v19, v0

    check-cast v19, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->isAvailableForDownload(I)Z

    move-result v17

    .line 980
    :cond_4
    if-nez v17, :cond_2

    .line 982
    const-string v19, "status"

    move-object/from16 v0, v19

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 984
    .local v8, "fileStatus":I
    const/16 v19, 0x3

    move/from16 v0, v19

    if-ne v8, v0, :cond_2

    .line 986
    const-string v19, "filename"

    move-object/from16 v0, v19

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 988
    .local v5, "fileName":Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v20, Lcom/sec/android/app/myfiles/utils/Constant;->RSHARE_DOWNLOAD_DIR:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    sget-object v20, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_1

    .line 1008
    .end local v2    # "argument":Landroid/os/Bundle;
    .end local v3    # "c":Landroid/database/Cursor;
    .end local v4    # "detail":Landroid/app/DialogFragment;
    .end local v5    # "fileName":Ljava/lang/String;
    .end local v6    # "filePath":Ljava/lang/String;
    .end local v8    # "fileStatus":I
    .end local v17    # "startDownload":Z
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->onCreateMultipleDetailFragmentListener()Landroid/app/DialogFragment;

    move-result-object v4

    .line 1010
    .restart local v4    # "detail":Landroid/app/DialogFragment;
    if-eqz v4, :cond_0

    .line 1014
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1016
    .restart local v2    # "argument":Landroid/os/Bundle;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1018
    .local v10, "formats":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 1020
    .local v13, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v11, v0, :cond_a

    .line 1022
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v20, v0

    invoke-virtual {v15, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v19

    move-object/from16 v0, v20

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v12

    .line 1024
    .local v12, "item":Ljava/lang/Object;
    if-eqz v12, :cond_7

    instance-of v0, v12, Landroid/database/Cursor;

    move/from16 v19, v0

    if-eqz v19, :cond_7

    move-object v3, v12

    .line 1026
    check-cast v3, Landroid/database/Cursor;

    .line 1028
    .restart local v3    # "c":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 1030
    .local v9, "format":I
    const-string v19, "local_file"

    move-object/from16 v0, v19

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1032
    .restart local v6    # "filePath":Ljava/lang/String;
    const-string v19, "file_size"

    move-object/from16 v0, v19

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 1034
    .local v7, "fileSize":Ljava/lang/Long;
    if-eqz v6, :cond_8

    .line 1036
    sget-object v19, Ljava/io/File;->separator:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 1060
    :cond_6
    :goto_3
    invoke-virtual {v7}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1062
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1020
    .end local v3    # "c":Landroid/database/Cursor;
    .end local v6    # "filePath":Ljava/lang/String;
    .end local v7    # "fileSize":Ljava/lang/Long;
    .end local v9    # "format":I
    :cond_7
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 1040
    .restart local v3    # "c":Landroid/database/Cursor;
    .restart local v6    # "filePath":Ljava/lang/String;
    .restart local v7    # "fileSize":Ljava/lang/Long;
    .restart local v9    # "format":I
    :cond_8
    const/16 v17, 0x0

    .line 1042
    .restart local v17    # "startDownload":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    instance-of v0, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;

    move/from16 v19, v0

    if-eqz v19, :cond_9

    .line 1044
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v19, v0

    check-cast v19, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->isAvailableForDownload(I)Z

    move-result v17

    .line 1047
    :cond_9
    if-nez v17, :cond_6

    .line 1049
    const-string v19, "status"

    move-object/from16 v0, v19

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 1051
    .restart local v8    # "fileStatus":I
    const/16 v19, 0x3

    move/from16 v0, v19

    if-ne v8, v0, :cond_6

    .line 1053
    const-string v19, "filename"

    move-object/from16 v0, v19

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1055
    .restart local v5    # "fileName":Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v20, Lcom/sec/android/app/myfiles/utils/Constant;->RSHARE_DOWNLOAD_DIR:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    sget-object v20, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_3

    .line 1066
    .end local v3    # "c":Landroid/database/Cursor;
    .end local v5    # "fileName":Ljava/lang/String;
    .end local v6    # "filePath":Ljava/lang/String;
    .end local v7    # "fileSize":Ljava/lang/Long;
    .end local v8    # "fileStatus":I
    .end local v9    # "format":I
    .end local v12    # "item":Ljava/lang/Object;
    .end local v17    # "startDownload":Z
    :cond_a
    const-string v19, "detail_item_path"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0, v13}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1068
    const-string v19, "detail_item_format"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0, v10}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1070
    invoke-virtual {v4, v2}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 1072
    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 1074
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v19

    const-string v20, "detail"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v4, v0, v1}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public startSelectMode(III)V
    .locals 1
    .param p1, "selectMode"    # I
    .param p2, "initialSelectedPosition"    # I
    .param p3, "from"    # I

    .prologue
    .line 823
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 830
    :cond_0
    :goto_0
    return-void

    .line 826
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;

    if-eqz v0, :cond_0

    .line 827
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->getSelectableItemsCount()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mSelectableItemsCount:I

    .line 828
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startSelectMode(III)V

    goto :goto_0
.end method

.method protected unselectAllItem()V
    .locals 1

    .prologue
    .line 584
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->unselectAllItem()V

    .line 585
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 588
    :cond_0
    return-void
.end method
