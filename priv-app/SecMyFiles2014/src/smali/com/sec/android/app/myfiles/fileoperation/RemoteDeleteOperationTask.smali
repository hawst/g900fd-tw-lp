.class public Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;
.super Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;
.source "RemoteDeleteOperationTask.java"


# static fields
.field private static final MODULE:Ljava/lang/String; = "RemoteDeleteOperationTask"


# instance fields
.field private mBaiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

.field private mContentType:I

.field private mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILandroid/view/View;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "operation"    # I
    .param p3, "contentType"    # I
    .param p4, "progressView"    # Landroid/view/View;

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;-><init>(Landroid/content/Context;IILandroid/view/View;)V

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mBaiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mContentType:I

    .line 49
    iput p3, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mContentType:I

    .line 50
    iget v0, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mContentType:I

    const/16 v1, 0x1f

    if-ne v0, v1, :cond_0

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mBaiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    .line 56
    :goto_0
    return-void

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    goto :goto_0
.end method

.method private DeleteFiles(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 9
    .param p2, "dstFolder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "srcFilesPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v8, 0x0

    .line 94
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->isCancelled()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    const-string v6, "RemoteDeleteOperationTask"

    const-string v7, "DeleteFiles start"

    invoke-static {v8, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 97
    const/4 v3, 0x0

    .line 98
    .local v3, "progress":I
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v6, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 99
    const/4 v1, 0x0

    .line 100
    .local v1, "i":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 101
    .local v0, "f":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->isCancelled()Z

    move-result v6

    if-nez v6, :cond_0

    .line 103
    add-int/lit8 v1, v1, 0x1

    .line 105
    iget v6, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mContentType:I

    const/16 v7, 0x1f

    if-ne v6, v7, :cond_6

    .line 106
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mBaiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    invoke-virtual {v6, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->deleteFile(Ljava/lang/String;)V

    .line 122
    :goto_2
    sget-char v6, Ljava/io/File;->separatorChar:C

    invoke-virtual {v0, v6}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 124
    .local v5, "shortcutName":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_2

    .line 126
    const-string v0, "Dropbox/"

    .line 128
    :cond_2
    const-string v6, "Dropbox/"

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 130
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Dropbox/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 133
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v6, v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSetHoemScreenPath(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 136
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mContext:Landroid/content/Context;

    invoke-static {v6, v0, v5}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteShortcutFromHome(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mContext:Landroid/content/Context;

    const/16 v7, 0x8

    invoke-static {v6, v0, v7}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteShortcut(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v4

    .line 141
    .local v4, "rowsDeleted":I
    const/4 v6, 0x1

    if-ge v4, v6, :cond_5

    .line 142
    const-string v6, "RemoteDeleteOperationTask"

    const-string v7, "Shortcut does not exist"

    invoke-static {v8, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 144
    :cond_5
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    div-int v6, v1, v6

    mul-int/lit8 v3, v6, 0x64

    .line 145
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v6, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_1

    .line 110
    .end local v4    # "rowsDeleted":I
    .end local v5    # "shortcutName":Ljava/lang/String;
    :cond_6
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->isAuthKeySaved()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 112
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    invoke-virtual {v6, v0}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->deleteFile(Ljava/lang/String;)V

    goto :goto_2

    .line 116
    :cond_7
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->requestGetToken()V

    .line 118
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    invoke-virtual {v6, v0}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->deleteFile(Ljava/lang/String;)V

    goto :goto_2

    .line 147
    .end local v0    # "f":Ljava/lang/String;
    :cond_8
    const-string v6, "RemoteDeleteOperationTask"

    const-string v7, "DeleteFiles finish"

    invoke-static {v8, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;
    .locals 4
    .param p1, "params"    # [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .prologue
    const/4 v1, 0x0

    .line 66
    aget-object v0, p1, v1

    .line 67
    .local v0, "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    iget-object v2, v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mTargetFolder:Ljava/lang/String;

    .line 68
    iget-object v2, v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    .line 69
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    if-nez v3, :cond_1

    :goto_0
    iput v1, v2, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mTotalTarget:I

    .line 70
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v2, v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mStartIndex:I

    iput v2, v1, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    .line 71
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 72
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mTargetFolder:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->DeleteFiles(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 74
    :cond_0
    const/4 v1, 0x0

    return-object v1

    .line 69
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 38
    check-cast p1, [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 0

    .prologue
    .line 90
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->onCancelled()V

    .line 91
    return-void
.end method

.method protected onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V
    .locals 2
    .param p1, "result"    # Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .prologue
    .line 84
    const v0, 0x7f0c000b

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v1, v1, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mTotalTarget:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->showToastPrurals(II)V

    .line 85
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    .line 86
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 38
    check-cast p1, Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Lcom/sec/android/app/myfiles/element/FileOperationProgress;)V
    .locals 0
    .param p1, "values"    # [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    .prologue
    .line 79
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 80
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 38
    check-cast p1, [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->onProgressUpdate([Lcom/sec/android/app/myfiles/element/FileOperationProgress;)V

    return-void
.end method

.method protected setupProgressView()V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;->mProcessing:Landroid/widget/TextView;

    const v1, 0x7f0b00cd

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 62
    return-void
.end method
