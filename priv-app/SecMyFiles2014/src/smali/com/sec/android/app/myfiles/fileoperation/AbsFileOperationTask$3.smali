.class Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;
.super Ljava/lang/Object;
.source "AbsFileOperationTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->showRenameDialog()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;)V
    .locals 0

    .prologue
    .line 761
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const v13, 0x7f0b0127

    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 766
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget-object v9, v9, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 767
    .local v7, "res":Landroid/content/res/Resources;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget-object v9, v9, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v9}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 768
    .local v0, "b":Landroid/app/AlertDialog$Builder;
    const-string v5, ""

    .line 769
    .local v5, "message":Ljava/lang/String;
    const-string v8, ""

    .line 770
    .local v8, "title":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget-object v4, v9, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mPath:Ljava/lang/String;

    .line 771
    .local v4, "fileName":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget-object v9, v9, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mPath:Ljava/lang/String;

    const/16 v10, 0x2f

    invoke-virtual {v9, v10}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v6

    .line 773
    .local v6, "pos":I
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget-object v9, v9, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    invoke-static {v9}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 774
    .local v3, "factory":Landroid/view/LayoutInflater;
    const v9, 0x7f04003a

    const/4 v10, 0x0

    invoke-virtual {v3, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 775
    .local v2, "customDialog":Landroid/view/View;
    const v9, 0x7f0f00d8

    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 777
    .local v1, "checkBox":Landroid/widget/CheckBox;
    new-instance v9, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$1;

    invoke-direct {v9, p0, v1}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$1;-><init>(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;Landroid/widget/CheckBox;)V

    invoke-virtual {v1, v9}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 789
    if-ltz v6, :cond_0

    .line 790
    add-int/lit8 v9, v6, 0x1

    invoke-virtual {v4, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 792
    :cond_0
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_1

    .line 793
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget-object v4, v9, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mPath:Ljava/lang/String;

    .line 795
    :cond_1
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget v9, v9, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mOperationType:I

    if-nez v9, :cond_3

    .line 797
    const v9, 0x7f0b0129

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-array v10, v11, [Ljava/lang/Object;

    aput-object v4, v10, v12

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 798
    invoke-virtual {v7, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 816
    :cond_2
    :goto_0
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 818
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 820
    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 822
    const v9, 0x7f0b0023

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$2;

    invoke-direct {v10, p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$2;-><init>(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;)V

    invoke-virtual {v0, v9, v10}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 857
    const v9, 0x7f0b0152

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$3;

    invoke-direct {v10, p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$3;-><init>(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;)V

    invoke-virtual {v0, v9, v10}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 892
    const v9, 0x7f0b0017

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;

    invoke-direct {v10, p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;-><init>(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;)V

    invoke-virtual {v0, v9, v10}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 970
    new-instance v9, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$5;

    invoke-direct {v9, p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$5;-><init>(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;)V

    invoke-virtual {v0, v9}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 985
    new-instance v9, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$6;

    invoke-direct {v9, p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$6;-><init>(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;)V

    invoke-virtual {v0, v9}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 1019
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v10

    iput-object v10, v9, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRenameOverwriteDialog:Landroid/app/AlertDialog;

    .line 1020
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget-object v9, v9, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRenameOverwriteDialog:Landroid/app/AlertDialog;

    invoke-virtual {v9}, Landroid/app/AlertDialog;->show()V

    .line 1021
    return-void

    .line 800
    :cond_3
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget v9, v9, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mOperationType:I

    if-ne v9, v11, :cond_4

    .line 802
    const v9, 0x7f0b0128

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-array v10, v11, [Ljava/lang/Object;

    aput-object v4, v10, v12

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 803
    invoke-virtual {v7, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    .line 805
    :cond_4
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget v9, v9, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mOperationType:I

    const/4 v10, 0x4

    if-ne v9, v10, :cond_5

    .line 807
    const v9, 0x7f0b00ed

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-array v10, v11, [Ljava/lang/Object;

    aput-object v4, v10, v12

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 808
    const v9, 0x7f0b0029

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    .line 810
    :cond_5
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget v9, v9, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mOperationType:I

    const/4 v10, 0x3

    if-ne v9, v10, :cond_2

    .line 812
    const v9, 0x7f0b0076

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-array v10, v11, [Ljava/lang/Object;

    aput-object v4, v10, v12

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 813
    const v9, 0x7f0b0027

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0
.end method
