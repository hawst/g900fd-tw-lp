.class Lcom/sec/android/app/myfiles/MainActivity$7$5;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/MainActivity$7;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/myfiles/MainActivity$7;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/MainActivity$7;)V
    .locals 0

    .prologue
    .line 2376
    iput-object p1, p0, Lcom/sec/android/app/myfiles/MainActivity$7$5;->this$1:Lcom/sec/android/app/myfiles/MainActivity$7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2381
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$7$5;->this$1:Lcom/sec/android/app/myfiles/MainActivity$7;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2383
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$7$5;->this$1:Lcom/sec/android/app/myfiles/MainActivity$7;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 2385
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$7$5;->this$1:Lcom/sec/android/app/myfiles/MainActivity$7;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    if-eqz v0, :cond_0

    .line 2387
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$7$5;->this$1:Lcom/sec/android/app/myfiles/MainActivity$7;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->selectItemControl(I)V

    .line 2389
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$7$5;->this$1:Lcom/sec/android/app/myfiles/MainActivity$7;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setAllItemCount()V

    .line 2406
    :cond_0
    :goto_0
    return-void

    .line 2396
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$7$5;->this$1:Lcom/sec/android/app/myfiles/MainActivity$7;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 2398
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$7$5;->this$1:Lcom/sec/android/app/myfiles/MainActivity$7;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    if-eqz v0, :cond_0

    .line 2400
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$7$5;->this$1:Lcom/sec/android/app/myfiles/MainActivity$7;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->selectItemControl(I)V

    .line 2402
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$7$5;->this$1:Lcom/sec/android/app/myfiles/MainActivity$7;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setAllItemCount()V

    goto :goto_0
.end method
