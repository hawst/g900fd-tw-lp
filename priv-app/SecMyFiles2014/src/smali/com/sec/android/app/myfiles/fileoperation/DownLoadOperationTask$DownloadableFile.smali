.class Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;
.super Ljava/lang/Object;
.source "DownLoadOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DownloadableFile"
.end annotation


# instance fields
.field public mDestinationPath:Ljava/lang/String;

.field public mDownload:Lcom/samsung/scloud/DropboxAPI$CancelableDownload;

.field public mDownloadPercent:J

.field public mServerPath:Ljava/lang/String;

.field public mSize:J

.field public mStarted:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;JLjava/lang/String;)V
    .locals 6
    .param p1, "serverPath"    # Ljava/lang/String;
    .param p2, "currentSize"    # J
    .param p4, "dstPath"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;->mServerPath:Ljava/lang/String;

    .line 173
    iput-wide v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;->mSize:J

    .line 175
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;->mDestinationPath:Ljava/lang/String;

    .line 177
    iput-wide v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;->mDownloadPercent:J

    .line 179
    iput-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;->mDownload:Lcom/samsung/scloud/DropboxAPI$CancelableDownload;

    .line 181
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;->mStarted:Z

    .line 185
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;->mServerPath:Ljava/lang/String;

    .line 186
    iput-wide p2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;->mSize:J

    .line 187
    iput-object p4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;->mDestinationPath:Ljava/lang/String;

    .line 188
    iput-wide v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;->mDownloadPercent:J

    .line 189
    iput-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;->mDownload:Lcom/samsung/scloud/DropboxAPI$CancelableDownload;

    .line 190
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;->mStarted:Z

    .line 191
    return-void
.end method
