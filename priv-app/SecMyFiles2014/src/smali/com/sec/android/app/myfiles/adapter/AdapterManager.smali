.class public Lcom/sec/android/app/myfiles/adapter/AdapterManager;
.super Ljava/lang/Object;
.source "AdapterManager.java"


# static fields
.field private static final MODULE:Ljava/lang/String; = "AdapterManager"

.field private static final mDocumentsProjection:[Ljava/lang/String;

.field private static final mFilesProjection:[Ljava/lang/String;

.field private static final mImageProjection:[Ljava/lang/String;

.field private static final mMusicProjection:[Ljava/lang/String;

.field private static final mRecentlyProjection:[Ljava/lang/String;

.field private static final mVideoProjection:[Ljava/lang/String;


# instance fields
.field private FILE_NAME_FIND_FILTER:Ljava/lang/String;

.field private mAdapterMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/widget/BaseAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCurrentInOrder:I

.field private mCurrentSortBy:I

.field private mCursor:Landroid/database/Cursor;

.field private mRestrictDRM:Z

.field private mSelectModeAdapterMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/app/myfiles/adapter/ISelectModeAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 101
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    const-string v1, "format"

    aput-object v1, v0, v5

    const-string v1, "date_modified"

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mFilesProjection:[Ljava/lang/String;

    .line 107
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    const-string v1, "bucket_display_name"

    aput-object v1, v0, v5

    const-string v1, "_size"

    aput-object v1, v0, v6

    const-string v1, "date_modified"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mImageProjection:[Ljava/lang/String;

    .line 116
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    const-string v1, "bucket_display_name"

    aput-object v1, v0, v5

    const-string v1, "_size"

    aput-object v1, v0, v6

    const-string v1, "date_modified"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mVideoProjection:[Ljava/lang/String;

    .line 125
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    const-string v1, "album_id"

    aput-object v1, v0, v5

    const-string v1, "_size"

    aput-object v1, v0, v6

    const-string v1, "date_modified"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "is_drm"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mMusicProjection:[Ljava/lang/String;

    .line 135
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    const-string v1, "_size"

    aput-object v1, v0, v5

    const-string v1, "date_modified"

    aput-object v1, v0, v6

    const-string v1, "mime_type"

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mDocumentsProjection:[Ljava/lang/String;

    .line 143
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    const-string v1, "_size"

    aput-object v1, v0, v5

    const-string v1, "date_modified"

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mRecentlyProjection:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "restrictDrm"    # Z
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    const-string v0, "\"abcdefghijklmnopqrstuvwxyz01234567890;.!@#$%^&()-_=+\""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->FILE_NAME_FIND_FILTER:Ljava/lang/String;

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    .line 167
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    .line 169
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mAdapterMap:Ljava/util/HashMap;

    .line 171
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSelectModeAdapterMap:Ljava/util/HashMap;

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentSortBy()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentSortBy:I

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentInOrder()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentInOrder:I

    .line 181
    iput-boolean p2, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mRestrictDRM:Z

    .line 183
    return-void
.end method

.method private createAdapter(I)Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    .locals 70
    .param p1, "fragmentId"    # I

    .prologue
    .line 265
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentSortBy()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentSortBy:I

    .line 267
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentInOrder()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentInOrder:I

    .line 269
    const/4 v1, 0x0

    .line 275
    .local v1, "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    sparse-switch p1, :sswitch_data_0

    .line 593
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 595
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentSortBy:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentInOrder:I

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->setSortBy(II)V

    .line 597
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->onRefresh()V

    .line 600
    :cond_1
    return-object v1

    .line 279
    :sswitch_0
    new-instance v67, Ljava/util/ArrayList;

    invoke-direct/range {v67 .. v67}, Ljava/util/ArrayList;-><init>()V

    .line 281
    .local v67, "datas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/CategoryItem;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v69

    .line 283
    .local v69, "res":Landroid/content/res/Resources;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getEasyMode()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 285
    new-instance v2, Lcom/sec/android/app/myfiles/element/CategoryItem;

    const/16 v3, 0x201

    const v4, 0x7f0b0001

    move-object/from16 v0, v69

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f020020

    const/4 v7, 0x0

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/sec/android/app/myfiles/element/CategoryItem;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    move-object/from16 v0, v67

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 291
    new-instance v2, Lcom/sec/android/app/myfiles/element/CategoryItem;

    const/4 v3, 0x2

    const v4, 0x7f0b0003

    move-object/from16 v0, v69

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f020028

    const/4 v7, 0x0

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/sec/android/app/myfiles/element/CategoryItem;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    move-object/from16 v0, v67

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 297
    new-instance v2, Lcom/sec/android/app/myfiles/element/CategoryItem;

    const/4 v3, 0x3

    const v4, 0x7f0b0004

    move-object/from16 v0, v69

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f020031

    const/4 v7, 0x0

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/sec/android/app/myfiles/element/CategoryItem;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    move-object/from16 v0, v67

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 303
    new-instance v2, Lcom/sec/android/app/myfiles/element/CategoryItem;

    const/4 v3, 0x4

    const v4, 0x7f0b0147

    move-object/from16 v0, v69

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f020029

    const/4 v7, 0x0

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/sec/android/app/myfiles/element/CategoryItem;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    move-object/from16 v0, v67

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 309
    new-instance v2, Lcom/sec/android/app/myfiles/element/CategoryItem;

    const/4 v3, 0x5

    const v4, 0x7f0b0006

    move-object/from16 v0, v69

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f020023

    const/4 v7, 0x0

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/sec/android/app/myfiles/element/CategoryItem;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    move-object/from16 v0, v67

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 315
    new-instance v2, Lcom/sec/android/app/myfiles/element/CategoryItem;

    const/16 v3, 0x28

    const v4, 0x7f0b014d

    move-object/from16 v0, v69

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f020024

    const/4 v7, 0x0

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/sec/android/app/myfiles/element/CategoryItem;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    move-object/from16 v0, v67

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 365
    :goto_1
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    .end local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, v67

    invoke-direct {v1, v2, v0}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 367
    .restart local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    goto/16 :goto_0

    .line 328
    :cond_2
    new-instance v2, Lcom/sec/android/app/myfiles/element/CategoryItem;

    const/4 v3, 0x7

    const v4, 0x7f0b0012

    move-object/from16 v0, v69

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f02002b

    const/4 v7, 0x0

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/sec/android/app/myfiles/element/CategoryItem;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    move-object/from16 v0, v67

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 334
    new-instance v2, Lcom/sec/android/app/myfiles/element/CategoryItem;

    const/4 v3, 0x2

    const v4, 0x7f0b0003

    move-object/from16 v0, v69

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f020028

    const/4 v7, 0x0

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/sec/android/app/myfiles/element/CategoryItem;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    move-object/from16 v0, v67

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 340
    new-instance v2, Lcom/sec/android/app/myfiles/element/CategoryItem;

    const/4 v3, 0x3

    const v4, 0x7f0b0004

    move-object/from16 v0, v69

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f020031

    const/4 v7, 0x0

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/sec/android/app/myfiles/element/CategoryItem;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    move-object/from16 v0, v67

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 346
    new-instance v2, Lcom/sec/android/app/myfiles/element/CategoryItem;

    const/4 v3, 0x4

    const v4, 0x7f0b0147

    move-object/from16 v0, v69

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f020029

    const/4 v7, 0x0

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/sec/android/app/myfiles/element/CategoryItem;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    move-object/from16 v0, v67

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 352
    new-instance v2, Lcom/sec/android/app/myfiles/element/CategoryItem;

    const/4 v3, 0x5

    const v4, 0x7f0b0006

    move-object/from16 v0, v69

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f020023

    const/4 v7, 0x0

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/sec/android/app/myfiles/element/CategoryItem;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    move-object/from16 v0, v67

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 358
    new-instance v2, Lcom/sec/android/app/myfiles/element/CategoryItem;

    const/4 v3, 0x6

    const v4, 0x7f0b0007

    move-object/from16 v0, v69

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f020025

    const/4 v7, 0x0

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/sec/android/app/myfiles/element/CategoryItem;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    move-object/from16 v0, v67

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 375
    .end local v67    # "datas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/CategoryItem;>;"
    .end local v69    # "res":Landroid/content/res/Resources;
    :sswitch_1
    const/4 v6, 0x0

    .line 377
    .local v6, "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    if-nez v6, :cond_3

    .line 379
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->createNavigation(I)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v6

    .line 384
    :cond_3
    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    .line 386
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    .end local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    const v3, 0x7f040015

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    const/4 v5, 0x2

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 389
    .restart local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    goto/16 :goto_0

    .line 394
    .end local v6    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    :sswitch_2
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->createNavigation(I)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v6

    .line 396
    .restart local v6    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    .end local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    const v3, 0x7f04003d

    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 399
    .restart local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    goto/16 :goto_0

    .line 404
    .end local v6    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    :sswitch_3
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getImageList()Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    .line 406
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;

    .end local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    const v9, 0x7f040015

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    const/4 v11, 0x2

    const/4 v12, 0x0

    move-object v7, v1

    invoke-direct/range {v7 .. v12}, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 409
    .restart local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    goto/16 :goto_0

    .line 414
    :sswitch_4
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getVideoList()Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    .line 416
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;

    .end local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    const v9, 0x7f040015

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    const/4 v11, 0x2

    const/4 v12, 0x0

    move-object v7, v1

    invoke-direct/range {v7 .. v12}, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 419
    .restart local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    goto/16 :goto_0

    .line 424
    :sswitch_5
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getMusicList()Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    .line 426
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;

    .end local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    const v9, 0x7f040015

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    const/4 v11, 0x2

    const/4 v12, 0x0

    move-object v7, v1

    invoke-direct/range {v7 .. v12}, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 429
    .restart local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    goto/16 :goto_0

    .line 434
    :sswitch_6
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getDocumentList()Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    .line 436
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/DocumentBrowserAdapter;

    .end local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    const v9, 0x7f040015

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    const/4 v11, 0x2

    const/4 v12, 0x0

    move-object v7, v1

    invoke-direct/range {v7 .. v12}, Lcom/sec/android/app/myfiles/adapter/DocumentBrowserAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 439
    .restart local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    goto/16 :goto_0

    .line 444
    :sswitch_7
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getRecentlyFileList()Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    .line 446
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    .end local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    const v9, 0x7f040015

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    const/4 v11, 0x2

    const/4 v12, 0x0

    move-object v7, v1

    invoke-direct/range {v7 .. v12}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 449
    .restart local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    goto/16 :goto_0

    .line 454
    :sswitch_8
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getRecentlyUsedFolderList()Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    .line 456
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    .end local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    const v9, 0x7f040015

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    const/4 v11, 0x2

    const/4 v12, 0x0

    move-object v7, v1

    invoke-direct/range {v7 .. v12}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 459
    .restart local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    goto/16 :goto_0

    .line 464
    :sswitch_9
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getFrequentlyUsedFolderList()Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    .line 466
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/FrequentlyUsedAdapter;

    .end local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    const v9, 0x7f040015

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    const/4 v11, 0x2

    const/4 v12, 0x0

    move-object v7, v1

    invoke-direct/range {v7 .. v12}, Lcom/sec/android/app/myfiles/adapter/FrequentlyUsedAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 469
    .restart local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    goto/16 :goto_0

    .line 474
    :sswitch_a
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    .end local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;-><init>(Landroid/content/Context;)V

    .line 476
    .restart local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    goto/16 :goto_0

    .line 480
    :sswitch_b
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->createNavigation(I)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v12

    .line 481
    .local v12, "remoteShareNavi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    .line 482
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;

    .end local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    const v9, 0x7f040016

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    const/4 v11, 0x2

    move-object v7, v1

    invoke-direct/range {v7 .. v12}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 484
    .restart local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    goto/16 :goto_0

    .line 488
    .end local v12    # "remoteShareNavi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    :sswitch_c
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->createNavigation(I)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v18

    .line 490
    .local v18, "dropboxNavi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    .line 492
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    .end local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    const v15, 0x7f040015

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    move-object/from16 v16, v0

    const/16 v17, 0x2

    move-object v13, v1

    invoke-direct/range {v13 .. v18}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 495
    .restart local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    goto/16 :goto_0

    .line 500
    .end local v18    # "dropboxNavi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    :sswitch_d
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 501
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->createNavigation(I)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v24

    .line 502
    .local v24, "baiduNavi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    .line 503
    new-instance v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    .end local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f040015

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    move-object/from16 v22, v0

    const/16 v23, 0x2

    move-object/from16 v19, v1

    move/from16 v25, p1

    invoke-direct/range {v19 .. v25}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;I)V

    .line 505
    .restart local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    goto/16 :goto_0

    .line 510
    .end local v24    # "baiduNavi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    :sswitch_e
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;

    .end local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 512
    .restart local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    goto/16 :goto_0

    .line 516
    :sswitch_f
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/WatchAdapter;

    .end local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/myfiles/adapter/WatchAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 518
    .restart local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    goto/16 :goto_0

    .line 523
    :sswitch_10
    invoke-static {}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->getInstance()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->getSelectedDeviceKey()Ljava/lang/String;

    move-result-object v68

    .line 525
    .local v68, "deviceKey":Ljava/lang/String;
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->createNavigation(I)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v30

    .local v30, "nearByDeviceNavi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    move-object/from16 v2, v30

    .line 527
    check-cast v2, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    move-object/from16 v0, v68

    invoke-virtual {v2, v0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->setProvider(Ljava/lang/String;)V

    .line 529
    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    .line 531
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    .end local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    const v27, 0x7f040015

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    move-object/from16 v28, v0

    const/16 v29, 0x2

    move-object/from16 v25, v1

    invoke-direct/range {v25 .. v30}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 533
    .restart local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    goto/16 :goto_0

    .line 538
    .end local v30    # "nearByDeviceNavi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    .end local v68    # "deviceKey":Ljava/lang/String;
    :sswitch_11
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->createNavigation(I)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v36

    .line 542
    .local v36, "ftpNavigation":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    .line 544
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;

    .end local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    move-object/from16 v32, v0

    const v33, 0x7f040015

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    move-object/from16 v34, v0

    const/16 v35, 0x2

    move-object/from16 v31, v1

    invoke-direct/range {v31 .. v36}, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 547
    .restart local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    goto/16 :goto_0

    .line 549
    .end local v36    # "ftpNavigation":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    :sswitch_12
    const/16 v2, 0x10

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->createNavigation(I)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v42

    .line 550
    .local v42, "ftpListNavigation":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    .line 551
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/FTPListAdapter;

    .end local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    move-object/from16 v38, v0

    const v39, 0x7f040015

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    move-object/from16 v40, v0

    const/16 v41, 0x2

    move-object/from16 v37, v1

    invoke-direct/range {v37 .. v42}, Lcom/sec/android/app/myfiles/adapter/FTPListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 553
    .restart local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    goto/16 :goto_0

    .line 556
    .end local v42    # "ftpListNavigation":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    :sswitch_13
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->createNavigation(I)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v48

    .line 557
    .local v48, "ftpShortcutsNavi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    invoke-virtual/range {v48 .. v48}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    .line 558
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/FTPShortcutAdapter;

    .end local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    move-object/from16 v44, v0

    const v45, 0x7f040015

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    move-object/from16 v46, v0

    const/16 v47, 0x2

    move-object/from16 v43, v1

    invoke-direct/range {v43 .. v48}, Lcom/sec/android/app/myfiles/adapter/FTPShortcutAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 560
    .restart local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    goto/16 :goto_0

    .line 563
    .end local v48    # "ftpShortcutsNavi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    :sswitch_14
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->createNavigation(I)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v54

    .line 564
    .local v54, "userShortcutnavi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    invoke-virtual/range {v54 .. v54}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    .line 565
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/UserShortcutAdapter;

    .end local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    move-object/from16 v50, v0

    const v51, 0x7f040015

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    move-object/from16 v52, v0

    const/16 v53, 0x2

    move-object/from16 v49, v1

    invoke-direct/range {v49 .. v54}, Lcom/sec/android/app/myfiles/adapter/UserShortcutAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 568
    .restart local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    goto/16 :goto_0

    .line 571
    .end local v54    # "userShortcutnavi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    :sswitch_15
    const/16 v2, 0x201

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->createNavigation(I)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v60

    .line 572
    .local v60, "userBrowsernavi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    invoke-virtual/range {v60 .. v60}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    .line 573
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    .end local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    move-object/from16 v56, v0

    const v57, 0x7f040015

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    move-object/from16 v58, v0

    const/16 v59, 0x2

    move-object/from16 v55, v1

    invoke-direct/range {v55 .. v60}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 576
    .restart local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    goto/16 :goto_0

    .line 580
    .end local v60    # "userBrowsernavi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    :sswitch_16
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->createNavigation(I)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v66

    .line 582
    .local v66, "navigation":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    invoke-virtual/range {v66 .. v66}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    .line 584
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    .end local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    move-object/from16 v62, v0

    const v63, 0x7f040015

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    move-object/from16 v64, v0

    const/16 v65, 0x2

    move-object/from16 v61, v1

    invoke-direct/range {v61 .. v66}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 587
    .restart local v1    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    goto/16 :goto_0

    .line 275
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_3
        0x3 -> :sswitch_4
        0x4 -> :sswitch_5
        0x5 -> :sswitch_6
        0x6 -> :sswitch_a
        0x7 -> :sswitch_7
        0x8 -> :sswitch_c
        0x9 -> :sswitch_10
        0xa -> :sswitch_11
        0x10 -> :sswitch_13
        0x11 -> :sswitch_12
        0x12 -> :sswitch_2
        0x13 -> :sswitch_10
        0x14 -> :sswitch_c
        0x16 -> :sswitch_1
        0x18 -> :sswitch_8
        0x19 -> :sswitch_9
        0x1a -> :sswitch_e
        0x1f -> :sswitch_d
        0x20 -> :sswitch_d
        0x21 -> :sswitch_f
        0x23 -> :sswitch_14
        0x24 -> :sswitch_15
        0x25 -> :sswitch_b
        0x28 -> :sswitch_16
        0x201 -> :sswitch_1
    .end sparse-switch
.end method

.method private createNavigation(I)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    .locals 2
    .param p1, "fragmentId"    # I

    .prologue
    .line 796
    const/4 v0, 0x0

    .line 798
    .local v0, "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    sparse-switch p1, :sswitch_data_0

    .line 887
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 888
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setShowFolderFileType(I)V

    .line 892
    :cond_1
    return-object v0

    .line 804
    :sswitch_0
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    .end local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;-><init>(Landroid/content/Context;I)V

    .line 838
    .restart local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    goto :goto_0

    .line 840
    :sswitch_1
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;

    .end local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;-><init>(Landroid/content/Context;I)V

    .line 841
    .restart local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    goto :goto_0

    .line 843
    :sswitch_2
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    .end local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;-><init>(Landroid/content/Context;I)V

    .line 844
    .restart local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    goto :goto_0

    .line 847
    :sswitch_3
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;

    .end local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;-><init>(Landroid/content/Context;I)V

    .line 849
    .restart local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    goto :goto_0

    .line 851
    :sswitch_4
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/DropBoxSearchNavigation;

    .end local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/myfiles/navigation/DropBoxSearchNavigation;-><init>(Landroid/content/Context;I)V

    .line 852
    .restart local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    goto :goto_0

    .line 854
    :sswitch_5
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    .end local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;-><init>(Landroid/content/Context;I)V

    .line 855
    .restart local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    goto :goto_0

    .line 857
    :sswitch_6
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    .end local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;-><init>(Landroid/content/Context;I)V

    .line 858
    .restart local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    goto :goto_0

    .line 860
    :sswitch_7
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    .end local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;-><init>(Landroid/content/Context;I)V

    .line 861
    .restart local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    goto :goto_0

    .line 863
    :sswitch_8
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/FTPShortcutNavigation;

    .end local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/myfiles/navigation/FTPShortcutNavigation;-><init>(Landroid/content/Context;I)V

    .line 864
    .restart local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    goto :goto_0

    .line 868
    :sswitch_9
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 869
    new-instance v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    .end local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;-><init>(Landroid/content/Context;I)V

    .restart local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    goto :goto_0

    .line 874
    :sswitch_a
    new-instance v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchNavigation;

    .end local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchNavigation;-><init>(Landroid/content/Context;I)V

    .line 875
    .restart local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    goto :goto_0

    .line 878
    :sswitch_b
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/UserShortcutNaigation;

    .end local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/myfiles/navigation/UserShortcutNaigation;-><init>(Landroid/content/Context;I)V

    .line 879
    .restart local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    goto :goto_0

    .line 882
    :sswitch_c
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;

    .end local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;-><init>(Landroid/content/Context;I)V

    .restart local v0    # "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    goto :goto_0

    .line 798
    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_3
        0x9 -> :sswitch_5
        0xa -> :sswitch_7
        0x10 -> :sswitch_8
        0x12 -> :sswitch_1
        0x13 -> :sswitch_6
        0x14 -> :sswitch_4
        0x16 -> :sswitch_0
        0x1f -> :sswitch_9
        0x20 -> :sswitch_a
        0x23 -> :sswitch_b
        0x24 -> :sswitch_0
        0x25 -> :sswitch_2
        0x28 -> :sswitch_c
        0x201 -> :sswitch_0
    .end sparse-switch
.end method

.method private getDocumentList()Landroid/database/Cursor;
    .locals 14

    .prologue
    .line 1248
    const/4 v6, 0x0

    .line 1250
    .local v6, "cursor":Landroid/database/Cursor;
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentSortBy:I

    iget v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentInOrder:I

    const/4 v2, 0x1

    const/4 v13, 0x0

    invoke-static {v0, v1, v2, v13}, Lcom/sec/android/app/myfiles/utils/MyFilesDB;->getOrderBy(IIIZ)Ljava/lang/String;

    move-result-object v5

    .line 1252
    .local v5, "orderBy":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 1257
    .local v10, "sb":Ljava/lang/StringBuilder;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getmMimeTypeForCategory()Ljava/lang/String;

    move-result-object v9

    .line 1258
    .local v9, "mimeType":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getmExtensionForCategory()Ljava/lang/String;

    move-result-object v7

    .line 1260
    .local v7, "extension":Ljava/lang/String;
    const-string v0, "*"

    invoke-virtual {v9, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1261
    const-string v9, ""

    .line 1263
    :cond_0
    const-string v0, "*"

    invoke-virtual {v7, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1264
    const-string v7, ""

    .line 1266
    :cond_1
    const-string v0, ""

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, ""

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1267
    invoke-static {}, Lcom/sec/android/app/myfiles/MediaFile;->getDocumentExtensions()[Ljava/lang/String;

    move-result-object v4

    .line 1269
    .local v4, "whereArgs":[Ljava/lang/String;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    array-length v0, v4

    if-ge v8, v0, :cond_2

    .line 1270
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "%."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, v4, v8

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    .line 1269
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1272
    :cond_2
    const-string v0, "("

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1273
    const/4 v8, 0x0

    :goto_1
    array-length v0, v4

    if-ge v8, v0, :cond_b

    .line 1275
    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    if-ge v8, v0, :cond_3

    .line 1277
    const-string v0, "_data LIKE ? OR "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1273
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 1281
    :cond_3
    const-string v0, "_data LIKE ?) AND "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1286
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .end local v8    # "i":I
    :cond_4
    const-string v0, ";"

    invoke-virtual {v9, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 1287
    .local v11, "whereArgs2":[Ljava/lang/String;
    const-string v0, ""

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1288
    invoke-static {}, Lcom/sec/android/app/myfiles/MediaFile;->getDocumentExtensions()[Ljava/lang/String;

    move-result-object v12

    .line 1292
    .local v12, "whereArgs3":[Ljava/lang/String;
    :goto_3
    array-length v0, v12

    array-length v1, v11

    add-int/2addr v0, v1

    new-array v4, v0, [Ljava/lang/String;

    .line 1294
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_4
    array-length v0, v4

    if-ge v8, v0, :cond_7

    .line 1295
    array-length v0, v12

    if-ge v8, v0, :cond_6

    .line 1296
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "%."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, v12, v8

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    .line 1294
    :goto_5
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    .line 1290
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .end local v8    # "i":I
    .end local v12    # "whereArgs3":[Ljava/lang/String;
    :cond_5
    const-string v0, ";"

    invoke-virtual {v7, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .restart local v12    # "whereArgs3":[Ljava/lang/String;
    goto :goto_3

    .line 1298
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    .restart local v8    # "i":I
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v1, v12

    sub-int v1, v8, v1

    aget-object v1, v11, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    goto :goto_5

    .line 1301
    :cond_7
    const-string v0, "("

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1303
    const/4 v8, 0x0

    :goto_6
    array-length v0, v4

    if-ge v8, v0, :cond_b

    .line 1305
    array-length v0, v12

    add-int/lit8 v0, v0, -0x1

    if-ge v8, v0, :cond_8

    .line 1306
    const-string v0, "_data LIKE ? OR "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1303
    :goto_7
    add-int/lit8 v8, v8, 0x1

    goto :goto_6

    .line 1307
    :cond_8
    array-length v0, v12

    if-ge v8, v0, :cond_9

    .line 1308
    const-string v0, "_data LIKE ? ) AND ("

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    .line 1309
    :cond_9
    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    if-ge v8, v0, :cond_a

    .line 1310
    const-string v0, "mime_type LIKE ? OR "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    .line 1312
    :cond_a
    const-string v0, "mime_type LIKE? ) AND "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    .line 1317
    .end local v11    # "whereArgs2":[Ljava/lang/String;
    .end local v12    # "whereArgs3":[Ljava/lang/String;
    :cond_b
    const-string v0, "format != 12289 "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1319
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1322
    .local v3, "where":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowHiddenFileStatus()Z

    move-result v0

    if-nez v0, :cond_c

    .line 1323
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "AND NOT (SUBSTR(LOWER(_data),LENGTH(RTRIM(LOWER(_data),"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->FILE_NAME_FIND_FILTER:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "))) LIKE \'/.%\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1326
    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "external"

    invoke-static {v1}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mDocumentsProjection:[Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1332
    return-object v6
.end method

.method private getFrequentlyUsedFolderList()Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1442
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Folders;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    const-string v3, "NOT count_used=0"

    const-string v5, "count_used DESC"

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private getImageList()Landroid/database/Cursor;
    .locals 14

    .prologue
    .line 898
    const/4 v6, 0x0

    .line 900
    .local v6, "cursor":Landroid/database/Cursor;
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentSortBy:I

    iget v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentInOrder:I

    const/4 v2, 0x1

    const/4 v13, 0x0

    invoke-static {v0, v1, v2, v13}, Lcom/sec/android/app/myfiles/utils/MyFilesDB;->getOrderBy(IIIZ)Ljava/lang/String;

    move-result-object v5

    .line 902
    .local v5, "orderBy":Ljava/lang/String;
    const/4 v4, 0x0

    .line 904
    .local v4, "whereArgs":[Ljava/lang/String;
    const/4 v11, 0x0

    .line 906
    .local v11, "whereArgs2":[Ljava/lang/String;
    const/4 v12, 0x0

    .line 908
    .local v12, "whereArgs3":[Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 910
    .local v10, "sb":Ljava/lang/StringBuilder;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getmMimeTypeForCategory()Ljava/lang/String;

    move-result-object v9

    .line 911
    .local v9, "mimeType":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getmExtensionForCategory()Ljava/lang/String;

    move-result-object v7

    .line 912
    .local v7, "extension":Ljava/lang/String;
    const/4 v3, 0x0

    .line 914
    .local v3, "where":Ljava/lang/String;
    const-string v0, "*"

    invoke-virtual {v9, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 915
    const-string v9, ""

    .line 917
    :cond_0
    const-string v0, "*"

    invoke-virtual {v7, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 918
    const-string v7, ""

    .line 920
    :cond_1
    const-string v0, ""

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, ""

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 921
    :cond_2
    const-string v0, ";"

    invoke-virtual {v9, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 922
    const-string v0, ";"

    invoke-virtual {v7, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 923
    const-string v0, ""

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 924
    array-length v0, v11

    new-array v4, v0, [Ljava/lang/String;

    .line 926
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    array-length v0, v4

    if-ge v8, v0, :cond_3

    .line 927
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, v11, v8

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    .line 926
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 930
    :cond_3
    const-string v0, "("

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 931
    const/4 v8, 0x0

    :goto_1
    array-length v0, v4

    if-ge v8, v0, :cond_b

    .line 933
    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    if-ge v8, v0, :cond_4

    .line 934
    const-string v0, "mime_type LIKE ? OR "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 931
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 936
    :cond_4
    const-string v0, "mime_type LIKE ?)"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 941
    .end local v8    # "i":I
    :cond_5
    array-length v0, v12

    array-length v1, v11

    add-int/2addr v0, v1

    new-array v4, v0, [Ljava/lang/String;

    .line 943
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_3
    array-length v0, v4

    if-ge v8, v0, :cond_7

    .line 944
    array-length v0, v12

    if-ge v8, v0, :cond_6

    .line 945
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "%."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, v12, v8

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    .line 943
    :goto_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 947
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v1, v12

    sub-int v1, v8, v1

    aget-object v1, v11, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    goto :goto_4

    .line 951
    :cond_7
    const-string v0, "("

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 952
    const/4 v8, 0x0

    :goto_5
    array-length v0, v4

    if-ge v8, v0, :cond_b

    .line 954
    array-length v0, v12

    add-int/lit8 v0, v0, -0x1

    if-ge v8, v0, :cond_8

    .line 955
    const-string v0, "_data LIKE ? OR "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 952
    :goto_6
    add-int/lit8 v8, v8, 0x1

    goto :goto_5

    .line 956
    :cond_8
    array-length v0, v12

    if-ge v8, v0, :cond_9

    .line 957
    const-string v0, "_data LIKE ? ) AND ("

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 958
    :cond_9
    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    if-ge v8, v0, :cond_a

    .line 959
    const-string v0, "mime_type LIKE ? OR "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 961
    :cond_a
    const-string v0, "mime_type LIKE? )"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 967
    :cond_b
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 969
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowHiddenFileStatus()Z

    move-result v0

    if-nez v0, :cond_c

    .line 970
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "AND NOT (SUBSTR(LOWER(_data),LENGTH(RTRIM(LOWER(_data),"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->FILE_NAME_FIND_FILTER:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "))) LIKE \'/.%\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 979
    .end local v8    # "i":I
    :cond_c
    :goto_7
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mImageProjection:[Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1043
    return-object v6

    .line 973
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowHiddenFileStatus()Z

    move-result v0

    if-nez v0, :cond_c

    .line 974
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NOT (SUBSTR(LOWER(_data),LENGTH(RTRIM(LOWER(_data),"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->FILE_NAME_FIND_FILTER:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "))) LIKE \'/.%\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_7
.end method

.method private getMusicList()Landroid/database/Cursor;
    .locals 14

    .prologue
    const/4 v0, 0x1

    .line 1141
    const/4 v6, 0x0

    .line 1143
    .local v6, "cursor":Landroid/database/Cursor;
    iget v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentSortBy:I

    iget v2, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentInOrder:I

    invoke-static {v1, v2, v0, v0}, Lcom/sec/android/app/myfiles/utils/MyFilesDB;->getOrderBy(IIIZ)Ljava/lang/String;

    move-result-object v5

    .line 1145
    .local v5, "orderBy":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1147
    .local v4, "whereArgs":[Ljava/lang/String;
    const/4 v11, 0x0

    .line 1149
    .local v11, "whereArgs2":[Ljava/lang/String;
    const/4 v12, 0x0

    .line 1151
    .local v12, "whereArgs3":[Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 1153
    .local v10, "sb":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getmMimeTypeForCategory()Ljava/lang/String;

    move-result-object v9

    .line 1155
    .local v9, "mimeType":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getmExtensionForCategory()Ljava/lang/String;

    move-result-object v7

    .line 1157
    .local v7, "extension":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1159
    .local v3, "where":Ljava/lang/String;
    const-string v1, "*"

    invoke-virtual {v9, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1160
    const-string v9, ""

    .line 1162
    :cond_0
    const-string v1, "*"

    invoke-virtual {v7, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1163
    const-string v7, ""

    .line 1165
    :cond_1
    const-string v1, ""

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, ""

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 1166
    :cond_2
    const-string v1, ";"

    invoke-virtual {v9, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 1167
    const-string v1, ";"

    invoke-virtual {v7, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 1169
    const-string v1, ""

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1170
    array-length v1, v11

    new-array v4, v1, [Ljava/lang/String;

    .line 1172
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    array-length v1, v4

    if-ge v8, v1, :cond_3

    .line 1173
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v2, v11, v8

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v8

    .line 1172
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1175
    :cond_3
    const-string v1, "("

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1177
    const/4 v8, 0x0

    :goto_1
    array-length v1, v4

    if-ge v8, v1, :cond_b

    .line 1179
    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    if-ge v8, v1, :cond_4

    .line 1180
    const-string v1, "mime_type LIKE ? OR "

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1177
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 1182
    :cond_4
    const-string v1, "mime_type LIKE ?)"

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1186
    .end local v8    # "i":I
    :cond_5
    array-length v1, v12

    array-length v2, v11

    add-int/2addr v1, v2

    new-array v4, v1, [Ljava/lang/String;

    .line 1188
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_3
    array-length v1, v4

    if-ge v8, v1, :cond_7

    .line 1189
    array-length v1, v12

    if-ge v8, v1, :cond_6

    .line 1190
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "%."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v2, v12, v8

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v8

    .line 1188
    :goto_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 1192
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, v12

    sub-int v2, v8, v2

    aget-object v2, v11, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v8

    goto :goto_4

    .line 1196
    :cond_7
    const-string v1, "("

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1197
    const/4 v8, 0x0

    :goto_5
    array-length v1, v4

    if-ge v8, v1, :cond_b

    .line 1199
    array-length v1, v12

    add-int/lit8 v1, v1, -0x1

    if-ge v8, v1, :cond_8

    .line 1200
    const-string v1, "_data LIKE ? OR "

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1197
    :goto_6
    add-int/lit8 v8, v8, 0x1

    goto :goto_5

    .line 1201
    :cond_8
    array-length v1, v12

    if-ge v8, v1, :cond_9

    .line 1202
    const-string v1, "_data LIKE ? ) AND ("

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 1203
    :cond_9
    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    if-ge v8, v1, :cond_a

    .line 1204
    const-string v1, "mime_type LIKE ? OR "

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 1206
    :cond_a
    const-string v1, "mime_type LIKE? )"

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 1211
    :cond_b
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1212
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowHiddenFileStatus()Z

    move-result v1

    if-nez v1, :cond_c

    .line 1213
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "AND NOT (SUBSTR(LOWER(_data),LENGTH(RTRIM(LOWER(_data),"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->FILE_NAME_FIND_FILTER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "))) LIKE \'/.%\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1222
    .end local v8    # "i":I
    :cond_c
    :goto_7
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mRestrictDRM:Z

    if-eqz v1, :cond_11

    .line 1223
    if-eqz v3, :cond_f

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_f

    .line 1224
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "AND (is_drm NOT LIKE ? OR is_drm IS NULL)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1227
    :goto_8
    if-eqz v4, :cond_d

    array-length v0, v4

    add-int/lit8 v0, v0, 0x1

    :cond_d
    new-array v13, v0, [Ljava/lang/String;

    .line 1228
    .local v13, "whereArgsDrm":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 1229
    .restart local v8    # "i":I
    if-eqz v4, :cond_10

    .line 1230
    const/4 v8, 0x0

    :goto_9
    array-length v0, v4

    if-ge v8, v0, :cond_10

    .line 1231
    aget-object v0, v4, v8

    aput-object v0, v13, v8

    .line 1230
    add-int/lit8 v8, v8, 0x1

    goto :goto_9

    .line 1216
    .end local v8    # "i":I
    .end local v13    # "whereArgsDrm":[Ljava/lang/String;
    :cond_e
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowHiddenFileStatus()Z

    move-result v1

    if-nez v1, :cond_c

    .line 1217
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NOT (SUBSTR(LOWER(_data),LENGTH(RTRIM(LOWER(_data),"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->FILE_NAME_FIND_FILTER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "))) LIKE \'/.%\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_7

    .line 1226
    :cond_f
    const-string v3, "(is_drm NOT LIKE ? OR is_drm IS NULL)"

    goto :goto_8

    .line 1232
    .restart local v8    # "i":I
    .restart local v13    # "whereArgsDrm":[Ljava/lang/String;
    :cond_10
    const-string v0, "1"

    aput-object v0, v13, v8

    .line 1233
    move-object v4, v13

    .line 1236
    .end local v8    # "i":I
    .end local v13    # "whereArgsDrm":[Ljava/lang/String;
    :cond_11
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mMusicProjection:[Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1242
    return-object v6
.end method

.method private getRecentlyFileList()Landroid/database/Cursor;
    .locals 20

    .prologue
    .line 1338
    const/4 v9, 0x0

    .line 1342
    .local v9, "cursor":Landroid/database/Cursor;
    const-string v7, "date_used DESC LIMIT 10"

    .line 1345
    .local v7, "orderBy":Ljava/lang/String;
    const/4 v5, 0x0

    .line 1347
    .local v5, "where":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowHiddenFileStatus()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1348
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NOT (SUBSTR(LOWER(_data),LENGTH(RTRIM(LOWER(_data),"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->FILE_NAME_FIND_FILTER:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "))) LIKE \'/.%\')"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1351
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mRecentlyProjection:[Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1357
    new-instance v15, Landroid/database/MatrixCursor;

    sget-object v2, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mRecentlyProjection:[Ljava/lang/String;

    invoke-direct {v15, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 1358
    .local v15, "matrixCursor":Landroid/database/MatrixCursor;
    if-eqz v9, :cond_e

    .line 1359
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 1360
    .local v14, "iDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 1361
    .local v8, "builder":Ljava/lang/StringBuilder;
    const-string v16, ""

    .line 1362
    .local v16, "preFix":Ljava/lang/String;
    :cond_1
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1363
    const-string v2, "_data"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1364
    .local v11, "filePath":Ljava/lang/String;
    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1365
    .local v10, "f":Ljava/io/File;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getSecretModeState()Z

    move-result v2

    if-nez v2, :cond_2

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-eqz v2, :cond_4

    :cond_2
    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v2, :cond_4

    .line 1366
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1367
    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "_data"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_size"

    invoke-interface {v9, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v9, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ""

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "date_modified"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v15, v2}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0

    .line 1373
    :cond_3
    const-string v2, "_id"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1375
    :cond_4
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getSecretModeState()Z

    move-result v2

    if-nez v2, :cond_6

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-nez v2, :cond_6

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v2, :cond_6

    .line 1376
    sget-object v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v11, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1377
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1378
    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "_data"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_size"

    invoke-interface {v9, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v9, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ""

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "date_modified"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v15, v2}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1384
    :cond_5
    const-string v2, "_id"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1387
    :cond_6
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getSecretModeState()Z

    move-result v2

    if-nez v2, :cond_7

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-eqz v2, :cond_9

    :cond_7
    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-nez v2, :cond_9

    .line 1388
    const-string v2, "/storage/extSdCard"

    invoke-virtual {v11, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1389
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1390
    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "_data"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_size"

    invoke-interface {v9, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v9, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ""

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "date_modified"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v15, v2}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1396
    :cond_8
    const-string v2, "_id"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1399
    :cond_9
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getSecretModeState()Z

    move-result v2

    if-nez v2, :cond_1

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-nez v2, :cond_1

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-nez v2, :cond_1

    .line 1400
    sget-object v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v11, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "/storage/extSdCard"

    invoke-virtual {v11, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1401
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1402
    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "_data"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_size"

    invoke-interface {v9, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v9, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ""

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "date_modified"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v15, v2}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1408
    :cond_a
    const-string v2, "_id"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1414
    .end local v10    # "f":Ljava/io/File;
    .end local v11    # "filePath":Ljava/lang/String;
    :cond_b
    invoke-virtual {v14}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_d

    .line 1415
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 1417
    .local v17, "s":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1418
    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1419
    const-string v16, ","

    .line 1420
    goto :goto_1

    .line 1421
    .end local v17    # "s":Ljava/lang/String;
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id IN ("

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ")"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v12

    .line 1422
    .local v12, "i":I
    const/4 v2, 0x0

    const-string v3, "AdapterManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "rows deleted from "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 1424
    .end local v12    # "i":I
    .end local v13    # "i$":Ljava/util/Iterator;
    :cond_d
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 1426
    .end local v8    # "builder":Ljava/lang/StringBuilder;
    .end local v14    # "iDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v16    # "preFix":Ljava/lang/String;
    :cond_e
    return-object v15
.end method

.method private getRecentlyUsedFolderList()Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1432
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Folders;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    const-string v3, "NOT date_used=0"

    const-string v5, "date_used DESC"

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private getVideoList()Landroid/database/Cursor;
    .locals 14

    .prologue
    .line 1049
    const/4 v6, 0x0

    .line 1051
    .local v6, "cursor":Landroid/database/Cursor;
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentSortBy:I

    iget v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentInOrder:I

    const/4 v2, 0x1

    const/4 v13, 0x0

    invoke-static {v0, v1, v2, v13}, Lcom/sec/android/app/myfiles/utils/MyFilesDB;->getOrderBy(IIIZ)Ljava/lang/String;

    move-result-object v5

    .line 1053
    .local v5, "orderBy":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1055
    .local v4, "whereArgs":[Ljava/lang/String;
    const/4 v11, 0x0

    .line 1057
    .local v11, "whereArgs2":[Ljava/lang/String;
    const/4 v12, 0x0

    .line 1059
    .local v12, "whereArgs3":[Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 1061
    .local v10, "sb":Ljava/lang/StringBuilder;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getmMimeTypeForCategory()Ljava/lang/String;

    move-result-object v9

    .line 1063
    .local v9, "mimeType":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getmExtensionForCategory()Ljava/lang/String;

    move-result-object v7

    .line 1065
    .local v7, "extension":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1067
    .local v3, "where":Ljava/lang/String;
    const-string v0, "*"

    invoke-virtual {v9, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1068
    const-string v9, ""

    .line 1070
    :cond_0
    const-string v0, "*"

    invoke-virtual {v7, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1071
    const-string v7, ""

    .line 1073
    :cond_1
    const-string v0, ""

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, ""

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 1074
    :cond_2
    const-string v0, ";"

    invoke-virtual {v9, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 1075
    const-string v0, ";"

    invoke-virtual {v7, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 1077
    const-string v0, ""

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1078
    array-length v0, v11

    new-array v4, v0, [Ljava/lang/String;

    .line 1080
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    array-length v0, v4

    if-ge v8, v0, :cond_3

    .line 1081
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, v11, v8

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    .line 1080
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1084
    :cond_3
    const-string v0, "("

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1086
    const/4 v8, 0x0

    :goto_1
    array-length v0, v4

    if-ge v8, v0, :cond_b

    .line 1087
    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    if-ge v8, v0, :cond_4

    .line 1088
    const-string v0, "mime_type LIKE ? OR "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1086
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 1090
    :cond_4
    const-string v0, "mime_type LIKE ?)"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1094
    .end local v8    # "i":I
    :cond_5
    array-length v0, v12

    array-length v1, v11

    add-int/2addr v0, v1

    new-array v4, v0, [Ljava/lang/String;

    .line 1096
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_3
    array-length v0, v4

    if-ge v8, v0, :cond_7

    .line 1097
    array-length v0, v12

    if-ge v8, v0, :cond_6

    .line 1098
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "%."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, v12, v8

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    .line 1096
    :goto_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 1100
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v1, v12

    sub-int v1, v8, v1

    aget-object v1, v11, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    goto :goto_4

    .line 1104
    :cond_7
    const-string v0, "("

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1105
    const/4 v8, 0x0

    :goto_5
    array-length v0, v4

    if-ge v8, v0, :cond_b

    .line 1107
    array-length v0, v12

    add-int/lit8 v0, v0, -0x1

    if-ge v8, v0, :cond_8

    .line 1108
    const-string v0, "_data LIKE ? OR "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1105
    :goto_6
    add-int/lit8 v8, v8, 0x1

    goto :goto_5

    .line 1109
    :cond_8
    array-length v0, v12

    if-ge v8, v0, :cond_9

    .line 1110
    const-string v0, "_data LIKE ? ) AND ("

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 1111
    :cond_9
    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    if-ge v8, v0, :cond_a

    .line 1112
    const-string v0, "mime_type LIKE ? OR "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 1114
    :cond_a
    const-string v0, "mime_type LIKE? )"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 1118
    :cond_b
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1119
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowHiddenFileStatus()Z

    move-result v0

    if-nez v0, :cond_c

    .line 1120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "AND NOT (SUBSTR(LOWER(_data),LENGTH(RTRIM(LOWER(_data),"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->FILE_NAME_FIND_FILTER:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "))) LIKE \'/.%\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1129
    .end local v8    # "i":I
    :cond_c
    :goto_7
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mVideoProjection:[Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1135
    return-object v6

    .line 1123
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowHiddenFileStatus()Z

    move-result v0

    if-nez v0, :cond_c

    .line 1124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NOT (SUBSTR(LOWER(_data),LENGTH(RTRIM(LOWER(_data),"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->FILE_NAME_FIND_FILTER:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "))) LIKE \'/.%\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_7
.end method

.method private isThumbnailListTransition(II)Z
    .locals 2
    .param p1, "oldMode"    # I
    .param p2, "newMode"    # I

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 1569
    if-ne p1, v1, :cond_1

    if-eqz p2, :cond_0

    if-ne p2, v0, :cond_1

    .line 1576
    :cond_0
    :goto_0
    return v0

    .line 1572
    :cond_1
    if-eqz p1, :cond_2

    if-ne p1, v0, :cond_3

    :cond_2
    if-eq p2, v1, :cond_0

    .line 1576
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public finishSelectMode()V
    .locals 5

    .prologue
    .line 1612
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSelectModeAdapterMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 1614
    .local v2, "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1618
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1620
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSelectModeAdapterMap:Ljava/util/HashMap;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/ISelectModeAdapter;

    .line 1622
    .local v0, "adapter":Lcom/sec/android/app/myfiles/adapter/ISelectModeAdapter;
    if-eqz v0, :cond_0

    .line 1624
    invoke-interface {v0}, Lcom/sec/android/app/myfiles/adapter/ISelectModeAdapter;->finishSelectMode()V

    goto :goto_0

    .line 1627
    .end local v0    # "adapter":Lcom/sec/android/app/myfiles/adapter/ISelectModeAdapter;
    :cond_1
    return-void
.end method

.method public finishSelectMode(I)V
    .locals 3
    .param p1, "fragmentId"    # I

    .prologue
    .line 1632
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSelectModeAdapterMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/ISelectModeAdapter;

    .line 1634
    .local v0, "adapter":Lcom/sec/android/app/myfiles/adapter/ISelectModeAdapter;
    if-eqz v0, :cond_0

    .line 1636
    invoke-interface {v0}, Lcom/sec/android/app/myfiles/adapter/ISelectModeAdapter;->finishSelectMode()V

    .line 1638
    :cond_0
    return-void
.end method

.method public getAdapter(I)Landroid/widget/BaseAdapter;
    .locals 3
    .param p1, "fragmentId"    # I

    .prologue
    .line 188
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mAdapterMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    .line 190
    .local v0, "adapter":Landroid/widget/BaseAdapter;
    if-nez v0, :cond_1

    .line 192
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->createAdapter(I)Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;

    move-result-object v0

    .line 194
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mAdapterMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    :cond_0
    :goto_0
    return-object v0

    .line 196
    :cond_1
    const/16 v1, 0x25

    if-eq p1, v1, :cond_0

    .line 198
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->refreshAdapter(I)V

    goto :goto_0
.end method

.method public getAdapter(IZ)Landroid/widget/BaseAdapter;
    .locals 3
    .param p1, "fragmentId"    # I
    .param p2, "refresh"    # Z

    .prologue
    .line 217
    if-eqz p2, :cond_1

    .line 219
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getAdapter(I)Landroid/widget/BaseAdapter;

    move-result-object v0

    .line 233
    :cond_0
    :goto_0
    return-object v0

    .line 223
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mAdapterMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    .line 225
    .local v0, "adapter":Landroid/widget/BaseAdapter;
    if-nez v0, :cond_0

    .line 227
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->createAdapter(I)Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;

    move-result-object v0

    .line 229
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mAdapterMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public getCategoryCursor(I)Landroid/database/Cursor;
    .locals 2
    .param p1, "fragmentId"    # I

    .prologue
    .line 240
    packed-switch p1, :pswitch_data_0

    .line 258
    :pswitch_0
    new-instance v0, Landroid/database/MatrixCursor;

    sget-object v1, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mFilesProjection:[Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    :goto_0
    return-object v0

    .line 243
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getImageList()Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 246
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getVideoList()Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 249
    :pswitch_3
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getMusicList()Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 252
    :pswitch_4
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getDocumentList()Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 255
    :pswitch_5
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getRecentlyFileList()Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 240
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public getCreateAdapter(I)Landroid/widget/BaseAdapter;
    .locals 3
    .param p1, "fragmentId"    # I

    .prologue
    .line 205
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mAdapterMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 206
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mAdapterMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->createAdapter(I)Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;

    move-result-object v0

    .line 209
    .local v0, "adapter":Landroid/widget/BaseAdapter;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mAdapterMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    return-object v0
.end method

.method public initCursor()V
    .locals 1

    .prologue
    .line 771
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 773
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 774
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCursor:Landroid/database/Cursor;

    .line 776
    :cond_0
    return-void
.end method

.method public isSelectMode(I)Z
    .locals 3
    .param p1, "fragmentId"    # I

    .prologue
    .line 1599
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSelectModeAdapterMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/ISelectModeAdapter;

    .line 1601
    .local v0, "adapter":Lcom/sec/android/app/myfiles/adapter/ISelectModeAdapter;
    if-eqz v0, :cond_0

    .line 1603
    invoke-interface {v0}, Lcom/sec/android/app/myfiles/adapter/ISelectModeAdapter;->isSelectMode()Z

    move-result v1

    .line 1606
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public refreshAdapter(I)V
    .locals 9
    .param p1, "fragmentId"    # I

    .prologue
    const/4 v8, 0x0

    .line 606
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentSortBy()I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentSortBy:I

    .line 608
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentInOrder()I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentInOrder:I

    .line 610
    const/4 v2, 0x0

    .line 612
    .local v2, "cursor":Landroid/database/Cursor;
    const-string v5, "AdapterManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "refreshAdapter() fragmentId : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 614
    if-nez p1, :cond_1

    .line 616
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mAdapterMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    .line 618
    .local v0, "adapter":Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->onRefresh()V

    .line 767
    .end local v0    # "adapter":Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
    :cond_0
    :goto_0
    return-void

    .line 620
    :cond_1
    const/16 v5, 0x1a

    if-eq p1, v5, :cond_0

    const/16 v5, 0x21

    if-eq p1, v5, :cond_0

    .line 627
    const/4 v5, 0x6

    if-ne p1, v5, :cond_2

    .line 629
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mAdapterMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    .line 631
    .local v3, "downloadedAppsAdatper":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;
    iget v5, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentSortBy:I

    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentInOrder:I

    invoke-virtual {v3, v5, v6}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->setSortBy(II)V

    .line 633
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->onRefresh()V

    goto :goto_0

    .line 637
    .end local v3    # "downloadedAppsAdatper":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mAdapterMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    .line 639
    .local v1, "browserAdapter":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
    const/4 v4, 0x0

    .line 641
    .local v4, "navi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    if-eqz v1, :cond_0

    .line 643
    sparse-switch p1, :sswitch_data_0

    .line 762
    :cond_3
    :goto_1
    if-eqz v2, :cond_0

    .line 763
    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->changeCursor(Landroid/database/Cursor;)V

    goto :goto_0

    .line 646
    :sswitch_0
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v4

    .line 647
    if-eqz v4, :cond_0

    .line 648
    iget v5, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentSortBy:I

    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentInOrder:I

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setSortBy(II)V

    .line 649
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->refreshNavigation()V

    goto :goto_0

    .line 656
    :sswitch_1
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v4

    .line 657
    if-eqz v4, :cond_3

    .line 658
    iget v5, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentSortBy:I

    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentInOrder:I

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setSortBy(II)V

    .line 659
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->refreshNavigation()V

    .line 660
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v2

    goto :goto_1

    .line 665
    :sswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getImageList()Landroid/database/Cursor;

    move-result-object v2

    .line 666
    goto :goto_1

    .line 669
    :sswitch_3
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getVideoList()Landroid/database/Cursor;

    move-result-object v2

    .line 670
    goto :goto_1

    .line 673
    :sswitch_4
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getMusicList()Landroid/database/Cursor;

    move-result-object v2

    .line 674
    goto :goto_1

    .line 677
    :sswitch_5
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getDocumentList()Landroid/database/Cursor;

    move-result-object v2

    .line 678
    goto :goto_1

    .line 681
    :sswitch_6
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getRecentlyFileList()Landroid/database/Cursor;

    move-result-object v2

    .line 682
    goto :goto_1

    .line 685
    :sswitch_7
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v4

    .line 686
    if-eqz v4, :cond_3

    .line 687
    iget v5, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentSortBy:I

    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentInOrder:I

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setSortBy(II)V

    .line 688
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->refreshNavigation()V

    .line 689
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v2

    goto :goto_1

    .line 695
    :sswitch_8
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 696
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v4

    .line 697
    const-string v5, "AdapterManager"

    const-string v6, "AdapterManager refreshAdapter() Constant.BAIDU"

    invoke-static {v8, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 698
    if-eqz v4, :cond_3

    .line 699
    iget v5, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentSortBy:I

    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentInOrder:I

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setSortBy(II)V

    .line 700
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->refreshNavigation()V

    .line 701
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v2

    goto :goto_1

    .line 708
    :sswitch_9
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v4

    .line 709
    if-eqz v4, :cond_3

    .line 710
    iget v5, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentSortBy:I

    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentInOrder:I

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setSortBy(II)V

    move-object v5, v4

    .line 711
    check-cast v5, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->refreshNavigation()V

    .line 712
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v2

    goto/16 :goto_1

    .line 717
    :sswitch_a
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v4

    .line 718
    if-eqz v4, :cond_3

    .line 719
    iget v5, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentSortBy:I

    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentInOrder:I

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setSortBy(II)V

    .line 720
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->refreshNavigation()V

    .line 721
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v2

    goto/16 :goto_1

    .line 726
    :sswitch_b
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v4

    .line 727
    if-eqz v4, :cond_3

    .line 728
    iget v5, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentSortBy:I

    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentInOrder:I

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setSortBy(II)V

    .line 729
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->refreshNavigation()V

    .line 730
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v2

    goto/16 :goto_1

    .line 735
    :sswitch_c
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v4

    .line 736
    if-eqz v4, :cond_3

    .line 737
    iget v5, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentSortBy:I

    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentInOrder:I

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setSortBy(II)V

    .line 738
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->refreshNavigation()V

    .line 739
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v2

    goto/16 :goto_1

    .line 744
    :sswitch_d
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v4

    .line 745
    if-eqz v4, :cond_3

    .line 746
    iget v5, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentSortBy:I

    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentInOrder:I

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setSortBy(II)V

    .line 747
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->refreshNavigation()V

    .line 748
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v2

    goto/16 :goto_1

    .line 753
    :sswitch_e
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v4

    .line 754
    if-eqz v4, :cond_3

    .line 755
    iget v5, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentSortBy:I

    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentInOrder:I

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setSortBy(II)V

    .line 756
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v2

    goto/16 :goto_1

    .line 643
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x7 -> :sswitch_6
        0x8 -> :sswitch_7
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0x10 -> :sswitch_b
        0x12 -> :sswitch_1
        0x13 -> :sswitch_9
        0x16 -> :sswitch_1
        0x1f -> :sswitch_8
        0x23 -> :sswitch_c
        0x24 -> :sswitch_d
        0x25 -> :sswitch_0
        0x28 -> :sswitch_e
        0x201 -> :sswitch_1
    .end sparse-switch
.end method

.method public setBrowserAdapterViewMode(I)V
    .locals 11
    .param p1, "mode"    # I

    .prologue
    const/16 v9, 0x13

    .line 1452
    const/4 v6, -0x1

    .line 1454
    .local v6, "layoutResId":I
    packed-switch p1, :pswitch_data_0

    .line 1469
    :goto_0
    const/4 v8, -0x1

    if-ne v6, v8, :cond_1

    .line 1470
    const/4 v8, 0x0

    const-string v9, "AdapterManager"

    const-string v10, "layoutResId is -1"

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 1556
    :cond_0
    return-void

    .line 1457
    :pswitch_0
    const v6, 0x7f040015

    .line 1458
    goto :goto_0

    .line 1461
    :pswitch_1
    const v6, 0x7f040015

    .line 1462
    goto :goto_0

    .line 1465
    :pswitch_2
    const v6, 0x7f04000f

    goto :goto_0

    .line 1474
    :cond_1
    new-array v4, v9, [I

    fill-array-data v4, :array_0

    .line 1498
    .local v4, "keys":[I
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1499
    new-array v5, v9, [I

    fill-array-data v5, :array_1

    .line 1521
    .local v5, "keysChina":[I
    move-object v4, v5

    .line 1526
    .end local v5    # "keysChina":[I
    :cond_2
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    array-length v8, v4

    if-ge v3, v8, :cond_0

    .line 1528
    iget-object v8, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mAdapterMap:Ljava/util/HashMap;

    aget v9, v4, v3

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/BaseAdapter;

    .line 1530
    .local v1, "adapter":Landroid/widget/BaseAdapter;
    if-eqz v1, :cond_5

    instance-of v8, v1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v8, :cond_5

    move-object v0, v1

    .line 1531
    check-cast v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    .line 1535
    .local v0, "absadapter":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
    instance-of v8, v1, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    if-eqz v8, :cond_4

    iget v8, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mViewMode:I

    invoke-direct {p0, v8, p1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->isThumbnailListTransition(II)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1537
    check-cast v1, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    .end local v1    # "adapter":Landroid/widget/BaseAdapter;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v8

    invoke-virtual {v1, v8, p1}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->createNewCursor(Landroid/database/Cursor;I)Landroid/database/Cursor;

    move-result-object v2

    .line 1539
    .local v2, "cursor":Landroid/database/Cursor;
    invoke-virtual {v0, p1, v6}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->setViewMode(II)V

    .line 1540
    invoke-virtual {v0, v2}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 1526
    .end local v0    # "absadapter":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
    .end local v2    # "cursor":Landroid/database/Cursor;
    :cond_3
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1542
    .restart local v0    # "absadapter":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
    .restart local v1    # "adapter":Landroid/widget/BaseAdapter;
    :cond_4
    invoke-virtual {v0, p1, v6}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->setViewMode(II)V

    .line 1543
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyDataSetChanged()V

    goto :goto_2

    .line 1547
    .end local v0    # "absadapter":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
    :cond_5
    if-eqz v1, :cond_3

    move-object v7, v1

    .line 1549
    check-cast v7, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    .line 1551
    .local v7, "mDownloadedAppAdapter":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;
    invoke-virtual {v7, p1}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->setViewMode(I)V

    .line 1553
    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->notifyDataSetChanged()V

    goto :goto_2

    .line 1454
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 1474
    :array_0
    .array-data 4
        0x201
        0x2
        0x3
        0x4
        0x5
        0x7
        0x6
        0x8
        0x9
        0xa
        0x10
        0x12
        0x14
        0x11
        0x23
        0x24
        0x28
        0x25
        0x13
    .end array-data

    .line 1499
    :array_1
    .array-data 4
        0x201
        0x2
        0x3
        0x4
        0x5
        0x7
        0x6
        0x1f
        0x9
        0xa
        0x10
        0x12
        0x20
        0x11
        0x23
        0x24
        0x28
        0x25
        0x13
    .end array-data
.end method

.method public setSortBy(II)V
    .locals 6
    .param p1, "sortBy"    # I
    .param p2, "inOrder"    # I

    .prologue
    const/16 v5, 0xb

    .line 1643
    iget v4, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentSortBy:I

    if-ne v4, p1, :cond_1

    iget v4, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentInOrder:I

    if-ne v4, p2, :cond_1

    .line 1679
    :cond_0
    return-void

    .line 1648
    :cond_1
    iput p1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentSortBy:I

    .line 1650
    iput p2, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mCurrentInOrder:I

    .line 1652
    new-array v2, v5, [I

    fill-array-data v2, :array_0

    .line 1656
    .local v2, "keys":[I
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1657
    new-array v3, v5, [I

    fill-array-data v3, :array_1

    .line 1659
    .local v3, "keysChina":[I
    move-object v2, v3

    .line 1664
    .end local v3    # "keysChina":[I
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v2

    if-ge v1, v4, :cond_0

    .line 1666
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mAdapterMap:Ljava/util/HashMap;

    aget v5, v2, v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;

    .line 1668
    .local v0, "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    if-nez v0, :cond_3

    .line 1664
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1674
    :cond_3
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->setSortBy(II)V

    .line 1676
    aget v4, v2, v1

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->refreshAdapter(I)V

    goto :goto_1

    .line 1652
    :array_0
    .array-data 4
        0x201
        0x2
        0x3
        0x4
        0x5
        0x9
        0x6
        0x8
        0x25
        0xa
        0x28
    .end array-data

    .line 1657
    :array_1
    .array-data 4
        0x201
        0x2
        0x3
        0x4
        0x5
        0x9
        0x6
        0x1f
        0x25
        0xa
        0x28
    .end array-data
.end method

.method public startSelectMode(IIII)V
    .locals 3
    .param p1, "fragmentId"    # I
    .param p2, "selectionType"    # I
    .param p3, "initialSelectPosition"    # I
    .param p4, "from"    # I

    .prologue
    .line 1581
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mAdapterMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/ISelectModeAdapter;

    .line 1583
    .local v0, "adapter":Lcom/sec/android/app/myfiles/adapter/ISelectModeAdapter;
    if-eqz v0, :cond_0

    .line 1585
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->mSelectModeAdapterMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1587
    invoke-interface {v0, p2, p4}, Lcom/sec/android/app/myfiles/adapter/ISelectModeAdapter;->startSelectMode(II)V

    .line 1588
    const/4 v1, -0x2

    if-eq p3, v1, :cond_0

    .line 1589
    invoke-interface {v0, p3}, Lcom/sec/android/app/myfiles/adapter/ISelectModeAdapter;->selectItem(I)V

    .line 1592
    :cond_0
    return-void
.end method
