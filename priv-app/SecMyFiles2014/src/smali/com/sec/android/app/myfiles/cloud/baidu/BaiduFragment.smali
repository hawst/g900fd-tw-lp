.class public Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;
.source "BaiduFragment.java"


# static fields
.field private static final MODULE:Ljava/lang/String; = "BaiduFragment"

.field private static final REQUEST_CREATE_FOLDER:I = 0x4

.field private static final REQUEST_EXTRACT:I = 0x6

.field private static final REQUEST_ZIP:I = 0x5


# instance fields
.field private context:Landroid/content/Context;

.field mBaiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

.field private mBaiduCopyMoveSourceFolderPath:Ljava/lang/String;

.field private mBaiduPathIndicator:Ljava/lang/String;

.field private mBaiduRoot:Ljava/lang/String;

.field private mBaiduRootReplace:Ljava/lang/String;

.field private mFromSelector:Z

.field private mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

.field private mSearchFromIndex:I

.field private mShortcutIndex:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 95
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;-><init>()V

    .line 80
    iput-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduPathIndicator:Ljava/lang/String;

    .line 82
    const-string v0, "/Baidu"

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduRoot:Ljava/lang/String;

    .line 83
    const-string v0, "Baidu/"

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduRootReplace:Ljava/lang/String;

    .line 85
    iput-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    .line 89
    iput-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->context:Landroid/content/Context;

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mFromSelector:Z

    .line 1338
    new-instance v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$6;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

    .line 97
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/view/MyFilesTreeView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Landroid/os/IBinder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->moveDropboxToSecretBox()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Landroid/os/IBinder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Landroid/os/IBinder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/samsung/android/privatemode/PrivateModeManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;Landroid/os/IBinder;)Landroid/os/IBinder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;
    .param p1, "x1"    # Landroid/os/IBinder;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/view/MyFilesTreeView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduPathIndicator:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduPathIndicator:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduRoot:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/view/PathIndicator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;Landroid/database/Cursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 69
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->updateAdapter(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 69
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->startDownload(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$3200(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 69
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->startDownload(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$3600(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/view/IDirectDragable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mCurrentListableView:Lcom/sec/android/app/myfiles/view/IDirectDragable;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/view/IDirectDragable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mCurrentListableView:Lcom/sec/android/app/myfiles/view/IDirectDragable;

    return-object v0
.end method

.method static synthetic access$4400(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->isShowTreeView()Z

    move-result v0

    return v0
.end method

.method static synthetic access$4700(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    return-object v0
.end method

.method static synthetic access$4800(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mSideSyncControl:Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;

    return-object v0
.end method

.method static synthetic access$5100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mSideSyncControl:Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;

    return-object v0
.end method

.method static synthetic access$5200(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    return-object v0
.end method

.method static synthetic access$5300(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    return-object v0
.end method

.method static synthetic access$5400(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/view/PathIndicator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    return-object v0
.end method

.method static synthetic access$5500(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    return-object v0
.end method

.method static synthetic access$5600(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$5700(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    return-object v0
.end method

.method static synthetic access$5800(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;Landroid/database/Cursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 69
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->updateAdapter(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$5900()Landroid/view/Menu;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mOptionsMenu:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    return-object v0
.end method

.method static synthetic access$6000()Landroid/view/Menu;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mOptionsMenu:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic access$6100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    return-object v0
.end method

.method static synthetic access$6200()Landroid/view/Menu;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mOptionsMenu:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic access$6300()Landroid/view/Menu;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mOptionsMenu:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic access$6400(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$6500(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method

.method static synthetic access$6600()Landroid/view/Menu;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mOptionsMenu:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic access$6700()Landroid/view/Menu;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mOptionsMenu:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/samsung/android/privatemode/PrivateModeManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;Landroid/os/IBinder;)Landroid/os/IBinder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;
    .param p1, "x1"    # Landroid/os/IBinder;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/samsung/android/privatemode/PrivateModeManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    return-object v0
.end method

.method public static newInstance()Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;
    .locals 1

    .prologue
    .line 100
    new-instance v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;-><init>()V

    return-object v0
.end method

.method private startCompress(Ljava/lang/String;)V
    .locals 9
    .param p1, "targetZipFile"    # Ljava/lang/String;

    .prologue
    .line 906
    const/4 v6, 0x3

    invoke-static {v6}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(I)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v1

    .line 908
    .local v1, "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    const/4 v6, 0x2

    invoke-virtual {v1, p0, v6}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 910
    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v4

    .line 912
    .local v4, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 914
    .local v5, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 916
    .local v3, "selectedItem":Ljava/lang/Object;
    check-cast v3, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v3    # "selectedItem":Ljava/lang/Object;
    iget-object v6, v3, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 919
    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 921
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v6, "src_folder"

    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 923
    const-string v6, "dst_folder"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 925
    const-string v6, "target_datas"

    invoke-virtual {v0, v6, v5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 927
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "compress"

    invoke-virtual {v1, v6, v7}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 928
    return-void
.end method

.method private startExtract(Ljava/lang/String;)V
    .locals 8
    .param p1, "targetFoler"    # Ljava/lang/String;

    .prologue
    .line 933
    const/4 v6, 0x4

    invoke-static {v6}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(I)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v1

    .line 935
    .local v1, "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    const/4 v6, 0x2

    invoke-virtual {v1, p0, v6}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 937
    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v4

    .line 939
    .local v4, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 941
    .local v5, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 943
    .local v3, "selectedItem":Ljava/lang/Object;
    check-cast v3, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v3    # "selectedItem":Ljava/lang/Object;
    iget-object v6, v3, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 946
    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 948
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v6, "src_folder"

    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 950
    const-string v6, "dst_folder"

    invoke-virtual {v0, v6, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 952
    const-string v6, "target_datas"

    invoke-virtual {v0, v6, v5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 954
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "extract"

    invoke-virtual {v1, v6, v7}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 955
    return-void
.end method


# virtual methods
.method public AsyncOpenCompleted()V
    .locals 2

    .prologue
    .line 1405
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v0

    .line 1407
    .local v0, "c":Landroid/database/Cursor;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 1409
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->invalidateOptionsMenu()V

    .line 1411
    return-void
.end method

.method public addCurrentFolderToShortcut()V
    .locals 9

    .prologue
    .line 845
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->addCurrentFolderToShortcut()V

    .line 846
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_6

    .line 847
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mAddShortcutFromActionmode:Z

    if-eqz v0, :cond_b

    .line 848
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .line 849
    .local v8, "item":Ljava/lang/Object;
    check-cast v8, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v8    # "item":Ljava/lang/Object;
    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 850
    .local v1, "path":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 851
    :cond_0
    const-string v1, "Baidu/"

    .line 853
    :cond_1
    const-string v0, "Baidu/"

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 854
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Baidu/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 856
    :cond_2
    sget-char v0, Ljava/io/File;->separatorChar:C

    invoke-virtual {v1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 858
    .local v2, "shortcutName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_3

    .line 859
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v3, 0x1f

    const/4 v4, -0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortCut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/drawable/Drawable;Z)V

    goto :goto_0

    .line 862
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v3, 0x1f

    const/4 v4, -0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortCut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/drawable/Drawable;Z)V

    goto :goto_0

    .line 866
    .end local v1    # "path":Ljava/lang/String;
    .end local v2    # "shortcutName":Ljava/lang/String;
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v0

    const/4 v3, 0x1

    if-le v0, v3, :cond_5

    .line 867
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v0

    sget v3, Lcom/sec/android/app/myfiles/utils/Utils;->mAddShortcutSuccess:I

    if-ne v0, v3, :cond_8

    .line 868
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v3, 0x7f0b0060

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 881
    :cond_5
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    const/16 v3, 0x1a

    invoke-virtual {v0, v3}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->refreshAdapter(I)V

    .line 898
    .end local v7    # "i$":Ljava/util/Iterator;
    :cond_6
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 899
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->finishSelectMode()V

    .line 901
    :cond_7
    return-void

    .line 870
    .restart local v7    # "i$":Ljava/util/Iterator;
    :cond_8
    sget v0, Lcom/sec/android/app/myfiles/utils/Utils;->mAddShortcutSuccess:I

    const/4 v3, 0x1

    if-ne v0, v3, :cond_9

    .line 871
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v3, "1 shortcut added, Existing shortcuts were excluded"

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 873
    :cond_9
    sget v0, Lcom/sec/android/app/myfiles/utils/Utils;->mAddShortcutSuccess:I

    if-nez v0, :cond_a

    .line 874
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v3, "Shortcuts already exist."

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 876
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget v4, Lcom/sec/android/app/myfiles/utils/Utils;->mAddShortcutSuccess:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " shortcuts added, Existing shortcuts were excluded"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 883
    .end local v7    # "i$":Ljava/util/Iterator;
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v1

    .line 884
    .restart local v1    # "path":Ljava/lang/String;
    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_d

    .line 885
    :cond_c
    const-string v1, "Baidu/"

    .line 887
    :cond_d
    const-string v0, "Baidu/"

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 888
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Baidu/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 890
    :cond_e
    sget-char v0, Ljava/io/File;->separatorChar:C

    invoke-virtual {v1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 892
    .restart local v2    # "shortcutName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v3, 0x1f

    const/4 v4, -0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortCut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/drawable/Drawable;Z)V

    .line 895
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    const/16 v3, 0x1a

    invoke-virtual {v0, v3}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->refreshAdapter(I)V

    goto/16 :goto_2
.end method

.method public addShortcutToHome()V
    .locals 6

    .prologue
    .line 1499
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->addShortcutToHome()V

    .line 1500
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v4, v4, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v4, :cond_7

    .line 1501
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v4, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mAddShortcutFromActionmode:Z

    if-eqz v4, :cond_3

    .line 1502
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1503
    .local v1, "item":Ljava/lang/Object;
    check-cast v1, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v1    # "item":Ljava/lang/Object;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 1504
    .local v2, "path":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 1505
    :cond_0
    const-string v2, "Baidu/"

    .line 1507
    :cond_1
    const-string v4, "Baidu/"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1508
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Baidu/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1510
    :cond_2
    sget-char v4, Ljava/io/File;->separatorChar:C

    invoke-virtual {v2, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 1512
    .local v3, "shortcutName":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortcutToHome(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1515
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "path":Ljava/lang/String;
    .end local v3    # "shortcutName":Ljava/lang/String;
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v2

    .line 1516
    .restart local v2    # "path":Ljava/lang/String;
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_5

    .line 1517
    :cond_4
    const-string v2, "Baidu/"

    .line 1519
    :cond_5
    const-string v4, "Baidu/"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 1520
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Baidu/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1522
    :cond_6
    sget-char v4, Ljava/io/File;->separatorChar:C

    invoke-virtual {v2, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 1524
    .restart local v3    # "shortcutName":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortcutToHome(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1527
    .end local v2    # "path":Ljava/lang/String;
    .end local v3    # "shortcutName":Ljava/lang/String;
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->isSelectMode()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1528
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->finishSelectMode()V

    .line 1530
    :cond_8
    return-void
.end method

.method public getOptionsMenuResId(Z)I
    .locals 1
    .param p1, "isSelectMode"    # Z

    .prologue
    .line 961
    if-eqz p1, :cond_1

    .line 963
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getSelectModeFrom()I

    move-result v0

    if-nez v0, :cond_0

    .line 965
    const v0, 0x7f0e0005

    .line 974
    :goto_0
    return v0

    .line 969
    :cond_0
    const v0, 0x7f0e0018

    goto :goto_0

    .line 974
    :cond_1
    const v0, 0x7f0e0004

    goto :goto_0
.end method

.method protected initTreeView()V
    .locals 2

    .prologue
    .line 815
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->initTreeView()V

    .line 817
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->setNavigation(Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 818
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 11
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 690
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 692
    sparse-switch p1, :sswitch_data_0

    .line 811
    :cond_0
    :goto_0
    return-void

    .line 696
    :sswitch_0
    const/4 v7, -0x1

    if-ne p2, v7, :cond_0

    .line 698
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "result_value"

    invoke-virtual {p3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".zip"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 700
    .local v6, "targetZipFile":Ljava/lang/String;
    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->startCompress(Ljava/lang/String;)V

    goto :goto_0

    .line 708
    .end local v6    # "targetZipFile":Ljava/lang/String;
    :sswitch_1
    const/4 v7, -0x1

    if-ne p2, v7, :cond_0

    .line 710
    const-string v7, "result_value"

    invoke-virtual {p3, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 712
    .local v5, "targetFolder":Ljava/lang/String;
    invoke-direct {p0, v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->startExtract(Ljava/lang/String;)V

    goto :goto_0

    .line 719
    .end local v5    # "targetFolder":Ljava/lang/String;
    :sswitch_2
    const/4 v7, -0x1

    if-ne p2, v7, :cond_0

    .line 721
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 722
    const/4 v7, 0x1

    sput-boolean v7, Lcom/sec/android/app/myfiles/utils/Utils;->needUpdate:Z

    .line 723
    const/4 v7, 0x0

    const-string v8, "BaiduFragment"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "REQUEST_CREATE_FOLDER Utils.needUpdate="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-boolean v10, Lcom/sec/android/app/myfiles/utils/Utils;->needUpdate:Z

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 724
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->AsyncOpen()V

    .line 728
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v1

    .line 730
    .local v1, "c":Landroid/database/Cursor;
    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v7, :cond_2

    .line 731
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->updateAdapter(Landroid/database/Cursor;)V

    goto :goto_0

    .line 733
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 742
    .end local v1    # "c":Landroid/database/Cursor;
    :sswitch_3
    const/4 v7, -0x1

    if-ne p2, v7, :cond_0

    .line 744
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 746
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->AsyncOpen()V

    goto/16 :goto_0

    .line 754
    :sswitch_4
    const/4 v7, -0x1

    if-ne p2, v7, :cond_5

    .line 756
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v7

    if-eqz v7, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 758
    const/4 v7, 0x0

    const-string v8, "BaiduFragment"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "REQUEST_FILE_OPERATION Utils.needUpdate="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-boolean v10, Lcom/sec/android/app/myfiles/utils/Utils;->needUpdate:Z

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 759
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->AsyncOpen()V

    .line 763
    :cond_3
    if-eqz p3, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v7, :cond_0

    .line 764
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 765
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 766
    const-string v7, "target_folder"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 767
    .local v4, "path":Ljava/lang/String;
    const-string v7, "FILE_OPERATION"

    const/4 v8, -0x1

    invoke-virtual {p3, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 768
    .local v3, "operationType":I
    const/4 v7, 0x1

    if-eq v3, v7, :cond_4

    if-nez v3, :cond_0

    .line 769
    :cond_4
    const-string v7, "dest_fragment_id"

    const/4 v8, -0x1

    invoke-virtual {p3, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 770
    .local v2, "dstFragmentId":I
    sget-object v7, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 771
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getFragmentId()I

    move-result v7

    invoke-virtual {p0, v7, v2, v4, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->switchFragments(IILjava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 775
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v2    # "dstFragmentId":I
    .end local v3    # "operationType":I
    .end local v4    # "path":Ljava/lang/String;
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduCopyMoveSourceFolderPath:Ljava/lang/String;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v7, :cond_0

    .line 777
    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduCopyMoveSourceFolderPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_6

    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goBack()Z

    move-result v7

    if-eqz v7, :cond_b

    .line 779
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduCopyMoveSourceFolderPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 780
    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goBack()Z

    move-result v7

    if-nez v7, :cond_6

    .line 784
    :cond_7
    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduCopyMoveSourceFolderPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 786
    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_8

    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 788
    :cond_8
    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduRoot:Ljava/lang/String;

    iput-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduPathIndicator:Ljava/lang/String;

    .line 790
    :cond_9
    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduPathIndicator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 791
    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduPathIndicator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentDirectory(Ljava/lang/String;)V

    .line 792
    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_a

    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v7, :cond_a

    .line 793
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->RemoveAllPaddingThumbnailMessage()V

    .line 795
    :cond_a
    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v1

    .line 796
    .restart local v1    # "c":Landroid/database/Cursor;
    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v7, :cond_c

    .line 797
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 806
    .end local v1    # "c":Landroid/database/Cursor;
    :cond_b
    :goto_1
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduCopyMoveSourceFolderPath:Ljava/lang/String;

    goto/16 :goto_0

    .line 799
    .restart local v1    # "c":Landroid/database/Cursor;
    :cond_c
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 803
    .end local v1    # "c":Landroid/database/Cursor;
    :cond_d
    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goToRoot(Ljava/lang/Boolean;)Z

    goto :goto_1

    .line 692
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_4
        0x4 -> :sswitch_2
        0x5 -> :sswitch_0
        0x6 -> :sswitch_1
        0x12 -> :sswitch_3
    .end sparse-switch
.end method

.method public onBackPressed()Z
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, -0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 274
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getSelectModeFrom()I

    move-result v5

    if-eq v5, v9, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getSelectModeFrom()I

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->isSelectMode()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 277
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->finishSelectMode()V

    .line 399
    :goto_0
    return v3

    .line 281
    :cond_1
    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mIsHomeBtnSelected:Z

    if-nez v5, :cond_2

    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mIsSelector:Z

    if-nez v5, :cond_12

    .line 282
    :cond_2
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mIsSelector:Z

    .line 283
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v5, :cond_f

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    if-eqz v5, :cond_f

    .line 284
    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mIsHomeBtnSelected:Z

    if-nez v5, :cond_3

    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mIsShortcutFolder:Z

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mShortcutRootFolder:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduRootReplace:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mShortcutRootFolder:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    :cond_3
    iget v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mSearchFromIndex:I

    const/4 v6, 0x6

    if-ne v5, v6, :cond_9

    .line 289
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getSelectModeFrom()I

    move-result v5

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-nez v5, :cond_6

    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getSelectModeFrom()I

    move-result v5

    if-ne v5, v8, :cond_8

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v5, :cond_8

    .line 292
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 294
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 295
    .local v1, "b":Landroid/os/Bundle;
    const-string v4, "shortcut_working_index_intent_key"

    iget v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mShortcutIndex:I

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 296
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v4, v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView(Landroid/os/Bundle;)V

    goto :goto_0

    .line 300
    .end local v1    # "b":Landroid/os/Bundle;
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    goto :goto_0

    .line 304
    :cond_8
    const-string v3, "BaiduFragment"

    const-string v5, " failed to go up"

    invoke-static {v4, v3, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 305
    goto :goto_0

    .line 308
    :cond_9
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goUp()Z

    move-result v5

    if-eqz v5, :cond_f

    .line 311
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->isSelectMode()Z

    move-result v4

    if-eqz v4, :cond_a

    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v4, :cond_a

    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v4

    if-lez v4, :cond_a

    .line 313
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 317
    :cond_a
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/view/PathIndicator;->goUp()V

    .line 319
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 322
    :cond_b
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduRoot:Ljava/lang/String;

    iput-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduPathIndicator:Ljava/lang/String;

    .line 323
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduPathIndicator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 324
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduPathIndicator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentDirectory(Ljava/lang/String;)V

    .line 332
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_c

    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v4, :cond_c

    .line 334
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->RemoveAllPaddingThumbnailMessage()V

    .line 337
    :cond_c
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v2

    .line 338
    .local v2, "c":Landroid/database/Cursor;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v4, :cond_e

    .line 340
    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->updateAdapter(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 327
    .end local v2    # "c":Landroid/database/Cursor;
    :cond_d
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduRoot:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduPathIndicator:Ljava/lang/String;

    .line 328
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduPathIndicator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 329
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduPathIndicator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentDirectory(Ljava/lang/String;)V

    goto :goto_1

    .line 344
    .restart local v2    # "c":Landroid/database/Cursor;
    :cond_e
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 349
    .end local v2    # "c":Landroid/database/Cursor;
    :cond_f
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mIsHomeBtnSelected:Z

    .line 385
    :cond_10
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getSelectModeFrom()I

    move-result v5

    if-eq v5, v9, :cond_11

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getSelectModeFrom()I

    move-result v5

    if-eq v5, v8, :cond_11

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getSelectModeFrom()I

    move-result v5

    if-nez v5, :cond_19

    :cond_11
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v5, :cond_19

    .line 388
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    goto/16 :goto_0

    .line 350
    :cond_12
    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mIsSelector:Z

    if-eqz v5, :cond_10

    .line 351
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mIsSelector:Z

    .line 352
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "ISSELECTOR"

    iget-boolean v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mIsSelector:Z

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 353
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goBack()Z

    move-result v4

    if-eqz v4, :cond_17

    .line 355
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_13

    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 357
    :cond_13
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduRoot:Ljava/lang/String;

    iput-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduPathIndicator:Ljava/lang/String;

    .line 359
    :cond_14
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduPathIndicator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 360
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduPathIndicator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentDirectory(Ljava/lang/String;)V

    .line 361
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_15

    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v4, :cond_15

    .line 362
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->RemoveAllPaddingThumbnailMessage()V

    .line 364
    :cond_15
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v2

    .line 365
    .restart local v2    # "c":Landroid/database/Cursor;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v4, :cond_16

    .line 366
    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->updateAdapter(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 368
    :cond_16
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 372
    .end local v2    # "c":Landroid/database/Cursor;
    :cond_17
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 373
    .local v0, "argument":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "RETURNPATH"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_18

    .line 374
    const-string v4, "FOLDERPATH"

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduRoot:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    :goto_2
    const-string v4, "RETURNPATH"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->clearHistoricalPaths()V

    .line 380
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mCallback:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$onCopyMoveSelectedListener;

    const-string v5, "FOLDERPATH"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v0, v5}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$onCopyMoveSelectedListener;->onCopyMoveSelected(Landroid/os/Bundle;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 376
    :cond_18
    const-string v4, "FOLDERPATH"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "RETURNPATH"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 391
    .end local v0    # "argument":Landroid/os/Bundle;
    :cond_19
    const-string v3, "BaiduFragment"

    const-string v5, " failed to go up"

    invoke-static {v4, v3, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 393
    goto/16 :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1429
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1430
    iget v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mRunFrom:I

    const/16 v1, 0x15

    if-ne v0, v1, :cond_0

    .line 1431
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->selectmodeActionbar()V

    .line 1433
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 107
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreate(Landroid/os/Bundle;)V

    .line 109
    const/16 v2, 0x1f

    iput v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mCategoryType:I

    .line 111
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 114
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .line 117
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v2, p0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setSupportAsync(Lcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    .line 119
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goToRoot(Ljava/lang/Boolean;)Z

    .line 121
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mSelectedItemChangeListener:Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    if-eqz v2, :cond_1

    .line 123
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mSelectedItemChangeListener:Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->setSelectedItemChangeListener(Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;)V

    .line 128
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 130
    .local v0, "argument":Landroid/os/Bundle;
    const/4 v1, 0x0

    .line 132
    .local v1, "startFolder":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 134
    const-string v2, "shortcut_working_index_intent_key"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mShortcutIndex:I

    .line 136
    const-string v2, "run_from"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const/16 v3, 0x15

    if-ne v2, v3, :cond_2

    .line 138
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mFromSelector:Z

    .line 140
    const-string v2, "FILE_OPERATION"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    .line 142
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    const-string v3, "audio/*;video/*"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->addExceptFilterMimeType(Ljava/lang/String;)V

    .line 144
    const-string v2, "CONTENT_TYPE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    .line 146
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v2, v5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->setEnableFiltering(Z)V

    .line 148
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v2, v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setAllAttachMode(Z)V

    .line 154
    :cond_2
    const-string v2, "search_option_type"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mSearchFromIndex:I

    .line 156
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "FOLDERPATH"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 157
    if-eqz v1, :cond_3

    .line 159
    const-string v2, "/Baidu"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 160
    const-string v1, ""

    .line 165
    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 167
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    .line 169
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduRoot:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduPathIndicator:Ljava/lang/String;

    .line 174
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->context:Landroid/content/Context;

    .line 175
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->context:Landroid/content/Context;

    if-eqz v2, :cond_4

    .line 176
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    .line 177
    :cond_4
    return-void

    .line 162
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduRootReplace:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method protected onCreateDetailFragmentListener()Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 1417
    new-instance v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDetailFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDetailFragment;-><init>()V

    return-object v0
.end method

.method protected onCreateFileOperationListener()Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;
    .locals 1

    .prologue
    .line 1321
    new-instance v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$5;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)V

    return-object v0
.end method

.method protected onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 982
    new-instance v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)V

    return-object v0
.end method

.method protected onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1

    .prologue
    .line 1235
    new-instance v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$4;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)V

    return-object v0
.end method

.method protected onCreateMultipleDetailFragmentListener()Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 1423
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/MultipleDropboxDetailFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/MultipleDropboxDetailFragment;-><init>()V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 183
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 185
    .local v0, "view":Landroid/view/View;
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mFromSelector:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getSelectionType()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 186
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mCreateFolderContainer:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 189
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setOnPathChangeListener(Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;)V

    .line 192
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduPathIndicator:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 194
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduRoot:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduPathIndicator:Ljava/lang/String;

    .line 201
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduPathIndicator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 202
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduPathIndicator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentDirectory(Ljava/lang/String;)V

    .line 204
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    if-eqz v1, :cond_2

    .line 205
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v1, p0}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setOnDropListener(Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;)V

    .line 208
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 210
    return-object v0

    .line 198
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduRoot:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduPathIndicator:Ljava/lang/String;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 404
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onDestroy()V

    .line 407
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v0, :cond_0

    .line 409
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->closeCursor()V

    .line 412
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 9
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v8, 0x4

    const/4 v7, -0x1

    const/4 v5, 0x1

    .line 563
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    .line 683
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v5

    :cond_0
    :goto_1
    return v5

    .line 567
    :sswitch_0
    new-instance v1, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    invoke-direct {v1}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;-><init>()V

    .line 569
    .local v1, "createFolder":Landroid/app/DialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 571
    .local v0, "argument":Landroid/os/Bundle;
    const-string v6, "title"

    const v7, 0x7f0b0019

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 573
    const-string v6, "kind_of_operation"

    invoke-virtual {v0, v6, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 576
    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v2

    .line 578
    .local v2, "currentfolder":Ljava/lang/String;
    if-nez v2, :cond_1

    .line 579
    const-string v2, "/"

    .line 581
    :cond_1
    const-string v6, "current_folder"

    invoke-virtual {v0, v6, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    const-string v6, "src_fragment_id"

    const/16 v7, 0x1f

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 585
    invoke-virtual {v1, v0}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 587
    invoke-virtual {v1, p0, v8}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 589
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "createFolder"

    invoke-virtual {v1, v6, v7}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1

    .line 596
    .end local v0    # "argument":Landroid/os/Bundle;
    .end local v1    # "createFolder":Landroid/app/DialogFragment;
    .end local v2    # "currentfolder":Ljava/lang/String;
    :sswitch_1
    const/4 v6, 0x0

    invoke-virtual {p0, v5, v7, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->startSelectMode(III)V

    goto :goto_1

    .line 602
    :sswitch_2
    const/4 v6, 0x5

    invoke-virtual {p0, v5, v7, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->startSelectMode(III)V

    goto :goto_1

    .line 607
    :sswitch_3
    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-le v6, v5, :cond_2

    .line 608
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->addCurrentFolderToShortcut()V

    goto :goto_1

    .line 610
    :cond_2
    const/16 v6, 0x8

    invoke-virtual {p0, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->showDialog(I)V

    goto :goto_1

    .line 615
    :sswitch_4
    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v6}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_3

    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v6}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 616
    new-instance v4, Lcom/sec/android/app/myfiles/fragment/NoNetworkFragment;

    invoke-direct {v4}, Lcom/sec/android/app/myfiles/fragment/NoNetworkFragment;-><init>()V

    .line 617
    .local v4, "noNetwork":Landroid/app/DialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "no_network"

    invoke-virtual {v4, v6, v7}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1

    .line 621
    .end local v4    # "noNetwork":Landroid/app/DialogFragment;
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    if-eqz v6, :cond_0

    .line 622
    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->context:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->requestSignIn(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 627
    :sswitch_5
    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v6}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretMode(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 629
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->moveBaiduToSecretBox()V

    goto/16 :goto_1

    .line 633
    :cond_4
    :try_start_0
    new-instance v6, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$2;

    invoke-direct {v6, p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$2;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)V

    iput-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPrivateModeClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

    .line 671
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPrivateModeClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

    invoke-static {v6, v7}, Lcom/samsung/android/privatemode/PrivateModeManager;->getInstance(Landroid/content/Context;Lcom/samsung/android/privatemode/IPrivateModeClient;)Lcom/samsung/android/privatemode/PrivateModeManager;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 673
    :catch_0
    move-exception v3

    .line 674
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 681
    .end local v3    # "e":Ljava/lang/Exception;
    :sswitch_6
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduCopyMoveSourceFolderPath:Ljava/lang/String;

    goto/16 :goto_0

    .line 563
    :sswitch_data_0
    .sparse-switch
        0x7f0f0122 -> :sswitch_6
        0x7f0f0123 -> :sswitch_6
        0x7f0f0124 -> :sswitch_5
        0x7f0f0129 -> :sswitch_3
        0x7f0f0137 -> :sswitch_1
        0x7f0f0138 -> :sswitch_2
        0x7f0f013a -> :sswitch_0
        0x7f0f013e -> :sswitch_4
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 16
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 418
    const/4 v4, 0x0

    .line 420
    .local v4, "bResult":Z
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    if-eqz v14, :cond_0

    .line 421
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->isBaiduCloudAccountIsSignin()Z

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    invoke-virtual {v15}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->isAuthKeySaved()Z

    move-result v15

    and-int v4, v14, v15

    .line 424
    :cond_0
    invoke-super/range {p0 .. p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 426
    if-eqz v4, :cond_9

    .line 428
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCount()I

    move-result v14

    if-eqz v14, :cond_2

    .line 430
    const v14, 0x7f0f0137

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    .line 432
    .local v8, "selectFileMenu":Landroid/view/MenuItem;
    if-eqz v8, :cond_1

    .line 434
    const/4 v14, 0x1

    invoke-interface {v8, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 437
    :cond_1
    const v14, 0x7f0f0138

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    .line 439
    .local v7, "normalDeleteMenu":Landroid/view/MenuItem;
    if-eqz v7, :cond_2

    .line 441
    const/4 v14, 0x1

    invoke-interface {v7, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 445
    .end local v7    # "normalDeleteMenu":Landroid/view/MenuItem;
    .end local v8    # "selectFileMenu":Landroid/view/MenuItem;
    :cond_2
    const v14, 0x7f0f013d

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v9

    .line 447
    .local v9, "settingMenu":Landroid/view/MenuItem;
    if-eqz v9, :cond_3

    .line 449
    const/4 v14, 0x0

    invoke-interface {v9, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 453
    :cond_3
    const v14, 0x7f0f012b

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v13

    .line 455
    .local v13, "zipMenu":Landroid/view/MenuItem;
    if-eqz v13, :cond_4

    .line 457
    const/4 v14, 0x0

    invoke-interface {v13, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 460
    :cond_4
    const v14, 0x7f0f012c

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 462
    .local v1, "UnzipMenu":Landroid/view/MenuItem;
    if-eqz v1, :cond_5

    .line 464
    const/4 v14, 0x0

    invoke-interface {v1, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 468
    :cond_5
    const v14, 0x7f0f012d

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 470
    .local v2, "UnzipMenuHere":Landroid/view/MenuItem;
    if-eqz v2, :cond_6

    .line 472
    const/4 v14, 0x0

    invoke-interface {v2, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 475
    :cond_6
    const v14, 0x7f0f013a

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 477
    .local v5, "createFolderMenu":Landroid/view/MenuItem;
    if-eqz v5, :cond_7

    .line 479
    const/4 v14, 0x1

    invoke-interface {v5, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 552
    :cond_7
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mFromSelector:Z

    if-eqz v14, :cond_8

    const v14, 0x7f0f013a

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v14

    if-eqz v14, :cond_8

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v14}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v14

    if-nez v14, :cond_8

    .line 553
    const v14, 0x7f0f013a

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    .line 554
    .local v6, "item":Landroid/view/MenuItem;
    const/4 v14, 0x0

    invoke-interface {v6, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 557
    .end local v6    # "item":Landroid/view/MenuItem;
    :cond_8
    return-void

    .line 484
    .end local v1    # "UnzipMenu":Landroid/view/MenuItem;
    .end local v2    # "UnzipMenuHere":Landroid/view/MenuItem;
    .end local v5    # "createFolderMenu":Landroid/view/MenuItem;
    .end local v9    # "settingMenu":Landroid/view/MenuItem;
    .end local v13    # "zipMenu":Landroid/view/MenuItem;
    :cond_9
    const v14, 0x7f0f013d

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v9

    .line 486
    .restart local v9    # "settingMenu":Landroid/view/MenuItem;
    if-eqz v9, :cond_a

    .line 488
    const/4 v14, 0x0

    invoke-interface {v9, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 492
    :cond_a
    const v14, 0x7f0f012b

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v13

    .line 494
    .restart local v13    # "zipMenu":Landroid/view/MenuItem;
    if-eqz v13, :cond_b

    .line 496
    const/4 v14, 0x0

    invoke-interface {v13, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 499
    :cond_b
    const v14, 0x7f0f012c

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 501
    .restart local v1    # "UnzipMenu":Landroid/view/MenuItem;
    if-eqz v1, :cond_c

    .line 503
    const/4 v14, 0x0

    invoke-interface {v1, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 507
    :cond_c
    const v14, 0x7f0f012d

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 509
    .restart local v2    # "UnzipMenuHere":Landroid/view/MenuItem;
    if-eqz v2, :cond_d

    .line 511
    const/4 v14, 0x0

    invoke-interface {v2, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 514
    :cond_d
    const v14, 0x7f0f013a

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 516
    .restart local v5    # "createFolderMenu":Landroid/view/MenuItem;
    if-eqz v5, :cond_e

    .line 518
    const/4 v14, 0x0

    invoke-interface {v5, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 522
    :cond_e
    const v14, 0x7f0f013b

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v12

    .line 524
    .local v12, "viewAsMenu":Landroid/view/MenuItem;
    if-eqz v12, :cond_f

    .line 526
    const/4 v14, 0x0

    invoke-interface {v12, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 529
    :cond_f
    const v14, 0x7f0f013c

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v11

    .line 531
    .local v11, "sortByMenu":Landroid/view/MenuItem;
    if-eqz v11, :cond_10

    .line 533
    const/4 v14, 0x0

    invoke-interface {v11, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 536
    :cond_10
    const v14, 0x7f0f0129

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 538
    .local v3, "addShortcutMenu":Landroid/view/MenuItem;
    if-eqz v3, :cond_11

    .line 540
    const/4 v14, 0x0

    invoke-interface {v3, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 543
    :cond_11
    const v14, 0x7f0f013e

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    .line 545
    .local v10, "signInMenu":Landroid/view/MenuItem;
    if-eqz v10, :cond_7

    .line 547
    const/4 v14, 0x1

    invoke-interface {v10, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_0
.end method

.method public onRefresh()V
    .locals 3

    .prologue
    .line 263
    const/4 v0, 0x0

    const-string v1, "BaiduFragment"

    const-string v2, "Baidu-refresh"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->refreshNavigation()V

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 268
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 252
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onResume()V

    .line 254
    const/4 v0, 0x0

    const-string v1, "BaiduFragment"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 256
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->showPathIndicator(Z)V

    .line 257
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 215
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 217
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 219
    .local v0, "argument":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    const-string v1, "run_from"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/16 v2, 0x15

    if-ne v1, v2, :cond_0

    .line 221
    const-string v1, "selection_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->startSelectMode(III)V

    .line 224
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    if-eqz v1, :cond_1

    .line 226
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    const-string v2, "31"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setCategoryItemListSelection(Ljava/lang/String;)I

    .line 228
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    new-instance v2, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$1;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->post(Ljava/lang/Runnable;)Z

    .line 248
    :cond_1
    return-void
.end method

.method protected selectAllItem()V
    .locals 2

    .prologue
    .line 822
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->selectAllItem()V

    .line 823
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 824
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getSelectModeFrom()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 825
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->selectAllFileItem()V

    .line 830
    :cond_0
    :goto_0
    return-void

    .line 827
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->selectAllItem()V

    goto :goto_0
.end method

.method public selectmodeActionbar()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1437
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 1438
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 1439
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 1440
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 1442
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getSelectionType()I

    move-result v1

    if-eq v1, v3, :cond_3

    .line 1443
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040008

    invoke-virtual {v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1444
    .local v0, "customview":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 1445
    const v1, 0x7f0f0008

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mActionbar_cancel:Landroid/widget/TextView;

    .line 1446
    const v1, 0x7f0f0009

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mActionbar_ok:Landroid/widget/TextView;

    .line 1448
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    if-eqz v1, :cond_0

    .line 1449
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->getmFileOperation()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 1450
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->getmFileOperation()I

    move-result v1

    if-nez v1, :cond_2

    .line 1451
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mActionbar_ok:Landroid/widget/TextView;

    const v2, 0x7f0b0013

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1457
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mActionbar_cancel:Landroid/widget/TextView;

    new-instance v2, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$7;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$7;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1478
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mActionbar_ok:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 1479
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v2, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$8;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$8;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1491
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v1, :cond_1

    .line 1492
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mActionbar_ok:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1495
    :cond_1
    return-void

    .line 1453
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mActionbar_ok:Landroid/widget/TextView;

    const v2, 0x7f0b0014

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 1473
    .end local v0    # "customview":Landroid/view/View;
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/high16 v2, 0x7f040000

    invoke-virtual {v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1475
    .restart local v0    # "customview":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 1476
    const v1, 0x7f0f0003

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mSelectAllButton:Landroid/widget/TextView;

    goto :goto_1
.end method

.method public setPathIndicator(Ljava/lang/String;)V
    .locals 2
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 1533
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduPathIndicator:Ljava/lang/String;

    .line 1534
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    if-eqz v0, :cond_0

    .line 1535
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduPathIndicator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 1536
    :cond_0
    return-void
.end method

.method protected unselectAllItem()V
    .locals 1

    .prologue
    .line 835
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->unselectAllItem()V

    .line 837
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 839
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 841
    :cond_0
    return-void
.end method
