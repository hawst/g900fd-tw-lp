.class Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$3;
.super Ljava/lang/Object;
.source "SearchsSetFileTypeFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->setupDialog(Landroid/app/AlertDialog$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 208
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    iget-boolean v1, v1, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->isfromAdvance:Z

    if-eqz v1, :cond_0

    .line 209
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getTargetRequestCode()I

    move-result v2

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 215
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 221
    :goto_1
    return-void

    .line 211
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getTargetRequestCode()I

    move-result v2

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->onClickSearchCallback(IILandroid/content/Intent;)V

    goto :goto_0

    .line 217
    :catch_0
    move-exception v0

    .line 219
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
