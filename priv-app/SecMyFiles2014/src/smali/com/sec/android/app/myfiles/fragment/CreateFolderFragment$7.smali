.class Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$7;
.super Ljava/lang/Object;
.source "CreateFolderFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->showToast(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

.field final synthetic val$resId:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;I)V
    .locals 0

    .prologue
    .line 513
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    iput p2, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$7;->val$resId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 518
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;)Landroid/widget/Toast;

    move-result-object v0

    if-nez v0, :cond_2

    .line 520
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 522
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mToast:Landroid/widget/Toast;
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->access$202(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 534
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;)Landroid/widget/Toast;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 536
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 538
    :cond_0
    return-void

    .line 526
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$7;->val$resId:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    # setter for: Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mToast:Landroid/widget/Toast;
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->access$202(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;Landroid/widget/Toast;)Landroid/widget/Toast;

    goto :goto_0

    .line 531
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;)Landroid/widget/Toast;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$7;->val$resId:I

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    goto :goto_0
.end method
