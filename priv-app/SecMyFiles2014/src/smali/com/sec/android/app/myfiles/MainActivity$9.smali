.class Lcom/sec/android/app/myfiles/MainActivity$9;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/MainActivity;->updateEmptyView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/MainActivity;)V
    .locals 0

    .prologue
    .line 2910
    iput-object p1, p0, Lcom/sec/android/app/myfiles/MainActivity$9;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 2915
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$9;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mEmptyTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/MainActivity;->access$1400(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2916
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$9;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mEmptyTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/MainActivity;->access$1400(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    .line 2917
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$9;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mEmptyTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/MainActivity;->access$1400(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x40

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    .line 2918
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$9;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mEmptyTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/MainActivity;->access$1400(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$9;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mEmptyTextView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/MainActivity;->access$1400(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 2920
    :cond_0
    return-void
.end method
