.class Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$ConcreteProgressListener;
.super Ljava/lang/Object;
.source "DropboxAdapter.java"

# interfaces
.implements Lcom/samsung/scloud/response/ProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ConcreteProgressListener"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 716
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onException(Lcom/samsung/scloud/exception/SCloudException;)V
    .locals 0
    .param p1, "e"    # Lcom/samsung/scloud/exception/SCloudException;

    .prologue
    .line 719
    return-void
.end method

.method public onUpdate(Ljava/lang/String;IJ)V
    .locals 0
    .param p1, "file"    # Ljava/lang/String;
    .param p2, "statusTransferOngoing"    # I
    .param p3, "progress"    # J

    .prologue
    .line 723
    return-void
.end method
