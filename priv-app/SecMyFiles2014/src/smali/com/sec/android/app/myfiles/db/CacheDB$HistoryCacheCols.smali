.class public Lcom/sec/android/app/myfiles/db/CacheDB$HistoryCacheCols;
.super Ljava/lang/Object;
.source "CacheDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/db/CacheDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HistoryCacheCols"
.end annotation


# static fields
.field public static final CATEGORY:Ljava/lang/String; = "_category"

.field public static final DATA:Ljava/lang/String; = "_data"

.field public static final DATE:Ljava/lang/String; = "_date"

.field public static final DESCRIPTION:Ljava/lang/String; = "_description"

.field public static final EXTERNAL_ID:Ljava/lang/String; = "_extern_id"

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final MIME_TYPE:Ljava/lang/String; = "_mime_type"

.field public static final NAME:Ljava/lang/String; = "_name"

.field public static final NAME_FOR_PINYIN_SORT:Ljava/lang/String; = "_name_for_pinyin_sort"

.field public static final SIZE:Ljava/lang/String; = "_size"

.field public static final SOURCE:Ljava/lang/String; = "_source"

.field public static final STATUS:Ljava/lang/String; = "_status"

.field public static final TYPE:Ljava/lang/String; = "_type"

.field public static final VENDOR:Ljava/lang/String; = "_vendor"

.field public static final VERSION:Ljava/lang/String; = "_version"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
