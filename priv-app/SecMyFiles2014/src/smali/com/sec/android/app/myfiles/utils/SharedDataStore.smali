.class public Lcom/sec/android/app/myfiles/utils/SharedDataStore;
.super Ljava/lang/Object;
.source "SharedDataStore.java"


# static fields
.field private static bMangaedNormalState:Z

.field private static isBaidu:Z

.field private static isDevice:Z

.field private static isDropbox:Z

.field private static isKNOXInstalled:Z

.field private static isNearby:Z

.field private static isPrivate:Z

.field private static isSDcard:Z

.field private static isSearchFileTypeAll:Z

.field private static isSearchFileTypeDocument:Z

.field private static isSearchFileTypeDownloadedApps:Z

.field private static isSearchFileTypeImages:Z

.field private static isSearchFileTypeMusic:Z

.field private static isSearchFileTypeVideos:Z

.field private static isUSBa:Z

.field private static isUSBb:Z

.field private static isUSBc:Z

.field private static isUSBd:Z

.field private static isUSBe:Z

.field private static isUSBf:Z

.field private static mBackFragmentID:I

.field private static mChangedItemsForAdvanceSearch:[Z

.field private static mCheckedNearby:Ljava/lang/String;

.field private static mChooserFromConvert:Z

.field private static mCurrentFtpShortcutLabel:Ljava/lang/String;

.field private static mCurrentInOrder:I

.field private static mCurrentListBy:I

.field private static mCurrentView:I

.field private static mDocumentDirectory:Ljava/lang/String;

.field private static mDropboxKey:Ljava/lang/String;

.field private static mDropboxSecret:Ljava/lang/String;

.field private static mExpandedViewIdInSplitView:I

.field private static mFtpShortcutMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mHomeDirectory:Ljava/lang/String;

.field private static mImageDirectory:Ljava/lang/String;

.field private static mIsCheckedShowDialogDropBox:Z

.field private static mIsCheckedShowDialogFTP:Z

.field private static mIsCheckedShowDialogWLANOn:Z

.field private static mLastDirectory:Ljava/lang/String;

.field private static mLockFilesDialog:Z

.field private static mMusicDirectory:Ljava/lang/String;

.field private static mPref:Landroid/content/SharedPreferences;

.field private static mSavedDocumentsTotalSize:J

.field private static mSavedDownloadedAppTotalSize:J

.field private static mSavedSetHomeShorcutPaths:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mSearchFileLocation:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mSearchFileTypeIndex:I

.field private static mSearchFromDate:J

.field private static mSearchSelectedDeviceIDMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mSearchToDate:J

.field private static mSettingsPref:Landroid/content/SharedPreferences;

.field private static mShowCategory:Z

.field private static mShowFileExtention:Z

.field private static mShowHiddenFiles:Z

.field private static mSplitLocation:I

.field private static mStartDirectory:Ljava/lang/String;

.field private static mVideoDirectory:Ljava/lang/String;

.field private static mViewType:I

.field private static sInstance:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

.field private static selectedPaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAdapter:Landroid/widget/BaseAdapter;

.field private mDownloadedAppTotalSize:J

.field private mDragSourceFragmentId:I

.field private mDragSourcePath:Ljava/lang/String;

.field private mExtensionForCategory:Ljava/lang/String;

.field private mFtpParamsString:Ljava/lang/String;

.field private mLastModifiedTime:J

.field private mMimeTypeForCategory:Ljava/lang/String;

.field private mPath:Ljava/lang/String;

.field private mPathForDropboxMultiDrag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 38
    sput-object v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->sInstance:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    .line 40
    sput-object v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    .line 42
    sput-object v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSettingsPref:Landroid/content/SharedPreferences;

    .line 81
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mViewType:I

    .line 111
    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isDevice:Z

    .line 113
    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSDcard:Z

    .line 115
    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBa:Z

    .line 117
    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBb:Z

    .line 119
    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBc:Z

    .line 121
    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBd:Z

    .line 123
    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBe:Z

    .line 125
    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBf:Z

    .line 127
    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isDropbox:Z

    .line 129
    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isBaidu:Z

    .line 131
    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isNearby:Z

    .line 133
    sput-object v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mCheckedNearby:Ljava/lang/String;

    .line 135
    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isPrivate:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object v2, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPath:Ljava/lang/String;

    .line 48
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mLastModifiedTime:J

    .line 164
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mMimeTypeForCategory:Ljava/lang/String;

    .line 166
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mExtensionForCategory:Ljava/lang/String;

    .line 173
    iput-object v2, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPathForDropboxMultiDrag:Ljava/lang/String;

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 185
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->sInstance:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    if-nez v0, :cond_1

    .line 187
    new-instance v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->sInstance:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    .line 189
    if-eqz p0, :cond_0

    .line 191
    const-string v0, "MyFiles"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    .line 194
    :cond_0
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->initValue()V

    .line 197
    :cond_1
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->sInstance:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    return-object v0
.end method

.method public static getmBackFragmentID()I
    .locals 1

    .prologue
    .line 1414
    sget v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mBackFragmentID:I

    return v0
.end method

.method public static getmChangedItemsForAdvanceSearch()[Z
    .locals 1

    .prologue
    .line 1432
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mChangedItemsForAdvanceSearch:[Z

    return-object v0
.end method

.method public static getmCheckedNearby()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1264
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mCheckedNearby:Ljava/lang/String;

    return-object v0
.end method

.method private static initValue()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 204
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mFtpShortcutMap:Ljava/util/Map;

    .line 205
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->selectedPaths:Ljava/util/ArrayList;

    .line 206
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchSelectedDeviceIDMap:Ljava/util/Map;

    .line 207
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchFileLocation:Ljava/util/ArrayList;

    .line 210
    const/16 v0, 0x201

    sput v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mViewType:I

    .line 212
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->sInstance:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    if-eqz v0, :cond_0

    .line 214
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "show_hidden_files"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mShowHiddenFiles:Z

    .line 215
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "show_file_extension"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mShowFileExtention:Z

    .line 216
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "lock_files_dialog"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mLockFilesDialog:Z

    .line 218
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "home_folder"

    sget-object v2, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mHomeDirectory:Ljava/lang/String;

    .line 219
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "current_view"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mCurrentView:I

    .line 220
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "current_sort_by"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mCurrentListBy:I

    .line 221
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "current_order_by"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mCurrentInOrder:I

    .line 222
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "show_category"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mShowCategory:Z

    .line 223
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "managed_browser_normal_state"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->bMangaedNormalState:Z

    .line 224
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "splitview_position"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSplitLocation:I

    .line 227
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "search_file_type_index"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchFileTypeIndex:I

    .line 228
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "search_from_date"

    invoke-interface {v0, v1, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchFromDate:J

    .line 229
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "search_to_date"

    invoke-interface {v0, v1, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchToDate:J

    .line 230
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "search_file_type_all"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeAll:Z

    .line 231
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "search_file_type_images"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeImages:Z

    .line 232
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "search_file_type_videos"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeVideos:Z

    .line 233
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "search_file_type_music"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeMusic:Z

    .line 234
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "search_file_type_doc"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeDocument:Z

    .line 235
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "search_file_type_down"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeDownloadedApps:Z

    .line 236
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "search_location_device"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isDevice:Z

    .line 237
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "search_location_sdcard"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSDcard:Z

    .line 238
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "search_location_usb_a"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBa:Z

    .line 239
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "search_location_usb_b"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBb:Z

    .line 240
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "search_location_usb_c"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBc:Z

    .line 241
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "search_location_usb_d"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBd:Z

    .line 242
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "search_location_usb_e"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBe:Z

    .line 243
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "search_location_usb_f"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBf:Z

    .line 244
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "search_location_dropbox"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isDropbox:Z

    .line 245
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "search_location_baidu"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isBaidu:Z

    .line 246
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "search_location_nearby"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isNearby:Z

    .line 247
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "search_location_private"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isPrivate:Z

    .line 250
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "dropbox_auth_key"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mDropboxKey:Ljava/lang/String;

    .line 251
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "dropbox_auth_secret"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mDropboxSecret:Ljava/lang/String;

    .line 258
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "expanded_view"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mExpandedViewIdInSplitView:I

    .line 262
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "dropbox_warning"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mIsCheckedShowDialogDropBox:Z

    .line 263
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "ftp_warning"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mIsCheckedShowDialogFTP:Z

    .line 266
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "wlanon_warning"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mIsCheckedShowDialogWLANOn:Z

    .line 268
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "downloadedappsize"

    invoke-interface {v0, v1, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSavedDownloadedAppTotalSize:J

    .line 270
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "documentssize"

    invoke-interface {v0, v1, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSavedDocumentsTotalSize:J

    .line 275
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "homescreen_shortcut_path"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSavedSetHomeShorcutPaths:Ljava/util/Set;

    .line 292
    :cond_0
    return-void
.end method

.method public static ismChooserFromConvert()Z
    .locals 1

    .prologue
    .line 1422
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mChooserFromConvert:Z

    return v0
.end method

.method private declared-synchronized loadListFromPreference(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 974
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-eqz v5, :cond_1

    .line 1002
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v2

    .line 979
    :cond_1
    const/4 v2, 0x0

    .line 981
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_1
    sget-object v5, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const/4 v6, 0x0

    invoke-interface {v5, p1, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 983
    .local v3, "savedListString":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 985
    new-instance v2, Ljava/util/ArrayList;

    .end local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 989
    .restart local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_2
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4, v3}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 991
    .local v4, "shortcutPathArray":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 993
    invoke-virtual {v4, v1}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 991
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 996
    .end local v1    # "i":I
    .end local v4    # "shortcutPathArray":Lorg/json/JSONArray;
    :catch_0
    move-exception v0

    .line 998
    .local v0, "e":Lorg/json/JSONException;
    :try_start_3
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 974
    .end local v0    # "e":Lorg/json/JSONException;
    .end local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3    # "savedListString":Ljava/lang/String;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method

.method private declared-synchronized saveListToPreference(Ljava/lang/String;Ljava/util/ArrayList;)Z
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 945
    .local p2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    monitor-enter p0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 947
    :cond_0
    const/4 v3, 0x0

    .line 968
    :goto_0
    monitor-exit p0

    return v3

    .line 950
    :cond_1
    :try_start_0
    sget-object v3, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 952
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 954
    .local v0, "array":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 956
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 954
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 959
    :cond_2
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 961
    const/4 v3, 0x0

    invoke-interface {v1, p1, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 968
    :goto_2
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v3

    goto :goto_0

    .line 965
    :cond_3
    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, p1, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 945
    .end local v0    # "array":Lorg/json/JSONArray;
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v2    # "i":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public static setmBackFragment(I)V
    .locals 0
    .param p0, "mBackFragmentID"    # I

    .prologue
    .line 1419
    sput p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mBackFragmentID:I

    .line 1420
    return-void
.end method

.method public static setmChangedItemsForAdvanceSearch([Z)V
    .locals 0
    .param p0, "mChangedItemsForAdvanceSearch"    # [Z

    .prologue
    .line 1437
    sput-object p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mChangedItemsForAdvanceSearch:[Z

    .line 1438
    return-void
.end method

.method public static setmCheckedNearby(Ljava/lang/String;)V
    .locals 0
    .param p0, "mCheckedNearby"    # Ljava/lang/String;

    .prologue
    .line 1269
    sput-object p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mCheckedNearby:Ljava/lang/String;

    .line 1270
    return-void
.end method

.method public static setmChooserFromConvert(Z)V
    .locals 0
    .param p0, "mChooserFromConvert"    # Z

    .prologue
    .line 1427
    sput-boolean p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mChooserFromConvert:Z

    .line 1428
    return-void
.end method


# virtual methods
.method public declared-synchronized GetSelectedFilesPath()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1016
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->selectedPaths:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized SavedSelectedFilesPath(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1010
    .local p1, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    monitor-enter p0

    :try_start_0
    sput-object p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->selectedPaths:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1011
    monitor-exit p0

    return-void

    .line 1010
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized clearSelectedFilesPath()V
    .locals 1

    .prologue
    .line 1022
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->selectedPaths:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1023
    monitor-exit p0

    return-void

    .line 1022
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public commit()Z
    .locals 4

    .prologue
    .line 311
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    .line 312
    const/4 v1, 0x0

    .line 360
    :goto_0
    return v1

    .line 315
    :cond_0
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 318
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "home_folder"

    sget-object v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mHomeDirectory:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 322
    const-string v1, "show_category"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mShowCategory:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 323
    const-string v1, "managed_browser_normal_state"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->bMangaedNormalState:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 326
    const-string v1, "search_file_type_index"

    sget v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchFileTypeIndex:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 327
    const-string v1, "search_from_date"

    sget-wide v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchFromDate:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 328
    const-string v1, "search_to_date"

    sget-wide v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchToDate:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 329
    const-string v1, "search_file_type_all"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeAll:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 330
    const-string v1, "search_file_type_images"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeImages:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 331
    const-string v1, "search_file_type_videos"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeVideos:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 332
    const-string v1, "search_file_type_music"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeMusic:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 333
    const-string v1, "search_file_type_doc"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeDocument:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 334
    const-string v1, "search_file_type_down"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeDownloadedApps:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 335
    const-string v1, "search_location_device"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isDevice:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 336
    const-string v1, "search_location_sdcard"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSDcard:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 337
    const-string v1, "search_location_usb_a"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBa:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 338
    const-string v1, "search_location_usb_b"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBb:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 339
    const-string v1, "search_location_usb_c"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBc:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 340
    const-string v1, "search_location_usb_d"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBd:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 341
    const-string v1, "search_location_usb_e"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBe:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 342
    const-string v1, "search_location_usb_f"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBf:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 343
    const-string v1, "search_location_dropbox"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isDropbox:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 344
    const-string v1, "search_location_nearby"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isNearby:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 345
    const-string v1, "search_location_private"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isPrivate:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 348
    const-string v1, "dropbox_auth_key"

    sget-object v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mDropboxKey:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 349
    const-string v1, "dropbox_auth_secret"

    sget-object v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mDropboxSecret:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 352
    const-string v1, "expanded_view"

    sget v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mExpandedViewIdInSplitView:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 353
    const-string v1, "splitview_position"

    sget v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSplitLocation:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 355
    const-string v1, "downloadedappsize"

    sget-wide v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSavedDownloadedAppTotalSize:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 357
    const-string v1, "documentssize"

    sget-wide v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSavedDocumentsTotalSize:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 360
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v1

    goto/16 :goto_0
.end method

.method public commitDocumentsSize()Z
    .locals 4

    .prologue
    .line 1589
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    .line 1590
    const/4 v1, 0x0

    .line 1597
    :goto_0
    return v1

    .line 1593
    :cond_0
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1595
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "documentssize"

    sget-wide v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSavedDocumentsTotalSize:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 1597
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v1

    goto :goto_0
.end method

.method public commitDownAppSize()Z
    .locals 4

    .prologue
    .line 1481
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    .line 1482
    const/4 v1, 0x0

    .line 1491
    :goto_0
    return v1

    .line 1485
    :cond_0
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1488
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "downloadedappsize"

    sget-wide v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSavedDownloadedAppTotalSize:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 1491
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v1

    goto :goto_0
.end method

.method public commitSettings()Z
    .locals 3

    .prologue
    .line 367
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    .line 368
    const/4 v1, 0x0

    .line 377
    :goto_0
    return v1

    .line 371
    :cond_0
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 374
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "show_hidden_files"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mShowHiddenFiles:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 375
    const-string v1, "show_file_extension"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mShowFileExtention:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 377
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v1

    goto :goto_0
.end method

.method public commitShowDataWarning()Z
    .locals 3

    .prologue
    .line 381
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    .line 382
    const/4 v1, 0x0

    .line 394
    :goto_0
    return v1

    .line 385
    :cond_0
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 388
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "dropbox_warning"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mIsCheckedShowDialogDropBox:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 389
    const-string v1, "ftp_warning"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mIsCheckedShowDialogFTP:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 392
    const-string v1, "wlanon_warning"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mIsCheckedShowDialogWLANOn:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 394
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v1

    goto :goto_0
.end method

.method public deleteSetHomescreenPath(Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1563
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSavedSetHomeShorcutPaths:Ljava/util/Set;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSavedSetHomeShorcutPaths:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1565
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSavedSetHomeShorcutPaths:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1567
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    if-nez v1, :cond_1

    .line 1578
    :cond_0
    :goto_0
    return-void

    .line 1572
    :cond_1
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1575
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "homescreen_shortcut_path"

    sget-object v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSavedSetHomeShorcutPaths:Ljava/util/Set;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 1576
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public declared-synchronized getCategoryDirectory(I)Ljava/lang/String;
    .locals 2
    .param p1, "viewType"    # I

    .prologue
    const/4 v0, 0x0

    .line 567
    monitor-enter p0

    const/4 v1, 0x2

    if-lt p1, v1, :cond_0

    const/4 v1, 0x5

    if-le p1, v1, :cond_1

    .line 586
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 572
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 574
    :pswitch_0
    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mImageDirectory:Ljava/lang/String;

    goto :goto_0

    .line 577
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mVideoDirectory:Ljava/lang/String;

    goto :goto_0

    .line 580
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mMusicDirectory:Ljava/lang/String;

    goto :goto_0

    .line 583
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mDocumentDirectory:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 567
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 572
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public declared-synchronized getCurrentDirectory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 515
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPath:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 517
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getRootFolder()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPath:Ljava/lang/String;

    .line 520
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPath:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 515
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getCurrentDirectory(Z)Ljava/lang/String;
    .locals 1
    .param p1, "isBuaFolder"    # Z

    .prologue
    .line 525
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPath:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 527
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mHomeDirectory:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPath:Ljava/lang/String;

    .line 537
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPath:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 525
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getCurrentInOrder()I
    .locals 1

    .prologue
    .line 718
    monitor-enter p0

    :try_start_0
    sget v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mCurrentInOrder:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getCurrentSortBy()I
    .locals 1

    .prologue
    .line 694
    monitor-enter p0

    :try_start_0
    sget v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mCurrentListBy:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getCurrentView()I
    .locals 3

    .prologue
    .line 469
    monitor-enter p0

    :try_start_0
    sget v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mCurrentView:I

    if-ltz v1, :cond_0

    sget v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mCurrentView:I

    const/4 v2, 0x2

    if-le v1, v2, :cond_1

    .line 471
    :cond_0
    const/4 v1, 0x0

    sput v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mCurrentView:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 475
    :try_start_1
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "mLastView error!!!"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 477
    :catch_0
    move-exception v0

    .line 479
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 483
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    sget v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mCurrentView:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return v1

    .line 469
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public getDoNotShowCheckDropBox()Z
    .locals 1

    .prologue
    .line 1374
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mIsCheckedShowDialogDropBox:Z

    return v0
.end method

.method public getDoNotShowCheckFTP()Z
    .locals 1

    .prologue
    .line 1379
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mIsCheckedShowDialogFTP:Z

    return v0
.end method

.method public getDoNotShowCheckWLANOn()Z
    .locals 1

    .prologue
    .line 1384
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mIsCheckedShowDialogWLANOn:Z

    return v0
.end method

.method public declared-synchronized getDocumentDirectory()Ljava/lang/String;
    .locals 2

    .prologue
    .line 652
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mDocumentDirectory:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mDocumentDirectory:Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mDocumentDirectory:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 654
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mDocumentDirectory:Ljava/lang/String;

    .line 657
    :cond_1
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mDocumentDirectory:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 652
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getDownloadedAppTotalSize()J
    .locals 2

    .prologue
    .line 1523
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mDownloadedAppTotalSize:J

    return-wide v0
.end method

.method public getDragSourceFragmentId()I
    .locals 1

    .prologue
    .line 1339
    iget v0, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mDragSourceFragmentId:I

    return v0
.end method

.method public getDragSourcePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1351
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mDragSourcePath:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized getDropboxAuthKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1321
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mDropboxKey:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDropboxAuthSecret()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1327
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mDropboxSecret:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getExpandedViewId()I
    .locals 1

    .prologue
    .line 1403
    monitor-enter p0

    :try_start_0
    sget v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mExpandedViewIdInSplitView:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getFtpParamsString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1395
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mFtpParamsString:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized getHomeDirectory()Ljava/lang/String;
    .locals 2

    .prologue
    .line 423
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mHomeDirectory:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 425
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mHomeDirectory:Ljava/lang/String;

    .line 427
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mHomeDirectory:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 423
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getImageDirectory()Ljava/lang/String;
    .locals 2

    .prologue
    .line 599
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mImageDirectory:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mImageDirectory:Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mImageDirectory:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 601
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mImageDirectory:Ljava/lang/String;

    .line 604
    :cond_1
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mImageDirectory:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 599
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getKNOXInstalled()Z
    .locals 1

    .prologue
    .line 1291
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isKNOXInstalled:Z

    return v0
.end method

.method public declared-synchronized getLastDirectory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 789
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mLastDirectory:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getLastModifiedTime()J
    .locals 2

    .prologue
    .line 670
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mLastModifiedTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getManagedBrowserState()Z
    .locals 1

    .prologue
    .line 739
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->bMangaedNormalState:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getMusicDirectory()Ljava/lang/String;
    .locals 2

    .prologue
    .line 634
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mMusicDirectory:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mMusicDirectory:Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mMusicDirectory:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 636
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mMusicDirectory:Ljava/lang/String;

    .line 639
    :cond_1
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mMusicDirectory:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 634
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getSavedDocumentsTotalSize()J
    .locals 2

    .prologue
    .line 1510
    sget-wide v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSavedDocumentsTotalSize:J

    return-wide v0
.end method

.method public getSavedDownloadedAppTotalSize()J
    .locals 2

    .prologue
    .line 1504
    sget-wide v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSavedDownloadedAppTotalSize:J

    return-wide v0
.end method

.method public getSearchFileLocation()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1051
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchFileLocation:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSearchFromDate()J
    .locals 2

    .prologue
    .line 1028
    sget-wide v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchFromDate:J

    return-wide v0
.end method

.method public getSearchLocationDeviceID(I)Ljava/lang/String;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 1062
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchSelectedDeviceIDMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1063
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchSelectedDeviceIDMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1065
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSearchToDate()J
    .locals 2

    .prologue
    .line 1040
    sget-wide v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchToDate:J

    return-wide v0
.end method

.method public declared-synchronized getShowCategory()Z
    .locals 1

    .prologue
    .line 726
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mShowCategory:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getShowFileExtensionStatus()Z
    .locals 1

    .prologue
    .line 411
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mShowFileExtention:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getShowHiddenFileStatus()Z
    .locals 1

    .prologue
    .line 400
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mShowHiddenFiles:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getShowLockFilesStatus()Z
    .locals 3

    .prologue
    .line 299
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "lock_files_dialog"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mLockFilesDialog:Z

    .line 300
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mLockFilesDialog:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 299
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSplitLocation()I
    .locals 1

    .prologue
    .line 491
    monitor-enter p0

    :try_start_0
    sget v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSplitLocation:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getStartDirectory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 776
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mStartDirectory:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getVideoDirectory()Ljava/lang/String;
    .locals 2

    .prologue
    .line 616
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mVideoDirectory:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mVideoDirectory:Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mVideoDirectory:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 618
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mVideoDirectory:Ljava/lang/String;

    .line 621
    :cond_1
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mVideoDirectory:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 616
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getViewType()I
    .locals 1

    .prologue
    .line 758
    monitor-enter p0

    :try_start_0
    sget v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mViewType:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getmAdapter()Landroid/widget/BaseAdapter;
    .locals 1

    .prologue
    .line 1462
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mAdapter:Landroid/widget/BaseAdapter;

    return-object v0
.end method

.method public getmExtensionForCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1452
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mExtensionForCategory:Ljava/lang/String;

    return-object v0
.end method

.method public getmMimeTypeForCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1442
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mMimeTypeForCategory:Ljava/lang/String;

    return-object v0
.end method

.method public getmPathForDropboxMultiDrag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1472
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPathForDropboxMultiDrag:Ljava/lang/String;

    return-object v0
.end method

.method public getmSearchFileTypeIndex()I
    .locals 1

    .prologue
    .line 1085
    sget v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchFileTypeIndex:I

    return v0
.end method

.method public isSearchFileTypeAll()Z
    .locals 1

    .prologue
    .line 1095
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeAll:Z

    return v0
.end method

.method public isSearchFileTypeDocument()Z
    .locals 1

    .prologue
    .line 1143
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeDocument:Z

    return v0
.end method

.method public isSearchFileTypeDownloadedApps()Z
    .locals 1

    .prologue
    .line 1300
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeDownloadedApps:Z

    return v0
.end method

.method public isSearchFileTypeImages()Z
    .locals 1

    .prologue
    .line 1107
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeImages:Z

    return v0
.end method

.method public isSearchFileTypeMusic()Z
    .locals 1

    .prologue
    .line 1131
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeMusic:Z

    return v0
.end method

.method public isSearchFileTypeVideos()Z
    .locals 1

    .prologue
    .line 1119
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeVideos:Z

    return v0
.end method

.method public isSearchLocationBaidu()Z
    .locals 1

    .prologue
    .line 1248
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isBaidu:Z

    return v0
.end method

.method public isSearchLocationDevice()Z
    .locals 1

    .prologue
    .line 1154
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isDevice:Z

    return v0
.end method

.method public isSearchLocationDropbox()Z
    .locals 1

    .prologue
    .line 1243
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isDropbox:Z

    return v0
.end method

.method public isSearchLocationNearby()Z
    .locals 1

    .prologue
    .line 1259
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isNearby:Z

    return v0
.end method

.method public isSearchLocationPrivate()Z
    .locals 1

    .prologue
    .line 1281
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isPrivate:Z

    return v0
.end method

.method public isSearchLocationSDcard()Z
    .locals 1

    .prologue
    .line 1165
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSDcard:Z

    return v0
.end method

.method public isSearchLocationUSBa()Z
    .locals 1

    .prologue
    .line 1176
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBa:Z

    return v0
.end method

.method public isSearchLocationUSBb()Z
    .locals 1

    .prologue
    .line 1182
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBb:Z

    return v0
.end method

.method public isSearchLocationUSBc()Z
    .locals 1

    .prologue
    .line 1192
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBc:Z

    return v0
.end method

.method public isSearchLocationUSBd()Z
    .locals 1

    .prologue
    .line 1202
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBd:Z

    return v0
.end method

.method public isSearchLocationUSBe()Z
    .locals 1

    .prologue
    .line 1212
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBe:Z

    return v0
.end method

.method public isSearchLocationUSBf()Z
    .locals 1

    .prologue
    .line 1222
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBf:Z

    return v0
.end method

.method public isSetHoemScreenPath(Ljava/lang/String;)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1553
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSavedSetHomeShorcutPaths:Ljava/util/Set;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSavedSetHomeShorcutPaths:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1555
    const/4 v0, 0x1

    .line 1558
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public saveSetHomescreenPath(Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1531
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSavedSetHomeShorcutPaths:Ljava/util/Set;

    if-nez v1, :cond_0

    .line 1533
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    sput-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSavedSetHomeShorcutPaths:Ljava/util/Set;

    .line 1536
    :cond_0
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSavedSetHomeShorcutPaths:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1537
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSavedSetHomeShorcutPaths:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1539
    :cond_1
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    if-nez v1, :cond_2

    .line 1549
    :goto_0
    return-void

    .line 1544
    :cond_2
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1547
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "homescreen_shortcut_path"

    sget-object v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSavedSetHomeShorcutPaths:Ljava/util/Set;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 1548
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public declared-synchronized setCategoryDirectory(ILjava/lang/String;)V
    .locals 1
    .param p1, "categoryType"    # I
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 545
    monitor-enter p0

    packed-switch p1, :pswitch_data_0

    .line 563
    :goto_0
    monitor-exit p0

    return-void

    .line 548
    :pswitch_0
    :try_start_0
    sput-object p2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mImageDirectory:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 545
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 552
    :pswitch_1
    :try_start_1
    sput-object p2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mVideoDirectory:Ljava/lang/String;

    goto :goto_0

    .line 556
    :pswitch_2
    sput-object p2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mMusicDirectory:Ljava/lang/String;

    goto :goto_0

    .line 560
    :pswitch_3
    sput-object p2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mDocumentDirectory:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 545
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public declared-synchronized setCurrentDirectory(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 503
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPath:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 511
    monitor-exit p0

    return-void

    .line 503
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setCurrentInOrder(I)V
    .locals 3
    .param p1, "currentInOrder"    # I

    .prologue
    .line 702
    monitor-enter p0

    :try_start_0
    sput p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mCurrentInOrder:I

    .line 704
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 714
    :goto_0
    monitor-exit p0

    return-void

    .line 709
    :cond_0
    :try_start_1
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 711
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "current_order_by"

    sget v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mCurrentInOrder:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 713
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 702
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized setCurrentSortBy(I)V
    .locals 3
    .param p1, "currentListBy"    # I

    .prologue
    .line 678
    monitor-enter p0

    :try_start_0
    sput p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mCurrentListBy:I

    .line 680
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 690
    :goto_0
    monitor-exit p0

    return-void

    .line 685
    :cond_0
    :try_start_1
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 687
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "current_sort_by"

    sget v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mCurrentListBy:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 689
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 678
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized setCurrentView(I)V
    .locals 4
    .param p1, "view"    # I

    .prologue
    .line 439
    monitor-enter p0

    :try_start_0
    sget v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mCurrentView:I

    if-ltz v2, :cond_0

    sget v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mCurrentView:I

    const/4 v3, 0x2

    if-le v2, v3, :cond_1

    .line 441
    :cond_0
    const/4 v2, 0x0

    sput v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mCurrentView:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 445
    :try_start_1
    new-instance v2, Ljava/lang/Exception;

    const-string v3, "mLastView error!!!"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 447
    :catch_0
    move-exception v0

    .line 449
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 453
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    sput p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mCurrentView:I

    .line 455
    sget-object v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v2, :cond_2

    .line 465
    :goto_0
    monitor-exit p0

    return-void

    .line 460
    :cond_2
    :try_start_3
    sget-object v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 462
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "current_view"

    sget v3, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mCurrentView:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 464
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 439
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public setDoNotShowCheckDropBox(Z)V
    .locals 0
    .param p1, "bCheck"    # Z

    .prologue
    .line 1359
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mIsCheckedShowDialogDropBox:Z

    .line 1360
    return-void
.end method

.method public setDoNotShowCheckFTP(Z)V
    .locals 0
    .param p1, "bCheck"    # Z

    .prologue
    .line 1364
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mIsCheckedShowDialogFTP:Z

    .line 1365
    return-void
.end method

.method public setDoNotShowCheckWLANOn(Z)V
    .locals 0
    .param p1, "bCheck"    # Z

    .prologue
    .line 1369
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mIsCheckedShowDialogWLANOn:Z

    .line 1370
    return-void
.end method

.method public declared-synchronized setDocumentDirectory(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 647
    monitor-enter p0

    :try_start_0
    sput-object p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mDocumentDirectory:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 648
    monitor-exit p0

    return-void

    .line 647
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setDocumentsTotalSize(J)V
    .locals 1
    .param p1, "totalDocumentSize"    # J

    .prologue
    .line 1583
    sput-wide p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSavedDocumentsTotalSize:J

    .line 1585
    return-void
.end method

.method public setDownloadedAppTotalSize(J)V
    .locals 1
    .param p1, "totalSize"    # J

    .prologue
    .line 1518
    iput-wide p1, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mDownloadedAppTotalSize:J

    .line 1519
    return-void
.end method

.method public setDragSourceFragmentId(I)V
    .locals 0
    .param p1, "fragmentId"    # I

    .prologue
    .line 1333
    iput p1, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mDragSourceFragmentId:I

    .line 1334
    return-void
.end method

.method public setDragSourcePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1345
    iput-object p1, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mDragSourcePath:Ljava/lang/String;

    .line 1346
    return-void
.end method

.method public declared-synchronized setDropboxAuthKey(Ljava/lang/String;)V
    .locals 1
    .param p1, "AuthKey"    # Ljava/lang/String;

    .prologue
    .line 1311
    monitor-enter p0

    :try_start_0
    sput-object p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mDropboxKey:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1312
    monitor-exit p0

    return-void

    .line 1311
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setDropboxAuthSecret(Ljava/lang/String;)V
    .locals 1
    .param p1, "Secret"    # Ljava/lang/String;

    .prologue
    .line 1316
    monitor-enter p0

    :try_start_0
    sput-object p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mDropboxSecret:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1317
    monitor-exit p0

    return-void

    .line 1316
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setExpandedViewId(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 1409
    monitor-enter p0

    :try_start_0
    sput p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mExpandedViewIdInSplitView:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1410
    monitor-exit p0

    return-void

    .line 1409
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setFtpParamsString(Ljava/lang/String;)V
    .locals 0
    .param p1, "paramsString"    # Ljava/lang/String;

    .prologue
    .line 1389
    iput-object p1, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mFtpParamsString:Ljava/lang/String;

    .line 1391
    return-void
.end method

.method public declared-synchronized setHomeDirectory(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 431
    monitor-enter p0

    :try_start_0
    sput-object p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mHomeDirectory:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 432
    monitor-exit p0

    return-void

    .line 431
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setImageDirectory(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 594
    monitor-enter p0

    :try_start_0
    sput-object p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mImageDirectory:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 595
    monitor-exit p0

    return-void

    .line 594
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setKNOXInstalled(Z)V
    .locals 0
    .param p1, "state"    # Z

    .prologue
    .line 1286
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isKNOXInstalled:Z

    .line 1287
    return-void
.end method

.method public declared-synchronized setLastDirectory(Ljava/lang/String;)V
    .locals 1
    .param p1, "directory"    # Ljava/lang/String;

    .prologue
    .line 784
    monitor-enter p0

    :try_start_0
    sput-object p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mLastDirectory:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 785
    monitor-exit p0

    return-void

    .line 784
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setLastModifiedTime(J)V
    .locals 1
    .param p1, "time"    # J

    .prologue
    .line 665
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mLastModifiedTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 666
    monitor-exit p0

    return-void

    .line 665
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setManagedBrowserState(Z)V
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 744
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 746
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->bMangaedNormalState:Z

    .line 748
    const-string v1, "managed_browser_normal_state"

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->bMangaedNormalState:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 750
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 751
    monitor-exit p0

    return-void

    .line 744
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized setMusicDirectory(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 629
    monitor-enter p0

    :try_start_0
    sput-object p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mMusicDirectory:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 630
    monitor-exit p0

    return-void

    .line 629
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setSavedDownloadedAppTotalSize(J)V
    .locals 1
    .param p1, "totalSize"    # J

    .prologue
    .line 1498
    sput-wide p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSavedDownloadedAppTotalSize:J

    .line 1499
    return-void
.end method

.method public setSearchFileLocation(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1056
    .local p1, "searchFileLocation":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sput-object p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchFileLocation:Ljava/util/ArrayList;

    .line 1057
    return-void
.end method

.method public setSearchFileTypeAll(Z)V
    .locals 0
    .param p1, "isSearchFileTypeAll"    # Z

    .prologue
    .line 1101
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeAll:Z

    .line 1102
    return-void
.end method

.method public setSearchFileTypeDocument(Z)V
    .locals 0
    .param p1, "isSearchFileTypeDocument"    # Z

    .prologue
    .line 1296
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeDocument:Z

    .line 1297
    return-void
.end method

.method public setSearchFileTypeDownloadedApps(Z)V
    .locals 0
    .param p1, "isSearchFileTypeDownloadedApps"    # Z

    .prologue
    .line 1305
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeDownloadedApps:Z

    .line 1306
    return-void
.end method

.method public setSearchFileTypeImages(Z)V
    .locals 0
    .param p1, "isSearchFileTypeImages"    # Z

    .prologue
    .line 1113
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeImages:Z

    .line 1114
    return-void
.end method

.method public setSearchFileTypeIndex(I)V
    .locals 0
    .param p1, "mSearchFileTypeIndex"    # I

    .prologue
    .line 1090
    sput p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchFileTypeIndex:I

    .line 1091
    return-void
.end method

.method public setSearchFileTypeMusic(Z)V
    .locals 0
    .param p1, "isSearchFileTypeMusic"    # Z

    .prologue
    .line 1137
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeMusic:Z

    .line 1138
    return-void
.end method

.method public setSearchFileTypeVideos(Z)V
    .locals 0
    .param p1, "isSearchFileTypeVideos"    # Z

    .prologue
    .line 1125
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeVideos:Z

    .line 1126
    return-void
.end method

.method public setSearchFromDate(J)V
    .locals 1
    .param p1, "mSearchFromDate"    # J

    .prologue
    .line 1034
    sput-wide p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchFromDate:J

    .line 1035
    return-void
.end method

.method public setSearchLocationBaidu(Z)V
    .locals 0
    .param p1, "isSearchLocationBaidu"    # Z

    .prologue
    .line 1238
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isBaidu:Z

    .line 1239
    return-void
.end method

.method public setSearchLocationDevice(Z)V
    .locals 0
    .param p1, "isSearchLocationDevice"    # Z

    .prologue
    .line 1148
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isDevice:Z

    .line 1149
    return-void
.end method

.method public setSearchLocationDeviceID(ILjava/lang/String;)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "searchLocationDeviceID"    # Ljava/lang/String;

    .prologue
    .line 1070
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchSelectedDeviceIDMap:Ljava/util/Map;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchSelectedDeviceIDMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1071
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchSelectedDeviceIDMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1073
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchSelectedDeviceIDMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1074
    return-void
.end method

.method public setSearchLocationDeviceIDClear()V
    .locals 1

    .prologue
    .line 1078
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchSelectedDeviceIDMap:Ljava/util/Map;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchSelectedDeviceIDMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1079
    sget-object v0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchSelectedDeviceIDMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1080
    :cond_0
    return-void
.end method

.method public setSearchLocationDropbox(Z)V
    .locals 0
    .param p1, "isSearchLocationDropbox"    # Z

    .prologue
    .line 1233
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isDropbox:Z

    .line 1234
    return-void
.end method

.method public setSearchLocationNearby(Z)V
    .locals 0
    .param p1, "isSearchLocationNearby"    # Z

    .prologue
    .line 1253
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isNearby:Z

    .line 1254
    return-void
.end method

.method public setSearchLocationPrivate(Z)V
    .locals 0
    .param p1, "isSearchLocationPrivate"    # Z

    .prologue
    .line 1275
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isPrivate:Z

    .line 1276
    return-void
.end method

.method public setSearchLocationSDcard(Z)V
    .locals 0
    .param p1, "isSearchLocationSDcard"    # Z

    .prologue
    .line 1159
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSDcard:Z

    .line 1160
    return-void
.end method

.method public setSearchLocationUSBa(Z)V
    .locals 0
    .param p1, "isSearchLocationUSBa"    # Z

    .prologue
    .line 1170
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBa:Z

    .line 1171
    return-void
.end method

.method public setSearchLocationUSBb(Z)V
    .locals 0
    .param p1, "isUSBb"    # Z

    .prologue
    .line 1187
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBb:Z

    .line 1188
    return-void
.end method

.method public setSearchLocationUSBc(Z)V
    .locals 0
    .param p1, "isUSBc"    # Z

    .prologue
    .line 1197
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBc:Z

    .line 1198
    return-void
.end method

.method public setSearchLocationUSBd(Z)V
    .locals 0
    .param p1, "isUSBd"    # Z

    .prologue
    .line 1207
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBd:Z

    .line 1208
    return-void
.end method

.method public setSearchLocationUSBe(Z)V
    .locals 0
    .param p1, "isUSBe"    # Z

    .prologue
    .line 1217
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBe:Z

    .line 1218
    return-void
.end method

.method public setSearchLocationUSBf(Z)V
    .locals 0
    .param p1, "isUSBf"    # Z

    .prologue
    .line 1227
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isUSBf:Z

    .line 1228
    return-void
.end method

.method public setSearchToDate(J)V
    .locals 1
    .param p1, "mSearchToDate"    # J

    .prologue
    .line 1046
    sput-wide p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSearchToDate:J

    .line 1047
    return-void
.end method

.method public declared-synchronized setShowCategory(Z)V
    .locals 1
    .param p1, "showCategory"    # Z

    .prologue
    .line 731
    monitor-enter p0

    :try_start_0
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mShowCategory:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 732
    monitor-exit p0

    return-void

    .line 731
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setShowFileExtensionStatus(Z)V
    .locals 1
    .param p1, "show"    # Z

    .prologue
    .line 415
    monitor-enter p0

    :try_start_0
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mShowFileExtention:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 416
    monitor-exit p0

    return-void

    .line 415
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setShowHiddenFileStatus(Z)V
    .locals 1
    .param p1, "show"    # Z

    .prologue
    .line 404
    monitor-enter p0

    :try_start_0
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mShowHiddenFiles:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405
    monitor-exit p0

    return-void

    .line 404
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setShowLockFilesStatus(Z)V
    .locals 4
    .param p1, "show"    # Z

    .prologue
    .line 304
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v2, "lock_files_dialog"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mLockFilesDialog:Z

    .line 305
    sget-object v1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 306
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "lock_files_dialog"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 307
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 308
    monitor-exit p0

    return-void

    .line 304
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized setSplitLocation(I)V
    .locals 1
    .param p1, "location"    # I

    .prologue
    .line 496
    monitor-enter p0

    :try_start_0
    sput p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mSplitLocation:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 497
    monitor-exit p0

    return-void

    .line 496
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setStartDirectory(Ljava/lang/String;)V
    .locals 1
    .param p1, "directory"    # Ljava/lang/String;

    .prologue
    .line 771
    monitor-enter p0

    :try_start_0
    sput-object p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mStartDirectory:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 772
    monitor-exit p0

    return-void

    .line 771
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setVideoDirectory(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 612
    monitor-enter p0

    :try_start_0
    sput-object p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mVideoDirectory:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 613
    monitor-exit p0

    return-void

    .line 612
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setViewType(I)V
    .locals 1
    .param p1, "viewType"    # I

    .prologue
    .line 763
    monitor-enter p0

    :try_start_0
    sput p1, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mViewType:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 764
    monitor-exit p0

    return-void

    .line 763
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setmAdapter(Landroid/widget/BaseAdapter;)V
    .locals 0
    .param p1, "mAdapter"    # Landroid/widget/BaseAdapter;

    .prologue
    .line 1467
    iput-object p1, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mAdapter:Landroid/widget/BaseAdapter;

    .line 1468
    return-void
.end method

.method public setmExtensionForCategory(Ljava/lang/String;)V
    .locals 0
    .param p1, "mExtensionForCategory"    # Ljava/lang/String;

    .prologue
    .line 1457
    iput-object p1, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mExtensionForCategory:Ljava/lang/String;

    .line 1458
    return-void
.end method

.method public setmMimeTypeForCategory(Ljava/lang/String;)V
    .locals 0
    .param p1, "mMimeTypeForCategory"    # Ljava/lang/String;

    .prologue
    .line 1447
    iput-object p1, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mMimeTypeForCategory:Ljava/lang/String;

    .line 1448
    return-void
.end method

.method public setmPathForDropboxMultiDrag(Ljava/lang/String;)V
    .locals 0
    .param p1, "mPathForDropboxMultiDrag"    # Ljava/lang/String;

    .prologue
    .line 1477
    iput-object p1, p0, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->mPathForDropboxMultiDrag:Ljava/lang/String;

    .line 1478
    return-void
.end method
