.class public Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;
.super Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
.source "VideoHoverPopup.java"


# static fields
.field private static final EXTERNAL_MEDIA_VIDEO_DB_URI:Landroid/net/Uri;

.field private static final INTERNAL_MEDIA_VIDEO_DB_URI:Landroid/net/Uri;

.field private static final MODULE:Ljava/lang/String; = "VideoHoverPopup"

.field private static final NO_DB_BASE_WIDTH_HEIGHT:I = 0x190

.field private static final PRE_W_LIST_HORI:I = 0x2ee

.field private static final PRE_W_LIST_NO_VIDEO:I = 0xc8

.field private static final PRE_W_LIST_VERT:I = 0x21c


# instance fields
.field private final CHANGE_SIZE_VALUE:I

.field protected layout:Landroid/widget/RelativeLayout;

.field private mFromDB:Z

.field protected mHoverOperationContainer:Landroid/view/View;

.field private mMeasuredVideoHeight:I

.field private mMeasuredVideoWidth:I

.field private mPreviewWidth:I

.field private mSHCallback:Landroid/view/SurfaceHolder$Callback;

.field private mTargetHeightWeight:I

.field private mTargetWidthWeight:I

.field private mTotalVideoHeight:I

.field protected mUri:Landroid/net/Uri;

.field protected mediaPlayer:Landroid/media/MediaPlayer;

.field private noVideoView:Landroid/widget/ImageView;

.field private surfaceHolder:Landroid/view/SurfaceHolder;

.field private surfaceView:Landroid/view/SurfaceView;

.field private videoHeight:I

.field private videoWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->EXTERNAL_MEDIA_VIDEO_DB_URI:Landroid/net/Uri;

    .line 77
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->INTERNAL_MEDIA_VIDEO_DB_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v2, 0xfa

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 106
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;-><init>(Landroid/content/Context;)V

    .line 66
    iput v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->CHANGE_SIZE_VALUE:I

    .line 81
    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mUri:Landroid/net/Uri;

    .line 83
    iput v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->videoWidth:I

    .line 84
    iput v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->videoHeight:I

    .line 85
    iput v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mPreviewWidth:I

    .line 86
    iput v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoWidth:I

    .line 87
    iput v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoHeight:I

    .line 88
    iput v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mTotalVideoHeight:I

    .line 90
    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->surfaceView:Landroid/view/SurfaceView;

    .line 91
    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mFromDB:Z

    .line 96
    iput v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mTargetWidthWeight:I

    .line 97
    iput v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mTargetHeightWeight:I

    .line 99
    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 101
    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->layout:Landroid/widget/RelativeLayout;

    .line 103
    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mHoverOperationContainer:Landroid/view/View;

    .line 110
    new-instance v0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$1;-><init>(Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    .line 108
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;
    .param p1, "x1"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    return-object p1
.end method

.method private checkIsCalling(Landroid/content/Context;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 280
    :try_start_0
    const-string v2, "audio"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    invoke-virtual {v2}, Landroid/media/AudioManager;->getMode()I

    move-result v0

    .line 282
    .local v0, "audio_mode":I
    const/4 v2, 0x2

    const-string v4, "VideoHoverPopup"

    const-string v5, "checkIsCalling() - audiomode = "

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 283
    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    .line 285
    const/4 v2, 0x1

    .line 293
    .end local v0    # "audio_mode":I
    :goto_0
    return v2

    .line 288
    :catch_0
    move-exception v1

    .line 290
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "VideoHoverPopup"

    const-string v4, "checkIsCalling() - Fail to get an AUDIO_SERVICE !!!"

    invoke-static {v3, v2, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    move v2, v3

    .line 293
    goto :goto_0
.end method

.method private extractData(Ljava/lang/String;I)Ljava/lang/String;
    .locals 9
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "dataType"    # I

    .prologue
    const/4 v8, 0x2

    .line 135
    const-string v4, "VideoHoverPopup"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "extractData() type:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v8, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 137
    if-nez p1, :cond_0

    .line 138
    const/4 v2, 0x0

    .line 161
    :goto_0
    return-object v2

    .line 140
    :cond_0
    const/4 v2, 0x0

    .line 141
    .local v2, "extracted":Ljava/lang/String;
    new-instance v3, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v3}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 144
    .local v3, "retriever":Landroid/media/MediaMetadataRetriever;
    :try_start_0
    invoke-virtual {v3, p1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 145
    invoke-virtual {v3, p2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 155
    invoke-virtual {v3}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 158
    :goto_1
    const-string v4, "VideoHoverPopup"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "=> path = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v8, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 159
    const-string v4, "VideoHoverPopup"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "=> extracted = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v8, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 146
    :catch_0
    move-exception v1

    .line 148
    .local v1, "ex":Ljava/lang/IllegalArgumentException;
    const/4 v4, 0x0

    :try_start_1
    const-string v5, "VideoHoverPopup"

    const-string v6, "IllegalArgumentException - Unable to open content"

    invoke-static {v4, v5, v6, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 150
    const/4 v4, 0x2

    const-string v5, "VideoHoverPopup"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "=> path = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 155
    invoke-virtual {v3}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_1

    .line 151
    .end local v1    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 153
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 155
    invoke-virtual {v3}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_1

    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    invoke-virtual {v3}, Landroid/media/MediaMetadataRetriever;->release()V

    throw v4
.end method

.method private fetchString(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "column"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x2

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 393
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mUri:Landroid/net/Uri;

    if-nez v0, :cond_1

    .line 395
    const-string v0, "VideoHoverPopup"

    const-string v1, "fetchString - mUri == null"

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    move-object v0, v8

    .line 433
    :cond_0
    :goto_0
    return-object v0

    .line 399
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_2

    .line 401
    const-string v0, "VideoHoverPopup"

    const-string v1, "fetchString - mResolver == null"

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    move-object v0, v8

    .line 402
    goto :goto_0

    .line 405
    :cond_2
    const/4 v6, 0x0

    .line 406
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    aput-object p1, v2, v9

    .line 410
    .local v2, "col":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mUri:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 412
    if-nez v6, :cond_3

    .line 413
    const/4 v0, 0x0

    const-string v1, "VideoHoverPopup"

    const-string v3, "cursor == null"

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 415
    :cond_3
    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    .line 416
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 417
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 427
    const-string v1, "VideoHoverPopup"

    const-string v3, "finally"

    invoke-static {v9, v1, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 429
    if-eqz v6, :cond_0

    .line 430
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 427
    :cond_4
    const-string v0, "VideoHoverPopup"

    const-string v1, "finally"

    invoke-static {v9, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 429
    if-eqz v6, :cond_5

    .line 430
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    :goto_1
    move-object v0, v8

    .line 433
    goto :goto_0

    .line 421
    :catch_0
    move-exception v7

    .line 423
    .local v7, "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    :try_start_1
    const-string v1, "VideoHoverPopup"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "returnString() e:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 427
    const-string v0, "VideoHoverPopup"

    const-string v1, "finally"

    invoke-static {v9, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 429
    if-eqz v6, :cond_5

    .line 430
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 427
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    const-string v1, "VideoHoverPopup"

    const-string v3, "finally"

    invoke-static {v9, v1, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 429
    if-eqz v6, :cond_6

    .line 430
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0
.end method

.method private getBitmapData(Ljava/lang/String;J)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "pos"    # J

    .prologue
    const/16 v4, 0xfa

    .line 217
    const/4 v0, 0x0

    .line 218
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 219
    .local v1, "retriever":Landroid/media/MediaMetadataRetriever;
    invoke-virtual {v1, p1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 220
    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p2

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(J)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 221
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 223
    if-eqz v0, :cond_0

    .line 225
    const/4 v2, 0x2

    invoke-static {v0, v4, v4, v2}, Landroid/media/ThumbnailUtils;->extractThumbnail(Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 228
    :cond_0
    return-object v0
.end method

.method private getCheckRoation()I
    .locals 4

    .prologue
    .line 233
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mFilePath:Ljava/lang/String;

    const/16 v3, 0x18

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->extractData(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 235
    .local v0, "rotation":Ljava/lang/String;
    if-eqz v0, :cond_0

    .end local v0    # "rotation":Ljava/lang/String;
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 237
    .local v1, "rotationValue":I
    packed-switch v1, :pswitch_data_0

    .line 247
    :pswitch_0
    const/4 v1, 0x1

    .line 251
    :goto_1
    return v1

    .line 235
    .end local v1    # "rotationValue":I
    .restart local v0    # "rotation":Ljava/lang/String;
    :cond_0
    const-string v0, "0"

    goto :goto_0

    .line 241
    .end local v0    # "rotation":Ljava/lang/String;
    .restart local v1    # "rotationValue":I
    :pswitch_1
    const/4 v1, 0x0

    .line 242
    goto :goto_1

    .line 237
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J
    .locals 10
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 166
    const-wide/16 v8, -0x1

    .line 167
    .local v8, "id":J
    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    .line 168
    .local v2, "cols":[Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 169
    .local v7, "where":Ljava/lang/StringBuilder;
    const-string v0, "_data=?"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    const/4 v6, 0x0

    .line 174
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v4, v1

    const/4 v5, 0x0

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 175
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 176
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    .line 182
    :cond_0
    if-eqz v6, :cond_1

    .line 183
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 186
    :cond_1
    return-wide v8

    .line 182
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 183
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private getRawY(I)I
    .locals 5
    .param p1, "mY"    # I

    .prologue
    .line 256
    const/4 v0, 0x0

    .line 258
    .local v0, "mHoverHeight":I
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->getStatusBarHeight(Landroid/content/Context;)I

    move-result v1

    .line 260
    .local v1, "statusBarHeight":I
    iget v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoHeight:I

    iget-object v3, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mHoverRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mHoverRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    sub-int/2addr v2, v3

    div-int/lit8 v0, v2, 0x2

    .line 262
    iget v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoHeight:I

    iget-object v3, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mHoverRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mHoverRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    if-gt v2, v3, :cond_0

    .line 263
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mHoverRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v1

    .line 272
    :goto_0
    return v2

    .line 266
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActionBar;->getHeight()I

    move-result v2

    add-int/2addr v2, v1

    sub-int v3, p1, v0

    if-le v2, v3, :cond_1

    .line 268
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mHoverRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v1

    goto :goto_0

    .line 272
    :cond_1
    sub-int v2, p1, v1

    sub-int/2addr v2, v0

    goto :goto_0
.end method

.method private getUriByPath(Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 8
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "ExternalMediaUri"    # Landroid/net/Uri;
    .param p3, "InternalMediaUri"    # Landroid/net/Uri;

    .prologue
    const/4 v3, 0x0

    const-wide/16 v6, -0x1

    .line 191
    if-eqz p1, :cond_0

    const-string v4, ""

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    move-object v2, v3

    .line 212
    :cond_1
    :goto_0
    return-object v2

    .line 194
    :cond_2
    const/4 v2, 0x0

    .line 195
    .local v2, "uri":Landroid/net/Uri;
    const-wide/16 v0, -0x1

    .line 197
    .local v0, "id":J
    cmp-long v4, v0, v6

    if-nez v4, :cond_3

    .line 199
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v0

    .line 200
    invoke-static {p2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 203
    :cond_3
    cmp-long v4, v0, v6

    if-nez v4, :cond_4

    .line 205
    invoke-direct {p0, p1, p3}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v0

    .line 206
    invoke-static {p3, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 209
    :cond_4
    cmp-long v4, v0, v6

    if-nez v4, :cond_1

    move-object v2, v3

    .line 210
    goto :goto_0
.end method

.method private getVideoViewSize()Z
    .locals 13

    .prologue
    const/16 v12, 0xfa

    const/4 v8, 0x1

    const/4 v11, 0x2

    const/16 v10, 0x190

    const/4 v7, 0x0

    .line 318
    const/4 v0, 0x0

    .line 320
    .local v0, "dimensionsString":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mUri:Landroid/net/Uri;

    if-nez v9, :cond_1

    .line 322
    const-string v8, "VideoHoverPopup"

    const-string v9, "mUri == null ~ !!!"

    invoke-static {v11, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 323
    iput v10, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoHeight:I

    .line 324
    iput v10, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoWidth:I

    .line 387
    :cond_0
    :goto_0
    return v7

    .line 328
    :cond_1
    const-string v9, "resolution"

    invoke-direct {p0, v9}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->fetchString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 330
    if-eqz v0, :cond_4

    .line 331
    const-string v9, "x"

    invoke-virtual {v0, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 332
    .local v1, "index":I
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    .line 334
    .local v5, "totalLenght":I
    invoke-virtual {v0, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iput v9, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->videoWidth:I

    .line 335
    add-int/lit8 v9, v1, 0x1

    invoke-virtual {v0, v9, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iput v9, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->videoHeight:I

    .line 345
    iget v9, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->videoWidth:I

    if-eqz v9, :cond_6

    iget v9, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->videoHeight:I

    if-eqz v9, :cond_6

    .line 347
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->getCheckRoation()I

    move-result v3

    .line 348
    .local v3, "rotation":I
    if-ne v3, v8, :cond_2

    .line 349
    iget v4, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->videoWidth:I

    .line 350
    .local v4, "temp":I
    iget v9, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->videoHeight:I

    iput v9, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->videoWidth:I

    .line 351
    iput v4, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->videoHeight:I

    .line 354
    .end local v4    # "temp":I
    :cond_2
    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->setPreviewZoomSize(I)Z

    move-result v2

    .line 355
    .local v2, "result":Z
    if-eqz v2, :cond_0

    .line 359
    const/4 v6, 0x0

    .line 361
    .local v6, "widthMulRatio":F
    iget v7, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mPreviewWidth:I

    int-to-float v7, v7

    iget v9, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->videoWidth:I

    int-to-float v9, v9

    div-float v6, v7, v9

    .line 363
    iget v7, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->videoHeight:I

    int-to-float v7, v7

    mul-float/2addr v7, v6

    float-to-double v10, v7

    invoke-static {v10, v11}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v10

    double-to-int v7, v10

    iput v7, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoHeight:I

    .line 364
    iget v7, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mPreviewWidth:I

    iput v7, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoWidth:I

    .line 367
    iput v12, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mTargetWidthWeight:I

    .line 368
    iput v12, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mTargetHeightWeight:I

    .line 370
    iget v7, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoWidth:I

    iget v9, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoHeight:I

    if-le v7, v9, :cond_5

    .line 372
    iget v7, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoHeight:I

    mul-int/lit16 v7, v7, 0xfa

    iget v9, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoWidth:I

    div-int/2addr v7, v9

    iput v7, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mTargetHeightWeight:I

    .line 379
    :cond_3
    :goto_1
    iget v7, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoHeight:I

    iget v9, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mTargetHeightWeight:I

    add-int/2addr v7, v9

    iput v7, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoHeight:I

    .line 380
    iget v7, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoWidth:I

    iget v9, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mTargetWidthWeight:I

    add-int/2addr v7, v9

    iput v7, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoWidth:I

    move v7, v8

    .line 382
    goto/16 :goto_0

    .line 339
    .end local v1    # "index":I
    .end local v2    # "result":Z
    .end local v3    # "rotation":I
    .end local v5    # "totalLenght":I
    .end local v6    # "widthMulRatio":F
    :cond_4
    const-string v8, "VideoHoverPopup"

    const-string v9, "dimensionsString == null ~ !!!"

    invoke-static {v11, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 340
    iput v10, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoHeight:I

    .line 341
    iput v10, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoWidth:I

    goto/16 :goto_0

    .line 374
    .restart local v1    # "index":I
    .restart local v2    # "result":Z
    .restart local v3    # "rotation":I
    .restart local v5    # "totalLenght":I
    .restart local v6    # "widthMulRatio":F
    :cond_5
    iget v7, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoWidth:I

    iget v9, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoHeight:I

    if-ge v7, v9, :cond_3

    .line 376
    iget v7, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoWidth:I

    mul-int/lit16 v7, v7, 0xfa

    iget v9, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoHeight:I

    div-int/2addr v7, v9

    iput v7, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mTargetWidthWeight:I

    goto :goto_1

    .line 386
    .end local v2    # "result":Z
    .end local v3    # "rotation":I
    .end local v6    # "widthMulRatio":F
    :cond_6
    const-string v8, "VideoHoverPopup"

    const-string v9, "Video width or height zero"

    invoke-static {v11, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private playVideo()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x2

    .line 467
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mUri:Landroid/net/Uri;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    if-nez v2, :cond_1

    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mFromDB:Z

    if-eqz v2, :cond_1

    .line 469
    const-string v2, "VideoHoverPopup"

    const-string v3, "openVideo() uri null or surfaceholder null"

    invoke-static {v5, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 600
    :goto_0
    return-void

    .line 473
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_2

    .line 475
    iput-object v3, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 478
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->requestAudioFocus()Z

    move-result v2

    if-nez v2, :cond_3

    .line 480
    const-string v2, "VideoHoverPopup"

    const-string v3, "start() : requestAudioFocus() is false"

    invoke-static {v6, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 481
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->getHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 487
    :cond_3
    :try_start_0
    new-instance v2, Landroid/media/MediaPlayer;

    invoke-direct {v2}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 489
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v3, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$2;-><init>(Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;)V

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 498
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v3, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$3;-><init>(Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;)V

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 505
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 506
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v3, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$4;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$4;-><init>(Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;)V

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 515
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v3, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$5;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$5;-><init>(Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;)V

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 527
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v3, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$6;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$6;-><init>(Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;)V

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 557
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mFilePath:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 559
    const/4 v2, 0x2

    const-string v3, "VideoHoverPopup"

    const-string v4, "playVideo() :: mPath is null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 560
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->getHandler()Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 584
    :catch_0
    move-exception v1

    .line 586
    .local v1, "ex":Ljava/io/IOException;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->showNoVideoView()V

    .line 587
    const-string v2, "VideoHoverPopup"

    const-string v3, "IOException - Unable to open content"

    invoke-static {v6, v2, v3, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 588
    const-string v2, "VideoHoverPopup"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "=> Uri = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 564
    .end local v1    # "ex":Ljava/io/IOException;
    :cond_4
    const/4 v2, 0x0

    :try_start_1
    const-string v3, "VideoHoverPopup"

    const-string v4, "playVideo"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 567
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mFilePath:Ljava/lang/String;

    const-string v3, "content://"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 568
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mFilePath:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 572
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v2, :cond_5

    .line 573
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 575
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v3, "audio"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 576
    .local v0, "audioManager":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v2

    if-ne v2, v5, :cond_7

    .line 577
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 581
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    .line 582
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_0

    .line 590
    .end local v0    # "audioManager":Landroid/media/AudioManager;
    :catch_1
    move-exception v1

    .line 592
    .local v1, "ex":Ljava/lang/IllegalStateException;
    const-string v2, "VideoHoverPopup"

    const-string v3, "IllegalStateException - Unable to open content"

    invoke-static {v6, v2, v3, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 593
    const-string v2, "VideoHoverPopup"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "=> Uri = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 570
    .end local v1    # "ex":Ljava/lang/IllegalStateException;
    :cond_6
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mFilePath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 595
    :catch_2
    move-exception v1

    .line 597
    .local v1, "ex":Ljava/lang/IllegalArgumentException;
    const-string v2, "VideoHoverPopup"

    const-string v3, "IllegalArgumentException - Unable to open content"

    invoke-static {v6, v2, v3, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 598
    const-string v2, "VideoHoverPopup"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "=> Uri = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 579
    .end local v1    # "ex":Ljava/lang/IllegalArgumentException;
    .restart local v0    # "audioManager":Landroid/media/AudioManager;
    :cond_7
    :try_start_3
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2
.end method

.method private setDialogWindow()V
    .locals 8

    .prologue
    .line 438
    iget-object v5, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v6, "window"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    .line 440
    .local v4, "wm":Landroid/view/WindowManager;
    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 442
    .local v0, "display":Landroid/view/Display;
    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v1}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 444
    .local v1, "lp":Landroid/view/WindowManager$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/view/WindowManager$LayoutParams;->copyFrom(Landroid/view/WindowManager$LayoutParams;)I

    .line 446
    iget-object v5, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->getStatusBarHeight(Landroid/content/Context;)I

    move-result v2

    .line 448
    .local v2, "statusBarHeight":I
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v5

    iget v6, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoHeight:I

    iget v7, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mTargetHeightWeight:I

    add-int/2addr v6, v7

    if-ge v5, v6, :cond_0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v5

    sub-int/2addr v5, v2

    iput v5, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mTotalVideoHeight:I

    :goto_0
    iput v5, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mTotalVideoHeight:I

    .line 454
    iget-object v5, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    .line 456
    .local v3, "window":Landroid/view/Window;
    invoke-virtual {v3, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 457
    return-void

    .line 448
    .end local v3    # "window":Landroid/view/Window;
    :cond_0
    iget v5, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoHeight:I

    iget v6, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mTargetHeightWeight:I

    add-int/2addr v5, v6

    goto :goto_0
.end method

.method private setPreviewZoomSize(I)Z
    .locals 6
    .param p1, "Rotation"    # I

    .prologue
    const/4 v2, 0x1

    .line 299
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mFilePath:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 300
    const/4 v1, 0x2

    const-string v2, "VideoHoverPopup"

    const-string v3, "setPreviewZoomSize == null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 301
    const/4 v1, 0x0

    .line 313
    :goto_0
    return v1

    .line 304
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mFilePath:Ljava/lang/String;

    const-wide/16 v4, 0x0

    invoke-direct {p0, v1, v4, v5}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->getBitmapData(Ljava/lang/String;J)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 306
    .local v0, "geBbitmap":Landroid/graphics/Bitmap;
    if-ne p1, v2, :cond_2

    const/16 v1, 0x21c

    :goto_1
    iput v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mPreviewWidth:I

    .line 307
    if-nez v0, :cond_1

    .line 309
    const/16 v1, 0xc8

    iput v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mPreviewWidth:I

    :cond_1
    move v1, v2

    .line 313
    goto :goto_0

    .line 306
    :cond_2
    const/16 v1, 0x2ee

    goto :goto_1
.end method

.method private setVideoView()V
    .locals 4

    .prologue
    .line 461
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->surfaceView:Landroid/view/SurfaceView;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mMeasuredVideoWidth:I

    iget v3, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mTargetWidthWeight:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mTotalVideoHeight:I

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 463
    return-void
.end method


# virtual methods
.method protected changeList()V
    .locals 0

    .prologue
    .line 808
    return-void
.end method

.method protected changeListThreadStart()V
    .locals 0

    .prologue
    .line 814
    return-void
.end method

.method protected dissmissPopup()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 774
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->isShow()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 775
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->abandonAudioFocus()V

    .line 777
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 778
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 783
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 784
    const/4 v1, 0x0

    const-string v2, "VideoHoverPopup"

    const-string v3, "videoPreviewDialog.dismiss()"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 792
    :goto_0
    iput-object v4, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    .line 795
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_2

    .line 796
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 798
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v1, :cond_3

    .line 799
    iput-object v4, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    .line 802
    :cond_3
    return-void

    .line 786
    :catch_0
    move-exception v0

    .line 788
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method protected initViews()Z
    .locals 3

    .prologue
    .line 630
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mFilePath:Ljava/lang/String;

    sget-object v1, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->EXTERNAL_MEDIA_VIDEO_DB_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->INTERNAL_MEDIA_VIDEO_DB_URI:Landroid/net/Uri;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->getUriByPath(Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mUri:Landroid/net/Uri;

    .line 632
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->checkIsCalling(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 633
    const/4 v0, 0x0

    .line 764
    :goto_0
    return v0

    .line 635
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->getVideoViewSize()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mFromDB:Z

    .line 639
    new-instance v0, Landroid/app/Dialog;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    .line 641
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v1, 0x7f04004f

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->layout:Landroid/widget/RelativeLayout;

    .line 643
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->layout:Landroid/widget/RelativeLayout;

    const v1, 0x7f0f00bf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mHoverOperationContainer:Landroid/view/View;

    .line 647
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mHoverOperationContainer:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 650
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->layout:Landroid/widget/RelativeLayout;

    const v1, 0x7f0f011a

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->noVideoView:Landroid/widget/ImageView;

    .line 652
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->layout:Landroid/widget/RelativeLayout;

    const v1, 0x7f0f0119

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->surfaceView:Landroid/view/SurfaceView;

    .line 653
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    .line 654
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 656
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->setDialogProperties()V

    .line 657
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->setDialogPosition()V

    .line 659
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 661
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->setDialogWindow()V

    .line 662
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->setVideoView()V

    .line 664
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->initHoverOperationView(Landroid/view/View;)V

    .line 666
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->layout:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$7;-><init>(Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 682
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->layout:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$8;-><init>(Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 764
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method protected setDialogPosition()V
    .locals 4

    .prologue
    .line 616
    const/4 v1, 0x0

    const-string v2, "VideoHoverPopup"

    const-string v3, "setPosition"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 618
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 621
    .local v0, "params":Landroid/view/WindowManager$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mHoverRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v1, v1, -0x32

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 622
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mHoverRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    add-int/lit8 v1, v1, -0x64

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 623
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 624
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x33

    invoke-virtual {v1, v2}, Landroid/view/Window;->setGravity(I)V

    .line 625
    return-void
.end method

.method protected showContent()V
    .locals 0

    .prologue
    .line 769
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->playVideo()V

    .line 770
    return-void
.end method

.method public showNoVideoView()V
    .locals 2

    .prologue
    .line 603
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->surfaceView:Landroid/view/SurfaceView;

    if-eqz v0, :cond_0

    .line 604
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->surfaceView:Landroid/view/SurfaceView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setVisibility(I)V

    .line 606
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->noVideoView:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 607
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->noVideoView:Landroid/widget/ImageView;

    const v1, 0x7f0200bd

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 608
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->noVideoView:Landroid/widget/ImageView;

    const-string v1, "#252628"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 609
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->noVideoView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 612
    :cond_1
    return-void
.end method
