.class public Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;
.super Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;
.source "CopyOrMoveOperationTask.java"


# static fields
.field private static final COPY_BUFFER_SIZE:I = 0x2000

.field private static final MODULE:Ljava/lang/String; = "CopyOrMoveOperationTask"


# instance fields
.field private isCopyMoveFile:Z

.field private isCopyMoveFolder:Z

.field private mContext:Landroid/content/Context;

.field private mDoneCount:I

.field private mErrorCode:I

.field private mMoveInSameStorage:Z

.field private mTotalCount:I

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILandroid/view/View;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "operation"    # I
    .param p3, "contentType"    # I
    .param p4, "progressView"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 68
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;-><init>(Landroid/content/Context;IILandroid/view/View;)V

    .line 51
    iput v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 53
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mMoveInSameStorage:Z

    .line 55
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 57
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCopyMoveFile:Z

    .line 59
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCopyMoveFolder:Z

    .line 61
    iput v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mTotalCount:I

    .line 63
    iput v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mDoneCount:I

    .line 70
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mContext:Landroid/content/Context;

    .line 71
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mContext:Landroid/content/Context;

    const-string v3, "power"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 73
    .local v1, "pm":Landroid/os/PowerManager;
    const/4 v0, 0x1

    .line 75
    .local v0, "flag":I
    const-string v2, "CopyOrMoveOperationTask"

    invoke-virtual {v1, v0, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 77
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 78
    return-void
.end method

.method private copyMoveFiles(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 18
    .param p2, "dstFolder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 350
    .local p1, "srcFilesPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mOperationType:I

    if-nez v13, :cond_1

    .line 352
    const/4 v13, 0x0

    const-string v14, "CopyOrMoveOperationTask"

    const-string v15, "copy start"

    invoke-static {v13, v14, v15}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 359
    :goto_0
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-lez v13, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v13

    if-nez v13, :cond_2

    .line 541
    :cond_0
    :goto_1
    return-void

    .line 356
    :cond_1
    const/4 v13, 0x0

    const-string v14, "CopyOrMoveOperationTask"

    const-string v15, "move start"

    invoke-static {v13, v14, v15}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 370
    :cond_2
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mOperationType:I

    const/4 v14, 0x1

    if-ne v13, v14, :cond_3

    .line 372
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-static {v13, v0}, Lcom/sec/android/app/myfiles/utils/Utils;->checkSameStorage(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mMoveInSameStorage:Z

    .line 377
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mMoveInSameStorage:Z

    if-nez v13, :cond_4

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v13, v0}, Lcom/sec/android/app/myfiles/utils/Utils;->getPathFilesSize(Landroid/content/Context;Ljava/util/ArrayList;)J

    move-result-wide v14

    invoke-static/range {p2 .. p2}, Lcom/sec/android/app/myfiles/utils/Utils;->getAvailableSpace(Ljava/lang/String;)J

    move-result-wide v16

    cmp-long v13, v14, v16

    if-ltz v13, :cond_4

    .line 379
    const/4 v13, 0x0

    const-string v14, "CopyOrMoveOperationTask"

    const-string v15, "There is no available space"

    invoke-static {v13, v14, v15}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 381
    const/4 v13, 0x6

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    goto :goto_1

    .line 387
    :cond_4
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 388
    .local v6, "filename":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mTargetFolder:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v6, v14}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v14

    add-int/lit8 v14, v14, 0x1

    invoke-virtual {v6, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v5, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 389
    .local v5, "file":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_5

    .line 390
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mOverwriteFileCount:I

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mOverwriteFileCount:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 518
    .end local v5    # "file":Ljava/io/File;
    .end local v6    # "filename":Ljava/lang/String;
    .end local v7    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v3

    .line 520
    .local v3, "e":Ljava/lang/Exception;
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 522
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 525
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_6
    const-string v13, ""

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mPath:Ljava/lang/String;

    .line 527
    const-string v13, ""

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mRename:Ljava/lang/String;

    .line 529
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mOverwrite:Z

    .line 531
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mRunRename:Z

    .line 533
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mOperationType:I

    if-nez v13, :cond_13

    .line 535
    const/4 v13, 0x0

    const-string v14, "CopyOrMoveOperationTask"

    const-string v15, "copy finish"

    invoke-static {v13, v14, v15}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 393
    .restart local v7    # "i$":Ljava/util/Iterator;
    :cond_7
    :try_start_1
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mOverwriteFileCount:I

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mTempOverwriteFileCount:I

    .line 396
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_8
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 398
    .local v4, "f":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCancelled()Z

    move-result v13

    if-nez v13, :cond_0

    .line 403
    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 405
    .local v11, "srcFile":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    .line 407
    .local v12, "srcFileName":Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 409
    .local v10, "newFileName":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 411
    .local v2, "dstFolderParent":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mFileNameHandler:Landroid/os/Handler;

    const/4 v14, 0x0

    invoke-static {v13, v14, v12}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v8

    .line 412
    .local v8, "msg":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mFileNameHandler:Landroid/os/Handler;

    invoke-virtual {v13, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 415
    invoke-virtual {v11}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mSameFolder:Z

    .line 417
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mOperationType:I

    const/4 v14, 0x1

    if-ne v13, v14, :cond_9

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mSameFolder:Z

    if-eqz v13, :cond_9

    .line 419
    const/4 v13, 0x0

    const-string v14, "CopyOrMoveOperationTask"

    const-string v15, "move is Same Folder"

    invoke-static {v13, v14, v15}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 421
    const/4 v13, 0x4

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    goto/16 :goto_1

    .line 427
    :cond_9
    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 429
    const/4 v13, 0x0

    const-string v14, "CopyOrMoveOperationTask"

    const-string v15, "dest folder is sub Folder"

    invoke-static {v13, v14, v15}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 431
    const/4 v13, 0x5

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    goto/16 :goto_1

    .line 436
    :cond_a
    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_b

    .line 438
    const/4 v13, 0x0

    const-string v14, "CopyOrMoveOperationTask"

    const-string v15, "dest folder is same as src folder"

    invoke-static {v13, v14, v15}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 440
    const/4 v13, 0x4

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    goto/16 :goto_1

    .line 445
    :cond_b
    invoke-virtual {v11}, Ljava/io/File;->isFile()Z

    move-result v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v13}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->checkDirOrFileName(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v9

    .line 447
    .local v9, "newFile":Ljava/io/File;
    if-nez v9, :cond_c

    .line 448
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isAllOverwriteCheck:Z

    if-eqz v13, :cond_8

    goto/16 :goto_1

    .line 455
    :cond_c
    invoke-virtual {v11}, Ljava/io/File;->isFile()Z

    move-result v13

    if-eqz v13, :cond_d

    .line 457
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCopyMoveFile:Z

    .line 463
    :goto_4
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mOperationType:I

    if-nez v13, :cond_10

    .line 465
    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v9}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->copy(Ljava/io/File;Ljava/io/File;)Z

    move-result v13

    if-eqz v13, :cond_e

    .line 467
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mRemainOverwriteFileCount:I

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mRemainOverwriteFileCount:I

    goto/16 :goto_3

    .line 460
    :cond_d
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCopyMoveFolder:Z

    goto :goto_4

    .line 471
    :cond_e
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    const/16 v14, 0x8

    if-ne v13, v14, :cond_8

    .line 473
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mRemainOverwriteFileCount:I

    if-nez v13, :cond_f

    .line 474
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 476
    :cond_f
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mRunStop:Z

    if-nez v13, :cond_0

    .line 482
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mRemainOverwriteFileCount:I

    if-lez v13, :cond_8

    .line 484
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    goto/16 :goto_3

    .line 492
    :cond_10
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mMoveInSameStorage:Z

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v9, v13}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->move(Ljava/io/File;Ljava/io/File;Z)Z

    move-result v13

    if-eqz v13, :cond_11

    .line 494
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mRemainOverwriteFileCount:I

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mRemainOverwriteFileCount:I

    goto/16 :goto_3

    .line 498
    :cond_11
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    const/16 v14, 0x8

    if-ne v13, v14, :cond_8

    .line 500
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mRemainOverwriteFileCount:I

    if-nez v13, :cond_12

    .line 501
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 503
    :cond_12
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mRunStop:Z

    if-nez v13, :cond_0

    .line 509
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mRemainOverwriteFileCount:I

    if-lez v13, :cond_8

    .line 510
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_3

    .line 539
    .end local v2    # "dstFolderParent":Ljava/io/File;
    .end local v4    # "f":Ljava/lang/String;
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "msg":Landroid/os/Message;
    .end local v9    # "newFile":Ljava/io/File;
    .end local v10    # "newFileName":Ljava/lang/String;
    .end local v11    # "srcFile":Ljava/io/File;
    .end local v12    # "srcFileName":Ljava/lang/String;
    :cond_13
    const/4 v13, 0x0

    const-string v14, "CopyOrMoveOperationTask"

    const-string v15, "move finish"

    invoke-static {v13, v14, v15}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method private getTotalItemCount(Ljava/lang/String;)I
    .locals 9
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1005
    const/4 v6, 0x0

    .line 1007
    .local v6, "totalCount":I
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    move v7, v6

    .line 1042
    .end local v6    # "totalCount":I
    .local v7, "totalCount":I
    :goto_0
    return v7

    .line 1012
    .end local v7    # "totalCount":I
    .restart local v6    # "totalCount":I
    :cond_0
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1014
    .local v5, "target":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_1

    move v7, v6

    .line 1016
    .end local v6    # "totalCount":I
    .restart local v7    # "totalCount":I
    goto :goto_0

    .line 1020
    .end local v7    # "totalCount":I
    .restart local v6    # "totalCount":I
    :cond_1
    add-int/lit8 v6, v6, 0x1

    .line 1022
    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1024
    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 1026
    .local v2, "children":[Ljava/io/File;
    if-eqz v2, :cond_3

    .line 1028
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_3

    aget-object v1, v0, v3

    .line 1030
    .local v1, "child":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1032
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->getTotalItemCount(Ljava/lang/String;)I

    move-result v8

    add-int/2addr v6, v8

    .line 1028
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1036
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "child":Ljava/io/File;
    .end local v2    # "children":[Ljava/io/File;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_3
    move v7, v6

    .line 1042
    .end local v6    # "totalCount":I
    .restart local v7    # "totalCount":I
    goto :goto_0
.end method

.method private getTotalItemCount(Ljava/util/ArrayList;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 987
    .local p1, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gtz v3, :cond_2

    .line 989
    :cond_0
    const/4 v2, 0x0

    .line 999
    :cond_1
    return v2

    .line 992
    :cond_2
    const/4 v2, 0x0

    .line 994
    .local v2, "totalCount":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 996
    .local v1, "path":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->getTotalItemCount(Ljava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 997
    goto :goto_0
.end method

.method private updateProgress(IZZ)V
    .locals 3
    .param p1, "percent"    # I
    .param p2, "updateDoneCount"    # Z
    .param p3, "increaseDone"    # Z

    .prologue
    const/4 v2, 0x1

    .line 1049
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iput p1, v0, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mCurrentTargetPercentage:I

    .line 1051
    if-eqz p3, :cond_0

    .line 1053
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v1, v0, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    .line 1056
    :cond_0
    if-eqz p2, :cond_1

    .line 1058
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v0, v0, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v1, v1, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mTotalTarget:I

    if-gt v0, v1, :cond_1

    .line 1060
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iput-boolean v2, v0, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTargetUpdated:Z

    .line 1064
    :cond_1
    new-array v0, v2, [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->publishProgress([Ljava/lang/Object;)V

    .line 1065
    return-void
.end method


# virtual methods
.method protected copy(Ljava/io/File;Ljava/io/File;)Z
    .locals 26
    .param p1, "src"    # Ljava/io/File;
    .param p2, "dst"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 546
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCancelled()Z

    move-result v20

    if-eqz v20, :cond_1

    .line 548
    const/16 v20, 0x0

    const-string v21, "CopyOrMoveOperationTask"

    const-string v22, "Operation was cancelled"

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 550
    const/16 v20, 0x0

    .line 793
    :cond_0
    :goto_0
    return v20

    .line 553
    :cond_1
    if-eqz p1, :cond_2

    if-nez p2, :cond_3

    .line 555
    :cond_2
    const/16 v20, 0x0

    const-string v21, "CopyOrMoveOperationTask"

    const-string v22, "src or dst is null"

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 557
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 559
    const/16 v20, 0x0

    goto :goto_0

    .line 562
    :cond_3
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/myfiles/utils/Utils;->getStorage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    if-nez v20, :cond_4

    .line 564
    const/16 v20, 0x0

    const-string v21, "CopyOrMoveOperationTask"

    const-string v22, "dst file storage is null"

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 566
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 568
    const/16 v20, 0x0

    goto :goto_0

    .line 571
    :cond_4
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/myfiles/utils/Utils;->hasAvailableSpace(Ljava/lang/String;)Z

    move-result v20

    if-nez v20, :cond_5

    .line 573
    const/16 v20, 0x0

    const-string v21, "CopyOrMoveOperationTask"

    const-string v22, "There is no available space"

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 575
    const/16 v20, 0x6

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 577
    const/16 v20, 0x0

    goto :goto_0

    .line 580
    :cond_5
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxFolder(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_6

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxFolder(Ljava/lang/String;)Z

    move-result v20

    if-nez v20, :cond_6

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    const/16 v21, 0xb0

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_6

    .line 582
    const/16 v20, 0x0

    const-string v21, "CopyOrMoveOperationTask"

    const-string v22, "The Path of file is too long in move to private"

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 584
    const/16 v20, 0x8

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 586
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->showStopDialog(Ljava/io/File;)V

    .line 588
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 591
    :cond_6
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    const/16 v21, 0xff

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_7

    .line 593
    const/16 v20, 0x0

    const-string v21, "CopyOrMoveOperationTask"

    const-string v22, "The Path of file is too long"

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 595
    const/16 v20, 0x8

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 597
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->showStopDialog(Ljava/io/File;)V

    .line 599
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 602
    :cond_7
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 604
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mTargetName:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 606
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->isFile()Z

    move-result v20

    if-eqz v20, :cond_21

    .line 608
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v20

    if-eqz v20, :cond_8

    .line 610
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mOverwrite:Z

    move/from16 v20, v0

    if-eqz v20, :cond_c

    .line 612
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->deleteInFileSystem(Ljava/io/File;)Z

    .line 624
    :cond_8
    new-instance v9, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v9, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 626
    .local v9, "from":Ljava/io/FileInputStream;
    new-instance v19, Ljava/io/FileOutputStream;

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 630
    .local v19, "to":Ljava/io/FileOutputStream;
    const/16 v20, 0x2000

    :try_start_0
    move/from16 v0, v20

    new-array v5, v0, [B

    .line 634
    .local v5, "buffer":[B
    const-wide/16 v14, 0x0

    .line 636
    .local v14, "readLength":J
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v12

    .line 638
    .local v12, "length":J
    const/16 v20, 0x0

    const/16 v21, 0x1

    const/16 v22, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->updateProgress(IZZ)V

    .line 640
    :goto_1
    const/16 v20, 0x0

    const/16 v21, 0x2000

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v9, v5, v0, v1}, Ljava/io/FileInputStream;->read([BII)I

    move-result v6

    .local v6, "bytesRead":I
    const/16 v20, -0x1

    move/from16 v0, v20

    if-eq v6, v0, :cond_10

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCancelled()Z

    move-result v20

    if-nez v20, :cond_10

    .line 642
    const/16 v20, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v0, v5, v1, v6}, Ljava/io/FileOutputStream;->write([BII)V

    .line 644
    int-to-long v0, v6

    move-wide/from16 v20, v0

    add-long v14, v14, v20

    .line 646
    cmp-long v20, v14, v12

    if-nez v20, :cond_d

    .line 648
    const/16 v20, 0x64

    const/16 v21, 0x1

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->updateProgress(IZZ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_1

    .line 661
    .end local v5    # "buffer":[B
    .end local v6    # "bytesRead":I
    .end local v12    # "length":J
    .end local v14    # "readLength":J
    :catch_0
    move-exception v8

    .line 663
    .local v8, "e":Ljava/io/IOException;
    :try_start_1
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v20

    if-eqz v20, :cond_9

    .line 665
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->deleteInFileSystem(Ljava/io/File;)Z

    .line 668
    :cond_9
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    .line 670
    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v20

    const-string v21, "ENOSPC"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_16

    .line 672
    const/16 v20, 0x6

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 679
    :goto_2
    const/16 v20, 0x0

    .line 691
    if-eqz v9, :cond_a

    .line 693
    :try_start_2
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 699
    :cond_a
    if-eqz v19, :cond_b

    .line 701
    :try_start_3
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    .line 711
    :cond_b
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCancelled()Z

    move-result v21

    if-eqz v21, :cond_0

    .line 713
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v21

    if-eqz v21, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v22

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->length()J

    move-result-wide v24

    cmp-long v21, v22, v24

    if-eqz v21, :cond_0

    .line 715
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z

    .line 717
    const/16 v20, 0x0

    const-string v21, "CopyOrMoveOperationTask"

    const-string v22, "copy failed - file is deleted."

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 719
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 616
    .end local v8    # "e":Ljava/io/IOException;
    .end local v9    # "from":Ljava/io/FileInputStream;
    .end local v19    # "to":Ljava/io/FileOutputStream;
    :cond_c
    const/16 v20, 0x2

    const-string v21, "CopyOrMoveOperationTask"

    const-string v22, "copy mOverwrite false :: Copy Failed"

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 618
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 620
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 652
    .restart local v5    # "buffer":[B
    .restart local v6    # "bytesRead":I
    .restart local v9    # "from":Ljava/io/FileInputStream;
    .restart local v12    # "length":J
    .restart local v14    # "readLength":J
    .restart local v19    # "to":Ljava/io/FileOutputStream;
    :cond_d
    const-wide/16 v20, 0x64

    mul-long v20, v20, v14

    :try_start_4
    div-long v20, v20, v12

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->updateProgress(IZZ)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_1

    .line 681
    .end local v5    # "buffer":[B
    .end local v6    # "bytesRead":I
    .end local v12    # "length":J
    .end local v14    # "readLength":J
    :catch_1
    move-exception v8

    .line 683
    .local v8, "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    .line 685
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 691
    if-eqz v9, :cond_e

    .line 693
    :try_start_6
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 699
    :cond_e
    if-eqz v19, :cond_f

    .line 701
    :try_start_7
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    .line 711
    .end local v8    # "e":Ljava/lang/Exception;
    :cond_f
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCancelled()Z

    move-result v20

    if-eqz v20, :cond_20

    .line 713
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v20

    if-eqz v20, :cond_20

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v20

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->length()J

    move-result-wide v22

    cmp-long v20, v20, v22

    if-eqz v20, :cond_20

    .line 715
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z

    .line 717
    const/16 v20, 0x0

    const-string v21, "CopyOrMoveOperationTask"

    const-string v22, "copy failed - file is deleted."

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 719
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 656
    .restart local v5    # "buffer":[B
    .restart local v6    # "bytesRead":I
    .restart local v12    # "length":J
    .restart local v14    # "readLength":J
    :cond_10
    :try_start_8
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCancelled()Z

    move-result v20

    if-nez v20, :cond_11

    .line 658
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->flush()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 691
    :cond_11
    if-eqz v9, :cond_12

    .line 693
    :try_start_9
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 699
    :cond_12
    if-eqz v19, :cond_13

    .line 701
    :try_start_a
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    .line 711
    :cond_13
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCancelled()Z

    move-result v20

    if-eqz v20, :cond_20

    .line 713
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v20

    if-eqz v20, :cond_20

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v20

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->length()J

    move-result-wide v22

    cmp-long v20, v20, v22

    if-eqz v20, :cond_20

    .line 715
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z

    .line 717
    const/16 v20, 0x0

    const-string v21, "CopyOrMoveOperationTask"

    const-string v22, "copy failed - file is deleted."

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 719
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 704
    :catch_2
    move-exception v8

    .line 706
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    .line 708
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    goto :goto_5

    .line 697
    .end local v8    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v20

    .line 699
    if-eqz v19, :cond_14

    .line 701
    :try_start_b
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_3

    .line 711
    :cond_14
    :goto_6
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCancelled()Z

    move-result v21

    if-eqz v21, :cond_15

    .line 713
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v21

    if-eqz v21, :cond_15

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v22

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->length()J

    move-result-wide v24

    cmp-long v21, v22, v24

    if-eqz v21, :cond_15

    .line 715
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z

    .line 717
    const/16 v20, 0x0

    const-string v21, "CopyOrMoveOperationTask"

    const-string v22, "copy failed - file is deleted."

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 719
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 704
    :catch_3
    move-exception v8

    .line 706
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    .line 708
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    goto :goto_6

    .line 719
    .end local v8    # "e":Ljava/io/IOException;
    :cond_15
    throw v20

    .line 676
    .end local v5    # "buffer":[B
    .end local v6    # "bytesRead":I
    .end local v12    # "length":J
    .end local v14    # "readLength":J
    .restart local v8    # "e":Ljava/io/IOException;
    :cond_16
    const/16 v20, 0x1

    :try_start_c
    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto/16 :goto_2

    .line 689
    .end local v8    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v20

    .line 691
    if-eqz v9, :cond_17

    .line 693
    :try_start_d
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    .line 699
    :cond_17
    if-eqz v19, :cond_18

    .line 701
    :try_start_e
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_8

    .line 711
    :cond_18
    :goto_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCancelled()Z

    move-result v21

    if-eqz v21, :cond_1f

    .line 713
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v21

    if-eqz v21, :cond_1f

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v22

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->length()J

    move-result-wide v24

    cmp-long v21, v22, v24

    if-eqz v21, :cond_1f

    .line 715
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z

    .line 717
    const/16 v20, 0x0

    const-string v21, "CopyOrMoveOperationTask"

    const-string v22, "copy failed - file is deleted."

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 719
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 704
    .restart local v8    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v8

    .line 706
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    .line 708
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    goto/16 :goto_3

    .line 697
    :catchall_2
    move-exception v20

    .line 699
    if-eqz v19, :cond_19

    .line 701
    :try_start_f
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_5

    .line 711
    :cond_19
    :goto_8
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCancelled()Z

    move-result v21

    if-eqz v21, :cond_1a

    .line 713
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v21

    if-eqz v21, :cond_1a

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v22

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->length()J

    move-result-wide v24

    cmp-long v21, v22, v24

    if-eqz v21, :cond_1a

    .line 715
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z

    .line 717
    const/16 v20, 0x0

    const-string v21, "CopyOrMoveOperationTask"

    const-string v22, "copy failed - file is deleted."

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 719
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 704
    :catch_5
    move-exception v8

    .line 706
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    .line 708
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    goto :goto_8

    .line 719
    :cond_1a
    throw v20

    .line 704
    .local v8, "e":Ljava/lang/Exception;
    :catch_6
    move-exception v8

    .line 706
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    .line 708
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    goto/16 :goto_4

    .line 697
    .local v8, "e":Ljava/lang/Exception;
    :catchall_3
    move-exception v20

    .line 699
    if-eqz v19, :cond_1b

    .line 701
    :try_start_10
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_7

    .line 711
    .end local v8    # "e":Ljava/lang/Exception;
    :cond_1b
    :goto_9
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCancelled()Z

    move-result v21

    if-eqz v21, :cond_1c

    .line 713
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v21

    if-eqz v21, :cond_1c

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v22

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->length()J

    move-result-wide v24

    cmp-long v21, v22, v24

    if-eqz v21, :cond_1c

    .line 715
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z

    .line 717
    const/16 v20, 0x0

    const-string v21, "CopyOrMoveOperationTask"

    const-string v22, "copy failed - file is deleted."

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 719
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 704
    .restart local v8    # "e":Ljava/lang/Exception;
    :catch_7
    move-exception v8

    .line 706
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    .line 708
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    goto :goto_9

    .line 719
    .end local v8    # "e":Ljava/io/IOException;
    :cond_1c
    throw v20

    .line 704
    :catch_8
    move-exception v8

    .line 706
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    .line 708
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    goto/16 :goto_7

    .line 697
    .end local v8    # "e":Ljava/io/IOException;
    :catchall_4
    move-exception v20

    .line 699
    if-eqz v19, :cond_1d

    .line 701
    :try_start_11
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_9

    .line 711
    :cond_1d
    :goto_a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCancelled()Z

    move-result v21

    if-eqz v21, :cond_1e

    .line 713
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v21

    if-eqz v21, :cond_1e

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v22

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->length()J

    move-result-wide v24

    cmp-long v21, v22, v24

    if-eqz v21, :cond_1e

    .line 715
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z

    .line 717
    const/16 v20, 0x0

    const-string v21, "CopyOrMoveOperationTask"

    const-string v22, "copy failed - file is deleted."

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 719
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 704
    :catch_9
    move-exception v8

    .line 706
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    .line 708
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    goto :goto_a

    .line 719
    .end local v8    # "e":Ljava/io/IOException;
    :cond_1e
    throw v20

    :cond_1f
    throw v20

    .line 725
    :cond_20
    const/16 v20, 0x1

    goto/16 :goto_0

    .line 727
    .end local v9    # "from":Ljava/io/FileInputStream;
    .end local v19    # "to":Ljava/io/FileOutputStream;
    :cond_21
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->isDirectory()Z

    move-result v20

    if-eqz v20, :cond_27

    .line 729
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mOverwrite:Z

    move/from16 v20, v0

    if-nez v20, :cond_22

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v20

    if-eqz v20, :cond_22

    .line 731
    const/16 v20, 0x0

    const-string v21, "CopyOrMoveOperationTask"

    const-string v22, "copy() : Destination folder already exists"

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 732
    const/16 v20, 0x2

    const-string v21, "CopyOrMoveOperationTask"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "path = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 734
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 736
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 739
    :cond_22
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mOverwrite:Z

    move/from16 v20, v0

    if-nez v20, :cond_23

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->mkdir()Z

    move-result v20

    if-nez v20, :cond_23

    .line 741
    const/16 v20, 0x0

    const-string v21, "CopyOrMoveOperationTask"

    const-string v22, "copy() : Fail to make destination folder"

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 742
    const/16 v20, 0x2

    const-string v21, "CopyOrMoveOperationTask"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "path = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 744
    const/16 v20, 0x3

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 746
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 749
    :cond_23
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mOverwrite:Z

    move/from16 v20, v0

    if-eqz v20, :cond_24

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v20

    if-nez v20, :cond_24

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->mkdir()Z

    move-result v20

    if-nez v20, :cond_24

    .line 751
    const/16 v20, 0x0

    const-string v21, "CopyOrMoveOperationTask"

    const-string v22, "copy() : Fail to make destination folder"

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 752
    const/16 v20, 0x2

    const-string v21, "CopyOrMoveOperationTask"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "path = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 754
    const/16 v20, 0x3

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 756
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 759
    :cond_24
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->canWrite()Z

    move-result v7

    .line 761
    .local v7, "canWrite":Z
    if-nez v7, :cond_25

    .line 763
    const/16 v20, 0x0

    const-string v21, "CopyOrMoveOperationTask"

    const-string v22, "copy mOverwrite false :: Copy Failed"

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 765
    const/16 v20, 0x9

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 767
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 771
    :cond_25
    const/16 v20, 0x64

    const/16 v21, 0x1

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->updateProgress(IZZ)V

    .line 774
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v18

    .line 776
    .local v18, "subFiles":[Ljava/io/File;
    if-eqz v18, :cond_26

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v20, v0

    if-eqz v20, :cond_26

    .line 778
    move-object/from16 v4, v18

    .local v4, "arr$":[Ljava/io/File;
    array-length v11, v4

    .local v11, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_b
    if-ge v10, v11, :cond_26

    aget-object v16, v4, v10

    .line 780
    .local v16, "sf":Ljava/io/File;
    new-instance v17, Ljava/io/File;

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 782
    .local v17, "subDst":Ljava/io/File;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->copy(Ljava/io/File;Ljava/io/File;)Z

    .line 778
    add-int/lit8 v10, v10, 0x1

    goto :goto_b

    .line 786
    .end local v4    # "arr$":[Ljava/io/File;
    .end local v10    # "i$":I
    .end local v11    # "len$":I
    .end local v16    # "sf":Ljava/io/File;
    .end local v17    # "subDst":Ljava/io/File;
    :cond_26
    const/16 v20, 0x1

    goto/16 :goto_0

    .line 790
    .end local v7    # "canWrite":Z
    .end local v18    # "subFiles":[Ljava/io/File;
    :cond_27
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 793
    const/16 v20, 0x0

    goto/16 :goto_0
.end method

.method protected varargs doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;
    .locals 4
    .param p1, "params"    # [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .prologue
    const/4 v1, 0x0

    .line 110
    aget-object v0, p1, v1

    .line 112
    .local v0, "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    iget-object v2, v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mTargetFolder:Ljava/lang/String;

    .line 114
    iget-object v2, v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    .line 116
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    if-nez v3, :cond_0

    :goto_0
    iput v1, v2, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mTotalTarget:I

    .line 118
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v2, v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mStartIndex:I

    iput v2, v1, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    .line 120
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mTargetFolder:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->copyMoveFiles(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 122
    const/4 v1, 0x0

    return-object v1

    .line 116
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->getTotalItemCount(Ljava/util/ArrayList;)I

    move-result v1

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 44
    check-cast p1, [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;

    move-result-object v0

    return-object v0
.end method

.method public isError()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 981
    iget v0, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected move(Ljava/io/File;Ljava/io/File;Z)Z
    .locals 11
    .param p1, "src"    # Ljava/io/File;
    .param p2, "dst"    # Ljava/io/File;
    .param p3, "rename"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 799
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCancelled()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 801
    const/4 v7, 0x0

    const-string v8, "CopyOrMoveOperationTask"

    const-string v9, "Operation was cancelled"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 803
    const/4 v7, 0x0

    .line 975
    :goto_0
    return v7

    .line 806
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1

    if-eqz p2, :cond_1

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 808
    :cond_1
    const/4 v7, 0x0

    const-string v8, "CopyOrMoveOperationTask"

    const-string v9, "Conditions are wrong"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 810
    const/4 v7, 0x1

    iput v7, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 812
    const/4 v7, 0x0

    goto :goto_0

    .line 815
    :cond_2
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->getStorage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_3

    .line 817
    const/4 v7, 0x0

    const-string v8, "CopyOrMoveOperationTask"

    const-string v9, "dst file storage is null"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 819
    const/4 v7, 0x1

    iput v7, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 821
    const/4 v7, 0x0

    goto :goto_0

    .line 824
    :cond_3
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxFolder(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxFolder(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_4

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    const/16 v8, 0xb0

    if-le v7, v8, :cond_4

    .line 826
    const/4 v7, 0x0

    const-string v8, "CopyOrMoveOperationTask"

    const-string v9, "The Path of file is too long in move to private"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 828
    const/16 v7, 0x8

    iput v7, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 830
    invoke-virtual {p0, p2}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->showStopDialog(Ljava/io/File;)V

    .line 832
    const/4 v7, 0x0

    goto :goto_0

    .line 836
    :cond_4
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    const/16 v8, 0xff

    if-le v7, v8, :cond_5

    .line 838
    const/4 v7, 0x0

    const-string v8, "CopyOrMoveOperationTask"

    const-string v9, "Path of file is too long"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 840
    const/16 v7, 0x8

    iput v7, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 842
    invoke-virtual {p0, p2}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->showStopDialog(Ljava/io/File;)V

    .line 844
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 848
    :cond_5
    if-nez p3, :cond_6

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->hasAvailableSpace(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 850
    const/4 v7, 0x0

    const-string v8, "CopyOrMoveOperationTask"

    const-string v9, "There is no available space"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 852
    const/4 v7, 0x6

    iput v7, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 854
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 857
    :cond_6
    sget-boolean v7, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-eqz v7, :cond_8

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_7

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 860
    :cond_7
    const/4 p3, 0x0

    .line 862
    const/4 v7, 0x2

    const-string v8, "CopyOrMoveOperationTask"

    const-string v9, "File is moving to Personal Page volume"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 865
    :cond_8
    const/4 v7, 0x0

    iput v7, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 867
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mTargetName:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 869
    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_d

    .line 871
    if-eqz p3, :cond_9

    .line 873
    invoke-virtual {p1, p2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 875
    const/16 v7, 0x64

    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-direct {p0, v7, v8, v9}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->updateProgress(IZZ)V

    .line 877
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mDoneFileList:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 879
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 883
    :cond_9
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->copy(Ljava/io/File;Ljava/io/File;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 885
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->deleteInFileSystem(Ljava/io/File;)Z

    .line 886
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_c

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 887
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mContext:Landroid/content/Context;

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScanFile(Landroid/content/Context;Ljava/lang/String;)V

    .line 891
    :cond_a
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mDoneFileList:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 894
    :cond_b
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 888
    :cond_c
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_a

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_a

    .line 889
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->deleteInDatabase(Ljava/lang/String;)Z

    goto :goto_1

    .line 896
    :cond_d
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_14

    .line 898
    iget-boolean v7, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mOverwrite:Z

    if-nez v7, :cond_e

    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_e

    .line 900
    const/4 v7, 0x0

    const-string v8, "CopyOrMoveOperationTask"

    const-string v9, "move() : Destination folder already exists"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 901
    const/4 v7, 0x2

    const-string v8, "CopyOrMoveOperationTask"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "path = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 903
    const/4 v7, 0x1

    iput v7, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 905
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 908
    :cond_e
    if-eqz p3, :cond_f

    .line 910
    invoke-virtual {p1, p2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 913
    const/16 v7, 0x64

    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-direct {p0, v7, v8, v9}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->updateProgress(IZZ)V

    .line 915
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 919
    :cond_f
    iget-boolean v7, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mOverwrite:Z

    if-nez v7, :cond_10

    invoke-virtual {p2}, Ljava/io/File;->mkdir()Z

    move-result v7

    if-nez v7, :cond_10

    .line 921
    const/4 v7, 0x0

    const-string v8, "CopyOrMoveOperationTask"

    const-string v9, "move() : Fail to make destination folder"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 922
    const/4 v7, 0x2

    const-string v8, "CopyOrMoveOperationTask"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "path = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 924
    const/4 v7, 0x3

    iput v7, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 926
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 929
    :cond_10
    iget-boolean v7, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mOverwrite:Z

    if-eqz v7, :cond_11

    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_11

    invoke-virtual {p2}, Ljava/io/File;->mkdir()Z

    move-result v7

    if-nez v7, :cond_11

    .line 931
    const/4 v7, 0x0

    const-string v8, "CopyOrMoveOperationTask"

    const-string v9, "move() : Fail to make destination folder"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 932
    const/4 v7, 0x2

    const-string v8, "CopyOrMoveOperationTask"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "path = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 934
    const/4 v7, 0x3

    iput v7, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 936
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 939
    :cond_11
    invoke-virtual {p2}, Ljava/io/File;->canWrite()Z

    move-result v1

    .line 941
    .local v1, "canWrite":Z
    if-nez v1, :cond_12

    .line 943
    const/4 v7, 0x0

    const-string v8, "CopyOrMoveOperationTask"

    const-string v9, "move() :: canWrite false"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 945
    const/16 v7, 0x9

    iput v7, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 947
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 951
    :cond_12
    const/16 v7, 0x64

    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-direct {p0, v7, v8, v9}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->updateProgress(IZZ)V

    .line 954
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v6

    .line 956
    .local v6, "subFiles":[Ljava/io/File;
    if-eqz v6, :cond_13

    array-length v7, v6

    if-eqz v7, :cond_13

    .line 958
    move-object v0, v6

    .local v0, "arr$":[Ljava/io/File;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_2
    if-ge v2, v3, :cond_13

    aget-object v4, v0, v2

    .line 960
    .local v4, "sf":Ljava/io/File;
    new-instance v5, Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, p2, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 962
    .local v5, "subDst":Ljava/io/File;
    invoke-virtual {p0, v4, v5, p3}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->move(Ljava/io/File;Ljava/io/File;Z)Z

    .line 958
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 966
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "sf":Ljava/io/File;
    .end local v5    # "subDst":Ljava/io/File;
    :cond_13
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 968
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 972
    .end local v1    # "canWrite":Z
    .end local v6    # "subFiles":[Ljava/io/File;
    :cond_14
    const/4 v7, 0x1

    iput v7, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    .line 975
    const/4 v7, 0x0

    goto/16 :goto_0
.end method

.method protected onCancelled()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 310
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 312
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 315
    :cond_0
    iget v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    const/16 v3, 0x8

    if-ne v2, v3, :cond_1

    .line 317
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mContext:Landroid/content/Context;

    const v3, 0x7f0b0177

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mRemainOverwriteFileCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->showToast(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 344
    :goto_0
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->onCancelled()V

    .line 345
    return-void

    .line 321
    :cond_1
    :try_start_1
    iget v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mOperationType:I

    if-ne v2, v4, :cond_2

    .line 323
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mContext:Landroid/content/Context;

    const v3, 0x7f0b00cc

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mDoneCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mTotalCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 325
    .local v0, "cancelMessage":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->showToast(Ljava/lang/String;)V

    .line 327
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mDoneCount:I

    .line 329
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mTotalCount:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 339
    .end local v0    # "cancelMessage":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 341
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 334
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    const v2, 0x7f0b00c9

    :try_start_2
    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->showToast(I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0
.end method

.method protected onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V
    .locals 7
    .param p1, "result"    # Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .prologue
    const/4 v4, 0x1

    .line 180
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 182
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 185
    :cond_0
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mRenameCancelled:Z

    if-nez v3, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_1

    .line 187
    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    if-eqz v3, :cond_4

    .line 189
    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mErrorCode:I

    packed-switch v3, :pswitch_data_0

    .line 220
    :pswitch_0
    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mOperationType:I

    if-ne v3, v4, :cond_2

    .line 222
    const v3, 0x7f0b00cb

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->showToast(I)V

    .line 290
    :goto_0
    const/4 v3, 0x0

    iput v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mDoneCount:I

    .line 292
    const/4 v3, 0x0

    iput v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mTotalCount:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 300
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    .line 301
    return-void

    .line 193
    :pswitch_1
    const v3, 0x7f0b0067

    :try_start_1
    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->showToast(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 295
    :catch_0
    move-exception v0

    .line 297
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 200
    .end local v0    # "e":Ljava/lang/Exception;
    :pswitch_2
    const v3, 0x7f0b0069

    :try_start_2
    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->showToast(I)V

    goto :goto_0

    .line 207
    :pswitch_3
    const v3, 0x7f0b006a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->showToast(I)V

    goto :goto_0

    .line 213
    :pswitch_4
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0177

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mRemainOverwriteFileCount:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->showToast(Ljava/lang/String;)V

    goto :goto_0

    .line 225
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mContext:Landroid/content/Context;

    const-string v4, "enterprise_policy"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 226
    .local v1, "edm":Landroid/app/enterprise/EnterpriseDeviceManager;
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/enterprise/RestrictionPolicy;->isSDCardWriteAllowed()Z

    move-result v3

    if-nez v3, :cond_3

    .line 227
    const v3, 0x7f0b014e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->showToast(I)V

    goto :goto_0

    .line 229
    :cond_3
    const v3, 0x7f0b00c9

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->showToast(I)V

    goto :goto_0

    .line 238
    .end local v1    # "edm":Landroid/app/enterprise/EnterpriseDeviceManager;
    :cond_4
    iget v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mRemainOverwriteFileCount:I

    .line 239
    .local v2, "size":I
    if-eqz v2, :cond_a

    .line 240
    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mOperationType:I

    if-ne v3, v4, :cond_7

    .line 242
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCopyMoveFile:Z

    if-eqz v3, :cond_5

    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCopyMoveFolder:Z

    if-nez v3, :cond_5

    .line 244
    const v3, 0x7f0c0009

    invoke-virtual {p0, v3, v2}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->showToastPrurals(II)V

    .line 255
    :goto_2
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCopyMoveFile:Z

    .line 256
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCopyMoveFolder:Z

    .line 257
    const/4 v3, 0x0

    iput v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mRemainOverwriteFileCount:I

    goto/16 :goto_0

    .line 246
    :cond_5
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCopyMoveFile:Z

    if-nez v3, :cond_6

    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCopyMoveFolder:Z

    if-eqz v3, :cond_6

    .line 248
    const v3, 0x7f0c000a

    invoke-virtual {p0, v3, v2}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->showToastPrurals(II)V

    goto :goto_2

    .line 252
    :cond_6
    const v3, 0x7f0c0008

    invoke-virtual {p0, v3, v2}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->showToastPrurals(II)V

    goto :goto_2

    .line 261
    :cond_7
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCopyMoveFile:Z

    if-eqz v3, :cond_8

    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCopyMoveFolder:Z

    if-nez v3, :cond_8

    .line 263
    const v3, 0x7f0c0006

    invoke-virtual {p0, v3, v2}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->showToastPrurals(II)V

    .line 274
    :goto_3
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCopyMoveFile:Z

    .line 275
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCopyMoveFolder:Z

    .line 276
    const/4 v3, 0x0

    iput v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mRemainOverwriteFileCount:I

    goto/16 :goto_0

    .line 265
    :cond_8
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCopyMoveFile:Z

    if-nez v3, :cond_9

    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->isCopyMoveFolder:Z

    if-eqz v3, :cond_9

    .line 267
    const v3, 0x7f0c0007

    invoke-virtual {p0, v3, v2}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->showToastPrurals(II)V

    goto :goto_3

    .line 271
    :cond_9
    const v3, 0x7f0c0005

    invoke-virtual {p0, v3, v2}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->showToastPrurals(II)V

    goto :goto_3

    .line 279
    :cond_a
    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mOperationType:I

    if-ne v3, v4, :cond_b

    .line 281
    const v3, 0x7f0b00cb

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->showToast(I)V

    goto/16 :goto_0

    .line 285
    :cond_b
    const v3, 0x7f0b00c9

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->showToast(I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 189
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 44
    check-cast p1, Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 90
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->onPreExecute()V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mDoneFileList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mDoneFolderList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 96
    iget v0, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mOperationType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mProcessing:Landroid/widget/TextView;

    const v1, 0x7f0b00ca

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 104
    :goto_0
    return-void

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mProcessing:Landroid/widget/TextView;

    const v1, 0x7f0b00c8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method protected varargs onProgressUpdate([Lcom/sec/android/app/myfiles/element/FileOperationProgress;)V
    .locals 6
    .param p1, "values"    # [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    .prologue
    const/4 v5, 0x0

    .line 128
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 130
    aget-object v1, p1, v5

    .line 133
    .local v1, "progress":Lcom/sec/android/app/myfiles/element/FileOperationProgress;
    iget-boolean v2, v1, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTargetUpdated:Z

    if-eqz v2, :cond_1

    .line 135
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mCountText:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 138
    iget v2, v1, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mTotalTarget:I

    iput v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mTotalCount:I

    .line 140
    iget v2, v1, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    iput v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mDoneCount:I

    .line 142
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mCountText:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, v1, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mTotalTarget:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    :cond_0
    iput-boolean v5, v1, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTargetUpdated:Z

    .line 149
    :cond_1
    const/4 v0, 0x0

    .line 160
    .local v0, "percent":I
    iget v0, v1, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mCurrentTargetPercentage:I

    .line 168
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mProgressBar:Landroid/widget/ProgressBar;

    iget v3, v1, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mCurrentTargetPercentage:I

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 170
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mPercentText:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, v1, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mCurrentTargetPercentage:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " %"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 44
    check-cast p1, [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->onProgressUpdate([Lcom/sec/android/app/myfiles/element/FileOperationProgress;)V

    return-void
.end method

.method protected setupProgressView()V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 85
    return-void
.end method
