.class public Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;
.super Landroid/os/AsyncTask;
.source "EncryptionUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/utils/EncryptionUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "lockFileThread"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# static fields
.field private static final algorithm:Ljava/lang/String; = "AES"

.field private static final default_alg_mode:Ljava/lang/String; = "AES/ECB/PKCS5Padding"

.field static mContext:Landroid/content/Context;


# instance fields
.field private isCancel:Z

.field private mDeviceSeperator:[B

.field private mLockState:I

.field private mProgressDialog:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "lockState"    # I

    .prologue
    .line 136
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 123
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->isCancel:Z

    .line 134
    const/16 v0, 0xb

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mDeviceSeperator:[B

    .line 137
    sput-object p1, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mContext:Landroid/content/Context;

    .line 138
    iput p2, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mLockState:I

    .line 139
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;
    .param p1, "x1"    # Z

    .prologue
    .line 121
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->isCancel:Z

    return p1
.end method

.method public static getDeviceID()Ljava/lang/String;
    .locals 3

    .prologue
    .line 390
    sget-object v1, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mContext:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 393
    .local v0, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method protected addPostfix(Ljava/lang/String;I)Ljava/lang/String;
    .locals 12
    .param p1, "fullName"    # Ljava/lang/String;
    .param p2, "postfixNum"    # I

    .prologue
    const/16 v11, 0x2e

    const/16 v10, 0x29

    const/4 v9, 0x0

    const/4 v8, -0x1

    .line 398
    const/4 v5, 0x0

    .line 400
    .local v5, "resultStr":Ljava/lang/String;
    const-string v0, ""

    .line 401
    .local v0, "encExt":Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->isEncryptionFile(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 402
    invoke-virtual {p1, v11}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 403
    .local v2, "index":I
    if-eq v2, v8, :cond_0

    .line 404
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {p1, v2, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 405
    invoke-virtual {p1, v9, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 408
    .end local v2    # "index":I
    :cond_0
    invoke-virtual {p1, v11}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 410
    .local v3, "lastDot":I
    if-eq v3, v8, :cond_1

    .line 412
    invoke-virtual {p1, v9, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 414
    .local v4, "name":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {p1, v3, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 416
    .local v1, "ext":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 418
    .local v6, "sb":Ljava/lang/StringBuffer;
    const-string v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 420
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    .line 431
    .end local v1    # "ext":Ljava/lang/String;
    .end local v4    # "name":Ljava/lang/String;
    :goto_0
    return-object v5

    .line 424
    .end local v6    # "sb":Ljava/lang/StringBuffer;
    :cond_1
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 426
    .restart local v6    # "sb":Ljava/lang/StringBuffer;
    const-string v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 428
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Long;
    .locals 30
    .param p1, "path"    # [Ljava/lang/String;

    .prologue
    .line 236
    const/16 v25, 0x2

    aget-object v25, p1, v25

    const-string v26, "unlock"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_0

    .line 237
    const/16 v25, 0x1

    const/16 v26, 0x1

    aget-object v26, p1, v26

    const/16 v27, 0x0

    const/16 v28, 0x1

    aget-object v28, p1, v28

    const-string v29, "."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v28

    invoke-virtual/range {v26 .. v28}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v26

    aput-object v26, p1, v25

    .line 239
    :cond_0
    new-instance v24, Ljava/io/File;

    const/16 v25, 0x0

    aget-object v25, p1, v25

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 240
    .local v24, "srcFile":Ljava/io/File;
    new-instance v7, Ljava/io/File;

    const/16 v25, 0x1

    aget-object v25, p1, v25

    move-object/from16 v0, v25

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 242
    .local v7, "desFile":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v20

    .line 243
    .local v20, "lastDesName":Ljava/lang/String;
    const/16 v16, 0x0

    .line 244
    .local v16, "i":I
    :goto_0
    new-instance v25, Ljava/io/File;

    move-object/from16 v0, v25

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->exists()Z

    move-result v25

    if-eqz v25, :cond_1

    .line 245
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->addPostfix(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v20

    .line 246
    add-int/lit8 v16, v16, 0x1

    goto :goto_0

    .line 248
    :cond_1
    new-instance v7, Ljava/io/File;

    .end local v7    # "desFile":Ljava/io/File;
    move-object/from16 v0, v20

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 250
    .restart local v7    # "desFile":Ljava/io/File;
    const-string v25, "12345678901234567890123456789012"

    # invokes: Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->CreateSecurityKey(Ljava/lang/String;)[B
    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->access$100(Ljava/lang/String;)[B

    move-result-object v5

    .line 252
    .local v5, "byteKey":[B
    if-nez v5, :cond_2

    .line 253
    const/16 v25, 0x0

    .line 386
    :goto_1
    return-object v25

    .line 255
    :cond_2
    new-instance v19, Ljavax/crypto/spec/SecretKeySpec;

    const-string v25, "AES"

    move-object/from16 v0, v19

    move-object/from16 v1, v25

    invoke-direct {v0, v5, v1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 257
    .local v19, "key":Ljavax/crypto/spec/SecretKeySpec;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->getDeviceID()Ljava/lang/String;

    move-result-object v8

    .line 258
    .local v8, "deviceSeperator":Ljava/lang/String;
    if-eqz v8, :cond_3

    .line 259
    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mDeviceSeperator:[B

    .line 260
    :cond_3
    const/4 v6, 0x0

    .line 262
    .local v6, "cipher":Ljavax/crypto/Cipher;
    :try_start_0
    const-string v25, "AES/ECB/PKCS5Padding"

    invoke-static/range {v25 .. v25}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    .line 271
    :goto_2
    if-nez v6, :cond_4

    .line 272
    const/16 v25, 0x0

    goto :goto_1

    .line 263
    :catch_0
    move-exception v11

    .line 265
    .local v11, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v11}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_2

    .line 266
    .end local v11    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v11

    .line 268
    .local v11, "e":Ljavax/crypto/NoSuchPaddingException;
    invoke-virtual {v11}, Ljavax/crypto/NoSuchPaddingException;->printStackTrace()V

    goto :goto_2

    .line 274
    .end local v11    # "e":Ljavax/crypto/NoSuchPaddingException;
    :cond_4
    const/16 v17, 0x0

    .line 275
    .local v17, "is":Ljava/io/InputStream;
    const/16 v21, 0x0

    .line 276
    .local v21, "os":Ljava/io/OutputStream;
    const/4 v9, 0x0

    .line 279
    .local v9, "deviceSeperatorInEncFileToString":Ljava/lang/String;
    :try_start_1
    new-instance v18, Ljava/io/BufferedInputStream;

    new-instance v25, Ljava/io/FileInputStream;

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_f
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 280
    .end local v17    # "is":Ljava/io/InputStream;
    .local v18, "is":Ljava/io/InputStream;
    :try_start_2
    new-instance v22, Ljava/io/BufferedOutputStream;

    new-instance v25, Ljava/io/FileOutputStream;

    move-object/from16 v0, v25

    invoke-direct {v0, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_10
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 282
    .end local v21    # "os":Ljava/io/OutputStream;
    .local v22, "os":Ljava/io/OutputStream;
    :try_start_3
    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->length()J

    move-result-wide v14

    .line 283
    .local v14, "fileSize":J
    # getter for: Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->BUFFER_SIZE:I
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->access$300()I

    move-result v25

    move/from16 v0, v25

    int-to-long v0, v0

    move-wide/from16 v26, v0

    div-long v26, v14, v26

    # setter for: Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->numberofParms:J
    invoke-static/range {v26 .. v27}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->access$202(J)J

    .line 284
    const/16 v25, 0x0

    # setter for: Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->index:I
    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->access$402(I)I

    .line 286
    # getter for: Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->BUFFER_SIZE:I
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->access$300()I

    move-result v25

    move/from16 v0, v25

    new-array v4, v0, [B

    .line 287
    .local v4, "buffer":[B
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v25

    move/from16 v0, v25

    new-array v13, v0, [B

    .line 288
    .local v13, "getDeviceSeperatorFromEncFile":[B
    const/16 v23, 0x800

    .line 290
    .local v23, "readCnt":I
    const/16 v25, 0x2

    aget-object v25, p1, v25

    const-string v26, "lock"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_b

    .line 291
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v6, v0, v1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 294
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mDeviceSeperator:[B

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v27

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 296
    :goto_3
    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/io/InputStream;->read([B)I

    move-result v23

    const/16 v25, -0x1

    move/from16 v0, v23

    move/from16 v1, v25

    if-eq v0, v1, :cond_7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->isCancel:Z

    move/from16 v25, v0

    if-nez v25, :cond_7

    .line 297
    const/16 v25, 0x0

    move/from16 v0, v25

    move/from16 v1, v23

    invoke-virtual {v6, v4, v0, v1}, Ljavax/crypto/Cipher;->update([BII)[B

    move-result-object v25

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 298
    # operator++ for: Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->index:I
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->access$408()I

    .line 299
    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Integer;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    # getter for: Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->index:I
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->access$400()I

    move-result v27

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    # getter for: Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->numberofParms:J
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->access$200()J

    move-result-wide v28

    move-wide/from16 v0, v28

    long-to-float v0, v0

    move/from16 v28, v0

    div-float v27, v27, v28

    const/high16 v28, 0x42c80000    # 100.0f

    mul-float v27, v27, v28

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->publishProgress([Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 326
    .end local v4    # "buffer":[B
    .end local v13    # "getDeviceSeperatorFromEncFile":[B
    .end local v14    # "fileSize":J
    .end local v23    # "readCnt":I
    :catch_2
    move-exception v12

    move-object/from16 v21, v22

    .end local v22    # "os":Ljava/io/OutputStream;
    .restart local v21    # "os":Ljava/io/OutputStream;
    move-object/from16 v17, v18

    .line 327
    .end local v18    # "is":Ljava/io/InputStream;
    .local v12, "ex":Ljava/lang/Exception;
    .restart local v17    # "is":Ljava/io/InputStream;
    :goto_4
    :try_start_4
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 330
    const/16 v25, 0x2

    aget-object v25, p1, v25

    const-string v26, "lock"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_18

    .line 331
    if-eqz v21, :cond_5

    .line 333
    :try_start_5
    invoke-virtual/range {v21 .. v21}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_b

    .line 338
    :cond_5
    :goto_5
    if-eqz v17, :cond_6

    .line 340
    :try_start_6
    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->close()V

    .line 342
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->isCancel:Z

    move/from16 v25, v0

    if-eqz v25, :cond_17

    .line 343
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 348
    :goto_6
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->isCancel:Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    .line 383
    :cond_6
    :goto_7
    sget-object v25, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    .line 386
    .end local v12    # "ex":Ljava/lang/Exception;
    :goto_8
    const/16 v25, 0x0

    goto/16 :goto_1

    .line 302
    .end local v17    # "is":Ljava/io/InputStream;
    .end local v21    # "os":Ljava/io/OutputStream;
    .restart local v4    # "buffer":[B
    .restart local v13    # "getDeviceSeperatorFromEncFile":[B
    .restart local v14    # "fileSize":J
    .restart local v18    # "is":Ljava/io/InputStream;
    .restart local v22    # "os":Ljava/io/OutputStream;
    .restart local v23    # "readCnt":I
    :cond_7
    :try_start_7
    invoke-virtual {v6}, Ljavax/crypto/Cipher;->doFinal()[B

    move-result-object v25

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 330
    :cond_8
    :goto_9
    const/16 v25, 0x2

    aget-object v25, p1, v25

    const-string v26, "lock"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_13

    .line 331
    if-eqz v22, :cond_9

    .line 333
    :try_start_8
    invoke-virtual/range {v22 .. v22}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_9

    .line 338
    :cond_9
    :goto_a
    if-eqz v18, :cond_a

    .line 340
    :try_start_9
    invoke-virtual/range {v18 .. v18}, Ljava/io/InputStream;->close()V

    .line 342
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->isCancel:Z

    move/from16 v25, v0

    if-eqz v25, :cond_12

    .line 343
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 348
    :goto_b
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->isCancel:Z
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    .line 383
    :cond_a
    :goto_c
    sget-object v25, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    move-object/from16 v21, v22

    .end local v22    # "os":Ljava/io/OutputStream;
    .restart local v21    # "os":Ljava/io/OutputStream;
    move-object/from16 v17, v18

    .line 384
    .end local v18    # "is":Ljava/io/InputStream;
    .restart local v17    # "is":Ljava/io/InputStream;
    goto :goto_8

    .line 304
    .end local v17    # "is":Ljava/io/InputStream;
    .end local v21    # "os":Ljava/io/OutputStream;
    .restart local v18    # "is":Ljava/io/InputStream;
    .restart local v22    # "os":Ljava/io/OutputStream;
    :cond_b
    const/16 v25, 0x2

    :try_start_a
    aget-object v25, p1, v25

    const-string v26, "unlock"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_8

    .line 305
    const/16 v25, 0x2

    move/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v6, v0, v1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 307
    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/io/InputStream;->read([B)I

    move-result v23

    const/16 v25, -0x1

    move/from16 v0, v23

    move/from16 v1, v25

    if-eq v0, v1, :cond_c

    .line 309
    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v13}, Ljava/lang/String;-><init>([B)V

    .end local v9    # "deviceSeperatorInEncFileToString":Ljava/lang/String;
    .local v10, "deviceSeperatorInEncFileToString":Ljava/lang/String;
    move-object v9, v10

    .line 312
    .end local v10    # "deviceSeperatorInEncFileToString":Ljava/lang/String;
    .restart local v9    # "deviceSeperatorInEncFileToString":Ljava/lang/String;
    :cond_c
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_11

    .line 314
    :goto_d
    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/io/InputStream;->read([B)I

    move-result v23

    const/16 v25, -0x1

    move/from16 v0, v23

    move/from16 v1, v25

    if-eq v0, v1, :cond_10

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->isCancel:Z

    move/from16 v25, v0

    if-nez v25, :cond_10

    .line 315
    if-eqz v22, :cond_d

    .line 316
    const/16 v25, 0x0

    move/from16 v0, v25

    move/from16 v1, v23

    invoke-virtual {v6, v4, v0, v1}, Ljavax/crypto/Cipher;->update([BII)[B

    move-result-object v25

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 317
    :cond_d
    # operator++ for: Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->index:I
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->access$408()I

    .line 318
    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Integer;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    # getter for: Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->index:I
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->access$400()I

    move-result v27

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    # getter for: Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->numberofParms:J
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->access$200()J

    move-result-wide v28

    move-wide/from16 v0, v28

    long-to-float v0, v0

    move/from16 v28, v0

    div-float v27, v27, v28

    const/high16 v28, 0x42c80000    # 100.0f

    mul-float v27, v27, v28

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->publishProgress([Ljava/lang/Object;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_d

    .line 330
    .end local v4    # "buffer":[B
    .end local v13    # "getDeviceSeperatorFromEncFile":[B
    .end local v14    # "fileSize":J
    .end local v23    # "readCnt":I
    :catchall_0
    move-exception v25

    move-object/from16 v21, v22

    .end local v22    # "os":Ljava/io/OutputStream;
    .restart local v21    # "os":Ljava/io/OutputStream;
    move-object/from16 v17, v18

    .end local v18    # "is":Ljava/io/InputStream;
    .restart local v17    # "is":Ljava/io/InputStream;
    :goto_e
    const/16 v26, 0x2

    aget-object v26, p1, v26

    const-string v27, "lock"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1d

    .line 331
    if-eqz v21, :cond_e

    .line 333
    :try_start_b
    invoke-virtual/range {v21 .. v21}, Ljava/io/OutputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_d

    .line 338
    :cond_e
    :goto_f
    if-eqz v17, :cond_f

    .line 340
    :try_start_c
    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->close()V

    .line 342
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->isCancel:Z

    move/from16 v26, v0

    if-eqz v26, :cond_1c

    .line 343
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 348
    :goto_10
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->isCancel:Z
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_7

    .line 383
    :cond_f
    :goto_11
    sget-object v26, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    throw v25

    .line 320
    .end local v17    # "is":Ljava/io/InputStream;
    .end local v21    # "os":Ljava/io/OutputStream;
    .restart local v4    # "buffer":[B
    .restart local v13    # "getDeviceSeperatorFromEncFile":[B
    .restart local v14    # "fileSize":J
    .restart local v18    # "is":Ljava/io/InputStream;
    .restart local v22    # "os":Ljava/io/OutputStream;
    .restart local v23    # "readCnt":I
    :cond_10
    :try_start_d
    invoke-virtual {v6}, Ljavax/crypto/Cipher;->doFinal()[B

    move-result-object v25

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    goto/16 :goto_9

    .line 322
    :cond_11
    const/16 v25, 0x0

    const-string v26, "EncryptionUtils"

    const-string v27, "other device"

    invoke-static/range {v25 .. v27}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_2
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_9

    .line 345
    :cond_12
    :try_start_e
    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->delete()Z

    .line 346
    sget-object v25, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mContext:Landroid/content/Context;

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_3

    goto/16 :goto_b

    .line 349
    :catch_3
    move-exception v25

    goto/16 :goto_c

    .line 353
    :cond_13
    const/16 v25, 0x2

    aget-object v25, p1, v25

    const-string v26, "unlock"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_a

    .line 354
    if-eqz v22, :cond_14

    .line 356
    :try_start_f
    invoke-virtual/range {v22 .. v22}, Ljava/io/OutputStream;->close()V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_a

    .line 361
    :cond_14
    :goto_12
    if-eqz v18, :cond_a

    .line 363
    :try_start_10
    invoke-virtual/range {v18 .. v18}, Ljava/io/InputStream;->close()V

    .line 365
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_16

    .line 367
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->isCancel:Z

    move/from16 v25, v0

    if-eqz v25, :cond_15

    .line 368
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 377
    :goto_13
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->isCancel:Z

    goto/16 :goto_c

    .line 378
    :catch_4
    move-exception v25

    goto/16 :goto_c

    .line 370
    :cond_15
    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->delete()Z

    .line 371
    sget-object v25, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mContext:Landroid/content/Context;

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_13

    .line 375
    :cond_16
    invoke-virtual {v7}, Ljava/io/File;->delete()Z
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_4

    goto :goto_13

    .line 345
    .end local v4    # "buffer":[B
    .end local v13    # "getDeviceSeperatorFromEncFile":[B
    .end local v14    # "fileSize":J
    .end local v18    # "is":Ljava/io/InputStream;
    .end local v22    # "os":Ljava/io/OutputStream;
    .end local v23    # "readCnt":I
    .restart local v12    # "ex":Ljava/lang/Exception;
    .restart local v17    # "is":Ljava/io/InputStream;
    .restart local v21    # "os":Ljava/io/OutputStream;
    :cond_17
    :try_start_11
    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->delete()Z

    .line 346
    sget-object v25, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mContext:Landroid/content/Context;

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_5

    goto/16 :goto_6

    .line 349
    :catch_5
    move-exception v25

    goto/16 :goto_7

    .line 353
    :cond_18
    const/16 v25, 0x2

    aget-object v25, p1, v25

    const-string v26, "unlock"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_6

    .line 354
    if-eqz v21, :cond_19

    .line 356
    :try_start_12
    invoke-virtual/range {v21 .. v21}, Ljava/io/OutputStream;->close()V
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_c

    .line 361
    :cond_19
    :goto_14
    if-eqz v17, :cond_6

    .line 363
    :try_start_13
    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->close()V

    .line 365
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_1b

    .line 367
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->isCancel:Z

    move/from16 v25, v0

    if-eqz v25, :cond_1a

    .line 368
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 377
    :goto_15
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->isCancel:Z

    goto/16 :goto_7

    .line 378
    :catch_6
    move-exception v25

    goto/16 :goto_7

    .line 370
    :cond_1a
    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->delete()Z

    .line 371
    sget-object v25, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mContext:Landroid/content/Context;

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_15

    .line 375
    :cond_1b
    invoke-virtual {v7}, Ljava/io/File;->delete()Z
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_6

    goto :goto_15

    .line 345
    .end local v12    # "ex":Ljava/lang/Exception;
    :cond_1c
    :try_start_14
    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->delete()Z

    .line 346
    sget-object v26, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mContext:Landroid/content/Context;

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_7

    goto/16 :goto_10

    .line 349
    :catch_7
    move-exception v26

    goto/16 :goto_11

    .line 353
    :cond_1d
    const/16 v26, 0x2

    aget-object v26, p1, v26

    const-string v27, "unlock"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_f

    .line 354
    if-eqz v21, :cond_1e

    .line 356
    :try_start_15
    invoke-virtual/range {v21 .. v21}, Ljava/io/OutputStream;->close()V
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_e

    .line 361
    :cond_1e
    :goto_16
    if-eqz v17, :cond_f

    .line 363
    :try_start_16
    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->close()V

    .line 365
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_20

    .line 367
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->isCancel:Z

    move/from16 v26, v0

    if-eqz v26, :cond_1f

    .line 368
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 377
    :goto_17
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->isCancel:Z

    goto/16 :goto_11

    .line 378
    :catch_8
    move-exception v26

    goto/16 :goto_11

    .line 370
    :cond_1f
    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->delete()Z

    .line 371
    sget-object v26, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mContext:Landroid/content/Context;

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_17

    .line 375
    :cond_20
    invoke-virtual {v7}, Ljava/io/File;->delete()Z
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_8

    goto :goto_17

    .line 334
    .end local v17    # "is":Ljava/io/InputStream;
    .end local v21    # "os":Ljava/io/OutputStream;
    .restart local v4    # "buffer":[B
    .restart local v13    # "getDeviceSeperatorFromEncFile":[B
    .restart local v14    # "fileSize":J
    .restart local v18    # "is":Ljava/io/InputStream;
    .restart local v22    # "os":Ljava/io/OutputStream;
    .restart local v23    # "readCnt":I
    :catch_9
    move-exception v25

    goto/16 :goto_a

    .line 357
    :catch_a
    move-exception v25

    goto/16 :goto_12

    .line 334
    .end local v4    # "buffer":[B
    .end local v13    # "getDeviceSeperatorFromEncFile":[B
    .end local v14    # "fileSize":J
    .end local v18    # "is":Ljava/io/InputStream;
    .end local v22    # "os":Ljava/io/OutputStream;
    .end local v23    # "readCnt":I
    .restart local v12    # "ex":Ljava/lang/Exception;
    .restart local v17    # "is":Ljava/io/InputStream;
    .restart local v21    # "os":Ljava/io/OutputStream;
    :catch_b
    move-exception v25

    goto/16 :goto_5

    .line 357
    :catch_c
    move-exception v25

    goto/16 :goto_14

    .line 334
    .end local v12    # "ex":Ljava/lang/Exception;
    :catch_d
    move-exception v26

    goto/16 :goto_f

    .line 357
    :catch_e
    move-exception v26

    goto :goto_16

    .line 330
    :catchall_1
    move-exception v25

    goto/16 :goto_e

    .end local v17    # "is":Ljava/io/InputStream;
    .restart local v18    # "is":Ljava/io/InputStream;
    :catchall_2
    move-exception v25

    move-object/from16 v17, v18

    .end local v18    # "is":Ljava/io/InputStream;
    .restart local v17    # "is":Ljava/io/InputStream;
    goto/16 :goto_e

    .line 326
    :catch_f
    move-exception v12

    goto/16 :goto_4

    .end local v17    # "is":Ljava/io/InputStream;
    .restart local v18    # "is":Ljava/io/InputStream;
    :catch_10
    move-exception v12

    move-object/from16 v17, v18

    .end local v18    # "is":Ljava/io/InputStream;
    .restart local v17    # "is":Ljava/io/InputStream;
    goto/16 :goto_4
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 121
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->doInBackground([Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Long;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Long;

    .prologue
    .line 227
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->mIsLock:Z

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 230
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 121
    check-cast p1, Ljava/lang/Long;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->onPostExecute(Ljava/lang/Long;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 6

    .prologue
    const/16 v5, 0x64

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 145
    new-instance v0, Landroid/app/ProgressDialog;

    sget-object v1, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mProgressDialog:Landroid/app/ProgressDialog;

    sget-object v1, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0017

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread$1;-><init>(Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/ProgressDialog;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 169
    iget v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mLockState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mProgressDialog:Landroid/app/ProgressDialog;

    sget-object v1, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0106

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mProgressDialog:Landroid/app/ProgressDialog;

    sget-object v1, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0136

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 205
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 215
    return-void

    .line 176
    :cond_1
    iget v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mLockState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mProgressDialog:Landroid/app/ProgressDialog;

    sget-object v1, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0134

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 193
    :cond_2
    iget v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mLockState:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mProgressDialog:Landroid/app/ProgressDialog;

    sget-object v1, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0107

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mProgressDialog:Landroid/app/ProgressDialog;

    sget-object v1, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0137

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setMax(I)V

    goto :goto_0

    .line 200
    :cond_3
    iget v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mLockState:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mProgressDialog:Landroid/app/ProgressDialog;

    sget-object v1, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0135

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 2
    .param p1, "values"    # [Ljava/lang/Integer;

    .prologue
    .line 220
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 221
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 121
    check-cast p1, [Ljava/lang/Integer;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
