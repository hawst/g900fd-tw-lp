.class public Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;
.super Ljava/lang/Object;
.source "NearbyDevicesContainer.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static mInstance:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;


# instance fields
.field private mDevices:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/Device;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectDeviceKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->TAG:Ljava/lang/String;

    .line 111
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->mInstance:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->mDevices:Ljava/util/HashMap;

    .line 109
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->mSelectDeviceKey:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;
    .locals 2

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->mInstance:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;

    if-nez v0, :cond_1

    .line 32
    const-class v1, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;

    monitor-enter v1

    .line 33
    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->mInstance:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->mInstance:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;

    .line 36
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38
    :cond_1
    sget-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->mInstance:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;

    return-object v0

    .line 36
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public getDevice(Ljava/lang/String;)Lcom/samsung/android/allshare/Device;
    .locals 3
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 80
    if-eqz p1, :cond_1

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->mDevices:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->mDevices:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Device;

    .line 88
    :cond_0
    :goto_0
    return-object v0

    .line 87
    :cond_1
    sget-object v1, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->TAG:Ljava/lang/String;

    const-string v2, "getDevice: Id cannot be null."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getSelectedDeviceKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->mSelectDeviceKey:Ljava/lang/String;

    return-object v0
.end method

.method public registerDevice(Lcom/samsung/android/allshare/Device;)V
    .locals 2
    .param p1, "device"    # Lcom/samsung/android/allshare/Device;

    .prologue
    .line 64
    if-nez p1, :cond_0

    .line 65
    sget-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->TAG:Ljava/lang/String;

    const-string v1, "registerDevice: Device cannot be null."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    :goto_0
    return-void

    .line 68
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->registerDevice(Ljava/lang/String;Lcom/samsung/android/allshare/Device;)V

    goto :goto_0
.end method

.method public registerDevice(Ljava/lang/String;Lcom/samsung/android/allshare/Device;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "device"    # Lcom/samsung/android/allshare/Device;

    .prologue
    .line 50
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 51
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->TAG:Ljava/lang/String;

    const-string v1, "registerDevice: Either id or the device cannot be null."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    :goto_0
    return-void

    .line 53
    :cond_1
    iput-object p1, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->mSelectDeviceKey:Ljava/lang/String;

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->mDevices:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 56
    sget-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->TAG:Ljava/lang/String;

    const-string v1, "registerDevice: id already exists."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 58
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->mDevices:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public registerDevices(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/samsung/android/allshare/Device;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p1, "devices":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/android/allshare/Device;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Device;

    .line 45
    .local v0, "device":Lcom/samsung/android/allshare/Device;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->registerDevice(Lcom/samsung/android/allshare/Device;)V

    goto :goto_0

    .line 47
    .end local v0    # "device":Lcom/samsung/android/allshare/Device;
    :cond_0
    return-void
.end method

.method public resolveProvider(Ljava/lang/String;)Lcom/samsung/android/allshare/media/Provider;
    .locals 3
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 97
    const/4 v1, 0x0

    .line 98
    .local v1, "provider":Lcom/samsung/android/allshare/media/Provider;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->getDevice(Ljava/lang/String;)Lcom/samsung/android/allshare/Device;

    move-result-object v0

    .line 99
    .local v0, "device":Lcom/samsung/android/allshare/Device;
    if-eqz v0, :cond_0

    .line 100
    instance-of v2, v0, Lcom/samsung/android/allshare/media/Provider;

    if-eqz v2, :cond_0

    move-object v1, v0

    .line 101
    check-cast v1, Lcom/samsung/android/allshare/media/Provider;

    .line 104
    :cond_0
    return-object v1
.end method

.method public unRegisterDevice()V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->mDevices:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->mDevices:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 77
    :cond_0
    return-void
.end method
