.class public Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
.source "TreeViewAdapter.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;,
        Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$OnItemStateChangeListener;
    }
.end annotation


# static fields
.field protected static final SCROLL_BY_Y_OFFSET:I = 0x20

.field protected static final TOP_HEIGHT_FOR_AUTO_SCROLL:I = 0xdc

.field protected static final WAITING_TIME_FOR_AUTO_EXPAND:J = 0xc8L


# instance fields
.field protected mAutoExpand:Z

.field protected mContext:Landroid/content/Context;

.field protected mCurrentItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

.field protected mDepthWidth:I

.field protected mFoldedItemMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/TreeViewItem;",
            ">;>;"
        }
    .end annotation
.end field

.field protected mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/TreeViewItem;",
            ">;"
        }
    .end annotation
.end field

.field protected mPathItemMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/myfiles/element/TreeViewItem;",
            ">;"
        }
    .end annotation
.end field

.field protected mPositionIndicatorPressed:Z

.field protected mPositionIndicatorShown:Z

.field protected mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

.field protected mTreeViewYOffset:I

.field protected mTreeViewYPos:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;-><init>()V

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mPositionIndicatorShown:Z

    .line 64
    iput v1, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mTreeViewYPos:I

    .line 66
    iput v1, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mTreeViewYOffset:I

    .line 77
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mContext:Landroid/content/Context;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mItems:Ljava/util/ArrayList;

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09012e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mDepthWidth:I

    .line 83
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mPathItemMap:Ljava/util/HashMap;

    .line 85
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mFoldedItemMap:Ljava/util/HashMap;

    .line 86
    return-void
.end method


# virtual methods
.method public addItem(Lcom/sec/android/app/myfiles/element/TreeViewItem;)V
    .locals 2
    .param p1, "item"    # Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .prologue
    .line 128
    if-eqz p1, :cond_0

    .line 130
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v1

    .line 132
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    monitor-exit v1

    .line 135
    :cond_0
    return-void

    .line 133
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected changeCurrentItem(Lcom/sec/android/app/myfiles/element/TreeViewItem;)V
    .locals 2
    .param p1, "item"    # Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mCurrentItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mCurrentItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mCurrentSelected:Z

    .line 170
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mCurrentItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mCurrentItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mCurrentSelected:Z

    .line 174
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->notifyDataSetChanged()V

    .line 175
    return-void
.end method

.method protected clearAll()V
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mPathItemMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mFoldedItemMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 160
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mCurrentItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .line 161
    return-void
.end method

.method public expandChild(Ljava/lang/String;)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 196
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->expandChild(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method protected expandChild(Ljava/lang/String;Z)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "withSynchronized"    # Z

    .prologue
    .line 206
    const/4 v0, 0x0

    return v0
.end method

.method public finishSelectMode()V
    .locals 0

    .prologue
    .line 428
    return-void
.end method

.method public foldChild(Ljava/lang/String;)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 201
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->foldChild(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method protected foldChild(Ljava/lang/String;Z)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "withSynchronized"    # Z

    .prologue
    .line 211
    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 120
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v1

    .line 122
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getCurrentItem()Lcom/sec/android/app/myfiles/element/TreeViewItem;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mCurrentItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    return-object v0
.end method

.method public getCurrentItemIndex()I
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mCurrentItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    if-nez v0, :cond_0

    .line 181
    const/4 v0, -0x1

    .line 185
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mItems:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mCurrentItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 140
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v1

    .line 142
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 143
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 149
    int-to-long v0, p1

    return-wide v0
.end method

.method public getSelectedItem()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 460
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSelectedItemCount()I
    .locals 1

    .prologue
    .line 466
    const/4 v0, 0x0

    return v0
.end method

.method public getSelectedItemPosition()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 454
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTreeViewYOffset()I
    .locals 1

    .prologue
    .line 482
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mTreeViewYOffset:I

    return v0
.end method

.method public getTreeViewYPos()I
    .locals 1

    .prologue
    .line 474
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mTreeViewYPos:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ResourceAsColor"
        }
    .end annotation

    .prologue
    const/16 v10, 0x1f

    const/16 v7, 0x8

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 247
    const/4 v4, 0x0

    .line 249
    .local v4, "vh":Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;
    if-nez p2, :cond_4

    .line 251
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mContext:Landroid/content/Context;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 253
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f04004e

    invoke-virtual {v0, v5, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 255
    new-instance v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;

    .end local v4    # "vh":Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;
    invoke-direct {v4}, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;-><init>()V

    .line 257
    .restart local v4    # "vh":Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;
    const v5, 0x7f0f0115

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mContentsContainer:Landroid/widget/LinearLayout;

    .line 259
    const v5, 0x7f0f0116

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mExpandIndicator:Landroid/widget/ImageView;

    .line 261
    const v5, 0x7f0f0031

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    .line 263
    const v5, 0x7f0f0004

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    .line 265
    const v5, 0x7f0f0117

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mPositionIndicator:Landroid/widget/ImageView;

    .line 267
    invoke-virtual {p2, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 274
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .line 276
    .local v1, "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    iput-object v1, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .line 278
    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mExpandIndicator:Landroid/widget/ImageView;

    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 280
    iget v5, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mDepth:I

    if-ltz v5, :cond_0

    .line 282
    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mContentsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 284
    .local v2, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 286
    .local v3, "oldLeftMargin":I
    iget v5, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mDepth:I

    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mDepthWidth:I

    mul-int/2addr v5, v6

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 288
    iget v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    if-eq v3, v5, :cond_0

    .line 290
    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mContentsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->requestLayout()V

    .line 294
    .end local v2    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v3    # "oldLeftMargin":I
    :cond_0
    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    iget-object v6, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mTitle:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 296
    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    iget v5, v5, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    if-eq v5, v7, :cond_1

    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    iget v5, v5, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    if-ne v5, v10, :cond_5

    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 299
    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    iput-boolean v8, v5, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mEnabled:Z

    .line 301
    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mContentsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v9}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 312
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "sec_container_"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    iget v5, v5, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    if-eq v5, v10, :cond_2

    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    iget v5, v5, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    if-eq v5, v7, :cond_2

    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    iget v5, v5, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    const/16 v6, 0xd

    if-ne v5, v6, :cond_3

    .line 315
    :cond_2
    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    iput-boolean v8, v5, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mEnabled:Z

    .line 316
    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mContentsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v9}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 319
    :cond_3
    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    iget-object v6, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    iget-boolean v6, v6, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mEnabled:Z

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 321
    iget-boolean v5, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mIsExpanded:Z

    if-eqz v5, :cond_6

    .line 323
    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mExpandIndicator:Landroid/widget/ImageView;

    const v6, 0x7f0200f4

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 325
    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mExpandIndicator:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b00fd

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 327
    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    iget v6, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mExpandedIconResId:I

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 338
    :goto_2
    iget-boolean v5, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mHasChildFolder:Z

    if-nez v5, :cond_7

    .line 340
    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mExpandIndicator:Landroid/widget/ImageView;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 348
    :goto_3
    iget-boolean v5, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mCurrentSelected:Z

    if-eqz v5, :cond_8

    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mPositionIndicatorShown:Z

    if-eqz v5, :cond_8

    .line 352
    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setSelected(Z)V

    .line 353
    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setActivated(Z)V

    .line 362
    :goto_4
    return-object p2

    .line 271
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    :cond_4
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "vh":Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;
    check-cast v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;

    .restart local v4    # "vh":Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;
    goto/16 :goto_0

    .line 305
    .restart local v1    # "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    :cond_5
    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    iput-boolean v9, v5, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mEnabled:Z

    .line 307
    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mContentsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v8}, Landroid/widget/LinearLayout;->setClickable(Z)V

    goto/16 :goto_1

    .line 331
    :cond_6
    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mExpandIndicator:Landroid/widget/ImageView;

    const v6, 0x7f0200f3

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 333
    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mExpandIndicator:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b00fc

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 335
    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    iget v6, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mIconResId:I

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 344
    :cond_7
    iget-object v5, v4, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mExpandIndicator:Landroid/widget/ImageView;

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    .line 357
    :cond_8
    const v5, 0x106000d

    invoke-virtual {p2, v5}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_4
.end method

.method public isCurrentItem(Lcom/sec/android/app/myfiles/element/TreeViewItem;)Z
    .locals 1
    .param p1, "item"    # Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mCurrentItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mCurrentItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 114
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSelectMode()Z
    .locals 1

    .prologue
    .line 433
    const/4 v0, 0x0

    return v0
.end method

.method public refresh(Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 0
    .param p1, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    .line 388
    return-void
.end method

.method public selectAllItem()V
    .locals 0

    .prologue
    .line 444
    return-void
.end method

.method public selectItem(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 439
    return-void
.end method

.method public setAutoExpand(Z)V
    .locals 0
    .param p1, "autoExpand"    # Z

    .prologue
    .line 90
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mAutoExpand:Z

    .line 91
    return-void
.end method

.method public setCurrentPosition(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 95
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v1

    .line 97
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mCurrentItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mCurrentItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mCurrentSelected:Z

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mCurrentItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mCurrentItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mCurrentSelected:Z

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->setSelection(I)V

    .line 107
    monitor-exit v1

    .line 108
    return-void

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setPositionIndicatorPressed(Z)V
    .locals 0
    .param p1, "pressed"    # Z

    .prologue
    .line 367
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mPositionIndicatorPressed:Z

    .line 368
    return-void
.end method

.method public setTreeView(Lcom/sec/android/app/myfiles/view/MyFilesTreeView;)V
    .locals 0
    .param p1, "treeView"    # Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    .prologue
    .line 470
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    .line 471
    return-void
.end method

.method public setTreeViewYOffset(I)V
    .locals 0
    .param p1, "mTreeViewYOffset"    # I

    .prologue
    .line 486
    iput p1, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mTreeViewYOffset:I

    .line 487
    return-void
.end method

.method public setTreeViewYPos(I)V
    .locals 0
    .param p1, "mTreeViewYPos"    # I

    .prologue
    .line 478
    iput p1, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mTreeViewYPos:I

    .line 479
    return-void
.end method

.method public showPositionIndicator(Z)V
    .locals 1
    .param p1, "show"    # Z

    .prologue
    .line 372
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mPositionIndicatorShown:Z

    if-eq v0, p1, :cond_0

    .line 374
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mPositionIndicatorShown:Z

    .line 376
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->notifyDataSetChanged()V

    .line 378
    :cond_0
    return-void
.end method

.method public startSelectMode(II)V
    .locals 0
    .param p1, "selectType"    # I
    .param p2, "from"    # I

    .prologue
    .line 423
    return-void
.end method

.method public toggleExpandAndFold(Lcom/sec/android/app/myfiles/element/TreeViewItem;)Z
    .locals 6
    .param p1, "item"    # Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .prologue
    const/4 v2, 0x1

    .line 216
    const/4 v1, 0x0

    .line 218
    .local v1, "result":Z
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v3

    .line 220
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 222
    iget-boolean v0, p1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mIsExpanded:Z

    .line 224
    .local v0, "isExpanded":Z
    if-eqz v0, :cond_1

    .line 226
    iget-object v4, p1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->foldChild(Ljava/lang/String;Z)Z

    move-result v1

    .line 233
    :goto_0
    if-eqz v1, :cond_0

    .line 235
    if-nez v0, :cond_2

    :goto_1
    iput-boolean v2, p1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mIsExpanded:Z

    .line 238
    .end local v0    # "isExpanded":Z
    :cond_0
    monitor-exit v3

    .line 240
    return v1

    .line 230
    .restart local v0    # "isExpanded":Z
    :cond_1
    iget-object v4, p1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->expandChild(Ljava/lang/String;Z)Z

    move-result v1

    goto :goto_0

    .line 235
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 238
    .end local v0    # "isExpanded":Z
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public unselectAllItem()V
    .locals 0

    .prologue
    .line 449
    return-void
.end method

.method public update(Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 0
    .param p1, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    .line 383
    return-void
.end method
