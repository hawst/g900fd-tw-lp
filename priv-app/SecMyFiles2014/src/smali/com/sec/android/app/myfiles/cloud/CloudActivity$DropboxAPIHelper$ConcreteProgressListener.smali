.class public Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper$ConcreteProgressListener;
.super Ljava/lang/Object;
.source "CloudActivity.java"

# interfaces
.implements Lcom/samsung/scloud/response/ProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ConcreteProgressListener"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onException(Lcom/samsung/scloud/exception/SCloudException;)V
    .locals 0
    .param p1, "e"    # Lcom/samsung/scloud/exception/SCloudException;

    .prologue
    .line 135
    return-void
.end method

.method public onUpdate(Ljava/lang/String;IJ)V
    .locals 0
    .param p1, "file"    # Ljava/lang/String;
    .param p2, "statusTransferOngoing"    # I
    .param p3, "progress"    # J

    .prologue
    .line 140
    return-void
.end method
