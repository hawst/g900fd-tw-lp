.class public abstract Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
.super Ljava/lang/Object;
.source "AbsNavigation.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/adapter/IContentsFilter;
.implements Lcom/sec/android/app/myfiles/navigation/INavigation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;,
        Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;
    }
.end annotation


# static fields
.field protected static final EXTENTION_FILTER:Ljava/lang/String; = "\"abcdefghijklmnopqrstuvwxyz01234567890;!@#$%^&()-_=+\""

.field protected static final FILE_NAME_FIND_FILTER:Ljava/lang/String; = "\"abcdefghijklmnopqrstuvwxyz01234567890;.!@#$%^&()-_=+\""


# instance fields
.field protected mCategoryType:I

.field protected mContext:Landroid/content/Context;

.field protected mCurrentFolderPath:Ljava/lang/String;

.field protected mCurrentInOrder:I

.field protected mCurrentSortBy:I

.field private mDrmFiltering:Z

.field private mEnableFiltering:Z

.field protected mExceptFilteringExtension:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mExceptMimetypeForFilter:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mFilteringExtension:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mHistoricalPaths:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;

.field private mIsAllAttachMode:Z

.field protected mMimetypeForFilter:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mObserverList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;",
            ">;"
        }
    .end annotation
.end field

.field protected mOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

.field protected mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

.field protected mRoot:Ljava/lang/String;

.field protected mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

.field protected mShowFolderFileType:I

.field public mWrongUpPath:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mContext:Landroid/content/Context;

    .line 94
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mHistoricalPaths:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    .line 98
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mMimetypeForFilter:Ljava/util/HashSet;

    .line 100
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mFilteringExtension:Ljava/util/HashSet;

    .line 102
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mExceptMimetypeForFilter:Ljava/util/HashSet;

    .line 104
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mExceptFilteringExtension:Ljava/util/HashSet;

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mObserverList:Ljava/util/ArrayList;

    .line 108
    iput p2, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mCategoryType:I

    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 112
    iget v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mCategoryType:I

    sparse-switch v0, :sswitch_data_0

    .line 153
    :goto_0
    :sswitch_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mShowFolderFileType:I

    .line 154
    return-void

    .line 116
    :sswitch_1
    const-string v0, "/storage"

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mRoot:Ljava/lang/String;

    .line 117
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getRootFolder()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mCurrentFolderPath:Ljava/lang/String;

    goto :goto_0

    .line 133
    :sswitch_2
    const-string v0, "/Dropbox"

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mRoot:Ljava/lang/String;

    .line 134
    const-string v0, "/Dropbox"

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mCurrentFolderPath:Ljava/lang/String;

    goto :goto_0

    .line 138
    :sswitch_3
    const-string v0, "/Baidu"

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mRoot:Ljava/lang/String;

    .line 139
    const-string v0, "/Baidu"

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mCurrentFolderPath:Ljava/lang/String;

    goto :goto_0

    .line 112
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x4 -> :sswitch_0
        0x5 -> :sswitch_0
        0x8 -> :sswitch_2
        0x9 -> :sswitch_0
        0x12 -> :sswitch_0
        0x16 -> :sswitch_1
        0x1f -> :sswitch_3
        0x201 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public abstract AsyncOpen()V
.end method

.method public addExceptFilterExtension(Ljava/lang/String;)V
    .locals 6
    .param p1, "extension"    # Ljava/lang/String;

    .prologue
    .line 328
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 330
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    .line 332
    const-string v5, ";"

    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 334
    const-string v5, ";"

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 336
    .local v1, "extensions":[Ljava/lang/String;
    move-object v0, v1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    .line 338
    .local v4, "oneExtension":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mExceptFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v5, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 340
    iget-object v5, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v5, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 336
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 345
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "extensions":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "oneExtension":Ljava/lang/String;
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mExceptFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v5, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 347
    iget-object v5, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v5, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 350
    :cond_1
    return-void
.end method

.method public addExceptFilterMimeType(Ljava/lang/String;)V
    .locals 7
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 286
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 290
    const-string v6, ";"

    invoke-virtual {p1, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 292
    const-string v6, ";"

    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 294
    .local v4, "mimeTypes":[Ljava/lang/String;
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v5, v0, v2

    .line 296
    .local v5, "oneMimeType":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mExceptMimetypeForFilter:Ljava/util/HashSet;

    invoke-virtual {v6, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 298
    invoke-static {v5}, Lcom/sec/android/app/myfiles/MediaFile;->getExtensionFromMimeType(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 300
    .local v1, "extensionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v1, :cond_0

    .line 302
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mExceptFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v6, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 304
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v6, v1}, Ljava/util/HashSet;->removeAll(Ljava/util/Collection;)Z

    .line 294
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 310
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "extensionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "mimeTypes":[Ljava/lang/String;
    .end local v5    # "oneMimeType":Ljava/lang/String;
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mExceptMimetypeForFilter:Ljava/util/HashSet;

    invoke-virtual {v6, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 312
    invoke-static {p1}, Lcom/sec/android/app/myfiles/MediaFile;->getExtensionFromMimeType(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 314
    .restart local v1    # "extensionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v1, :cond_2

    .line 316
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mExceptFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v6, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 318
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v6, v1}, Ljava/util/HashSet;->removeAll(Ljava/util/Collection;)Z

    .line 322
    .end local v1    # "extensionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    return-void
.end method

.method public addFilterExtension(Ljava/lang/String;)V
    .locals 6
    .param p1, "extension"    # Ljava/lang/String;

    .prologue
    .line 262
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 264
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    .line 266
    const-string v5, ";"

    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 268
    const-string v5, ";"

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 270
    .local v1, "extensions":[Ljava/lang/String;
    move-object v0, v1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    .line 272
    .local v4, "oneExtension":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v5, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 270
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 277
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "extensions":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "oneExtension":Ljava/lang/String;
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v5, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 280
    :cond_1
    return-void
.end method

.method public addFilterMimeType(Ljava/lang/String;)V
    .locals 7
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 224
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 228
    const-string v6, ";"

    invoke-virtual {p1, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 230
    const-string v6, ";"

    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 232
    .local v4, "mimeTypes":[Ljava/lang/String;
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v5, v0, v2

    .line 234
    .local v5, "oneMimeType":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mMimetypeForFilter:Ljava/util/HashSet;

    invoke-virtual {v6, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 236
    invoke-static {v5}, Lcom/sec/android/app/myfiles/MediaFile;->getExtensionFromMimeType(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 238
    .local v1, "extensionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v1, :cond_0

    .line 240
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v6, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 232
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 246
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "extensionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "mimeTypes":[Ljava/lang/String;
    .end local v5    # "oneMimeType":Ljava/lang/String;
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mMimetypeForFilter:Ljava/util/HashSet;

    invoke-virtual {v6, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 248
    invoke-static {p1}, Lcom/sec/android/app/myfiles/MediaFile;->getExtensionFromMimeType(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 250
    .restart local v1    # "extensionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v1, :cond_2

    .line 252
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v6, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 256
    .end local v1    # "extensionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    return-void
.end method

.method public clearFilterExtension()V
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 369
    return-void
.end method

.method public clearFilterMimeType()V
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mMimetypeForFilter:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 364
    return-void
.end method

.method public clearHistoricalPaths()V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mHistoricalPaths:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;

    if-eqz v0, :cond_1

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mHistoricalPaths:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->pop()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 85
    :cond_1
    return-void
.end method

.method public getCategoryType()I
    .locals 1

    .prologue
    .line 158
    iget v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mCategoryType:I

    return v0
.end method

.method public getCurrentFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mCurrentFolderPath:Ljava/lang/String;

    return-object v0
.end method

.method public getHistoricalPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mHistoricalPaths:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->pop()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHistoricalPathWithoutRemoving()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mHistoricalPaths:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->get()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getInstance()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    .locals 0

    .prologue
    .line 524
    return-object p0
.end method

.method public getOnScrollListener()Landroid/widget/AbsListView$OnScrollListener;
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    return-object v0
.end method

.method public getRootPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mRoot:Ljava/lang/String;

    return-object v0
.end method

.method public isAllAttachMode()Z
    .locals 1

    .prologue
    .line 386
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mIsAllAttachMode:Z

    return v0
.end method

.method public isDrmFiltering()Z
    .locals 1

    .prologue
    .line 532
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mDrmFiltering:Z

    return v0
.end method

.method public isEnableFiltering()Z
    .locals 1

    .prologue
    .line 217
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mEnableFiltering:Z

    return v0
.end method

.method public notifyUpdate(Z)V
    .locals 2
    .param p1, "needToRefresh"    # Z

    .prologue
    .line 490
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/myfiles/AbsMainActivity;

    new-instance v1, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$1;-><init>(Lcom/sec/android/app/myfiles/navigation/AbsNavigation;Z)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 511
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 186
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 190
    return-void
.end method

.method public abstract refreshNavigation()V
.end method

.method public registerObserver(Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;)V
    .locals 2
    .param p1, "observer"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;

    .prologue
    .line 469
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mObserverList:Ljava/util/ArrayList;

    monitor-enter v1

    .line 471
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mObserverList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 473
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mObserverList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 475
    :cond_0
    monitor-exit v1

    .line 476
    return-void

    .line 475
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeFilterMimeType(Ljava/lang/String;)V
    .locals 1
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 356
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mMimetypeForFilter:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 357
    return-void
.end method

.method public removeObserver(Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;)V
    .locals 2
    .param p1, "observer"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;

    .prologue
    .line 481
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mObserverList:Ljava/util/ArrayList;

    monitor-enter v1

    .line 483
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mObserverList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 484
    monitor-exit v1

    .line 485
    return-void

    .line 484
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setAllAttachMode(Z)V
    .locals 0
    .param p1, "isAllAttachMode"    # Z

    .prologue
    .line 391
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mIsAllAttachMode:Z

    .line 392
    return-void
.end method

.method public setDrmFiltering(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 528
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mDrmFiltering:Z

    .line 529
    return-void
.end method

.method public setEnableFiltering(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 210
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mEnableFiltering:Z

    .line 211
    return-void
.end method

.method public setShowFolderFileType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 203
    iput p1, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mShowFolderFileType:I

    .line 204
    return-void
.end method

.method public setSortBy(II)V
    .locals 0
    .param p1, "sortBy"    # I
    .param p2, "orderBy"    # I

    .prologue
    .line 194
    iput p1, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mCurrentSortBy:I

    .line 196
    iput p2, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mCurrentInOrder:I

    .line 197
    return-void
.end method

.method public setSupportAsync(Lcom/sec/android/app/myfiles/fragment/AbsFragment;)V
    .locals 0
    .param p1, "fg"    # Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .prologue
    .line 380
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 381
    return-void
.end method
