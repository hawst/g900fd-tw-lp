.class Lcom/sec/android/app/myfiles/SearchActivity$SpinnerArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/SearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SpinnerArrayAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/CharSequence;",
        ">;"
    }
.end annotation


# instance fields
.field private mCurrentPosition:I

.field private mSelectedColor:Landroid/content/res/ColorStateList;

.field private mTextColor:Landroid/content/res/ColorStateList;


# direct methods
.method public constructor <init>(Landroid/content/Context;I[Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textViewResourceId"    # I
    .param p3, "objects"    # [Ljava/lang/CharSequence;

    .prologue
    .line 757
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 748
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/SearchActivity$SpinnerArrayAdapter;->mCurrentPosition:I

    .line 759
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080031

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity$SpinnerArrayAdapter;->mTextColor:Landroid/content/res/ColorStateList;

    .line 761
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080032

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity$SpinnerArrayAdapter;->mSelectedColor:Landroid/content/res/ColorStateList;

    .line 762
    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/SearchActivity$SpinnerArrayAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SearchActivity$SpinnerArrayAdapter;

    .prologue
    .line 746
    iget v0, p0, Lcom/sec/android/app/myfiles/SearchActivity$SpinnerArrayAdapter;->mCurrentPosition:I

    return v0
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 774
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 776
    .local v0, "view":Landroid/widget/TextView;
    iget v1, p0, Lcom/sec/android/app/myfiles/SearchActivity$SpinnerArrayAdapter;->mCurrentPosition:I

    if-ne p1, v1, :cond_0

    .line 778
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SearchActivity$SpinnerArrayAdapter;->mSelectedColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 785
    :goto_0
    return-object v0

    .line 782
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SearchActivity$SpinnerArrayAdapter;->mTextColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0
.end method

.method public setCurrentPosition(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 767
    iput p1, p0, Lcom/sec/android/app/myfiles/SearchActivity$SpinnerArrayAdapter;->mCurrentPosition:I

    .line 768
    return-void
.end method
