.class public Lcom/sec/android/app/myfiles/SettingsActivity;
.super Landroid/preference/PreferenceActivity;
.source "SettingsActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final MODULE:Ljava/lang/String; = "SettingsActivity"


# instance fields
.field private mChangePin:Landroid/preference/Preference;

.field private mCurrentExtension:Z

.field private mCurrentHidden:Z

.field private mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

.field private mShowFileExtension:Landroid/preference/CheckBoxPreference;

.field private mShowHiddenFile:Landroid/preference/CheckBoxPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method

.method private ChangePinSetting()V
    .locals 3

    .prologue
    .line 208
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 210
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.myfiles"

    const-string v2, "com.sec.android.app.myfiles.activity.ChangePasswordsActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 211
    const-string v1, "change_pin"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 213
    const/16 v1, 0x1a

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/myfiles/SettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 215
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/SettingsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SettingsActivity;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/SettingsActivity;->ChangePinSetting()V

    return-void
.end method

.method private pinCheck()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 189
    const/4 v0, 0x2

    const-string v1, "SettingsActivity"

    const-string v2, "pinCheck"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 191
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SettingsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "applock_enable"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mChangePin:Landroid/preference/Preference;

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 204
    :goto_0
    return-void

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mChangePin:Landroid/preference/Preference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 3

    .prologue
    .line 132
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mCurrentExtension:Z

    iget-object v2, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowFileExtensionStatus()Z

    move-result v2

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mCurrentHidden:Z

    iget-object v2, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowHiddenFileStatus()Z

    move-result v2

    if-eq v1, v2, :cond_1

    .line 134
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 135
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "settingChanged"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 136
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/myfiles/SettingsActivity;->setResult(ILandroid/content/Intent;)V

    .line 138
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onBackPressed()V

    .line 139
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SettingsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SettingsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 65
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->setNotibarAutoShowing(Landroid/app/Activity;)V

    .line 67
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SettingsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 71
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SettingsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SettingsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SettingsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b001e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 77
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    .line 79
    const v0, 0x7f060001

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/SettingsActivity;->addPreferencesFromResource(I)V

    .line 81
    const-string v0, "show_hidden_files"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mShowHiddenFile:Landroid/preference/CheckBoxPreference;

    .line 83
    const-string v0, "show_file_extension"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mShowFileExtension:Landroid/preference/CheckBoxPreference;

    .line 87
    const-string v0, "change_pin"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mChangePin:Landroid/preference/Preference;

    .line 90
    const-string v0, "ChinaNalSecurity"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Common_ConfigLocalSecurityPolicy"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/SettingsActivity;->pinCheck()V

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mShowHiddenFile:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mShowHiddenFile:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mShowFileExtension:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_2

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mShowFileExtension:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 104
    :cond_2
    const-string v0, "ChinaNalSecurity"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Common_ConfigLocalSecurityPolicy"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mChangePin:Landroid/preference/Preference;

    new-instance v1, Lcom/sec/android/app/myfiles/SettingsActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/SettingsActivity$1;-><init>(Lcom/sec/android/app/myfiles/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 121
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowFileExtensionStatus()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mCurrentExtension:Z

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mShowFileExtension:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_3

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mShowFileExtension:Landroid/preference/CheckBoxPreference;

    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mCurrentExtension:Z

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 126
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowHiddenFileStatus()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mCurrentHidden:Z

    .line 127
    return-void

    .line 115
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mChangePin:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 144
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->commitSettings()Z

    .line 147
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 153
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 162
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 157
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SettingsActivity;->onBackPressed()V

    .line 159
    const/4 v0, 0x1

    goto :goto_0

    .line 153
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    .line 169
    const/4 v1, 0x2

    const-string v2, "SettingsActivity"

    const-string v3, "onPreferenceChange()"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 171
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mShowHiddenFile:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 173
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setShowHiddenFileStatus(Z)V

    .line 184
    :goto_0
    return v0

    .line 177
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mShowFileExtension:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 179
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SettingsActivity;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setShowFileExtensionStatus(Z)V

    goto :goto_0

    .line 184
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
