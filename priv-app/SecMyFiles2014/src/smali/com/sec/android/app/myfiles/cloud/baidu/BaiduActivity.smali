.class public Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity;
.super Ljava/lang/Object;
.source "BaiduActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;
    }
.end annotation


# static fields
.field private static final CLOUD_ACTION_REQUEST_SIGNIN:Ljava/lang/String; = "com.sec.android.cloudagent.ACTION_REQUEST_SIGNIN"

.field private static final CLOUD_ACTION_REQUEST_TOKEN:Ljava/lang/String; = "com.sec.android.cloudagent.ACTION_REQUEST_TOKEN"

.field private static final DEBUG:Z

.field private static final TAG:Ljava/lang/String; = "BaiduAPIHelper"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity;->DEBUG:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 41
    sget-boolean v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity;->DEBUG:Z

    return v0
.end method
