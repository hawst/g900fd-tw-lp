.class Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$9;
.super Ljava/lang/Object;
.source "AbsBrowserFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->lock(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

.field final synthetic val$path:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2500
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iput-object p2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$9;->val$path:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 2503
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$9;->val$path:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->isEncryptionFile(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2505
    if-eqz p1, :cond_0

    .line 2506
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2509
    :cond_0
    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$200()Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2510
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setShowLockFilesStatus(Z)V

    .line 2513
    :cond_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2514
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.sec.android.app.myfiles"

    const-string v3, "com.sec.android.app.myfiles.activity.ChangePasswordsActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2519
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    const/16 v3, 0x19

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2547
    :goto_0
    return-void

    .line 2520
    :catch_0
    move-exception v0

    .line 2522
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "AbsBrowserFragment"

    const-string v3, "om.sec.android.app.myfiles.activity.ChangePasswordsActivity not found - ShowLockFilesStatus unlock"

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2527
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_2
    if-eqz p1, :cond_3

    .line 2528
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2531
    :cond_3
    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$200()Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2532
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setShowLockFilesStatus(Z)V

    .line 2534
    :cond_4
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2535
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v2, "com.sec.android.app.myfiles"

    const-string v3, "com.sec.android.app.myfiles.activity.ChangePasswordsActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2541
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    const/16 v3, 0x17

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 2542
    :catch_1
    move-exception v0

    .line 2544
    .restart local v0    # "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "AbsBrowserFragment"

    const-string v3, "om.sec.android.app.myfiles.activity.ChangePasswordsActivity not found - ShowLockFilesStatus lock"

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
