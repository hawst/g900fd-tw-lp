.class public interface abstract Lcom/sec/android/app/myfiles/adapter/IContentsFilter;
.super Ljava/lang/Object;
.source "IContentsFilter.java"


# virtual methods
.method public abstract addExceptFilterExtension(Ljava/lang/String;)V
.end method

.method public abstract addExceptFilterMimeType(Ljava/lang/String;)V
.end method

.method public abstract addFilterExtension(Ljava/lang/String;)V
.end method

.method public abstract addFilterMimeType(Ljava/lang/String;)V
.end method

.method public abstract clearFilterMimeType()V
.end method

.method public abstract isEnableFiltering()Z
.end method

.method public abstract removeFilterMimeType(Ljava/lang/String;)V
.end method

.method public abstract setEnableFiltering(Z)V
.end method

.method public abstract setShowFolderFileType(I)V
.end method
