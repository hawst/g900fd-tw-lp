.class Lcom/sec/android/app/myfiles/MainActivity$5;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/MainActivity;)V
    .locals 0

    .prologue
    .line 1759
    iput-object p1, p0, Lcom/sec/android/app/myfiles/MainActivity$5;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1764
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    move v0, v1

    .line 1813
    :cond_0
    :goto_0
    return v0

    .line 1769
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$5;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v2, v2, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    if-eqz v2, :cond_1

    .line 1771
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$5;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onRefresh()V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1776
    goto :goto_0

    .line 1780
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$5;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$5;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v2, :cond_2

    .line 1781
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$5;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v2, v2, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    if-eqz v2, :cond_2

    .line 1783
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$5;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onRefresh()V

    .line 1785
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getDialogInstance()Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1786
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getDialogInstance()Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->dismissWaitProgressDialog()V

    .line 1787
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->setDialogInstance(Lcom/sec/android/app/myfiles/utils/MyFilesDialog;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1793
    goto :goto_0

    .line 1797
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$5;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mSearchable:Lcom/sec/android/app/myfiles/fragment/ISearchable;

    if-eqz v2, :cond_3

    .line 1799
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$5;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # setter for: Lcom/sec/android/app/myfiles/MainActivity;->isSearchPeforming:Z
    invoke-static {v1, v0}, Lcom/sec/android/app/myfiles/MainActivity;->access$402(Lcom/sec/android/app/myfiles/MainActivity;Z)Z

    .line 1801
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$5;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mSearchable:Lcom/sec/android/app/myfiles/fragment/ISearchable;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$5;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mParameter:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    invoke-interface {v1, v2}, Lcom/sec/android/app/myfiles/fragment/ISearchable;->search(Lcom/sec/android/app/myfiles/utils/SearchParameter;)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1805
    goto :goto_0

    .line 1808
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$5;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/MainActivity;->unRegisterReceiver()V

    move v0, v1

    .line 1810
    goto :goto_0

    .line 1764
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
