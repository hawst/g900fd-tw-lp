.class public Lcom/sec/android/app/myfiles/utils/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    }
.end annotation


# static fields
.field public static final ACTION_DOCUMENT_SERVICE_INDEX_FINISHED:Ljava/lang/String; = "action.documentservice.index.finished"

.field private static final ANDROID_FOLDER:Ljava/lang/String; = "/Android"

.field private static final ANDROID_FOLDER_OF_NOSCAN:Ljava/lang/String;

.field private static CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB; = null

.field private static final DROPBOX_FOLDER:Ljava/lang/String; = "Dropbox/"

.field public static final EDIT_LIST_OFF_ANIMATION_MS:I = 0x1f4

.field public static final EDIT_LIST_OFF_CORRECTION_VALUE:F = 0.4f

.field public static final EDIT_LIST_ON_ANIMATION_MS:I = 0x190

.field public static final HOVER_DETECT_MS:I = 0x1f4

.field private static final INVALID_CHAR:[Ljava/lang/String;

.field private static final KNOX_MODE_PROPERTY:Ljava/lang/String; = "dev.knoxapp.running"

.field private static final MAX_LENGTH:I = 0x32

.field private static final MAX_QUERY_PATH_COUNT:I = 0x320

.field public static final MEDIA_DB_UPDATE_FINISHED:Ljava/lang/String; = "Media_DB_Update_Finsished"

.field public static final MEDIA_DB_UPDATE_FINISHED_BY_SCANNER:Ljava/lang/String; = "Media_DB_Update_Finsished_by_Scanner"

.field private static final MODULE:Ljava/lang/String; = "Utils"

.field public static final MTP_OBJECT_REMOVED:Ljava/lang/String; = "com.android.MTP.OBJECT_REMOVED"

.field public static final SEARCH_TAB_ANIMATION_MS:I = 0x190

.field private static final SYSTEM_MEMORY_UNIT_GB:J = 0x40000000L

.field public static final USE_SPLIT:Z

.field private static isEnableMultiWindow:Z = false

.field private static isEnableMultiWindowCheck:Z = false

.field public static mAddShortcutSuccess:I = 0x0

.field public static final mBaidu:Ljava/lang/String; = "Baidu/"

.field public static final mBaiduRoot:Ljava/lang/String; = "/Baidu"

.field private static mConfirmDeleteinstance:Landroid/app/Dialog; = null

.field public static mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager; = null

.field public static final mDataFolder:Ljava/lang/String;

.field private static mDialog:Lcom/sec/android/app/myfiles/utils/MyFilesDialog; = null

.field public static mEasyMode:Z = false

.field public static final mEmulatedEntryFolder:Ljava/lang/String; = "/storage/emulated"

.field public static final mExtSdcardRoot:Ljava/lang/String; = "/storage/extSdCard"

.field public static final mInternalName:Ljava/lang/String; = "emulated/0"

.field public static final mInternalRoot:Ljava/lang/String;

.field public static mIsActiveWarningUsingWLAN:Z = false

.field private static mIsMediaScannerScannig:Z = false

.field private static mIsShowing:Z = false

.field public static mKNOXInstalled:Z = false

.field public static mKNOXModeOn:Z = false

.field private static final mKey_sync:Ljava/lang/Object;

.field public static mKnoxInfoForAppBundle:Landroid/os/Bundle; = null

.field private static mLastClickTime:J = 0x0L

.field private static mMountedUsbStorage:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final mRootPath:Ljava/lang/String; = "/storage"

.field public static mSdCardMounted:Z = false

.field public static mSdCardStoragePath:Ljava/lang/String; = null

.field public static final mSdcardName:Ljava/lang/String; = "extSdCard"

.field public static mSecretBoxMounted:Z = false

.field public static final mSecretBoxName:Ljava/lang/String; = "Private"

.field public static mSecretBoxRoot:Ljava/lang/String; = null

.field public static mSecretModeOn:Z = false

.field public static mStorageManager:Landroid/os/storage/StorageManager; = null

.field private static mStorageUsageIsShowing:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/Context;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static mToast:Landroid/widget/Toast; = null

.field public static final mUsbAName:Ljava/lang/String; = "UsbDriveA"

.field public static final mUsbBName:Ljava/lang/String; = "UsbDriveB"

.field public static final mUsbCName:Ljava/lang/String; = "UsbDriveC"

.field public static final mUsbDName:Ljava/lang/String; = "UsbDriveD"

.field public static final mUsbEName:Ljava/lang/String; = "UsbDriveE"

.field public static final mUsbFName:Ljava/lang/String; = "UsbDriveF"

.field public static mUsbMounted:Z = false

.field public static mUsbStorage:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;",
            ">;"
        }
    .end annotation
.end field

.field public static final mUsbStorageA:Ljava/lang/String; = "/storage/UsbDriveA"

.field public static final mUsbStorageB:Ljava/lang/String; = "/storage/UsbDriveB"

.field public static final mUsbStorageC:Ljava/lang/String; = "/storage/UsbDriveC"

.field public static final mUsbStorageD:Ljava/lang/String; = "/storage/UsbDriveD"

.field public static final mUsbStorageE:Ljava/lang/String; = "/storage/UsbDriveE"

.field public static final mUsbStorageF:Ljava/lang/String; = "/storage/UsbDriveF"

.field public static mUsbStorageMount:I = 0x0

.field public static final mUsbStoragePrefix:Ljava/lang/String; = "/storage/UsbDrive"

.field public static needUpdate:Z

.field private static stringSize:I

.field private static tempEnd:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, -0x1

    const/4 v1, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 142
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    .line 144
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mDataFolder:Ljava/lang/String;

    .line 182
    const-string v0, "/storage/Private"

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    .line 187
    sput-object v6, Lcom/sec/android/app/myfiles/utils/Utils;->mStorageManager:Landroid/os/storage/StorageManager;

    .line 189
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    .line 191
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mMountedUsbStorage:Ljava/util/ArrayList;

    .line 193
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardStoragePath:Ljava/lang/String;

    .line 195
    sput-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    .line 197
    sput-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    .line 199
    sput-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    .line 201
    sput-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretModeOn:Z

    .line 203
    sput-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->mEasyMode:Z

    .line 207
    sput-object v6, Lcom/sec/android/app/myfiles/utils/Utils;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    .line 209
    sput-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->mKNOXInstalled:Z

    .line 211
    sput-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->mKNOXModeOn:Z

    .line 213
    sput v7, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorageMount:I

    .line 217
    sput-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->needUpdate:Z

    .line 219
    sput-object v6, Lcom/sec/android/app/myfiles/utils/Utils;->mDialog:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    .line 223
    const-wide/16 v4, 0x0

    sput-wide v4, Lcom/sec/android/app/myfiles/utils/Utils;->mLastClickTime:J

    .line 225
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "\\"

    aput-object v3, v0, v2

    const-string v3, "/"

    aput-object v3, v0, v1

    const/4 v3, 0x2

    const-string v4, ":"

    aput-object v4, v0, v3

    const/4 v3, 0x3

    const-string v4, "*"

    aput-object v4, v0, v3

    const/4 v3, 0x4

    const-string v4, "?"

    aput-object v4, v0, v3

    const/4 v3, 0x5

    const-string v4, "\""

    aput-object v4, v0, v3

    const/4 v3, 0x6

    const-string v4, "<"

    aput-object v4, v0, v3

    const/4 v3, 0x7

    const-string v4, ">"

    aput-object v4, v0, v3

    const/16 v3, 0x8

    const-string v4, "|"

    aput-object v4, v0, v3

    const/16 v3, 0x9

    const-string v4, "\n"

    aput-object v4, v0, v3

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->INVALID_CHAR:[Ljava/lang/String;

    .line 231
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mKey_sync:Ljava/lang/Object;

    .line 233
    sput v2, Lcom/sec/android/app/myfiles/utils/Utils;->mAddShortcutSuccess:I

    .line 237
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/Android"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->ANDROID_FOLDER_OF_NOSCAN:Ljava/lang/String;

    .line 258
    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/Utils;->mIsActiveWarningUsingWLAN:Z

    .line 264
    invoke-static {}, Landroid/util/GeneralUtil;->isPhone()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/Utils;->USE_SPLIT:Z

    .line 271
    sput-object v6, Lcom/sec/android/app/myfiles/utils/Utils;->mKnoxInfoForAppBundle:Landroid/os/Bundle;

    .line 2964
    sput-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->isEnableMultiWindowCheck:Z

    .line 2965
    sput-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->isEnableMultiWindow:Z

    .line 2991
    sput v7, Lcom/sec/android/app/myfiles/utils/Utils;->tempEnd:I

    .line 2992
    sput v2, Lcom/sec/android/app/myfiles/utils/Utils;->stringSize:I

    .line 5015
    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/Utils;->mIsShowing:Z

    .line 5033
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mStorageUsageIsShowing:Ljava/util/HashMap;

    .line 5052
    sput-object v6, Lcom/sec/android/app/myfiles/utils/Utils;->mConfirmDeleteinstance:Landroid/app/Dialog;

    return-void

    :cond_0
    move v0, v2

    .line 264
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3495
    return-void
.end method

.method public static ClearKNOXContainerInstallManager()V
    .locals 1

    .prologue
    .line 2102
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    if-eqz v0, :cond_0

    .line 2104
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    invoke-virtual {v0}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->unbindContainerManager()V

    .line 2106
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    .line 2109
    :cond_0
    return-void
.end method

.method public static InitKNOXContainerInstallManager(Landroid/content/Context;)V
    .locals 0
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    .line 2115
    return-void
.end method

.method public static KNOXFileRelayCancel(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 3
    .param p1, "sndPackageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2217
    .local p0, "filePath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x2

    const-string v1, "Utils"

    const-string v2, "KNOXFileRelayCancel "

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 2218
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    if-eqz v0, :cond_0

    .line 2220
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    invoke-virtual {v0, p0, p1}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->KNOXFileRelayCancel(Ljava/util/List;Ljava/lang/String;)V

    .line 2224
    :cond_0
    return-void
.end method

.method public static KNOXFileRelayRequest(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;II)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "sndPackageName"    # Ljava/lang/String;
    .param p3, "operationType"    # I
    .param p4, "containerId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "II)I"
        }
    .end annotation

    .prologue
    .line 2181
    .local p1, "filePath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x2

    const-string v2, "Utils"

    const-string v3, "KNOXFileRelayRequestk "

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 2183
    const/4 v0, 0x0

    .line 2206
    .local v0, "result":I
    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    if-eqz v1, :cond_0

    .line 2208
    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    invoke-virtual {v1, p1, p2}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->KNOXFileRelayRequest(Ljava/util/List;Ljava/lang/String;)V

    .line 2211
    :cond_0
    return v0
.end method

.method public static RemoveAllPaddingThumbnailMessage()V
    .locals 2

    .prologue
    .line 4534
    sget-object v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;->sThumbnailLoader:Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread;

    iget-object v0, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread;->mBackThreadHandler:Landroid/os/Handler;

    .line 4536
    .local v0, "h":Landroid/os/Handler;
    if-eqz v0, :cond_0

    .line 4537
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 4539
    :cond_0
    return-void
.end method

.method public static UsingFileSystem()Z
    .locals 1

    .prologue
    .line 3845
    const/4 v0, 0x1

    return v0
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 116
    sget v0, Lcom/sec/android/app/myfiles/utils/Utils;->stringSize:I

    return v0
.end method

.method static synthetic access$100()Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$102(Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Landroid/widget/Toast;

    .prologue
    .line 116
    sput-object p0, Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;

    return-object p0
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 116
    sget v0, Lcom/sec/android/app/myfiles/utils/Utils;->tempEnd:I

    return v0
.end method

.method static synthetic access$202(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 116
    sput p0, Lcom/sec/android/app/myfiles/utils/Utils;->tempEnd:I

    return p0
.end method

.method static synthetic access$300()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->INVALID_CHAR:[Ljava/lang/String;

    return-object v0
.end method

.method public static addShortCut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/drawable/Drawable;Z)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "shortcutName"    # Ljava/lang/String;
    .param p3, "type"    # I
    .param p4, "index"    # I
    .param p5, "icon"    # Landroid/graphics/drawable/Drawable;
    .param p6, "moreThanOne"    # Z

    .prologue
    const/4 v7, 0x0

    .line 4087
    const/4 v1, 0x0

    .line 4089
    .local v1, "iconByteArray":[B
    const/4 v3, 0x0

    .line 4090
    .local v3, "isFTP":Z
    const/16 v5, 0xa

    if-eq p3, v5, :cond_0

    const/16 v5, 0x1c

    if-ne p3, v5, :cond_1

    .line 4091
    :cond_0
    const/4 v3, 0x1

    .line 4093
    :cond_1
    if-eqz v3, :cond_2

    .line 4095
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Security;->getInstance()Lcom/sec/android/app/myfiles/utils/Security;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/sec/android/app/myfiles/utils/Security;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 4099
    :cond_2
    const/4 v5, -0x1

    if-ne p4, v5, :cond_3

    .line 4100
    invoke-static {p0, v3}, Lcom/sec/android/app/myfiles/utils/Utils;->getFirstUnsuedIndexFromShottut(Landroid/content/Context;Z)I

    move-result p4

    .line 4103
    :cond_3
    if-eqz p5, :cond_4

    .line 4105
    invoke-static {p5}, Lcom/sec/android/app/myfiles/utils/Utils;->convertDrawableToByteArray(Landroid/graphics/drawable/Drawable;)[B

    move-result-object v1

    .line 4109
    :cond_4
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v5

    if-eqz v5, :cond_7

    const-string v5, "Baidu/"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 4110
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b00fa

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 4136
    :cond_5
    :goto_0
    invoke-static {p0, p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isUserShortcutAdded(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_12

    .line 4137
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 4139
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v5, "shortcut_index"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4140
    const-string v5, "_data"

    invoke-virtual {v0, v5, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4141
    const-string v5, "title"

    invoke-virtual {v0, v5, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4142
    const-string v5, "type"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4143
    const-string v5, "shortcut_drawable"

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 4145
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;->getContentUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v5

    if-eqz v5, :cond_6

    .line 4147
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 4149
    .local v2, "intent":Landroid/content/Intent;
    const-string v5, "com.sec.android.action.SHORTCUT_ACTION"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 4151
    invoke-virtual {p0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 4153
    const-string v5, "Utils"

    const-string v6, "notify the new shortcut was added"

    invoke-static {v7, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 4155
    if-eqz p6, :cond_11

    .line 4156
    sget v5, Lcom/sec/android/app/myfiles/utils/Utils;->mAddShortcutSuccess:I

    add-int/lit8 v5, v5, 0x1

    sput v5, Lcom/sec/android/app/myfiles/utils/Utils;->mAddShortcutSuccess:I

    .line 4176
    .end local v0    # "cv":Landroid/content/ContentValues;
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_6
    :goto_1
    return-void

    .line 4112
    :cond_7
    const-string v5, "Dropbox/"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 4113
    const-string p2, "Dropbox"

    goto :goto_0

    .line 4114
    :cond_8
    const/16 v5, 0x1a

    if-ne p3, v5, :cond_5

    .line 4115
    sget-object v5, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 4116
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b003e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 4117
    :cond_9
    const-string v5, "/storage/extSdCard"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 4118
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b003f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 4119
    :cond_a
    const-string v5, "/storage/UsbDriveA"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 4120
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0042

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 4121
    :cond_b
    const-string v5, "/storage/UsbDriveB"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 4122
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0043

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 4123
    :cond_c
    const-string v5, "/storage/UsbDriveC"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 4124
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0044

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 4125
    :cond_d
    const-string v5, "/storage/UsbDriveD"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 4126
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0045

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 4127
    :cond_e
    const-string v5, "/storage/UsbDriveE"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 4128
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0046

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 4129
    :cond_f
    const-string v5, "/storage/UsbDriveF"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 4130
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0047

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 4132
    :cond_10
    sget-char v5, Ljava/io/File;->separatorChar:C

    invoke-virtual {p1, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {p1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 4158
    .restart local v0    # "cv":Landroid/content/ContentValues;
    .restart local v2    # "intent":Landroid/content/Intent;
    :cond_11
    const v5, 0x7f0b0060

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 4160
    const/4 v5, 0x1

    invoke-static {p0, v5}, Lcom/sec/android/app/myfiles/utils/Utils;->getShortCutCount(Landroid/content/Context;Z)I

    move-result v4

    .line 4162
    .local v4, "shortcutsCount":I
    const/16 v5, 0x32

    if-lt v4, v5, :cond_6

    .line 4164
    const v5, 0x7f0b00d5

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 4171
    .end local v0    # "cv":Landroid/content/ContentValues;
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v4    # "shortcutsCount":I
    :cond_12
    if-nez p6, :cond_6

    .line 4172
    const v5, 0x7f0b0061

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1
.end method

.method public static addShortcutToHome(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "shortcutName"    # Ljava/lang/String;

    .prologue
    .line 4797
    if-nez p0, :cond_0

    .line 4858
    :goto_0
    return-void

    .line 4802
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v3

    .line 4803
    .local v3, "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    invoke-virtual {v3, p1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->saveSetHomescreenPath(Ljava/lang/String;)V

    .line 4805
    new-instance v0, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4807
    .local v0, "addHome":Landroid/content/Intent;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, p0, v4}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 4808
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 4809
    const/high16 v4, 0x4000000

    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 4810
    const-string v4, "from_home_shortcut"

    const/4 v5, 0x1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 4812
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.android.launcher.action.INSTALL_SHORTCUT"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4813
    .local v2, "intent_home":Landroid/content/Intent;
    const-string v4, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v2, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 4816
    const v4, 0x7f020035

    invoke-static {p0, v4}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v1

    .line 4817
    .local v1, "iconResource":Landroid/os/Parcelable;
    const-string v4, "Dropbox/"

    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 4818
    const v4, 0x7f020033

    invoke-static {p0, v4}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v1

    .line 4819
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 4820
    const-string p2, "Dropbox"

    .line 4854
    :cond_1
    :goto_1
    const-string v4, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v2, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4855
    const-string v4, "android.intent.extra.shortcut.ICON_RESOURCE"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 4856
    invoke-virtual {p0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 4822
    :cond_2
    const-string v4, "Baidu/"

    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 4823
    const/4 v4, 0x0

    const-string v5, "Utils"

    const-string v6, "addShortcutToHome Baidu"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 4824
    const v4, 0x7f02004f

    invoke-static {p0, v4}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v1

    .line 4825
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 4826
    const-string p2, "Baidu"

    goto :goto_1

    .line 4828
    :cond_3
    const-string v4, "{\"port\""

    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 4829
    const v4, 0x7f020034

    invoke-static {p0, v4}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v1

    .line 4830
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 4831
    const-string p2, "FTP"

    goto :goto_1

    .line 4834
    :cond_4
    sget-object v4, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 4835
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b003e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 4836
    :cond_5
    const-string v4, "/storage/extSdCard"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 4837
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b003f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 4838
    :cond_6
    const-string v4, "/storage/UsbDriveA"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 4839
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0042

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 4840
    :cond_7
    const-string v4, "/storage/UsbDriveB"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 4841
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0043

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 4842
    :cond_8
    const-string v4, "/storage/UsbDriveC"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 4843
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0044

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 4844
    :cond_9
    const-string v4, "/storage/UsbDriveD"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 4845
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0045

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 4846
    :cond_a
    const-string v4, "/storage/UsbDriveE"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 4847
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0046

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 4848
    :cond_b
    const-string v4, "/storage/UsbDriveF"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 4849
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0047

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 4851
    :cond_c
    sget-char v4, Ljava/io/File;->separatorChar:C

    invoke-virtual {p1, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1
.end method

.method public static checkIfFilesNameIsValid(Ljava/lang/String;)Z
    .locals 1
    .param p0, "fName"    # Ljava/lang/String;

    .prologue
    .line 3821
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/16 v0, 0x5c

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-gez v0, :cond_0

    const/16 v0, 0x2f

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-gez v0, :cond_0

    const/16 v0, 0x3a

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-gez v0, :cond_0

    const/16 v0, 0x3f

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-gez v0, :cond_0

    const/16 v0, 0x3c

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-gez v0, :cond_0

    const/16 v0, 0x3e

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-gez v0, :cond_0

    const/16 v0, 0x22

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-gez v0, :cond_0

    const/16 v0, 0x7c

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ltz v0, :cond_1

    .line 3824
    :cond_0
    const/4 v0, 0x0

    .line 3826
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static checkNotVewingItem(Ljava/io/File;)Z
    .locals 4
    .param p0, "file"    # Ljava/io/File;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2771
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 2773
    .local v0, "path":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2804
    :cond_0
    :goto_0
    return v1

    .line 2778
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isHiddenItem(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isSystemFolder(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isOEMHiddenItem(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2782
    sget-object v3, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardStoragePath:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    sget-boolean v3, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v3, :cond_0

    .line 2788
    :cond_2
    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isUsbStorageRootFolder(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2790
    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->checkUSBStorage(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    move v1, v2

    .line 2792
    goto :goto_0

    .line 2796
    :cond_3
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    .line 2801
    goto :goto_0
.end method

.method public static checkSameStorage(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p0, "filea"    # Ljava/lang/String;
    .param p1, "fileb"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 3809
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->getStorage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->getStorage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 3815
    :cond_0
    :goto_0
    return v0

    .line 3812
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->getStorage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->getStorage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3813
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static checkShareMIMEType(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "fileMIMEtype"    # Ljava/lang/String;
    .param p1, "overallMIMEtype"    # Ljava/lang/String;

    .prologue
    .line 275
    const/4 v0, 0x0

    .line 277
    .local v0, "MIMECategory":Ljava/lang/String;
    const/4 v1, 0x0

    .line 280
    .local v1, "overallMIMEcategory":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 282
    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 284
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->getMIMEcategory(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 286
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->getMIMEcategory(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 288
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 290
    const-string p0, "application/*"

    .line 299
    .end local p0    # "fileMIMEtype":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .restart local p0    # "fileMIMEtype":Ljava/lang/String;
    :cond_1
    move-object p0, v1

    .line 294
    goto :goto_0
.end method

.method public static checkUSBStorage(Ljava/lang/String;)Z
    .locals 3
    .param p0, "folder"    # Ljava/lang/String;

    .prologue
    .line 1668
    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_2

    .line 1670
    sget-object v2, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    monitor-enter v2

    .line 1672
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1674
    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->isMounted()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1676
    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->getStoragePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1678
    sput v0, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorageMount:I

    .line 1680
    const/4 v1, 0x1

    monitor-exit v2

    .line 1687
    .end local v0    # "i":I
    :goto_1
    return v1

    .line 1672
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1684
    :cond_1
    monitor-exit v2

    .line 1687
    .end local v0    # "i":I
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 1684
    .restart local v0    # "i":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static convertByteArrayToDrawable([B)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p0, "bytea"    # [B

    .prologue
    .line 4256
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 4261
    :goto_0
    return-object v0

    .line 4257
    :cond_0
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 4258
    .local v1, "imageStream":Ljava/io/ByteArrayInputStream;
    const/4 p0, 0x0

    .line 4259
    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 4260
    .local v2, "theImage":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 4261
    .local v0, "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    goto :goto_0
.end method

.method public static convertDrawableToByteArray(Landroid/graphics/drawable/Drawable;)[B
    .locals 5
    .param p0, "icon"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 4247
    check-cast p0, Landroid/graphics/drawable/BitmapDrawable;

    .end local p0    # "icon":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 4248
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 4249
    .local v2, "stream":Ljava/io/ByteArrayOutputStream;
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    invoke-virtual {v0, v3, v4, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 4250
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 4251
    .local v1, "iconByteArray":[B
    return-object v1
.end method

.method public static deleteDropboxThumbnailCache(Ljava/io/File;)V
    .locals 9
    .param p0, "thumbnailCacheFolder"    # Ljava/io/File;

    .prologue
    .line 3698
    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v6

    if-nez v6, :cond_0

    .line 3701
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    .line 3703
    .local v5, "subFiles":[Ljava/io/File;
    if-eqz v5, :cond_0

    array-length v6, v5

    if-nez v6, :cond_1

    .line 3723
    .end local v5    # "subFiles":[Ljava/io/File;
    :cond_0
    :goto_0
    return-void

    .line 3709
    .restart local v5    # "subFiles":[Ljava/io/File;
    :cond_1
    move-object v0, v5

    .local v0, "arr$":[Ljava/io/File;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v4, v0, v2

    .line 3711
    .local v4, "sf":Ljava/io/File;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteDropboxThumbnailCache(Ljava/io/File;)V

    .line 3709
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3715
    .end local v4    # "sf":Ljava/io/File;
    :cond_2
    invoke-virtual {p0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3719
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v5    # "subFiles":[Ljava/io/File;
    :catch_0
    move-exception v1

    .line 3721
    .local v1, "ex":Ljava/lang/SecurityException;
    const/4 v6, 0x0

    const-string v7, "Utils"

    const-string v8, "DropboxCache delete() - Dropbox Thumbnail Cache delete is failed"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static deleteFTPShortcuts(Landroid/content/Context;Ljava/util/ArrayList;)I
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 4317
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4319
    .local v0, "arg0":Ljava/lang/StringBuilder;
    const-string v6, ""

    .line 4320
    .local v6, "preFix":Ljava/lang/String;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 4321
    .local v7, "whareList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .local v5, "item":Ljava/lang/Object;
    move-object v2, v5

    .line 4322
    check-cast v2, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .line 4323
    .local v2, "ftpItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "_data"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " =  ? "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4325
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4327
    const-string v6, " OR "

    .line 4328
    goto :goto_0

    .line 4329
    .end local v2    # "ftpItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    .end local v5    # "item":Ljava/lang/Object;
    :cond_0
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v9

    new-array v9, v9, [Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/String;

    .line 4330
    .local v8, "wherArgs":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;->getContentUri()Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 4331
    .local v1, "deletedRow":I
    if-lez v1, :cond_1

    .line 4332
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 4334
    .local v4, "intent":Landroid/content/Intent;
    const-string v9, "com.sec.android.action.SHORTCUT_ACTION"

    invoke-virtual {v4, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 4336
    invoke-virtual {p0, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 4338
    const/4 v9, 0x0

    const-string v10, "Utils"

    const-string v11, "notify the shortcuts were deleted"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 4341
    .end local v4    # "intent":Landroid/content/Intent;
    :cond_1
    return v1
.end method

.method public static deleteFileShortcutFromHome(Landroid/content/Context;Ljava/lang/String;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 4925
    if-nez p0, :cond_0

    .line 4958
    :goto_0
    return-void

    .line 4930
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v4

    .line 4931
    .local v4, "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    invoke-virtual {v4, p1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->deleteSetHomescreenPath(Ljava/lang/String;)V

    .line 4933
    const/4 v5, 0x0

    .line 4934
    .local v5, "shortcutIcon":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 4935
    .local v3, "returnIntent":Landroid/content/Intent;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090012

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v1, v7

    .line 4936
    .local v1, "iconWidthHeight":I
    sget-char v7, Ljava/io/File;->separatorChar:C

    invoke-virtual {p1, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 4937
    .local v6, "shortcutName":Ljava/lang/String;
    const-string v7, "android.intent.action.VIEW"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 4938
    const/4 v2, 0x0

    .line 4939
    .local v2, "mimeType":Ljava/lang/String;
    invoke-static {p0, p1}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isOMADrmFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 4940
    invoke-static {p0, p1}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->getRealMimeTypeOfDRM(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4945
    :goto_1
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v3, v7, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 4947
    const/4 v7, 0x1

    invoke-static {p0, v1, v1, p1, v7}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getHomeShortcutBitmap(Landroid/content/Context;IILjava/lang/String;Z)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 4949
    new-instance v0, Landroid/content/Intent;

    const-string v7, "com.android.launcher.action.UNINSTALL_SHORTCUT"

    invoke-direct {v0, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4951
    .local v0, "addShortcutToHomescreenIntent":Landroid/content/Intent;
    const-string v7, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v0, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 4953
    const-string v7, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v0, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4955
    const-string v7, "android.intent.extra.shortcut.ICON"

    invoke-virtual {v0, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 4957
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 4942
    .end local v0    # "addShortcutToHomescreenIntent":Landroid/content/Intent;
    :cond_1
    invoke-static {p1}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public static deleteShortcut(Landroid/content/Context;Ljava/lang/String;I)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "shortcutType"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 4346
    const-string v5, "_data = ?"

    .line 4348
    .local v5, "where":Ljava/lang/String;
    const/4 v6, 0x0

    .line 4350
    .local v6, "whereArgs":[Ljava/lang/String;
    sparse-switch p2, :sswitch_data_0

    .line 4360
    new-array v6, v7, [Ljava/lang/String;

    .end local v6    # "whereArgs":[Ljava/lang/String;
    aput-object p1, v6, v9

    .line 4366
    .restart local v6    # "whereArgs":[Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;->getContentUri()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v7, v8, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 4367
    .local v1, "deletedRow":I
    if-lez v1, :cond_1

    .line 4368
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 4370
    .local v3, "intent":Landroid/content/Intent;
    const-string v7, "com.sec.android.action.SHORTCUT_ACTION"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 4372
    invoke-virtual {p0, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 4374
    const-string v7, "Utils"

    const-string v8, "notify the shortcut was deleted"

    invoke-static {v9, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 4377
    const/16 v7, 0x1c

    if-eq p2, v7, :cond_0

    const/16 v7, 0xa

    if-ne p2, v7, :cond_1

    .line 4379
    :cond_0
    new-instance v4, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-direct {v4, p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Ljava/lang/String;)V

    .line 4381
    .local v4, "params":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    invoke-static {p0, v4}, Lcom/sec/android/app/myfiles/utils/Utils;->doesAnyOtherFtpShortcutExists(Landroid/content/Context;Lcom/sec/android/app/myfiles/ftp/FTPParams;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 4383
    new-instance v0, Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    .line 4387
    .local v0, "cacheDB":Lcom/sec/android/app/myfiles/db/CacheDB;
    :try_start_0
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/sec/android/app/myfiles/db/CacheDB;->deleteFtpCachedData(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4400
    .end local v0    # "cacheDB":Lcom/sec/android/app/myfiles/db/CacheDB;
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "params":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    :cond_1
    :goto_1
    return v1

    .line 4354
    .end local v1    # "deletedRow":I
    :sswitch_0
    new-array v6, v7, [Ljava/lang/String;

    .end local v6    # "whereArgs":[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Security;->getInstance()Lcom/sec/android/app/myfiles/utils/Security;

    move-result-object v7

    invoke-virtual {v7, p1}, Lcom/sec/android/app/myfiles/utils/Security;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    .line 4356
    .restart local v6    # "whereArgs":[Ljava/lang/String;
    goto :goto_0

    .line 4389
    .restart local v0    # "cacheDB":Lcom/sec/android/app/myfiles/db/CacheDB;
    .restart local v1    # "deletedRow":I
    .restart local v3    # "intent":Landroid/content/Intent;
    .restart local v4    # "params":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    :catch_0
    move-exception v2

    .line 4391
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    const-string v7, "Utils"

    const-string v8, "cannot create FTPParams object out of the path"

    invoke-static {v9, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 4350
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x1c -> :sswitch_0
    .end sparse-switch
.end method

.method public static deleteShortcutFromHome(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "shortcutName"    # Ljava/lang/String;

    .prologue
    .line 4862
    if-nez p0, :cond_0

    .line 4921
    :goto_0
    return-void

    .line 4867
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v3

    .line 4868
    .local v3, "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    invoke-virtual {v3, p1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->deleteSetHomescreenPath(Ljava/lang/String;)V

    .line 4870
    new-instance v0, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4872
    .local v0, "addHome":Landroid/content/Intent;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, p0, v4}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 4873
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 4875
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.android.launcher.action.UNINSTALL_SHORTCUT"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4876
    .local v2, "intent_home":Landroid/content/Intent;
    const-string v4, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v2, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 4879
    const v4, 0x7f020032

    invoke-static {p0, v4}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v1

    .line 4880
    .local v1, "iconResource":Landroid/os/Parcelable;
    const-string v4, "Dropbox/"

    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 4881
    const v4, 0x7f020050

    invoke-static {p0, v4}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v1

    .line 4882
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 4883
    const-string p2, "Dropbox"

    .line 4917
    :cond_1
    :goto_1
    const-string v4, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v2, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4918
    const-string v4, "android.intent.extra.shortcut.ICON_RESOURCE"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 4919
    invoke-virtual {p0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 4885
    :cond_2
    const-string v4, "Baidu/"

    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 4886
    const/4 v4, 0x0

    const-string v5, "Utils"

    const-string v6, "deleteShortcutFromHome Baidu"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 4887
    const v4, 0x7f02004f

    invoke-static {p0, v4}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v1

    .line 4888
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 4889
    const-string p2, "Baidu"

    goto :goto_1

    .line 4891
    :cond_3
    const-string v4, "{\"port\""

    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 4892
    const v4, 0x7f020051

    invoke-static {p0, v4}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v1

    .line 4893
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 4894
    const-string p2, "FTP"

    goto :goto_1

    .line 4897
    :cond_4
    sget-object v4, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 4898
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b003e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 4899
    :cond_5
    const-string v4, "/storage/extSdCard"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 4900
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b003f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 4901
    :cond_6
    const-string v4, "/storage/UsbDriveA"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 4902
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0042

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 4903
    :cond_7
    const-string v4, "/storage/UsbDriveB"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 4904
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0043

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 4905
    :cond_8
    const-string v4, "/storage/UsbDriveC"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 4906
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0044

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 4907
    :cond_9
    const-string v4, "/storage/UsbDriveD"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 4908
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0045

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 4909
    :cond_a
    const-string v4, "/storage/UsbDriveE"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 4910
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0046

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 4911
    :cond_b
    const-string v4, "/storage/UsbDriveF"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 4912
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0047

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 4914
    :cond_c
    sget-char v4, Ljava/io/File;->separatorChar:C

    invoke-virtual {p1, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1
.end method

.method public static deleteShortcuts(Landroid/content/Context;Ljava/util/ArrayList;)I
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 4266
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4269
    .local v0, "arg0":Ljava/lang/StringBuilder;
    const-string v6, ""

    .line 4270
    .local v6, "preFix":Ljava/lang/String;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 4271
    .local v8, "whareList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 4272
    .local v5, "item":Ljava/lang/Object;
    instance-of v10, v5, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    if-eqz v10, :cond_0

    move-object v7, v5

    .line 4273
    check-cast v7, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 4274
    .local v7, "shortcutItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "_data"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " =  ? "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4276
    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    .line 4288
    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4299
    .end local v7    # "shortcutItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :goto_1
    const-string v6, " OR "

    .line 4300
    goto :goto_0

    .line 4281
    .restart local v7    # "shortcutItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :sswitch_0
    const-string v10, " AND type =  ? "

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4282
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Security;->getInstance()Lcom/sec/android/app/myfiles/utils/Security;

    move-result-object v10

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/android/app/myfiles/utils/Security;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4283
    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .end local v7    # "shortcutItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_0
    move-object v1, v5

    .line 4295
    check-cast v1, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .line 4296
    .local v1, "browserItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "_data"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " =  ? "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4297
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 4301
    .end local v1    # "browserItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    .end local v5    # "item":Ljava/lang/Object;
    :cond_1
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v10, v10, [Ljava/lang/String;

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/String;

    .line 4302
    .local v9, "wherArgs":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;->getContentUri()Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 4303
    .local v2, "deletedRow":I
    if-lez v2, :cond_2

    .line 4304
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 4306
    .local v4, "intent":Landroid/content/Intent;
    const-string v10, "com.sec.android.action.SHORTCUT_ACTION"

    invoke-virtual {v4, v10}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 4308
    invoke-virtual {p0, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 4310
    const/4 v10, 0x0

    const-string v11, "Utils"

    const-string v12, "notify the shortcuts were deleted"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 4313
    .end local v4    # "intent":Landroid/content/Intent;
    :cond_2
    return v2

    .line 4276
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x1c -> :sswitch_0
    .end sparse-switch
.end method

.method public static deleteThumbnail(Ljava/lang/String;)V
    .locals 3
    .param p0, "serverPath"    # Ljava/lang/String;

    .prologue
    .line 3676
    if-nez p0, :cond_0

    .line 3678
    const-string p0, ""

    .line 3681
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->getFileThumbnailFolderName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3683
    .local v1, "thumbnailCacheFile":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3685
    .local v0, "thumbFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3687
    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteDropboxThumbnailCache(Ljava/io/File;)V

    .line 3689
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->removeThumbnailCache(Ljava/lang/String;)V

    .line 3691
    :cond_1
    return-void
.end method

.method public static doesAnyOtherFtpShortcutExists(Landroid/content/Context;Lcom/sec/android/app/myfiles/ftp/FTPParams;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .prologue
    .line 4437
    if-eqz p0, :cond_2

    if-eqz p1, :cond_2

    .line 4439
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->getFTPShortcutPaths(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 4441
    .local v0, "ftpParamPaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v5, 0x0

    .line 4443
    .local v5, "shortcutsCount":I
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v3

    .line 4445
    .local v3, "param":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 4447
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 4449
    .local v4, "path":Ljava/lang/String;
    new-instance v2, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-direct {v2, v4}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Ljava/lang/String;)V

    .line 4451
    .local v2, "item":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 4453
    add-int/lit8 v5, v5, 0x1

    .line 4457
    :cond_1
    if-lez v5, :cond_0

    .line 4459
    const/4 v6, 0x1

    .line 4469
    .end local v0    # "ftpParamPaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "item":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .end local v3    # "param":Ljava/lang/String;
    .end local v4    # "path":Ljava/lang/String;
    .end local v5    # "shortcutsCount":I
    :goto_0
    return v6

    :cond_2
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static getAllFilesSize(Landroid/content/Context;I)J
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "storage"    # I

    .prologue
    .line 350
    new-instance v5, Ljava/io/File;

    sget-object v6, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->getTotalSize(Ljava/io/File;)J

    move-result-wide v2

    .line 352
    .local v2, "totalSize":J
    sget-boolean v5, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v5, :cond_0

    .line 354
    new-instance v5, Ljava/io/File;

    const-string v6, "/storage/extSdCard"

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->getTotalSize(Ljava/io/File;)J

    move-result-wide v6

    add-long/2addr v2, v6

    .line 357
    :cond_0
    sget-boolean v5, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-eqz v5, :cond_1

    .line 359
    new-instance v5, Ljava/io/File;

    sget-object v6, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->getTotalSize(Ljava/io/File;)J

    move-result-wide v6

    add-long/2addr v2, v6

    .line 362
    :cond_1
    sget-boolean v5, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-eqz v5, :cond_2

    .line 364
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getMountedUsbStorage()Ljava/util/ArrayList;

    move-result-object v1

    .line 366
    .local v1, "mountedUsbStorages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;

    .line 368
    .local v4, "usbStorage":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    new-instance v5, Ljava/io/File;

    iget-object v6, v4, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->mStoragePath:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->getTotalSize(Ljava/io/File;)J

    move-result-wide v6

    add-long/2addr v2, v6

    .line 369
    goto :goto_0

    .line 372
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "mountedUsbStorages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;>;"
    .end local v4    # "usbStorage":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    :cond_2
    return-wide v2
.end method

.method public static getAllFilesSizeUsingDB(Landroid/content/Context;I)J
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "storage"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v10, 0x0

    .line 392
    if-nez p0, :cond_1

    .line 394
    const-wide/16 v8, 0x0

    .line 470
    :cond_0
    :goto_0
    return-wide v8

    .line 397
    :cond_1
    const-wide/16 v8, 0x0

    .line 399
    .local v8, "totalSize":J
    const/4 v3, 0x0

    .line 401
    .local v3, "where":Ljava/lang/String;
    const/4 v4, 0x0

    .line 403
    .local v4, "whereArgs":[Ljava/lang/String;
    sparse-switch p1, :sswitch_data_0

    .line 432
    :goto_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "external"

    invoke-static {v1}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    const-string v5, "SUM(_size)"

    aput-object v5, v2, v10

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 438
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_3

    .line 440
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 455
    invoke-interface {v7, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 458
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 462
    :cond_3
    if-nez p1, :cond_0

    .line 465
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    const-string v1, "/Android"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 467
    .local v6, "android_path":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->getTotalSize(Ljava/io/File;)J

    move-result-wide v0

    add-long/2addr v8, v0

    goto :goto_0

    .line 407
    .end local v6    # "android_path":Ljava/lang/String;
    .end local v7    # "cursor":Landroid/database/Cursor;
    :sswitch_0
    const-string v3, "_data LIKE ?"

    .line 408
    new-array v4, v2, [Ljava/lang/String;

    .end local v4    # "whereArgs":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v10

    .line 409
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 412
    :sswitch_1
    const-string v3, "_data LIKE ?"

    .line 413
    new-array v4, v2, [Ljava/lang/String;

    .end local v4    # "whereArgs":[Ljava/lang/String;
    const-string v0, "/storage/extSdCard%"

    aput-object v0, v4, v10

    .line 414
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 417
    :sswitch_2
    const-string v3, "_data LIKE ?"

    .line 418
    new-array v4, v2, [Ljava/lang/String;

    .end local v4    # "whereArgs":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v10

    .line 419
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 427
    :sswitch_3
    const-string v3, "_data LIKE ?"

    .line 428
    new-array v4, v2, [Ljava/lang/String;

    .end local v4    # "whereArgs":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->getUsbStoragePath(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v10

    .restart local v4    # "whereArgs":[Ljava/lang/String;
    goto/16 :goto_1

    .line 403
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0x101 -> :sswitch_0
        0x202 -> :sswitch_1
        0x603 -> :sswitch_3
        0x604 -> :sswitch_3
        0x605 -> :sswitch_3
        0x606 -> :sswitch_3
        0x607 -> :sswitch_3
        0x608 -> :sswitch_3
    .end sparse-switch
.end method

.method public static getAutoRename(Ljava/lang/String;IZ)Ljava/lang/String;
    .locals 7
    .param p0, "fullName"    # Ljava/lang/String;
    .param p1, "postfixNum"    # I
    .param p2, "isFile"    # Z

    .prologue
    const/16 v6, 0x29

    .line 4707
    const/4 v3, 0x0

    .line 4709
    .local v3, "resultStr":Ljava/lang/String;
    if-eqz p2, :cond_1

    .line 4711
    const/16 v5, 0x2e

    invoke-virtual {p0, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 4713
    .local v1, "lastDot":I
    const/4 v5, -0x1

    if-eq v1, v5, :cond_0

    .line 4715
    const/4 v5, 0x0

    invoke-virtual {p0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 4717
    .local v2, "name":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {p0, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 4719
    .local v0, "ext":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 4721
    .local v4, "sb":Ljava/lang/StringBuffer;
    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 4723
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4743
    .end local v0    # "ext":Ljava/lang/String;
    .end local v1    # "lastDot":I
    .end local v2    # "name":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 4727
    .end local v4    # "sb":Ljava/lang/StringBuffer;
    .restart local v1    # "lastDot":I
    :cond_0
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4, p0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 4729
    .restart local v4    # "sb":Ljava/lang/StringBuffer;
    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 4731
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 4736
    .end local v1    # "lastDot":I
    .end local v4    # "sb":Ljava/lang/StringBuffer;
    :cond_1
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4, p0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 4738
    .restart local v4    # "sb":Ljava/lang/StringBuffer;
    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 4740
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static getAvailableSpace(Ljava/lang/String;)J
    .locals 8
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 2460
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->getStorage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2462
    .local v3, "storageDirectory":Ljava/lang/String;
    const-wide/16 v0, 0x0

    .line 2468
    .local v0, "remainingSpace":J
    const/4 v2, 0x0

    .line 2470
    .local v2, "stat":Landroid/os/StatFs;
    if-eqz v3, :cond_0

    .line 2474
    new-instance v2, Landroid/os/StatFs;

    .end local v2    # "stat":Landroid/os/StatFs;
    invoke-direct {v2, v3}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 2486
    .restart local v2    # "stat":Landroid/os/StatFs;
    :goto_0
    invoke-virtual {v2}, Landroid/os/StatFs;->getAvailableBytes()J

    move-result-wide v0

    .line 2489
    const/4 v4, 0x1

    const-string v5, "Utils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getAvailableSpace() - remainingSpace : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 2496
    return-wide v0

    .line 2480
    :cond_0
    new-instance v2, Landroid/os/StatFs;

    .end local v2    # "stat":Landroid/os/StatFs;
    invoke-direct {v2, p0}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .restart local v2    # "stat":Landroid/os/StatFs;
    goto :goto_0
.end method

.method public static getCategoryId(Ljava/lang/String;)I
    .locals 1
    .param p0, "category"    # Ljava/lang/String;

    .prologue
    .line 1563
    const-string v0, "images"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1564
    const/4 v0, 0x2

    .line 1578
    :goto_0
    return v0

    .line 1565
    :cond_0
    const-string v0, "videos"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1566
    const/4 v0, 0x3

    goto :goto_0

    .line 1567
    :cond_1
    const-string v0, "music"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1568
    const/4 v0, 0x4

    goto :goto_0

    .line 1569
    :cond_2
    const-string v0, "documents"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1570
    const/4 v0, 0x5

    goto :goto_0

    .line 1571
    :cond_3
    const-string v0, "downloaded_apps"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1572
    const/4 v0, 0x6

    goto :goto_0

    .line 1573
    :cond_4
    const-string v0, "recently_files"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1574
    const/4 v0, 0x7

    goto :goto_0

    .line 1575
    :cond_5
    const-string v0, "history"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1576
    const/16 v0, 0x28

    goto :goto_0

    .line 1578
    :cond_6
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static getCategoryTag(I)Ljava/lang/String;
    .locals 1
    .param p0, "categoryId"    # I

    .prologue
    .line 1550
    sparse-switch p0, :sswitch_data_0

    .line 1559
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1551
    :sswitch_0
    const-string v0, "images"

    goto :goto_0

    .line 1552
    :sswitch_1
    const-string v0, "videos"

    goto :goto_0

    .line 1553
    :sswitch_2
    const-string v0, "music"

    goto :goto_0

    .line 1554
    :sswitch_3
    const-string v0, "documents"

    goto :goto_0

    .line 1555
    :sswitch_4
    const-string v0, "downloaded_apps"

    goto :goto_0

    .line 1556
    :sswitch_5
    const-string v0, "recently_files"

    goto :goto_0

    .line 1557
    :sswitch_6
    const-string v0, "history"

    goto :goto_0

    .line 1550
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_1
        0x4 -> :sswitch_2
        0x5 -> :sswitch_3
        0x6 -> :sswitch_4
        0x7 -> :sswitch_5
        0x28 -> :sswitch_6
    .end sparse-switch
.end method

.method public static getChildFileCount(Landroid/content/Context;Ljava/lang/String;)I
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v13, 0x1

    const/4 v5, 0x0

    const/4 v7, -0x1

    .line 2594
    const/4 v6, 0x0

    .line 2596
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v11, 0x0

    .line 2598
    .local v11, "where":Ljava/lang/String;
    const/4 v12, 0x0

    .line 2600
    .local v12, "whereArgs":[Ljava/lang/String;
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowHiddenFileStatus()Z

    move-result v10

    .line 2602
    .local v10, "settingsHiddenFileStatus":Z
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isAbsoluteRootFolder(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v4, v12

    .end local v12    # "whereArgs":[Ljava/lang/String;
    .local v4, "whereArgs":[Ljava/lang/String;
    move-object v3, v11

    .line 2684
    .end local v11    # "where":Ljava/lang/String;
    .local v3, "where":Ljava/lang/String;
    :cond_0
    :goto_0
    return v7

    .line 2606
    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .restart local v11    # "where":Ljava/lang/String;
    .restart local v12    # "whereArgs":[Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->UsingFileSystem()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2608
    invoke-static {p1, v10}, Lcom/sec/android/app/myfiles/utils/Utils;->getChildFileCount(Ljava/lang/String;Z)I

    move-result v7

    move-object v4, v12

    .end local v12    # "whereArgs":[Ljava/lang/String;
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    move-object v3, v11

    .end local v11    # "where":Ljava/lang/String;
    .restart local v3    # "where":Ljava/lang/String;
    goto :goto_0

    .line 2610
    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .restart local v11    # "where":Ljava/lang/String;
    .restart local v12    # "whereArgs":[Ljava/lang/String;
    :cond_2
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isInternalRootFolder(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2612
    const-string v3, "_data LIKE ? AND parent=0"

    .line 2614
    .end local v11    # "where":Ljava/lang/String;
    .restart local v3    # "where":Ljava/lang/String;
    new-array v4, v13, [Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 2669
    .end local v12    # "whereArgs":[Ljava/lang/String;
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    :goto_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "external"

    invoke-static {v1}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2675
    if-eqz v6, :cond_0

    .line 2677
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 2679
    .local v7, "result":I
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2616
    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .end local v7    # "result":I
    .restart local v11    # "where":Ljava/lang/String;
    .restart local v12    # "whereArgs":[Ljava/lang/String;
    :cond_3
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2618
    const-string v3, "_data LIKE ? AND parent=0"

    .line 2620
    .end local v11    # "where":Ljava/lang/String;
    .restart local v3    # "where":Ljava/lang/String;
    new-array v4, v13, [Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .end local v12    # "whereArgs":[Ljava/lang/String;
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 2622
    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .restart local v11    # "where":Ljava/lang/String;
    .restart local v12    # "whereArgs":[Ljava/lang/String;
    :cond_4
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isExternalRootFolder(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2624
    const-string v3, "_data LIKE ? AND parent=0"

    .line 2626
    .end local v11    # "where":Ljava/lang/String;
    .restart local v3    # "where":Ljava/lang/String;
    new-array v4, v13, [Ljava/lang/String;

    const-string v0, "/storage/extSdCard%"

    aput-object v0, v4, v5

    .end local v12    # "whereArgs":[Ljava/lang/String;
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 2631
    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .restart local v11    # "where":Ljava/lang/String;
    .restart local v12    # "whereArgs":[Ljava/lang/String;
    :cond_5
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isAndroidFolder(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2633
    invoke-static {p1, v10}, Lcom/sec/android/app/myfiles/utils/Utils;->getChildFileCount(Ljava/lang/String;Z)I

    move-result v7

    move-object v4, v12

    .end local v12    # "whereArgs":[Ljava/lang/String;
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    move-object v3, v11

    .end local v11    # "where":Ljava/lang/String;
    .restart local v3    # "where":Ljava/lang/String;
    goto/16 :goto_0

    .line 2635
    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .restart local v11    # "where":Ljava/lang/String;
    .restart local v12    # "whereArgs":[Ljava/lang/String;
    :cond_6
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isUsbStorageFolder(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2637
    invoke-static {p1, v10}, Lcom/sec/android/app/myfiles/utils/Utils;->getChildFileCount(Ljava/lang/String;Z)I

    move-result v7

    move-object v4, v12

    .end local v12    # "whereArgs":[Ljava/lang/String;
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    move-object v3, v11

    .end local v11    # "where":Ljava/lang/String;
    .restart local v3    # "where":Ljava/lang/String;
    goto/16 :goto_0

    .line 2640
    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .restart local v11    # "where":Ljava/lang/String;
    .restart local v12    # "whereArgs":[Ljava/lang/String;
    :cond_7
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "external"

    invoke-static {v1}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v3, "_data LIKE ?"

    new-array v4, v13, [Ljava/lang/String;

    aput-object p1, v4, v5

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2646
    if-eqz v6, :cond_9

    .line 2648
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2650
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 2652
    .local v8, "id":J
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "parent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2661
    .end local v11    # "where":Ljava/lang/String;
    .restart local v3    # "where":Ljava/lang/String;
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v4, v12

    .end local v12    # "whereArgs":[Ljava/lang/String;
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    goto/16 :goto_1

    .line 2656
    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .end local v8    # "id":J
    .restart local v11    # "where":Ljava/lang/String;
    .restart local v12    # "whereArgs":[Ljava/lang/String;
    :cond_8
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v4, v12

    .end local v12    # "whereArgs":[Ljava/lang/String;
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    move-object v3, v11

    .line 2658
    .end local v11    # "where":Ljava/lang/String;
    .restart local v3    # "where":Ljava/lang/String;
    goto/16 :goto_0

    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .restart local v11    # "where":Ljava/lang/String;
    .restart local v12    # "whereArgs":[Ljava/lang/String;
    :cond_9
    move-object v4, v12

    .end local v12    # "whereArgs":[Ljava/lang/String;
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    move-object v3, v11

    .line 2665
    .end local v11    # "where":Ljava/lang/String;
    .restart local v3    # "where":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method private static getChildFileCount(Ljava/lang/String;Z)I
    .locals 3
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "settingsHiddenFileStatus"    # Z

    .prologue
    .line 2704
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2705
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2706
    const/4 v1, 0x0

    .line 2707
    .local v1, "files":[Ljava/io/File;
    new-instance v2, Lcom/sec/android/app/myfiles/utils/Utils$2;

    invoke-direct {v2, p1}, Lcom/sec/android/app/myfiles/utils/Utils$2;-><init>(Z)V

    invoke-virtual {v0, v2}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v1

    .line 2722
    if-eqz v1, :cond_0

    .line 2723
    array-length v2, v1

    .line 2725
    .end local v1    # "files":[Ljava/io/File;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getContactName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 16
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "contactId"    # Ljava/lang/String;

    .prologue
    .line 5074
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 5075
    :cond_0
    const/4 v9, 0x0

    .line 5129
    :goto_0
    return-object v9

    .line 5077
    :cond_1
    const-string v3, "content://com.sec.rshare/address"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static/range {p1 .. p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    invoke-static {v3, v14, v15}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 5078
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 5079
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v3, 0x1

    new-array v2, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "address"

    aput-object v4, v2, v3

    .line 5080
    .local v2, "projection":[Ljava/lang/String;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 5081
    .local v10, "cs":Landroid/database/Cursor;
    if-nez v10, :cond_2

    .line 5082
    const/4 v9, 0x0

    goto :goto_0

    .line 5084
    :cond_2
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-nez v3, :cond_3

    .line 5085
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 5086
    const/4 v9, 0x0

    goto :goto_0

    .line 5088
    :cond_3
    const/4 v3, 0x0

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 5089
    .local v9, "address":Ljava/lang/String;
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 5090
    if-eqz v9, :cond_4

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_5

    .line 5091
    :cond_4
    const/4 v9, 0x0

    goto :goto_0

    .line 5093
    :cond_5
    const/4 v3, 0x5

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "data1"

    aput-object v4, v5, v3

    const/4 v3, 0x2

    const-string v4, "display_name"

    aput-object v4, v5, v3

    const/4 v3, 0x3

    const-string v4, "contact_id"

    aput-object v4, v5, v3

    const/4 v3, 0x4

    const-string v4, "data4"

    aput-object v4, v5, v3

    .line 5100
    .local v5, "CONTACT_PROJECTION":[Ljava/lang/String;
    invoke-static {v9}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 5103
    invoke-static {v9}, Landroid/telephony/PhoneNumberUtils;->toCallerIDMinMatch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 5104
    .local v11, "minMatch":Ljava/lang/String;
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    .line 5105
    .local v13, "numberLen":Ljava/lang/String;
    const-string v3, "+"

    invoke-virtual {v9, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 5106
    const-string v6, " Data._ID IN  (SELECT DISTINCT lookup.data_id  FROM  (SELECT data_id, normalized_number, length(normalized_number) as len  FROM phone_lookup  WHERE min_match = ?) AS lookup  WHERE lookup.normalized_number = ? OR (lookup.len <= ? AND  substr(?, ? - lookup.len + 1) = lookup.normalized_number))"

    .line 5109
    .local v6, "selection":Ljava/lang/String;
    const/4 v3, 0x5

    new-array v7, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v11, v7, v3

    const/4 v3, 0x1

    aput-object v9, v7, v3

    const/4 v3, 0x2

    aput-object v13, v7, v3

    const/4 v3, 0x3

    aput-object v9, v7, v3

    const/4 v3, 0x4

    aput-object v13, v7, v3

    .line 5116
    .local v7, "args":[Ljava/lang/String;
    :goto_1
    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    move-object v3, v0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 5117
    if-nez v10, :cond_7

    .line 5118
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 5111
    .end local v6    # "selection":Ljava/lang/String;
    .end local v7    # "args":[Ljava/lang/String;
    :cond_6
    const-string v6, " Data._ID IN  (SELECT DISTINCT lookup.data_id  FROM  (SELECT data_id, normalized_number, length(normalized_number) as len  FROM phone_lookup  WHERE min_match = ?) AS lookup  WHERE  (lookup.len <= ? AND  substr(?, ? - lookup.len + 1) = lookup.normalized_number))"

    .line 5114
    .restart local v6    # "selection":Ljava/lang/String;
    const/4 v3, 0x4

    new-array v7, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v11, v7, v3

    const/4 v3, 0x1

    aput-object v13, v7, v3

    const/4 v3, 0x2

    aput-object v9, v7, v3

    const/4 v3, 0x3

    aput-object v13, v7, v3

    .restart local v7    # "args":[Ljava/lang/String;
    goto :goto_1

    .line 5120
    :cond_7
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-nez v3, :cond_8

    .line 5121
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 5124
    :cond_8
    const-string v3, "display_name"

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 5125
    .local v12, "name":Ljava/lang/String;
    if-eqz v12, :cond_9

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_a

    .line 5126
    :cond_9
    const-string v3, "data4"

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 5128
    :cond_a
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    move-object v9, v12

    .line 5129
    goto/16 :goto_0
.end method

.method public static getContactNames(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "contactIds"    # Ljava/lang/String;

    .prologue
    .line 5133
    const-string v6, " |;"

    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 5134
    .local v3, "ids":[Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 5135
    .local v5, "sb":Ljava/lang/StringBuilder;
    move-object v0, v3

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v2, v0, v1

    .line 5136
    .local v2, "id":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-eqz v6, :cond_0

    .line 5137
    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5139
    :cond_0
    invoke-static {p0, v2}, Lcom/sec/android/app/myfiles/utils/Utils;->getContactName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5135
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5141
    .end local v2    # "id":Ljava/lang/String;
    :cond_1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method public static getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "date"    # J

    .prologue
    .line 2530
    const/4 v2, 0x0

    .line 2531
    .local v2, "mDateString":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .line 2533
    .local v0, "FinalDateString":Ljava/lang/String;
    if-nez p0, :cond_0

    .line 2535
    const/4 v7, 0x0

    .line 2562
    :goto_0
    return-object v7

    .line 2538
    :cond_0
    new-instance v6, Landroid/text/format/Time;

    invoke-direct {v6}, Landroid/text/format/Time;-><init>()V

    .line 2540
    .local v6, "time":Landroid/text/format/Time;
    invoke-virtual {v6}, Landroid/text/format/Time;->setToNow()V

    .line 2542
    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    .line 2544
    .local v4, "now":J
    sub-long v8, v4, p1

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-gez v7, :cond_1

    .line 2546
    const-wide/16 v8, 0x3e8

    div-long/2addr p1, v8

    .line 2548
    :cond_1
    const/4 v3, 0x0

    .line 2550
    .local v3, "shortDateFormat":Ljava/text/DateFormat;
    const/4 v1, 0x0

    .line 2552
    .local v1, "TimeFormat":Ljava/text/DateFormat;
    invoke-static {p0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    .line 2555
    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 2557
    new-instance v2, Ljava/lang/StringBuffer;

    .end local v2    # "mDateString":Ljava/lang/StringBuffer;
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 2558
    .restart local v2    # "mDateString":Ljava/lang/StringBuffer;
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, " "

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2560
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    .line 2562
    goto :goto_0
.end method

.method public static getDialogInstance()Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    .locals 1

    .prologue
    .line 3558
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mDialog:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    return-object v0
.end method

.method public static getDocumentFilesSize(Landroid/content/Context;I)J
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "storage"    # I

    .prologue
    const/4 v9, 0x0

    .line 773
    if-nez p0, :cond_1

    .line 775
    const-wide/16 v10, 0x0

    .line 875
    :cond_0
    :goto_0
    return-wide v10

    .line 778
    :cond_1
    const-wide/16 v10, 0x0

    .line 782
    .local v10, "totalSize":J
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 786
    .local v8, "sb":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .line 788
    .local v3, "where":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/MediaFile;->getDocumentExtensions()[Ljava/lang/String;

    move-result-object v4

    .line 790
    .local v4, "whereArgs":[Ljava/lang/String;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    array-length v0, v4

    if-ge v7, v0, :cond_2

    .line 792
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "%."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, v4, v7

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    .line 790
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 796
    :cond_2
    sparse-switch p1, :sswitch_data_0

    .line 821
    :goto_2
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 823
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 826
    :cond_3
    const-string v0, "("

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 828
    const/4 v7, 0x0

    :goto_3
    array-length v0, v4

    if-ge v7, v0, :cond_5

    .line 830
    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    if-ge v7, v0, :cond_4

    .line 832
    const-string v0, "_data LIKE ? OR "

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 828
    :goto_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 799
    :sswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_data LIKE \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%\' AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 800
    goto :goto_2

    .line 803
    :sswitch_1
    const-string v3, "_data LIKE \'/storage/extSdCard%\' AND "

    .line 804
    goto :goto_2

    .line 807
    :sswitch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_data LIKE \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%\' AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 808
    goto :goto_2

    .line 816
    :sswitch_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_data LIKE \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->getUsbStoragePath(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%\' AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 836
    :cond_4
    const-string v0, "_data LIKE ?"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 840
    :cond_5
    const-string v0, ")"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 842
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 846
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "external"

    invoke-static {v1}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v5, "SUM(_size)"

    aput-object v5, v2, v9

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 852
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 854
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 869
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 872
    :cond_6
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 796
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0x101 -> :sswitch_0
        0x202 -> :sswitch_1
        0x603 -> :sswitch_3
        0x604 -> :sswitch_3
        0x605 -> :sswitch_3
        0x606 -> :sswitch_3
        0x607 -> :sswitch_3
        0x608 -> :sswitch_3
    .end sparse-switch
.end method

.method public static getDownloadedAppSize(Landroid/content/Context;)J
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 895
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const/16 v7, 0x2200

    invoke-virtual {v6, v7}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v3

    .line 898
    .local v3, "mApplications":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 900
    .local v0, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    const-wide/16 v4, 0x0

    .line 902
    .local v4, "size":J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    if-ge v1, v6, :cond_1

    .line 904
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ApplicationInfo;

    .line 905
    .local v2, "info":Landroid/content/pm/ApplicationInfo;
    iget v6, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v6, v6, 0x1

    if-nez v6, :cond_0

    iget v6, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v7, 0x800000

    and-int/2addr v6, v7

    if-eqz v6, :cond_0

    .line 906
    new-instance v6, Ljava/io/File;

    iget-object v7, v2, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v6

    add-long/2addr v4, v6

    .line 907
    new-instance v6, Ljava/io/File;

    iget-object v7, v2, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v6

    add-long/2addr v4, v6

    .line 902
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 912
    .end local v2    # "info":Landroid/content/pm/ApplicationInfo;
    :cond_1
    return-wide v4
.end method

.method public static getDropBoxFilteredContent(Landroid/content/Context;Lcom/sec/android/app/myfiles/utils/SearchParameter;)Landroid/database/Cursor;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "param"    # Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .prologue
    .line 1180
    if-nez p0, :cond_0

    .line 1182
    const/4 v0, 0x0

    .line 1189
    :goto_0
    return-object v0

    .line 1185
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    if-nez v0, :cond_1

    .line 1187
    new-instance v0, Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    .line 1189
    :cond_1
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/db/CacheDB;->getDropBoxFilteredContent(Lcom/sec/android/app/myfiles/utils/SearchParameter;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method

.method public static getDropboxChildFileCount(Landroid/content/Context;Ljava/lang/String;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 2690
    new-instance v0, Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    .line 2692
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->getDropboxDepthCount(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->getCloudChildFileCount(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getDropboxDepthCount(Ljava/lang/String;)I
    .locals 3
    .param p0, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 5196
    const/4 v0, 0x1

    .line 5198
    .local v0, "Count":I
    if-eqz p0, :cond_0

    const-string v2, ""

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "/"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 5199
    :cond_0
    const/4 v2, 0x1

    .line 5211
    :goto_0
    return v2

    .line 5204
    :cond_1
    const-string v2, "/"

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 5206
    .local v1, "split":[Ljava/lang/String;
    array-length v2, v1

    if-gtz v2, :cond_2

    .line 5207
    const/4 v0, 0x1

    :goto_1
    move v2, v0

    .line 5211
    goto :goto_0

    .line 5209
    :cond_2
    array-length v0, v1

    goto :goto_1
.end method

.method public static getDropboxFilesSize(Landroid/content/Context;)J
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 881
    if-nez p0, :cond_0

    .line 883
    const-wide/16 v0, 0x0

    .line 888
    :goto_0
    return-wide v0

    .line 886
    :cond_0
    new-instance v0, Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    .line 888
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->getCloudTotalSize()J

    move-result-wide v0

    goto :goto_0
.end method

.method public static getDropboxFilesinFolder(Landroid/content/Context;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 2697
    new-instance v0, Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    .line 2699
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->getDropboxDepthCount(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->getCloudDirList(Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static getEasyMode()Z
    .locals 1

    .prologue
    .line 4791
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/Utils;->mEasyMode:Z

    return v0
.end method

.method public static getEditTextFilter(Landroid/content/Context;I)[Landroid/text/InputFilter;
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "maxSize"    # I

    .prologue
    .line 3150
    sput p1, Lcom/sec/android/app/myfiles/utils/Utils;->stringSize:I

    .line 3152
    const/4 v1, 0x2

    new-array v0, v1, [Landroid/text/InputFilter;

    .line 3154
    .local v0, "FilterArray":[Landroid/text/InputFilter;
    const/4 v1, 0x0

    new-instance v2, Lcom/sec/android/app/myfiles/utils/Utils$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/utils/Utils$5;-><init>(Landroid/content/Context;)V

    aput-object v2, v0, v1

    .line 3283
    const/4 v1, 0x1

    new-instance v2, Lcom/sec/android/app/myfiles/utils/Utils$6;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/utils/Utils$6;-><init>(Landroid/content/Context;)V

    aput-object v2, v0, v1

    .line 3344
    return-object v0
.end method

.method public static getEditTextFilterDigit(Landroid/content/Context;I)[Landroid/text/InputFilter;
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "maxSize"    # I

    .prologue
    .line 2996
    sput p1, Lcom/sec/android/app/myfiles/utils/Utils;->stringSize:I

    .line 2998
    const/4 v1, 0x2

    new-array v0, v1, [Landroid/text/InputFilter;

    .line 3000
    .local v0, "FilterArray":[Landroid/text/InputFilter;
    const/4 v1, 0x0

    new-instance v2, Lcom/sec/android/app/myfiles/utils/Utils$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/utils/Utils$3;-><init>(Landroid/content/Context;)V

    aput-object v2, v0, v1

    .line 3111
    const/4 v1, 0x1

    new-instance v2, Lcom/sec/android/app/myfiles/utils/Utils$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/utils/Utils$4;-><init>(Landroid/content/Context;)V

    aput-object v2, v0, v1

    .line 3144
    return-object v0
.end method

.method public static getEditTextMaxLengthFilter(Landroid/content/Context;I)[Landroid/text/InputFilter;
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "maxSize"    # I

    .prologue
    .line 3417
    const/4 v1, 0x1

    new-array v0, v1, [Landroid/text/InputFilter;

    .line 3419
    .local v0, "FilterArray":[Landroid/text/InputFilter;
    const/4 v1, 0x0

    new-instance v2, Lcom/sec/android/app/myfiles/utils/Utils$7;

    invoke-direct {v2, p1, p0}, Lcom/sec/android/app/myfiles/utils/Utils$7;-><init>(ILandroid/content/Context;)V

    aput-object v2, v0, v1

    .line 3491
    return-object v0
.end method

.method public static getFTPShortCutCount(Landroid/content/Context;)I
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 4425
    const-string v3, "type =  \'10\'"

    .line 4427
    .local v3, "where":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 4428
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_0

    .line 4429
    const/4 v6, 0x0

    .line 4432
    :goto_0
    return v6

    .line 4430
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 4431
    .local v6, "count":I
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static getFTPShortcutPaths(Landroid/content/Context;)Ljava/util/List;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 4494
    const-string v3, "type =  \'28\'"

    .line 4496
    .local v3, "where":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 4498
    .local v6, "cursor":Landroid/database/Cursor;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 4499
    .local v7, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v6, :cond_1

    .line 4500
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4501
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Security;->getInstance()Lcom/sec/android/app/myfiles/utils/Security;

    move-result-object v0

    const-string v1, "_data"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/utils/Security;->decrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 4504
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 4506
    :cond_1
    return-object v7
.end method

.method public static getFileSizeFormat(Landroid/content/Context;Lcom/sec/android/app/myfiles/element/HistoryItem;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "item"    # Lcom/sec/android/app/myfiles/element/HistoryItem;

    .prologue
    .line 2566
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getStatus()I

    move-result v0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    .line 2567
    const v0, 0x7f0b00ad

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2569
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getSize()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getFileThumbnailFolderName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 3666
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3667
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/storage/sdcard0/Android/data/com.baidu.netdisk/cache/thumbs"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3669
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/storage/sdcard0/Android/data/com.dropbox.android/cache/thumbs"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getFileTitleWithRespectToShowExtensionSettings(Lcom/sec/android/app/myfiles/utils/SharedDataStore;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "sds"    # Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 707
    if-eqz p0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 709
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowFileExtensionStatus()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 732
    .end local p1    # "fileName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 715
    .restart local p1    # "fileName":Ljava/lang/String;
    :cond_1
    const/16 v1, 0x2e

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 718
    .local v0, "lastDot":I
    if-lez v0, :cond_0

    .line 720
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public static getFilesFormats(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/HashMap;
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 926
    .local p1, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p2, "arrUsbPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p3, "androidPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_2

    .line 927
    :cond_0
    const/4 v13, 0x0

    .line 992
    :cond_1
    return-object v13

    .line 928
    :cond_2
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 929
    .local v13, "fileFormats":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v19

    .line 931
    .local v19, "pathSize":I
    const/4 v9, 0x0

    .line 932
    .local v9, "currBatchBeginIndex":I
    add-int/lit8 v10, v19, -0x1

    .line 933
    .local v10, "currBatchEndIndex":I
    const/16 v1, 0x320

    move/from16 v0, v19

    if-le v0, v1, :cond_3

    .line 934
    const/16 v10, 0x31f

    .line 935
    :cond_3
    sub-int v1, v10, v9

    add-int/lit8 v8, v1, 0x1

    .line 936
    .local v8, "curBatchLen":I
    const/4 v11, 0x1

    .line 938
    .local v11, "endLoopCond":Z
    :cond_4
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    .line 939
    .local v20, "sb":Ljava/lang/StringBuilder;
    new-array v5, v8, [Ljava/lang/String;

    .line 940
    .local v5, "whereArgs":[Ljava/lang/String;
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_0
    if-ge v15, v8, :cond_6

    .line 941
    if-lez v15, :cond_5

    .line 942
    const-string v1, " OR "

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 943
    :cond_5
    const-string v1, "_data LIKE ? "

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 944
    add-int v1, v9, v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v5, v15

    .line 940
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 946
    :cond_6
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 947
    .local v4, "where":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "external"

    invoke-static {v2}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v21, "_data"

    aput-object v21, v3, v6

    const/4 v6, 0x1

    const-string v21, "format"

    aput-object v21, v3, v6

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 949
    .local v7, "cr":Landroid/database/Cursor;
    if-eqz v7, :cond_9

    .line 950
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 952
    :cond_7
    const-string v1, "_data"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 953
    .local v17, "name":Ljava/lang/String;
    const-string v1, "format"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 954
    .local v14, "format":I
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v13, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 955
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_7

    .line 957
    .end local v14    # "format":I
    .end local v17    # "name":Ljava/lang/String;
    :cond_8
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 959
    :cond_9
    add-int/lit8 v1, v19, -0x1

    if-ge v10, v1, :cond_c

    .line 960
    const/4 v11, 0x0

    .line 961
    add-int/lit8 v9, v10, 0x1

    .line 962
    sub-int v1, v19, v9

    const/16 v2, 0x320

    if-le v1, v2, :cond_b

    .line 963
    add-int/lit16 v1, v9, 0x320

    add-int/lit8 v10, v1, -0x1

    .line 966
    :goto_1
    sub-int v1, v10, v9

    add-int/lit8 v8, v1, 0x1

    .line 969
    :goto_2
    if-eqz v11, :cond_4

    .line 970
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_e

    .line 971
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :cond_a
    :goto_3
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    .line 972
    .local v18, "path":Ljava/lang/String;
    new-instance v12, Ljava/io/File;

    move-object/from16 v0, v18

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 973
    .local v12, "file":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 974
    invoke-virtual {v12}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v17

    .line 975
    .restart local v17    # "name":Ljava/lang/String;
    invoke-virtual {v12}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_d

    const/16 v14, 0x3001

    .line 976
    .restart local v14    # "format":I
    :goto_4
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v13, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 965
    .end local v12    # "file":Ljava/io/File;
    .end local v14    # "format":I
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v17    # "name":Ljava/lang/String;
    .end local v18    # "path":Ljava/lang/String;
    :cond_b
    add-int/lit8 v10, v19, -0x1

    goto :goto_1

    .line 968
    :cond_c
    const/4 v11, 0x1

    goto :goto_2

    .line 975
    .restart local v12    # "file":Ljava/io/File;
    .restart local v16    # "i$":Ljava/util/Iterator;
    .restart local v17    # "name":Ljava/lang/String;
    .restart local v18    # "path":Ljava/lang/String;
    :cond_d
    const/4 v14, 0x0

    goto :goto_4

    .line 981
    .end local v12    # "file":Ljava/io/File;
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v17    # "name":Ljava/lang/String;
    .end local v18    # "path":Ljava/lang/String;
    :cond_e
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 982
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .restart local v16    # "i$":Ljava/util/Iterator;
    :cond_f
    :goto_5
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    .line 983
    .restart local v18    # "path":Ljava/lang/String;
    new-instance v12, Ljava/io/File;

    move-object/from16 v0, v18

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 984
    .restart local v12    # "file":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 985
    invoke-virtual {v12}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v17

    .line 986
    .restart local v17    # "name":Ljava/lang/String;
    invoke-virtual {v12}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_10

    const/16 v14, 0x3001

    .line 987
    .restart local v14    # "format":I
    :goto_6
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v13, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 986
    .end local v14    # "format":I
    :cond_10
    const/4 v14, 0x0

    goto :goto_6
.end method

.method public static getFirstUnsuedIndexFromShottut(Landroid/content/Context;Z)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isFTP"    # Z

    .prologue
    const/4 v9, 0x0

    .line 4048
    const-string v5, "shortcut_index ASC"

    .line 4049
    .local v5, "orderBy":Ljava/lang/String;
    const/4 v3, 0x0

    .line 4051
    .local v3, "where":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "shortcut_index"

    aput-object v4, v2, v9

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 4057
    .local v7, "cursor":Landroid/database/Cursor;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 4059
    .local v6, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v7, :cond_1

    .line 4060
    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4061
    const-string v0, "shortcut_index"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 4063
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 4066
    :cond_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 4077
    .local v8, "size":I
    if-nez v8, :cond_2

    move v0, v9

    .line 4081
    :goto_1
    return v0

    :cond_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static getFragmentTagByID(I)Ljava/lang/String;
    .locals 1
    .param p0, "fragmentId"    # I

    .prologue
    .line 3953
    const/4 v0, 0x0

    .line 3955
    .local v0, "fragmentTag":Ljava/lang/String;
    sparse-switch p0, :sswitch_data_0

    .line 4041
    :goto_0
    return-object v0

    .line 3958
    :sswitch_0
    const-string v0, "category_home"

    .line 3959
    goto :goto_0

    .line 3962
    :sswitch_1
    const-string v0, "all_files"

    .line 3963
    goto :goto_0

    .line 3966
    :sswitch_2
    const-string v0, "images"

    .line 3967
    goto :goto_0

    .line 3970
    :sswitch_3
    const-string v0, "videos"

    .line 3971
    goto :goto_0

    .line 3974
    :sswitch_4
    const-string v0, "music"

    .line 3975
    goto :goto_0

    .line 3978
    :sswitch_5
    const-string v0, "documents"

    .line 3979
    goto :goto_0

    .line 3982
    :sswitch_6
    const-string v0, "recently_files"

    .line 3983
    goto :goto_0

    .line 3986
    :sswitch_7
    const-string v0, "downloaded_apps"

    .line 3987
    goto :goto_0

    .line 3990
    :sswitch_8
    const-string v0, "dropbox"

    .line 3991
    goto :goto_0

    .line 3994
    :sswitch_9
    const-string v0, "remote_share"

    .line 3995
    goto :goto_0

    .line 3999
    :sswitch_a
    const-string v0, "baidu"

    .line 4000
    goto :goto_0

    .line 4004
    :sswitch_b
    const-string v0, "nearby_devices"

    .line 4005
    goto :goto_0

    .line 4009
    :sswitch_c
    const-string v0, "ftp"

    .line 4010
    goto :goto_0

    .line 4013
    :sswitch_d
    const-string v0, "sftp"

    .line 4014
    goto :goto_0

    .line 4017
    :sswitch_e
    const-string v0, "add_ftp"

    .line 4018
    goto :goto_0

    .line 4021
    :sswitch_f
    const-string v0, "add_ftps"

    .line 4022
    goto :goto_0

    .line 4025
    :sswitch_10
    const-string v0, "add_sftp"

    .line 4026
    goto :goto_0

    .line 4028
    :sswitch_11
    const-string v0, "ftp_list"

    .line 4029
    goto :goto_0

    .line 4031
    :sswitch_12
    const-string v0, "search"

    .line 4032
    goto :goto_0

    .line 4034
    :sswitch_13
    const-string v0, "SearchAdvanceFragment"

    .line 4035
    goto :goto_0

    .line 4037
    :sswitch_14
    const-string v0, "history"

    goto :goto_0

    .line 3955
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_7
        0x7 -> :sswitch_6
        0x8 -> :sswitch_8
        0x9 -> :sswitch_b
        0xa -> :sswitch_c
        0xb -> :sswitch_c
        0xc -> :sswitch_d
        0xd -> :sswitch_e
        0xe -> :sswitch_f
        0xf -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x1f -> :sswitch_a
        0x22 -> :sswitch_13
        0x25 -> :sswitch_9
        0x28 -> :sswitch_14
        0x201 -> :sswitch_1
    .end sparse-switch
.end method

.method public static getFtpNewFolderItem(Landroid/content/Context;Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p1, "names":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const v7, 0x7f0b001a

    .line 4682
    const/4 v4, 0x0

    .line 4684
    .local v4, "ncount":I
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 4686
    .local v0, "NewFolderName":Ljava/lang/String;
    if-eqz p1, :cond_3

    .line 4688
    :cond_0
    const/4 v2, 0x0

    .line 4689
    .local v2, "isFolderExists":Z
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 4690
    .local v3, "name":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 4692
    const/4 v2, 0x1

    .line 4693
    add-int/lit8 v4, v4, 0x1

    .line 4694
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 4698
    .end local v3    # "name":Ljava/lang/String;
    :cond_2
    if-nez v2, :cond_0

    .line 4702
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "isFolderExists":Z
    :goto_0
    return-object v0

    .line 4700
    :cond_3
    const/4 v5, 0x0

    const-string v6, "Utils"

    const-string v7, "ftp folder names is null"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getFtpShortcutCursor(Landroid/content/Context;)Landroid/database/Cursor;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 4511
    if-eqz p0, :cond_1

    .line 4513
    const-string v3, "type =  \'10\'"

    .line 4515
    .local v3, "where":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 4517
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4528
    .end local v3    # "where":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :goto_0
    return-object v6

    .line 4521
    .restart local v3    # "where":Ljava/lang/String;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :cond_0
    if-eqz v6, :cond_1

    .line 4523
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v3    # "where":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_1
    move-object v6, v2

    .line 4528
    goto :goto_0
.end method

.method public static getIfDropboxFileExist(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 1150
    if-nez p0, :cond_0

    .line 1152
    const/4 v0, 0x0

    .line 1159
    :goto_0
    return v0

    .line 1155
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    if-nez v0, :cond_1

    .line 1157
    new-instance v0, Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    .line 1159
    :cond_1
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/db/CacheDB;->getIfDropboxFileExist(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static getIfFtpFileExist(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1165
    if-nez p0, :cond_0

    .line 1167
    const/4 v0, 0x0

    .line 1174
    :goto_0
    return v0

    .line 1170
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    if-nez v0, :cond_1

    .line 1172
    new-instance v0, Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    .line 1174
    :cond_1
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/db/CacheDB;->checkFtpFileExist(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static getImageFilesSize(Landroid/content/Context;I)J
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "storage"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x0

    .line 477
    if-nez p0, :cond_1

    .line 479
    const-wide/16 v8, 0x0

    .line 547
    :cond_0
    :goto_0
    return-wide v8

    .line 482
    :cond_1
    const-wide/16 v8, 0x0

    .line 486
    .local v8, "totalSize":J
    const/4 v3, 0x0

    .line 488
    .local v3, "where":Ljava/lang/String;
    const/4 v4, 0x0

    .line 490
    .local v4, "whereArgs":[Ljava/lang/String;
    sparse-switch p1, :sswitch_data_0

    .line 518
    :goto_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-array v2, v2, [Ljava/lang/String;

    const-string v5, "SUM(_size)"

    aput-object v5, v2, v7

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 524
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 526
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 541
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 544
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 493
    .end local v6    # "cursor":Landroid/database/Cursor;
    :sswitch_0
    const-string v3, "_data LIKE ?"

    .line 494
    new-array v4, v2, [Ljava/lang/String;

    .end local v4    # "whereArgs":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    .line 495
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 498
    :sswitch_1
    const-string v3, "_data LIKE ?"

    .line 499
    new-array v4, v2, [Ljava/lang/String;

    .end local v4    # "whereArgs":[Ljava/lang/String;
    const-string v0, "/storage/extSdCard%"

    aput-object v0, v4, v7

    .line 500
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 503
    :sswitch_2
    const-string v3, "_data LIKE ?"

    .line 504
    new-array v4, v2, [Ljava/lang/String;

    .end local v4    # "whereArgs":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    .line 505
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 513
    :sswitch_3
    const-string v3, "_data LIKE ?"

    .line 514
    new-array v4, v2, [Ljava/lang/String;

    .end local v4    # "whereArgs":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->getUsbStoragePath(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    .restart local v4    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 490
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0x101 -> :sswitch_0
        0x202 -> :sswitch_1
        0x603 -> :sswitch_3
        0x604 -> :sswitch_3
        0x605 -> :sswitch_3
        0x606 -> :sswitch_3
        0x607 -> :sswitch_3
        0x608 -> :sswitch_3
    .end sparse-switch
.end method

.method public static getKNOXVersion(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x2

    .line 2227
    const-string v4, "Utils"

    const-string v5, "getKNOXVersion()"

    invoke-static {v6, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 2228
    const-string v3, "1.0"

    .line 2231
    .local v3, "version":Ljava/lang/String;
    :try_start_0
    const-string v4, "persona"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PersonaManager;

    .line 2232
    .local v2, "mPersona":Landroid/os/PersonaManager;
    const/4 v0, 0x0

    .line 2234
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->getKnoxinfoForAppBundle(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 2236
    const-string v4, "version"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 2243
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v2    # "mPersona":Landroid/os/PersonaManager;
    :goto_0
    return-object v3

    .line 2237
    :catch_0
    move-exception v1

    .line 2238
    .local v1, "e":Ljava/lang/NoClassDefFoundError;
    const-string v4, "Utils"

    const-string v5, "getKNOXVersion NoClassDefFoundError"

    invoke-static {v6, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2239
    .end local v1    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v1

    .line 2240
    .local v1, "e":Ljava/lang/NoSuchMethodError;
    const-string v4, "Utils"

    const-string v5, "getKNOXVersion NoSuchMethodError"

    invoke-static {v6, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getKnoxinfoForAppBundle(Landroid/content/Context;)Landroid/os/Bundle;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 5026
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mKnoxInfoForAppBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method public static getLastUsedShortcutIndex(Landroid/content/Context;)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x0

    .line 4221
    const/4 v3, 0x0

    .line 4223
    .local v3, "where":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "MAX ( shortcut_index ) AS shortcut_index"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 4228
    .local v7, "maxCursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 4229
    .local v8, "maxIndex":I
    if-eqz v7, :cond_0

    .line 4230
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 4231
    const-string v0, "shortcut_index"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 4232
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 4240
    .end local v7    # "maxCursor":Landroid/database/Cursor;
    .end local v8    # "maxIndex":I
    :cond_0
    :goto_0
    return v8

    .line 4235
    :catch_0
    move-exception v6

    .line 4236
    .local v6, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v6}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    move v8, v9

    .line 4237
    goto :goto_0

    .line 4238
    .end local v6    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v6

    .line 4239
    .local v6, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v6}, Ljava/lang/IllegalStateException;->printStackTrace()V

    move v8, v9

    .line 4240
    goto :goto_0
.end method

.method public static getLocation(Ljava/lang/String;)I
    .locals 1
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 2030
    if-nez p0, :cond_0

    .line 2031
    const/4 v0, -0x1

    .line 2040
    :goto_0
    return v0

    .line 2033
    :cond_0
    const-string v0, "["

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2034
    const/16 v0, 0x9

    goto :goto_0

    .line 2035
    :cond_1
    const-string v0, "/storage"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2036
    const/16 v0, 0x201

    goto :goto_0

    .line 2037
    :cond_2
    const-string v0, "@"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2038
    const/16 v0, 0xa

    goto :goto_0

    .line 2040
    :cond_3
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static getMIMEcategory(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "aMIMEtype"    # Ljava/lang/String;

    .prologue
    .line 305
    if-eqz p0, :cond_0

    .line 307
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    const-string v2, "/"

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 314
    :goto_0
    return-object p0

    .line 311
    :cond_0
    const-string p0, "application/*"

    goto :goto_0
.end method

.method public static getMountedUsbStorage()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2752
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2754
    .local v1, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;>;"
    sget-object v4, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    monitor-enter v4

    .line 2756
    :try_start_0
    sget-object v3, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;

    .line 2758
    .local v2, "usb":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->isMounted()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2760
    new-instance v3, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;

    invoke-direct {v3, v2}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;-><init>(Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2763
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "usb":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2765
    return-object v1
.end method

.method public static getMusicFilesSize(Landroid/content/Context;I)J
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "storage"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x0

    .line 631
    if-nez p0, :cond_1

    .line 633
    const-wide/16 v8, 0x0

    .line 702
    :cond_0
    :goto_0
    return-wide v8

    .line 636
    :cond_1
    const-wide/16 v8, 0x0

    .line 640
    .local v8, "totalSize":J
    const/4 v3, 0x0

    .line 642
    .local v3, "where":Ljava/lang/String;
    const/4 v4, 0x0

    .line 645
    .local v4, "whereArgs":[Ljava/lang/String;
    sparse-switch p1, :sswitch_data_0

    .line 673
    :goto_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-array v2, v2, [Ljava/lang/String;

    const-string v5, "SUM(_size)"

    aput-object v5, v2, v7

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 679
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 681
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 696
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 699
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 648
    .end local v6    # "cursor":Landroid/database/Cursor;
    :sswitch_0
    const-string v3, "_data LIKE ?"

    .line 649
    new-array v4, v2, [Ljava/lang/String;

    .end local v4    # "whereArgs":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    .line 650
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 653
    :sswitch_1
    const-string v3, "_data LIKE ?"

    .line 654
    new-array v4, v2, [Ljava/lang/String;

    .end local v4    # "whereArgs":[Ljava/lang/String;
    const-string v0, "/storage/extSdCard%"

    aput-object v0, v4, v7

    .line 655
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 658
    :sswitch_2
    const-string v3, "_data LIKE ?"

    .line 659
    new-array v4, v2, [Ljava/lang/String;

    .end local v4    # "whereArgs":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    .line 660
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 668
    :sswitch_3
    const-string v3, "_data LIKE ?"

    .line 669
    new-array v4, v2, [Ljava/lang/String;

    .end local v4    # "whereArgs":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->getUsbStoragePath(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    .restart local v4    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 645
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0x101 -> :sswitch_0
        0x202 -> :sswitch_1
        0x603 -> :sswitch_3
        0x604 -> :sswitch_3
        0x605 -> :sswitch_3
        0x606 -> :sswitch_3
        0x607 -> :sswitch_3
        0x608 -> :sswitch_3
    .end sparse-switch
.end method

.method public static getNewFolderItem(Landroid/content/Context;[Ljava/io/File;)Ljava/lang/String;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "files"    # [Ljava/io/File;

    .prologue
    const v10, 0x7f0b001a

    .line 4653
    const/4 v6, 0x0

    .line 4655
    .local v6, "ncount":I
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 4657
    .local v0, "NewFolderName":Ljava/lang/String;
    if-eqz p1, :cond_3

    .line 4659
    :cond_0
    const/4 v3, 0x0

    .line 4660
    .local v3, "isFolderExists":Z
    move-object v1, p1

    .local v1, "arr$":[Ljava/io/File;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v4, v1, v2

    .line 4662
    .local v4, "item":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    .line 4664
    .local v7, "strExistName":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 4665
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 4667
    const/4 v3, 0x1

    .line 4668
    add-int/lit8 v6, v6, 0x1

    .line 4669
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 4674
    .end local v4    # "item":Ljava/io/File;
    .end local v7    # "strExistName":Ljava/lang/String;
    :cond_1
    if-nez v3, :cond_0

    .line 4678
    .end local v1    # "arr$":[Ljava/io/File;
    .end local v2    # "i$":I
    .end local v3    # "isFolderExists":Z
    .end local v5    # "len$":I
    :goto_1
    return-object v0

    .line 4660
    .restart local v1    # "arr$":[Ljava/io/File;
    .restart local v2    # "i$":I
    .restart local v3    # "isFolderExists":Z
    .restart local v4    # "item":Ljava/io/File;
    .restart local v5    # "len$":I
    .restart local v7    # "strExistName":Ljava/lang/String;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 4676
    .end local v1    # "arr$":[Ljava/io/File;
    .end local v2    # "i$":I
    .end local v3    # "isFolderExists":Z
    .end local v4    # "item":Ljava/io/File;
    .end local v5    # "len$":I
    .end local v7    # "strExistName":Ljava/lang/String;
    :cond_3
    const/4 v8, 0x0

    const-string v9, "Utils"

    const-string v10, "files is null"

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static getNextShortcutIndex(Landroid/content/Context;)I
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 4203
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "MAX ( _id ) AS _id"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 4208
    .local v6, "maxCursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 4209
    .local v7, "maxIndex":I
    if-eqz v6, :cond_0

    .line 4210
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 4211
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 4212
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 4215
    :cond_0
    add-int/lit8 v0, v7, 0x1

    return v0
.end method

.method public static getOtherConfirmDeleteInstance(Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 1
    .param p0, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 5054
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mConfirmDeleteinstance:Landroid/app/Dialog;

    .line 5055
    .local v0, "retValue":Landroid/app/Dialog;
    sput-object p0, Lcom/sec/android/app/myfiles/utils/Utils;->mConfirmDeleteinstance:Landroid/app/Dialog;

    .line 5056
    return-object v0
.end method

.method public static getPathDropboxFilesSize(Landroid/content/Context;Ljava/util/ArrayList;)J
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 1135
    .local p1, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez p0, :cond_0

    .line 1137
    const-wide/16 v0, 0x0

    .line 1144
    :goto_0
    return-wide v0

    .line 1140
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    if-nez v0, :cond_1

    .line 1142
    new-instance v0, Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    .line 1144
    :cond_1
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/db/CacheDB;->getCloudPathSizes(Ljava/util/ArrayList;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public static getPathFilesSize(Landroid/content/Context;Ljava/util/ArrayList;)J
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 1091
    .local p1, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-wide/16 v2, 0x0

    .line 1093
    .local v2, "totalSize":J
    if-eqz p1, :cond_0

    .line 1095
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1097
    .local v1, "path":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->getTotalSize(Ljava/io/File;)J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 1098
    goto :goto_0

    .line 1101
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "path":Ljava/lang/String;
    :cond_0
    const/4 v4, 0x1

    const-string v5, "Utils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getPathFilesSize() - TotalSize of files to copy or move : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1103
    return-wide v2
.end method

.method public static getPathNearByDevicesFilesSize(Landroid/content/Context;Ljava/util/ArrayList;)J
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 1195
    .local p1, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez p0, :cond_0

    .line 1197
    const-wide/16 v0, 0x0

    .line 1204
    :goto_0
    return-wide v0

    .line 1200
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    if-nez v0, :cond_1

    .line 1202
    new-instance v0, Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    .line 1204
    :cond_1
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/db/CacheDB;->getNearByPathSizes(Ljava/util/ArrayList;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public static getPattern()Ljava/util/regex/Pattern;
    .locals 8

    .prologue
    .line 3363
    const/4 v0, 0x0

    .line 3364
    .local v0, "EMOJI_PATTERN":Ljava/util/regex/Pattern;
    const/4 v3, 0x0

    .line 3365
    .local v3, "patternMade":Z
    if-nez v3, :cond_1

    .line 3366
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/EmojiList;->getUnicodeList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 3368
    .local v1, "emojiPattern":Ljava/lang/StringBuilder;
    const/16 v5, 0x28

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3370
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/EmojiList;->getUnicodeList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 3371
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 3372
    .local v4, "str":Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->unicodeToUTF16(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3373
    const/16 v5, 0x7c

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 3375
    .end local v4    # "str":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    const-string v7, ")"

    invoke-virtual {v1, v5, v6, v7}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 3377
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 3378
    const/4 v3, 0x1

    .line 3380
    .end local v1    # "emojiPattern":Ljava/lang/StringBuilder;
    .end local v2    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_1
    return-object v0
.end method

.method public static getRecentFilesSize(Landroid/content/Context;)J
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 740
    new-instance v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    const/4 v3, 0x0

    invoke-direct {v0, p0, v3}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;-><init>(Landroid/content/Context;Z)V

    .line 742
    .local v0, "adapterManager":Lcom/sec/android/app/myfiles/adapter/AdapterManager;
    if-eqz p0, :cond_0

    if-nez v0, :cond_2

    .line 743
    :cond_0
    const-wide/16 v4, 0x0

    .line 768
    :cond_1
    :goto_0
    return-wide v4

    .line 746
    :cond_2
    const-wide/16 v4, 0x0

    .line 748
    .local v4, "totalSize":J
    const/4 v3, 0x7

    invoke-virtual {v0, v3}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getCategoryCursor(I)Landroid/database/Cursor;

    move-result-object v1

    .line 750
    .local v1, "cursor":Landroid/database/Cursor;
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_3

    .line 752
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 754
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 756
    const-string v3, "_size"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    add-long/2addr v4, v6

    .line 758
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    .line 754
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 765
    .end local v2    # "i":I
    :cond_3
    if-eqz v1, :cond_1

    .line 766
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static getRootFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 320
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-eqz v0, :cond_1

    .line 322
    :cond_0
    const-string v0, "/storage"

    .line 326
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    goto :goto_0
.end method

.method public static getScreenState(Landroid/content/Context;)I
    .locals 11
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 3749
    const-string v6, "window"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    .line 3751
    .local v5, "wm":Landroid/view/WindowManager;
    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 3753
    .local v0, "display":Landroid/view/Display;
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v2

    .line 3757
    .local v2, "rotation":I
    const/4 v1, 0x0

    .line 3759
    .local v1, "mwa":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    instance-of v6, p0, Landroid/app/Activity;

    if-eqz v6, :cond_0

    .line 3761
    new-instance v1, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .end local v1    # "mwa":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    move-object v6, p0

    check-cast v6, Landroid/app/Activity;

    invoke-direct {v1, v6}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    .line 3764
    .restart local v1    # "mwa":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 3766
    if-eqz v2, :cond_1

    if-ne v2, v9, :cond_3

    :cond_1
    move v3, v8

    .line 3782
    .local v3, "screenState":I
    :cond_2
    :goto_0
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isEnableMultiWindow(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_8

    move v4, v3

    .line 3796
    .end local v3    # "screenState":I
    .local v4, "screenState":I
    :goto_1
    return v4

    .end local v4    # "screenState":I
    :cond_3
    move v3, v7

    .line 3766
    goto :goto_0

    .line 3771
    :cond_4
    if-eqz v2, :cond_5

    if-ne v2, v9, :cond_6

    :cond_5
    move v3, v7

    .line 3774
    .restart local v3    # "screenState":I
    :goto_2
    invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I

    move-result v6

    if-eqz v6, :cond_2

    .line 3776
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v6

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v9

    if-le v6, v9, :cond_7

    move v3, v7

    .line 3778
    :goto_3
    const-string v6, "Utils"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getScreenState - screenState : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v6, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .end local v3    # "screenState":I
    :cond_6
    move v3, v8

    .line 3771
    goto :goto_2

    .restart local v3    # "screenState":I
    :cond_7
    move v3, v8

    .line 3776
    goto :goto_3

    .line 3787
    :cond_8
    if-nez v3, :cond_a

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 3789
    const/4 v3, 0x2

    :cond_9
    :goto_4
    move v4, v3

    .line 3796
    .end local v3    # "screenState":I
    .restart local v4    # "screenState":I
    goto :goto_1

    .line 3791
    .end local v4    # "screenState":I
    .restart local v3    # "screenState":I
    :cond_a
    if-ne v3, v8, :cond_9

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 3793
    const/4 v3, 0x3

    goto :goto_4
.end method

.method public static getSecretBoxRootFolder(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 1927
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/privatemode/PrivateModeManager;->getPrivateStorageDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    .line 1938
    :goto_0
    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    return-object v1

    .line 1929
    :catch_0
    move-exception v0

    .line 1931
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    sput-object v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    goto :goto_0

    .line 1933
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v0

    .line 1935
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    sput-object v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    goto :goto_0
.end method

.method public static getSecretModeState()Z
    .locals 1

    .prologue
    .line 1724
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretModeOn:Z

    return v0
.end method

.method public static getShortCutCount(Landroid/content/Context;Z)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "ignoreFtpServer"    # Z

    .prologue
    const/4 v9, 0x0

    .line 4474
    const/4 v3, 0x0

    .line 4475
    .local v3, "where":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 4476
    const-string v3, "type!=10"

    .line 4479
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 4480
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_1

    .line 4481
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 4482
    .local v6, "count":I
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4488
    .end local v6    # "count":I
    .end local v7    # "cursor":Landroid/database/Cursor;
    :goto_0
    return v6

    .restart local v7    # "cursor":Landroid/database/Cursor;
    :cond_1
    move v6, v9

    .line 4485
    goto :goto_0

    .line 4486
    .end local v7    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v8

    .line 4487
    .local v8, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v8}, Ljava/lang/IllegalStateException;->printStackTrace()V

    move v6, v9

    .line 4488
    goto :goto_0
.end method

.method public static getSizeUnit(J)Ljava/lang/String;
    .locals 6
    .param p0, "size"    # J

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1892
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1894
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-wide/16 v2, 0x400

    cmp-long v1, p0, v2

    if-gez v1, :cond_1

    .line 1896
    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1898
    const-string v1, " B"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1919
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 1900
    :cond_1
    const-wide/32 v2, 0x100000

    cmp-long v1, p0, v2

    if-gez v1, :cond_2

    .line 1902
    const-string v1, "%.1f"

    new-array v2, v4, [Ljava/lang/Object;

    long-to-float v3, p0

    const/high16 v4, 0x49800000    # 1048576.0f

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1904
    const-string v1, " KB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1906
    :cond_2
    const-wide/32 v2, 0x40000000

    cmp-long v1, p0, v2

    if-gez v1, :cond_3

    .line 1908
    const-string v1, "%.1f"

    new-array v2, v4, [Ljava/lang/Object;

    long-to-float v3, p0

    const/high16 v4, 0x4e800000

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1910
    const-string v1, " MB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1912
    :cond_3
    const-wide/16 v2, 0x0

    cmp-long v1, p0, v2

    if-gez v1, :cond_0

    .line 1914
    const-string v1, "%.1f"

    new-array v2, v4, [Ljava/lang/Object;

    long-to-float v3, p0

    const/4 v4, 0x0

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1916
    const-string v1, " GB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static getSmallFormatedDateFromLong(Landroid/content/Context;J)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "date"    # J

    .prologue
    .line 2574
    if-nez p0, :cond_0

    .line 2576
    const-string v0, ""

    .line 2588
    :goto_0
    return-object v0

    .line 2579
    :cond_0
    const/4 v1, 0x0

    .line 2580
    .local v1, "dateString":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .line 2581
    .local v0, "FinalDateString":Ljava/lang/String;
    const/4 v2, 0x0

    .line 2583
    .local v2, "shortDateFormat":Ljava/text/DateFormat;
    invoke-static {p0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    .line 2585
    new-instance v1, Ljava/lang/StringBuffer;

    .end local v1    # "dateString":Ljava/lang/StringBuffer;
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 2586
    .restart local v1    # "dateString":Ljava/lang/StringBuffer;
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, " "

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2588
    goto :goto_0
.end method

.method public static getStatusBarHeight(Landroid/content/Context;)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 5061
    const/4 v0, 0x0

    .line 5063
    .local v0, "height":I
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "status_bar_height"

    const-string v4, "dimen"

    const-string v5, "android"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 5065
    .local v1, "resourceId":I
    if-lez v1, :cond_0

    .line 5067
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 5070
    :cond_0
    return v0
.end method

.method public static getStorage(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "folder"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 1635
    if-nez p0, :cond_0

    .line 1637
    const-string v1, "Utils"

    const-string v2, "getStorage folder == null return null!!!"

    invoke-static {v3, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1662
    :goto_0
    return-object v0

    .line 1642
    :cond_0
    sget-boolean v1, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->checkUSBStorage(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1644
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    sget v1, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorageMount:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->mStoragePath:Ljava/lang/String;

    goto :goto_0

    .line 1646
    :cond_1
    sget-boolean v1, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v1, :cond_2

    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardStoragePath:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1648
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardStoragePath:Ljava/lang/String;

    goto :goto_0

    .line 1650
    :cond_2
    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1652
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    goto :goto_0

    .line 1654
    :cond_3
    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1656
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    goto :goto_0

    .line 1660
    :cond_4
    const-string v1, "Utils"

    const-string v2, "getStorage return null!!!"

    invoke-static {v3, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getStorageCapacity(Landroid/content/Context;I)J
    .locals 20
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "storage"    # I

    .prologue
    .line 1213
    const-wide/16 v2, 0x0

    .line 1217
    .local v2, "capacity":J
    const/4 v10, 0x0

    .line 1221
    .local v10, "stat":Landroid/os/StatFs;
    sparse-switch p1, :sswitch_data_0

    .line 1367
    :cond_0
    :goto_0
    return-wide v2

    .line 1226
    :sswitch_0
    const/16 v14, 0x101

    move-object/from16 v0, p0

    invoke-static {v0, v14}, Lcom/sec/android/app/myfiles/utils/Utils;->getStorageCapacity(Landroid/content/Context;I)J

    move-result-wide v2

    .line 1229
    sget-boolean v14, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v14, :cond_1

    .line 1237
    new-instance v10, Landroid/os/StatFs;

    .end local v10    # "stat":Landroid/os/StatFs;
    const-string v14, "/storage/extSdCard"

    invoke-direct {v10, v14}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1239
    .restart local v10    # "stat":Landroid/os/StatFs;
    invoke-virtual {v10}, Landroid/os/StatFs;->getTotalBytes()J

    move-result-wide v14

    add-long/2addr v2, v14

    .line 1244
    :cond_1
    sget-boolean v14, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-eqz v14, :cond_0

    .line 1246
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getMountedUsbStorage()Ljava/util/ArrayList;

    move-result-object v5

    .line 1248
    .local v5, "mountedUsbStorage":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;

    .line 1254
    .local v11, "usb":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    new-instance v10, Landroid/os/StatFs;

    .end local v10    # "stat":Landroid/os/StatFs;
    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->getStoragePath()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v10, v14}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1256
    .restart local v10    # "stat":Landroid/os/StatFs;
    invoke-virtual {v10}, Landroid/os/StatFs;->getTotalBytes()J

    move-result-wide v14

    add-long/2addr v2, v14

    .line 1258
    goto :goto_1

    .line 1270
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "mountedUsbStorage":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;>;"
    .end local v11    # "usb":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    :sswitch_1
    new-instance v10, Landroid/os/StatFs;

    .end local v10    # "stat":Landroid/os/StatFs;
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v14

    invoke-virtual {v14}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v10, v14}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1272
    .restart local v10    # "stat":Landroid/os/StatFs;
    invoke-virtual {v10}, Landroid/os/StatFs;->getTotalBytes()J

    move-result-wide v2

    .line 1276
    const/4 v6, 0x2

    .line 1278
    .local v6, "power":I
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    const-string v15, "storage_mmc_size"

    const-wide/16 v16, 0x0

    invoke-static/range {v14 .. v17}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v8

    .line 1284
    .local v8, "realTotalSize":J
    :goto_2
    const-wide/32 v14, 0x40000000

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    int-to-double v0, v6

    move-wide/from16 v18, v0

    invoke-static/range {v16 .. v19}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-long v0, v0

    move-wide/from16 v16, v0

    mul-long v12, v14, v16

    .line 1286
    .local v12, "tempTotalSize":J
    cmp-long v14, v8, v12

    if-gtz v14, :cond_2

    .line 1298
    move-wide v2, v12

    .line 1300
    goto :goto_0

    .line 1292
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 1305
    .end local v6    # "power":I
    .end local v8    # "realTotalSize":J
    .end local v12    # "tempTotalSize":J
    :sswitch_2
    sget-boolean v14, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v14, :cond_0

    .line 1311
    new-instance v10, Landroid/os/StatFs;

    .end local v10    # "stat":Landroid/os/StatFs;
    const-string v14, "/storage/extSdCard"

    invoke-direct {v10, v14}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1313
    .restart local v10    # "stat":Landroid/os/StatFs;
    invoke-virtual {v10}, Landroid/os/StatFs;->getTotalBytes()J

    move-result-wide v2

    goto/16 :goto_0

    .line 1322
    :sswitch_3
    sget-boolean v14, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-eqz v14, :cond_0

    .line 1324
    new-instance v7, Ljava/io/File;

    sget-object v14, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-direct {v7, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1326
    .local v7, "secretBoxRoot":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_0

    .line 1332
    new-instance v10, Landroid/os/StatFs;

    .end local v10    # "stat":Landroid/os/StatFs;
    sget-object v14, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-direct {v10, v14}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1334
    .restart local v10    # "stat":Landroid/os/StatFs;
    invoke-virtual {v10}, Landroid/os/StatFs;->getTotalBytes()J

    move-result-wide v14

    add-long/2addr v2, v14

    goto/16 :goto_0

    .line 1348
    .end local v7    # "secretBoxRoot":Ljava/io/File;
    :sswitch_4
    sget-boolean v14, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-eqz v14, :cond_0

    .line 1354
    new-instance v10, Landroid/os/StatFs;

    .end local v10    # "stat":Landroid/os/StatFs;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/Utils;->getUsbStoragePath(I)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v10, v14}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1356
    .restart local v10    # "stat":Landroid/os/StatFs;
    invoke-virtual {v10}, Landroid/os/StatFs;->getTotalBytes()J

    move-result-wide v2

    goto/16 :goto_0

    .line 1221
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_3
        0x101 -> :sswitch_1
        0x202 -> :sswitch_2
        0x603 -> :sswitch_4
        0x604 -> :sswitch_4
        0x605 -> :sswitch_4
        0x606 -> :sswitch_4
        0x607 -> :sswitch_4
        0x608 -> :sswitch_4
    .end sparse-switch
.end method

.method public static getStorageFreeSpace(I)J
    .locals 8
    .param p0, "storage"    # I

    .prologue
    .line 1373
    const-wide/16 v0, 0x0

    .line 1377
    .local v0, "freeSpace":J
    const/4 v4, 0x0

    .line 1382
    .local v4, "stat":Landroid/os/StatFs;
    sparse-switch p0, :sswitch_data_0

    .line 1499
    :cond_0
    :goto_0
    return-wide v0

    .line 1391
    :sswitch_0
    new-instance v4, Landroid/os/StatFs;

    .end local v4    # "stat":Landroid/os/StatFs;
    sget-object v6, Lcom/sec/android/app/myfiles/utils/Utils;->mDataFolder:Ljava/lang/String;

    invoke-direct {v4, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1393
    .restart local v4    # "stat":Landroid/os/StatFs;
    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBytes()J

    move-result-wide v0

    .line 1396
    sget-boolean v6, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v6, :cond_1

    .line 1402
    new-instance v4, Landroid/os/StatFs;

    .end local v4    # "stat":Landroid/os/StatFs;
    const-string v6, "/storage/extSdCard"

    invoke-direct {v4, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1404
    .restart local v4    # "stat":Landroid/os/StatFs;
    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBytes()J

    move-result-wide v6

    add-long/2addr v0, v6

    .line 1409
    :cond_1
    sget-boolean v6, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-eqz v6, :cond_0

    .line 1411
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getMountedUsbStorage()Ljava/util/ArrayList;

    move-result-object v3

    .line 1413
    .local v3, "mountedUsbStorage":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;

    .line 1419
    .local v5, "usb":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    new-instance v4, Landroid/os/StatFs;

    .end local v4    # "stat":Landroid/os/StatFs;
    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->getStoragePath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1421
    .restart local v4    # "stat":Landroid/os/StatFs;
    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBytes()J

    move-result-wide v6

    add-long/2addr v0, v6

    .line 1422
    goto :goto_1

    .line 1434
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "mountedUsbStorage":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;>;"
    .end local v5    # "usb":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    :sswitch_1
    new-instance v4, Landroid/os/StatFs;

    .end local v4    # "stat":Landroid/os/StatFs;
    sget-object v6, Lcom/sec/android/app/myfiles/utils/Utils;->mDataFolder:Ljava/lang/String;

    invoke-direct {v4, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1436
    .restart local v4    # "stat":Landroid/os/StatFs;
    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBytes()J

    move-result-wide v0

    .line 1438
    goto :goto_0

    .line 1443
    :sswitch_2
    sget-boolean v6, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v6, :cond_0

    .line 1449
    new-instance v4, Landroid/os/StatFs;

    .end local v4    # "stat":Landroid/os/StatFs;
    const-string v6, "/storage/extSdCard"

    invoke-direct {v4, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1451
    .restart local v4    # "stat":Landroid/os/StatFs;
    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBytes()J

    move-result-wide v0

    goto :goto_0

    .line 1459
    :sswitch_3
    sget-boolean v6, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-eqz v6, :cond_0

    .line 1465
    new-instance v4, Landroid/os/StatFs;

    .end local v4    # "stat":Landroid/os/StatFs;
    sget-object v6, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-direct {v4, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1467
    .restart local v4    # "stat":Landroid/os/StatFs;
    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBytes()J

    move-result-wide v6

    add-long/2addr v0, v6

    goto :goto_0

    .line 1480
    :sswitch_4
    sget-boolean v6, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-eqz v6, :cond_0

    .line 1486
    new-instance v4, Landroid/os/StatFs;

    .end local v4    # "stat":Landroid/os/StatFs;
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->getUsbStoragePath(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1488
    .restart local v4    # "stat":Landroid/os/StatFs;
    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBytes()J

    move-result-wide v0

    goto/16 :goto_0

    .line 1382
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_3
        0x101 -> :sswitch_1
        0x202 -> :sswitch_2
        0x603 -> :sswitch_4
        0x604 -> :sswitch_4
        0x605 -> :sswitch_4
        0x606 -> :sswitch_4
        0x607 -> :sswitch_4
        0x608 -> :sswitch_4
    .end sparse-switch
.end method

.method public static getStorageUsageIsShowing(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 5046
    if-eqz p0, :cond_0

    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mStorageUsageIsShowing:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5047
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mStorageUsageIsShowing:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 5049
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getTotalSize(Ljava/io/File;)J
    .locals 10
    .param p0, "file"    # Ljava/io/File;

    .prologue
    .line 1114
    const-wide/16 v6, 0x0

    .line 1115
    .local v6, "totalSize":J
    if-eqz p0, :cond_2

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1116
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1117
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 1118
    .local v2, "fileList":[Ljava/io/File;
    if-eqz v2, :cond_2

    .line 1119
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v1, v0, v3

    .line 1120
    .local v1, "content":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1121
    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getTotalSize(Ljava/io/File;)J

    move-result-wide v8

    add-long/2addr v6, v8

    .line 1119
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1123
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v8

    add-long/2addr v6, v8

    goto :goto_1

    .line 1127
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "content":Ljava/io/File;
    .end local v2    # "fileList":[Ljava/io/File;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v8

    .line 1130
    :goto_2
    return-wide v8

    :cond_2
    move-wide v8, v6

    goto :goto_2
.end method

.method public static final getUsbStorage(Ljava/lang/String;)Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    .locals 5
    .param p0, "folder"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 2383
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v1, v2

    .line 2399
    :goto_0
    return-object v1

    .line 2388
    :cond_0
    sget-object v3, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    monitor-enter v3

    .line 2390
    :try_start_0
    sget-object v4, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;

    .line 2392
    .local v1, "usbStorage":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->isMounted()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, v1, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->mStoragePath:Ljava/lang/String;

    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2394
    monitor-exit v3

    goto :goto_0

    .line 2397
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "usbStorage":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v1, v2

    .line 2399
    goto :goto_0
.end method

.method public static getUsbStoragePath(I)Ljava/lang/String;
    .locals 2
    .param p0, "storageType"    # I

    .prologue
    .line 333
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/storage/UsbDrive"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 v1, p0, 0x41

    add-int/lit16 v1, v1, -0x603

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getUserShortCutCount(Landroid/content/Context;ZZ)I
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "enableDropbox"    # Z
    .param p2, "enableFtp"    # Z

    .prologue
    const/4 v2, 0x0

    .line 4607
    const-string v3, "type =  \'26\'"

    .line 4608
    .local v3, "where":Ljava/lang/String;
    if-eqz p1, :cond_2

    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isDropboxAccountIsSignin(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 4609
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR type =  \'8\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4612
    :cond_0
    :goto_0
    if-eqz p2, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 4613
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR type =  \'28\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4615
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 4616
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_3

    .line 4617
    const/4 v6, 0x0

    .line 4620
    :goto_1
    return v6

    .line 4610
    .end local v7    # "cursor":Landroid/database/Cursor;
    :cond_2
    if-eqz p1, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isBaiduCloudAccountIsSignin(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4611
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR type =  \'31\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 4618
    .restart local v7    # "cursor":Landroid/database/Cursor;
    :cond_3
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 4619
    .local v6, "count":I
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method

.method public static getUserShortcutCursor(Landroid/content/Context;ZZ)Landroid/database/Cursor;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "enableDropbox"    # Z
    .param p2, "enableFtp"    # Z

    .prologue
    const/4 v2, 0x0

    .line 4624
    if-eqz p0, :cond_4

    .line 4626
    const-string v3, "type =  \'26\'"

    .line 4627
    .local v3, "where":Ljava/lang/String;
    if-eqz p1, :cond_2

    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isDropboxAccountIsSignin(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 4628
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR type =  \'8\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4631
    :cond_0
    :goto_0
    if-eqz p2, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 4632
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR type =  \'28\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4634
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 4636
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4647
    .end local v3    # "where":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :goto_1
    return-object v6

    .line 4629
    .restart local v3    # "where":Ljava/lang/String;
    :cond_2
    if-eqz p1, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isBaiduCloudAccountIsSignin(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4630
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR type =  \'31\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 4640
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :cond_3
    if-eqz v6, :cond_4

    .line 4642
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v3    # "where":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_4
    move-object v6, v2

    .line 4647
    goto :goto_1
.end method

.method private static getVersionOfContextProviders(Landroid/content/Context;)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 4994
    const/4 v2, -0x1

    .line 4996
    .local v2, "version":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.samsung.android.providers.context"

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 4998
    .local v1, "pInfo":Landroid/content/pm/PackageInfo;
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5002
    .end local v1    # "pInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return v2

    .line 4999
    :catch_0
    move-exception v0

    .line 5000
    .local v0, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v3, 0x0

    const-string v4, "Utils"

    const-string v5, "[SW] Could not find ContextProvider"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getVideoFilesSize(Landroid/content/Context;I)J
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "storage"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x0

    .line 553
    if-nez p0, :cond_1

    .line 555
    const-wide/16 v8, 0x0

    .line 625
    :cond_0
    :goto_0
    return-wide v8

    .line 558
    :cond_1
    const-wide/16 v8, 0x0

    .line 562
    .local v8, "totalSize":J
    const/4 v3, 0x0

    .line 564
    .local v3, "where":Ljava/lang/String;
    const/4 v4, 0x0

    .line 567
    .local v4, "whereArgs":[Ljava/lang/String;
    sparse-switch p1, :sswitch_data_0

    .line 596
    :goto_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-array v2, v2, [Ljava/lang/String;

    const-string v5, "SUM(_size)"

    aput-object v5, v2, v7

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 602
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 604
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 619
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 622
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 570
    .end local v6    # "cursor":Landroid/database/Cursor;
    :sswitch_0
    const-string v3, "_data LIKE ?"

    .line 571
    new-array v4, v2, [Ljava/lang/String;

    .end local v4    # "whereArgs":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    .line 572
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 575
    :sswitch_1
    const-string v3, "_data LIKE ?"

    .line 576
    new-array v4, v2, [Ljava/lang/String;

    .end local v4    # "whereArgs":[Ljava/lang/String;
    const-string v0, "/storage/extSdCard%"

    aput-object v0, v4, v7

    .line 577
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 580
    :sswitch_2
    const-string v3, "_data LIKE ?"

    .line 581
    new-array v4, v2, [Ljava/lang/String;

    .end local v4    # "whereArgs":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    .line 582
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 590
    :sswitch_3
    const-string v3, "_data LIKE ?"

    .line 591
    new-array v4, v2, [Ljava/lang/String;

    .end local v4    # "whereArgs":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->getUsbStoragePath(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    .restart local v4    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 567
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0x101 -> :sswitch_0
        0x202 -> :sswitch_1
        0x603 -> :sswitch_3
        0x604 -> :sswitch_3
        0x605 -> :sswitch_3
        0x606 -> :sswitch_3
        0x607 -> :sswitch_3
        0x608 -> :sswitch_3
    .end sparse-switch
.end method

.method public static getZipItem(Ljava/lang/String;[Ljava/io/File;)Ljava/lang/String;
    .locals 10
    .param p0, "FileName"    # Ljava/lang/String;
    .param p1, "files"    # [Ljava/io/File;

    .prologue
    const/4 v9, 0x0

    .line 4747
    const/4 v6, 0x0

    .line 4748
    .local v6, "ncount":I
    const/4 v3, 0x0

    .line 4749
    .local v3, "isExists":Z
    move-object v0, p0

    .line 4751
    .local v0, "NewZipName":Ljava/lang/String;
    if-eqz p1, :cond_3

    .line 4753
    :cond_0
    const/4 v3, 0x0

    .line 4754
    move-object v1, p1

    .local v1, "arr$":[Ljava/io/File;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v4, v1, v2

    .line 4756
    .local v4, "item":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 4757
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 4759
    const/4 v3, 0x1

    .line 4760
    add-int/lit8 v6, v6, 0x1

    .line 4761
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "."

    invoke-virtual {p0, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {p0, v9, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".zip"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 4767
    .end local v4    # "item":Ljava/io/File;
    :cond_1
    if-nez v3, :cond_0

    .line 4772
    .end local v1    # "arr$":[Ljava/io/File;
    .end local v2    # "i$":I
    .end local v5    # "len$":I
    :goto_1
    const-string v7, "."

    invoke-virtual {v0, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v0, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    return-object v7

    .line 4754
    .restart local v1    # "arr$":[Ljava/io/File;
    .restart local v2    # "i$":I
    .restart local v4    # "item":Ljava/io/File;
    .restart local v5    # "len$":I
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 4769
    .end local v1    # "arr$":[Ljava/io/File;
    .end local v2    # "i$":I
    .end local v4    # "item":Ljava/io/File;
    .end local v5    # "len$":I
    :cond_3
    const-string v7, "Utils"

    const-string v8, "create zip files is null"

    invoke-static {v9, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static hasAvailableSpace(Ljava/lang/String;)Z
    .locals 10
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 2417
    if-nez p0, :cond_1

    .line 2454
    :cond_0
    :goto_0
    return v4

    .line 2422
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->getStorage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2424
    .local v3, "storageDirectory":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 2438
    new-instance v2, Landroid/os/StatFs;

    invoke-direct {v2, v3}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 2440
    .local v2, "stat":Landroid/os/StatFs;
    invoke-virtual {v2}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v6

    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockCountLong()J

    move-result-wide v8

    mul-long v0, v6, v8

    .line 2442
    .local v0, "remaining":J
    const/4 v5, 0x2

    const-string v6, "Utils"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "hasAvailableSpace() - remaining : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 2444
    const-wide/16 v6, 0x1

    cmp-long v5, v0, v6

    if-ltz v5, :cond_0

    .line 2454
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public static hasEmojiString(Ljava/lang/CharSequence;)Z
    .locals 4
    .param p0, "chars"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v3, 0x0

    .line 3350
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getPattern()Ljava/util/regex/Pattern;

    move-result-object v2

    .line 3351
    .local v2, "ptn":Ljava/util/regex/Pattern;
    if-eqz v2, :cond_0

    .line 3352
    invoke-virtual {v2, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 3353
    .local v1, "match":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 3358
    .end local v1    # "match":Ljava/util/regex/Matcher;
    .end local v2    # "ptn":Ljava/util/regex/Pattern;
    :cond_0
    :goto_0
    return v3

    .line 3357
    :catch_0
    move-exception v0

    .line 3358
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public static insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "feature"    # Ljava/lang/String;
    .param p2, "extra"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 4964
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->getVersionOfContextProviders(Landroid/content/Context;)I

    move-result v4

    const/4 v5, 0x2

    if-ge v4, v5, :cond_0

    .line 4991
    :goto_0
    return-void

    .line 4967
    :cond_0
    const-string v4, "content://com.samsung.android.providers.context.log.use_app_feature_survey"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 4972
    .local v3, "uri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 4973
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 4974
    .local v2, "row":Landroid/content/ContentValues;
    const-string v4, "app_id"

    const-string v5, "com.sec.android.app.myfiles"

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4975
    const-string v4, "feature"

    invoke-virtual {v2, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4978
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 4980
    const-string v4, "extra"

    invoke-virtual {v2, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4983
    :cond_1
    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 4984
    const/4 v4, 0x0

    const-string v5, "Utils"

    const-string v6, "ContextProvider insertion operation is performed."

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 4986
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "row":Landroid/content/ContentValues;
    :catch_0
    move-exception v1

    .line 4988
    .local v1, "ex":Ljava/lang/Exception;
    const-string v4, "Utils"

    const-string v5, "Error while using the ContextProvider"

    invoke-static {v7, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 4989
    const-string v4, "Utils"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v7, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isAbsoluteRootFolder(Ljava/lang/String;)Z
    .locals 1
    .param p0, "folder"    # Ljava/lang/String;

    .prologue
    .line 1505
    if-eqz p0, :cond_0

    const-string v0, "/storage"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1507
    const/4 v0, 0x1

    .line 1511
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isAndroidFolder(Ljava/lang/String;)Z
    .locals 6
    .param p0, "folder"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 2248
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2265
    :cond_0
    :goto_0
    return v3

    .line 2253
    :cond_1
    const-string v4, "/Android"

    invoke-virtual {p0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2254
    const-string v4, "/Android"

    invoke-virtual {p0, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 2255
    .local v2, "split":I
    const-string v4, "/Android"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int v0, v2, v4

    .line 2256
    .local v0, "endIndex":I
    sget-object v4, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    const-string v5, "/Android"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    sget-object v4, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    const-string v5, "/Android"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v0, v4, :cond_3

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v0, :cond_0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x2f

    if-ne v4, v5, :cond_0

    .line 2258
    :cond_3
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2259
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2260
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public static isBaiduCloudAccountIsSignin(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x1

    .line 2933
    const/4 v2, 0x0

    .line 2936
    .local v2, "bAvailable":Z
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2937
    invoke-static {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    move-result-object v3

    .line 2938
    .local v3, "baiduAPIHelper":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->isBaiduCloudAccountIsSignin()Z

    move-result v2

    .line 2939
    const-string v5, "Utils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isBaiduCloudAccountIsSignin() - bAvailable : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 2942
    .end local v3    # "baiduAPIHelper":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;
    :cond_0
    if-eqz v2, :cond_2

    .line 2943
    const/4 v2, 0x1

    .line 2946
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v4

    .line 2948
    .local v4, "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDropboxAuthKey()Ljava/lang/String;

    move-result-object v0

    .line 2949
    .local v0, "authKey":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDropboxAuthSecret()Ljava/lang/String;

    move-result-object v1

    .line 2951
    .local v1, "authSecret":Ljava/lang/String;
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_2

    .line 2952
    :cond_1
    const/4 v2, 0x0

    .line 2958
    .end local v0    # "authKey":Ljava/lang/String;
    .end local v1    # "authSecret":Ljava/lang/String;
    .end local v4    # "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    :cond_2
    const-string v5, "Utils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isBaiduCloudAccountIsSignin() - bAvailable : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 2960
    return v2
.end method

.method public static isBaiduFolder(Ljava/lang/String;)Z
    .locals 2
    .param p0, "folder"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 2870
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2880
    :cond_0
    :goto_0
    return v0

    .line 2875
    :cond_1
    const-string v1, "Baidu/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "/Baidu"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2877
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isCategoryFolder(I)Z
    .locals 1
    .param p0, "fragmentId"    # I

    .prologue
    .line 1544
    const/4 v0, 0x2

    if-lt p0, v0, :cond_0

    const/4 v0, 0x7

    if-gt p0, v0, :cond_0

    .line 1545
    const/4 v0, 0x1

    .line 1546
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isCategoryTag(Ljava/lang/String;)Z
    .locals 1
    .param p0, "category"    # Ljava/lang/String;

    .prologue
    .line 1582
    const-string v0, "images"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "videos"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "music"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "documents"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "downloaded_apps"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "recently_files"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "history"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1589
    :cond_0
    const/4 v0, 0x1

    .line 1590
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isCheckMountedStorage()Z
    .locals 1

    .prologue
    .line 1517
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isChinaMobileOn(Landroid/net/NetworkInfo;)Z
    .locals 5
    .param p0, "netInfo"    # Landroid/net/NetworkInfo;

    .prologue
    const/4 v1, 0x0

    .line 3564
    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3566
    .local v0, "salesCode":Ljava/lang/String;
    const-string v2, "CTC"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 3568
    const-string v2, "Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "not CTC salesCode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 3594
    :cond_0
    :goto_0
    return v1

    .line 3573
    :cond_1
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_2

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v2

    const/4 v3, 0x7

    if-eq v2, v3, :cond_2

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v2

    const/4 v3, 0x5

    if-eq v2, v3, :cond_2

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v2

    const/4 v3, 0x6

    if-eq v2, v3, :cond_2

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v2

    const/16 v3, 0xc

    if-eq v2, v3, :cond_2

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v2

    const/16 v3, 0xe

    if-ne v2, v3, :cond_0

    .line 3589
    :cond_2
    const-string v2, "Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " [CTC] if (netInfo != null) netInfo.getSubtype() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 3591
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isChinaModel()Z
    .locals 4

    .prologue
    .line 3883
    const-string v3, "ro.csc.country_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3884
    .local v1, "country":Ljava/lang/String;
    const-string v3, "ro.csc.countryiso_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3885
    .local v2, "countryISO":Ljava/lang/String;
    const-string v3, "CHINA"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "CN"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 3887
    .local v0, "bCN":Z
    :goto_0
    return v0

    .line 3885
    .end local v0    # "bCN":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isClickValid()Z
    .locals 8

    .prologue
    .line 5145
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 5146
    .local v0, "newClickTime":Ljava/lang/Long;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-wide v4, Lcom/sec/android/app/myfiles/utils/Utils;->mLastClickTime:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x2ee

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 5147
    const/4 v1, 0x2

    const-string v2, "Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid click - time diff = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sget-wide v6, Lcom/sec/android/app/myfiles/utils/Utils;->mLastClickTime:J

    sub-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 5149
    const/4 v1, 0x0

    .line 5152
    :goto_0
    return v1

    .line 5151
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sput-wide v2, Lcom/sec/android/app/myfiles/utils/Utils;->mLastClickTime:J

    .line 5152
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isCloudListShowing()Z
    .locals 1

    .prologue
    .line 5022
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/Utils;->mIsShowing:Z

    return v0
.end method

.method public static isDirectory(Ljava/lang/String;)Z
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 1107
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1108
    .local v0, "file":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1109
    const/4 v1, 0x1

    .line 1110
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isDirectoryOfDropbox(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 3802
    new-instance v0, Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    .line 3804
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/db/CacheDB;->isDirectory(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isDropBoxEnabled(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 3850
    const/4 v0, 0x0

    .line 3851
    .local v0, "pi":Landroid/content/pm/PackageInfo;
    const/4 v1, 0x0

    .line 3853
    .local v1, "piDropbox":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.sec.android.cloudagent"

    const/4 v5, 0x5

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 3856
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.dropbox.android"

    const/4 v5, 0x5

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3863
    :goto_0
    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v3

    if-ne v3, v2, :cond_1

    .line 3868
    :cond_0
    :goto_1
    return v2

    .line 3865
    :cond_1
    if-eqz v0, :cond_2

    if-nez v1, :cond_0

    .line 3866
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 3859
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public static isDropboxAccountAdded(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 2885
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 2887
    .local v1, "manager":Landroid/accounts/AccountManager;
    if-nez v1, :cond_1

    .line 2899
    :cond_0
    :goto_0
    return v2

    .line 2891
    :cond_1
    const-string v3, "com.dropbox.android.account"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 2893
    .local v0, "accountAttr":[Landroid/accounts/Account;
    array-length v3, v0

    if-lez v3, :cond_0

    .line 2895
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static isDropboxAccountIsSignin(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2905
    const/4 v2, 0x0

    .line 2907
    .local v2, "bResult":Z
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isDropboxAccountAdded(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2909
    const/4 v2, 0x1

    .line 2912
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v3

    .line 2914
    .local v3, "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDropboxAuthKey()Ljava/lang/String;

    move-result-object v0

    .line 2916
    .local v0, "authKey":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDropboxAuthSecret()Ljava/lang/String;

    move-result-object v1

    .line 2918
    .local v1, "authSecret":Ljava/lang/String;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 2920
    :cond_0
    const/4 v2, 0x0

    .line 2927
    .end local v0    # "authKey":Ljava/lang/String;
    .end local v1    # "authSecret":Ljava/lang/String;
    .end local v3    # "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    :cond_1
    return v2
.end method

.method public static isDropboxFolder(Ljava/lang/String;)Z
    .locals 2
    .param p0, "folder"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 2854
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2864
    :cond_0
    :goto_0
    return v0

    .line 2859
    :cond_1
    const-string v1, "Dropbox/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "Dropbox/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "/Dropbox"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2861
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isEasyMode(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 4777
    if-nez p0, :cond_0

    .line 4781
    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "easy_mode_switch"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "easy_mode_myfiles"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static isEmulatedEntryFolder(Ljava/lang/String;)Z
    .locals 1
    .param p0, "folder"    # Ljava/lang/String;

    .prologue
    .line 1622
    if-eqz p0, :cond_0

    const-string v0, "/storage/emulated"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1624
    const/4 v0, 0x1

    .line 1628
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isEnableMultiWindow(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2970
    sget-boolean v1, Lcom/sec/android/app/myfiles/utils/Utils;->isEnableMultiWindowCheck:Z

    if-nez v1, :cond_0

    .line 2972
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 2974
    .local v0, "pm":Landroid/content/pm/PackageManager;
    const/4 v1, 0x0

    const-string v2, "Utils"

    const-string v3, "jsh isEnableMultiWindow checking!!"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 2976
    if-eqz v0, :cond_0

    .line 2978
    const-string v1, "com.sec.feature.multiwindow"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    .line 2980
    const-string v1, "com.sec.feature.multiwindow"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/Utils;->isEnableMultiWindow:Z

    .line 2982
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/Utils;->isEnableMultiWindowCheck:Z

    .line 2986
    .end local v0    # "pm":Landroid/content/pm/PackageManager;
    :cond_0
    sget-boolean v1, Lcom/sec/android/app/myfiles/utils/Utils;->isEnableMultiWindow:Z

    return v1
.end method

.method public static isExternalFolder(Ljava/lang/String;)Z
    .locals 1
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 2018
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2020
    const-string v0, "/storage/extSdCard"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    .line 2024
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isExternalRootFolder(Ljava/lang/String;)Z
    .locals 1
    .param p0, "folder"    # Ljava/lang/String;

    .prologue
    .line 2005
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardStoragePath:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2007
    const/4 v0, 0x1

    .line 2010
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isHiddenItem(Ljava/lang/String;)Z
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 2828
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x2e

    if-ne v1, v2, :cond_0

    .line 2830
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowHiddenFileStatus()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2832
    const/4 v0, 0x1

    .line 2836
    :cond_0
    return v0
.end method

.method public static isHoverSettingDisabled(Landroid/content/Context;Landroid/view/MotionEvent;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 4573
    const/4 v4, 0x1

    .line 4575
    .local v4, "retValue":Z
    if-eqz p0, :cond_6

    .line 4577
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "pen_hovering"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-ne v7, v5, :cond_1

    move v2, v5

    .line 4578
    .local v2, "PEN_HOVERING":Z
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "finger_air_view"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-ne v7, v5, :cond_2

    move v0, v5

    .line 4579
    .local v0, "FINGER_AIR_VIEW":Z
    :goto_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "pen_hovering_information_preview"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-ne v7, v5, :cond_3

    move v3, v5

    .line 4580
    .local v3, "PEN_HOVERING_INFORMATION_PREVIEW":Z
    :goto_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "finger_air_view_information_preview"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-ne v7, v5, :cond_4

    move v1, v5

    .line 4582
    .local v1, "FINGER_AIR_VIEW_INFORMATION_PREVIEW":Z
    :goto_3
    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_5

    .line 4584
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 4585
    const/4 v4, 0x0

    .line 4603
    .end local v0    # "FINGER_AIR_VIEW":Z
    .end local v1    # "FINGER_AIR_VIEW_INFORMATION_PREVIEW":Z
    .end local v2    # "PEN_HOVERING":Z
    .end local v3    # "PEN_HOVERING_INFORMATION_PREVIEW":Z
    :cond_0
    :goto_4
    return v4

    :cond_1
    move v2, v6

    .line 4577
    goto :goto_0

    .restart local v2    # "PEN_HOVERING":Z
    :cond_2
    move v0, v6

    .line 4578
    goto :goto_1

    .restart local v0    # "FINGER_AIR_VIEW":Z
    :cond_3
    move v3, v6

    .line 4579
    goto :goto_2

    .restart local v3    # "PEN_HOVERING_INFORMATION_PREVIEW":Z
    :cond_4
    move v1, v6

    .line 4580
    goto :goto_3

    .line 4587
    .restart local v1    # "FINGER_AIR_VIEW_INFORMATION_PREVIEW":Z
    :cond_5
    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v6

    if-ne v6, v5, :cond_0

    .line 4589
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 4592
    const/4 v4, 0x0

    goto :goto_4

    .line 4600
    .end local v0    # "FINGER_AIR_VIEW":Z
    .end local v1    # "FINGER_AIR_VIEW_INFORMATION_PREVIEW":Z
    .end local v2    # "PEN_HOVERING":Z
    .end local v3    # "PEN_HOVERING_INFORMATION_PREVIEW":Z
    :cond_6
    const-string v5, "Utils"

    const-string v7, "Utils - isHoverSettingDisabled() context is null."

    invoke-static {v6, v5, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_4
.end method

.method public static isInExternalSdStorage(Ljava/lang/String;)Z
    .locals 2
    .param p0, "folder"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1853
    if-nez p0, :cond_1

    .line 1863
    :cond_0
    :goto_0
    return v0

    .line 1858
    :cond_1
    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardStoragePath:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1860
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isInKNOXMode(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 267
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sec_container_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isInUsbHostStorage(Ljava/lang/String;)Z
    .locals 4
    .param p0, "folder"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1870
    if-nez p0, :cond_0

    move v1, v2

    .line 1886
    :goto_0
    return v1

    .line 1875
    :cond_0
    sget-object v3, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    monitor-enter v3

    .line 1877
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    :try_start_0
    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 1879
    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->getStoragePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1881
    const/4 v1, 0x1

    monitor-exit v3

    goto :goto_0

    .line 1884
    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 1877
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1884
    :cond_2
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v1, v2

    .line 1886
    goto :goto_0
.end method

.method public static isInternalFolderPath(Ljava/lang/String;)Z
    .locals 1
    .param p0, "folder"    # Ljava/lang/String;

    .prologue
    .line 1609
    if-eqz p0, :cond_0

    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1611
    const/4 v0, 0x1

    .line 1615
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isInternalRootFolder(Ljava/lang/String;)Z
    .locals 1
    .param p0, "folder"    # Ljava/lang/String;

    .prologue
    .line 1596
    if-eqz p0, :cond_0

    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1598
    const/4 v0, 0x1

    .line 1602
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isKNOXFileRelayAvailable(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 2139
    if-eqz p0, :cond_0

    .line 2140
    const-string v4, "persona"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PersonaManager;

    .line 2144
    .local v1, "mPersona":Landroid/os/PersonaManager;
    const/4 v0, 0x0

    .line 2146
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 2148
    invoke-static {p0}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 2150
    const-string v4, "2.0"

    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->getKNOXVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2152
    invoke-virtual {v1}, Landroid/os/PersonaManager;->getPersonas()Ljava/util/List;

    move-result-object v2

    .line 2153
    .local v2, "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 2154
    const-string v4, "true"

    const-string v5, "isSupportMoveTo"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2156
    const/4 v3, 0x1

    .line 2174
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "mPersona":Landroid/os/PersonaManager;
    .end local v2    # "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    :cond_0
    :goto_0
    return v3

    .line 2165
    .restart local v0    # "bundle":Landroid/os/Bundle;
    .restart local v1    # "mPersona":Landroid/os/PersonaManager;
    :cond_1
    const-string v4, "1.0"

    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->getKNOXVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2167
    sget-object v4, Lcom/sec/android/app/myfiles/utils/Utils;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    if-eqz v4, :cond_0

    .line 2168
    const/4 v3, 0x2

    const-string v4, "Utils"

    const-string v5, "isKNOXFileRelayAvailable "

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 2169
    sget-object v3, Lcom/sec/android/app/myfiles/utils/Utils;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    invoke-virtual {v3}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->isKNOXFileRelayAvailable()Z

    move-result v3

    goto :goto_0
.end method

.method public static isKNOXInstalled(Landroid/content/Context;)Z
    .locals 5
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 2091
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v0

    .line 2093
    .local v0, "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    const-string v1, "Utils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isKNOXInstalled() - sds.getKNOXInstalled() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getKNOXInstalled()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 2095
    return v4
.end method

.method public static isKNOXMode(Landroid/content/Context;)Z
    .locals 5
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2045
    const-string v1, "2.0"

    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->getKNOXVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2047
    const/4 v0, 0x0

    .line 2049
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->getKnoxinfoForAppBundle(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 2052
    const-string v1, "true"

    const-string v2, "isKnoxMode"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2053
    sput-boolean v3, Lcom/sec/android/app/myfiles/utils/Utils;->mKNOXModeOn:Z

    .line 2075
    .end local v0    # "bundle":Landroid/os/Bundle;
    :goto_0
    const-string v1, "Utils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isKNOXMode() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/sec/android/app/myfiles/utils/Utils;->mKNOXModeOn:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 2077
    sget-boolean v1, Lcom/sec/android/app/myfiles/utils/Utils;->mKNOXModeOn:Z

    return v1

    .line 2055
    .restart local v0    # "bundle":Landroid/os/Bundle;
    :cond_0
    sput-boolean v4, Lcom/sec/android/app/myfiles/utils/Utils;->mKNOXModeOn:Z

    goto :goto_0

    .line 2057
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_1
    const-string v1, "1.0"

    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->getKNOXVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2060
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "sec_container_"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2063
    sput-boolean v3, Lcom/sec/android/app/myfiles/utils/Utils;->mKNOXModeOn:Z

    goto :goto_0

    .line 2067
    :cond_2
    sput-boolean v4, Lcom/sec/android/app/myfiles/utils/Utils;->mKNOXModeOn:Z

    goto :goto_0

    .line 2071
    :cond_3
    sput-boolean v4, Lcom/sec/android/app/myfiles/utils/Utils;->mKNOXModeOn:Z

    goto :goto_0
.end method

.method public static isMediaScannerScannig()Z
    .locals 1

    .prologue
    .line 2405
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/Utils;->mIsMediaScannerScannig:Z

    return v0
.end method

.method public static isMountedStateCheck(Landroid/content/Context;)V
    .locals 15
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1730
    const/4 v9, 0x0

    const-string v10, "Utils"

    const-string v11, "isMountedStateCheck start"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1735
    const-string v9, "trigger_restart_min_framework"

    const-string v10, "vold.decrypt"

    const-string v11, ""

    invoke-static {v10, v11}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1848
    :goto_0
    return-void

    .line 1739
    :cond_0
    sget-object v10, Lcom/sec/android/app/myfiles/utils/Utils;->mKey_sync:Ljava/lang/Object;

    monitor-enter v10

    .line 1741
    :try_start_0
    sget-object v9, Lcom/sec/android/app/myfiles/utils/Utils;->mStorageManager:Landroid/os/storage/StorageManager;

    if-nez v9, :cond_1

    .line 1743
    const-string v9, "storage"

    invoke-virtual {p0, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/storage/StorageManager;

    sput-object v9, Lcom/sec/android/app/myfiles/utils/Utils;->mStorageManager:Landroid/os/storage/StorageManager;

    .line 1748
    :cond_1
    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1750
    sget-object v9, Lcom/sec/android/app/myfiles/utils/Utils;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v9}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v7

    .line 1752
    .local v7, "storageVolumes":[Landroid/os/storage/StorageVolume;
    array-length v3, v7

    .line 1754
    .local v3, "length":I
    const-string v4, ""

    .line 1756
    .local v4, "mStoragePath":Ljava/lang/String;
    const/4 v9, 0x0

    sput-boolean v9, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    .line 1758
    const/4 v1, 0x0

    .line 1760
    .local v1, "checkUsbMounted":Z
    sget-object v9, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 1762
    sget-object v9, Lcom/sec/android/app/myfiles/utils/Utils;->mMountedUsbStorage:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 1764
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->getKnoxinfoForAppBundle(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v9

    if-nez v9, :cond_2

    .line 1765
    invoke-static {p0}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/myfiles/utils/Utils;->setKnoxinfoForAppBundle(Landroid/os/Bundle;)V

    .line 1767
    :cond_2
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->getKnoxinfoForAppBundle(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 1769
    .local v0, "bundle":Landroid/os/Bundle;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v3, :cond_9

    .line 1771
    aget-object v6, v7, v2

    .line 1773
    .local v6, "storageVolume":Landroid/os/storage/StorageVolume;
    invoke-virtual {v6}, Landroid/os/storage/StorageVolume;->getSubSystem()Ljava/lang/String;

    move-result-object v8

    .line 1775
    .local v8, "subsystem":Ljava/lang/String;
    if-eqz v8, :cond_3

    .line 1777
    invoke-virtual {v6}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v4

    .line 1779
    sget-object v9, Lcom/sec/android/app/myfiles/utils/Utils;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v9, v4}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1781
    .local v5, "state":Ljava/lang/String;
    const-string v9, "/storage/extSdCard"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1783
    sput-object v4, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardStoragePath:Ljava/lang/String;

    .line 1785
    const-string v9, "mounted"

    invoke-virtual {v9, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    const-string v9, "true"

    const-string v10, "isBlockExternalSD"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 1787
    const/4 v9, 0x0

    const-string v10, "Utils"

    const-string v11, "mmc is mounted"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1789
    const/4 v9, 0x1

    sput-boolean v9, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    .line 1769
    .end local v5    # "state":Ljava/lang/String;
    :cond_3
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1748
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "checkUsbMounted":Z
    .end local v2    # "i":I
    .end local v3    # "length":I
    .end local v4    # "mStoragePath":Ljava/lang/String;
    .end local v6    # "storageVolume":Landroid/os/storage/StorageVolume;
    .end local v7    # "storageVolumes":[Landroid/os/storage/StorageVolume;
    .end local v8    # "subsystem":Ljava/lang/String;
    :catchall_0
    move-exception v9

    :try_start_1
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v9

    .line 1793
    .restart local v0    # "bundle":Landroid/os/Bundle;
    .restart local v1    # "checkUsbMounted":Z
    .restart local v2    # "i":I
    .restart local v3    # "length":I
    .restart local v4    # "mStoragePath":Ljava/lang/String;
    .restart local v5    # "state":Ljava/lang/String;
    .restart local v6    # "storageVolume":Landroid/os/storage/StorageVolume;
    .restart local v7    # "storageVolumes":[Landroid/os/storage/StorageVolume;
    .restart local v8    # "subsystem":Ljava/lang/String;
    :cond_4
    const/4 v9, 0x0

    sput-boolean v9, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    goto :goto_2

    .line 1796
    :cond_5
    const-string v9, "usb"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1798
    sget-object v10, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    monitor-enter v10

    .line 1800
    :try_start_2
    const-string v9, "mounted"

    invoke-virtual {v9, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    const-string v9, "true"

    const-string v11, "isBlockExternalSD"

    invoke-virtual {v0, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_6

    .line 1802
    const/4 v9, 0x0

    const-string v11, "Utils"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "/"

    invoke-virtual {v4, v13}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v13

    add-int/lit8 v13, v13, 0x1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v14

    invoke-virtual {v4, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " is mounted"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v11, v12}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1806
    const/4 v1, 0x1

    .line 1808
    sget-object v9, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    new-instance v11, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;

    const/4 v12, 0x1

    invoke-direct {v11, v4, v12}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1810
    sget-object v9, Lcom/sec/android/app/myfiles/utils/Utils;->mMountedUsbStorage:Ljava/util/ArrayList;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1816
    :goto_3
    monitor-exit v10

    goto :goto_2

    :catchall_1
    move-exception v9

    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v9

    .line 1814
    :cond_6
    :try_start_3
    sget-object v9, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    new-instance v11, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;

    const/4 v12, 0x0

    invoke-direct {v11, v4, v12}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_3

    .line 1818
    :cond_7
    const-string v9, "private"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1820
    const-string v9, "mounted"

    invoke-virtual {v9, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 1822
    invoke-static {p0}, Lcom/samsung/android/privatemode/PrivateModeManager;->isPrivateStorageMounted(Landroid/content/Context;)Z

    move-result v9

    sput-boolean v9, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    .line 1823
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->getSecretBoxRootFolder(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    sput-object v9, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    .line 1832
    :goto_4
    const/4 v9, 0x0

    const-string v10, "Utils"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SecretBox is "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ". mStoragePath : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", mSecretBoxMounted : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-boolean v12, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", mSecretModeOn : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-boolean v12, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretModeOn:Z

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 1827
    :cond_8
    invoke-static {p0}, Lcom/samsung/android/privatemode/PrivateModeManager;->isPrivateStorageMounted(Landroid/content/Context;)Z

    move-result v9

    sput-boolean v9, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    goto :goto_4

    .line 1838
    .end local v5    # "state":Ljava/lang/String;
    .end local v6    # "storageVolume":Landroid/os/storage/StorageVolume;
    .end local v8    # "subsystem":Ljava/lang/String;
    :cond_9
    if-eqz v1, :cond_a

    .line 1840
    const/4 v9, 0x1

    sput-boolean v9, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    .line 1847
    :goto_5
    const/4 v9, 0x0

    const-string v10, "Utils"

    const-string v11, "isMountedStateCheck end"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1844
    :cond_a
    const/4 v9, 0x0

    sput-boolean v9, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    goto :goto_5
.end method

.method public static final isMountedUsbStorageFolder(Ljava/lang/String;)Z
    .locals 2
    .param p0, "folder"    # Ljava/lang/String;

    .prologue
    .line 2369
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2371
    const/4 v0, 0x0

    .line 2376
    :goto_0
    return v0

    .line 2374
    :cond_0
    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mMountedUsbStorage:Ljava/util/ArrayList;

    monitor-enter v1

    .line 2376
    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mMountedUsbStorage:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 2377
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static isMultiWindowMode(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 3734
    const/4 v0, 0x0

    .line 3736
    .local v0, "multiWindowMode":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    instance-of v1, p0, Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 3738
    new-instance v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .end local v0    # "multiWindowMode":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    .line 3741
    .restart local v0    # "multiWindowMode":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    :cond_0
    if-nez v0, :cond_1

    .line 3742
    const/4 v1, 0x0

    .line 3744
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v1

    goto :goto_0
.end method

.method public static isNeedReplaceDropboxWithBaiduCloud()Z
    .locals 4

    .prologue
    .line 3875
    const-string v3, "ro.csc.country_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3876
    .local v1, "country":Ljava/lang/String;
    const-string v3, "ro.csc.countryiso_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3877
    .local v2, "countryISO":Ljava/lang/String;
    const-string v3, "CHINA"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "CN"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 3879
    .local v0, "bCN":Z
    :goto_0
    return v0

    .line 3877
    .end local v0    # "bCN":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isNetworkConnectedNearByDevice(Landroid/content/Context;)Z
    .locals 12
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/16 v11, 0xd

    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 5161
    const-string v9, "connectivity"

    invoke-virtual {p0, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 5164
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v8}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 5165
    .local v2, "netInfo":Landroid/net/NetworkInfo;
    const/4 v5, 0x0

    .line 5167
    .local v5, "wifiConnected":Z
    if-eqz v2, :cond_1

    .line 5168
    sget-object v9, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    sget-object v9, Landroid/net/NetworkInfo$DetailedState;->VERIFYING_POOR_LINK:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    :cond_0
    move v5, v8

    .line 5172
    :cond_1
    :goto_0
    const/4 v1, 0x0

    .line 5173
    .local v1, "hotspotEnabled":Z
    const-string v9, "wifi"

    invoke-virtual {p0, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/wifi/WifiManager;

    .line 5174
    .local v6, "wm":Landroid/net/wifi/WifiManager;
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Landroid/net/wifi/WifiManager;->getWifiApState()I

    move-result v9

    if-ne v9, v11, :cond_2

    .line 5175
    const/4 v1, 0x1

    .line 5178
    :cond_2
    invoke-virtual {v0, v11}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 5180
    .local v3, "netInfoP2p":Landroid/net/NetworkInfo;
    const/4 v4, 0x0

    .line 5182
    .local v4, "p2pConnected":Z
    if-eqz v3, :cond_4

    .line 5183
    sget-object v9, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    sget-object v9, Landroid/net/NetworkInfo$DetailedState;->VERIFYING_POOR_LINK:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    :cond_3
    move v4, v8

    .line 5187
    :cond_4
    :goto_1
    if-nez v5, :cond_5

    if-nez v4, :cond_5

    if-eqz v1, :cond_6

    :cond_5
    move v7, v8

    .line 5191
    :cond_6
    return v7

    .end local v1    # "hotspotEnabled":Z
    .end local v3    # "netInfoP2p":Landroid/net/NetworkInfo;
    .end local v4    # "p2pConnected":Z
    .end local v6    # "wm":Landroid/net/wifi/WifiManager;
    :cond_7
    move v5, v7

    .line 5168
    goto :goto_0

    .restart local v1    # "hotspotEnabled":Z
    .restart local v3    # "netInfoP2p":Landroid/net/NetworkInfo;
    .restart local v4    # "p2pConnected":Z
    .restart local v6    # "wm":Landroid/net/wifi/WifiManager;
    :cond_8
    move v4, v7

    .line 5183
    goto :goto_1
.end method

.method public static isNetworkOn(Landroid/content/Context;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/16 v7, 0xd

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3599
    if-nez p0, :cond_1

    .line 3659
    :cond_0
    :goto_0
    return v2

    .line 3604
    :cond_1
    const-string v4, "connectivity"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 3606
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    if-eqz v0, :cond_0

    .line 3608
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 3610
    .local v1, "netInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_4

    .line 3612
    const-string v4, "Utils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "netInfo.getType() = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 3613
    const-string v4, "Utils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "netInfo.getSubtype() = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 3618
    :goto_1
    if-eqz v1, :cond_0

    sget-object v4, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v4

    if-eq v4, v3, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v4

    if-eq v4, v7, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v4

    if-eq v4, v7, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v4

    const/16 v5, 0xf

    if-eq v4, v5, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v4

    const/16 v5, 0x9

    if-eq v4, v5, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v4

    const/16 v5, 0xa

    if-eq v4, v5, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v4

    const/4 v5, 0x4

    if-eq v4, v5, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v4

    const/4 v5, 0x7

    if-eq v4, v5, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v4

    const/4 v5, 0x5

    if-eq v4, v5, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v4

    const/4 v5, 0x6

    if-eq v4, v5, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v4

    const/16 v5, 0xc

    if-eq v4, v5, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v4

    const/16 v5, 0xe

    if-eq v4, v5, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v4

    if-eq v4, v3, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v4

    const/4 v5, 0x2

    if-eq v4, v5, :cond_3

    :cond_2
    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isChinaMobileOn(Landroid/net/NetworkInfo;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 3651
    :cond_3
    const-string v4, "Utils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " if (netInfo != null) netInfo.getType() = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    move v2, v3

    .line 3653
    goto/16 :goto_0

    .line 3616
    :cond_4
    const-string v4, "Utils"

    const-string v5, "netInfo == null"

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public static isOEMHiddenItem(Ljava/lang/String;)Z
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 2810
    const-string v0, "/sdcard"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/acct"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/mnt/asec"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/mnt/obb"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/mnt/secure"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/mnt/.lfs"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/storage/sdcard1"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2820
    :cond_0
    const/4 v0, 0x1

    .line 2822
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isOnCall(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2731
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 2734
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 2736
    const/4 v1, 0x1

    .line 2739
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isPhysicalRootFolder(Ljava/lang/String;)Z
    .locals 1
    .param p0, "folder"    # Ljava/lang/String;

    .prologue
    .line 339
    if-eqz p0, :cond_0

    const-string v0, "/storage"

    if-eqz v0, :cond_0

    .line 341
    const-string v0, "/storage"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 344
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isRemoteShareEnabled(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 3915
    new-instance v2, Lcom/sec/orca/easysignup/api/PolicyAPI;

    invoke-direct {v2, p0}, Lcom/sec/orca/easysignup/api/PolicyAPI;-><init>(Landroid/content/Context;)V

    .line 3917
    .local v2, "policyAPI":Lcom/sec/orca/easysignup/api/PolicyAPI;
    const/4 v1, -0x1

    .line 3919
    .local v1, "features":I
    if-eqz v2, :cond_0

    .line 3923
    const/4 v4, 0x2

    :try_start_0
    invoke-virtual {v2, v4}, Lcom/sec/orca/easysignup/api/PolicyAPI;->getSupportedFeatures(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 3935
    :cond_0
    :goto_0
    if-ltz v1, :cond_1

    const/4 v3, 0x1

    :cond_1
    return v3

    .line 3925
    :catch_0
    move-exception v0

    .line 3927
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "Utils"

    const-string v5, "isRemoteShareEnabled : Exception during getSupportedFeatures operating."

    invoke-static {v3, v4, v5, v0}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 3929
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 3931
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    const-string v4, "Utils"

    const-string v5, "isRemoteShareEnabled : NoSuchMethodError."

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isRootFolder(Ljava/lang/String;)Z
    .locals 2
    .param p0, "folder"    # Ljava/lang/String;

    .prologue
    .line 1524
    sget-boolean v1, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-eqz v1, :cond_1

    .line 1526
    :cond_0
    const-string v0, "/storage"

    .line 1533
    .local v0, "currentRoot":Ljava/lang/String;
    :goto_0
    if-eqz p0, :cond_2

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1535
    const/4 v1, 0x1

    .line 1539
    :goto_1
    return v1

    .line 1530
    .end local v0    # "currentRoot":Ljava/lang/String;
    :cond_1
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    .restart local v0    # "currentRoot":Ljava/lang/String;
    goto :goto_0

    .line 1539
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static isRtl()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 5157
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSecretBoxFolder(Ljava/lang/String;)Z
    .locals 2
    .param p0, "folder"    # Ljava/lang/String;

    .prologue
    .line 1960
    const/4 v0, 0x0

    .line 1962
    .local v0, "isSecret":Z
    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    if-eqz v1, :cond_0

    if-eqz p0, :cond_0

    .line 1964
    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1966
    const/4 v0, 0x1

    .line 1970
    :cond_0
    return v0
.end method

.method public static isSecretBoxRootFolder(Ljava/lang/String;)Z
    .locals 2
    .param p0, "folder"    # Ljava/lang/String;

    .prologue
    .line 1944
    const/4 v0, 0x0

    .line 1946
    .local v0, "isSecret":Z
    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    if-eqz v1, :cond_0

    if-eqz p0, :cond_0

    .line 1948
    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1950
    const/4 v0, 0x1

    .line 1954
    :cond_0
    return v0
.end method

.method public static isSecretMode(Landroid/content/Context;)Z
    .locals 6
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 1978
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/privatemode/PrivateModeManager;->isPrivateStorageMounted(Landroid/content/Context;)Z

    move-result v2

    sput-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    .line 1979
    const/4 v2, 0x0

    const-string v3, "Utils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isSecretMode () - mSecretBoxMounted : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-boolean v5, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    .line 1991
    :goto_0
    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-eqz v2, :cond_0

    .line 1993
    const/4 v1, 0x1

    .line 1997
    :cond_0
    return v1

    .line 1982
    :catch_0
    move-exception v0

    .line 1984
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    goto :goto_0

    .line 1986
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v0

    .line 1988
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    goto :goto_0
.end method

.method public static isSupportDownloadSaveAsInChinaModel()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 3892
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Web_SupportDownloadSaveAs"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 3896
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSystemFolder(Ljava/lang/String;)Z
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 2842
    const-string v0, "/system"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/sys"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/proc"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/storage/sdcard0"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/storage/emulated/legacy"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/storage/container"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2845
    :cond_0
    const/4 v0, 0x1

    .line 2848
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isTablet(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 3728
    const/4 v0, 0x0

    return v0
.end method

.method public static isUSBStorageType(I)Z
    .locals 2
    .param p0, "storageType"    # I

    .prologue
    .line 3839
    and-int/lit16 v0, p0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isUsbStorageFolder(Ljava/lang/String;)Z
    .locals 6
    .param p0, "folder"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 2318
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2363
    :cond_0
    :goto_0
    return v3

    .line 2323
    :cond_1
    if-nez p0, :cond_2

    .line 2325
    const-string v4, "Utils"

    const-string v5, "isUSBStorage folder is null!!!!"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2332
    :cond_2
    :try_start_0
    sget-object v4, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 2334
    sget-object v5, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 2336
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    :try_start_1
    sget-object v4, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_4

    .line 2338
    sget-object v4, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;

    .line 2340
    .local v2, "usbStorage":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    iget-object v4, v2, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->mStoragePath:Ljava/lang/String;

    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2342
    const/4 v4, 0x1

    monitor-exit v5

    move v3, v4

    goto :goto_0

    .line 2336
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2345
    .end local v2    # "usbStorage":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    :cond_4
    monitor-exit v5

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v4
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 2349
    .end local v1    # "i":I
    :catch_0
    move-exception v0

    .line 2351
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 2354
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 2356
    .local v0, "e":Ljava/util/ConcurrentModificationException;
    invoke-virtual {v0}, Ljava/util/ConcurrentModificationException;->printStackTrace()V

    goto :goto_0

    .line 2359
    .end local v0    # "e":Ljava/util/ConcurrentModificationException;
    :catch_2
    move-exception v0

    .line 2361
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static isUsbStorageRootFolder(Ljava/lang/String;)Z
    .locals 6
    .param p0, "folder"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 2271
    if-nez p0, :cond_1

    .line 2273
    const-string v4, "Utils"

    const-string v5, "isUSBStorage folder is null!!!!"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 2312
    :cond_0
    :goto_0
    return v3

    .line 2280
    :cond_1
    :try_start_0
    sget-object v4, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 2282
    sget-object v5, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 2284
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    :try_start_1
    sget-object v4, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_3

    .line 2286
    sget-object v4, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;

    .line 2288
    .local v2, "usbStorage":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->isMounted()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->getStoragePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2290
    const/4 v4, 0x1

    monitor-exit v5

    move v3, v4

    goto :goto_0

    .line 2284
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2293
    .end local v2    # "usbStorage":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    :cond_3
    monitor-exit v5

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v4
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 2297
    .end local v1    # "i":I
    :catch_0
    move-exception v0

    .line 2299
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 2302
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 2304
    .local v0, "e":Ljava/util/ConcurrentModificationException;
    invoke-virtual {v0}, Ljava/util/ConcurrentModificationException;->printStackTrace()V

    goto :goto_0

    .line 2307
    .end local v0    # "e":Ljava/util/ConcurrentModificationException;
    :catch_2
    move-exception v0

    .line 2309
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static isUserShortcutAdded(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 4406
    if-nez p1, :cond_1

    .line 4419
    :cond_0
    :goto_0
    return v7

    .line 4408
    :cond_1
    const-string v3, "_data = ? and type != 10"

    .line 4409
    .local v3, "where":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    aput-object p1, v4, v7

    .line 4410
    .local v4, "whereArgs":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 4411
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 4414
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 4415
    const/4 v7, 0x0

    .line 4418
    .local v7, "status":Z
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 4417
    .end local v7    # "status":Z
    :cond_2
    const/4 v7, 0x1

    .restart local v7    # "status":Z
    goto :goto_1
.end method

.method public static isUsingNavigation(I)Z
    .locals 2
    .param p0, "fragmentId"    # I

    .prologue
    .line 3832
    and-int/lit16 v0, p0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWifiOn(Landroid/content/Context;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    .line 4545
    const-string v6, "wifi"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/WifiManager;

    .line 4546
    .local v3, "wifiManager":Landroid/net/wifi/WifiManager;
    const-string v6, "wifip2p"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/wifi/p2p/WifiP2pManager;

    .line 4547
    .local v4, "wifiP2pManager":Landroid/net/wifi/p2p/WifiP2pManager;
    const-string v6, "connectivity"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 4550
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    .line 4551
    invoke-virtual {v0, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 4552
    .local v2, "netInfoWifi":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_1

    sget-object v6, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 4567
    .end local v2    # "netInfoWifi":Landroid/net/NetworkInfo;
    :cond_0
    :goto_0
    return v5

    .line 4559
    :cond_1
    if-eqz v4, :cond_2

    if-eqz v0, :cond_2

    .line 4560
    const/16 v6, 0xd

    invoke-virtual {v0, v6}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 4561
    .local v1, "netInfoP2p":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_2

    sget-object v6, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 4567
    .end local v1    # "netInfoP2p":Landroid/net/NetworkInfo;
    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static sendScan(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 2520
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_SCAN"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v2, "where"

    const-string v3, "myfiles"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2522
    .local v0, "intent":Landroid/content/Intent;
    if-eqz p0, :cond_0

    .line 2523
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2524
    :cond_0
    return-void
.end method

.method public static sendScanFile(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 2510
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "file://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2513
    .local v0, "intent":Landroid/content/Intent;
    if-eqz p0, :cond_0

    .line 2514
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2515
    :cond_0
    return-void
.end method

.method public static setCloudListShowing(Z)V
    .locals 0
    .param p0, "isShowing"    # Z

    .prologue
    .line 5018
    sput-boolean p0, Lcom/sec/android/app/myfiles/utils/Utils;->mIsShowing:Z

    .line 5019
    return-void
.end method

.method public static setDialogInstance(Lcom/sec/android/app/myfiles/utils/MyFilesDialog;)V
    .locals 0
    .param p0, "dialog"    # Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    .prologue
    .line 3552
    sput-object p0, Lcom/sec/android/app/myfiles/utils/Utils;->mDialog:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    .line 3553
    return-void
.end method

.method public static setEasyMode(Z)V
    .locals 0
    .param p0, "easyMode"    # Z

    .prologue
    .line 4786
    sput-boolean p0, Lcom/sec/android/app/myfiles/utils/Utils;->mEasyMode:Z

    .line 4787
    return-void
.end method

.method public static setKNOXInstalled(Z)V
    .locals 3
    .param p0, "State"    # Z

    .prologue
    .line 2084
    sput-boolean p0, Lcom/sec/android/app/myfiles/utils/Utils;->mKNOXInstalled:Z

    .line 2086
    const/4 v0, 0x0

    const-string v1, "Utils"

    const-string v2, "isKNOXInstalled() - setKNOXInstalled "

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 2087
    return-void
.end method

.method public static setKnoxinfoForAppBundle(Landroid/os/Bundle;)V
    .locals 0
    .param p0, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 5030
    sput-object p0, Lcom/sec/android/app/myfiles/utils/Utils;->mKnoxInfoForAppBundle:Landroid/os/Bundle;

    .line 5031
    return-void
.end method

.method public static setMediaScannerScannig(Z)V
    .locals 0
    .param p0, "scanning"    # Z

    .prologue
    .line 2411
    sput-boolean p0, Lcom/sec/android/app/myfiles/utils/Utils;->mIsMediaScannerScannig:Z

    .line 2412
    return-void
.end method

.method public static setMountedStateChange(Ljava/lang/String;Z)V
    .locals 3
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "mountState"    # Z

    .prologue
    .line 1693
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isInExternalSdStorage(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1695
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    .line 1713
    :cond_0
    :goto_0
    return-void

    .line 1697
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isInUsbHostStorage(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1699
    sget-object v2, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbStorage:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;

    .line 1701
    .local v1, "usb":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->getStoragePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1703
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->setMounted(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 1709
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "usb":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    :cond_3
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1711
    sput-boolean p1, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    goto :goto_0
.end method

.method public static setNotibarAutoShowing(Landroid/app/Activity;)V
    .locals 5
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 5006
    if-eqz p0, :cond_0

    .line 5007
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 5008
    .local v1, "window":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 5009
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 5010
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 5014
    .end local v0    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v1    # "window":Landroid/view/Window;
    :goto_0
    return-void

    .line 5012
    :cond_0
    const/4 v2, 0x0

    const-string v3, "Utils"

    const-string v4, "setNotibarAutoShowing activity is null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setSecretModeState(Z)V
    .locals 0
    .param p0, "State"    # Z

    .prologue
    .line 1718
    sput-boolean p0, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretModeOn:Z

    .line 1719
    return-void
.end method

.method public static setStorageUsageIsShowing(Landroid/content/Context;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isShowing"    # Z

    .prologue
    .line 5036
    if-eqz p0, :cond_0

    .line 5037
    if-eqz p1, :cond_1

    .line 5038
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mStorageUsageIsShowing:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5043
    :cond_0
    :goto_0
    return-void

    .line 5039
    :cond_1
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mStorageUsageIsShowing:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5040
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mStorageUsageIsShowing:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static shadeQuotes(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    const/16 v3, 0x27

    .line 916
    const/4 v0, 0x0

    .line 917
    .local v0, "pos":I
    if-eqz p0, :cond_0

    .line 918
    :goto_0
    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    if-lez v0, :cond_0

    .line 919
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 920
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 922
    :cond_0
    return-object p0
.end method

.method public static showNoPlayDuringCallToast(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 2745
    const v0, 0x7f0b007e

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2747
    return-void
.end method

.method public static unicodeToUTF16(Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p0, "unicode"    # Ljava/lang/String;

    .prologue
    .line 3384
    const-string v0, ""

    .line 3386
    .local v0, "convertedUTF16Str":Ljava/lang/String;
    invoke-static {p0}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 3388
    .local v9, "unicodeInteger":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v12

    add-int/lit8 v12, v12, -0x2

    const/4 v13, 0x4

    if-le v12, v13, :cond_0

    .line 3389
    const/high16 v12, 0x1f0000

    and-int/2addr v12, v9

    shr-int/lit8 v11, v12, 0x10

    .line 3390
    .local v11, "zzzzz":I
    add-int/lit8 v10, v11, -0x1

    .line 3392
    .local v10, "yyyy":I
    const/4 v5, 0x0

    .line 3393
    .local v5, "rear2byte":I
    const v6, 0xdc00

    .line 3394
    .local v6, "rear2byteFixed":I
    and-int/lit16 v12, v9, 0x3ff

    or-int v5, v6, v12

    .line 3395
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "0x"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 3397
    .local v8, "rear2byteString":Ljava/lang/String;
    const/4 v1, 0x0

    .line 3398
    .local v1, "former2byte":I
    const v2, 0xd800

    .line 3399
    .local v2, "former2byteFixed":I
    const v12, 0xfc00

    and-int/2addr v12, v9

    shr-int/lit8 v12, v12, 0xa

    or-int v1, v2, v12

    .line 3400
    shl-int/lit8 v12, v10, 0x6

    or-int/2addr v1, v12

    .line 3401
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "0x"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 3403
    .local v4, "former2byteString":Ljava/lang/String;
    invoke-static {v8}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 3404
    .local v7, "rear2byteInt":I
    invoke-static {v4}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 3406
    .local v3, "former2byteInt":I
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    int-to-char v13, v3

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v12

    int-to-char v13, v7

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3412
    .end local v1    # "former2byte":I
    .end local v2    # "former2byteFixed":I
    .end local v3    # "former2byteInt":I
    .end local v4    # "former2byteString":Ljava/lang/String;
    .end local v5    # "rear2byte":I
    .end local v6    # "rear2byteFixed":I
    .end local v7    # "rear2byteInt":I
    .end local v8    # "rear2byteString":Ljava/lang/String;
    .end local v10    # "yyyy":I
    .end local v11    # "zzzzz":I
    :goto_0
    return-object v0

    .line 3409
    :cond_0
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    int-to-char v13, v9

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static updateShortcut(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)I
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "index"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 4181
    const-string v3, "shortcut_index =  ? "

    .line 4182
    .local v3, "where":Ljava/lang/String;
    const/4 v5, 0x1

    new-array v4, v5, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    .line 4183
    .local v4, "whereArgs":[Ljava/lang/String;
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 4184
    .local v0, "arg1":Landroid/content/ContentValues;
    const-string v5, "title"

    invoke-virtual {v0, v5, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4185
    const-string v5, "_data"

    invoke-virtual {v0, v5, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4186
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;->getContentUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 4187
    .local v2, "updatedRowsCount":I
    if-lez v2, :cond_0

    .line 4188
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 4190
    .local v1, "intent":Landroid/content/Intent;
    const-string v5, "com.sec.android.action.SHORTCUT_ACTION"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 4192
    invoke-virtual {p0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 4194
    const-string v5, "Utils"

    const-string v6, "notify the shortcut was updated"

    invoke-static {v7, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 4197
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    return v2
.end method
