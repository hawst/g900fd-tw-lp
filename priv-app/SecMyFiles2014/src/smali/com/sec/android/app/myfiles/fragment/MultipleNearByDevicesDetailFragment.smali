.class public Lcom/sec/android/app/myfiles/fragment/MultipleNearByDevicesDetailFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment;
.source "MultipleNearByDevicesDetailFragment.java"


# static fields
.field private static final FILESIZE_TAG:Ljava/lang/String; = "filesize"


# instance fields
.field private mFileSize:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment;-><init>()V

    .line 27
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/fragment/MultipleNearByDevicesDetailFragment;->mFileSize:J

    return-void
.end method


# virtual methods
.method public getFileSize(Ljava/util/ArrayList;)J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 56
    .local p1, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/MultipleNearByDevicesDetailFragment;->mFileSize:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/MultipleNearByDevicesDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/sec/android/app/myfiles/utils/Utils;->getPathNearByDevicesFilesSize(Landroid/content/Context;Ljava/util/ArrayList;)J

    move-result-wide v0

    .line 60
    .local v0, "fileSize":J
    iput-wide v0, p0, Lcom/sec/android/app/myfiles/fragment/MultipleNearByDevicesDetailFragment;->mFileSize:J

    .line 66
    .end local v0    # "fileSize":J
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/fragment/MultipleNearByDevicesDetailFragment;->mFileSize:J

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment;->onCreate(Landroid/os/Bundle;)V

    .line 36
    if-eqz p1, :cond_0

    .line 38
    const-string v0, "filesize"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/fragment/MultipleNearByDevicesDetailFragment;->mFileSize:J

    .line 42
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 47
    const-string v0, "filesize"

    iget-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/MultipleNearByDevicesDetailFragment;->mFileSize:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 49
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 51
    return-void
.end method
