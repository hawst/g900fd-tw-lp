.class Lcom/sec/android/app/myfiles/fragment/FTPListFragment$1;
.super Ljava/lang/Object;
.source "FTPListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/FTPListFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/FTPListFragment;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 9
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 115
    .local p1, "parentView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPListFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v6}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 116
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPListFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->isSelectMode()Z

    move-result v6

    if-nez v6, :cond_1

    .line 117
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 118
    .local v0, "argument":Landroid/os/Bundle;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPListFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6, p3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    .line 119
    .local v1, "cursor":Landroid/database/Cursor;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Security;->getInstance()Lcom/sec/android/app/myfiles/utils/Security;

    move-result-object v6

    const-string v7, "_data"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/myfiles/utils/Security;->decrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 121
    .local v4, "ftpInfo":Ljava/lang/String;
    const-string v6, "FOLDERPATH"

    invoke-virtual {v0, v6, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPListFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v6, v6, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v6, :cond_0

    .line 123
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPListFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v6, Lcom/sec/android/app/myfiles/MainActivity;

    const/16 v7, 0xa

    invoke-virtual {v6, v7, v0}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    .line 135
    .end local v0    # "argument":Landroid/os/Bundle;
    .end local v1    # "cursor":Landroid/database/Cursor;
    .end local v4    # "ftpInfo":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPListFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6, p3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    .line 128
    .restart local v1    # "cursor":Landroid/database/Cursor;
    const-string v6, "_id"

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 129
    .local v2, "cursorId":J
    const-string v6, "_data"

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 130
    .local v5, "path":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPListFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6, v2, v3, v5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->multipleSelect(JLjava/lang/String;)V

    goto :goto_0

    .line 133
    .end local v1    # "cursor":Landroid/database/Cursor;
    .end local v2    # "cursorId":J
    .end local v5    # "path":Ljava/lang/String;
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPListFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPListFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v8, 0x7f0b00c7

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
