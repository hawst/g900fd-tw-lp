.class Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;
.super Ljava/lang/Object;
.source "AbsBrowserFragment.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V
    .locals 0

    .prologue
    .line 6082
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public refresh(Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 2
    .param p1, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    .line 6129
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->dismissDialog(I)V

    .line 6131
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNeedToRefresh:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectModeFrom()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 6133
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6135
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    .line 6140
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    if-eqz v0, :cond_1

    .line 6142
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 6145
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->invalidateOptionsMenu()V

    .line 6146
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mTempCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mTempCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 6147
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mTempCursor:Landroid/database/Cursor;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 6152
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNeedToRefresh:Z

    .line 6153
    return-void

    .line 6149
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->updateAdapter(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method public update(Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 3
    .param p1, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    .line 6087
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectModeFrom()I

    move-result v1

    if-nez v1, :cond_2

    .line 6089
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->isSelectMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6091
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    .line 6111
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    if-eqz v1, :cond_1

    .line 6113
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 6116
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->invalidateOptionsMenu()V

    .line 6118
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mTempCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_3

    .line 6119
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mTempCursor:Landroid/database/Cursor;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 6123
    :goto_1
    return-void

    .line 6096
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectionType()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectionType()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    .line 6099
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mOptionsMenu:Landroid/view/Menu;

    if-eqz v1, :cond_0

    .line 6101
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mOptionsMenu:Landroid/view/Menu;

    const v2, 0x7f0f0148

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 6103
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 6105
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 6121
    .end local v0    # "item":Landroid/view/MenuItem;
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->updateAdapter(Landroid/database/Cursor;)V

    goto :goto_1
.end method
