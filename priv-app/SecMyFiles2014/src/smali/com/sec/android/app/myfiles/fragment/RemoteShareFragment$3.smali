.class Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$3;
.super Ljava/lang/Object;
.source "RemoteShareFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->showDownloadLargeFilesDialog(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

.field final synthetic val$checkbox:Landroid/widget/CheckBox;

.field final synthetic val$selItemsInt:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;Landroid/widget/CheckBox;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 436
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iput-object p2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$3;->val$checkbox:Landroid/widget/CheckBox;

    iput-object p3, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$3;->val$selItemsInt:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 439
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$3;->val$checkbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 440
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$3;->val$selItemsInt:Ljava/util/ArrayList;

    # invokes: Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->menuDownload(Ljava/util/ArrayList;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;Ljava/util/ArrayList;)V

    .line 444
    :goto_0
    return-void

    .line 442
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v1, 0x7f0b013a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
