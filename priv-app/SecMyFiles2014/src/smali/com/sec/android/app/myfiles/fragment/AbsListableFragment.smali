.class public abstract Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsFragment;
.source "AbsListableFragment.java"


# static fields
.field private static final DIRECT_DRAG_FIRE_TIME:I = 0x3e8

.field private static final MODULE:Ljava/lang/String; = "AbsListableFragment"

.field private static SINE_IN_OUT_70:Landroid/view/animation/Interpolator;


# instance fields
.field private isCanceled:Z

.field protected mAbsBaseAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;

.field protected mCategoryTitleContainer:Landroid/view/View;

.field private mContainerPaddingLeft:I

.field protected mCreateFolder:Landroid/widget/LinearLayout;

.field public mCreateFolderContainer:Landroid/widget/LinearLayout;

.field protected mCurrentInOrder:I

.field protected mCurrentListableView:Lcom/sec/android/app/myfiles/view/IDirectDragable;

.field protected mCurrentSortBy:I

.field protected mEmptyView:Landroid/view/View;

.field protected mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

.field private mIsInEditMode:Z

.field private mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mItemLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

.field protected mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

.field private mMultiWindowStatusChangeReceiver:Landroid/content/BroadcastReceiver;

.field protected mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

.field protected mPathIndicatorContainer:Landroid/widget/LinearLayout;

.field private mPrevListGridState:Landroid/os/Parcelable;

.field public mRadioClickListener:Landroid/view/View$OnClickListener;

.field private mShowTreeView:Z

.field private mSortByReceiver:Landroid/content/BroadcastReceiver;

.field private mSplitViewLeftPanel:Landroid/view/ViewGroup;

.field protected mTitleBarTextView:Landroid/widget/TextView;

.field protected mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

.field protected mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

.field private mTreeViewItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/TreeViewItem;",
            ">;"
        }
    .end annotation
.end field

.field private mTreeViewSelectedPosition:I

.field private mViewKeyListener:Landroid/view/View$OnKeyListener;

.field protected mViewMode:I

.field private toCheckInOrder:Landroid/widget/RadioButton;

.field private toCheckSortBy:Landroid/widget/RadioButton;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    new-instance v0, Landroid/view/animation/interpolator/SineInOut70;

    invoke-direct {v0}, Landroid/view/animation/interpolator/SineInOut70;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->SINE_IN_OUT_70:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 71
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;-><init>()V

    .line 105
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->isCanceled:Z

    .line 107
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mIsInEditMode:Z

    .line 121
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mShowTreeView:Z

    .line 131
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeViewItems:Ljava/util/ArrayList;

    .line 1004
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$14;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$14;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mViewKeyListener:Landroid/view/View$OnKeyListener;

    .line 1052
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$15;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$15;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mMultiWindowStatusChangeReceiver:Landroid/content/BroadcastReceiver;

    .line 1090
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$16;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$16;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mRadioClickListener:Landroid/view/View$OnClickListener;

    .line 1111
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$17;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$17;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mSortByReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;Landroid/widget/RadioButton;)Landroid/widget/RadioButton;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;
    .param p1, "x1"    # Landroid/widget/RadioButton;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->toCheckSortBy:Landroid/widget/RadioButton;

    return-object p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;Landroid/widget/RadioButton;)Landroid/widget/RadioButton;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;
    .param p1, "x1"    # Landroid/widget/RadioButton;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->toCheckInOrder:Landroid/widget/RadioButton;

    return-object p1
.end method

.method static synthetic access$202(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->isCanceled:Z

    return p1
.end method

.method static synthetic access$302(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mIsInEditMode:Z

    return p1
.end method

.method static synthetic access$402(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;
    .param p1, "x1"    # I

    .prologue
    .line 71
    iput p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mContainerPaddingLeft:I

    return p1
.end method

.method static synthetic access$500()Landroid/view/animation/Interpolator;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->SINE_IN_OUT_70:Landroid/view/animation/Interpolator;

    return-object v0
.end method


# virtual methods
.method public addCurrentFolderToShortcut()V
    .locals 0

    .prologue
    .line 1135
    return-void
.end method

.method public addShortcutToHome()V
    .locals 0

    .prologue
    .line 1139
    return-void
.end method

.method protected changeViewMode(I)V
    .locals 5
    .param p1, "mode"    # I

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 685
    iput p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mViewMode:I

    .line 687
    packed-switch p1, :pswitch_data_0

    .line 722
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->setBrowserAdapterViewMode(I)V

    .line 723
    return-void

    .line 692
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setVisibility(I)V

    .line 694
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->setVisibility(I)V

    .line 696
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 698
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 700
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCurrentListableView:Lcom/sec/android/app/myfiles/view/IDirectDragable;

    goto :goto_0

    .line 706
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v0, :cond_0

    .line 707
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v1, "THUM"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/Utils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 709
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setVisibility(I)V

    .line 711
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->setVisibility(I)V

    .line 713
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 715
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 717
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCurrentListableView:Lcom/sec/android/app/myfiles/view/IDirectDragable;

    goto :goto_0

    .line 687
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public finishSelectMode()V
    .locals 1

    .prologue
    .line 997
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->finishSelectMode()V

    .line 998
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/MyFilesFeatures;->FEATURE_SUPPORT_LIST_ANIMATION:Z

    if-eqz v0, :cond_0

    .line 999
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->setEditMode(Z)V

    .line 1002
    :cond_0
    return-void
.end method

.method protected abstract getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;",
            ">()TT;"
        }
    .end annotation
.end method

.method public handleAddShortcutRequest()V
    .locals 0

    .prologue
    .line 1137
    return-void
.end method

.method protected hideCheckBox()V
    .locals 5

    .prologue
    .line 886
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    if-eqz v3, :cond_1

    .line 966
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mIsInEditMode:Z

    .line 968
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getChildCount()I

    move-result v1

    .line 969
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 970
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0f0037

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 971
    .local v0, "chb":Landroid/view/View;
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 972
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 969
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 974
    .end local v0    # "chb":Landroid/view/View;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->invalidate()V

    .line 976
    .end local v1    # "childCount":I
    .end local v2    # "i":I
    :cond_1
    return-void
.end method

.method protected initTreeView()V
    .locals 5

    .prologue
    .line 729
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onCreateTreeViewItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    .line 731
    .local v1, "treeViewItemClickListener":Landroid/widget/AdapterView$OnItemClickListener;
    if-eqz v1, :cond_0

    .line 733
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 736
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getTAdapter()Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    move-result-object v2

    if-nez v2, :cond_1

    .line 737
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v3, 0x201

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;-><init>(Landroid/content/Context;I)V

    .line 738
    .local v0, "mNavigation":Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;
    new-instance v2, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v2, v3, v0}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    .line 739
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentSortBy()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentInOrder()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setSortBy(II)V

    .line 740
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setAutoExpand(Z)V

    .line 741
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 744
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->setNavigation(Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 745
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setTAdapter(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)V

    .line 752
    .end local v0    # "mNavigation":Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setTreeView(Lcom/sec/android/app/myfiles/view/MyFilesTreeView;)V

    .line 759
    return-void

    .line 747
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getTAdapter()Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    .line 748
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method protected isShowTreeView()Z
    .locals 1

    .prologue
    .line 798
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mShowTreeView:Z

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v0, 0x0

    .line 381
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 383
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isNormalWindow()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isScaleWindow()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 385
    :cond_0
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mRunFrom:I

    const/16 v2, 0x15

    if-eq v1, v2, :cond_2

    .line 386
    sget-boolean v1, Lcom/sec/android/app/myfiles/utils/Utils;->USE_SPLIT:Z

    if-eqz v1, :cond_3

    .line 387
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->showTreeView(Z)V

    .line 393
    :cond_2
    :goto_0
    return-void

    .line 389
    :cond_3
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->showTreeView(Z)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 139
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 142
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mAbsBaseAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mSelectedItemChangeListener:Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mAbsBaseAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mAbsBaseAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mSelectedItemChangeListener:Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->setSelectedItemChangeListener(Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;)V

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mMultiWindowStatusChangeReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.sec.android.action.NOTIFY_MULTIWINDOW_STATUS"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentSortBy()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCurrentSortBy:I

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentInOrder()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCurrentInOrder:I

    .line 155
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 12
    .param p1, "id"    # I

    .prologue
    const v11, 0x7f0b0017

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v5

    .line 401
    .local v5, "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    new-instance v8, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v8, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 403
    .local v8, "dialog":Landroid/app/AlertDialog$Builder;
    sparse-switch p1, :sswitch_data_0

    .line 679
    :goto_0
    invoke-virtual {v8}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 407
    :sswitch_0
    const v9, 0x7f070001

    .line 415
    .local v9, "itemId":I
    const v0, 0x7f0b001b

    invoke-virtual {v8, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 417
    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentView()I

    move-result v0

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$2;

    invoke-direct {v1, p0, v5}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;Lcom/sec/android/app/myfiles/utils/SharedDataStore;)V

    invoke-virtual {v8, v9, v0, v1}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 430
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;)V

    invoke-virtual {v8, v11, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 439
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$4;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;)V

    invoke-virtual {v8, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 453
    .end local v9    # "itemId":I
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040049

    const/4 v10, 0x0

    invoke-virtual {v0, v1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 455
    .local v2, "custom":Landroid/view/View;
    const v0, 0x7f0f0103

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioGroup;

    .line 457
    .local v3, "sortByRadioGroup":Landroid/widget/RadioGroup;
    const v6, 0x7f0f0105

    .line 459
    .local v6, "currentSortByRadio":I
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCurrentSortBy:I

    packed-switch v0, :pswitch_data_0

    .line 478
    :goto_1
    invoke-virtual {v3, v6}, Landroid/widget/RadioGroup;->check(I)V

    .line 480
    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->sortbyRadioClick(Landroid/widget/RadioGroup;)V

    .line 482
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$5;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;)V

    invoke-virtual {v3, v0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 515
    const v0, 0x7f0f0108

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioGroup;

    .line 517
    .local v4, "inOrderRadioGroup":Landroid/widget/RadioGroup;
    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->orderbyRadioClick(Landroid/widget/RadioGroup;)V

    .line 519
    const v7, 0x7f0f0109

    .line 521
    .local v7, "currentinOrderRadio":I
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCurrentInOrder:I

    packed-switch v0, :pswitch_data_1

    .line 532
    :goto_2
    invoke-virtual {v4, v7}, Landroid/widget/RadioGroup;->check(I)V

    .line 534
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$6;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;)V

    invoke-virtual {v4, v0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 559
    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->toCheckSortBy:Landroid/widget/RadioButton;

    .line 561
    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->toCheckInOrder:Landroid/widget/RadioButton;

    .line 563
    const v0, 0x7f0b001c

    invoke-virtual {v8, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 565
    invoke-virtual {v8, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 567
    const v10, 0x7f0b0015

    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;Landroid/view/View;Landroid/widget/RadioGroup;Landroid/widget/RadioGroup;Lcom/sec/android/app/myfiles/utils/SharedDataStore;)V

    invoke-virtual {v8, v10, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 598
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$8;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;)V

    invoke-virtual {v8, v11, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 616
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$9;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;)V

    invoke-virtual {v8, v0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_0

    .line 462
    .end local v4    # "inOrderRadioGroup":Landroid/widget/RadioGroup;
    .end local v7    # "currentinOrderRadio":I
    :pswitch_0
    const v6, 0x7f0f0105

    .line 463
    goto :goto_1

    .line 466
    :pswitch_1
    const v6, 0x7f0f0104

    .line 467
    goto :goto_1

    .line 470
    :pswitch_2
    const v6, 0x7f0f0106

    .line 471
    goto :goto_1

    .line 474
    :pswitch_3
    const v6, 0x7f0f0107

    goto :goto_1

    .line 524
    .restart local v4    # "inOrderRadioGroup":Landroid/widget/RadioGroup;
    .restart local v7    # "currentinOrderRadio":I
    :pswitch_4
    const v7, 0x7f0f0109

    .line 525
    goto :goto_2

    .line 528
    :pswitch_5
    const v7, 0x7f0f010a

    goto :goto_2

    .line 631
    .end local v2    # "custom":Landroid/view/View;
    .end local v3    # "sortByRadioGroup":Landroid/widget/RadioGroup;
    .end local v4    # "inOrderRadioGroup":Landroid/widget/RadioGroup;
    .end local v6    # "currentSortByRadio":I
    .end local v7    # "currentinOrderRadio":I
    :sswitch_2
    const v0, 0x7f0b005c

    invoke-virtual {v8, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 633
    const v0, 0x7f070007

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$10;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$10;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;)V

    invoke-virtual {v8, v0, v1}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 658
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$11;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;)V

    invoke-virtual {v8, v11, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 667
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$12;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;)V

    invoke-virtual {v8, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_0

    .line 403
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x8 -> :sswitch_2
    .end sparse-switch

    .line 459
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 521
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method protected abstract onCreateTreeViewItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v3, 0x8

    .line 161
    const v1, 0x7f040011

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 163
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f0f0055

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/view/MyFilesListView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    .line 165
    const v1, 0x7f0f0056

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    .line 167
    const v1, 0x7f0f004b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCreateFolder:Landroid/widget/LinearLayout;

    .line 169
    const v1, 0x7f0f004a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCreateFolderContainer:Landroid/widget/LinearLayout;

    .line 171
    const v1, 0x7f0f0057

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mEmptyView:Landroid/view/View;

    .line 173
    const v1, 0x7f0f0044

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/view/PathIndicator;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    .line 175
    const v1, 0x7f0f0043

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mPathIndicatorContainer:Landroid/widget/LinearLayout;

    .line 177
    const v1, 0x7f0f0045

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCategoryTitleContainer:Landroid/view/View;

    .line 179
    const v1, 0x7f0f0046

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTitleBarTextView:Landroid/widget/TextView;

    .line 181
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 183
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mItemLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 185
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mPathIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 187
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setVisibility(I)V

    .line 189
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCategoryTitleContainer:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 191
    const v1, 0x7f0f003f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    .line 195
    const v1, 0x7f0f003e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mSplitViewLeftPanel:Landroid/view/ViewGroup;

    .line 197
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    if-eqz v1, :cond_0

    .line 199
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 204
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mViewKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 206
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mViewKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 208
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mItemLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    if-eqz v1, :cond_1

    .line 210
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mItemLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 212
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mItemLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 215
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->initTreeView()V

    .line 217
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCreateFolder:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 228
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 304
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setTAdapter(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)V

    .line 305
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onDestroy()V

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mMultiWindowStatusChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 308
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 340
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 374
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    .line 344
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    goto :goto_0

    .line 351
    :sswitch_1
    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->showDialog(I)V

    goto :goto_0

    .line 358
    :sswitch_2
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->showDialog(I)V

    .line 360
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->isCanceled:Z

    if-eqz v1, :cond_0

    .line 362
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->toCheckSortBy:Landroid/widget/RadioButton;

    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 364
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->toCheckInOrder:Landroid/widget/RadioButton;

    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 366
    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->isCanceled:Z

    goto :goto_0

    .line 340
    :sswitch_data_0
    .sparse-switch
        0x7f0f0136 -> :sswitch_0
        0x7f0f013b -> :sswitch_1
        0x7f0f013c -> :sswitch_2
    .end sparse-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 286
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onPause()V

    .line 288
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mViewMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mPrevListGridState:Landroid/os/Parcelable;

    .line 298
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mSortByReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 299
    return-void

    .line 293
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mPrevListGridState:Landroid/os/Parcelable;

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x0

    .line 313
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 315
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mAbsBaseAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->getCount()I

    move-result v2

    if-nez v2, :cond_0

    .line 317
    if-nez p1, :cond_1

    .line 334
    :cond_0
    :goto_0
    return-void

    .line 322
    :cond_1
    const v2, 0x7f0f0137

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 323
    .local v0, "item":Landroid/view/MenuItem;
    const v2, 0x7f0f0138

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 325
    .local v1, "normalDeleteItem":Landroid/view/MenuItem;
    if-eqz v0, :cond_2

    .line 327
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 330
    :cond_2
    if-eqz v1, :cond_0

    .line 331
    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 264
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onResume()V

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mPrevListGridState:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    .line 268
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mViewMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mPrevListGridState:Landroid/os/Parcelable;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 279
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mSortByReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.sec.android.app.myfiles.SORTBY_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 281
    return-void

    .line 274
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mPrevListGridState:Landroid/os/Parcelable;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v0, 0x0

    .line 235
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 237
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mAbsBaseAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->setListView(Lcom/sec/android/app/myfiles/view/MyFilesListView;)V

    .line 239
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentView()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->changeViewMode(I)V

    .line 241
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentSortBy()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCurrentSortBy:I

    .line 243
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentInOrder()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCurrentInOrder:I

    .line 245
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isNormalWindow()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 247
    :cond_0
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mRunFrom:I

    const/16 v2, 0x15

    if-eq v1, v2, :cond_2

    .line 248
    sget-boolean v1, Lcom/sec/android/app/myfiles/utils/Utils;->USE_SPLIT:Z

    if-eqz v1, :cond_3

    .line 249
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->showTreeView(Z)V

    .line 256
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->restoreTreeYPosition()V

    .line 258
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onRefresh()V

    .line 259
    return-void

    .line 251
    :cond_3
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->showTreeView(Z)V

    goto :goto_0
.end method

.method public orderbyRadioClick(Landroid/widget/RadioGroup;)V
    .locals 3
    .param p1, "radioGroup"    # Landroid/widget/RadioGroup;

    .prologue
    .line 1084
    const v2, 0x7f0f0109

    invoke-virtual {p1, v2}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 1085
    .local v0, "ascendingRadio":Landroid/widget/RadioButton;
    const v2, 0x7f0f010a

    invoke-virtual {p1, v2}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    .line 1086
    .local v1, "descendingRadio":Landroid/widget/RadioButton;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mRadioClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1087
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mRadioClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1088
    return-void
.end method

.method public refreshCategoryTree()V
    .locals 1

    .prologue
    .line 762
    instance-of v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    if-nez v0, :cond_0

    .line 763
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->initTreeView()V

    .line 765
    :cond_0
    return-void
.end method

.method public restoreTreeYPosition()V
    .locals 3

    .prologue
    .line 1105
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    if-eqz v0, :cond_0

    .line 1106
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->getTreeViewYPos()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->getTreeViewYOffset()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->setSelectionFromTop(II)V

    .line 1107
    :cond_0
    return-void
.end method

.method public saveTreeYPosition()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1097
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    if-eqz v2, :cond_0

    .line 1098
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->getFirstVisiblePosition()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setTreeViewYPos(I)V

    .line 1099
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1100
    .local v0, "v":Landroid/view/View;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    if-nez v0, :cond_1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setTreeViewYOffset(I)V

    .line 1102
    .end local v0    # "v":Landroid/view/View;
    :cond_0
    return-void

    .line 1100
    .restart local v0    # "v":Landroid/view/View;
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    goto :goto_0
.end method

.method protected setEditMode(Z)V
    .locals 1
    .param p1, "isInEditMode"    # Z

    .prologue
    .line 980
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mIsInEditMode:Z

    if-nez v0, :cond_0

    .line 981
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->showCheckBox()V

    .line 983
    :cond_0
    if-nez p1, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mIsInEditMode:Z

    if-eqz v0, :cond_1

    .line 984
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->hideCheckBox()V

    .line 986
    :cond_1
    return-void
.end method

.method protected showCheckBox()V
    .locals 5

    .prologue
    .line 803
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    if-eqz v3, :cond_1

    .line 804
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$13;

    invoke-direct {v4, p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$13;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;)V

    invoke-virtual {v3, v4}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 874
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getChildCount()I

    move-result v1

    .line 875
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 876
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0f0037

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 877
    .local v0, "chb":Landroid/view/View;
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 878
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 875
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 880
    .end local v0    # "chb":Landroid/view/View;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->invalidate()V

    .line 882
    .end local v1    # "childCount":I
    .end local v2    # "i":I
    :cond_1
    return-void
.end method

.method protected showTreeView(Z)V
    .locals 3
    .param p1, "show"    # Z

    .prologue
    const/16 v2, 0x8

    .line 770
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mSplitViewLeftPanel:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 793
    :goto_0
    return-void

    .line 775
    :cond_0
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mFragmentId:I

    const/16 v1, 0x12

    if-ne v0, v1, :cond_1

    .line 777
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mSplitViewLeftPanel:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 783
    :cond_1
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mShowTreeView:Z

    .line 785
    if-eqz p1, :cond_2

    .line 787
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mSplitViewLeftPanel:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 791
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mSplitViewLeftPanel:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public sortbyRadioClick(Landroid/widget/RadioGroup;)V
    .locals 5
    .param p1, "radioGroup"    # Landroid/widget/RadioGroup;

    .prologue
    .line 1073
    const v4, 0x7f0f0104

    invoke-virtual {p1, v4}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    .line 1074
    .local v2, "timeRadio":Landroid/widget/RadioButton;
    const v4, 0x7f0f0105

    invoke-virtual {p1, v4}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    .line 1075
    .local v3, "typeRadio":Landroid/widget/RadioButton;
    const v4, 0x7f0f0106

    invoke-virtual {p1, v4}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 1076
    .local v0, "nameRadio":Landroid/widget/RadioButton;
    const v4, 0x7f0f0107

    invoke-virtual {p1, v4}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    .line 1077
    .local v1, "sizeRadio":Landroid/widget/RadioButton;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mRadioClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1078
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mRadioClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1079
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mRadioClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1080
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mRadioClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1081
    return-void
.end method

.method public startSelectMode(III)V
    .locals 1
    .param p1, "selectMode"    # I
    .param p2, "initialSelectedPosition"    # I
    .param p3, "from"    # I

    .prologue
    .line 989
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->startSelectMode(III)V

    .line 991
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/MyFilesFeatures;->FEATURE_SUPPORT_LIST_ANIMATION:Z

    if-eqz v0, :cond_0

    .line 992
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->setEditMode(Z)V

    .line 994
    :cond_0
    return-void
.end method
