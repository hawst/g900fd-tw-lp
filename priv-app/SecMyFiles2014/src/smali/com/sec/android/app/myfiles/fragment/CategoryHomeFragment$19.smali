.class Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$19;
.super Ljava/lang/Object;
.source "CategoryHomeFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

.field final synthetic val$dialogId:I

.field final synthetic val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;ILcom/sec/android/app/myfiles/utils/SharedDataStore;)V
    .locals 0

    .prologue
    .line 1922
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$19;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iput p2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$19;->val$dialogId:I

    iput-object p3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$19;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 1927
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 1929
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$19;->val$dialogId:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$19;->val$dialogId:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 1932
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$19;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$19;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->bDoNotCheckDropBox:Z
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1700(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setDoNotShowCheckDropBox(Z)V

    .line 1933
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$19;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->commitShowDataWarning()Z

    .line 1935
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$19;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_1

    .line 1937
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$19;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setStartBrowserState(Z)V

    .line 1939
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$19;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->bDropBoxFromShortcut:Z

    if-eqz v0, :cond_2

    .line 1942
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$19;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->startBrowserFTPorDropBox()V

    .line 1959
    :cond_1
    :goto_0
    return-void

    .line 1946
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$19;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$19;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mModeForWarningDialog:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 1950
    :cond_3
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$19;->val$dialogId:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 1952
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$19;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$19;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->bDoNotCheckFTP:Z
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1800(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setDoNotShowCheckFTP(Z)V

    .line 1953
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$19;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->commitShowDataWarning()Z

    .line 1956
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$19;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->startBrowserFTPorDropBox()V

    goto :goto_0
.end method
