.class Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$1;
.super Ljava/lang/Object;
.source "VideoHoverPopup.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 113
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;

    # setter for: Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->surfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0, p1}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->access$002(Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->showContent()V

    .line 118
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 4
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    const/4 v3, 0x0

    .line 122
    const/4 v0, 0x2

    const-string v1, "VideoHoverPopup"

    const-string v2, "surfaceDestroyed ~ !!!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;

    # setter for: Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->surfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0, v3}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->access$002(Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;

    iput-object v3, v0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 131
    :cond_0
    return-void
.end method
