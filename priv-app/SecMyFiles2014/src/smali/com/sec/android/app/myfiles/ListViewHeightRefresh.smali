.class public Lcom/sec/android/app/myfiles/ListViewHeightRefresh;
.super Ljava/lang/Object;
.source "ListViewHeightRefresh.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getListViewSize(Landroid/widget/ListView;)V
    .locals 7
    .param p0, "myListView"    # Landroid/widget/ListView;

    .prologue
    const/4 v6, 0x0

    .line 19
    invoke-virtual {p0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    .line 21
    .local v1, "myListAdapter":Landroid/widget/ListAdapter;
    if-nez v1, :cond_0

    .line 77
    :goto_0
    return-void

    .line 29
    :cond_0
    const/4 v4, 0x0

    .local v4, "totalHeight":I
    const/4 v2, 0x0

    .line 31
    .local v2, "numCount":I
    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    .line 33
    if-lez v2, :cond_1

    .line 34
    const/4 v5, 0x0

    invoke-interface {v1, v6, v5, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 35
    .local v0, "listItem":Landroid/view/View;
    invoke-virtual {v0, v6, v6}, Landroid/view/View;->measure(II)V

    .line 36
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    mul-int v4, v5, v2

    .line 73
    .end local v0    # "listItem":Landroid/view/View;
    :cond_1
    invoke-virtual {p0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 75
    .local v3, "params":Landroid/view/ViewGroup$LayoutParams;
    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0
.end method
