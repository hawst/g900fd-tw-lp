.class Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask$1;
.super Landroid/os/Handler;
.source "RemoteShareDownloadTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->createHandler()Landroid/os/Handler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 136
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 140
    .local v0, "data":Landroid/os/Bundle;
    iget v3, p1, Landroid/os/Message;->what:I

    const/16 v8, 0x7d1

    if-ne v3, v8, :cond_0

    .line 143
    iget v3, p1, Landroid/os/Message;->arg1:I

    packed-switch v3, :pswitch_data_0

    .line 168
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;

    iget v8, p1, Landroid/os/Message;->arg1:I

    # setter for: Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mErrorCode:I
    invoke-static {v3, v8}, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->access$402(Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;I)I

    .line 169
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mLock:Ljava/util/concurrent/locks/Lock;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->access$100(Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;)Ljava/util/concurrent/locks/Lock;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 170
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mDownloadedCondition:Ljava/util/concurrent/locks/Condition;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->access$200(Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;)Ljava/util/concurrent/locks/Condition;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/concurrent/locks/Condition;->signal()V

    .line 171
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mLock:Ljava/util/concurrent/locks/Lock;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->access$100(Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;)Ljava/util/concurrent/locks/Lock;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 172
    iget v3, p1, Landroid/os/Message;->arg1:I

    if-gez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_0

    .line 173
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mContext:Landroid/content/Context;

    const v8, 0x7f0b00ad

    invoke-static {v3, v8, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 145
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iput v10, v3, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mCurrentTargetPercentage:I

    .line 146
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;

    new-array v8, v9, [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;

    iget-object v9, v9, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    aput-object v9, v8, v10

    # invokes: Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->publishProgress([Ljava/lang/Object;)V
    invoke-static {v3, v8}, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->access$000(Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;[Ljava/lang/Object;)V

    goto :goto_0

    .line 149
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mContext:Landroid/content/Context;

    sget-object v8, Lcom/sec/android/app/myfiles/utils/Constant;->RSHARE_DOWNLOAD_DIR_URI:Ljava/lang/String;

    invoke-static {v3, v8}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    .line 150
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mLock:Ljava/util/concurrent/locks/Lock;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->access$100(Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;)Ljava/util/concurrent/locks/Lock;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 151
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mDownloadedCondition:Ljava/util/concurrent/locks/Condition;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->access$200(Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;)Ljava/util/concurrent/locks/Condition;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/concurrent/locks/Condition;->signal()V

    .line 152
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mLock:Ljava/util/concurrent/locks/Lock;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->access$100(Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;)Ljava/util/concurrent/locks/Lock;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_0

    .line 156
    :pswitch_2
    const-string v3, "extra_progress_byte"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 157
    .local v4, "progressByte":J
    const-string v3, "extra_progress_file_name"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 158
    .local v1, "filename":Ljava/lang/String;
    const-string v3, "extra_progress_total_file_length"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 159
    .local v6, "totfileSize":J
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mFileName:Landroid/widget/TextView;

    if-eqz v3, :cond_2

    .line 160
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mFileName:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    :cond_2
    long-to-float v3, v4

    const/high16 v8, 0x42c80000    # 100.0f

    mul-float/2addr v3, v8

    long-to-float v8, v6

    div-float/2addr v3, v8

    float-to-int v2, v3

    .line 163
    .local v2, "percent":I
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iput v2, v3, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mCurrentTargetPercentage:I

    .line 164
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;

    new-array v8, v9, [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;

    iget-object v9, v9, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    aput-object v9, v8, v10

    # invokes: Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->publishProgress([Ljava/lang/Object;)V
    invoke-static {v3, v8}, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->access$300(Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 143
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
