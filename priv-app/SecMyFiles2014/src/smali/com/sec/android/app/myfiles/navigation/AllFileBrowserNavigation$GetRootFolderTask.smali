.class Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$GetRootFolderTask;
.super Landroid/os/AsyncTask;
.source "AllFileBrowserNavigation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GetRootFolderTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$GetRootFolderTask;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Landroid/database/Cursor;
    .locals 8
    .param p1, "params"    # [Ljava/lang/Object;

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$GetRootFolderTask;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    # getter for: Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mContentResolver:Landroid/content/ContentResolver;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->access$200(Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$GetRootFolderTask;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    # getter for: Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mFileUri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->access$000(Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;)Landroid/net/Uri;

    move-result-object v1

    # getter for: Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mFolderProjection:[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->access$100()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "_data LIKE ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v7, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$GetRootFolderTask;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 180
    .local v6, "cursor":Landroid/database/Cursor;
    return-object v6
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 172
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$GetRootFolderTask;->doInBackground([Ljava/lang/Object;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 186
    if-eqz p1, :cond_0

    .line 187
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$GetRootFolderTask;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isAndroidFolder(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 189
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 192
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$GetRootFolderTask;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getRootFolder()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$GetRootFolderTask;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->notifyUpdate(Z)V

    .line 195
    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 172
    check-cast p1, Landroid/database/Cursor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$GetRootFolderTask;->onPostExecute(Landroid/database/Cursor;)V

    return-void
.end method
