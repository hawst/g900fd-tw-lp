.class public Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
.source "RemoteShareListAdapter.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mContext:Landroid/content/Context;

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mItems:Ljava/util/ArrayList;

    .line 29
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 30
    .local v6, "res":Landroid/content/res/Resources;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isRemoteShareEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mItems:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    const/16 v1, 0x26

    const v2, 0x7f0b0009

    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 32
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mItems:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    const/16 v1, 0x27

    const v2, 0x7f0b000a

    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    const/4 v5, 0x2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    :cond_0
    return-void
.end method


# virtual methods
.method public finishSelectMode()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mIsSelectMode:Z

    .line 54
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->notifyDataSetChanged()V

    .line 55
    return-void
.end method

.method public getCount()I
    .locals 3

    .prologue
    .line 92
    const/4 v0, 0x0

    .line 93
    .local v0, "value":I
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v2

    .line 94
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 95
    monitor-exit v2

    .line 96
    return v0

    .line 95
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 101
    const/4 v1, 0x0

    .line 103
    .local v1, "obj":Ljava/lang/Object;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 105
    monitor-exit v3

    .line 109
    .end local v1    # "obj":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 105
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 106
    :catch_0
    move-exception v0

    .line 107
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 114
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getSelectedItem()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSelectedItemCount()I
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return v0
.end method

.method public getSelectedItemPosition()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    const/4 v0, 0x0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x0

    .line 119
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mContext:Landroid/content/Context;

    const-string v8, "layout_inflater"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 120
    .local v1, "inflater":Landroid/view/LayoutInflater;
    if-nez p2, :cond_0

    .line 121
    const v7, 0x7f040031

    invoke-virtual {v1, v7, p3, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 123
    :cond_0
    const v7, 0x7f0f00c4

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 124
    .local v6, "v":Landroid/view/View;
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090013

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    iput v8, v7, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 125
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 126
    .local v2, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    const v7, 0x7f0f0031

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 127
    .local v0, "icon":Landroid/widget/ImageView;
    const v7, 0x7f0f0032

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 128
    .local v5, "title":Landroid/widget/TextView;
    const v7, 0x7f0f0067

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    .line 129
    .local v4, "sizeContainer":Landroid/widget/RelativeLayout;
    const v7, 0x7f0f0037

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    .line 130
    .local v3, "mSelectCheckBox":Landroid/widget/CheckBox;
    invoke-virtual {v3, v11}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 131
    invoke-virtual {v4, v11}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 132
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v7

    const/16 v8, 0x26

    if-ne v7, v8, :cond_2

    .line 133
    const v7, 0x7f02002d

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 137
    :cond_1
    :goto_0
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    iget-boolean v7, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mIsSelectMode:Z

    if-eqz v7, :cond_3

    .line 140
    invoke-virtual {p2, v10}, Landroid/view/View;->setEnabled(Z)V

    .line 141
    const v7, 0x3ecccccd    # 0.4f

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 142
    const v7, -0x777778

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 149
    :goto_1
    return-object p2

    .line 134
    :cond_2
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v7

    const/16 v8, 0x27

    if-ne v7, v8, :cond_1

    .line 135
    const v7, 0x7f02002e

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 144
    :cond_3
    const/4 v7, 0x1

    invoke-virtual {p2, v7}, Landroid/view/View;->setEnabled(Z)V

    .line 145
    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 146
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f080019

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mIsSelectMode:Z

    if-nez v0, :cond_0

    .line 167
    const/4 v0, 0x1

    .line 169
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSelectMode()Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mIsSelectMode:Z

    return v0
.end method

.method public notifyDataSetChanged()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 156
    .local v6, "res":Landroid/content/res/Resources;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isRemoteShareEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mItems:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    const/16 v1, 0x26

    const v2, 0x7f0b0009

    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 158
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mItems:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    const/16 v1, 0x27

    const v2, 0x7f0b000a

    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    const/4 v5, 0x2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->notifyDataSetChanged()V

    .line 161
    return-void
.end method

.method public onRefresh()V
    .locals 0

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->notifyDataSetChanged()V

    .line 47
    return-void
.end method

.method public selectAllItem()V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public selectItem(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 65
    return-void
.end method

.method public selectMode(Z)V
    .locals 0
    .param p1, "selectMode"    # Z

    .prologue
    .line 175
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mIsSelectMode:Z

    .line 176
    return-void
.end method

.method public startSelectMode(II)V
    .locals 1
    .param p1, "selectType"    # I
    .param p2, "from"    # I

    .prologue
    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->mIsSelectMode:Z

    .line 41
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->notifyDataSetChanged()V

    .line 42
    return-void
.end method

.method public unselectAllItem()V
    .locals 0

    .prologue
    .line 73
    return-void
.end method
