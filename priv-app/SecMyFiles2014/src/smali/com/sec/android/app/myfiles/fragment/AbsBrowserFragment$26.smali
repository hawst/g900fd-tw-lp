.class Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;
.super Ljava/lang/Object;
.source "AbsBrowserFragment.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreateSelectedItemChangeListener()Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V
    .locals 0

    .prologue
    .line 4962
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public multipleSelectionChanged(II)V
    .locals 24
    .param p1, "totalItems"    # I
    .param p2, "selectedItems"    # I

    .prologue
    .line 4977
    const/4 v14, 0x0

    .line 4979
    .local v14, "menuItem":Landroid/view/MenuItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    if-eqz v19, :cond_f

    sget-object v19, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mOptionsMenu:Landroid/view/Menu;

    if-eqz v19, :cond_f

    .line 4982
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    if-eqz v14, :cond_0

    .line 4984
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 4987
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeRename:Landroid/view/MenuItem;

    if-eqz v14, :cond_1

    .line 4989
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 4992
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDetail:Landroid/view/MenuItem;

    if-eqz v14, :cond_2

    .line 4994
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 4997
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeCopy:Landroid/view/MenuItem;

    if-eqz v14, :cond_3

    .line 4999
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5002
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeMove:Landroid/view/MenuItem;

    if-eqz v14, :cond_4

    .line 5004
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5006
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeShare:Landroid/view/MenuItem;

    if-eqz v14, :cond_5

    .line 5008
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5010
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeAddShortcut:Landroid/view/MenuItem;

    if-eqz v14, :cond_6

    .line 5012
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5014
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeAddShortcutHome:Landroid/view/MenuItem;

    if-eqz v14, :cond_7

    .line 5016
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5019
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v19

    if-nez v19, :cond_a

    .line 5021
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeZip:Landroid/view/MenuItem;

    if-eqz v14, :cond_8

    .line 5023
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5026
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeExtract:Landroid/view/MenuItem;

    if-eqz v14, :cond_9

    .line 5028
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5031
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeExtractHere:Landroid/view/MenuItem;

    if-eqz v14, :cond_a

    .line 5033
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5038
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDownload:Landroid/view/MenuItem;

    if-eqz v14, :cond_b

    .line 5040
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5042
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeFtpEdit:Landroid/view/MenuItem;

    if-eqz v14, :cond_c

    .line 5044
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5047
    :cond_c
    if-nez p2, :cond_28

    .line 5048
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectModeFrom()I

    move-result v19

    const/16 v20, 0x5

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_10

    .line 5049
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    if-eqz v14, :cond_24

    .line 5051
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5052
    const/16 v19, 0x0

    move-object/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 5053
    const v19, 0x7f0b0016

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 5055
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    if-eqz v19, :cond_d

    .line 5056
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 5060
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0b0038

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-virtual/range {v19 .. v21}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 5062
    .local v15, "msg":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    move-object/from16 v19, v0

    if-eqz v19, :cond_e

    .line 5063
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5064
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Lcom/sec/android/app/myfiles/MainActivity;->setSpinnerWidth(Ljava/lang/String;)V

    .line 5067
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    if-eqz v19, :cond_f

    .line 5069
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    const/16 v20, 0x1

    const/16 v21, 0x0

    invoke-virtual/range {v19 .. v21}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    .line 5811
    .end local v15    # "msg":Ljava/lang/String;
    :cond_f
    :goto_0
    return-void

    .line 5075
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    if-eqz v14, :cond_11

    .line 5077
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5080
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeRename:Landroid/view/MenuItem;

    if-eqz v14, :cond_12

    .line 5082
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5085
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDetail:Landroid/view/MenuItem;

    if-eqz v14, :cond_13

    .line 5087
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5090
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeCopy:Landroid/view/MenuItem;

    if-eqz v14, :cond_14

    .line 5092
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5095
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeMove:Landroid/view/MenuItem;

    if-eqz v14, :cond_15

    .line 5097
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5099
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeShare:Landroid/view/MenuItem;

    if-eqz v14, :cond_16

    .line 5101
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5103
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeAddShortcut:Landroid/view/MenuItem;

    if-eqz v14, :cond_17

    .line 5105
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5107
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeAddShortcutHome:Landroid/view/MenuItem;

    if-eqz v14, :cond_18

    .line 5109
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5112
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmoveMoveToPrivate:Landroid/view/MenuItem;

    if-eqz v14, :cond_19

    .line 5114
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5117
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmoveRemoveFromPrivate:Landroid/view/MenuItem;

    if-eqz v14, :cond_1a

    .line 5119
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5122
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeMoveToKNOX:Landroid/view/MenuItem;

    if-eqz v14, :cond_1b

    .line 5124
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5127
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeRemoveFromKNOX:Landroid/view/MenuItem;

    if-eqz v14, :cond_1c

    .line 5129
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5132
    :cond_1c
    const/16 v19, 0x1

    move/from16 v0, p2

    move/from16 v1, v19

    if-eq v0, v1, :cond_1e

    .line 5133
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeUnlock:Landroid/view/MenuItem;

    if-eqz v14, :cond_1d

    .line 5134
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5136
    :cond_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeLock:Landroid/view/MenuItem;

    if-eqz v14, :cond_1e

    .line 5137
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5141
    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v19

    if-nez v19, :cond_21

    .line 5143
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeZip:Landroid/view/MenuItem;

    if-eqz v14, :cond_1f

    .line 5145
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5148
    :cond_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeExtract:Landroid/view/MenuItem;

    if-eqz v14, :cond_20

    .line 5150
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5153
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeExtractHere:Landroid/view/MenuItem;

    if-eqz v14, :cond_21

    .line 5155
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5161
    :cond_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x9

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_22

    .line 5163
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDownload:Landroid/view/MenuItem;

    if-eqz v14, :cond_22

    .line 5165
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5168
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeFtpEdit:Landroid/view/MenuItem;

    if-eqz v14, :cond_23

    .line 5170
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5173
    :cond_23
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectModeFrom()I

    move-result v19

    if-nez v19, :cond_24

    .line 5175
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    const/16 v20, 0x1

    const/16 v21, 0x0

    invoke-virtual/range {v19 .. v21}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    .line 5177
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    if-eqz v19, :cond_24

    .line 5179
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 5575
    :cond_24
    :goto_1
    const/4 v4, 0x0

    .line 5577
    .local v4, "blockSendVia":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/MainActivity;->blockSendtoKNOX:Z

    .line 5579
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/MainActivity;->mEnableMoveToPrivate:Z

    .line 5581
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/MainActivity;->mDisableMoveToPrivate:Z

    .line 5583
    const/4 v2, 0x0

    .line 5585
    .local v2, "addShortcutPossible":Z
    const/4 v3, 0x0

    .line 5587
    .local v3, "addShortcutPossibleHome":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v13

    .line 5589
    .local v13, "mSelectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x3

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x4

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x5

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_5f

    :cond_25
    if-eqz v13, :cond_5f

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v19

    if-lez v19, :cond_5f

    .line 5591
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_26
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_64

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    .local v18, "selectedItem":Ljava/lang/Object;
    move-object/from16 v17, v18

    .line 5593
    check-cast v17, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .line 5595
    .local v17, "selected":Lcom/sec/android/app/myfiles/element/BrowserItem;
    sget-boolean v19, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-eqz v19, :cond_5e

    .line 5597
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v19, v0

    if-eqz v19, :cond_5d

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v19, v0

    sget-object v20, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_5d

    .line 5599
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/MainActivity;->blockSendtoKNOX:Z

    .line 5601
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/MainActivity;->mDisableMoveToPrivate:Z

    .line 5615
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v19, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isForwardable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_27

    new-instance v19, Ljava/io/File;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-direct/range {v19 .. v20}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->exists()Z

    move-result v19

    if-nez v19, :cond_26

    .line 5617
    :cond_27
    const/4 v4, 0x1

    goto :goto_2

    .line 5185
    .end local v2    # "addShortcutPossible":Z
    .end local v3    # "addShortcutPossibleHome":Z
    .end local v4    # "blockSendVia":Z
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v13    # "mSelectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v17    # "selected":Lcom/sec/android/app/myfiles/element/BrowserItem;
    .end local v18    # "selectedItem":Ljava/lang/Object;
    :cond_28
    move/from16 v0, p1

    move/from16 v1, p2

    if-ne v0, v1, :cond_40

    .line 5187
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectModeFrom()I

    move-result v19

    const/16 v20, 0x5

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_2b

    .line 5188
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    if-eqz v14, :cond_24

    .line 5189
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5190
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 5191
    const/16 v19, 0x0

    move-object/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 5192
    const v19, 0x7f0b0016

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 5194
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    if-eqz v19, :cond_29

    .line 5195
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 5198
    :cond_29
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0b0038

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-virtual/range {v19 .. v21}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 5200
    .restart local v15    # "msg":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    move-object/from16 v19, v0

    if-eqz v19, :cond_2a

    .line 5201
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5202
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Lcom/sec/android/app/myfiles/MainActivity;->setSpinnerWidth(Ljava/lang/String;)V

    .line 5205
    :cond_2a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    const/16 v20, 0x0

    const/16 v21, 0x1

    invoke-virtual/range {v19 .. v21}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    goto/16 :goto_0

    .line 5210
    .end local v15    # "msg":Ljava/lang/String;
    :cond_2b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x9

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_33

    .line 5211
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDownload:Landroid/view/MenuItem;

    if-eqz v14, :cond_2c

    .line 5213
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5289
    :cond_2c
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDetail:Landroid/view/MenuItem;

    if-eqz v14, :cond_2d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x11

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_2d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x28

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_2d

    .line 5291
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5293
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeFtpEdit:Landroid/view/MenuItem;

    if-eqz v14, :cond_2d

    .line 5294
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5297
    :cond_2d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeRename:Landroid/view/MenuItem;

    if-eqz v14, :cond_2e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x7

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_2e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x9

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_2e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x11

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_2e

    const/16 v19, 0x1

    move/from16 v0, p2

    move/from16 v1, v19

    if-ne v0, v1, :cond_2e

    .line 5302
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5305
    :cond_2e
    const-string v19, "ChinaNalSecurity"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v20

    const-string v21, "CscFeature_Common_ConfigLocalSecurityPolicy"

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_32

    .line 5307
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v12

    .line 5308
    .local v12, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/app/myfiles/element/BrowserItem;

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/element/BrowserItem;->isFile()Ljava/lang/Boolean;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    if-eqz v19, :cond_2f

    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->isEncryptionFile(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_2f

    .line 5311
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeUnlock:Landroid/view/MenuItem;

    if-eqz v14, :cond_2f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x7

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_2f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x9

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_2f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0xa

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_2f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x11

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_2f

    const/16 v19, 0x1

    move/from16 v0, p2

    move/from16 v1, v19

    if-ne v0, v1, :cond_2f

    .line 5317
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxFolder(Ljava/lang/String;)Z

    move-result v19

    if-nez v19, :cond_2f

    .line 5319
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5323
    :cond_2f
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/app/myfiles/element/BrowserItem;

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/element/BrowserItem;->isFile()Ljava/lang/Boolean;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    if-eqz v19, :cond_30

    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->isEncryptionFile(Ljava/lang/String;)Z

    move-result v19

    if-nez v19, :cond_30

    .line 5326
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeLock:Landroid/view/MenuItem;

    if-eqz v14, :cond_30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x7

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x9

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0xa

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x11

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_30

    const/16 v19, 0x1

    move/from16 v0, p2

    move/from16 v1, v19

    if-ne v0, v1, :cond_30

    .line 5332
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxFolder(Ljava/lang/String;)Z

    move-result v19

    if-nez v19, :cond_30

    .line 5334
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5339
    :cond_30
    const/16 v19, 0x1

    move/from16 v0, p2

    move/from16 v1, v19

    if-eq v0, v1, :cond_32

    .line 5340
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeUnlock:Landroid/view/MenuItem;

    if-eqz v14, :cond_31

    .line 5341
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5343
    :cond_31
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeLock:Landroid/view/MenuItem;

    if-eqz v14, :cond_32

    .line 5344
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5350
    .end local v12    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_32
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectModeFrom()I

    move-result v19

    if-nez v19, :cond_24

    .line 5352
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    const/16 v20, 0x0

    const/16 v21, 0x1

    invoke-virtual/range {v19 .. v21}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    .line 5354
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    if-eqz v19, :cond_24

    .line 5355
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setSelected(Z)V

    goto/16 :goto_1

    .line 5217
    :cond_33
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    if-eqz v14, :cond_34

    .line 5219
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5221
    :cond_34
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDownload:Landroid/view/MenuItem;

    if-eqz v14, :cond_35

    .line 5222
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5225
    :cond_35
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeCopy:Landroid/view/MenuItem;

    if-eqz v14, :cond_36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x7

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x11

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_36

    .line 5229
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5232
    :cond_36
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeMove:Landroid/view/MenuItem;

    if-eqz v14, :cond_37

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x7

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_37

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x11

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_37

    .line 5236
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5239
    :cond_37
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v19

    if-nez v19, :cond_2c

    .line 5241
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeZip:Landroid/view/MenuItem;

    if-eqz v14, :cond_39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_38

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0xb

    move/from16 v0, v19

    move/from16 v1, v20

    if-lt v0, v1, :cond_39

    :cond_38
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x11

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_39

    .line 5245
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x1f

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_39

    .line 5246
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5249
    :cond_39
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v11

    .line 5250
    .local v11, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v19

    if-nez v19, :cond_3a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0xa

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_3a

    .line 5251
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->selectAllItem()V

    .line 5252
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v11

    .line 5254
    :cond_3a
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v6

    .line 5256
    .local v6, "fileTypeInt":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeExtract:Landroid/view/MenuItem;

    if-eqz v14, :cond_3c

    .line 5258
    invoke-virtual {v11}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v19

    if-nez v19, :cond_3c

    .line 5261
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x8

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_3b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x1f

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_3b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x7

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_3b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x9

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_3b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0xa

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_3b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x11

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_3b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x28

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_3e

    .line 5264
    :cond_3b
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5273
    :cond_3c
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeExtractHere:Landroid/view/MenuItem;

    if-eqz v14, :cond_2c

    .line 5275
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x8

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_3d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x1f

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_3d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x7

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_3d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x9

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_3d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0xa

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_3d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x11

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_3d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x28

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_3f

    .line 5278
    :cond_3d
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_4

    .line 5269
    :cond_3e
    invoke-static {v6}, Lcom/sec/android/app/myfiles/MediaFile;->isArchiveFileType(I)Z

    move-result v19

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_5

    .line 5283
    :cond_3f
    invoke-static {v6}, Lcom/sec/android/app/myfiles/MediaFile;->isArchiveFileType(I)Z

    move-result v19

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_4

    .line 5362
    .end local v6    # "fileTypeInt":I
    .end local v11    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_40
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectModeFrom()I

    move-result v19

    const/16 v20, 0x5

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_43

    .line 5363
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    if-eqz v14, :cond_42

    .line 5364
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5365
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 5366
    const/16 v19, 0x0

    move-object/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 5367
    const v19, 0x7f0b0016

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 5369
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    if-eqz v19, :cond_41

    .line 5370
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 5373
    :cond_41
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    const/16 v20, 0x1

    const/16 v21, 0x0

    invoke-virtual/range {v19 .. v21}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    .line 5377
    :cond_42
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0b0038

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-virtual/range {v19 .. v21}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 5379
    .restart local v15    # "msg":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    move-object/from16 v19, v0

    if-eqz v19, :cond_f

    .line 5380
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5381
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Lcom/sec/android/app/myfiles/MainActivity;->setSpinnerWidth(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5386
    .end local v15    # "msg":Ljava/lang/String;
    :cond_43
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectModeFrom()I

    move-result v19

    if-nez v19, :cond_45

    .line 5388
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    const/16 v20, 0x1

    const/16 v21, 0x1

    invoke-virtual/range {v19 .. v21}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    .line 5389
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    if-eqz v19, :cond_44

    .line 5390
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 5393
    :cond_44
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectModeFrom()I

    move-result v19

    const/16 v20, 0x5

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_45

    .line 5394
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    if-eqz v14, :cond_45

    .line 5395
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5396
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 5397
    const/16 v19, 0x0

    move-object/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 5398
    const v19, 0x7f0b0016

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 5404
    :cond_45
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x9

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_50

    .line 5406
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDownload:Landroid/view/MenuItem;

    if-eqz v14, :cond_46

    .line 5408
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5442
    :cond_46
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDetail:Landroid/view/MenuItem;

    if-eqz v14, :cond_47

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x11

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_47

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x28

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_47

    .line 5445
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5447
    :cond_47
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeFtpEdit:Landroid/view/MenuItem;

    if-eqz v14, :cond_48

    .line 5449
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5451
    :cond_48
    const/16 v19, 0x1

    move/from16 v0, p2

    move/from16 v1, v19

    if-ne v0, v1, :cond_58

    .line 5453
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeRename:Landroid/view/MenuItem;

    if-eqz v14, :cond_49

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x7

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_49

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x9

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_49

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x11

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_49

    .line 5457
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5460
    :cond_49
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v12

    .line 5462
    .restart local v12    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v12}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v19

    if-nez v19, :cond_4d

    .line 5464
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v6

    .line 5466
    .restart local v6    # "fileTypeInt":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v19

    if-nez v19, :cond_4d

    .line 5468
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeExtract:Landroid/view/MenuItem;

    if-eqz v14, :cond_4b

    .line 5470
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x8

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_4a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x1f

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_4a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x7

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_4a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x9

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_4a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0xa

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_4a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x11

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_4a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x28

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_56

    .line 5473
    :cond_4a
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5482
    :cond_4b
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeExtractHere:Landroid/view/MenuItem;

    if-eqz v14, :cond_4d

    .line 5484
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x8

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_4c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x1f

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_4c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x7

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_4c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x9

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_4c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0xa

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_4c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x11

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_4c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x28

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_57

    .line 5487
    :cond_4c
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5498
    .end local v6    # "fileTypeInt":I
    :cond_4d
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeFtpEdit:Landroid/view/MenuItem;

    if-eqz v14, :cond_4e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x11

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_4e

    .line 5501
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5505
    :cond_4e
    const-string v19, "ChinaNalSecurity"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v20

    const-string v21, "CscFeature_Common_ConfigLocalSecurityPolicy"

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_24

    .line 5507
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/app/myfiles/element/BrowserItem;

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/element/BrowserItem;->isFile()Ljava/lang/Boolean;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    if-eqz v19, :cond_4f

    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->isEncryptionFile(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_4f

    .line 5510
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeUnlock:Landroid/view/MenuItem;

    if-eqz v14, :cond_4f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x7

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_4f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x9

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_4f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0xa

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_4f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x11

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_4f

    const/16 v19, 0x1

    move/from16 v0, p2

    move/from16 v1, v19

    if-ne v0, v1, :cond_4f

    .line 5516
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxFolder(Ljava/lang/String;)Z

    move-result v19

    if-nez v19, :cond_4f

    .line 5518
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5522
    :cond_4f
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/app/myfiles/element/BrowserItem;

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/element/BrowserItem;->isFile()Ljava/lang/Boolean;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    if-eqz v19, :cond_24

    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->isEncryptionFile(Ljava/lang/String;)Z

    move-result v19

    if-nez v19, :cond_24

    .line 5525
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeLock:Landroid/view/MenuItem;

    if-eqz v14, :cond_24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x7

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x9

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0xa

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x11

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_24

    const/16 v19, 0x1

    move/from16 v0, p2

    move/from16 v1, v19

    if-ne v0, v1, :cond_24

    .line 5531
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxFolder(Ljava/lang/String;)Z

    move-result v19

    if-nez v19, :cond_24

    .line 5533
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_1

    .line 5411
    .end local v12    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_50
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDownload:Landroid/view/MenuItem;

    if-eqz v14, :cond_51

    .line 5412
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDownload:Landroid/view/MenuItem;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-interface/range {v19 .. v20}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5414
    :cond_51
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    if-eqz v14, :cond_52

    .line 5416
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5419
    :cond_52
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeCopy:Landroid/view/MenuItem;

    if-eqz v14, :cond_53

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x7

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_53

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x11

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_53

    .line 5422
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5425
    :cond_53
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeMove:Landroid/view/MenuItem;

    if-eqz v14, :cond_54

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x7

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_54

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x11

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_54

    .line 5428
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5431
    :cond_54
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v19

    if-nez v19, :cond_46

    .line 5433
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeZip:Landroid/view/MenuItem;

    if-eqz v14, :cond_46

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_55

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0xb

    move/from16 v0, v19

    move/from16 v1, v20

    if-lt v0, v1, :cond_46

    :cond_55
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x11

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_46

    .line 5437
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x1f

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_46

    .line 5438
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_6

    .line 5478
    .restart local v6    # "fileTypeInt":I
    .restart local v12    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_56
    invoke-static {v6}, Lcom/sec/android/app/myfiles/MediaFile;->isArchiveFileType(I)Z

    move-result v19

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_7

    .line 5492
    :cond_57
    invoke-static {v6}, Lcom/sec/android/app/myfiles/MediaFile;->isArchiveFileType(I)Z

    move-result v19

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_8

    .line 5541
    .end local v6    # "fileTypeInt":I
    .end local v12    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_58
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeLock:Landroid/view/MenuItem;

    if-eqz v14, :cond_59

    .line 5543
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5545
    :cond_59
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeUnlock:Landroid/view/MenuItem;

    if-eqz v14, :cond_5a

    .line 5547
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5550
    :cond_5a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeRename:Landroid/view/MenuItem;

    if-eqz v14, :cond_5b

    .line 5552
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5555
    :cond_5b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v19

    if-nez v19, :cond_24

    .line 5557
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeExtract:Landroid/view/MenuItem;

    if-eqz v14, :cond_5c

    .line 5559
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5562
    :cond_5c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeExtractHere:Landroid/view/MenuItem;

    if-eqz v14, :cond_24

    .line 5564
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_1

    .line 5605
    .restart local v2    # "addShortcutPossible":Z
    .restart local v3    # "addShortcutPossibleHome":Z
    .restart local v4    # "blockSendVia":Z
    .restart local v7    # "i$":Ljava/util/Iterator;
    .restart local v13    # "mSelectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .restart local v17    # "selected":Lcom/sec/android/app/myfiles/element/BrowserItem;
    .restart local v18    # "selectedItem":Ljava/lang/Object;
    :cond_5d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/MainActivity;->mEnableMoveToPrivate:Z

    goto/16 :goto_3

    .line 5611
    :cond_5e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/MainActivity;->mEnableMoveToPrivate:Z

    goto/16 :goto_3

    .line 5624
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v17    # "selected":Lcom/sec/android/app/myfiles/element/BrowserItem;
    .end local v18    # "selectedItem":Ljava/lang/Object;
    :cond_5f
    if-eqz v13, :cond_74

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v19

    if-lez v19, :cond_74

    .line 5626
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_60
    :goto_9
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_64

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    .restart local v18    # "selectedItem":Ljava/lang/Object;
    move-object/from16 v17, v18

    .line 5628
    check-cast v17, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .line 5630
    .restart local v17    # "selected":Lcom/sec/android/app/myfiles/element/BrowserItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x8

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_61

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x1f

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_6d

    .line 5632
    :cond_61
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v19, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/myfiles/utils/Utils;->isDirectoryOfDropbox(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 5634
    .local v9, "isDirectory":Ljava/lang/Boolean;
    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    if-eqz v19, :cond_62

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_62

    .line 5636
    const/4 v2, 0x1

    .line 5639
    :cond_62
    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    if-nez v19, :cond_63

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_60

    .line 5641
    :cond_63
    const/4 v4, 0x1

    .line 5719
    .end local v9    # "isDirectory":Ljava/lang/Boolean;
    .end local v17    # "selected":Lcom/sec/android/app/myfiles/element/BrowserItem;
    .end local v18    # "selectedItem":Ljava/lang/Object;
    :cond_64
    :goto_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeShare:Landroid/view/MenuItem;

    if-eqz v14, :cond_65

    .line 5721
    if-nez v4, :cond_75

    const/16 v19, 0x1

    :goto_b
    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5724
    :cond_65
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeAddShortcut:Landroid/view/MenuItem;

    if-eqz v14, :cond_66

    .line 5726
    invoke-interface {v14, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5729
    :cond_66
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeAddShortcutHome:Landroid/view/MenuItem;

    if-eqz v14, :cond_67

    .line 5731
    invoke-interface {v14, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5734
    :cond_67
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x28

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_6c

    .line 5736
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeCopy:Landroid/view/MenuItem;

    if-eqz v14, :cond_68

    .line 5738
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5740
    :cond_68
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeMove:Landroid/view/MenuItem;

    if-eqz v14, :cond_69

    .line 5742
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5744
    :cond_69
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeRename:Landroid/view/MenuItem;

    if-eqz v14, :cond_6a

    .line 5746
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5748
    :cond_6a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeZip:Landroid/view/MenuItem;

    if-eqz v14, :cond_6b

    .line 5750
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5752
    :cond_6b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionmoveMoveToPrivate:Landroid/view/MenuItem;

    if-eqz v14, :cond_6c

    .line 5754
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5757
    :cond_6c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectModeFrom()I

    move-result v19

    if-nez v19, :cond_f

    .line 5759
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v16

    .line 5762
    .local v16, "nf":Ljava/text/NumberFormat;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0b0038

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-virtual/range {v19 .. v21}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 5763
    .restart local v15    # "msg":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    move-object/from16 v19, v0

    if-eqz v19, :cond_f

    .line 5764
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5765
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Lcom/sec/android/app/myfiles/MainActivity;->setSpinnerWidth(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5646
    .end local v15    # "msg":Ljava/lang/String;
    .end local v16    # "nf":Ljava/text/NumberFormat;
    .restart local v17    # "selected":Lcom/sec/android/app/myfiles/element/BrowserItem;
    .restart local v18    # "selectedItem":Ljava/lang/Object;
    :cond_6d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0xa

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_6f

    .line 5648
    const/4 v4, 0x1

    .line 5650
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v19, v0

    check-cast v19, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->getSelectedItemForAddshortcut()Ljava/util/ArrayList;

    move-result-object v11

    .line 5652
    .restart local v11    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_6e
    :goto_c
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_60

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    .line 5654
    .local v10, "item":Ljava/lang/Object;
    check-cast v10, Lcom/sec/android/app/myfiles/ftp/FTPItem;

    .end local v10    # "item":Ljava/lang/Object;
    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->isFile()Z

    move-result v19

    if-nez v19, :cond_6e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_6e

    .line 5656
    const/4 v2, 0x1

    goto :goto_c

    .line 5663
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v11    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_6f
    sget-boolean v19, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-eqz v19, :cond_73

    .line 5665
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v19, v0

    if-eqz v19, :cond_72

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v19, v0

    sget-object v20, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_72

    .line 5667
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/MainActivity;->blockSendtoKNOX:Z

    .line 5669
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/MainActivity;->mDisableMoveToPrivate:Z

    .line 5681
    :goto_d
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_70

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/myfiles/element/BrowserItem;->isFile()Ljava/lang/Boolean;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    if-nez v19, :cond_70

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    const/16 v20, 0x28

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_70

    .line 5683
    const/4 v2, 0x1

    .line 5686
    :cond_70
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/myfiles/element/BrowserItem;->isFile()Ljava/lang/Boolean;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    if-eqz v19, :cond_71

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getFile()Ljava/io/File;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->length()J

    move-result-wide v20

    const-wide/16 v22, 0x0

    cmp-long v19, v20, v22

    if-eqz v19, :cond_71

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v19, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isForwardable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_71

    new-instance v19, Ljava/io/File;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-direct/range {v19 .. v20}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->exists()Z

    move-result v19

    if-nez v19, :cond_60

    .line 5689
    :cond_71
    const/4 v4, 0x1

    .line 5692
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v19, v0

    if-eqz v19, :cond_60

    .line 5694
    new-instance v5, Ljava/io/File;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5696
    .local v5, "file":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v19

    if-eqz v19, :cond_60

    .line 5698
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/MainActivity;->blockSendtoKNOX:Z

    goto/16 :goto_9

    .line 5673
    .end local v5    # "file":Ljava/io/File;
    :cond_72
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/MainActivity;->mEnableMoveToPrivate:Z

    goto/16 :goto_d

    .line 5678
    :cond_73
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/MainActivity;->mEnableMoveToPrivate:Z

    goto/16 :goto_d

    .line 5709
    .end local v17    # "selected":Lcom/sec/android/app/myfiles/element/BrowserItem;
    .end local v18    # "selectedItem":Ljava/lang/Object;
    :cond_74
    const/4 v4, 0x1

    .line 5710
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/MainActivity;->blockSendtoKNOX:Z

    .line 5711
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/MainActivity;->mEnableMoveToPrivate:Z

    .line 5712
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v19

    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/MainActivity;->mDisableMoveToPrivate:Z

    .line 5713
    const/4 v2, 0x0

    .line 5714
    const/4 v3, 0x0

    goto/16 :goto_a

    .line 5721
    :cond_75
    const/16 v19, 0x0

    goto/16 :goto_b
.end method

.method public singleSelectionChanged(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 5817
    const/4 v2, 0x0

    .line 5819
    .local v2, "menuItem":Landroid/view/MenuItem;
    sget-object v4, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mOptionsMenu:Landroid/view/Menu;

    if-eqz v4, :cond_0

    .line 5821
    sget-object v4, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mOptionsMenu:Landroid/view/Menu;

    const v5, 0x7f0f0148

    invoke-interface {v4, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 5823
    const/4 v4, 0x1

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 5827
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getIsAudioSelector()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 5828
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 5829
    .local v0, "cursor":Landroid/database/Cursor;
    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 5830
    const-string v4, "_data"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 5831
    .local v3, "path":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5832
    .local v1, "file":Ljava/io/File;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v4, Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, p1}, Lcom/sec/android/app/myfiles/SelectorActivity;->mediaExecute(Ljava/lang/String;I)V

    .line 5835
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "path":Ljava/lang/String;
    :cond_1
    return-void
.end method
