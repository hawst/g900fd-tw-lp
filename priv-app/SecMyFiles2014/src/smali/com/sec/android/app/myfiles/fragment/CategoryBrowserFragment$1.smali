.class Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$1;
.super Landroid/content/BroadcastReceiver;
.source "CategoryBrowserFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 138
    const/4 v0, 0x2

    const-string v1, "CategoryBrowserFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Broadcast receive : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 139
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Media_DB_Update_Finsished"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->onRefresh()V

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Media_DB_Update_Finsished_by_Scanner"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mFragmentId:I

    const/4 v1, 0x7

    if-eq v0, v1, :cond_0

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->onRefresh()V

    goto :goto_0

    .line 148
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.android.MTP.OBJECT_REMOVED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 152
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
