.class Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1;
.super Ljava/lang/Object;
.source "NearByDeviceFileBrowserAdapter.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

.field final synthetic val$fileMimeType:Ljava/lang/String;

.field final synthetic val$fileType:I

.field final synthetic val$fileUri:Ljava/lang/String;

.field final synthetic val$thumbFileUri:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 275
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    iput p2, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1;->val$fileType:I

    iput-object p3, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1;->val$thumbFileUri:Ljava/lang/String;

    iput-object p4, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1;->val$fileUri:Ljava/lang/String;

    iput-object p5, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1;->val$fileMimeType:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v1, 0x201

    const/4 v11, 0x2

    const/4 v6, 0x1

    const/4 v10, 0x0

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->isSelectMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/sec/android/app/myfiles/utils/Utils;->isHoverSettingDisabled(Landroid/content/Context;Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 283
    const/4 v7, 0x0

    .line 284
    .local v7, "bSupportHover":Z
    const/4 v9, 0x0

    .line 286
    .local v9, "hoverMgr":Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 287
    .local v2, "hoverRect":Landroid/graphics/Rect;
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 289
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1;->val$fileType:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_2

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v9

    .line 293
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1;->val$thumbFileUri:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mThumbnailCache:Landroid/util/LruCache;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;)Landroid/util/LruCache;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mThumbnailCache:Landroid/util/LruCache;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;)Landroid/util/LruCache;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1;->val$thumbFileUri:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1;->val$fileUri:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1;->val$fileMimeType:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(ILandroid/graphics/Rect;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 304
    :goto_0
    const/4 v7, 0x1

    .line 312
    :goto_1
    if-eqz v7, :cond_0

    .line 314
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 360
    .end local v2    # "hoverRect":Landroid/graphics/Rect;
    .end local v7    # "bSupportHover":Z
    .end local v9    # "hoverMgr":Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    :cond_0
    :goto_2
    :pswitch_0
    return v10

    .line 300
    .restart local v2    # "hoverRect":Landroid/graphics/Rect;
    .restart local v7    # "bSupportHover":Z
    .restart local v9    # "hoverMgr":Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v0

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1;->val$fileUri:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1;->val$fileMimeType:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(ILandroid/graphics/Rect;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 308
    :cond_2
    const/4 v7, 0x0

    goto :goto_1

    .line 319
    :pswitch_1
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    if-ne v0, v11, :cond_3

    .line 321
    const/16 v0, 0xa

    const/4 v1, -0x1

    invoke-static {v0, v1}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 329
    :cond_3
    :goto_3
    const/16 v0, 0x1f4

    invoke-virtual {v9, v0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->sendDelayedMessage(I)V

    goto :goto_2

    .line 324
    :catch_0
    move-exception v8

    .line 326
    .local v8, "e":Landroid/os/RemoteException;
    invoke-virtual {v8}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3

    .line 335
    .end local v8    # "e":Landroid/os/RemoteException;
    :pswitch_2
    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->removeDelayedMessage()V

    .line 338
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    if-ne v0, v11, :cond_0

    .line 340
    const/4 v0, 0x1

    const/4 v1, -0x1

    invoke-static {v0, v1}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 343
    :catch_1
    move-exception v8

    .line 345
    .restart local v8    # "e":Landroid/os/RemoteException;
    invoke-virtual {v8}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 314
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
