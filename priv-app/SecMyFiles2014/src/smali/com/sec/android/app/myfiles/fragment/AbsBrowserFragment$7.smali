.class Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$7;
.super Lcom/samsung/android/privatemode/IPrivateModeClient$Stub;
.source "AbsBrowserFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V
    .locals 0

    .prologue
    .line 2009
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-direct {p0}, Lcom/samsung/android/privatemode/IPrivateModeClient$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onStateChange(II)V
    .locals 5
    .param p1, "state"    # I
    .param p2, "extInfo"    # I

    .prologue
    const/4 v4, 0x2

    .line 2014
    const-string v0, "AbsBrowserFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PrivateModeClient onStateChange: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 2017
    if-nez p1, :cond_0

    .line 2019
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    if-eqz v0, :cond_0

    .line 2021
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    invoke-virtual {v1, p0}, Lcom/samsung/android/privatemode/PrivateModeManager;->registerClient(Lcom/samsung/android/privatemode/IPrivateModeClient;)Landroid/os/IBinder;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    .line 2024
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    if-nez v0, :cond_0

    .line 2025
    const-string v0, "AbsBrowserFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PrivateModeClient is not registered : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 2032
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 2033
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2036
    :cond_1
    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    .line 2038
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    if-eqz v0, :cond_2

    .line 2040
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/privatemode/PrivateModeManager;->unregisterClient(Landroid/os/IBinder;Z)Z

    .line 2042
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    .line 2045
    :cond_2
    return-void
.end method
