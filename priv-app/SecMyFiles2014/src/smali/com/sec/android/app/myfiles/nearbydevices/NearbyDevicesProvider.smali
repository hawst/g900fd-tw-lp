.class public Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;
.super Ljava/lang/Object;
.source "NearbyDevicesProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$4;
    }
.end annotation


# static fields
.field public static final NEARBY_DEVICES_DEFAULT_DEVICE_DOMAIN:Lcom/samsung/android/allshare/Device$DeviceDomain;

.field public static final NEARBY_DEVICES_DEFAULT_DEVICE_TYPE:Lcom/samsung/android/allshare/Device$DeviceType;

.field public static final TAG:Ljava/lang/String; = "MyFiles[NearbyDevices]"

.field private static mDownloader:Lcom/samsung/android/allshare/extension/SECDownloader;


# instance fields
.field private addDeviceNotifiableCollection:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;",
            ">;"
        }
    .end annotation
.end field

.field private bIsAvailableAllshare:Z

.field private mConnected:Z

.field private final mContext:Landroid/content/Context;

.field private final mDeviceDiscoveryListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

.field private final mDevicesCollection:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/samsung/android/allshare/Device;",
            ">;"
        }
    .end annotation
.end field

.field private mSaveCurDevices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Device;",
            ">;"
        }
    .end annotation
.end field

.field private mServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

.field private removeDeviceNotifiableCollection:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    sput-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->NEARBY_DEVICES_DEFAULT_DEVICE_TYPE:Lcom/samsung/android/allshare/Device$DeviceType;

    .line 50
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceDomain;->LOCAL_NETWORK:Lcom/samsung/android/allshare/Device$DeviceDomain;

    sput-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->NEARBY_DEVICES_DEFAULT_DEVICE_DOMAIN:Lcom/samsung/android/allshare/Device$DeviceDomain;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/Collection;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Lcom/samsung/android/allshare/Device;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "devicesCollection":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/android/allshare/Device;>;"
    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->bIsAvailableAllshare:Z

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mSaveCurDevices:Ljava/util/ArrayList;

    .line 289
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mConnected:Z

    .line 294
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->addDeviceNotifiableCollection:Ljava/util/Set;

    .line 295
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->removeDeviceNotifiableCollection:Ljava/util/Set;

    .line 297
    new-instance v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$3;-><init>(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mDeviceDiscoveryListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    .line 66
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 67
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 69
    :cond_1
    iput-object p2, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mDevicesCollection:Ljava/util/Collection;

    .line 70
    iput-object p1, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mContext:Landroid/content/Context;

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mSaveCurDevices:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mSaveCurDevices:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 74
    :cond_2
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->connect()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)Lcom/samsung/android/allshare/media/MediaServiceProvider;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;Lcom/samsung/android/allshare/Device;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;
    .param p1, "x1"    # Lcom/samsung/android/allshare/Device;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->isRegisteredDevice(Lcom/samsung/android/allshare/Device;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;Lcom/samsung/android/allshare/media/MediaServiceProvider;)Lcom/samsung/android/allshare/media/MediaServiceProvider;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;
    .param p1, "x1"    # Lcom/samsung/android/allshare/media/MediaServiceProvider;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    return-object p1
.end method

.method static synthetic access$202(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;
    .param p1, "x1"    # Z

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mConnected:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)Ljava/util/Collection;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mDevicesCollection:Ljava/util/Collection;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mSaveCurDevices:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$502(Lcom/samsung/android/allshare/extension/SECDownloader;)Lcom/samsung/android/allshare/extension/SECDownloader;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/SECDownloader;

    .prologue
    .line 48
    sput-object p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mDownloader:Lcom/samsung/android/allshare/extension/SECDownloader;

    return-object p0
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->fillDevices()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;Lcom/samsung/android/allshare/Device;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;
    .param p1, "x1"    # Lcom/samsung/android/allshare/Device;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->notifyDeviceRemoved(Lcom/samsung/android/allshare/Device;)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;Lcom/samsung/android/allshare/Device;)Lcom/samsung/android/allshare/Device;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;
    .param p1, "x1"    # Lcom/samsung/android/allshare/Device;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->checkDevice(Lcom/samsung/android/allshare/Device;)Lcom/samsung/android/allshare/Device;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;Lcom/samsung/android/allshare/Device;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;
    .param p1, "x1"    # Lcom/samsung/android/allshare/Device;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->notifyDeviceAdded(Lcom/samsung/android/allshare/Device;)V

    return-void
.end method

.method private checkDevice(Lcom/samsung/android/allshare/Device;)Lcom/samsung/android/allshare/Device;
    .locals 2
    .param p1, "device"    # Lcom/samsung/android/allshare/Device;

    .prologue
    .line 243
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 244
    .local v0, "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 245
    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->checkDevices(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 247
    if-eqz v0, :cond_0

    .line 249
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/Device;

    .line 253
    :goto_0
    return-object v1

    :cond_0
    move-object v1, p1

    goto :goto_0
.end method

.method private checkDevices(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Device;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Device;",
            ">;"
        }
    .end annotation

    .prologue
    .line 230
    .local p1, "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    const/4 v0, 0x0

    .line 233
    .local v0, "checkedDevices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    :try_start_0
    invoke-static {p1}, Lcom/samsung/android/allshare/extension/DeviceChecker;->getDeviceCheckedList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 238
    :goto_0
    return-object v0

    .line 234
    :catch_0
    move-exception v1

    .line 235
    .local v1, "e":Ljava/lang/NoSuchMethodError;
    const-string v2, "MyFiles[NearbyDevices]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DeviceChecker.getDeviceCheckedList method not found: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/NoSuchMethodError;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private declared-synchronized connect()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 143
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mConnected:Z

    if-eqz v2, :cond_0

    .line 144
    const-string v2, "MyFiles[NearbyDevices]"

    const-string v3, "connect: already connected"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    :goto_0
    monitor-exit p0

    return v1

    .line 148
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mContext:Landroid/content/Context;

    new-instance v3, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$2;-><init>(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)V

    const-string v4, "com.samsung.android.allshare.media"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/ServiceConnector;->createServiceProvider(Landroid/content/Context;Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;

    move-result-object v0

    .line 173
    .local v0, "err":Lcom/samsung/android/allshare/ERROR;
    sget-object v2, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$4;->$SwitchMap$com$samsung$android$allshare$ERROR:[I

    invoke-virtual {v0}, Lcom/samsung/android/allshare/ERROR;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 181
    const/4 v1, 0x1

    goto :goto_0

    .line 175
    :pswitch_0
    const-string v2, "MyFiles[NearbyDevices]"

    const-string v3, "connect: FRAMEWORK_NOT_INSTALLED - Unable to construct ServiceConnector."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 143
    .end local v0    # "err":Lcom/samsung/android/allshare/ERROR;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 178
    .restart local v0    # "err":Lcom/samsung/android/allshare/ERROR;
    :pswitch_1
    :try_start_2
    const-string v2, "MyFiles[NearbyDevices]"

    const-string v3, "connect: INVALID_ARGUMENT - Unable to construct ServiceConnector."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 173
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private fillDevices()V
    .locals 8

    .prologue
    .line 187
    iget-object v5, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    if-eqz v5, :cond_0

    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mConnected:Z

    if-nez v5, :cond_2

    .line 189
    :cond_0
    const-string v5, "MyFiles[NearbyDevices]"

    const-string v6, "fillDevices: improper state"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    :cond_1
    return-void

    .line 193
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/media/MediaServiceProvider;->getDeviceFinder()Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    move-result-object v1

    .line 194
    .local v1, "deviceFinder":Lcom/samsung/android/allshare/media/MediaDeviceFinder;
    sget-object v5, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->NEARBY_DEVICES_DEFAULT_DEVICE_TYPE:Lcom/samsung/android/allshare/Device$DeviceType;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mDeviceDiscoveryListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    invoke-virtual {v1, v5, v6}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->setDeviceFinderEventListener(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V

    .line 195
    sget-object v5, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->NEARBY_DEVICES_DEFAULT_DEVICE_DOMAIN:Lcom/samsung/android/allshare/Device$DeviceDomain;

    sget-object v6, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->NEARBY_DEVICES_DEFAULT_DEVICE_TYPE:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v1, v5, v6}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->getDevices(Lcom/samsung/android/allshare/Device$DeviceDomain;Lcom/samsung/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;

    move-result-object v4

    .line 201
    .local v4, "tempDevices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    :try_start_0
    invoke-static {v4}, Lcom/samsung/android/allshare/extension/DeviceChecker;->getDeviceCheckedList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 208
    :goto_0
    if-eqz v4, :cond_1

    .line 210
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Device;

    .line 212
    .local v0, "device":Lcom/samsung/android/allshare/Device;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mDevicesCollection:Ljava/util/Collection;

    if-eqz v5, :cond_3

    .line 214
    iget-object v5, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mDevicesCollection:Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v5

    if-nez v5, :cond_4

    .line 216
    iget-object v5, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mSaveCurDevices:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 217
    iget-object v5, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mDevicesCollection:Ljava/util/Collection;

    invoke-interface {v5, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 203
    .end local v0    # "device":Lcom/samsung/android/allshare/Device;
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 205
    .local v2, "e":Ljava/lang/NoSuchMethodError;
    const-string v5, "MyFiles[NearbyDevices]"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "DeviceChecker no such method exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/NoSuchMethodError;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 219
    .end local v2    # "e":Ljava/lang/NoSuchMethodError;
    .restart local v0    # "device":Lcom/samsung/android/allshare/Device;
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_4
    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->isRegisteredDevice(Lcom/samsung/android/allshare/Device;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 221
    iget-object v5, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mSaveCurDevices:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    iget-object v5, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mDevicesCollection:Ljava/util/Collection;

    invoke-interface {v5, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static getDownLoader()Lcom/samsung/android/allshare/extension/SECDownloader;
    .locals 1

    .prologue
    .line 130
    sget-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mDownloader:Lcom/samsung/android/allshare/extension/SECDownloader;

    return-object v0
.end method

.method private isRegisteredDevice(Lcom/samsung/android/allshare/Device;)Z
    .locals 7
    .param p1, "device"    # Lcom/samsung/android/allshare/Device;

    .prologue
    const/4 v4, 0x0

    .line 259
    iget-object v3, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mSaveCurDevices:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mSaveCurDevices:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 261
    invoke-virtual {p1}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v5

    const-string v6, "+"

    invoke-virtual {v5, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 262
    .local v0, "deviceID":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mSaveCurDevices:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 264
    iget-object v3, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mSaveCurDevices:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/Device;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v5

    iget-object v3, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mSaveCurDevices:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/Device;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v3

    const-string v6, "+"

    invoke-virtual {v3, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v5, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 265
    .local v2, "savedDeviceID":Ljava/lang/String;
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 266
    const/4 v3, 0x1

    .line 270
    .end local v0    # "deviceID":Ljava/lang/String;
    .end local v1    # "i":I
    .end local v2    # "savedDeviceID":Ljava/lang/String;
    :goto_1
    return v3

    .line 262
    .restart local v0    # "deviceID":Ljava/lang/String;
    .restart local v1    # "i":I
    .restart local v2    # "savedDeviceID":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v0    # "deviceID":Ljava/lang/String;
    .end local v1    # "i":I
    .end local v2    # "savedDeviceID":Ljava/lang/String;
    :cond_1
    move v3, v4

    .line 270
    goto :goto_1
.end method

.method private notifyDeviceAdded(Lcom/samsung/android/allshare/Device;)V
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/allshare/Device;

    .prologue
    .line 275
    iget-object v2, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->addDeviceNotifiableCollection:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;

    .line 277
    .local v1, "item":Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;
    invoke-interface {v1, p1}, Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;->deviceAdded(Lcom/samsung/android/allshare/Device;)V

    goto :goto_0

    .line 279
    .end local v1    # "item":Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;
    :cond_0
    return-void
.end method

.method private notifyDeviceRemoved(Lcom/samsung/android/allshare/Device;)V
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/allshare/Device;

    .prologue
    .line 283
    iget-object v2, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->removeDeviceNotifiableCollection:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;

    .line 285
    .local v1, "item":Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;
    invoke-interface {v1, p1}, Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;->deviceRemoved(Lcom/samsung/android/allshare/Device;)V

    goto :goto_0

    .line 287
    .end local v1    # "item":Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;
    :cond_0
    return-void
.end method


# virtual methods
.method public checkAllshareEnabled()Z
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkConnectedNearByDevice(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->bIsAvailableAllshare:Z

    .line 84
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->bIsAvailableAllshare:Z

    return v0

    .line 81
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->bIsAvailableAllshare:Z

    goto :goto_0
.end method

.method public disConnect()V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    invoke-static {v0}, Lcom/samsung/android/allshare/ServiceConnector;->deleteServiceProvider(Lcom/samsung/android/allshare/ServiceProvider;)V

    .line 137
    return-void
.end method

.method public loadDevices()V
    .locals 2

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mConnected:Z

    if-eqz v0, :cond_0

    .line 95
    const-string v0, "MyFiles[NearbyDevices]"

    const-string v1, "loadDevices: service is already running."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :goto_0
    return-void

    .line 97
    :cond_0
    new-instance v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$1;-><init>(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)V

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$1;->run()V

    goto :goto_0
.end method

.method public registerToAddNotification(Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;)V
    .locals 3
    .param p1, "item"    # Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;

    .prologue
    .line 107
    iget-object v2, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->addDeviceNotifiableCollection:Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 108
    iget-object v2, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mDevicesCollection:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Device;

    .line 109
    .local v0, "device":Lcom/samsung/android/allshare/Device;
    invoke-interface {p1, v0}, Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;->deviceAdded(Lcom/samsung/android/allshare/Device;)V

    goto :goto_0

    .line 111
    .end local v0    # "device":Lcom/samsung/android/allshare/Device;
    :cond_0
    return-void
.end method

.method public registerToRemoveNotification(Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;)V
    .locals 1
    .param p1, "item"    # Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->removeDeviceNotifiableCollection:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 115
    return-void
.end method

.method public unregisterFromAddNotification(Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;)V
    .locals 1
    .param p1, "item"    # Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->addDeviceNotifiableCollection:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->addDeviceNotifiableCollection:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 121
    :cond_0
    return-void
.end method

.method public unregisterFromRemovedNotification(Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;)V
    .locals 1
    .param p1, "item"    # Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->removeDeviceNotifiableCollection:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->removeDeviceNotifiableCollection:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 127
    :cond_0
    return-void
.end method
