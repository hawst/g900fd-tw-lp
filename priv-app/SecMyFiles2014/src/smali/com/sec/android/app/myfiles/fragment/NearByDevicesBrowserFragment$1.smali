.class Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$1;
.super Ljava/lang/Object;
.source "NearByDevicesBrowserFragment.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->onCreateDevicesCollectionUpdatedListener()Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->onRefresh()V

    .line 159
    return-void
.end method

.method public removeItem(Lcom/sec/android/app/myfiles/element/ShortcutItem;)V
    .locals 0
    .param p1, "shortcutItem"    # Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .prologue
    .line 154
    return-void
.end method

.method public setDevicesCollection(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;)V
    .locals 0
    .param p1, "devicesCollection"    # Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    .prologue
    .line 150
    return-void
.end method

.method public setNearbyDevicesProvider(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)V
    .locals 0
    .param p1, "nearbyDevicesProvider"    # Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    .prologue
    .line 146
    return-void
.end method

.method public showDialog(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->showDialog(I)V

    .line 164
    return-void
.end method

.method public showToastForScanningNearbyDevices(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->showToastForScanningNearbyDevices(Z)V

    .line 142
    return-void
.end method
