.class Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$3;
.super Ljava/lang/Object;
.source "SearchSetDateFragment.java"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;)V
    .locals 0

    .prologue
    .line 205
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDateSet(Landroid/widget/DatePicker;III)V
    .locals 7
    .param p1, "arg0"    # Landroid/widget/DatePicker;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    const/4 v4, 0x0

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->cal:Ljava/util/Calendar;

    const/4 v6, 0x1

    move v1, p2

    move v2, p3

    move v3, p4

    move v5, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->cal:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    # setter for: Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mFromDate:J
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->access$102(Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;J)J

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mSearchFromDateButton:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mFromDate:J
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;)J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/Utils;->getSmallFormatedDateFromLong(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 213
    return-void
.end method
