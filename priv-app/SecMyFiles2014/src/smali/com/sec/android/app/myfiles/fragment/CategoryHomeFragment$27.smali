.class Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;
.super Ljava/lang/Object;
.source "CategoryHomeFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V
    .locals 0

    .prologue
    .line 2921
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 18
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2926
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->isSelectMode()Z

    move-result v12

    if-eqz v12, :cond_2

    .line 2927
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v12}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v12

    move/from16 v0, p3

    invoke-virtual {v12, v0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 2928
    .local v6, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    const v12, 0x7f0f0037

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 2929
    .local v4, "checkbox":Landroid/widget/CheckBox;
    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelected()Z

    move-result v12

    if-eqz v12, :cond_1

    .line 2930
    const/4 v12, 0x0

    invoke-virtual {v4, v12}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2931
    const/4 v12, 0x0

    invoke-virtual {v6, v12}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setSelected(Z)V

    .line 2936
    :goto_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setAllItemCount()V

    .line 2937
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v12

    invoke-virtual {v12}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 3103
    .end local v4    # "checkbox":Landroid/widget/CheckBox;
    :cond_0
    :goto_1
    return-void

    .line 2933
    .restart local v4    # "checkbox":Landroid/widget/CheckBox;
    :cond_1
    const/4 v12, 0x1

    invoke-virtual {v4, v12}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2934
    const/4 v12, 0x1

    invoke-virtual {v6, v12}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setSelected(Z)V

    goto :goto_0

    .line 2939
    .end local v4    # "checkbox":Landroid/widget/CheckBox;
    .end local v6    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_2
    move-wide/from16 v0, p4

    long-to-int v3, v0

    .line 2941
    .local v3, "categoryType":I
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 2943
    .restart local v6    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    const/4 v2, 0x0

    .line 2945
    .local v2, "argument":Landroid/os/Bundle;
    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v9

    .line 2947
    .local v9, "path":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v12, v9}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentDirectory(Ljava/lang/String;)V

    .line 2949
    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v12

    sparse-switch v12, :sswitch_data_0

    goto :goto_1

    .line 3022
    :sswitch_0
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    check-cast v6, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 3023
    .restart local v6    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v12}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v12

    iput-object v6, v12, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 3024
    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/Utils;->isDropboxFolder(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_3

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/Utils;->isDropboxAccountIsSignin(Landroid/content/Context;)Z

    move-result v12

    if-nez v12, :cond_4

    :cond_3
    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/Utils;->isBaiduFolder(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_13

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/Utils;->isBaiduCloudAccountIsSignin(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_13

    .line 3029
    :cond_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_12

    .line 3031
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v12

    if-nez v12, :cond_f

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckDropBox()Z

    move-result v12

    if-nez v12, :cond_f

    .line 3032
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setDropBoxFromShortcut(Z)V

    .line 3033
    const/16 v12, 0x1f

    if-ne v3, v12, :cond_e

    .line 3034
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v13, 0x5

    invoke-virtual {v12, v13}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDataWarningDialog(I)V

    goto/16 :goto_1

    .line 2952
    :sswitch_1
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    check-cast v6, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 2954
    .restart local v6    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    const/16 v3, 0x201

    .line 2956
    new-instance v5, Ljava/io/File;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v5, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2958
    .local v5, "file":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_7

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_5

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretMode(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_7

    :cond_5
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    const-string v13, "/storage/extSdCard"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    sget-boolean v12, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v12, :cond_7

    :cond_6
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    const-string v13, "/storage/UsbDrive"

    invoke-virtual {v12, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_9

    sget-boolean v12, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-nez v12, :cond_9

    .line 2962
    :cond_7
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 2964
    .local v10, "res":Landroid/content/res/Resources;
    const v12, 0x7f0c0013

    const/4 v13, 0x1

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const/16 v16, 0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v10, v12, v13, v14}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2965
    .local v8, "notiStr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/4 v13, 0x0

    invoke-static {v12, v8, v13}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/Toast;->show()V

    .line 2966
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v14

    invoke-static {v12, v13, v14}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteShortcut(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v11

    .line 2967
    .local v11, "rowsDeleted":I
    if-lez v11, :cond_8

    .line 2969
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v12}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v12

    invoke-virtual {v12, v6}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->removeItem(Ljava/lang/Object;)Z

    .line 2970
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v12}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->onRefresh()V

    .line 2971
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListView:Landroid/widget/ListView;
    invoke-static {v12}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Landroid/widget/ListView;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/android/app/myfiles/ListViewHeightRefresh;->getListViewSize(Landroid/widget/ListView;)V

    goto/16 :goto_1

    .line 2977
    :cond_8
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v13, 0x7f0b0063

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/Toast;->show()V

    .line 2982
    .end local v8    # "notiStr":Ljava/lang/String;
    .end local v10    # "res":Landroid/content/res/Resources;
    .end local v11    # "rowsDeleted":I
    :cond_9
    new-instance v2, Landroid/os/Bundle;

    .end local v2    # "argument":Landroid/os/Bundle;
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2984
    .restart local v2    # "argument":Landroid/os/Bundle;
    const-string v12, "FOLDERPATH"

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3098
    .end local v5    # "file":Ljava/io/File;
    :cond_a
    :goto_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v12, v12, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v12, :cond_0

    .line 3099
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setStartBrowserState(Z)V

    .line 3100
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v12, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v12, v3, v2}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto/16 :goto_1

    .line 2987
    :sswitch_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_b

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v12

    if-nez v12, :cond_b

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckFTP()Z

    move-result v12

    if-nez v12, :cond_b

    .line 2989
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    check-cast v6, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 2990
    .restart local v6    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v12}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v12

    iput-object v6, v12, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 2991
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v13, 0x4

    invoke-virtual {v12, v13}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDataWarningDialog(I)V

    goto/16 :goto_1

    .line 2993
    :cond_b
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v12

    if-nez v12, :cond_c

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v12

    if-nez v12, :cond_c

    .line 2994
    new-instance v7, Lcom/sec/android/app/myfiles/fragment/NoNetworkFragment;

    invoke-direct {v7}, Lcom/sec/android/app/myfiles/fragment/NoNetworkFragment;-><init>()V

    .line 2995
    .local v7, "noNetwork":Landroid/app/DialogFragment;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v12

    const-string v13, "storage_usage"

    invoke-virtual {v7, v12, v13}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2999
    .end local v7    # "noNetwork":Landroid/app/DialogFragment;
    :cond_c
    sget-boolean v12, Lcom/sec/android/app/myfiles/utils/Utils;->mIsActiveWarningUsingWLAN:Z

    if-eqz v12, :cond_d

    const-string v12, "ChinaNalSecurity"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v13

    const-string v14, "CscFeature_Common_ConfigLocalSecurityPolicy"

    invoke-virtual {v13, v14}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_d

    .line 3001
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_d

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckWLANOn()Z

    move-result v12

    if-nez v12, :cond_d

    .line 3003
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    check-cast v6, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 3004
    .restart local v6    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v12}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v12

    iput-object v6, v12, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 3005
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/16 v13, 0xa

    invoke-virtual {v12, v13}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDataWarningDialog(I)V

    goto/16 :goto_1

    .line 3011
    :cond_d
    new-instance v2, Landroid/os/Bundle;

    .end local v2    # "argument":Landroid/os/Bundle;
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 3012
    .restart local v2    # "argument":Landroid/os/Bundle;
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    check-cast v6, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 3013
    .restart local v6    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v12}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v12

    iput-object v6, v12, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 3014
    const-string v12, "FOLDERPATH"

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3015
    const-string v12, "IS_SHORTCUT_FOLDER"

    const/4 v13, 0x1

    invoke-virtual {v2, v12, v13}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 3016
    const-string v12, "SHORTCUT_ROOT_FOLDER"

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3017
    const/16 v3, 0xa

    .line 3018
    goto/16 :goto_2

    .line 3036
    :cond_e
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v13, 0x3

    invoke-virtual {v12, v13}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDataWarningDialog(I)V

    goto/16 :goto_1

    .line 3041
    :cond_f
    sget-boolean v12, Lcom/sec/android/app/myfiles/utils/Utils;->mIsActiveWarningUsingWLAN:Z

    if-eqz v12, :cond_10

    const-string v12, "ChinaNalSecurity"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v13

    const-string v14, "CscFeature_Common_ConfigLocalSecurityPolicy"

    invoke-virtual {v13, v14}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_10

    .line 3043
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckWLANOn()Z

    move-result v12

    if-nez v12, :cond_10

    .line 3044
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setDropBoxFromShortcut(Z)V

    .line 3045
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/16 v13, 0xb

    invoke-virtual {v12, v13}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDataWarningDialog(I)V

    goto/16 :goto_1

    .line 3050
    :cond_10
    new-instance v2, Landroid/os/Bundle;

    .end local v2    # "argument":Landroid/os/Bundle;
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 3051
    .restart local v2    # "argument":Landroid/os/Bundle;
    const-string v12, "IS_SHORTCUT_FOLDER"

    const/4 v13, 0x1

    invoke-virtual {v2, v12, v13}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 3052
    const-string v12, "SHORTCUT_ROOT_FOLDER"

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3054
    const-string v12, "FOLDERPATH"

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3055
    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v12

    const/16 v13, 0x1f

    if-ne v12, v13, :cond_11

    .line 3056
    const/16 v3, 0x1f

    goto/16 :goto_2

    .line 3057
    :cond_11
    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v12

    const/16 v13, 0x8

    if-ne v12, v13, :cond_a

    .line 3058
    const/16 v3, 0x8

    goto/16 :goto_2

    .line 3064
    :cond_12
    new-instance v7, Lcom/sec/android/app/myfiles/fragment/NoNetworkFragment;

    invoke-direct {v7}, Lcom/sec/android/app/myfiles/fragment/NoNetworkFragment;-><init>()V

    .line 3065
    .restart local v7    # "noNetwork":Landroid/app/DialogFragment;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v12

    const-string v13, "storage_usage"

    invoke-virtual {v7, v12, v13}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 3073
    .end local v7    # "noNetwork":Landroid/app/DialogFragment;
    :cond_13
    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/Utils;->isDropboxFolder(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_14

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/Utils;->isDropboxAccountIsSignin(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_15

    :cond_14
    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/Utils;->isBaiduFolder(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_a

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/Utils;->isBaiduCloudAccountIsSignin(Landroid/content/Context;)Z

    move-result v12

    if-nez v12, :cond_a

    .line 3075
    :cond_15
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 3077
    .restart local v10    # "res":Landroid/content/res/Resources;
    const v12, 0x7f0c0013

    const/4 v13, 0x1

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const/16 v16, 0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v10, v12, v13, v14}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 3078
    .restart local v8    # "notiStr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/4 v13, 0x0

    invoke-static {v12, v8, v13}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/Toast;->show()V

    .line 3079
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v14

    invoke-static {v12, v13, v14}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteShortcut(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v11

    .line 3080
    .restart local v11    # "rowsDeleted":I
    if-lez v11, :cond_16

    .line 3082
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v12}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v12

    invoke-virtual {v12, v6}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->removeItem(Ljava/lang/Object;)Z

    .line 3083
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v12}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->onRefresh()V

    .line 3084
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListView:Landroid/widget/ListView;
    invoke-static {v12}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Landroid/widget/ListView;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/android/app/myfiles/ListViewHeightRefresh;->getListViewSize(Landroid/widget/ListView;)V

    goto/16 :goto_1

    .line 3090
    :cond_16
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v13, 0x7f0b0063

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    .line 2949
    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0x1a -> :sswitch_1
        0x1c -> :sswitch_2
        0x1f -> :sswitch_0
    .end sparse-switch
.end method
