.class Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$1;
.super Ljava/lang/Object;
.source "RenameOnConflictFragment.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 64
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 76
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 66
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->dismiss()V

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mListener:Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$RenameOnConflictListener;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;)Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$RenameOnConflictListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mListener:Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$RenameOnConflictListener;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;)Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$RenameOnConflictListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->newName:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$RenameOnConflictListener;->onSuccessRename(Ljava/lang/String;)V

    goto :goto_0

    .line 71
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->dismiss()V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mListener:Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$RenameOnConflictListener;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;)Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$RenameOnConflictListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mListener:Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$RenameOnConflictListener;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;)Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$RenameOnConflictListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$RenameOnConflictListener;->onCancelRename()V

    goto :goto_0

    .line 64
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
