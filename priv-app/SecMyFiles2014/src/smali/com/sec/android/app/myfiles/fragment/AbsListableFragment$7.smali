.class Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;
.super Ljava/lang/Object;
.source "AbsListableFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

.field final synthetic val$custom:Landroid/view/View;

.field final synthetic val$inOrderRadioGroup:Landroid/widget/RadioGroup;

.field final synthetic val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

.field final synthetic val$sortByRadioGroup:Landroid/widget/RadioGroup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;Landroid/view/View;Landroid/widget/RadioGroup;Landroid/widget/RadioGroup;Lcom/sec/android/app/myfiles/utils/SharedDataStore;)V
    .locals 0

    .prologue
    .line 567
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iput-object p2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;->val$custom:Landroid/view/View;

    iput-object p3, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;->val$sortByRadioGroup:Landroid/widget/RadioGroup;

    iput-object p4, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;->val$inOrderRadioGroup:Landroid/widget/RadioGroup;

    iput-object p5, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 572
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;->val$custom:Landroid/view/View;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;->val$sortByRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v3}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    # setter for: Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->toCheckSortBy:Landroid/widget/RadioButton;
    invoke-static {v2, v1}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->access$002(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;Landroid/widget/RadioButton;)Landroid/widget/RadioButton;

    .line 574
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;->val$custom:Landroid/view/View;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;->val$inOrderRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v3}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    # setter for: Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->toCheckInOrder:Landroid/widget/RadioButton;
    invoke-static {v2, v1}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->access$102(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;Landroid/widget/RadioButton;)Landroid/widget/RadioButton;

    .line 576
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget v2, v2, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCurrentSortBy:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentSortBy(I)V

    .line 578
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget v2, v2, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCurrentInOrder:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentInOrder(I)V

    .line 580
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getTAdapter()Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 581
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getTAdapter()Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget v2, v2, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCurrentSortBy:I

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget v3, v3, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCurrentInOrder:I

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setSortBy(II)V

    .line 584
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.myfiles.SORTBY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 586
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "sort_by"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget v2, v2, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCurrentSortBy:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 588
    const-string v1, "in_order"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget v2, v2, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCurrentInOrder:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 590
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 594
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 595
    return-void
.end method
