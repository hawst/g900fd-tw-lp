.class Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$MyListAdapter;
.super Landroid/widget/BaseAdapter;
.source "SearchsSetFileTypeFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyListAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;)V
    .locals 0

    .prologue
    .line 303
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$MyListAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 305
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$MyListAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 314
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 319
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "arg0"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "arg2"    # Landroid/view/ViewGroup;

    .prologue
    .line 325
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$MyListAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    if-nez v3, :cond_1

    .line 326
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$MyListAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 330
    .local v1, "inflater":Landroid/view/LayoutInflater;
    :goto_0
    if-nez p2, :cond_0

    .line 331
    const v3, 0x7f04003e

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 333
    :cond_0
    const v3, 0x7f0f00f4

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 334
    .local v0, "checkBox":Landroid/widget/CheckBox;
    const v3, 0x7f0f00f3

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 335
    .local v2, "text":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$MyListAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 336
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$MyListAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->isChecked()Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 337
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$MyListAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    iget-boolean v3, v3, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->isDownloadedAppsNeedToHidden:Z

    if-eqz v3, :cond_2

    const/4 v3, 0x5

    if-ne p1, v3, :cond_2

    .line 338
    const v3, 0x3ecccccd    # 0.4f

    invoke-virtual {p2, v3}, Landroid/view/View;->setAlpha(F)V

    .line 339
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 343
    :goto_1
    return-object p2

    .line 328
    .end local v0    # "checkBox":Landroid/widget/CheckBox;
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    .end local v2    # "text":Landroid/widget/TextView;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$MyListAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .restart local v1    # "inflater":Landroid/view/LayoutInflater;
    goto :goto_0

    .line 341
    .restart local v0    # "checkBox":Landroid/widget/CheckBox;
    .restart local v2    # "text":Landroid/widget/TextView;
    :cond_2
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {p2, v3}, Landroid/view/View;->setAlpha(F)V

    goto :goto_1
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 347
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$MyListAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->isDownloadedAppsNeedToHidden:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    .line 348
    const/4 v0, 0x0

    .line 350
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
