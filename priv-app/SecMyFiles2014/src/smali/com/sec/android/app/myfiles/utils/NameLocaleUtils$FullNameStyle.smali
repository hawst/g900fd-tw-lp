.class public interface abstract Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$FullNameStyle;
.super Ljava/lang/Object;
.source "NameLocaleUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FullNameStyle"
.end annotation


# static fields
.field public static final CHINESE:I = 0x3

.field public static final CJK:I = 0x2

.field public static final JAPANESE:I = 0x4

.field public static final KOREAN:I = 0x5

.field public static final UNDEFINED:I = 0x0

.field public static final WESTERN:I = 0x1
