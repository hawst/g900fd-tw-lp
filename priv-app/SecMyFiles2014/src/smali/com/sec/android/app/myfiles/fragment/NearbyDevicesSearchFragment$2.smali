.class Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$2;
.super Ljava/lang/Object;
.source "NearbyDevicesSearchFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->AsyncOpenCompleted()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;)V
    .locals 0

    .prologue
    .line 238
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const v3, 0x7f0b0145

    const/4 v1, 0x0

    .line 243
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->bBackOnsearch:Z
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->resetPathIndicator(Z)V

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    # invokes: Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->callSuperAsyncOpenCompleted()V
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/MainActivity;->updateEmptySearchView()V

    .line 256
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->bBackOnsearch:Z
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    # setter for: Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->bBackOnsearch:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->access$002(Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;Z)Z

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->resetPathIndicator(Z)V

    .line 263
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mAdvancedSearchResult:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 267
    return-void
.end method
