.class Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$5;
.super Ljava/lang/Object;
.source "SearchSetDateFragment.java"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;)V
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDateSet(Landroid/widget/DatePicker;III)V
    .locals 7
    .param p1, "arg0"    # Landroid/widget/DatePicker;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->cal:Ljava/util/Calendar;

    const/16 v4, 0x17

    const/16 v5, 0x3b

    const/16 v6, 0x38

    move v1, p2

    move v2, p3

    move v3, p4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->cal:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    # setter for: Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mToDate:J
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->access$302(Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;J)J

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mSearchToDateButton:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mToDate:J
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;)J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/Utils;->getSmallFormatedDateFromLong(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 246
    return-void
.end method
