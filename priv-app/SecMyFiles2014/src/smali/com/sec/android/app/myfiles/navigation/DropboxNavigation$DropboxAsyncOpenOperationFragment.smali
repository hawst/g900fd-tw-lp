.class public Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$DropboxAsyncOpenOperationFragment;
.super Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;
.source "DropboxNavigation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DropboxAsyncOpenOperationFragment"
.end annotation


# instance fields
.field private mAsyncOpenRunnable:Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$AsyncOpenRunnable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 971
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public AsyncOpenExcute()V
    .locals 1

    .prologue
    .line 982
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$DropboxAsyncOpenOperationFragment;->mAsyncOpenRunnable:Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$AsyncOpenRunnable;

    if-eqz v0, :cond_0

    .line 984
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$DropboxAsyncOpenOperationFragment;->mAsyncOpenRunnable:Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$AsyncOpenRunnable;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$AsyncOpenRunnable;->run()V

    .line 988
    :cond_0
    return-void
.end method

.method public setAsyncOpenRunnable(Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$AsyncOpenRunnable;)V
    .locals 0
    .param p1, "asyncOpenRunnable"    # Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$AsyncOpenRunnable;

    .prologue
    .line 975
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$DropboxAsyncOpenOperationFragment;->mAsyncOpenRunnable:Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$AsyncOpenRunnable;

    .line 977
    return-void
.end method
