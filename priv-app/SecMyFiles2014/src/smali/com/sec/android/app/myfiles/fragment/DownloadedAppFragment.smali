.class public Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;
.source "DownloadedAppFragment.java"


# static fields
.field private static final MODULE:Ljava/lang/String; = "DownloadedAppFragment"

.field private static final REQUEST_UNINSTALL_APP:I


# instance fields
.field private final AIRBUTTON_DELETE:I

.field private bDoNotCheckDropBox:Z

.field private mActivity:Lcom/sec/android/app/myfiles/MainActivity;

.field private mAdapter:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

.field private mDailog:Landroid/app/Dialog;

.field protected mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

.field private mItemCount:I

.field protected mSelectActionBarView:Landroid/view/View;

.field protected mSelectAllButton:Landroid/widget/Button;

.field private mSelectAllPopupMenu:Landroid/widget/PopupMenu;

.field protected mSelectAllPopupWindow:Landroid/widget/ListPopupWindow;

.field private mSelectedItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedItemCount:I

.field private mTwDragSelectedItemMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mTwMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

.field private mUninstallAppIndex:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;-><init>()V

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->bDoNotCheckDropBox:Z

    .line 292
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mTwMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

    .line 657
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->AIRBUTTON_DELETE:I

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mTwDragSelectedItemMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->bDoNotCheckDropBox:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->bDoNotCheckDropBox:Z

    return p1
.end method

.method static synthetic access$302(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;
    .param p1, "x1"    # I

    .prologue
    .line 68
    iput p1, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectedItemCount:I

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/MainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    return-object v0
.end method

.method private startUninstallApps(I)V
    .locals 8
    .param p1, "index"    # I

    .prologue
    const/4 v7, 0x0

    .line 850
    const-string v4, "DownloadedAppFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "startUninstallApps() start ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v7, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 855
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 856
    .local v2, "manager":Landroid/content/pm/PackageManager;
    if-eqz v2, :cond_0

    .line 857
    const/16 v4, 0x2200

    invoke-virtual {v2, v4}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mItemCount:I

    .line 860
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectedItem:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 862
    .local v3, "packageName":Ljava/lang/String;
    const/4 v4, 0x2

    const-string v5, "DownloadedAppFragment"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "startUninstallApps() packageName ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 864
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.DELETE"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 866
    .local v1, "intent":Landroid/content/Intent;
    if-ltz p1, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectedItem:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge p1, v4, :cond_1

    .line 867
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "package:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 869
    :cond_1
    const/4 v4, 0x0

    invoke-virtual {p0, v1, v4}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 874
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "manager":Landroid/content/pm/PackageManager;
    .end local v3    # "packageName":Ljava/lang/String;
    :goto_0
    return-void

    .line 870
    :catch_0
    move-exception v0

    .line 872
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private updateSelectedCount()V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 765
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->isSelectMode()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 766
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->getSelectedItemCount()I

    move-result v1

    .line 767
    .local v1, "selectedItems":I
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0038

    new-array v6, v2, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 768
    .local v0, "msg":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    if-eqz v4, :cond_0

    .line 769
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 770
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v4, v0}, Lcom/sec/android/app/myfiles/MainActivity;->setSpinnerWidth(Ljava/lang/String;)V

    .line 773
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    if-eqz v4, :cond_1

    .line 774
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->getCount()I

    move-result v5

    if-ne v1, v5, :cond_2

    :goto_0
    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 777
    .end local v0    # "msg":Ljava/lang/String;
    .end local v1    # "selectedItems":I
    :cond_1
    return-void

    .restart local v0    # "msg":Ljava/lang/String;
    .restart local v1    # "selectedItems":I
    :cond_2
    move v2, v3

    .line 774
    goto :goto_0
.end method


# virtual methods
.method public AsyncOpenCompleted()V
    .locals 0

    .prologue
    .line 1286
    return-void
.end method

.method protected dismissPopupMenu()V
    .locals 1

    .prologue
    .line 1405
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectAllPopupWindow:Landroid/widget/ListPopupWindow;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_0

    .line 1407
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/MainActivity;->getSelectAllPopupWindow()Landroid/widget/ListPopupWindow;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectAllPopupWindow:Landroid/widget/ListPopupWindow;

    .line 1409
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectAllPopupWindow:Landroid/widget/ListPopupWindow;

    if-eqz v0, :cond_1

    .line 1411
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectAllPopupWindow:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 1412
    :cond_1
    return-void
.end method

.method public finishDragMode()V
    .locals 0

    .prologue
    .line 1296
    return-void
.end method

.method protected bridge synthetic getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    move-result-object v0

    return-object v0
.end method

.method protected getAdapter()Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;
    .locals 2

    .prologue
    .line 824
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    if-nez v0, :cond_0

    .line 826
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mFragmentId:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getAdapter(I)Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    .line 830
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    return-object v0
.end method

.method public getOptionsMenuResId(Z)I
    .locals 1
    .param p1, "isSelectMode"    # Z

    .prologue
    .line 837
    if-eqz p1, :cond_0

    .line 839
    const v0, 0x7f0e000b

    .line 843
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0e000a

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 781
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 783
    if-nez p1, :cond_1

    .line 785
    const/4 v2, -0x1

    if-ne p2, v2, :cond_0

    .line 790
    :cond_0
    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mUninstallAppIndex:I

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectedItem:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 792
    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mUninstallAppIndex:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mUninstallAppIndex:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->startUninstallApps(I)V

    .line 818
    :cond_1
    :goto_0
    return-void

    .line 796
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 797
    .local v1, "manager":Landroid/content/pm/PackageManager;
    if-eqz v1, :cond_3

    .line 798
    const/16 v2, 0x2200

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 799
    .local v0, "currCount":I
    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mItemCount:I

    if-le v2, v0, :cond_3

    .line 800
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->finishSelectMode()V

    .line 804
    .end local v0    # "currCount":I
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->onRefresh()V

    .line 806
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->updateSelectedCount()V

    goto :goto_0
.end method

.method public onBackPressed()Z
    .locals 1

    .prologue
    .line 478
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 480
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->finishSelectMode()V

    .line 491
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 484
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v0, :cond_0

    .line 486
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 464
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 472
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 116
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onCreate(Landroid/os/Bundle;)V

    .line 125
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    .line 129
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    .line 130
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 9
    .param p1, "id"    # I

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x0

    .line 497
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v1, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 499
    .local v1, "dialog":Landroid/app/AlertDialog$Builder;
    packed-switch p1, :pswitch_data_0

    .line 589
    :pswitch_0
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v6

    .line 595
    :goto_0
    return-object v6

    .line 504
    :pswitch_1
    move v2, p1

    .line 506
    .local v2, "dialogId":I
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v6}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v5

    .line 508
    .local v5, "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    const/4 v6, 0x3

    if-eq v2, v6, :cond_0

    if-ne v2, v8, :cond_3

    :cond_0
    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckDropBox()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 512
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v6, v6, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v6, :cond_1

    .line 514
    if-ne v2, v8, :cond_2

    .line 515
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v6, Lcom/sec/android/app/myfiles/MainActivity;

    const/16 v8, 0x1f

    invoke-virtual {v6, v8, v7}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    :cond_1
    :goto_1
    move-object v6, v7

    .line 520
    goto :goto_0

    .line 517
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v6, Lcom/sec/android/app/myfiles/MainActivity;

    const/16 v8, 0x8

    invoke-virtual {v6, v8, v7}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto :goto_1

    .line 523
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 525
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f040032

    invoke-virtual {v3, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 527
    .local v0, "customView":Landroid/view/View;
    const v6, 0x7f0b0125

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 529
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isChinaModel()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 530
    const v6, 0x7f0f00c5

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 531
    .local v4, "mobileDataWarningDialog":Landroid/widget/TextView;
    const v6, 0x7f0b00e0

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    .line 534
    .end local v4    # "mobileDataWarningDialog":Landroid/widget/TextView;
    :cond_4
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 536
    new-instance v6, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$3;

    invoke-direct {v6, p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)V

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 551
    const v6, 0x7f0b017a

    new-instance v7, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$4;

    invoke-direct {v7, p0, v2, v5}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$4;-><init>(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;ILcom/sec/android/app/myfiles/utils/SharedDataStore;)V

    invoke-virtual {v1, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 576
    const v6, 0x7f0b0017

    new-instance v7, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$5;

    invoke-direct {v7, p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$5;-><init>(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)V

    invoke-virtual {v1, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 593
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mDailog:Landroid/app/Dialog;

    .line 595
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mDailog:Landroid/app/Dialog;

    goto/16 :goto_0

    .line 499
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 880
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$7;-><init>(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)V

    return-object v0
.end method

.method protected onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1

    .prologue
    .line 923
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$8;-><init>(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)V

    return-object v0
.end method

.method public onCreateSelectedItemChangeListener()Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;
    .locals 1

    .prologue
    .line 1055
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$10;-><init>(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)V

    return-object v0
.end method

.method protected onCreateTreeViewItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 994
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$9;-><init>(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 136
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 138
    .local v1, "view":Landroid/view/View;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mTitleBarTextView:Landroid/widget/TextView;

    const v3, 0x7f0b0007

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 140
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mCategoryTitleContainer:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 142
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mTwMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setTwMultiSelectedListener(Landroid/widget/AdapterView$OnTwMultiSelectedListener;)V

    .line 144
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mTwMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->setTwMultiSelectedListener(Landroid/widget/AdapterView$OnTwMultiSelectedListener;)V

    .line 146
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mTwDragSelectedItemMap:Ljava/util/HashMap;

    if-nez v2, :cond_0

    .line 148
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mTwDragSelectedItemMap:Ljava/util/HashMap;

    .line 151
    :cond_0
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)V

    .line 186
    .local v0, "sl":Landroid/widget/AbsListView$OnScrollListener;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 187
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 281
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    if-eqz v2, :cond_1

    .line 282
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mEmptyView:Landroid/view/View;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setEmptyView(Landroid/view/View;)V

    .line 285
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    if-eqz v2, :cond_2

    .line 286
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mEmptyView:Landroid/view/View;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->setEmptyView(Landroid/view/View;)V

    .line 289
    :cond_2
    return-object v1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    const/4 v3, 0x1

    .line 701
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 760
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    :goto_0
    return v3

    .line 705
    :sswitch_0
    invoke-virtual {p0, v3, v5, v6}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->startSelectMode(III)V

    goto :goto_0

    .line 713
    :sswitch_1
    const/4 v4, 0x5

    invoke-virtual {p0, v3, v5, v4}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->startSelectMode(III)V

    goto :goto_0

    .line 730
    :sswitch_2
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v2

    .line 732
    .local v2, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectedItem:Ljava/util/ArrayList;

    if-nez v4, :cond_0

    .line 734
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectedItem:Ljava/util/ArrayList;

    .line 741
    :goto_1
    iput v6, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mUninstallAppIndex:I

    .line 745
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 747
    .local v1, "selectedItem":Ljava/lang/Object;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectedItem:Ljava/util/ArrayList;

    check-cast v1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .end local v1    # "selectedItem":Ljava/lang/Object;
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 738
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectedItem:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    goto :goto_1

    .line 750
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    iget v4, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mUninstallAppIndex:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mUninstallAppIndex:I

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->startUninstallApps(I)V

    goto :goto_0

    .line 756
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :sswitch_3
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v5, "com.vcast.mediamanager.ACTION_FILES"

    invoke-static {v4, v5}, Lcom/sec/android/app/myfiles/utils/VZCloudUtils;->launchVZCloud(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 701
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0f0121 -> :sswitch_2
        0x7f0f0137 -> :sswitch_0
        0x7f0f0138 -> :sswitch_1
        0x7f0f0139 -> :sswitch_3
    .end sparse-switch
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 7
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 602
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 604
    packed-switch p1, :pswitch_data_0

    .line 652
    .end local p2    # "dialog":Landroid/app/Dialog;
    :goto_0
    :pswitch_0
    return-void

    .line 609
    .restart local p2    # "dialog":Landroid/app/Dialog;
    :pswitch_1
    move v2, p1

    .line 611
    .local v2, "dialogId":I
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 613
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f040032

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 615
    .local v1, "customView":Landroid/view/View;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isChinaModel()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 616
    const v5, 0x7f0f00c5

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 617
    .local v4, "mobileDataWarningDialog":Landroid/widget/TextView;
    const v5, 0x7f0b00e0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 620
    .end local v4    # "mobileDataWarningDialog":Landroid/widget/TextView;
    :cond_0
    const v5, 0x7f0f00c7

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 633
    .local v0, "checkDoNotShow":Landroid/widget/CheckBox;
    new-instance v5, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$6;

    invoke-direct {v5, p0, v2, v0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$6;-><init>(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;ILandroid/widget/CheckBox;)V

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 649
    check-cast p2, Landroid/app/AlertDialog;

    .end local p2    # "dialog":Landroid/app/Dialog;
    invoke-virtual {p2, v1}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    goto :goto_0

    .line 604
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 8
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v7, 0x7f0f0125

    const v6, 0x7f0f0124

    const/4 v5, 0x1

    const v4, 0x7f0f0138

    const/4 v3, 0x0

    .line 948
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 950
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mOptionsMenu:Landroid/view/Menu;

    if-eqz v1, :cond_4

    .line 953
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectedItemCount:I

    if-nez v1, :cond_0

    .line 955
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mOptionsMenu:Landroid/view/Menu;

    const v2, 0x7f0f0121

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 957
    .local v0, "deleteItem":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 959
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 963
    .end local v0    # "deleteItem":Landroid/view/MenuItem;
    :cond_0
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mOptionsMenu:Landroid/view/Menu;

    const v2, 0x7f0f013d

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 965
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mOptionsMenu:Landroid/view/Menu;

    const v2, 0x7f0f013d

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 967
    :cond_1
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 969
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 971
    :cond_2
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 973
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 976
    :cond_3
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 978
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->getCount()I

    move-result v1

    if-lez v1, :cond_5

    .line 979
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 987
    :cond_4
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v2, 0x7f0f0139

    invoke-static {v1, p1, v2}, Lcom/sec/android/app/myfiles/utils/VZCloudUtils;->prepareOptionsMenu(Landroid/content/Context;Landroid/view/Menu;I)V

    .line 988
    return-void

    .line 981
    :cond_5
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onRefresh()V
    .locals 2

    .prologue
    .line 430
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mFragmentId:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->refreshAdapter(I)V

    .line 433
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mEmptyView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 442
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mViewMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 444
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mEmptyView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->setEmptyView(Landroid/view/View;)V

    .line 451
    :cond_0
    :goto_0
    return-void

    .line 448
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mEmptyView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setEmptyView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 409
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onResume()V

    .line 411
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->onRefresh()V

    .line 413
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->updateSelectedCount()V

    .line 415
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    const-string v1, "6"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setCategoryItemListSelection(Ljava/lang/String;)I

    .line 417
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mEmptyView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 418
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_0

    .line 419
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/MainActivity;->updateEmptyView()V

    .line 424
    :cond_0
    return-void
.end method

.method protected selectAllItem()V
    .locals 3

    .prologue
    .line 1374
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_0

    .line 1376
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    .line 1379
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->dismissPopupMenu()V

    .line 1381
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    if-eqz v0, :cond_1

    .line 1383
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->selectAllItem()V

    .line 1385
    :cond_1
    return-void
.end method

.method public selectItemControl(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 1415
    if-nez p1, :cond_0

    .line 1416
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->selectAllItem()V

    .line 1420
    :goto_0
    return-void

    .line 1418
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->unselectAllItem()V

    goto :goto_0
.end method

.method public setSelectModeActionBar()V
    .locals 4

    .prologue
    const/4 v3, 0x7

    .line 1303
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->getSelectModeFrom()I

    move-result v0

    if-nez v0, :cond_1

    .line 1305
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v0, :cond_0

    .line 1307
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const/high16 v1, 0x7f040000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectActionBarView:Landroid/view/View;

    .line 1310
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectActionBarView:Landroid/view/View;

    const v1, 0x7f0f0003

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectAllButton:Landroid/widget/Button;

    .line 1313
    new-instance v0, Landroid/widget/PopupMenu;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectAllButton:Landroid/widget/Button;

    invoke-direct {v0, v1, v2}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectAllPopupMenu:Landroid/widget/PopupMenu;

    .line 1315
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectAllPopupMenu:Landroid/widget/PopupMenu;

    const v1, 0x7f0e001e

    invoke-virtual {v0, v1}, Landroid/widget/PopupMenu;->inflate(I)V

    .line 1343
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectAllPopupMenu:Landroid/widget/PopupMenu;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$11;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$11;-><init>(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 1363
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActionBarManager:Lcom/sec/android/app/myfiles/view/ActionBarManager;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectActionBarView:Landroid/view/View;

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/app/myfiles/view/ActionBarManager;->setActionBar(ILandroid/view/View;)V

    .line 1370
    :cond_0
    :goto_0
    return-void

    .line 1366
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActionBarManager:Lcom/sec/android/app/myfiles/view/ActionBarManager;

    if-eqz v0, :cond_0

    .line 1368
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActionBarManager:Lcom/sec/android/app/myfiles/view/ActionBarManager;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/myfiles/view/ActionBarManager;->setActionBar(I)V

    goto :goto_0
.end method

.method public startDragMode(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "arg"    # Landroid/os/Bundle;

    .prologue
    .line 1291
    return-void
.end method

.method public startFileOperation(I)V
    .locals 5
    .param p1, "menuType"    # I

    .prologue
    .line 661
    const/4 v3, 0x2

    if-ne p1, v3, :cond_2

    .line 672
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v2

    .line 674
    .local v2, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectedItem:Ljava/util/ArrayList;

    if-nez v3, :cond_0

    .line 676
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectedItem:Ljava/util/ArrayList;

    .line 683
    :goto_0
    const/4 v3, 0x0

    iput v3, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mUninstallAppIndex:I

    .line 687
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 689
    .local v1, "selectedItem":Ljava/lang/Object;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectedItem:Ljava/util/ArrayList;

    check-cast v1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .end local v1    # "selectedItem":Ljava/lang/Object;
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 680
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mSelectedItem:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 692
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    iget v3, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mUninstallAppIndex:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mUninstallAppIndex:I

    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->startUninstallApps(I)V

    .line 694
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_2
    return-void
.end method

.method protected unselectAllItem()V
    .locals 3

    .prologue
    .line 1390
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_0

    .line 1392
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    .line 1395
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->dismissPopupMenu()V

    .line 1397
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    if-eqz v0, :cond_1

    .line 1399
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->unselectAllItem()V

    .line 1401
    :cond_1
    return-void
.end method
