.class public Lcom/sec/android/app/myfiles/utils/EncryptionUtils;
.super Landroid/preference/Preference;
.source "EncryptionUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;
    }
.end annotation


# static fields
.field private static BUFFER_SIZE:I = 0x0

.field private static final DEFAULT_CRYPTO_KEY:Ljava/lang/String; = "12345678901234567890123456789012"

.field private static final MODULE:Ljava/lang/String; = "EncryptionUtils"

.field private static index:I

.field public static mIsLock:Z

.field private static numberofParms:J


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 56
    const/16 v0, 0x800

    sput v0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->BUFFER_SIZE:I

    .line 60
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->numberofParms:J

    .line 62
    sput v2, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->index:I

    .line 64
    sput-boolean v2, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->mIsLock:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->mContext:Landroid/content/Context;

    .line 72
    iput-object p1, p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->mContext:Landroid/content/Context;

    .line 74
    return-void
.end method

.method private static CreateSecurityKey(Ljava/lang/String;)[B
    .locals 8
    .param p0, "szKey"    # Ljava/lang/String;

    .prologue
    .line 104
    const/16 v5, 0x10

    .line 105
    .local v5, "radix":I
    const/16 v6, 0x10

    if-ne v5, v6, :cond_1

    const/4 v1, 0x2

    .line 106
    .local v1, "divLen":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    .line 107
    .local v4, "lenKeyString":I
    rem-int v6, v4, v1

    const/4 v7, 0x1

    if-ne v6, v7, :cond_2

    .line 108
    const/4 v0, 0x0

    .line 118
    :cond_0
    return-object v0

    .line 105
    .end local v1    # "divLen":I
    .end local v4    # "lenKeyString":I
    :cond_1
    const/4 v1, 0x3

    goto :goto_0

    .line 110
    .restart local v1    # "divLen":I
    .restart local v4    # "lenKeyString":I
    :cond_2
    div-int/2addr v4, v1

    .line 112
    new-array v0, v4, [B

    .line 113
    .local v0, "byteKey":[B
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v4, :cond_0

    .line 114
    mul-int v3, v2, v1

    .line 115
    .local v3, "idx":I
    add-int v6, v3, v1

    invoke-virtual {p0, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v5}, Ljava/lang/Short;->parseShort(Ljava/lang/String;I)S

    move-result v6

    int-to-byte v6, v6

    aput-byte v6, v0, v2

    .line 113
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method static synthetic access$100(Ljava/lang/String;)[B
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->CreateSecurityKey(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200()J
    .locals 2

    .prologue
    .line 49
    sget-wide v0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->numberofParms:J

    return-wide v0
.end method

.method static synthetic access$202(J)J
    .locals 0
    .param p0, "x0"    # J

    .prologue
    .line 49
    sput-wide p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->numberofParms:J

    return-wide p0
.end method

.method static synthetic access$300()I
    .locals 1

    .prologue
    .line 49
    sget v0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->BUFFER_SIZE:I

    return v0
.end method

.method static synthetic access$400()I
    .locals 1

    .prologue
    .line 49
    sget v0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->index:I

    return v0
.end method

.method static synthetic access$402(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 49
    sput p0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->index:I

    return p0
.end method

.method static synthetic access$408()I
    .locals 2

    .prologue
    .line 49
    sget v0, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->index:I

    return v0
.end method

.method public static isEncryptionFile(Ljava/lang/String;)Z
    .locals 5
    .param p0, "fileStr"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x2

    .line 85
    const/4 v0, 0x0

    .line 87
    .local v0, "extension":Ljava/lang/String;
    if-eqz p0, :cond_0

    .line 89
    const-string v1, "EncryptionUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fileStr="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 90
    const-string v1, "."

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 91
    const-string v1, "EncryptionUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "extension="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 94
    const-string v1, "enc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    const/4 v1, 0x1

    .line 99
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static setRandomNum()[B
    .locals 3

    .prologue
    .line 77
    new-instance v1, Ljava/security/SecureRandom;

    invoke-direct {v1}, Ljava/security/SecureRandom;-><init>()V

    .line 78
    .local v1, "sr":Ljava/security/SecureRandom;
    const/16 v2, 0x10

    new-array v0, v2, [B

    .line 79
    .local v0, "output":[B
    invoke-virtual {v1, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 81
    return-object v0
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 67
    invoke-super {p0}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method
