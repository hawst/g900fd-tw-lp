.class Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$1;
.super Landroid/content/BroadcastReceiver;
.source "DropboxNavigation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;-><init>(Landroid/content/Context;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x0

    .line 133
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.sec.android.cloudagent.ACTION_ACCOUNT_SIGNED_IN"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 134
    iget-object v4, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;

    # getter for: Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->access$000(Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;)Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    move-result-object v4

    iput-boolean v6, v4, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->mSignInRequested:Z

    .line 135
    const-string v4, "result"

    invoke-virtual {p2, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 136
    iget-object v4, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;

    # invokes: Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->actionsOnSignIn()V
    invoke-static {v4}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->access$100(Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;)V

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.sec.android.cloudagent.ACTION_RESPONSE_TOKEN"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 139
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 140
    .local v2, "b":Landroid/os/Bundle;
    const-string v4, "com.sec.android.cloudagent.dropbox.credential.tokenkey"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 141
    .local v0, "authKey":Ljava/lang/String;
    const-string v4, "com.sec.android.cloudagent.dropbox.credential.tokensecret"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 142
    .local v1, "authSecret":Ljava/lang/String;
    if-eqz v0, :cond_2

    if-nez v1, :cond_3

    .line 143
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;

    # getter for: Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mResigninRequested:Z
    invoke-static {v4}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->access$200(Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 144
    iget-object v4, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;

    const/4 v5, 0x1

    # setter for: Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mResigninRequested:Z
    invoke-static {v4, v5}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->access$202(Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;Z)Z

    .line 145
    iget-object v4, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;

    # getter for: Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->access$000(Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;)Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->requestSignIn()V

    goto :goto_0

    .line 148
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v3

    .line 149
    .local v3, "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    invoke-virtual {v3, v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setDropboxAuthKey(Ljava/lang/String;)V

    .line 150
    invoke-virtual {v3, v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setDropboxAuthSecret(Ljava/lang/String;)V

    .line 151
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->commit()Z

    .line 152
    iget-object v4, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;

    # getter for: Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->access$000(Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;)Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->setAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    iget-object v4, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->AsyncOpenWithFullSync()V

    goto :goto_0
.end method
