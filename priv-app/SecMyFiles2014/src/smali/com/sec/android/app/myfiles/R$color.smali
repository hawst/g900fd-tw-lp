.class public final Lcom/sec/android/app/myfiles/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final actionbar_menu_text_disabled:I = 0x7f080037

.field public static final actionbar_menu_text_enabled:I = 0x7f080036

.field public static final actionbar_menu_text_pressed:I = 0x7f080038

.field public static final actionbar_menu_text_selector:I = 0x7f080045

.field public static final add_ftp_touch_out_backcolor:I = 0x7f08003a

.field public static final advance_list_sub_line_text:I = 0x7f080012

.field public static final bottom_button_textcolor_for_locksettings:I = 0x7f080046

.field public static final btn_text_style:I = 0x7f080047

.field public static final category_home_grid_background:I = 0x7f080044

.field public static final checked_bitmap_color_list:I = 0x7f080048

.field public static final confirm_oma_label_text:I = 0x7f080042

.field public static final default_text_color1:I = 0x7f080039

.field public static final detail_view_sub_text:I = 0x7f080043

.field public static final direct_drag_drop_background:I = 0x7f08003e

.field public static final drag_and_drop_file_name:I = 0x7f080014

.field public static final drag_entered_item_background:I = 0x7f08003d

.field public static final drag_entered_item_text:I = 0x7f08003b

.field public static final drag_entered_unavailable_item_text:I = 0x7f08003c

.field public static final drag_shadow_with_thumbs_file_name:I = 0x7f080015

.field public static final drag_shadow_with_thumbs_file_size:I = 0x7f080016

.field public static final empty_view_title:I = 0x7f08003f

.field public static final expandable_list_sub_titlebar_text:I = 0x7f08002e

.field public static final list_divider:I = 0x7f080013

.field public static final list_file_modified_time:I = 0x7f080010

.field public static final list_file_name:I = 0x7f08000f

.field public static final list_file_size:I = 0x7f080011

.field public static final list_headline_text:I = 0x7f08000d

.field public static final list_sub_line_text:I = 0x7f08000e

.field public static final main_category_grid_background:I = 0x7f08001d

.field public static final main_category_grid_cell_background:I = 0x7f08001e

.field public static final main_category_grid_default_color:I = 0x7f080021

.field public static final main_category_grid_divider_color_one:I = 0x7f08001f

.field public static final main_category_grid_divider_color_two:I = 0x7f080020

.field public static final main_category_list_header_color:I = 0x7f080022

.field public static final main_category_list_item_color:I = 0x7f080023

.field public static final main_category_size:I = 0x7f08001b

.field public static final main_category_size_shadow:I = 0x7f08001c

.field public static final main_category_title:I = 0x7f080019

.field public static final main_category_title_shadow:I = 0x7f08001a

.field public static final orange_f:I = 0x7f080004

.field public static final orange_n:I = 0x7f080005

.field public static final orange_p:I = 0x7f080003

.field public static final path_indicator_background:I = 0x7f08002d

.field public static final path_indicator_button_text:I = 0x7f080049

.field public static final path_indicator_button_text_disabled:I = 0x7f08002c

.field public static final path_indicator_button_text_enabled:I = 0x7f08002b

.field public static final path_indicator_button_text_selected:I = 0x7f08002a

.field public static final search_result_highlighted_color:I = 0x7f080033

.field public static final search_spinner_default_text:I = 0x7f080031

.field public static final search_spinner_selected_text:I = 0x7f080032

.field public static final select_all_title:I = 0x7f080041

.field public static final set_date_edit_text:I = 0x7f080035

.field public static final set_date_text:I = 0x7f080034

.field public static final softkey_background:I = 0x7f080029

.field public static final sort_by_sub_title:I = 0x7f080040

.field public static final storage_usage_category_text:I = 0x7f080024

.field public static final storage_usage_empty_bar:I = 0x7f080027

.field public static final storage_usage_space_info_text:I = 0x7f080026

.field public static final storage_usage_storage_type_text:I = 0x7f080025

.field public static final text_color1:I = 0x7f080007

.field public static final text_color12:I = 0x7f080008

.field public static final text_color13:I = 0x7f080009

.field public static final text_color14:I = 0x7f08000a

.field public static final text_color15:I = 0x7f08000b

.field public static final text_color16:I = 0x7f08000c

.field public static final textappearance_softkey:I = 0x7f08004a

.field public static final thumb_background_color:I = 0x7f080006

.field public static final thumbnail_view_item_title:I = 0x7f080017

.field public static final title_bar_background:I = 0x7f080018

.field public static final tree_view_item_bg:I = 0x7f080030

.field public static final tree_view_item_text:I = 0x7f08002f

.field public static final treeview_item:I = 0x7f08004b

.field public static final tw_color005:I = 0x7f080028

.field public static final twcolor001:I = 0x7f080000

.field public static final twcolor101:I = 0x7f080001

.field public static final twcolor201:I = 0x7f080002

.field public static final unchecked_bitmap_color_list:I = 0x7f08004c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
