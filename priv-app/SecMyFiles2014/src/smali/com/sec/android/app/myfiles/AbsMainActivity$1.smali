.class Lcom/sec/android/app/myfiles/AbsMainActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "AbsMainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/AbsMainActivity;->setEasyModeChangeReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/AbsMainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/AbsMainActivity;)V
    .locals 0

    .prologue
    .line 357
    iput-object p1, p0, Lcom/sec/android/app/myfiles/AbsMainActivity$1;->this$0:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 362
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 363
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 365
    .local v2, "uri":Landroid/net/Uri;
    const/4 v3, 0x2

    const-string v4, "AbsMainActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Broadcast mEasyModeChangeReceiver receive : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " , uri : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 367
    const-string v3, "com.android.launcher.action.EASY_MODE_CHANGE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 369
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v1

    .line 370
    .local v1, "easyModeState":Z
    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->setEasyMode(Z)V

    .line 372
    iget-object v3, p0, Lcom/sec/android/app/myfiles/AbsMainActivity$1;->this$0:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 373
    iget-object v3, p0, Lcom/sec/android/app/myfiles/AbsMainActivity$1;->this$0:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getCreateAdapter(I)Landroid/widget/BaseAdapter;

    .line 374
    iget-object v3, p0, Lcom/sec/android/app/myfiles/AbsMainActivity$1;->this$0:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->refreshCategoryTree()V

    .line 376
    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/AbsMainActivity$1;->this$0:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/AbsMainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/AbsMainActivity$1;->this$0:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/AbsMainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget v3, v3, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mId:I

    if-nez v3, :cond_0

    .line 378
    iget-object v3, p0, Lcom/sec/android/app/myfiles/AbsMainActivity$1;->this$0:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/AbsMainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v3, :cond_0

    .line 379
    iget-object v3, p0, Lcom/sec/android/app/myfiles/AbsMainActivity$1;->this$0:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/AbsMainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v3, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->dismissDialog()V

    .line 384
    .end local v1    # "easyModeState":Z
    :cond_0
    return-void
.end method
