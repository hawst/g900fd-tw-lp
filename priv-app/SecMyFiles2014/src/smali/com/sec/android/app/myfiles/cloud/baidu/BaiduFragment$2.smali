.class Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$2;
.super Lcom/samsung/android/privatemode/IPrivateModeClient$Stub;
.source "BaiduFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)V
    .locals 0

    .prologue
    .line 633
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    invoke-direct {p0}, Lcom/samsung/android/privatemode/IPrivateModeClient$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onStateChange(II)V
    .locals 4
    .param p1, "state"    # I
    .param p2, "extInfo"    # I

    .prologue
    const/4 v3, 0x2

    .line 638
    const-string v0, "BaiduFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PrivateModeClient onStateChange: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 641
    if-nez p1, :cond_0

    .line 643
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$700(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/samsung/android/privatemode/PrivateModeManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 645
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$900(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/samsung/android/privatemode/PrivateModeManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/samsung/android/privatemode/PrivateModeManager;->registerClient(Lcom/samsung/android/privatemode/IPrivateModeClient;)Landroid/os/IBinder;

    move-result-object v1

    # setter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$802(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;Landroid/os/IBinder;)Landroid/os/IBinder;

    .line 648
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$1000(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Landroid/os/IBinder;

    move-result-object v0

    if-nez v0, :cond_0

    .line 649
    const-string v0, "BaiduFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PrivateModeClient is not registered : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 656
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 657
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # invokes: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->moveDropboxToSecretBox()V
    invoke-static {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$1100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)V

    .line 660
    :cond_1
    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    .line 662
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$1200(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 664
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$1400(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/samsung/android/privatemode/PrivateModeManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$1300(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/privatemode/PrivateModeManager;->unregisterClient(Landroid/os/IBinder;Z)Z

    .line 666
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$1502(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;Landroid/os/IBinder;)Landroid/os/IBinder;

    .line 669
    :cond_2
    return-void
.end method
