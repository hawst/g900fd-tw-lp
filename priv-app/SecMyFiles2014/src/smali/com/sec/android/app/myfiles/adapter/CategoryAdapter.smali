.class public Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
.source "CategoryAdapter.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIsDropBoxEnabled:Z

.field private mIsItemViewLayerTypeSoftware:Z

.field private mIsSelectMode:Z

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/CategoryItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/CategoryItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/CategoryItem;>;"
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;-><init>()V

    .line 47
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mIsItemViewLayerTypeSoftware:Z

    .line 49
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mIsDropBoxEnabled:Z

    .line 55
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mContext:Landroid/content/Context;

    .line 57
    iput-object p2, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    .line 58
    return-void
.end method

.method private isApplyMarqueeToSize()Z
    .locals 2

    .prologue
    .line 796
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 798
    .local v0, "languageCode":Ljava/lang/String;
    const-string v1, "ar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method


# virtual methods
.method public addItem(ILcom/sec/android/app/myfiles/element/CategoryItem;)V
    .locals 2
    .param p1, "position"    # I
    .param p2, "item"    # Lcom/sec/android/app/myfiles/element/CategoryItem;

    .prologue
    .line 72
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v1

    .line 74
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 75
    monitor-exit v1

    .line 76
    return-void

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addItem(Lcom/sec/android/app/myfiles/element/CategoryItem;)V
    .locals 2
    .param p1, "item"    # Lcom/sec/android/app/myfiles/element/CategoryItem;

    .prologue
    .line 63
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v1

    .line 65
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 66
    monitor-exit v1

    .line 67
    return-void

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 281
    const/4 v0, 0x0

    return v0
.end method

.method public clear()V
    .locals 6

    .prologue
    .line 86
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v4

    .line 88
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 90
    .local v2, "removeItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/CategoryItem;>;"
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/CategoryItem;

    .line 92
    .local v1, "item":Lcom/sec/android/app/myfiles/element/CategoryItem;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getType()I

    move-result v3

    const/16 v5, 0x1a

    if-ne v3, v5, :cond_0

    .line 94
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 99
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/CategoryItem;
    .end local v2    # "removeItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/CategoryItem;>;"
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 98
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v2    # "removeItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/CategoryItem;>;"
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 99
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 100
    return-void
.end method

.method public finishSelectMode()V
    .locals 1

    .prologue
    .line 725
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mIsSelectMode:Z

    .line 727
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->notifyDataSetChanged()V

    .line 729
    return-void
.end method

.method public getCount()I
    .locals 3

    .prologue
    .line 135
    const/4 v0, 0x0

    .line 137
    .local v0, "value":I
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v2

    .line 139
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 140
    monitor-exit v2

    .line 142
    return v0

    .line 140
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 175
    const/4 v1, 0x0

    .line 179
    .local v1, "obj":Ljava/lang/Object;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 182
    monitor-exit v3

    .line 189
    .end local v1    # "obj":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 182
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 184
    :catch_0
    move-exception v0

    .line 186
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getItemByType(I)Ljava/lang/Object;
    .locals 6
    .param p1, "type"    # I

    .prologue
    .line 195
    const/4 v3, 0x0

    .line 199
    .local v3, "obj":Ljava/lang/Object;
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 201
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/element/CategoryItem;

    .line 203
    .local v2, "item":Lcom/sec/android/app/myfiles/element/CategoryItem;
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getType()I

    move-result v4

    if-ne v4, p1, :cond_0

    .line 205
    move-object v3, v2

    .line 210
    .end local v2    # "item":Lcom/sec/android/app/myfiles/element/CategoryItem;
    .end local v3    # "obj":Ljava/lang/Object;
    :cond_1
    monitor-exit v5

    .line 217
    .end local v1    # "i$":Ljava/util/Iterator;
    :goto_0
    return-object v3

    .line 210
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v4
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 212
    :catch_0
    move-exception v0

    .line 214
    .local v0, "e":Ljava/lang/Exception;
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getItemCount(I)I
    .locals 5
    .param p1, "itemType"    # I

    .prologue
    .line 148
    const/4 v2, 0x0

    .line 150
    .local v2, "result":I
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v4

    .line 152
    const/4 v3, -0x1

    if-ne p1, v3, :cond_1

    .line 154
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 166
    :cond_0
    monitor-exit v4

    .line 168
    return v2

    .line 158
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/CategoryItem;

    .line 160
    .local v1, "item":Lcom/sec/android/app/myfiles/element/CategoryItem;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getType()I

    move-result v3

    if-ne v3, p1, :cond_2

    .line 162
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 166
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/CategoryItem;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public getItemId(I)J
    .locals 7
    .param p1, "position"    # I

    .prologue
    .line 262
    const-wide/16 v2, -0x1

    .line 266
    .local v2, "id":J
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 268
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/CategoryItem;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getType()I

    move-result v1

    int-to-long v2, v1

    .line 269
    monitor-exit v6

    move-wide v4, v2

    .line 276
    .end local v2    # "id":J
    .local v4, "id":J
    :goto_0
    return-wide v4

    .line 269
    .end local v4    # "id":J
    .restart local v2    # "id":J
    :catchall_0
    move-exception v1

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 271
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    move-wide v4, v2

    .line 273
    .end local v2    # "id":J
    .restart local v4    # "id":J
    goto :goto_0
.end method

.method public getSelectedItem()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 223
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 225
    .local v2, "selectedItem":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v4

    .line 227
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/CategoryItem;

    .line 229
    .local v1, "item":Lcom/sec/android/app/myfiles/element/CategoryItem;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 231
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 234
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/CategoryItem;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 236
    return-object v2
.end method

.method public getSelectedItemCount()I
    .locals 5

    .prologue
    .line 242
    const/4 v2, 0x0

    .line 244
    .local v2, "result":I
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v4

    .line 246
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/CategoryItem;

    .line 248
    .local v1, "item":Lcom/sec/android/app/myfiles/element/CategoryItem;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 250
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 253
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/CategoryItem;
    :cond_1
    monitor-exit v4

    .line 255
    return v2

    .line 253
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public getSelectedItemPosition()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 790
    const/4 v0, 0x0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 26
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 299
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const-string v22, "layout_inflater"

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 300
    .local v6, "inflater":Landroid/view/LayoutInflater;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 301
    .local v12, "res":Landroid/content/res/Resources;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/myfiles/element/CategoryItem;

    .line 303
    .local v9, "item":Lcom/sec/android/app/myfiles/element/CategoryItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/myfiles/utils/Utils;->getScreenState(Landroid/content/Context;)I

    move-result v11

    .line 304
    .local v11, "mScreenState":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v7

    .line 306
    .local v7, "isEasyMode":Z
    if-eqz v7, :cond_b

    .line 308
    const v21, 0x7f040018

    const/16 v22, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p3

    move/from16 v2, v22

    invoke-virtual {v6, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 321
    :goto_0
    const v21, 0x7f0f0031

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 323
    .local v5, "icon":Landroid/widget/ImageView;
    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getId()I

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 325
    const v21, 0x7f0f0004

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .line 327
    .local v15, "title":Landroid/widget/TextView;
    sget-object v21, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 329
    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getTitle()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 340
    const v21, 0x7f0f006e

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 344
    .local v16, "totalSize":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getTotalSize()J

    move-result-wide v22

    invoke-static/range {v21 .. v23}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v18

    .line 346
    .local v18, "totalSizeText":Ljava/lang/String;
    const-string v17, ""

    .line 350
    .local v17, "totalSizeDescription":Ljava/lang/String;
    const/4 v8, 0x0

    .line 354
    .local v8, "isSingular":Z
    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getTotalSize()J

    move-result-wide v22

    const-wide/16 v24, 0x1

    cmp-long v21, v22, v24

    if-eqz v21, :cond_2

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getTotalSize()J

    move-result-wide v22

    const-wide/16 v24, 0x3fa

    cmp-long v21, v22, v24

    if-lez v21, :cond_0

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getTotalSize()J

    move-result-wide v22

    const-wide/16 v24, 0x406

    cmp-long v21, v22, v24

    if-ltz v21, :cond_2

    :cond_0
    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getTotalSize()J

    move-result-wide v22

    const-wide/32 v24, 0xfeb85

    cmp-long v21, v22, v24

    if-lez v21, :cond_1

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getTotalSize()J

    move-result-wide v22

    const-wide/32 v24, 0x10147b

    cmp-long v21, v22, v24

    if-ltz v21, :cond_2

    :cond_1
    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getTotalSize()J

    move-result-wide v22

    const-wide/32 v24, 0x3fae145f

    cmp-long v21, v22, v24

    if-lez v21, :cond_3

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getTotalSize()J

    move-result-wide v22

    const-wide/32 v24, 0x4051ebc0

    cmp-long v21, v22, v24

    if-gez v21, :cond_3

    .line 368
    :cond_2
    const/4 v8, 0x1

    .line 374
    :cond_3
    if-eqz v8, :cond_e

    .line 378
    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getTotalSize()J

    move-result-wide v22

    const-wide/16 v24, 0x386

    cmp-long v21, v22, v24

    if-gez v21, :cond_d

    .line 450
    :cond_4
    :goto_1
    if-eqz v7, :cond_15

    .line 453
    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getType()I

    move-result v21

    const/16 v22, 0x201

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_13

    .line 456
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f0b0161

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 457
    .local v19, "totalStorage":Ljava/lang/String;
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 459
    .local v4, "available":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-static/range {v22 .. v23}, Lcom/sec/android/app/myfiles/utils/Utils;->getStorageCapacity(Landroid/content/Context;I)J

    move-result-wide v22

    invoke-static/range {v21 .. v23}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v14

    .line 460
    .local v14, "storageSizeText":Ljava/lang/String;
    const-string v13, ""

    .line 463
    .local v13, "storageSizeDescription":Ljava/lang/String;
    const-string v21, "G"

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_5

    const-string v21, "g"

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_f

    .line 465
    :cond_5
    const-string v21, "G"

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_6

    .line 500
    :cond_6
    :goto_2
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "/"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 501
    const-string v21, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_7

    const-string v21, ""

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_7

    .line 526
    .end local v4    # "available":Ljava/lang/String;
    .end local v13    # "storageSizeDescription":Ljava/lang/String;
    .end local v14    # "storageSizeText":Ljava/lang/String;
    .end local v19    # "totalStorage":Ljava/lang/String;
    :cond_7
    :goto_3
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->isApplyMarqueeToSize()Z

    move-result v21

    if-eqz v21, :cond_16

    .line 528
    sget-object v21, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 530
    const/16 v21, -0x1

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMarqueeRepeatLimit(I)V

    .line 532
    const/16 v21, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 541
    :goto_4
    const/16 v21, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 630
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mIsSelectMode:Z

    move/from16 v21, v0

    if-eqz v21, :cond_17

    .line 631
    const/16 v21, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 632
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mIsDropBoxEnabled:Z

    .line 633
    const v21, 0x3ecccccd    # 0.4f

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 634
    const v21, -0x777778

    move/from16 v0, v21

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 635
    const v21, -0x777778

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 669
    :goto_5
    const/16 v21, 0x3

    move/from16 v0, v21

    if-ne v11, v0, :cond_8

    .line 670
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 672
    .local v10, "mRes":Landroid/content/res/Resources;
    const v21, 0x7f0f006d

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    .line 673
    .local v20, "v":Landroid/view/View;
    new-instance v21, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v22, -0x1

    const v23, 0x7f0900ac

    move/from16 v0, v23

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    invoke-direct/range {v21 .. v23}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 676
    .end local v10    # "mRes":Landroid/content/res/Resources;
    .end local v20    # "v":Landroid/view/View;
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mIsItemViewLayerTypeSoftware:Z

    move/from16 v21, v0

    if-eqz v21, :cond_9

    .line 678
    const/16 v21, 0x1

    const/16 v22, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 681
    :cond_9
    if-eqz v7, :cond_a

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getType()I

    move-result v21

    const/16 v22, 0x28

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_a

    .line 699
    const/16 v21, 0x8

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 703
    :cond_a
    return-object p2

    .line 313
    .end local v5    # "icon":Landroid/widget/ImageView;
    .end local v8    # "isSingular":Z
    .end local v15    # "title":Landroid/widget/TextView;
    .end local v16    # "totalSize":Landroid/widget/TextView;
    .end local v17    # "totalSizeDescription":Ljava/lang/String;
    .end local v18    # "totalSizeText":Ljava/lang/String;
    :cond_b
    const/16 v21, 0x3

    move/from16 v0, v21

    if-eq v11, v0, :cond_c

    .line 314
    const v21, 0x7f040017

    const/16 v22, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p3

    move/from16 v2, v22

    invoke-virtual {v6, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_0

    .line 316
    :cond_c
    const v21, 0x7f040019

    const/16 v22, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p3

    move/from16 v2, v22

    invoke-virtual {v6, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_0

    .line 386
    .restart local v5    # "icon":Landroid/widget/ImageView;
    .restart local v8    # "isSingular":Z
    .restart local v15    # "title":Landroid/widget/TextView;
    .restart local v16    # "totalSize":Landroid/widget/TextView;
    .restart local v17    # "totalSizeDescription":Ljava/lang/String;
    .restart local v18    # "totalSizeText":Ljava/lang/String;
    :cond_d
    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getTotalSize()J

    move-result-wide v22

    const-wide/32 v24, 0xe1002

    cmp-long v21, v22, v24

    if-ltz v21, :cond_4

    .line 394
    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getTotalSize()J

    move-result-wide v22

    const-wide/32 v24, 0x38400021

    cmp-long v21, v22, v24

    if-gez v21, :cond_4

    goto/16 :goto_1

    .line 418
    :cond_e
    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getTotalSize()J

    move-result-wide v22

    const-wide/16 v24, 0x386

    cmp-long v21, v22, v24

    if-ltz v21, :cond_4

    .line 426
    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getTotalSize()J

    move-result-wide v22

    const-wide/32 v24, 0xe1002

    cmp-long v21, v22, v24

    if-ltz v21, :cond_4

    .line 434
    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getTotalSize()J

    move-result-wide v22

    const-wide/32 v24, 0x38400021

    cmp-long v21, v22, v24

    if-gez v21, :cond_4

    goto/16 :goto_1

    .line 474
    .restart local v4    # "available":Ljava/lang/String;
    .restart local v13    # "storageSizeDescription":Ljava/lang/String;
    .restart local v14    # "storageSizeText":Ljava/lang/String;
    .restart local v19    # "totalStorage":Ljava/lang/String;
    :cond_f
    const-string v21, "M"

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_10

    const-string v21, "m"

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_11

    .line 476
    :cond_10
    const-string v21, "M"

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_6

    goto/16 :goto_2

    .line 485
    :cond_11
    const-string v21, "K"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_12

    const-string v21, "k"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_6

    .line 487
    :cond_12
    const-string v21, "K"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_6

    goto/16 :goto_2

    .line 506
    .end local v4    # "available":Ljava/lang/String;
    .end local v13    # "storageSizeDescription":Ljava/lang/String;
    .end local v14    # "storageSizeText":Ljava/lang/String;
    .end local v19    # "totalStorage":Ljava/lang/String;
    :cond_13
    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getType()I

    move-result v21

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_14

    .line 507
    const-string v21, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 510
    :cond_14
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 511
    const-string v21, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_7

    goto/16 :goto_3

    .line 519
    :cond_15
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 520
    const-string v21, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_7

    goto/16 :goto_3

    .line 536
    :cond_16
    const/16 v21, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 538
    const/16 v21, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    goto/16 :goto_4

    .line 637
    :cond_17
    const/16 v21, 0x1

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 638
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mIsDropBoxEnabled:Z

    .line 639
    const/high16 v21, 0x3f800000    # 1.0f

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 640
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f080019

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getColor(I)I

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 641
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f08001b

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getColor(I)I

    move-result v21

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_5
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 286
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mIsDropBoxEnabled:Z

    if-eqz v0, :cond_0

    .line 287
    const/4 v0, 0x1

    .line 289
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSelectMode()Z
    .locals 1

    .prologue
    .line 735
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mIsSelectMode:Z

    return v0
.end method

.method public onRefresh()V
    .locals 0

    .prologue
    .line 81
    return-void
.end method

.method public removeAllItem(Ljava/util/ArrayList;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 118
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    if-eqz p1, :cond_0

    .line 120
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v1

    .line 122
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    monitor-exit v1

    .line 127
    :goto_0
    return v0

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 127
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeItem(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 105
    const/4 v0, 0x0

    .line 107
    .local v0, "result":Z
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v2

    .line 109
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 110
    monitor-exit v2

    .line 112
    return v0

    .line 110
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public selectAllItem()V
    .locals 4

    .prologue
    .line 756
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v3

    .line 758
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/CategoryItem;

    .line 760
    .local v1, "item":Lcom/sec/android/app/myfiles/element/CategoryItem;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/element/CategoryItem;->setSelected(Z)V

    goto :goto_0

    .line 762
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/CategoryItem;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 764
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->notifyDataSetChanged()V

    .line 765
    return-void
.end method

.method public selectItem(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 742
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v2

    .line 744
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/element/CategoryItem;

    .line 746
    .local v0, "item":Lcom/sec/android/app/myfiles/element/CategoryItem;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getSelected()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/element/CategoryItem;->setSelected(Z)V

    .line 747
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 749
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->notifyDataSetChanged()V

    .line 750
    return-void

    .line 746
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 747
    .end local v0    # "item":Lcom/sec/android/app/myfiles/element/CategoryItem;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public selectMode(Z)V
    .locals 0
    .param p1, "selectMode"    # Z

    .prologue
    .line 803
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mIsSelectMode:Z

    .line 804
    return-void
.end method

.method public setItemViewLayerTypeSoftware(Z)V
    .locals 0
    .param p1, "useSoftwareLayer"    # Z

    .prologue
    .line 710
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mIsItemViewLayerTypeSoftware:Z

    .line 711
    return-void
.end method

.method public setSelectedItemChangeListener(Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    .prologue
    .line 785
    return-void
.end method

.method public startSelectMode(II)V
    .locals 1
    .param p1, "selectionType"    # I
    .param p2, "from"    # I

    .prologue
    .line 717
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mIsSelectMode:Z

    .line 719
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->notifyDataSetChanged()V

    .line 720
    return-void
.end method

.method public unselectAllItem()V
    .locals 4

    .prologue
    .line 771
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v3

    .line 773
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/CategoryItem;

    .line 775
    .local v1, "item":Lcom/sec/android/app/myfiles/element/CategoryItem;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/element/CategoryItem;->setSelected(Z)V

    goto :goto_0

    .line 777
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/CategoryItem;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 779
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->notifyDataSetChanged()V

    .line 780
    return-void
.end method
